/**
 * @properties={typeid:35,uuid:"CBB45632-09A7-4499-9112-71CA0BD68651",variableType:-4}
 */
var logged = false;

/**
 * @properties={typeid:35,uuid:"F3BC4473-F2D5-430F-8D8F-5CA79EEFF728",variableType:-4}
 */
var aut_user = null;

/**
 * @properties={typeid:35,uuid:"F5402D3C-52B1-4D25-BA72-FEEC636278D7",variableType:-4}
 */
var aut_email = null;

/**
 * @properties={typeid:35,uuid:"A3337CAA-7C14-4F1E-9508-8CEACDE71AB2",variableType:-4}
 */
var foundsetLog = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"191D5ED6-9D6E-4C9B-98A2-EB5EDD1E37E3"}
 */
var error = '';

/**
 * @AllowToRunInFind
 *
 * TODO generated, please specify type and doc for the params
 * @param User
 * @param resultLDAP
 * @param fromWhere
 *
 * @return {String} error
 *
 * @properties={typeid:24,uuid:"29E653E2-7C92-4D0B-8D68-CA361257FF13"}
 */
function loginUser(User, resultLDAP,fromWhere) {

	var arr = {
		user: null,
		mail: null
	};

	if (!User) {
		error = "Username e/o Password errati.";
		return error;
	}

	if (resultLDAP == true) {

		/** @type {JSFoundSet<db:/ferrari/k8_gestione_utenze>} */
		var users = databaseManager.getFoundSet('ferrari', 'k8_gestione_utenze');
		if (users.find()) {
			users.user_servoy = User;
			var found = users.search();

			if (found > 0) {

				aut_user = users.user_servoy;
				aut_email = users.email;
				foundsetLog = users;
				arr.user = users.user_servoy;
				arr.mail = users.email;
				var us = arr.user;
				var id_us = users.user_id;
				var group = ['users'];
				var result = security.login(us, id_us, group);
				if (result == true) {
					application.output("Autenticazione effettuata.");
					//Inserisco nel log l'accesso appena eseguito
					/** @type {JSFoundSet<db:/nfx/nfx_access>} */
					var access = databaseManager.getFoundSet('nfx', 'nfx_access');
					var pk = databaseManager.getDataSetByQuery('nfx', "select max( nfx_acces_id) from NFX_ACCESS", [], -1).getValue(1, 1);
					access.newRecord();
					access.nfx_acces_id = pk;
					access.date_time = new Date();
					access.db_action = "login";
					access.login_name = User;
					databaseManager.saveData(access);
					logged = true;
					aut_user = arr.user;
					aut_email = arr.mail;
					return error;
				} else {
					error = 'Login fallito (errore interno).';
					return error;
				}
			} else {
				if (fromWhere == 'VC2') {
					logged = false;
					error = 'Username e/o Password errati.';
					return error;
				}

				/** @type {JSFoundSet<db:/ferrari/so_operatori>} */
				var operators = databaseManager.getFoundSet('ferrari', 'so_operatori');

				if (operators.find()) {
					operators.username = User;
					var opfound = operators.search();

					if (opfound > 0) {
						//IF FOUND OPERATOR
						aut_user = operators.username;
						foundsetLog = operators;
						arr.user = operators.username;
						var us2 = arr.user;
						var id_us2 = operators.so_operatore_id;
						var group2 = ['users'];
						var result2 = security.login(us2, id_us2, group2);
						if (result2 == true) {
							application.output("Autenticazione effettuata.");
							//Inserisco nel log l'accesso appena eseguito
							/** @type {JSFoundSet<db:/nfx/nfx_access>} */
							var access2 = databaseManager.getFoundSet('nfx', 'nfx_access');
							var pk2 = databaseManager.getDataSetByQuery('nfx', "select max( nfx_acces_id) from NFX_ACCESS", [], -1).getValue(1, 1);
							access2.newRecord();
							access2.nfx_acces_id = pk2;
							access2.date_time = new Date();
							access2.db_action = "login";
							access2.login_name = User;
							databaseManager.saveData(access);
							logged = true;
							aut_user = arr.user;
							return error;

						} else {
							logged = false;
							error = 'Username e/o Password errati.';
							return error;
						}

					} else {
						logged = false;
						error = 'Username e/o Password errati.';
						return error;
					}

				}
			}
		}

	} else {
		error = 'Autenticazione fallita';
		return error;
	}

	return error;

}
