/**
 * Base method to begin an in-memory transaction
 *
 * @param {JSEvent} event The event that triggered the action.
 * @return {Boolean} true if started the transaction, false if not
 * @properties={typeid:24,uuid:"59DD7269-3917-4240-A175-8A17719533B0"}
 *
 * @properties={typeid:24,uuid:"59DD7269-3917-4240-A175-8A17719533B0"}
 */
function startEditing(event) {
	application.output(globals.msgLog + 'START _base.startEditing() ', LOGGINGLEVEL.INFO);
	// begin in-memory transaction
	if (databaseManager.setAutoSave(false)) {
		updateUI(event);
		application.output(globals.msgLog + 'STOP _base.startEditing() OK', LOGGINGLEVEL.INFO);
		return true;
	}
	application.output(globals.msgLog + 'STOP _base.startEditing() KO', LOGGINGLEVEL.INFO);
	return false;
}

/**
 * Base method to close an in-memory transaction.
 * All edits are rolled back, so a save should have already been called
 * @param {JSEvent} event The event that triggered the action.
 *
 * @properties={typeid:24,uuid:"21472EB0-C899-4781-B50B-FDB3588E5A1C"}
 * @AllowToRunInFind
 */
function stopEditing(event) {
	application.output(globals.msgLog + 'START _base.stopEditing()', LOGGINGLEVEL.INFO);
	// revert edits only if in editing

	var answer = globals.DIALOGS.showWarningDialog("Conferma", "Si è sicuri di annullare l'operazione?", "Si", "No");

	if (answer == 'Si') {

		if (foundset.isInFind()) {
			foundset.clear();
			foundset.search();
		}

		if (isEditing()) databaseManager.revertEditedRecords();
		// close in-memory transaction
		databaseManager.setAutoSave(true);
		// MVC: update the view based on the model

		updateUI(event);
		application.output(globals.msgLog + 'STOP _base.stopEditing()', LOGGINGLEVEL.INFO);
	} else return;
}

/**
 * Commit transaction
 * @param event
 *
 * @properties={typeid:24,uuid:"33F5ED50-4623-4173-A258-12812882E10E"}
 */
function saveEditing(event) {
	application.output(globals.msgLog + 'START _base.saveEdits', LOGGINGLEVEL.INFO);
	// validation occurs at entity level
	if (databaseManager.saveData()) {
		// force to recalculate all calculated fields. IMPORTANT!
		databaseManager.recalculate(foundset);
		// close transactions
		stopEditingAfterSave(event);
		// record was saved successfully
		application.output(globals.msgLog + 'STOP _base.saveEdits', LOGGINGLEVEL.INFO);
		return true;
	} else {
		//application.output(globals.msgLog + 'STOP _base.saveEdits ERROR saveEdits failed ' + globals.validationExceptionMessage,LOGGINGLEVEL.INFO);
		globals.DIALOGS.showErrorDialog('Error', 'Failed to save Data', 'OK');
		updateUI(event);
		// Failed to save data
		return false;
	}
}

/**
 * Base method to close an in-memory transaction.
 * All edits are rolled back, so a save should have already been called
 * @param {JSEvent} event The event that triggered the action.
 * @properties={typeid:24,uuid:"EC0EA81E-2415-4CB4-8567-2C5C9575F224"}
 */
function stopEditingAfterSave(event) {
	application.output(globals.messageLog + 'START _base.stopEditing()', LOGGINGLEVEL.DEBUG);
	// revert edits only if in editing
	if (isEditing()) databaseManager.revertEditedRecords();
	// close in-memory transaction
	databaseManager.setAutoSave(true);
	// MVC: update the view based on the model
	updateUI(event);
	application.output(globals.messageLog + 'STOP _base.stopEditing()', LOGGINGLEVEL.DEBUG);
}

/**
 * Base method to determine if a form is editing.
 * This is a convenience method for checking auto-save.
 *
 * @returns {Boolean} True if the form is editing
 *
 * @properties={typeid:24,uuid:"BD190052-F23E-4800-B104-70B5E67DD6F5"}
 */
function isEditing() {
	return !databaseManager.getAutoSave();
}

/**
 * Adds a new record. It opens an in-memory transaction.
 * New records cannot be added if an invalid state exists outside of a transaction
 *
 * @param {JSEvent} event the event that triggered the action
 * @returns {Number} The index of the record that was added
 *
 * @properties={typeid:24,uuid:"346FDE8D-E175-44FD-AE86-7A1C0A3F9723"}
 */
function newRecord(event) {
	// start in-memory transaction
	if (startEditing(event)) {
		// create a new record
		return foundset.newRecord(false);
	}
	return null;
}

/**
 * Handle record selected. Invokes the updateUI() method
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"1FA27AF8-FAB1-4A4D-98B8-ABC9B348F26F"}
 */
function onRecordSelection(event) {
	updateUI(event);
}

/**
 * Callback method for when form is shown. Invokes the updateUI() method
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"4646713A-75C8-4FAD-9CDC-8243A01398C0"}
 */
function onShow(firstShow, event) {
	updateUI(event);
}

/**
 * Default handler for record edit stop event. (record save)
 * Invokes the updateUI() method.
 * @param {JSRecord} record that record was being edited
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"B43BF461-97C2-4BEF-B19D-E6715A32EE70"}
 */
function onRecordEditStop(record, event) {
	updateUI(event);
}

/**
 * This method updates the form appearance based on the state of the model.
 * Called any time the state of the model may have changed.
 * Including: onRecordSelection, onShow, onRecordEditStop
 * Implementation forms should override this method to update the user interface.
 * Implementation forms should call this method when the data model is programmatically changed.
 * Updates visible forms in tabpanels and splitpanes
 *
 * @param {JSEvent} event The event that triggered the action.
 *
 * @properties={typeid:24,uuid:"FD2997C1-E884-4092-8DD0-B43A65D4239C"}
 */
function updateUI(event) {

	var form = null;

	// Iterate over elements
	for (var i = 0; i < elements.length; i++) {
		var e = elements[i];
		// get the element type
		var type = (e.getElementType) ? e.getElementType() : null;

		switch (type) {
		// tab panels
		case ELEMENT_TYPES.TABPANEL:
			// get the selected tab form
			form = forms[elements[i].getTabFormNameAt(elements[i].tabIndex)];
			// update its UI
			if (form && form['updateUI']) form['updateUI']();
			break;
		case ELEMENT_TYPES.SPLITPANE:
			// update left form UI
			form = elements[i].getLeftForm();
			if (form && form['updateUI']) form['updateUI']();
			// update right form UI
			form = elements[i].getRightForm();
			if (form && form['updateUI']) form['updateUI']();
			break;
		}
	}
}
