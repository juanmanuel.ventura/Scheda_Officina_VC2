/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"205D6911-9BD4-4F1C-BF5C-4897837CCCC8"}
 */
function onLoad(event) {

	globals.messageLog = security.getUserUID() ? 'UID: ' + security.getUserUID() + ' - ' : '';

	application.output(globals.messageLog + "START _homepage.onLoad() ", LOGGINGLEVEL.INFO);

	globals.setData();
	globals.setUser();

	application.output(globals.messageLog + "STOP _homepage.onLoad() ", LOGGINGLEVEL.INFO);
}

/**
 * Callback method when form is resized.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"4E6C6881-7C19-4311-B337-36B84334E2F4"}
 */
function onResize(event) {

	application.output(globals.messageLog + "START _homepage.onResize() ", LOGGINGLEVEL.INFO);

	//	var x = controller.getFormWidth()/50;
	//	var y = controller.getPartHeight(JSPart.BODY)/50;
	//
	//	elements.logout.setSize(x*5,y*3);
	//	elements.name.setSize(x*15,y*4);
	//	elements.benvenuto.setSize(x*5,y*4);
	//	elements.data.setSize(x*5,y*4);
	//
	//	elements.logout.setLocation(x*40, y);
	//	elements.name.setLocation(x*28, y/3);
	//	elements.benvenuto.setLocation(x*26, y/3);
	//	elements.data.setLocation(x*33, y*2);

	application.output(globals.messageLog + "STOP _homepage.onResize() ", LOGGINGLEVEL.INFO);

}

/**
 * @param {JSEvent} event
 * @param {String} solutionName
 *
 * @properties={typeid:24,uuid:"F3B59357-1F81-4056-8C65-D4AA93D2F5E5"}
 */
function goToSolution(event, solutionName) {

	application.output(globals.messageLog + 'START _homepage.goToSolution() ', LOGGINGLEVEL.INFO);

	//	if (isEditing()) {
	//		globals.DIALOGS.showInfoDialog("Attenzione", "Stai editando un record!","OK");
	//		return;
	//	}
	application.output(globals.messageLog + '_homepage.goToSolution() apri Solution ' + solutionName, LOGGINGLEVEL.INFO);
	var serverUrl = application.getServerURL();
	application.output(globals.messageLog + '_homepage.goToSolution() serverUrl ' + serverUrl, LOGGINGLEVEL.INFO);
	serverUrl = globals.getServerUrl(serverUrl);
	serverUrl = serverUrl + '/servoy-webclient/application/solution/' + solutionName;
	application.output('-----url finale2 ' + serverUrl)
	application.showURL(serverUrl, '_self');
	application.output(globals.messageLog + '_homepage.goToSolution() serverUrl2 ' + serverUrl, LOGGINGLEVEL.INFO);
	try {
		application.closeSolution();
	} catch (e) {
		application.output(globals.messageLog + '_homepage.goToSolution() ERROR: ' + e.message, LOGGINGLEVEL.ERROR);
		application.output(globals.messageLog + '_homepage.goToSolution() ' + e, LOGGINGLEVEL.ERROR);
	}
	application.output(globals.messageLog + 'STOP _homepage.goToSolution() ', LOGGINGLEVEL.INFO);
}
