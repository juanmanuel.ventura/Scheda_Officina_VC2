/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"176087AE-C96B-469F-A145-B2814B5834B9"}
 */
function onLoad(event) { }

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"DD86BEBD-3430-4CFF-8DCC-66BCCF594419"}
 */
function onShow(firstShow, event) {

	_super.onShow(firstShow, event);
	updateUI(event);

}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"EE849B13-E5E2-4396-A01E-F2DB80B9087F"}
 */
function onActionNuova(event) {

	_super.newRecord(event);
	updateUI(event);
}

/**
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"EB5F0DE6-5265-4DC3-8F4B-FBF557929B67"}
 */
function updateUI(event) {
	if (isEditing()) {
		elements.btnNuovaFoot.visible = false;
		elements.btnCancellaFoot.visible = false;

		elements.btnSalvaFoot.visible = true;
		elements.btnAnnullaFoot.visible = true;

		controller.readOnly = false;
	} else {
		elements.btnNuovaFoot.visible = true;
		elements.btnCancellaFoot.visible = true;

		elements.btnSalvaFoot.visible = false;
		elements.btnAnnullaFoot.visible = false;

		controller.readOnly = true;
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"7BBFC8D4-C99E-4031-8D50-C086FBB219FD"}
 */
function onActionAnnulla(event) {
	_super.stopEditing(event);
	updateUI(event);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"128DCF25-A41C-4923-9E5A-FDE9B0D91C9C"}
 */
function onActionSave(event) {
	_super.saveEditing(event);
	updateUI(event);
}
