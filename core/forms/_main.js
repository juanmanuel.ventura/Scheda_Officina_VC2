/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"FB7C3F2A-0B82-4A33-902B-F233D0E3659C"}
 */
var totRec = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"235FD35E-82CD-4ABA-9D86-A87A0602B6F4",variableType:8}
 */
var total = 0;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"8C28786E-E23A-48E6-9F99-36C94AE0989F",variableType:4}
 */
var selected = 0;

/**
 * @type {String}
 *
 *
 *
 * @properties={typeid:35,uuid:"94F6DC58-6B14-47DF-B36B-599BF9C6B591"}
 */
var button6 = "Scheda Anomalia";

/**
 * @type {String}
 *
 *
 *
 * @properties={typeid:35,uuid:"DD719B02-0038-4C00-AC7B-CEABC3EB5261"}
 */
var button5 = "Scheda Collaudo Affidabilità";

/**
 * @type {String}
 *
 *
 *
 * @properties={typeid:35,uuid:"18788945-E8D4-4507-8A3D-06A9A0751863"}
 */
var button4 = "Scheda Collaudo Sperimentazione";

/**
 * @type {String}
 *
 *
 *
 * @properties={typeid:35,uuid:"9AE7C6F0-09B2-4446-82E6-41268F3A5844"}
 */
var button3 = "Scheda Officina Costruzione Sperimentale";

/**
 * @type {String}
 *
 *
 *
 * @properties={typeid:35,uuid:"84994C4D-C6E9-4F14-8346-B3ECF50E6EDC"}
 */
var button2 = "Scheda Officina Veicolo";

/**
 * @type {String}
 *
 *
 *
 * @properties={typeid:35,uuid:"CADA36A2-B16F-465A-A77C-4E51C067E4D2"}
 */
var button1 = "Allestimento";

/**
 * @properties={typeid:35,uuid:"015FCB55-18AF-46D3-8664-F0C93DA764BD",variableType:-4}
 */
var widthTab = null;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 *
 * @properties={typeid:24,uuid:"0A50F636-BE9D-483B-913D-C1D044079668"}
 */
function onLoad(event) {

	application.output(globals.messageLog + "START _main.onLoad() ", LOGGINGLEVEL.INFO);

	application.output(globals.messageLog + "STOP _main.onLoad() ", LOGGINGLEVEL.INFO);

}

/**
 * Callback method when form is resized.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"AA21E2DF-0C60-4488-8641-5C5A51BFABDF"}
 */
function onResize(event) {

	application.output(globals.messageLog + "START _main.onResize() ", LOGGINGLEVEL.INFO);

	application.output(globals.messageLog + "STOP _main.onResize() ", LOGGINGLEVEL.INFO);

}

/**
 * Action onClickButton to change Solution
 * @param {JSEvent} event
 * @param {String} solutionName
 *
 * @properties={typeid:24,uuid:"E52E57B6-B888-47E4-88B7-288E019A4B6B"}
 */
function onActionButton(event, solutionName) {
	application.output(globals.messageLog + "START _main.onActionButton1() ", LOGGINGLEVEL.INFO);
	//	goToSolution(event, "launcher");
	application.output(globals.messageLog + "STOP _main.onActionButton1() ", LOGGINGLEVEL.INFO);
}

/**
 *
 * @properties={typeid:24,uuid:"4741F713-EDE9-42F5-98FD-3EC677F3A455"}
 */
function updateButton(button, text) {
	application.output(globals.messageLog + 'START globals.updateButton() n _ ' + button + 'text : ' + text, LOGGINGLEVEL.INFO);
	if (button == 1) {
		button1 = text;
	}
	if (button == 2) {
		button2 = text;
	}
	if (button == 3) {
		button3 = text;
	}
	if (button == 4) {
		button4 = text;
	}
	if (button == 5) {
		button5 = text;
	}
	if (button == 6) {
		button6 = text;
	}

	application.output(globals.messageLog + 'STOP globals.updateButton() ', LOGGINGLEVEL.INFO);

}

/**
 * Enable or Disabel Navigator
 * @param {JSEvent} event
 * @param {Boolean} isEdit
 * @param {Boolean} isFind
 *
 * @properties={typeid:24,uuid:"9BC2640C-BBBE-413A-89D4-EB5DAABE16E7"}
 */
function checkNavigator(event, isEdit, isFind) {

	application.output(globals.messageLog + "START _navigation_form.disableNavigator() ", LOGGINGLEVEL.INFO);
	elements.btnAllBack.visible = !isEdit;
	elements.btnAllNext.visible = !isEdit;
	elements.tot.visible = !isEdit;
	elements.btnOneBack.visible = !isEdit;
	elements.btnOneNext.visible = !isEdit;

	if (isFind) {
		elements.btnAllBack.visible = !isFind;
		elements.btnAllNext.visible = !isFind;
		elements.tot.visible = !isFind;
		elements.btnOneBack.visible = !isFind;
		elements.btnOneNext.visible = !isFind;
	}

	application.output(globals.messageLog + "STOP _navigation_form.disableNavigator() ", LOGGINGLEVEL.INFO);

}

/**
 * @param nameTab
 *
 * @properties={typeid:24,uuid:"94E2809E-F3E2-4688-B809-E79B3D59BE03"}
 */
function goToTab(nameTab) {
	application.output(globals.messageLog + "START _main.goToTab() " + nameTab, LOGGINGLEVEL.INFO);
	//	if (elements.tabless.tabIndex)
	elements.tabless.removeAllTabs();

	if (forms[nameTab] && forms[nameTab].onLoad && typeof forms[nameTab].onLoad == "function")forms[nameTab].onLoad();
	if (forms[nameTab] && forms[nameTab].onResize && typeof forms[nameTab].onResize == "function")forms[nameTab].onResize();
	if (forms[nameTab] && forms[nameTab].onShow && typeof forms[nameTab].onShow == "function")forms[nameTab].onShow();

	elements.tabless.addTab(forms[nameTab]);

	application.output(globals.messageLog + "STOP _main.goToTab() ", LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"D164A52E-BFCA-41F4-A969-CCAA86412951"}
 */
function onActionNextRecord(event) { }

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"950F10F1-9B9F-4F66-A747-1EE40A9EF50F"}
 */
function onActionBackRecord(event) { }

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"CFABC69F-F904-4CB2-8838-ABD355CD1837"}
 */
function onShow(firstShow, event) { }

/**
 * @param actual
 * @param max
 *
 * @properties={typeid:24,uuid:"480AC7B4-661E-4721-ABB8-487866492984"}
 */
function updateCount(actual, max) {

	application.output(globals.messageLog + "START _main.updateCount() ", LOGGINGLEVEL.INFO);

	var act = actual.toString();
	var m = max.toString();

	var totalRec = act + '/' + m;

	totRec = totalRec.toString();

	application.output(globals.messageLog + "STOP _main.updateCount() " + actual + "/" + max, LOGGINGLEVEL.INFO);

}

/**
 * @param {JSEvent} event The event that triggered the action.
 *
 * @properties={typeid:24,uuid:"3317E691-6048-4D7F-A6E8-7ADC7D10988C"}
 */
function updateUI(event) {

	application.output('START _main.updateUI ');
	_super.updateUI(event);
	var isEdit = isEditing();
	var isFind = foundset.isInFind();

	controller.readOnly = !isEdit;

	elements.btnNew.visible = !isEdit;
	elements.btnDelete.visible = !isEdit;
	elements.btnEdit.visible = !isEdit;

	elements.btnSave.visible = isEdit;
	elements.btnCancel.visible = isEdit;

	if (isFind) {

		controller.readOnly = !isFind;

		elements.ricerca.visible = !isFind;
		elements.btnNew.visible = !isFind;
		elements.btnDelete.visible = !isFind;
		elements.btnEdit.visible = !isFind;
		elements.btnSave.visible = !isFind;
		elements.btnCancel.visible = isFind;
		elements.btnSearch.visible = isFind;
	}

	checkNavigator(event, isEdit, isFind);
	application.output('STOP _main.updateUI ');
}
