/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F067BEB2-529A-49A5-8BBE-5B0FCD553062"}
 */
var msgLog = '';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"84485824-0EE4-4C8E-A002-33381B11A6CF"}
 */
var coreuser = '';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"0E68C2B0-0D36-42C5-AA6B-06E592AFDAF7"}
 */
var data = '';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"0598BE06-0BB3-4738-931C-24F2C53F24DF"}
 */
var servoyuser = '';

/**
 * @properties={typeid:35,uuid:"A096BB1C-F4F1-4664-959B-3E153A8FFC52",variableType:-4}
 */
var messageLog = null;

/**
 * @properties={typeid:35,uuid:"599F9461-1253-4765-83A5-2280D094E324",variableType:-4}
 */
var widthTab = null;

/**
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"608C7CF8-BE8D-43E3-AC7B-AC4D73AE7ED6"}
 */
function setUser() {

	application.output(globals.messageLog + "START globals.setUser() ", LOGGINGLEVEL.INFO);

	globals.coreuser = security.getUserName();

	/** @type {JSFoundSet<db:/ferrari/k8_gestione_utenze>} */
	var users = databaseManager.getFoundSet('ferrari', 'k8_gestione_utenze');

	if (users.find()) {

		users.user_servoy = globals.coreuser;

		var found = users.search();

		if (found > 0) {

			servoyuser = users.user_servoy;

			coreuser = users.nome + " " + users.cognome;

			application.output("Login : " + globals.coreuser);

		} else {

			/** @type {JSFoundSet<db:/ferrari/so_operatori>} */
			var ops = databaseManager.getFoundSet('ferrari', 'so_operatori');
			if (ops.find()) {
				ops.username = security.getUserName();
				var sear = ops.search();
				if (sear > 0) {
					servoyuser = ops.username;
					coreuser = ops.username;
					application.output("Login : " + globals.coreuser);
				}
			}

		}
	}

	application.output(globals.messageLog + "STOP globals.setUser() ", LOGGINGLEVEL.INFO);

}

/**
 * Base method to determine if a form is editing.
 * This is a convenience method for checking auto-save.
 *
 * @returns {Boolean} True if the form is editing
 * @properties={typeid:24,uuid:"C79C02F4-49CE-4249-9FF2-5A53EA8E4DCE"}
 */
function isEditing() {
	// Check Auto-Save state
	return !databaseManager.getAutoSave();
}

/**
 * @properties={typeid:24,uuid:"F13C5F6D-FC7B-4C45-B2F6-2974449D9997"}
 */
function logoutIfEditing() {
	if (isEditing()) {
		var answer = globals.DIALOGS.showWarningDialog("Logout", "you are about to log out, you Edit will be lost if you hit Yes, would you like to log out ?", "yes", "no");
		if (answer == 'yes')
			logout();
		else
			return
	} else {
		logout();
	}
}

/**
 * @properties={typeid:24,uuid:"46537F7C-1736-457B-BFBA-B13E7A5E00FD"}
 */
function logout() {

	application.output(globals.messageLog + "START globals.logout() ", LOGGINGLEVEL.INFO);

	//var serverUrl = application.getServerURL();
	var solutionName = application.getSolutionName();

	var access = databaseManager.getFoundSet('nfx', 'nfx_access');
	access.newRecord();
	access.login_name = servoyuser;
	access.date_time = new Date();
	access.db_action = "logout";
	databaseManager.saveData();
	application.output("Logout : " + globals.coreuser);
	//	application.showURL()
	//	application.closeAllWindows();
	//	var solutionName = application.getSolutionName();
	//	security.logout(solutionName);
	application.output("Logout " + globals.coreuser);

	application.closeAllWindows();
	//	if (serverUrl != 'http://localhost:8080') application.showURL(serverUrl + '/servoy-webclient/application/solution/launcher', '_self');
	security.logout(solutionName);

	application.output(globals.messageLog + "STOP globals.logout() ", LOGGINGLEVEL.INFO);

}

/**
 * @properties={typeid:24,uuid:"849C0FA3-C0C7-4CC2-86AD-64A762154238"}
 */
function setData() {

	application.output(globals.messageLog + "START globals.setData() ", LOGGINGLEVEL.INFO);

	var dataIntera = new Date();

	var giorno = dataIntera.getDate().toString();
	giorno = giorno.substr(0, 2);
	var mese = dataIntera.getMonth() + 1;
	var anno = dataIntera.getFullYear();
	data = giorno + '/' + mese + '/' + anno;
	application.output("Data attuale : " + data);

	application.output(globals.messageLog + "STOP globals.setData() ", LOGGINGLEVEL.INFO);

}

/**
 * @param {String} url
 * @return {String}
 *
 * @properties={typeid:24,uuid:"901CC054-7E4D-442C-9700-5257A38DB24D"}
 */
function getServerUrl(url) {

	application.output(globals.messageLog + "START globals.getServerUrl() ", LOGGINGLEVEL.INFO);

	//http://localhost:8080
	//	application.output('primo ' + url.indexOf(':'));
	//	application.output('secondo ' + url.lastIndexOf(':'));
	//	application.output('ultimo index https: ' + url.indexOf('https'));
	if (url.indexOf('https') == -1 || url.indexOf(':') != url.lastIndexOf(':')) {
		//		application.output('index last : ' + url.lastIndexOf(':'));
		//		application.output('lunghezza ' + url.length);
		//		if (url != 'https://213.254.15.84:8443') url = url.substr(0, (url.lastIndexOf(':')));
		application.output('url finale ' + url);
	}

	application.output(globals.messageLog + "STOP globals.getServerUrl() ", LOGGINGLEVEL.INFO);

	return url;

}
