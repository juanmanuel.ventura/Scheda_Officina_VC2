/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"347F58F0-B401-4E8E-9A26-92FAB8853954"}
 */
var fail = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D7372D57-FDB2-471F-A3CD-397901ADEB03"}
 */
var failMessage = "Con l'ultimo aggiornamento di <b>VirtualCar2</b> è stata introdotta una nuova modalità di login che si appoggia al sistema LDAP di <b>Ferrari</b>, per autenticarsi è quindi necessario inserire: <ul style='list-style-type:disc;'><li><span style='font-weight:bold; text-decoration:underline;'>Nome Utente</span>: questo campo resta invariato, inserisca pure l'utenza utilizzata fino a oggi.</li><li><span style='font-weight:bold; text-decoration:underline;'>Password</span>: da oggi questo campo andrà compilato con la Sua <b style='color:red;'>password di rete Ferrari</b> (<i>ovvero quella che utilizza per accedere agli altri servizi quali posta elettronica etc...</i>).</li></ul>";

/**
 * @properties={typeid:35,uuid:"D906A14E-CCDF-4345-8A3E-30B5FC3EAAAE",variableType:-4}
 */
var infoLDAP = {
	host: "oraoid.unix.ferlan.it",
	login: "DC=gt,cn=AD,cn=Users,dc=ferlan,dc=it"
};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"EC5BEF8D-F7E8-4CC3-BA97-D67F76FDE985"}
 */
var login = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A6B8B5E9-9A1B-4E94-96E2-5608D54CA1D6"}
 */
var password = null;

/**
 * @properties={typeid:24,uuid:"ADC23522-A50D-4B7C-9AA5-0DF010D41E59"}
 */
function loginOnLoad() {

	globals.nfx_registerPlugins();
	globals.nfx_hideMenu();
	fail = "<html><body><table style='border-width:1px; border-style:solid; border-color:red; padding-top:5px;'><tr><td style='text-align:right;'><img src=media:///warning16.png /></td><td style='color:red; font-size:10px; font-weight:bold;'>Attenzione!<td></tr><tr><td colspan=2 style='color:white; font-size:8px; text-align:justify; margin-left:7px; margin-bottom:5px;'>" + failMessage + "</td></tr></table></body></html>";
	elements.fail.visible = false;
}

/**
 * @properties={typeid:24,uuid:"142AC0A5-713A-4CAC-B24A-302FD81DFCBB"}
 */
function loginOnShow() {
	elements.login.requestFocus();
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"ED3F9961-FE46-4D50-9E6F-BF59C93A99DE"}
 */
function enter(event) {
	if(password == null || password == ""){
		setError("Inserisci Password",null);
		return;
	}
	try {
		enterLDAP(event);
	} catch (er) {
		setError(er, 0);
	}
}

/**
 * TODO generated, please specify type and doc for the params
 * @param event
 *
 * @properties={typeid:24,uuid:"BB02D3DF-BA57-4823-9E21-D7257CE83D01"}
 * @AllowToRunInFind
 */
function enterLDAP(event) {

	elements.fail.visible = false;
	elements.enterbutton.enabled = false;
	elements.accessLabel.fgcolor = "#DDDDDD";
	elements.accessLabel.text = "Accesso in corso, attendere ...";
	application.updateUI();

	application.output("Indirizzo : " + application.getServerURL());
	var URL = application.getServerURL();
	
	var ind = URL.lastIndexOf(":");
	
	var resultLDAP = false;
	var log;
	URL = URL.substring(0, ind);
	
	globals.FromVC2 = true;

	application.output("URL : "+URL);	
	if (URL == "http://localhost" || URL == "http://127.0.0.1") {
		//Locale
		application.output("Sei in locale");
		resultLDAP = true;
		application.output("LDAP : "+resultLDAP);
		log = security.authenticate("authenticator_solution", "loginUser", [login, resultLDAP,'VC2']);

	} else if (URL == "http://192.168.32.197") {
		//ServerDev
		application.output("Dev Spindox");
		resultLDAP = true;
		application.output("LDAP : "+resultLDAP);
		log = security.authenticate("authenticator_solution", "loginUser", [login, resultLDAP,'VC2']);
	} else {
		//Produzione
		application.output("Test/Prod");
		var us = login.toLowerCase();
		var connection = local_getConnection(infoLDAP.host, "cn=" + us + "," + infoLDAP.login, password);
		resultLDAP = plugins.it2be_ldapclient.isAuthorized(connection);
		application.output("Connection : "+connection);
		application.output("LDAP : "+resultLDAP);
		log = security.authenticate("authenticator_solution", "loginUser", [login, resultLDAP,'VC2']);
	}

	//metodo

	if (log) {
		setError(log, 0);
	}
}

/**
 * @properties={typeid:24,uuid:"08D8DF9C-CC7D-480C-A3E9-002CC954EBC8"}
 */
function setError(text, color) {
	elements.error.fgcolor = color || "#FF0000";
	elements.error.text = text || "Login fallito (errore interno)";
	elements.accessLabel.text = "";
	elements.enterbutton.enabled = true;
	elements.fail.visible = true;
	application.updateUI();
}

/**
 * TODO generated, please specify type and doc for the params
 * @param _host
 * @param _login
 * @param _password
 *
 * @properties={typeid:24,uuid:"83F40A77-DD53-48AD-8CD3-5FA94E41FF44"}
 */
function local_getConnection(_host, _login, _password) {
	var _connection = plugins.it2be_ldapclient.getConnection(_host);
	_connection.c(_login);
	_connection.d(_password);
	return _connection;
}
