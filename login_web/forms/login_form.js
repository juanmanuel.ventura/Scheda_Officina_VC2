/**
 * @type {String}
 *
 *
 * @properties={typeid:35,uuid:"8D08FB63-97E1-4C41-8664-97077A273E55"}
 */
var failWeb = "";

/**
 * @type {String}
 *
 *
 * @properties={typeid:35,uuid:"5EA52434-7FFC-4D3D-B7DE-2BB2A3570943"}
 */
var messageWeb = "";

/**
 * @properties={typeid:35,uuid:"B013BBF8-583B-443F-BF85-257138018150",variableType:-4}
 */
var firstTime = true;

/**
 *
 * @properties={typeid:35,uuid:"C22C4016-73C4-42AD-9555-EA977559A7D4",variableType:-4}
 */
var infoLDAPWeb = {
	host: "oraoid.unix.ferlan.it",
	login: "DC=gt,cn=AD,cn=Users,dc=ferlan,dc=it"
};

/**
 * @type {String}
 *
 *
 * @properties={typeid:35,uuid:"CA6AA1C3-F8B9-4761-AFD3-0BDEBECD8B52"}
 */
var loginWeb = null;

/**
 * @type {String}
 *
 *
 * @properties={typeid:35,uuid:"F678929B-35EF-4252-B190-DBED55B6A803"}
 */
var passwordWeb = null;

/**
 *
 * @properties={typeid:24,uuid:"B19070F0-8712-4C4F-A1E5-7A71C5BD15EF"}
 */
function loginOnLoadWeb() {

	application.output(globals.messageLog + "START login_form.loginOnLoadWeb() ", LOGGINGLEVEL.INFO);

	setMessageWeb(null, null);

	plugins.it2be_ldapclient.register("4BB8N0EF3BEV9GIP81QD4M7L442E5DLR6VM48I3G99M34CMPONK0H8RC1R2AC492K7NREQ8EDP7BO79KV4EFFD9LGLPF942CR9PJ6ISS8THV7IV51M24A1CLQE9NLBODLM0GPM2K0D08SNBCBO9IPFURVFD9L04QJ5JVJCC57R0DVIRS7JG91J46QMMVJQN60341BC65O8JQUGPBJTI4G0JHSF42BCIRBUR2VHBBHLKODIEBRTV1I3QACV8B1A7TLFEQ7NCSVCR6AFQ9V07NP7T4RUCHVTFJ4SEJK8MLACC00FUODT74DVHT209O10Q7CMIL4D73A633NBS5GQV0Q2TG6HD5C0GNKUQEP66J6UUJ4D36NT94V7QO7VTGPM11TJVL0HBNGHF8CF6JAU9BTU2PFU71QIJIIIPAF9AFF2S3ANL61KTN1M6CETC0HL4CIDKR5KT81GIRQIG5AP78JJ2NIKNDKH47I5VFR69HQANHL5UQ2FCO9Q64O5GQCJ58BOKEQ1F9JPF4VCUVVQS4UL7KJNGPA8AMIG15SNBHDSNAMK2LK383QQI83R0A7PA444J6MIKNAR3B8HNNNGNGF2B20CH51LMG0NGKCNLR9BTPPGOG24HMTCVQD110E5H5SRQFGAAUEKDU90517D50K134Q607F894AFB7C8SDKVCHBBGT3CRA9C9OEA6GQV6JCNRP575AHTQN4G030OSJ816EPNI211SEDK0NJJ8E4BS1BLE80RO9E6A9DKD1QESO8AQDMUMISUNKC39V3FHS7KFL7SE4T9IURPO1QSGJJ7792RSA9A5");
	application.output("Plugin registered successfully");

	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		var win = controller.getWindow();
		var x = win.getWidth() / 2 - 181;
		var y = win.getHeight() / 2 - 136;
		elements.cavallino.setLocation(x, y);

		var x1 = win.getWidth() / 2 - 100;
		var y1 = win.getHeight() / 2 - 60;
		elements.ferrari.setLocation(x1, y1 + 200);

	} else if (application.getApplicationType() == APPLICATION_TYPES.MOBILE_CLIENT) {

	}

	application.output(globals.messageLog + "STOP login_form.loginOnLoadWeb() ", LOGGINGLEVEL.INFO);
}

/**
 * TODO generated, please specify type and doc for the params
 * @param event
 *
 *
 * @properties={typeid:24,uuid:"D6A3352A-6072-4416-B705-0132A6F59BDF"}
 */
function onResize(event) {

	application.output(globals.messageLog + "START login_form.onResize() ", LOGGINGLEVEL.INFO);
	
//	if(firstTime == true){

	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		var win = controller.getWindow();
		var x = win.getWidth() / 2 - 181;
		var y = win.getHeight() / 2 - 136;
		elements.cavallino.setLocation(x, y);

		var x1 = win.getWidth() / 2 - 100;
		var y1 = win.getHeight() / 2 - 60;
		elements.ferrari.setLocation(x1, y1 + 200);

	} else if (application.getApplicationType() == APPLICATION_TYPES.MOBILE_CLIENT) {

	}
	firstTime = false;
//	}
	application.output(globals.messageLog + "STOP login_form.onResize() ", LOGGINGLEVEL.INFO);

}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 *
 * @properties={typeid:24,uuid:"A1158963-4911-4F1A-B24D-6937DD61AB6A"}
 */
function enterWeb(event) {

	application.output(globals.messageLog + "START login_form.enterWeb() ", LOGGINGLEVEL.INFO);

	try {
		enterLDAPWeb(event);
	} catch (er) {
		setMessageWeb(er, "#FF2301");
	}

	application.output(globals.messageLog + "STOP login_form.enterWeb() ", LOGGINGLEVEL.INFO);

}

/**
 * TODO generated, please specify type and doc for the params
 * @param event
 *
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"9FEAC1A0-1180-427C-B9BB-E82450741139"}
 */
function enterLDAPWeb(event) {

	application.output(globals.messageLog + "START login_form.enterLDAPWeb() ", LOGGINGLEVEL.INFO);

	setMessageWeb("Accesso in corso, attendere ...", "#FFFFFF");

	application.output("Indirizzo : " + application.getServerURL());
	var URL = application.getServerURL();
	var ind = URL.lastIndexOf(":");

	var resultLDAP = false;
	var log;
	URL = URL.substring(0, ind);
	
	/** @type {JSFoundSet<db:/ferrari/k8_gestione_utenze>} */
	var users = databaseManager.getFoundSet('ferrari', 'k8_gestione_utenze');
	if (users.find()) {
		users.user_servoy = loginWeb;
		var res = users.search();
		if(res>0){
			
			if (passwordWeb == null || passwordWeb == "") {
				setMessageWeb("Inserisci Password", "#FF2301");
				if (loginWeb == null || loginWeb == "" && passwordWeb == null || passwordWeb == "") {
					setMessageWeb("Inserisci Username e Password", "#FF2301");
					return;
				}
				return;
			}
			
		}else{
			resultLDAP = true;
			log = security.authenticate("authenticator_solution", "loginUser", [loginWeb, resultLDAP,'SCHEDA_OFFICINA']);
		}
	}

	application.output("URL : " + URL);

	if (URL == "http://localhost" || URL == "http://127.0.0.1") {
		//Locale
		application.output("Sei in locale");
		resultLDAP = true;
		application.output("LDAP : " + resultLDAP);
		
		//test to commit 
		
		log = security.authenticate("authenticator_solution", "loginUser", [loginWeb, resultLDAP,'SCHEDA_OFFICINA']);

	} else if (URL == "http://192.168.32.197") {
		//ServerDev
		application.output("Dev Spindox");
		resultLDAP = true;
		application.output("LDAP : " + resultLDAP);
		log = security.authenticate("authenticator_solution", "loginUser", [loginWeb, resultLDAP,'SCHEDA_OFFICINA']);
	} else {
		//Produzione
		application.output("Test/Prod");
		var us = loginWeb.toLowerCase();
		var connection = local_getConnectionWeb(infoLDAPWeb.host, "cn=" + us + "," + infoLDAPWeb.login, passwordWeb);
		resultLDAP = plugins.it2be_ldapclient.isAuthorized(connection);
		application.output("Connection : " + connection);
		application.output("LDAP : " + resultLDAP);
		log = security.authenticate("authenticator_solution", "loginUser", [loginWeb, resultLDAP,'SCHEDA_OFFICINA']);
	}

	//metodo

	if (log) {
		setMessageWeb(log, "#FF2301");
	}

	application.output(globals.messageLog + "STOP login_form.enterLDAPWeb() ", LOGGINGLEVEL.INFO);

}

/**
 *
 * TODO generated, please specify type and doc for the params
 * @param text
 * @param color
 *
 * @properties={typeid:24,uuid:"C462BC19-DF62-4248-A9C8-5DBED02B5E0F"}
 */
function setMessageWeb(text, color) {
	application.output(globals.messageLog + "START login_form.setMessageWeb() ", LOGGINGLEVEL.INFO);

	messageWeb = text;
	elements.message.fgcolor = color;
	elements.message.visible = true;

	application.output(globals.messageLog + "STOP login_form.setMessageWeb() ", LOGGINGLEVEL.INFO);

}

/**
 * TODO generated, please specify type and doc for the params
 * @param _host
 * @param _login
 * @param _password
 *
 *
 * @properties={typeid:24,uuid:"E82CFE4C-1030-4E9E-8353-C61A04E45001"}
 */
function local_getConnectionWeb(_host, _login, _password) {

	application.output(globals.messageLog + "START login_form.local_getConnectionWeb() ", LOGGINGLEVEL.INFO);

	var _connection = plugins.it2be_ldapclient.getConnection(_host);
	_connection.c(_login);
	_connection.d(_password);

	application.output(globals.messageLog + "STOP login_form.local_getConnectionWeb() ", LOGGINGLEVEL.INFO);

	return _connection;

}
