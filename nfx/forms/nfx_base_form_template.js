/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"6F387FB4-502D-4502-8951-D42D9064505A"}
 */
var nfx_orderBy = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"FF5BB887-56A7-4AB6-AF8A-1CFBBA814642"}
 */
var nfx_related = null;

/**
 * @properties={typeid:24,uuid:"15D6D192-EEAF-400E-AB8C-381E273A169E"}
 */
function nfx_defineAccess() {
	return [false, false, false, true];
}

/**
 * @properties={typeid:24,uuid:"6BA9429F-8CEE-44C5-A83B-A2A16A616ED2"}
 */
function README() {
	/*
	 * Questo è il prototipo di un form per NFX.
	 * Ciò che è NECESSARIO definire in questo tipo di form è:
	 *
	 * - nfx_orderBy: è una variabile che definisce l'ordinamento iniziale del foundset del form, va
	 *   specificato in una stringa nel formato "nome_campo1 asc/desc, nome_campo2 asc/desc,...,nome_campoN asc/desc"
	 * - nfx_related: è un array di stringe in cui vanno specificati i nomi degli eventuali form correlati
	 *   a quello corrente nel formato ["nome_form1","nome_form2",...,"nome_formN"]
	 * - nfx_defineAccess: questo metodo definisce se l'utente può aggiungere/togliere/modificare
	 *   record, in più aggiungendo un utimo "true" si andrà ad overridare il metodo nfx_defineAccess
	 *   dei form a questo correlati, impedendo qualsiasi operazione
	 *
	 * É ora possibile definire form "figli" che estendono quello appena creato ereditandone le caratteristiche,
	 * in questi form (se non ci sono vanno definiti qui) è NECESSARIO definire:
	 *
	 * - nfx_getTitle: è un metodo che torna una stringa che sarà il "titolo" del form nell'albero di navigazione, nei
	 *   tabpanel e in tutte quelle situazione in cui serve un "nome umano"
	 * - nfx_isProgram: è un metodo che torna un booleano che identifica la possibilità di inserire il form
	 *   in questione nell'albero di navigazione
	 *
	 * Vi è inoltre la possibilità di definere numerosi metodi atti a personalizzare il comportamento di NFX per adeguarsi
	 * alle più disparate esigenze. Purtroppo non esiste ancora una lista completa di questi metodi, ma per un'idea generale
	 * rimando alla lettura dei file globals.js e nfx_interfaccia_pannello_buttons_method.js, dove è possibile ritrovare
	 * le chiamate a questi metodi e un breve commento agli stessi.
	 */
}
