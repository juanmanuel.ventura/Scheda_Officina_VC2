/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"DFBDF0A5-5226-4966-BA07-EDDD66BB5EB5"}
 */
var search = null;

/**
 *
 * @properties={typeid:24,uuid:"A648E241-6830-4B95-9A34-0DA13CA7E390"}
 * @AllowToRunInFind
 */
function searchForm() {
	if (search) {
		if (controller.find()) {
			nome = search;
			controller.search();
		}
		if (foundset.getSize() > 0) {
			search = null;
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"A41766DC-511C-4710-9061-18DCA3244FD9"}
 * @AllowToRunInFind
 */
function updateAll() {
	var toAdd = new Array();
	var toRemove = new Array();
//	JStaffa
//	var ds = databaseManager.getDataSetByQuery(controller.getServerName(),"select nome from nfx_forms",[],-1);
	var ds = databaseManager.getDataSetByQuery(controller.getDataSource().split('/')[1],"select nome from nfx_forms",[],-1);
	var namesFromDB = ds.getColumnAsArray(1);
//	JStaffa
//	for (var i = 0; i < forms.allnames.length; i++) {
	for (var i = 0; i < solutionModel.getForms().length; i++) {
//		JStaffa
//		if (namesFromDB.indexOf(forms.allnames[i]) == -1) {
		if (namesFromDB.indexOf(solutionModel.getForms[i]) == -1) {
//			JStaffa
//			toAdd.push(forms.allnames[i]);
			toAdd.push(solutionModel.getForms[i]);
		}
	}
	for (var i2 = 0; i2 < namesFromDB.length; i2++) {
//		JStaffa
//		if (forms.allnames.indexOf(namesFromDB[i2]) == -1) {
		if (solutionModel.getForms().indexOf(namesFromDB[i2]) == -1) {
			toRemove.push(namesFromDB[i2]);
		}
	}
	for (var i3 = 0; i3 < toRemove.length; i3++) {
		if (controller.find()) {
			nome = toRemove[i3];
			controller.search();
		}
		controller.deleteRecord();
	}
	for (var i4 = 0; i4 < toAdd.length; i4++) {
		controller.newRecord();
		nome = toAdd[i4];
		titolo = (forms[toAdd[i4]].nfx_getTitle && typeof forms[toAdd[i4]].nfx_getTitle == "function") ? forms[toAdd[i4]].nfx_getTitle() : null;
	}
	databaseManager.saveData();
	plugins.dialogs.showInfoDialog("Completato!", toRemove.length + " form eliminati e " + toAdd.length + " form aggiunti", "Ok");
}

/**
 *
 * @properties={typeid:24,uuid:"95590146-053D-480A-9731-8ACB2C33B81E"}
 */
function updateForm() {
	if (forms[nome]) {
		titolo = (forms[nome].nfx_getTitle && typeof forms[nome].nfx_getTitle == "function") ? forms[nome].nfx_getTitle() : null;
	} else {
		controller.deleteRecord();
	}
	databaseManager.saveData();
}
