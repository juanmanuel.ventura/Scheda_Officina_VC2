/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B7ABD8E9-2B54-4BA4-8B69-14E67B8EA075"}
 */
var password_value = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D1805395-E471-46B0-A749-904843958A0E"}
 */
var user_servoy = null;

/**
 *
 * @properties={typeid:24,uuid:"7F098FD6-1B5B-422F-8584-87887064D2A1"}
 * @AllowToRunInFind
 */
function set() {
	//	JStaffa
	//	var gu = databaseManager.getFoundSet(controller.getServerName(),"k8_gestione_utenze");
	var gu = databaseManager.getFoundSet(controller.getDataSource().split('/')[1], "k8_gestione_utenze");
	if (gu.find()) {
		gu.user_servoy = user_servoy;
		gu.search();
	}

	if (gu.getSize() != 0) {
		gu.password_value = utils.stringMD5HashBase64(password_value);
		var r = gu.getRecord(gu.getSelectedIndex());
		databaseManager.saveData(r);
		user_servoy = null;
		password_value = null;
	}
}
