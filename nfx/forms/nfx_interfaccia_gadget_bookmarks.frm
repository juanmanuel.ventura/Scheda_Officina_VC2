dataSource:"db:/ferrari/k8_dummy",
items:[
{
anchors:11,
formIndex:-4,
imageMediaID:"DC52426B-B15E-47B6-A325-65212EF6567C",
location:"0,0",
mediaOptions:6,
size:"230,20",
tabSeq:-1,
typeid:7,
uuid:"02BB5C00-EF66-48CD-A7AD-0BC542593E3D"
},
{
anchors:15,
beanClassName:"javax.swing.JScrollPane",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_16\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JScrollPane\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>230<\/int> \
    <int>88<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <object class=\"javax.swing.border.MatteBorder\"> \
    <int>1<\/int> \
    <int>0<\/int> \
    <int>0<\/int> \
    <int>0<\/int> \
    <object class=\"java.awt.Color\"> \
     <int>105<\/int> \
     <int>105<\/int> \
     <int>105<\/int> \
     <int>255<\/int> \
    <\/object> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>bookScroll<\/string> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:-3,
location:"0,20",
name:"bookScroll",
size:"230,88",
typeid:12,
uuid:"166D2138-6272-490E-A664-CCF50D369213"
},
{
anchors:6,
formIndex:4,
imageMediaID:"618F9AA0-E8E0-4019-A9D8-472CC462832F",
location:"208,111",
mediaOptions:10,
size:"16,16",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"2591A8EE-B7DA-4D1D-A707-611697E953AC"
},
{
anchors:11,
formIndex:5,
imageMediaID:"06681961-53B4-4528-A0D1-33E5DDCAC16F",
location:"0,21",
mediaOptions:6,
size:"230,10",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"44AE665B-8049-4920-AE79-D3549CD374C3"
},
{
imageMediaID:"E6C052AF-2185-445F-932A-5F615B6333EC",
location:"50,2",
mediaOptions:10,
onActionMethodID:"470322DC-FBD9-4835-9FE4-6E85AA07CE37",
showClick:false,
size:"16,16",
toolTipText:"Cancella tutte le Smart Folder",
transparent:true,
typeid:7,
uuid:"76E5D674-9FEC-4946-97D5-2CE14D67793C"
},
{
imageMediaID:"81A07451-3060-4F42-84A4-136B9107003B",
location:"30,2",
mediaOptions:10,
onActionMethodID:"20168AF6-4CB3-4E66-8B92-633E8EE2981D",
showClick:false,
size:"16,16",
toolTipText:"Rimuovi",
transparent:true,
typeid:7,
uuid:"81FE6EFE-A774-41A3-B730-3358FC7F6DC3"
},
{
anchors:3,
imageMediaID:"40905D44-A323-4D4C-9CCA-CC832C1E545D",
location:"210,2",
mediaOptions:14,
onActionMethodID:"EEAC16D6-2830-4462-A450-952A95D77E06",
showClick:false,
size:"16,16",
transparent:true,
typeid:7,
uuid:"85BD35D4-718F-4355-9627-EFE67BFE4970"
},
{
imageMediaID:"CD8D63BB-A07B-44F1-8C6C-607ADB33202D",
location:"10,2",
mediaOptions:10,
onActionMethodID:"4FA03990-E58C-44DB-A506-6DA1B4A89E76",
showClick:false,
size:"16,16",
toolTipText:"Aggiungi",
transparent:true,
typeid:7,
uuid:"AD600ECB-9683-4EDF-A0A0-A8792F32E1A8"
},
{
background:"#d8eafe",
height:130,
partType:5,
typeid:19,
uuid:"CEA15DF1-4E54-4DDB-B7B1-77F3E8A55012"
},
{
beanClassName:"javax.swing.JList",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_16\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JList\"> \
  <void property=\"background\"> \
   <object class=\"java.awt.Color\"> \
    <int>216<\/int> \
    <int>234<\/int> \
    <int>254<\/int> \
    <int>255<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>bookList<\/string> \
  <\/void> \
  <void property=\"selectionMode\"> \
   <int>0<\/int> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:1,
location:"1,26",
name:"bookList",
size:"80,80",
typeid:12,
uuid:"D312EBA6-A029-421F-AA0D-901E3A299EF8"
},
{
anchors:14,
borderType:"LineBorder,1,#282828",
dataProviderID:"filter",
formIndex:3,
location:"1,110",
name:"filter1",
onActionMethodID:"14C2D817-DD10-4847-B53C-399F438CB3D2",
size:"200,19",
text:"Filter",
typeid:4,
uuid:"E25C6187-35F6-4587-9EB5-14DCCAABFA95"
}
],
name:"nfx_interfaccia_gadget_bookmarks",
navigatorID:"-1",
onLoadMethodID:"3380A870-AD7B-4A84-868C-A23AA306F57F",
paperPrintScale:100,
showInMenu:true,
size:"230,130",
styleName:"keeneight",
typeid:3,
uuid:"74E19AFA-777E-464A-BC12-A2FE1ADA2A09"