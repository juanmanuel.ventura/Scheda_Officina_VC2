/**
 *
 * @type {Array}
 * @properties={typeid:35,uuid:"A508E455-1E60-4BA2-9A73-E436BA0C393C",variableType:-4}
 */
var bookmarks = new Array();

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"68EF26BA-2503-4B97-B05D-AEA92036E845"}
 */
var filter = null;

/**
 * @properties={typeid:35,uuid:"A1AC67A4-DC95-42A0-9318-889B61BA32E0",variableType:-4}
 */
var first = true;

/**
 * @properties={typeid:24,uuid:"4FA03990-E58C-44DB-A506-6DA1B4A89E76"}
 */
function add() {
	var form = forms.nfx_interfaccia_pannello_body.mainForm;

	var id = globals.utils_getIdDataProvider(form);
	var idValue = forms[form].foundset[id];

	var name = plugins.dialogs.showInputDialog("Salva come:", "Inserire il nome da associare al segnalibro");

	if (name && getNames(null).indexOf(name) == -1) {
		var smartFolder = { name: name, form: form, id: id, idValue: idValue };
		bookmarks.push(smartFolder);
	}

	save(bookmarks);
	elements.bookList.setListData(getNames(null));
}

/**
 * @properties={typeid:24,uuid:"470322DC-FBD9-4835-9FE4-6E85AA07CE37"}
 */
function clear() {
	if (plugins.dialogs.showQuestionDialog("Cancello?", "Svoutare la lista dei segnalibri?", "Sì", "No") == "Sì") {
		bookmarks = new Array();
		save(bookmarks);
		elements.bookList.setListData(getNames(null));
	}
}

/**
 * @properties={typeid:24,uuid:"4717D48D-7733-40BF-832E-DFB499470B3B"}
 */
function dcGoTo(e) {
	if (e.getClickCount() == 2) {
		goTo();
	}
}

/**
 * @properties={typeid:24,uuid:"1F9BA658-882C-49ED-B666-59F3CB884B73"}
 * @AllowToRunInFind
 */
function filterList(list) {

	if (list == null)list = getNames(bookmarks);

	if (filter == "") {
		filter = null;
		application.updateUI();
	}

	if (filter) {
		var filtered = new Array();
		for (var i = 0; i < list.length; i++) {
			if (list[i].search(filter) != -1) {
				filtered.push(list[i]);
			}
		}
		return filtered;
	} else {
		return list;
	}
}

/**
 * @properties={typeid:24,uuid:"14C2D817-DD10-4847-B53C-399F438CB3D2"}
 */
function filterWrap() {
	elements.bookList.setListData(filterList(null));
}

/**
 * @properties={typeid:24,uuid:"09213B53-5818-46F4-9503-E07ADE5BB050"}
 */
function getNames(list) {

	if (list == null)list = bookmarks;

	var names = new Array();
	for (var i = 0; i < list.length; i++) {
		names.push(list[i].name);
	}

	return names;
}

/**
 *
 * @properties={typeid:24,uuid:"CBA5F9FE-E3B6-436E-8F4C-269DC5D9AD38"}
 */
function getObject(list, toFind) {

	var daRit = null;

	if (list && toFind) {
		for (var i = 0; i < list.length; i++) {
			if (list[i].name == toFind) {
				daRit = list[i];
				return daRit;
			}
		}
	} else {
		throw "You've forgotten one or more parameters!";
	}
	return daRit;
}

/**
 * @properties={typeid:24,uuid:"EEAC16D6-2830-4462-A450-952A95D77E06"}
 */
function goTo() {
	globals.nfx_setBusy(true);
	var toFind = elements.bookList.getSelectedValue();

	if (toFind) {

		var bookmark = getObject(bookmarks, toFind);

		if(bookmark.idValue == null){
			globals.nfx_goTo(bookmark.form);
		}
		else{
		globals.nfx_goToProgramAndRecord(bookmark.form, bookmark.idValue, bookmark.id);
		}

	}
	globals.nfx_setBusy(false);
}

/**
 * @properties={typeid:24,uuid:"3380A870-AD7B-4A84-868C-A23AA306F57F"}
 */
function onLoad() {
	var query = "select bookmarks from k8_gestione_utenze where user_servoy = ?";
	//	JStaffa
	//	var bm = databaseManager.getDataSetByQuery(controller.getServerName(),query,[globals.nfx_user],1);
	var bm = databaseManager.getDataSetByQuery(controller.getDataSource().split('/')[1], query, [globals.nfx_user], 1);

	var vp = elements.bookScroll.getViewport();
	
	//SAuc
	//vp.add(elements.bookList);
	vp.add(java.awt.Component(elements.bookList));

	if (first) {
		//SAuc
		//elements.bookList.addMouseListener(dcGoTo);
		elements.bookList.addMouseListener(java.awt.event.MouseListener(dcGoTo));
		first = null;
	}
	bookmarks = (bm.getValue(1, 1)) ? forms.nfx_json_serializer.parse(bm.getValue(1, 1), null) : bookmarks;
	elements.bookList.setListData(getNames(null));
}

/**
 * @properties={typeid:24,uuid:"E2E3381A-B68D-400F-BB3B-96F06667BF0F"}
 */
function remove(toRemove) {

	if (toRemove) {
		var removed = getObject(bookmarks, toRemove);
		//FS	
		bookmarks.splice(bookmarks.indexOf(removed), 1);
		save(bookmarks);
		elements.bookList.setListData(filterList(null))
	} else {
		throw "You've forgotten a parameter!";
	}
}

/**
 * @properties={typeid:24,uuid:"20168AF6-4CB3-4E66-8B92-633E8EE2981D"}
 */
function removeWrap() {

	var toRemove = elements.bookList.getSelectedValue();

	if (toRemove) {
		remove(toRemove);
	}
}

/**
 * @properties={typeid:24,uuid:"4DD7F155-E785-479F-872D-C31FFB2AA190"}
 * @AllowToRunInFind
 */
function save(toSave) {

	if (toSave) {
		var users = databaseManager.getFoundSet(databaseManager.getDataSourceServerName(controller.getDataSource()), "k8_gestione_utenze");

		if (users.find()) {
			users.user_servoy = globals.nfx_user;
			users.search();
		}

		users.bookmarks = forms.nfx_json_serializer.stringify(toSave, null, null);
	} else {
		throw "You've forgotten a parameter!";
	}
}
