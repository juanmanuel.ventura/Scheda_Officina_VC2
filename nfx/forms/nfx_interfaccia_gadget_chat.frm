dataSource:"db:/ferrari/k8_dummy",
items:[
{
anchors:13,
beanClassName:"javax.swing.JScrollPane",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_16\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JScrollPane\"> \
  <void property=\"background\"> \
   <object class=\"java.awt.Color\"> \
    <int>216<\/int> \
    <int>234<\/int> \
    <int>254<\/int> \
    <int>255<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>101<\/int> \
    <int>110<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <object class=\"javax.swing.border.MatteBorder\"> \
    <int>1<\/int> \
    <int>0<\/int> \
    <int>0<\/int> \
    <int>1<\/int> \
    <object class=\"java.awt.Color\"> \
     <int>105<\/int> \
     <int>105<\/int> \
     <int>105<\/int> \
     <int>255<\/int> \
    <\/object> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>buddiesScroll<\/string> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"0,20",
name:"buddiesScroll",
size:"101,110",
typeid:12,
uuid:"2C830F29-2BD3-4497-BAF7-70978C6AC7BB"
},
{
anchors:11,
foreground:"#000000",
formIndex:3,
horizontalAlignment:0,
location:"20,0",
mediaOptions:14,
name:"chatWith",
size:"190,20",
styleClass:"form",
tabSeq:-1,
text:"Chat",
transparent:true,
typeid:7,
uuid:"334F9011-A9F1-4AB6-B1FB-A9FBB3CD7F51"
},
{
beanClassName:"javax.swing.JList",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_16\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JList\"> \
  <void property=\"background\"> \
   <object class=\"java.awt.Color\"> \
    <int>216<\/int> \
    <int>234<\/int> \
    <int>254<\/int> \
    <int>255<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"font\"> \
   <object class=\"java.awt.Font\"> \
    <string>Arial<\/string> \
    <int>0<\/int> \
    <int>10<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>60<\/int> \
    <int>60<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <object class=\"javax.swing.border.EmptyBorder\"> \
    <object class=\"java.awt.Insets\"> \
     <int>0<\/int> \
     <int>0<\/int> \
     <int>0<\/int> \
     <int>0<\/int> \
    <\/object> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>buddiesList<\/string> \
  <\/void> \
  <void property=\"selectionMode\"> \
   <int>0<\/int> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:2,
location:"0,21",
name:"buddiesList",
size:"101,109",
typeid:12,
uuid:"4E615599-306E-4B3D-AF6C-74F7FFC76A5E"
},
{
anchors:11,
imageMediaID:"DC52426B-B15E-47B6-A325-65212EF6567C",
location:"0,0",
mediaOptions:6,
size:"230,20",
tabSeq:-1,
typeid:7,
uuid:"64D8FDE1-2A80-4C8F-AC0E-216686D596FF"
},
{
anchors:3,
formIndex:1,
imageMediaID:"DA4A0092-D9C0-4E8E-ADBF-763547D9A9C1",
location:"210,0",
mediaOptions:10,
name:"logout",
onActionMethodID:"63F20CA7-053D-45A6-9B0A-AB3ADB3EAFA0",
showClick:false,
size:"20,20",
toolTipText:"Logout",
transparent:true,
typeid:7,
uuid:"689C50E6-5F81-46AD-82F5-D063E3321553"
},
{
anchors:14,
background:"#ffffff",
borderType:"MatteBorder,1,0,0,0,#696969",
dataProviderID:"message",
formIndex:1,
location:"99,110",
name:"message1",
onActionMethodID:"913EB09F-B243-441E-8B03-BD75E25FD527",
onDataChangeMethodID:"-1",
size:"131,20",
text:"Message",
typeid:4,
uuid:"8A13455C-E5D5-42FE-99CE-A97B3739656B"
},
{
anchors:11,
formIndex:4,
imageMediaID:"06681961-53B4-4528-A0D1-33E5DDCAC16F",
location:"0,21",
mediaOptions:6,
size:"230,10",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"A17A4F55-5841-4127-99F1-14F3302DDD2B"
},
{
anchors:3,
formIndex:2,
imageMediaID:"5C899C02-8B5E-429E-B0AD-298889285E3C",
location:"210,0",
mediaOptions:10,
name:"login",
onActionMethodID:"40C632AB-7002-48EE-9802-79633FBD235A",
showClick:false,
size:"20,20",
toolTipText:"Login",
transparent:true,
typeid:7,
uuid:"BF3E7BA4-CF80-46E7-BFC5-5575B464ABBD"
},
{
height:130,
partType:5,
typeid:19,
uuid:"CDE3039B-964F-4889-9DA4-3FC83642086F"
},
{
anchors:15,
background:"#ebf4fe",
borderType:"MatteBorder,1,0,0,0,#696969",
dataProviderID:"board",
displayType:8,
editable:false,
fontType:"Arial,0,10",
formIndex:3,
location:"99,20",
name:"board1",
size:"131,90",
text:"Board",
typeid:4,
uuid:"CED9FB5F-F00C-46C5-823B-E41DF0FB2793"
}
],
name:"nfx_interfaccia_gadget_chat",
navigatorID:"-1",
onHideMethodID:"-1",
onLoadMethodID:"193CA144-4535-4140-840B-5B21E5846520",
onShowMethodID:"ACB5A123-A877-475D-BA3F-852DD9A52B66",
onUnLoadMethodID:"-1",
paperPrintScale:100,
showInMenu:true,
size:"230,130",
styleName:"keeneight",
transparent:true,
typeid:3,
uuid:"5BBE2C3B-FC3A-408B-BB44-6B4F1B2773A2"