/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A05E5BD9-912B-4E3B-9877-F3D49F8C36C6"}
 */
var board = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"36E9B2D1-ACC1-43CE-A92F-C9F93FAD291D"}
 */
var chatWith = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"BC76D565-7C01-4110-9CC1-A70A380C9A2D"}
 */
var ipFilter = "10.100.";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B4C9DD18-5BFA-4EE1-BF96-8B46EC571A57"}
 */
var ipFilterEnabled = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"08B961A5-560A-4A25-92AB-AE9F3398D4EF"}
 */
var logged = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"CBA9841E-72DF-42B4-AC11-0A5EB45D3AE5"}
 */
var message = "";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"759F5E92-43A4-4393-A042-5FC58BBA9E79",variableType:4}
 */
var timeIdle = 900000;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"32318481-3FA1-4E18-9159-BF69F5B8EA32",variableType:4}
 */
var timeOut = 1800000;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D551D4B5-08B5-4B7E-8347-CEF5AC846B81"}
 */
var updateListGO = null;

/**
 * @properties={typeid:35,uuid:"6D1A8ADD-D680-41CE-9671-1E97C42073A6",variableType:-4}
 */
var users = new Array();

/**
 * @properties={typeid:35,uuid:"8CE4E666-8920-4BD0-B54D-CE874B30AA6A",variableType:-4}
 */
var index = null;

/**
 * @properties={typeid:35,uuid:"0B86399F-1060-4458-A101-C986DD6AF2BF",variableType:-4}
 */
var cellHasFocus = null;

/**
 * @properties={typeid:24,uuid:"193CA144-4535-4140-840B-5B21E5846520"}
 */
function chatOnLoad() {
//	application.output("OnLoadChat comment");
//	var vp = elements.buddiesScroll.getViewport();
//	//SAuc
//	//vp.add(elements.buddiesList);
//	vp.add(java.awt.Component(elements.buddiesList));
//
//	var lcrImpl = { getListCellRendererComponent: getLabel };
//	elements.buddiesList.setCellRenderer(new Packages.javax.swing.ListCellRenderer(lcrImpl));
//	elements.buddiesList.addMouseListener(openChatWith());
//
//	var exceptions = globals.nfx_developers.concat(["rfedeli"]);
//	if (exceptions.indexOf(globals.nfx_user) == -1) {
//		login();
//	} else {
//		elements.login.visible = true;
//		elements.logout.visible = false;
//	}
}

/**
 *
 * @properties={typeid:24,uuid:"E90FABF6-4D44-4568-85EE-DAEC2510FC47"}
 */
function getIpFromName(name) {
//	application.output("GetIpFromName chat comment");
	//	JStaffa
	//	var dataSet = databaseManager.getDataSetByQuery(controller.getServerName(),"select last_ip from k8_gestione_utenze where user_servoy = ?",[name],-1);
//	var dataSet = databaseManager.getDataSetByQuery(controller.getDataSource().split('/')[1], "select last_ip from k8_gestione_utenze where user_servoy = ?", [name], -1);
//	var r = dataSet.getValue(1, 1)
//	return r;
}

/**
 *
 * @properties={typeid:24,uuid:"1EE19473-8E44-42A2-A308-94EC7DED428B"}
 */
function getLabel(lista, value, indexa, isSelected, cellHasFocusa) {
//	application.output("GetLabelChat comment");
//
//	var url = null;
//
//	url = new java.net.URL("media:///black_man.png");
//	var youIcon = new Packages.javax.swing.ImageIcon(url);
//	url = new java.net.URL("media:///chat.png");
//	var unreadIcon = new Packages.javax.swing.ImageIcon(url);
//	url = new java.net.URL("media:///green_dot.png");
//	var activeIcon = new Packages.javax.swing.ImageIcon(url);
//	url = new java.net.URL("media:///yellow_dot.png");
//	var idleIcon = new Packages.javax.swing.ImageIcon(url);
//
//	var icon = null;
//	if (value == globals.nfx_user) {
//		icon = youIcon;
//	} else if (hasUnread(value)) {
//		icon = unreadIcon
//	} else {
//		icon = (isActiveVal(value)) ? activeIcon : idleIcon;
//	}
//
//	var label = new Packages.javax.swing.JLabel(value, icon, 2);
//	if (isSelected && value != globals.nfx_user) {
//		label.setForeground(java.awt.SystemColor.textHighlightText);
//		label.setBackground(java.awt.SystemColor.textHighlight);
//		label.setOpaque(true);
//	} else {
//		label.setForeground(java.awt.SystemColor.textText);
//		label.setBackground(java.awt.SystemColor.text);
//		label.setOpaque(false);
//	}
//
//	return label;
}

/**
 *
 * @properties={typeid:24,uuid:"1668F778-70B2-4B6A-840C-2542203E98F2"}
 */
function getNameFromIp(ip) {
//	application.output("GetNameFromIp chat comment");
	//	JStaffa
	//	var dataSet = databaseManager.getDataSetByQuery(controller.getServerName(),"select user_servoy from k8_gestione_utenze where last_ip = ?",[ip],-1);
//	var dataSet = databaseManager.getDataSetByQuery(controller.getDataSource().split('/')[1], "select user_servoy from k8_gestione_utenze where last_ip = ?", [ip], -1);
//	return dataSet.getValue(1, 1);
}

/**
 *
 * @properties={typeid:24,uuid:"F4B30059-DB36-410D-96CB-75A369C697F7"}
 */
function getNames(lista) {
//	application.output("getName chat comment");
//
//	if (lista == null)lista = users;
//
//	var names = new Array();
//
//	for (var i = 0; i < lista.length; i++) {
//		names.push(lista[i].user);
//	}
//
//	return names;
}

/**
 *
 * @properties={typeid:24,uuid:"A1E7E4D5-D281-477C-8EAD-7801992142BF"}
 */
function getObject(value) {
//	application.output("getObjectchat comment");
//	for (var i = 0; i < users.length; i++) {
//		if (users[i].user == value) {
//			return users[i];
//		}
//	}
//	return null;
}

/**
 *
 * @properties={typeid:24,uuid:"C5038DF8-942E-4A0E-88DB-8C4316A8215A"}
 */
function hasUnread(value) {
//	application.output("hasunreadchat comment");
//	var obj = getObject(value);
//	var result = (obj.unread) ? true : null;
//	return result;
}

/**
 *
 * @properties={typeid:24,uuid:"E5191BF7-FF8D-4450-9F65-313E69A0C149"}
 * @AllowToRunInFind
 */
function ipIsValid(ip) {
//	application.output("ipisvalid chat comment");
//	if (ip) {
//		if (ipFilterEnabled) {
//			return (ip.search(ipFilter) != -1) ? true : null;
//		} else {
//			return true;
//		}
//	} else {
//		return null;
//	}
}

/**
 *
 * @properties={typeid:24,uuid:"092B7DDB-7CA5-4A9A-8380-1986BCB94312"}
 */
function isActive(value) {
//	application.output("isActivecomment chat");
//	var obj = getObject(value);
//	var result = (obj.status == "active") ? true : null;
//	return result;
}

/**
 * TODO generated, please specify type and doc for the params
 * @param value
 *
 * @properties={typeid:24,uuid:"E4020FBE-B5BF-4C30-A64D-9F801FCEB9FD"}
 */
function isActiveVal(value) {
//	application.output("isActiveVal chat comment");

//	var obj = getObject(value);
//	var result = (obj.status == "active") ? true : null;
//	return result;
}

/**
 *
 * @properties={typeid:24,uuid:"40C632AB-7002-48EE-9802-79633FBD235A"}
 * @AllowToRunInFind
 */
function login() {
//	application.output("login chat comment");
//	globals.nfx_setUserActive();
//	if (ipIsValid(getIpFromName(globals.nfx_user))) {
//		var gestioneUtenze = forms.nfx_interfaccia_gestione_utenze;
//		gestioneUtenze.controller.loadAllRecords();
//		if (gestioneUtenze.controller.find()) {
//			gestioneUtenze.user_servoy = globals.nfx_user;
//			gestioneUtenze.controller.search();
//		}
//		gestioneUtenze.invisible = null;
//		databaseManager.saveData(gestioneUtenze.foundset.getRecord(1));
//		updateListGO = true;
//		updateListTH = new java.lang.Thread(thRun);
//		updateListTH.start();
//		logged = true;
//
//		//parte grafica
//		elements.login.visible = false;
//		elements.logout.visible = true;
//	} else {
//		//plugins.dialogs.showErrorDialog("Errore di rete", "Non è possibile utilizzare la chat dalla tua locazione", "Ok");
//	}
}

/**
 *
 * @properties={typeid:24,uuid:"63F20CA7-053D-45A6-9B0A-AB3ADB3EAFA0"}
 * @AllowToRunInFind
 */
function logout() {
//	application.output("logout chat comment");
//	globals.nfx_setUserInactive();
//	var gestioneUtenze = forms.nfx_interfaccia_gestione_utenze;
//	gestioneUtenze.controller.loadAllRecords();
//	if (gestioneUtenze.controller.find()) {
//		gestioneUtenze.user_servoy = globals.nfx_user;
//		gestioneUtenze.controller.search();
//	}
//	gestioneUtenze.invisible = 1;
//	databaseManager.saveData(gestioneUtenze.foundset.getRecord(1));
//	updateListGO = null;
//	updateListTH.interrupt();
//	application.sleep(1000);
//	elements.buddiesList.setListData(new Array());
//	chatWith = null;
//	elements.chatWith.text = "Chat con: ";
//	logged = null;
//
//	//parte grafica
//	elements.login.visible = true;
//	elements.logout.visible = false;
}

/**
 *
 * @properties={typeid:24,uuid:"B2A0BB57-6C7C-42ED-994F-8B347B00F760"}
 */
function openChatWith() {
//	application.output("openchat with comment");
//	var value = elements.buddiesList.getSelectedValue();
//	if (value && value != chatWith) {
//		if (value != globals.nfx_user) {
//			var cw = getObject(value);
//			chatWith = cw.user;
//			elements.chatWith.text = "Chat con: " + chatWith;
//			setBoardContent(cw.chat);
//			cw.unread = null;
//		} else {
//			elements.buddiesList.setSelectedValue(chatWith, true);
//		}
//	}
}

/**
 *
 * @properties={typeid:24,uuid:"913EB09F-B243-441E-8B03-BD75E25FD527"}
 */
function send() {
//	application.output("send chat comment");
//	if (chatWith) {
//		var msgClean = message.replace(/<([^&<>]+)>/gi, "");
//		var cw = getObject(chatWith);
//		var ip = getIpFromName(chatWith);
//		if (ip && msgClean != "") {
//			//preparo il messaggio da spedire
//			var packet = plugins.udp.createNewPacket();
//			var msg = getObject(globals.nfx_user).name + "<br/>" + msgClean;
//			packet.writeUTF(msg);
//			plugins.udp.sendPacket(ip, packet, globals.nfx_udpPort);
//			//preparo il messaggio da visualizzare;
//			var html = "<p style='margin:0;padding:2px;text-align:right;color:#636363;background-color:#DEE3ED'>" + msg + "</p>";
//			cw.chat += html;
//		} else if (!ip) {
//			cw.chat += "<p style='margin:0;padding:2px;text-align:left;color:#FF0000;'>** Il messaggio non e' stato inviato l'utente non e' piu' attivo **</p>";
//		}
//		message = "";
//		setBoardContent(cw.chat);
//	}
}

/**
 *
 * @properties={typeid:24,uuid:"24F2686D-5EC2-4FCA-877C-4572A20103EF"}
 */
function setBoardContent(content) {
//	application.output("setBoardcontent chat comment");
//	board = "<html>" + content + "</html>";
//	elements.board1.caretPosition = board.length;
}

/**
 *
 * @properties={typeid:24,uuid:"5158122C-4922-40A1-BF32-08F1DD4E2922"}
 */
function thRun() {
//	application.output("threadRun chat comment");
//	while (updateListGO) {
//		updateList();
//		//SAuc
//		updateListTH.sleep(499);
//
//		//java.lang.Thread.sleep(499);
//
//	}
}

/**
 *
 * @properties={typeid:24,uuid:"41F90858-0F35-43C2-8CA4-57FEDE9CAA8D"}
 */
function updateList() {
//	application.output("updateList chat comment");
//	var active = new Array();
//	var idle = new Array();
//	var now = new Date();
//
//	//	JStaffa
//	//	var query = "select user_servoy,nome,cognome,last_ip,last_activity,invisible from " + forms.nfx_interfaccia_gestione_utenze.controller.getTableName() + " order by user_servoy asc";
//	//	var ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,null,-1);
//	var query = "select user_servoy,nome,cognome,last_ip,last_activity,invisible from " + forms.nfx_interfaccia_gestione_utenze.controller.getDataSource().split('/')[2] + " order by user_servoy asc";
//	var ds = databaseManager.getDataSetByQuery(controller.getDataSource().split('/')[1], query, null, -1);
//	var lastIndex = ds.getMaxRowIndex();
//	for (var i = 1; i <= lastIndex; i++) {
//		var gestioneUtenze = { user_servoy: ds.getValue(i, 1), nome: ds.getValue(i, 2), cognome: ds.getValue(i, 3), last_ip: ds.getValue(i, 4), last_activity: ds.getValue(i, 5), invisible: ds.getValue(i, 6) };
//
//		if (!gestioneUtenze.invisible && ipIsValid(gestioneUtenze.last_ip) && now - gestioneUtenze.last_activity < timeOut) {
//			var user = null;
//			if (getObject(gestioneUtenze.user_servoy)) {
//				user = getObject(gestioneUtenze.user_servoy);
//			} else {
//				user = { user: gestioneUtenze.user_servoy, name: gestioneUtenze.nome + " " + gestioneUtenze.cognome, status: null, unread: null, chat: "" };
//			}
//			if (now - gestioneUtenze.last_activity < timeIdle) {
//				user.status = "active";
//				active.push(user);
//			} else {
//				user.status = "idle"
//				idle.push(user);
//			}
//		}
//	}
//	users = active.concat(idle);
//	var names = getNames(null);
//	elements.buddiesList.setListData(names);
//	if (chatWith && !getObject(chatWith)) {
//		chatWith = null;
//		elements.chatWith.text = "Chat con: ";
//		setBoardContent("<html><p style='margin:0;padding:2px;text-align:left;color:#FF0000;'>** L'utente non e' piu' attivo **</p></html>");
//	} else {
//		elements.buddiesList.setSelectedValue(chatWith, true);
//	}
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"ACB5A123-A877-475D-BA3F-852DD9A52B66"}
 */
function onShow(firstShow, event) {
//	application.output("onShow chat comment");
//	login();
}
