dataSource:"db:/ferrari/k8_dummy",
items:[
{
anchors:15,
beanClassName:"javax.swing.JScrollPane",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_16\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JScrollPane\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>230<\/int> \
    <int>88<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <object class=\"javax.swing.border.MatteBorder\"> \
    <int>1<\/int> \
    <int>0<\/int> \
    <int>0<\/int> \
    <int>0<\/int> \
    <object class=\"java.awt.Color\"> \
     <int>105<\/int> \
     <int>105<\/int> \
     <int>105<\/int> \
     <int>255<\/int> \
    <\/object> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>smartScroll<\/string> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"0,20",
name:"smartScroll",
size:"230,88",
typeid:12,
uuid:"089D99A8-7966-4F03-A1C9-2115AE0F006C"
},
{
anchors:14,
borderType:"LineBorder,1,#282828",
dataProviderID:"filter",
location:"1,110",
name:"filter1",
onActionMethodID:"3D5D38BD-7C35-424D-97C0-25D92741428D",
size:"200,19",
text:"Filter",
typeid:4,
uuid:"555B9987-2C50-4A5B-9E55-D75B443CE0EF"
},
{
anchors:11,
imageMediaID:"DC52426B-B15E-47B6-A325-65212EF6567C",
location:"0,0",
mediaOptions:6,
size:"230,20",
tabSeq:-1,
typeid:7,
uuid:"5D04D3EB-B831-4883-B80E-44C2F7F3830B"
},
{
anchors:6,
imageMediaID:"618F9AA0-E8E0-4019-A9D8-472CC462832F",
location:"208,111",
mediaOptions:10,
size:"16,16",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"6A9B92C3-C64B-44E4-870A-ABDE269D75E1"
},
{
background:"#d8eafe",
height:130,
partType:5,
typeid:19,
uuid:"6C4833C2-D11A-48CC-9861-5E1421A68DCC"
},
{
formIndex:2,
imageMediaID:"E6C052AF-2185-445F-932A-5F615B6333EC",
location:"50,2",
mediaOptions:10,
onActionMethodID:"5D53F075-FB2B-4B39-8007-7ABDA6DE4DB7",
showClick:false,
size:"16,16",
toolTipText:"Cancella tutte le Smart Folder",
transparent:true,
typeid:7,
uuid:"789FB635-AEA3-4F7A-82EA-FE7F76DB6B48"
},
{
formIndex:3,
imageMediaID:"81A07451-3060-4F42-84A4-136B9107003B",
location:"30,2",
mediaOptions:10,
onActionMethodID:"ED477768-E154-4A32-ABDB-9078BC07801F",
showClick:false,
size:"16,16",
toolTipText:"Rimuovi",
transparent:true,
typeid:7,
uuid:"89B73DC5-E07C-42A4-8D7C-1CABC66F3624"
},
{
anchors:3,
formIndex:4,
imageMediaID:"40905D44-A323-4D4C-9CCA-CC832C1E545D",
location:"210,2",
mediaOptions:14,
onActionMethodID:"12689326-BACC-4B07-A7E3-00B09AC9F9BB",
showClick:false,
size:"16,16",
transparent:true,
typeid:7,
uuid:"AE3A96DF-4D82-47C8-BB21-0D452D906734"
},
{
anchors:11,
formIndex:2,
imageMediaID:"06681961-53B4-4528-A0D1-33E5DDCAC16F",
location:"0,21",
mediaOptions:6,
size:"230,10",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"C150052C-BE1C-4E3C-ACCB-4C73E7C4BB10"
},
{
formIndex:1,
imageMediaID:"CD8D63BB-A07B-44F1-8C6C-607ADB33202D",
location:"10,2",
mediaOptions:10,
onActionMethodID:"7BF96F07-D1D9-48BF-A38D-1535DBE021C4",
showClick:false,
size:"16,16",
toolTipText:"Aggiungi",
transparent:true,
typeid:7,
uuid:"C2B8D8D4-61DD-4C38-A173-A45DA89641A3"
},
{
beanClassName:"javax.swing.JList",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_16\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JList\"> \
  <void property=\"background\"> \
   <object class=\"java.awt.Color\"> \
    <int>216<\/int> \
    <int>234<\/int> \
    <int>254<\/int> \
    <int>255<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>smartList<\/string> \
  <\/void> \
  <void property=\"selectionMode\"> \
   <int>0<\/int> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:1,
location:"1,23",
name:"smartList",
size:"80,80",
typeid:12,
uuid:"D49DACA7-1A23-4DEB-A664-37E91006BBAC"
}
],
name:"nfx_interfaccia_gadget_smartfolder",
navigatorID:"-1",
onLoadMethodID:"F5F5D65E-9499-4797-AF73-0AA1A1C75970",
paperPrintScale:100,
showInMenu:true,
size:"230,130",
styleName:"keeneight",
typeid:3,
uuid:"01FECE26-D13D-4772-AFB5-8D575B6C3C25"