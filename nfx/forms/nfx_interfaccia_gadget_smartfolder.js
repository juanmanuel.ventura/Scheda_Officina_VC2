/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"023F8801-3C88-48D6-BB42-FA73939FCAAA"}
 */
var filter = null;

/**
 * @properties={typeid:35,uuid:"A6DAFC47-1DC0-459B-80BC-C947ADCE2CEB",variableType:-4}
 */
var first = true;

/**
 /** @type {Array}
 * @properties={typeid:35,uuid:"D6521244-26F4-4DBB-8CA7-7D1ABA69A807",variableType:-4}
 */
var smartFolders = new Array();

/**
 *
 * @properties={typeid:24,uuid:"7BF96F07-D1D9-48BF-A38D-1535DBE021C4"}
 */
function add() {
	var form = forms.nfx_interfaccia_pannello_body.mainForm;

	var query = databaseManager.getSQL(forms[form].foundset);
	var args = databaseManager.getSQLParameters(forms[form].foundset);
	var name = plugins.dialogs.showInputDialog("Salva come:", "Inserire il nome da associare alla Smart Folder");

	if (name && getNames(smartFolders).indexOf(name) == -1) {
		var smartFolder = { name: name, form: form, query: query, args: args };
		smartFolders.push(smartFolder);
	}

	save(smartFolders);
	elements.smartList.setListData(getNames(null));
}

/**
 *
 * @properties={typeid:24,uuid:"5D53F075-FB2B-4B39-8007-7ABDA6DE4DB7"}
 */
function clear() {
	if (plugins.dialogs.showQuestionDialog("Cancello?", "Svoutare la lista delle Smart Folder?", "Sì", "No") == "Sì") {
		smartFolders = new Array();
		save(smartFolders);
		elements.smartList.setListData(getNames(null));
	}
}

/**
 *
 * @properties={typeid:24,uuid:"514E5F53-81F7-40CB-B567-86E63D50F014"}
 */
function dcGoTo(e) {

	if (e.getClickCount() == 2) {
		goTo();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"9D6DF078-0755-4363-8433-4DECDBC8B21F"}
 * @AllowToRunInFind
 */
function filterList(list) {

	if (filter == "") {
		filter = null;
		application.updateUI();
	}

	if (list == null)list = getNames(smartFolders);

	if (filter) {
		var filtered = new Array();
		for (var i = 0; i < list.length; i++) {
			if (list[i].search(filter) != -1) {
				filtered.push(list[i]);
			}
		}
		return filtered;
	} else {
		return list;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"3D5D38BD-7C35-424D-97C0-25D92741428D"}
 */
function filterWrap() {
	elements.smartList.setListData(filterList(null));
}

/**
 *
 * @properties={typeid:24,uuid:"09361DC9-3F6C-4F14-95EF-5E10A58A7156"}
 */
function getNames(list) {

	if (list == null)list = smartFolders;

	var names = new Array();
	for (var i = 0; i < list.length; i++) {
		names.push(list[i].name);
	}

	return names;
}

/**
 *
 * @properties={typeid:24,uuid:"1A008C09-3CE1-4660-9D25-33BEC32516CC"}
 */
function getObject(list, toFind) {

	if (list && toFind) {
		for (var i = 0; i < list.length; i++) {
			if (list[i].name == toFind) {
				return list[i];
			}
		}
	} else {
		throw "You've forgotten one or more parameters!";
	}

	return null;

}

/**
 *
 * @properties={typeid:24,uuid:"12689326-BACC-4B07-A7E3-00B09AC9F9BB"}
 */
function goTo() {
	globals.nfx_setBusy(true);
	var toFind = elements.smartList.getSelectedValue();

	if (toFind) {
		var smartFolder = getObject(smartFolders, toFind);

		globals.nfx_goTo(smartFolder.form);
		forms[smartFolder.form].controller.loadRecords(smartFolder.query, smartFolder.args);
	}
	globals.nfx_setBusy(false);
}

/**
 *
 * @properties={typeid:24,uuid:"F5F5D65E-9499-4797-AF73-0AA1A1C75970"}
 */
function onLoad() {
	var query = "select smart_folder from k8_gestione_utenze where user_servoy = ?";
	//	JStaffa
	//	var sf = databaseManager.getDataSetByQuery(controller.getServerName(),query,[globals.nfx_user],1);
	var sf = databaseManager.getDataSetByQuery(controller.getDataSource().split('/')[1], query, [globals.nfx_user], 1);

	var vp = elements.smartScroll.getViewport();
	
	//SAuc
	//vp.add(elements.smartList);
	vp.add(java.awt.Component(elements.smartList));
	
	if (first) {
		//SAuc
		//elements.smartList.addMouseListener(dcGoTo);
		elements.smartList.addMouseListener(java.awt.event.MouseListener(dcGoTo));

		first = null;
	}
	smartFolders = (sf.getValue(1, 1)) ? forms.nfx_json_serializer.parse(sf.getValue(1, 1), null) : smartFolders;
	rebuildSmartFolders();
	elements.smartList.setListData(getNames(null));

}

/**
 * @properties={typeid:24,uuid:"6190AEC4-8BDB-429F-82E4-9140FA142326"}
 */
function rebuildSmartFolders() {
	for (var i = 0; i < smartFolders.length; i++) {
		var args = smartFolders[i].args;
		var backup = new Array();
		for (var j in args) {
			backup.push(args[j]);
		}
		smartFolders[i].args = backup;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"0E8F1B29-AEED-44E1-B7DA-674C712969D0"}
 */
function remove(toRemove) {

	if (toRemove) {
		var removed = getObject(smartFolders, toRemove);
		smartFolders.splice(smartFolders.indexOf(removed), 1);
		save(smartFolders);
		elements.smartList.setListData(filterList(null))
	} else {
		throw "You've forgotten a parameter!";
	}
}

/**
 *
 * @properties={typeid:24,uuid:"ED477768-E154-4A32-ABDB-9078BC07801F"}
 */
function removeWrap() {
	var toRemove = elements.smartList.getSelectedValue();

	if (toRemove) {
		remove(toRemove);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"170383B4-8FFC-49BE-8160-F5256BF95B52"}
 * @AllowToRunInFind
 */
function save(toSave) {

	if (toSave) {
		var users = databaseManager.getFoundSet(databaseManager.getDataSourceServerName(controller.getDataSource()), "k8_gestione_utenze");

		if (users.find()) {
			users.user_servoy = globals.nfx_user;
			users.search();
		}

		users.smart_folder = forms.nfx_json_serializer.stringify(toSave, null, null);
	} else {
		throw "You've forgotten a parameter!";
	}
}
