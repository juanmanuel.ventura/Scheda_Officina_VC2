/**
 * @properties={typeid:24,uuid:"016DD66C-251F-4AD8-AE7E-E89A71FE3258"}
 */
function load(form) {
	if (!form) throw "missing paramenter";
	var query = "select saved_value from nfx_values where form_name = ? and user_servoy = ?";
	var args = [form, globals.nfx_user];
	//	JStaffa
	//	var value = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,1).getValue(1,1);
	var value = databaseManager.getDataSetByQuery(controller.getDataSource().split('/')[1], query, args, 1).getValue(1, 1);
	return (value) ? forms.nfx_json_serializer.parse(value, null) : null;
}

/**
 * @properties={typeid:24,uuid:"06114DED-2C94-42DC-9F88-2A0296BD892E"}
 */
function save(value, form) {
	if (!value || !form) throw "missing paramenter";
	(alreadySaved(form)) ? updateRecord(value, form) : insertRecord(value, form);
}

/**
 * @properties={typeid:24,uuid:"8284061D-AAEB-41E3-BBA7-01A541F2E1A7"}
 */
function alreadySaved(form) {
	var query = "select count(*) from nfx_values where form_name = ? and user_servoy = ?";
	var args = [form, globals.nfx_user];
	//	JStaffa
	//	var ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,1);
	var ds = databaseManager.getDataSetByQuery(controller.getDataSource().split('/')[1], query, args, 1);
	return (ds.getValue(1, 1) > 0) ? true : null;
}

/**
 * @properties={typeid:24,uuid:"C250F210-2AC7-4B36-9D98-21706EABFCB8"}
 */
function insertRecord(value, form) {
	var query = "insert into nfx_values (saved_value,form_name,user_servoy) values (?,?,?)";
	var args = [forms.nfx_json_serializer.stringify(value, null, null), form, globals.nfx_user];
	//	JStaffa
	//	plugins.rawSQL.executeSQL(controller.getServerName(), controller.getTableName(), query,args);
	plugins.rawSQL.executeSQL(controller.getDataSource().split('/')[1], controller.getDataSource().split('/')[2], query, args);
}

/**
 * @properties={typeid:24,uuid:"C1757053-E3B4-4CF0-A464-AF8069F080B1"}
 */
function updateRecord(value, form) {
	var query = "update nfx_values set saved_value = ? where form_name = ? and user_servoy = ?";
	var args = [forms.nfx_json_serializer.stringify(value, null, null), form, globals.nfx_user];
	//	JStaffa
	//	plugins.rawSQL.executeSQL(controller.getServerName(), controller.getTableName(), query,args);
	plugins.rawSQL.executeSQL(controller.getDataSource().split('/')[1], controller.getDataSource().split('/')[2], query, args);
}
