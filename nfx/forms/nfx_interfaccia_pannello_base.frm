dataSource:"db:/ferrari/k8_dummy",
items:[
{
anchors:15,
items:[
{
containsFormID:"24148957-27EE-471B-86B5-F40CAF89D242",
location:"520,450",
text:"nfx_interfaccia_pannello_middle",
typeid:15,
uuid:"8FA7C395-7579-4727-A80D-FA616C3407A4"
}
],
location:"490,360",
name:"content",
printable:false,
size:"532,240",
typeid:16,
uuid:"186A9C1D-FECA-4B14-A151-FAFFB309BC32"
},
{
anchors:15,
beanClassName:"javax.swing.JSplitPane",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_16\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JSplitPane\"> \
  <int>1<\/int> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>1024<\/int> \
    <int>570<\/int> \
   <\/object> \
  <\/void> \
  <void method=\"add\"> \
   <object id=\"JButton0\" class=\"javax.swing.JButton\"> \
    <string>tasto destro<\/string> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <object class=\"javax.swing.border.EmptyBorder\"> \
    <object class=\"java.awt.Insets\"> \
     <int>0<\/int> \
     <int>0<\/int> \
     <int>0<\/int> \
     <int>0<\/int> \
    <\/object> \
   <\/object> \
  <\/void> \
  <void property=\"bottomComponent\"> \
   <object idref=\"JButton0\"/> \
  <\/void> \
  <void property=\"dividerLocation\"> \
   <int>95<\/int> \
  <\/void> \
  <void property=\"leftComponent\"> \
   <object class=\"javax.swing.JButton\"> \
    <string>tasto sinistro<\/string> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>base_split_pane<\/string> \
  <\/void> \
  <void property=\"rightComponent\"> \
   <object idref=\"JButton0\"/> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:-1,
location:"0,25",
name:"base_split_pane",
size:"1024,570",
typeid:12,
uuid:"1D1C9016-D812-4CF2-868B-561E0E192531"
},
{
anchors:11,
items:[
{
containsFormID:"96CD3598-5DF2-468B-B748-BBECDE93E74B",
location:"680,6",
text:"header",
typeid:15,
uuid:"3D19FB5A-A027-4AAA-A660-922F2D0D26AB"
}
],
location:"0,0",
name:"header",
printable:false,
size:"1024,25",
typeid:16,
uuid:"5DE685E4-664D-463B-A85F-AB31CEF9FB87"
},
{
height:600,
partType:5,
typeid:19,
uuid:"ABF7D4F7-192A-4C45-BB11-0D576BDFDD23"
},
{
anchors:15,
items:[
{
containsFormID:"D3440F7C-E371-45BA-BD6E-4C955CDD1821",
location:"770,180",
text:"nfx_interfaccia_pannello_laterale",
typeid:15,
uuid:"BC0EF459-CADB-41AB-88A3-817D0C6F2CAA"
}
],
location:"780,100",
name:"sidebar",
printable:false,
size:"170,380",
typeid:16,
uuid:"CB111B24-A41F-4ECA-A478-318C6EE7C288"
},
{
beanClassName:"javax.swing.JLabel",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_11\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JLabel\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>110<\/int> \
    <int>20<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"focusable\"> \
   <boolean>false<\/boolean> \
  <\/void> \
  <void property=\"inheritsPopupMenu\"> \
   <boolean>false<\/boolean> \
  <\/void> \
  <void property=\"name\"> \
   <string>bean_599<\/string> \
  <\/void> \
  <void property=\"requestFocusEnabled\"> \
   <boolean>false<\/boolean> \
  <\/void> \
  <void property=\"verifyInputWhenFocusTarget\"> \
   <boolean>false<\/boolean> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"170,0",
name:"keyLabel",
size:"110,20",
typeid:12,
uuid:"D5F1437E-8048-4B37-ADE7-F8B88738346C"
}
],
name:"nfx_interfaccia_pannello_base",
navigatorID:"-1",
onHideMethodID:"-1",
onLoadMethodID:"4CBE0613-5FEA-4B2B-8234-15883B23DEFA",
onShowMethodID:"1337BBC6-E854-426F-84DB-5EA24FC10C4D",
paperPrintScale:100,
scrollbars:36,
showInMenu:true,
size:"1024,600",
styleName:"keeneight",
titleText:"VIRTUALCAR 2.0 - nfx",
typeid:3,
uuid:"BF611297-957D-44D8-AC80-93B64E6EB7CF"