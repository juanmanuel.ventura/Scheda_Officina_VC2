/**
 * @properties={typeid:35,uuid:"351B20B3-BD3A-47B5-80CA-3AD1121FA745",variableType:-4}
 */
var first = true;

/**
 *
 * @properties={typeid:24,uuid:"91CCD048-0CA5-4127-9E07-D2F624CDBBC6"}
 * @AllowToRunInFind
 */
function mouseActivity() {
//	globals.nfx_setUserActive();
}

/**
 *
 * @properties={typeid:24,uuid:"4CBE0613-5FEA-4B2B-8234-15883B23DEFA"}
 */
function pannelloBaseOnLoad() {
	setupSplitPane();
}

/**
 *
 * @properties={typeid:24,uuid:"1337BBC6-E854-426F-84DB-5EA24FC10C4D"}
 * @AllowToRunInFind
 */
function pannelloBaseOnShow() {

	if (first === true) {
		//FS
		/** @type {java.awt.Component} */		
		var labelAsComponent = elements.keyLabel;
		//dep 	var win = Packages.javax.swing.SwingUtilities.getWindowAncestor(elements.keyLabel);
		var win = Packages.javax.swing.SwingUtilities.getWindowAncestor(labelAsComponent);
		var impl = { mouseDragged: mouseActivity, mouseMoved: mouseActivity };
		win.addMouseMotionListener(new java.awt.event.MouseMotionListener(impl));

		first = false;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"1CE9D43B-F222-4512-A15E-2EB7599A42B7"}
 */
function setupSplitPane() {
	globals.utils_setSplitPane({
		splitpane: elements.base_split_pane,
		leftComponent: elements.sidebar,
		rightComponent: elements.content,
		resizeWeight: 1,
		dividerLocation: 260,
		orientation: 1,
		dividerSize: 1,
		continuousLayout: 0
	});
	elements.base_split_pane.dividerLocation = 260;
}
