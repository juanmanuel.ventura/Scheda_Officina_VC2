/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"8ECEC5B0-AA26-4F60-A644-F78C6142F25F"}
 */
var mainForm = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"DDA270CE-973A-4C77-9FB3-9F0F514E580A"}
 */
var relatedSelected = null;

/**
 * @properties={typeid:35,uuid:"EC2760AD-BFB6-4187-A2D9-76DCFD84E948",variableType:-4}
 */
var sk0_method = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B985CC3D-C2CF-4235-989E-02BF5F1C4432"}
 */
var sk1_method = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"56EA1E8E-04FE-4D24-99E7-0DDEC280703C"}
 */
var sk2_method = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"75E57AB6-51CF-4569-B574-BC7C2E9EAF4B"}
 */
var sk3_method = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"603DAB2C-8373-476B-8C9F-5DFDA621D5F9"}
 */
var sk4_method = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"68501622-D794-4962-8295-041AA92572EB"}
 */
var sk5_method = null;

/**
 * @properties={typeid:35,uuid:"6153159E-36CE-4E7A-9759-CD223080E966",variableType:-4}
 */
var sk6_method = null;

/**
 * @properties={typeid:35,uuid:"BCA4ABEB-A1E2-463E-B1BB-EFE1B3E6409E",variableType:-4}
 */
var sk7_method = null;

/**
 * @properties={typeid:35,uuid:"F7597053-C04B-4682-82B8-6B13922DA107",variableType:-4}
 */
var sk8_method = null;

/**
 * @properties={typeid:35,uuid:"5F915704-3A4B-4385-8863-CC9BB72CAAE5",variableType:-4}
 */
var sk9_method = null;

/**
 *
 * @properties={typeid:24,uuid:"3AC58E49-9162-4297-9DEF-42E2B928925B"}
 */
function getSelectedTabNumber() {
	for (var i = 0; i < elements.related.getMaxTabIndex(); i++) {
		if (elements.related.getTabFormNameAt(i) == relatedSelected) {
			return i;
		}
	}
	return null;
}

/**
 * @properties={typeid:24,uuid:"FE6B18D7-6FFD-49B0-8728-170886C014DD"}
 */
function pannelloDxOnLoad() {
	elements.main.removeAllTabs();
	elements.related.removeAllTabs();
	forms.nfx_interfaccia_pannello_buttons.hideNavButtons();
	setSplitPane();

	if (forms["application_main_form"]) {
		elements.main.addTab(forms["application_main_form"]);
		mainForm = "application_main_form";
	}
}

/**
 * @properties={typeid:24,uuid:"922C307A-42C7-4B86-9C0A-0E37F64737CA"}
 */
function pannelloDxOnShow() { }

/**
 *
 * @properties={typeid:24,uuid:"F9BCAC32-8B62-49B8-A2F1-BE97D831D759"}
 */
function pannelloDxOntabchange() {
	syncTabs();
}

/**
 *
 * @properties={typeid:24,uuid:"5911B038-5685-47C9-9B76-F1BF88D2B8EB"}
 */
function setSplitPane() {

	//SAuc

	//var e=application.getWindowHeight();

	var _context = controller.getFormContext();
	/** @type {String} */
	var _window = _context.getValue(1, 1);
	var _windowObject = application.getWindow(_window);

	var height = _windowObject.getHeight();

	//	globals.utils_setSplitPane({splitpane : elements.main_split_pane,
	//								   leftComponent : elements.main,
	//								   rightComponent : elements.related,
	//								   resizeWeight : 1,
	//								   dividerLocation : Math.floor(application.getWindowHeight()),
	//								   orientation : 0,
	//								   dividerSize : 1,
	//								   continuousLayout : 0});

	globals.utils_setSplitPane({
		splitpane: elements.main_split_pane,
		leftComponent: elements.main,
		rightComponent: elements.related,
		resizeWeight: 1,
		dividerLocation: Math.floor(height),
		orientation: 0,
		dividerSize: 1,
		continuousLayout: 0
	});
}

/**
 *
 * @properties={typeid:24,uuid:"4F826816-D262-414F-B37B-3461E54A8676"}
 * @AllowToRunInFind
 */
function syncTabs() {
	
	var form = elements.main.getTabFormNameAt(1);
	for (var i = 1; i <= elements.related.getMaxTabIndex(); i++) {
		var tabFormName = elements.related.getTabFormNameAt(i);
		var tabRelationName = elements.related.getTabRelationNameAt(i);
		try{
		forms[tabFormName].controller.loadRecords(forms[form][tabRelationName]);
		}catch(ex){}
	}
	
}
