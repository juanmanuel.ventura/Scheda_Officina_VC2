/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"E58A8597-E655-4880-AB1F-28CE57867D1D"}
 */
var clearedOrReduced = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"86F3BD5A-47B9-40A2-ACBC-A2363D7D06AA"}
 */
var mode = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D2238288-1A99-4F66-B243-4E3CC1FC695D"}
 */
var replacingForm = null;

/**
 * @properties={typeid:35,uuid:"60E0D484-E1E5-4C19-BC9C-876C2525FAB3",variableType:-4}
 */
var savedFoundset = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B5BAB9CE-1B08-4CED-A130-611B9C9D5A3E"}
 */
var savedSql = null;

/**
 * @properties={typeid:35,uuid:"A3E72043-BE26-4E08-A915-68F28DFCCBEB",variableType:-4}
 */
var savedSqlParams = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"52A88E71-092A-40F4-B19A-F289EC45794E"}
 */
var workingForm = null;

/**
 * 
 * @properties={typeid:24,uuid:"368D61A1-4CCA-41F6-B213-AA3F49B8CA95"}
 */
function add() {
	
	
	var form=arguments[5];
	//application.output(form);
	
	globals.nfx_setBusy(true);
	
	workingForm = form;
	
	if(form=="anagrafica_progetti_ver_table" ||form=="anagrafica_progetti_ver_record" ||
	form=="anagrafica_progetti_ver" ||  form=="application_main_form_record"){

		var pow = globals["isPowerUser"](globals.nfx_user);
		if(pow==false){
			plugins.dialogs.showWarningDialog("Attenzione", "Operazione non consentita, non hai i permessi.", "Ok");
			globals.nfx_setBusy(true);
			var form1 = workingForm;
			if (forms[form1].nfx_postAddOnCancel && typeof forms[form1].nfx_postAddOnCancel == "function") {
			forms[form1].nfx_postAddOnCancel();
			}
			databaseManager.revertEditedRecords();
			databaseManager.setAutoSave(true);
			enableCorrectButtons();
			forms[form1].controller.readOnly = true;
			enlargeFoundset(form1);
			mode = null;
			forms.nfx_interfaccia_pannello_header.elements.formName.text = globals.nfx_getTitleFormName(forms.nfx_interfaccia_pannello_body.mainForm);
			updatePosition();
			enableNavigationTree();
			globals.nfx_setBusy(false);
			return;
		}
		
	}
	if(form=="distinte_vetture_note_record"){
		edit(form.toString());
		return;
	}
	if(form=="distinte_vetture_children_table"){
		plugins.dialogs.showWarningDialog("Attenzione", "Operazione non consentita.", "Ok");
		globals.nfx_setBusy(true);
		var form3 = workingForm;
		if (forms[form3].nfx_postAddOnCancel && typeof forms[form3].nfx_postAddOnCancel == "function") {
		forms[form3].nfx_postAddOnCancel();
		}
		databaseManager.revertEditedRecords();
		databaseManager.setAutoSave(true);
		enableCorrectButtons();
		forms[form3].controller.readOnly = true;
		enlargeFoundset(form3);
		mode = null;
		forms.nfx_interfaccia_pannello_header.elements.formName.text = globals.nfx_getTitleFormName(forms.nfx_interfaccia_pannello_body.mainForm);
		updatePosition();
		enableNavigationTree();
		globals.nfx_setBusy(false);
		return;
	}
	if(form=="distinte_progetto_note_record"){
		edit(form.toString());
		return;
	}
	
	/*
	 * TIPO: function
	 * È possibile defnire un metodo che bypassi completamente la procedura di inserimento di default.
	 * Se oltra a questa si ha la necessità di eseguire la procedura standard è possibile forzare questo
	 * comportamento facendo tornare a questa funzione un valore NON NULLO (-1,"vai avanti", etc.)
	 */
	if (forms[form].nfx_customAdd && typeof forms[form].nfx_customAdd == "function") {
		var goAhead = forms[form].nfx_customAdd();
		if (!goAhead) {
			globals.nfx_setBusy(false);
			//			JStaffa
			//			return 1;
			return;
		}
	}

	disableNavigationTree();

	if (globals.nfx_canAddRecord(form)) {
		forms.nfx_interfaccia_pannello_header.elements.formName.text += " - Modalità AGGIUNTA";
		mode = "add";

		clearFoundset(form);
		
		databaseManager.setAutoSave(false);

		
		try{
			application.output("New Record on:"+forms[form].foundset.getDataSource());
		forms[form].foundset.newRecord(true, true);
		}catch(Exc){
			
			plugins.dialogs.showWarningDialog("Attenzione", "Operazione non consentita.", "Ok");
			
			globals.nfx_setBusy(true);
			var form2 = workingForm;
			if (forms[form2].nfx_postAddOnCancel && typeof forms[form2].nfx_postAddOnCancel == "function") {
			forms[form2].nfx_postAddOnCancel();
			}
			databaseManager.revertEditedRecords();
			databaseManager.setAutoSave(true);
			enableCorrectButtons();
			forms[form2].controller.readOnly = true;
			enlargeFoundset(form2);
			mode = null;
			forms.nfx_interfaccia_pannello_header.elements.formName.text = globals.nfx_getTitleFormName(forms.nfx_interfaccia_pannello_body.mainForm);
			updatePosition();
			enableNavigationTree();
			globals.nfx_setBusy(false);
			return;
			
		}
		
		/*
		 * TIPO: function
		 * È possibile defnire un metodo che esegua operazioni preliminari all'aggiunta di un nuovo record,
		 * se questo ritorna il valore '-1' l'inserimento fallisce
		 */
		if (forms[form].nfx_preAdd && typeof forms[form].nfx_preAdd == "function") {
			var result = forms[form].nfx_preAdd();
			if (result == -1) {
				//				JStaffa
				//				databaseManager.rollbackEditedRecords();
				databaseManager.revertEditedRecords();

				databaseManager.setAutoSave(true);
				enlargeFoundset(form);
				mode = null;
				forms.nfx_interfaccia_pannello_header.elements.formName.text = globals.nfx_getTitleFormName(forms.nfx_interfaccia_pannello_body.mainForm);
				enableNavigationTree();
				globals.nfx_setBusy(false);
				//				JStaffa
				//				return -1;
				return;
			}
		}

		saveCancelMode();
		forms[form].controller.readOnly = false;
	} else {
		plugins.dialogs.showWarningDialog("Attenzione", "Operazione non consentita su " + form + ".", "Ok");
	}

	globals.nfx_setBusy(false);
}

/**
 * @properties={typeid:24,uuid:"849CBD4C-32A1-4401-AC6F-7BB440A9C576"}
 */
function addCancel(formIN) {
	globals.nfx_setBusy(true);

	var form = (formIN) ? formIN : workingForm;

	var risposta = plugins.dialogs.showQuestionDialog("Annulla aggiunta", "Sei certo di voler cancellare il record appena creato?", "Sì", "No");
	if (risposta == "No") {
		return;
	}
	if (risposta == "Sì") {
		if (forms[form].nfx_postAddOnCancel && typeof forms[form].nfx_postAddOnCancel == "function") {
			forms[form].nfx_postAddOnCancel();
		}
		//SAuc
		//databaseManager.rollbackEditedRecords();
		databaseManager.revertEditedRecords();
		databaseManager.setAutoSave(true);
		enableCorrectButtons();
		forms[form].controller.readOnly = true;
		enlargeFoundset(form);
		mode = null;
		forms.nfx_interfaccia_pannello_header.elements.formName.text = globals.nfx_getTitleFormName(forms.nfx_interfaccia_pannello_body.mainForm);
		updatePosition();
		enableNavigationTree();
	}
	globals.repaintAllButtons();
	

	
	globals.nfx_setBusy(false);
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"7868AB2D-45E1-457B-95C9-3610D469EFFC"}
 */
function addRelated(event) {

//	var main = forms.nfx_interfaccia_pannello_body.mainForm;
//	var relatedSelected = forms.nfx_interfaccia_pannello_body.relatedSelected;
//	var menuItems = new Array();
//
//		if (globals.nfx_canAddRecord(main)) {
//			menuItems.push(plugins.popupmenu.createMenuItem(globals.nfx_getTitleFormName(main), add));
//			menuItems[menuItems.length - 1].setMethodArguments(main);
//		}
//		if ( (databaseManager.getFoundSetCount(forms[main].foundset) > 0 || forms[main].controller.getTableName() == "k8_dummy") && relatedSelected && globals.nfx_canAddRecord(relatedSelected)) {
//			menuItems.push(plugins.popupmenu.createMenuItem(globals.nfx_getTitleFormName(relatedSelected), add));
//			menuItems[menuItems.length - 1].setMethodArguments(relatedSelected);
//		}
//		if (menuItems.length == 0) {
//			menuItems.push(plugins.popupmenu.createMenuItem("Impossibile aggiungere record", null));
//		}
//	
//		plugins.popupmenu.showPopupMenu(elements.add, menuItems);		
//SAuc
		var main = forms.nfx_interfaccia_pannello_body.mainForm;
		var relatedSelected = forms.nfx_interfaccia_pannello_body.relatedSelected;
		var menu = plugins.window.createPopupMenu();

	if (globals.nfx_canAddRecord(main)) {
		var item = menu.addMenuItem(globals.nfx_getTitleFormName(main),add);
		item.methodArguments = [main];
		//application.output("Arguments 1 : "+item.methodArguments);
	}
	if ( (databaseManager.getFoundSetCount(forms[main].foundset) > 0 || forms[main].controller.getDataSource().split('/')[2] == "k8_dummy") && relatedSelected && globals.nfx_canAddRecord(relatedSelected)) {
	
		var item2 = menu.addMenuItem(globals.nfx_getTitleFormName(relatedSelected),add);
		item2.methodArguments = [relatedSelected];
		//application.output("Arguments 2 : "+item2.methodArguments);
		
	}
	if (menu.getItemCount() == 0) {
		var item3 = menu.addMenuItem("Impossibile aggiungere record",null);
		//application.output("Arguments 3 : "+item3.methodArguments);
	}
	menu.show(elements.add);
	
}

/**
 * @properties={typeid:24,uuid:"E3F47B62-391A-411E-9208-AD0F309F3D23"}
 * @AllowToRunInFind
 */
function addSave(formIN) {
	globals.nfx_setBusy(true);

	var form = (formIN) ? formIN : workingForm;
	
	if(form == "segnalazioni_anomalie_record" || form == "segnalazioni_anomalie_table"){
		
		var lead = forms["segnalazioni_anomalie_record"].leader ;
		
		/** @type {JSFoundSet<db:/ferrari/k8_gestione_utenze>} */
		var k8_gestione_utenze = databaseManager.getFoundSet('ferrari', 'k8_gestione_utenze');
		
		if(k8_gestione_utenze.find()){
			k8_gestione_utenze.user_as400 = lead;
			var found = k8_gestione_utenze.search();
			if(found > 0){
				application.output("Leader : "+lead);
			}
			else{
				plugins.dialogs.showWarningDialog("Attenzione", "Leader Inesistente!", "Ok");
				globals.nfx_setBusy(false);
				return;
			}
			
		}
		
	}
	
	
		
	if(form == "distinte_vetture_record" || form == "distinte_vetture_table"){
		
		var rec = forms['distinte_vetture_record'].foundset.getSelectedRecord();
		
		var distpr = rec.progetto;
		var vet = rec.vettura;
		var matr = rec.matricola;
		if(distpr == null || matr == null || vet == null){
			plugins.dialogs.showWarningDialog("Attenzione", "Campi obbligatori : Progetto , Vettura e Matricola", "Ok");
			globals.nfx_setBusy(false);
			return;
		}
	}
	if(form == "anagrafica_vetture_table" || form == "anagrafica_vetture_record"){
		
		var rec = forms['anagrafica_vetture_table'].foundset.getSelectedRecord();
		var cd1 = rec.codice_vettura;
		var cd2 = rec.codice_vettura_dist_progetto;
		var tl = rec.numero_telaio;
		if(cd1 == null || cd2 == null || tl == null){
			plugins.dialogs.showWarningDialog("Attenzione", "Campi obbligatori : Progetto , Vettura , Codice Vettura e Numero Telaio", "Ok");
			globals.nfx_setBusy(false);
			return;
		}	
		
	}

	if(forms[form].foundset.utente_creazione == null){
		forms[form].foundset.utente_creazione = globals.nfx_user;
	application.output("Creation by : "+globals.nfx_user);}
	if(forms[form].foundset.timestamp_creazione == null){
		forms[form].foundset.timestamp_creazione = new Date();
		application.output("Creation time : "+new Date());}
	
	
	

	var valid = runValidations(form);
	if (valid == -1) {
		//globals.nfx_setBusy(false);
		return -1;
	}
	
	var t=databaseManager.saveData();

	if(t==false){
		plugins.dialogs.showErrorDialog("Errore","Errore nel salvataggio","Ok");
		globals.nfx_setBusy(false);
		return -1;
	}
	
//	if (tryToSavedata() < 0) {
//		globals.nfx_setBusy(false);
//		return -1;
//	}
	
	databaseManager.setAutoSave(true);
	
	enlargeFoundset(form);
	
	foundset.loadRecords();
	
	
	var currentSort = forms[form].foundset.getCurrentSort();
	
	//SAuc
	//Raccolgo le informazioni sul record appena inserito
	//	var srv = forms[form].controller.getServerName();
	//	var tbl = forms[form].controller.getTableName();
	
//	var srv = forms[form].controller.getDataSource().split('/')[1];
//	var tbl = forms[form].controller.getDataSource().split('/')[2];
//	var pkf = globals.utils_getIdDataProvider(form);
//	var query = "select max(" + pkf + ") from " + tbl;
//	var pk = (srv && tbl && pkf) ? databaseManager.getDataSetByQuery(srv, query, [], 1).getValue(1, 1) : null;
//	var checkFS = forms[form].foundset.duplicateFoundSet();
//	if (checkFS.find()) {
//		checkFS[pkf] = pk;
//		if (!checkFS.search(false, true)) {
//			//Se all'interno del foundset caricato non trovo il record lo aggiungo
//			if (forms[form].foundset.find()) {
//				forms[form].foundset[pkf] = pk;
//				forms[form].foundset.search(false, false);
//			}
//		}
//		forms[form].foundset.sort(currentSort);
//		//Ricerco il record e lo seleziono
//		var indexFS = forms[form].foundset.duplicateFoundSet();
//		for (var i = 1; i <= indexFS.getSize(); i++) {
//			var rec = indexFS.getRecord(i);
//			if (rec[pkf] == pk) {
//				forms[form].foundset.setSelectedIndex(i);
//				break;
//			}
//		}
	
	
	
	var srv = forms[form].controller.getDataSource().split('/')[1];
	var tbl = forms[form].controller.getDataSource().split('/')[2];
	
	var pkf = globals.utils_getIdDataProvider(form);
	var query = "select max(" + pkf + ") from " + tbl;
	var pk = (srv && tbl && pkf) ? databaseManager.getDataSetByQuery(srv, query, [], 1).getValue(1, 1) : null;
	application.output("PK created on table "+tbl+" PK : "+pk);
	
	try{
	var queryREC = 'select * from '+tbl+' Where '+pkf+' = '+pk;
	var REC = databaseManager.getDataSetByQuery(srv, queryREC, [], 1).getRowAsArray(1);
	application.output(REC);
	}catch(Exp){
		application.output("Record Not Available");
	}
	
	//var checkFS = forms[form].foundset.duplicateFoundSet();
	if (foundset.find()) {
		foundset[pkf] = pk;
		if (!foundset.search(false, true)) {
			//Se all'interno del foundset caricato non trovo il record lo aggiungo
			if (forms[form].foundset.find()) {
				forms[form].foundset[pkf] = pk;
				forms[form].foundset.search(false, false);
				
			}
		}
		forms[form].foundset.sort(currentSort);
		//Ricerco il record e lo seleziono
		//var indexFS = forms[form].foundset.duplicateFoundSet();
		for (var i = 1; i <= foundset.getSize(); i++) {
			var rec = foundset.getRecord(i);
			if (rec[pkf] == pk) {
				forms[form].foundset.setSelectedIndex(i);
				break;
			}
		}
	}
	
	

	/*
	 * TIPO: function
	 * È possibile defnire un metodo che esegua operazioni successive all'aggiunta di un nuovo record.
	 */
	if (forms[form].nfx_postAdd && typeof forms[form].nfx_postAdd == "function") {
		forms[form].nfx_postAdd();
	}
	
	

	forms[form].controller.readOnly = true;
	enableCorrectButtons();
	mode = null;
	forms.nfx_interfaccia_pannello_header.elements.formName.text = globals.nfx_getTitleFormName(forms.nfx_interfaccia_pannello_body.mainForm);
	updatePosition();
	enableNavigationTree();
	globals.nfx_setBusy(false);
	return 1;
}

/**
 *
 * @properties={typeid:24,uuid:"9E8951FD-8CCA-4335-920B-18F11C73F82F"}
 */
function addSpecialButton(ic, to, me) {
	
	
	
	var icon = ic || null;
	var tooltip = to || null;
	var method = me || null;

	//	if(formName!=null){
	//		var success = history.removeForm(formName);
	//		if (success) {
	//			solutionModel.removeForm(formName);
	//		}
	//		success = solutionModel.removeForm(formName);
	//		//application.output('solutionModel ' + success);
	//		if (forms[formName]) {
	//
	//			//application.output('esiste ancora');
	//			success = solutionModel.removeForm(formName);
	//			//application.closeAllWindows();
	//		}
	//		forms[formName].controller.recreateUI();
	//	}
	//

	if (!elements.sk0) {
		setSpecialButton(method, icon, tooltip, 630, "sk0");
	} else if (!elements.sk1) {
		setSpecialButton(method, icon, tooltip, 655, "sk1");
	} else if (!elements.sk2) {
		setSpecialButton(method, icon, tooltip, 680, "sk2");
	} else if (!elements.sk3) {
		setSpecialButton(method, icon, tooltip, 705, "sk3");
	} else if (!elements.sk4) {
		setSpecialButton(method, icon, tooltip, 730, "sk4");
	} else if (!elements.sk5) {
		setSpecialButton(method, icon, tooltip, 755, "sk5");
	} else if (!elements.sk6) {
		setSpecialButton(method, icon, tooltip, 780, "sk6");
	} else if (!elements.sk7) {
		setSpecialButton(method, icon, tooltip, 805, "sk7");
	} else if (!elements.sk8) {
		setSpecialButton(method, icon, tooltip, 730, "sk8");
	} else if (!elements.sk9) {
		setSpecialButton(method, icon, tooltip, 755, "sk9");
	} else {
		// 
		plugins.dialogs.showErrorDialog("Errore di rete", "At the moment You cannot set more than ten Special Keys... sorry", "Ok");
	}
	
//	enableCorrectButtons();
//	enableNavigationTree();
	
//	navNext();
//	navPrevious();
	
	//globals.nfx_setBusy(false);
	
	return;
}

/**
 *
 * @properties={typeid:24,uuid:"30FA41A3-DB2D-4401-ADCD-30ADA3A71BF9"}
 */
function adjustValue(label) {

	if (label) {
		//Se la stringa in ingresso rappresenta un numero la lascio invariata
		try {
			java.lang.Double.parseDouble(label);
			return label;
		} catch (e) {
			//non è un numero, continua
		}
		//se il primo carattere è uno di questi operatori lascio invariata la stringa
		var skip = [">", "<", "#", "^"];
		if (skip.indexOf(label.substr(0, 1)) != -1) return label;
		//se l'operatore è di "non-uguaglianza" devo inserire lo # tra lui e il
		//valore per rendere la ricerca "case-insensitive"
		if (label.substr(0, 1) == "!") return (label.substr(1, 1) == "=") ? label.replace("!=", "!#") : label.replace("!", "!#");
		//se è un valore "normale" aggiungo lo # e restituisco il valore
		return "#" + label;
	} else {
		throw "Missing parameter";
	}
}

/**
 *
 * @properties={typeid:24,uuid:"3C70E028-0D86-4C7D-8C99-BF6EF2899A7C"}
 */
function cancel() {
	if (mode == "add") {
		addCancel(null);
		globals.nfx_syncTabs();
		return;
	}
	if (mode == "edit") {
		editCancel(null);
		globals.nfx_syncTabs();
		return;
	}
	if (mode == "find") {
		findCancel();
		globals.nfx_syncTabs();
		return;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"923E1814-36E1-468C-B6B7-E8921F85E574"}
 * @AllowToRunInFind
 */
function clearFoundset(form) {
	clearedOrReduced = null;
	//il form è nella parte alta ed è una tabella
	if (form == globals.nfx_selectedProgram() && globals.nfx_isDynamic(form) != "record") {
		savedFoundset = forms[form].foundset.duplicateFoundSet();
		clearedOrReduced = "main";
	}
	//in form è un correlato
	if (form == globals.nfx_selectedTab()) {
		clearedOrReduced = "related";
	}
	if (clearedOrReduced) {
		var idDataProvider = globals.utils_getIdDataProvider(form);
		if (idDataProvider) {
			if (forms[form].foundset.find()) {
				forms[form].foundset[idDataProvider] = "^";
				forms[form].foundset.search();
			}
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"EAB34159-D2F4-4BE1-A92A-59E31BB2726F"}
 */
function disableButtons(exceptions) {
	for (var i = 0; i < elements.allnames.length; i++) {
		if (!exceptions || exceptions.indexOf(elements.allnames[i]) == -1) elements[elements.allnames[i]].enabled = false;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"7CC766A3-FF12-411E-946B-6EE3A572EA1D"}
 */
function disableNavigationTree() {
	forms.nfx_interfaccia_pannello_tree.elements.tree.enabled = false;
	forms.nfx_interfaccia_pannello_tree.elements.back.enabled = false;
	forms.nfx_interfaccia_pannello_tree.elements.next.enabled = false;
	forms.nfx_interfaccia_pannello_tree.elements.configurator.enabled = false;
}

/**
 *@param {String} [formName]
 * @properties={typeid:24,uuid:"22330BF4-922A-4EAC-B641-3BAB14C1C53D"}
 */
function edit(formName) {
	
	
	globals.nfx_setBusy(true);
	/** @type {String} */	
	var form="";
	if(formName=="" || formName==null || formName==-1){
		form=arguments[5];
	}
	else{
		form=formName;
	}
	
	if(form=="anagrafica_progetti_ver_table" ||form=="anagrafica_progetti_ver_record" ||
	form=="anagrafica_progetti_ver" ||  form=="application_main_form_record"){

		var pow = globals["isPowerUser"](globals.nfx_user);
		if(pow==false){
			plugins.dialogs.showWarningDialog("Attenzione", "Operazione non consentita, non hai i permessi.", "Ok");
			globals.nfx_setBusy(true);
			var form1 = workingForm;
			if (forms[form1].nfx_postAddOnCancel && typeof forms[form1].nfx_postAddOnCancel == "function") {
			forms[form1].nfx_postAddOnCancel();
			}
			databaseManager.revertEditedRecords();
			databaseManager.setAutoSave(true);
			enableCorrectButtons();
			forms[form1].controller.readOnly = true;
			enlargeFoundset(form1);
			mode = null;
			forms.nfx_interfaccia_pannello_header.elements.formName.text = globals.nfx_getTitleFormName(forms.nfx_interfaccia_pannello_body.mainForm);
			updatePosition();
			enableNavigationTree();
			globals.nfx_setBusy(false);
			return;
		}
		
	}
	
	disableNavigationTree();
	workingForm = form;

	if (globals.nfx_canEditRecord(form)) {
		databaseManager.setAutoSave(false);
		var success = databaseManager.acquireLock(forms[form].foundset, forms[form].controller.getSelectedIndex());

		if (!success) {
			plugins.dialogs.showErrorDialog("Operazione non riuscita!", "Questo record è bloccato da un altro utente", "Ok");
			databaseManager.setAutoSave(true);
			enableNavigationTree();
			globals.nfx_setBusy(false);
			return ;
		}
		/*
		 * TIPO: function
		 * È possibile defnire un metodo che esegua operazioni preliminari alla modifica di un record,
		 * se questo ritorna il valore '-1' la modifica fallisce
		 */
		if (forms[form].nfx_preEdit && typeof forms[form].nfx_preEdit == "function") {
			var result = forms[form].nfx_preEdit();
			if (result == -1) {
				//SAuc
				//databaseManager.rollbackEditedRecords();
				databaseManager.revertEditedRecords();
				databaseManager.releaseAllLocks();
				databaseManager.setAutoSave(true);
				enableNavigationTree();
				globals.nfx_setBusy(false);
				return ;
			}
		}

		reduceFoundset(form);
		saveCancelMode();
		forms[form].controller.readOnly = false;
		forms.nfx_interfaccia_pannello_header.elements.formName.text += " - Modalità MODIFICA";
		mode = "edit";
	} else {
		plugins.dialogs.showWarningDialog("Attenzione", "Operazione non consentita su " + form + ".", "Ok");
		
	}

	globals.nfx_setBusy(false);
	return ;
}

/**
 *
 * @properties={typeid:24,uuid:"7CAB6783-2B00-4B80-B44F-20FA0A1BADCF"}
 */
function editCancel(formIN) {
	globals.nfx_setBusy(true);

	var form = (formIN) ? formIN : workingForm;

	var risposta = plugins.dialogs.showQuestionDialog("Annulla cambiamenti", "Sei certo di voler annullare i cambiamenti apportati al record?", "Sì", "No");
	if (risposta == "Sì") {
		if (forms[form].nfx_postEditOnCancel && typeof forms[form].nfx_postEditOnCancel == "function") {
			forms[form].nfx_postEditOnCancel();
		}

		//SAuc
		//databaseManager.rollbackEditedRecords();
		databaseManager.revertEditedRecords();

		databaseManager.releaseAllLocks();
		databaseManager.setAutoSave(true);

		enableCorrectButtons();
		forms[form].controller.readOnly = true;
		enlargeFoundset(form);
		forms.nfx_interfaccia_pannello_header.elements.formName.text = globals.nfx_getTitleFormName(forms.nfx_interfaccia_pannello_body.mainForm);
		enableNavigationTree();
		mode = null;
	}

	globals.nfx_setBusy(false);
}

/**
 *@param {JSEvent} event
 * @properties={typeid:24,uuid:"0CBB0CFB-9F5C-486A-90F7-2840BA36333A"}
 */
function editRelated(event) {
//SAuc	
	var main = forms.nfx_interfaccia_pannello_body.mainForm;
	var relatedSelected = forms.nfx_interfaccia_pannello_body.relatedSelected;
	var menu = plugins.window.createPopupMenu();
	
	if (globals.nfx_canEditRecord(main) && forms[main].foundset.getSize() != 0) {
		var item = menu.addMenuItem(globals.nfx_getTitleFormName(main),edit);
		item.methodArguments = [main];
		//application.output("Arguments 1 : "+item.methodArguments);
		}
	if (relatedSelected && globals.nfx_canEditRecord(relatedSelected) && forms[relatedSelected].foundset.getSize() != 0) {
		var item2 = menu.addMenuItem(globals.nfx_getTitleFormName(relatedSelected),edit);
		item2.methodArguments = [relatedSelected];
		//application.output("Arguments 2 : "+item2.methodArguments);
		}
		if (menu.getItemCount() == 0) {
			var item3 = menu.addMenuItem("Impossibile modificare record",null);
			//application.output("Arguments 3 : "+item3.methodArguments);
			}
		
			menu.show(elements.add);
		
	//plugins.popupmenu.showPopupMenu(elements.edit, menuItems);

//	var main = forms.nfx_interfaccia_pannello_body.mainForm;
//	var relatedSelected = forms.nfx_interfaccia_pannello_body.relatedSelected;
//	var menuItems = plugins.window.createPopupMenu();
//	
//	if (globals.nfx_canEditRecord(main) && forms[main].foundset.getSize() != 0) {
//		menuItems.addMenuItem(globals.nfx_getTitleFormName(main), edit(main));
//	}
//	if (relatedSelected && globals.nfx_canEditRecord(relatedSelected) && forms[relatedSelected].foundset.getSize() != 0) {
//		menuItems.addMenuItem(globals.nfx_getTitleFormName(relatedSelected), edit(relatedSelected));
//	}
//	if (menuItems.getItemCount() == 0) {
//		menuItems.addMenuItem("Impossibile modificare record", null);
//	}
//	menuItems.show(elements.add);
}

/**
 *
 * @properties={typeid:24,uuid:"C78C9E9C-ADA3-4C0F-9CE0-BF0E9BCA8BD6"}
 * @AllowToRunInFind
 */
function editSave(formIN) {
	globals.nfx_setBusy(true);

	var form = (formIN) ? formIN : workingForm;
	
if(form == "segnalazioni_anomalie_record" || form == "segnalazioni_anomalie_table"){
		
		var lead = forms["segnalazioni_anomalie_record"].leader ;
		
		/** @type {JSFoundSet<db:/ferrari/k8_gestione_utenze>} */
		var k8_gestione_utenze = databaseManager.getFoundSet('ferrari', 'k8_gestione_utenze');
		
		if(k8_gestione_utenze.find()){
			k8_gestione_utenze.user_as400 = lead;
			var found = k8_gestione_utenze.search();
			if(found > 0){
				application.output("Leader : "+lead);
			}
			else{
				plugins.dialogs.showWarningDialog("Attenzione", "Leader Inesistente!", "Ok");
				globals.nfx_setBusy(false);
				return;
			}
			
		}
		
		
		
		
	}
	
if(form == "distinte_vetture_record" || form == "distinte_vetture_table"){
		
		var rec = forms['distinte_vetture_record'].foundset.getSelectedRecord();
		
		var distpr = rec.progetto;
		var vet = rec.vettura;
		var matr = rec.matricola;
		if(distpr == null || matr == null || vet == null){
			plugins.dialogs.showWarningDialog("Attenzione", "Campi obbligatori : Progetto , Vettura e Matricola", "Ok");
			globals.nfx_setBusy(false);
			return;
		}
	}
	if(form == "anagrafica_vetture_table" || form == "anagrafica_vetture_record"){
		
		var rec = forms['anagrafica_vetture_table'].foundset.getSelectedRecord();
		var cd1 = rec.codice_vettura;
		var cd2 = rec.codice_vettura_dist_progetto;
		var tl = rec.numero_telaio;
		if(cd1 == null || cd2 == null || tl == null){
			plugins.dialogs.showWarningDialog("Attenzione", "Campi obbligatori : Progetto , Vettura , Codice Vettura e Numero Telaio", "Ok");
			globals.nfx_setBusy(false);
			return;
		}	
		
	}

	if(forms[form].foundset.utente_modifica == null || forms[form].foundset.utente_modifica){
		forms[form].foundset.utente_modifica = globals.nfx_user;
		application.output("Modified by : "+globals.nfx_user);
	}
	if(forms[form].foundset.utente_modifica == null || forms[form].foundset.utente_modifica){
		forms[form].foundset.timestamp_modifica = new Date();
		application.output("Modified time : "+new Date());		
		
	}
	
	var valid = runValidations(form);
	if (valid == -1) {
		globals.nfx_setBusy(false);
		return -1;
	}
	var t=databaseManager.saveData();

	if(t==false){
		plugins.dialogs.showErrorDialog("Errore","Errore nel salvataggio","Ok");
		globals.nfx_setBusy(false);
		return -1;
	}
	
//	if (tryToSavedata() < 0) {
//		globals.nfx_setBusy(false);
//		return -1;
//	}

	var currentSort = forms[form].foundset.getCurrentSort();
	var pkf = globals.utils_getIdDataProvider(form);
	var pk = forms[form].foundset[pkf];
	
	application.output("Modified : "+forms[form].foundset.getSelectedRecord());
	

	databaseManager.releaseAllLocks();
	databaseManager.setAutoSave(true);
	enlargeFoundset(form);

	//var checkFS = forms[form].foundset.duplicateFoundSet();
	if (foundset.find()) {
		foundset[pkf] = pk;
		if (!foundset.search(false, true)) {
			//Se all'interno del foundset caricato non trovo il record lo aggiungo
			if (forms[form].foundset.find()) {
				forms[form].foundset[pkf] = pk;
				forms[form].foundset.search(false, false);
			}
		}
		forms[form].foundset.sort(currentSort);
		//Ricerco il record e lo seleziono
		//var indexFS = forms[form].foundset.duplicateFoundSet();
		for (var i = 1; i <= foundset.getSize(); i++) {
			var rec = foundset.getRecord(i);
			if (rec[pkf] == pk) {
				forms[form].foundset.setSelectedIndex(i);
				break;
			}
		}
	}

	/*
	 * TIPO: function
	 * È possibile defnire un metodo che esegua operazioni successive alla modifica di un record.
	 */
	if (forms[form].nfx_postEdit && typeof forms[form].nfx_postEdit == "function") {
		forms[form].nfx_postEdit();
	}

	forms[form].controller.readOnly = true;
	enableCorrectButtons();
	forms.nfx_interfaccia_pannello_header.elements.formName.text = globals.nfx_getTitleFormName(forms.nfx_interfaccia_pannello_body.mainForm);
	enableNavigationTree();
	mode = null;
	
	//globals.repaintAllButtons();
	globals.nfx_setBusy(false);
	return 1;
}

/**
 *
 * @properties={typeid:24,uuid:"09BA1E68-16DF-4064-9F12-64DD3B50C97B"}
 * @AllowToRunInFind
 */
function enableCorrectButtons() {
	var form = forms.nfx_interfaccia_pannello_body.mainForm
	


	elements.add.enabled = true;
	elements.remove.enabled = true;
	elements.edit.enabled = true;

	elements.find.enabled = true;
	elements.sort.enabled = true;

	if (forms[globals.nfx_getBaseName(form) + "_report"]) {
		elements.report.enabled = true;
	} else {
		elements.report.enabled = false;
	}

	elements.xexport.enabled = true;

	elements.save.enabled = false;
	elements.cancel.enabled = false;

	if (form.search("_table") != -1 && forms[form.replace("_table", "_record")]) {
		elements.dtl.enabled = true;
	} else {
		elements.dtl.enabled = false;
	}

	if (form.search("_record") != -1 && forms[form.replace("_record", "_table")]) {
		elements.tbl.enabled = true;
	} else {
		elements.tbl.enabled = false;
	}

	elements.first.enabled = true;
	elements.previous.enabled = true;
	elements.position.enabled = true;
	elements.next.enabled = true;
	elements.last.enabled = true;

	if (elements.sk0) {
		elements.sk0.enabled = true;
	}
	if (elements.sk1) {
		elements.sk1.enabled = true;
	}
	if (elements.sk2) {
		elements.sk2.enabled = true;
	}
	if (elements.sk3) {
		elements.sk3.enabled = true;
	}
	if (elements.sk4) {
		elements.sk4.enabled = true;
	}
	if (elements.sk5) {
		elements.sk5.enabled = true;
	}
	if (elements.sk6) {
		elements.sk6.enabled = true;
	}
	if (elements.sk7) {
		elements.sk7.enabled = true;
	}
	if (elements.sk8) {
		elements.sk8.enabled = true;
	}
	if (elements.sk9) {
		elements.sk9.enabled = true;
	}

	/*
	 * TIPO: function
	 * Metodo che permette di abilitare/disabilitare funzioni
	 * in maniera indipendente da quato definito dalla logica di NFX
	 */
	if (forms[form].nfx_customEnableButtons) {
		forms[form].nfx_customEnableButtons();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"52EEF4AF-C6F1-4FDD-9FFC-AC0E5A15F293"}
 */
function enableNavigationTree() {
	forms.nfx_interfaccia_pannello_tree.elements.tree.enabled = true;
	forms.nfx_interfaccia_pannello_tree.elements.back.enabled = true;
	forms.nfx_interfaccia_pannello_tree.elements.next.enabled = true;
	forms.nfx_interfaccia_pannello_tree.elements.configurator.enabled = true;
	return;
}

/**
 *
 * @properties={typeid:24,uuid:"B4B7EBEF-5B0D-47F8-A961-03BB1E63DA2E"}
 * @AllowToRunInFind
 */
function enlargeFoundset(form) {
	if (clearedOrReduced) {
		if (clearedOrReduced == "related") {
			var idDataProvider = globals.utils_getIdDataProvider(form);
			if (idDataProvider) {
				forms[form].foundset.loadRecords();
				databaseManager.refreshRecordFromDatabase(forms[form].foundset, -1);
				if (forms[form].foundset.find()) {
					forms[form].foundset[idDataProvider] = "!^";
					forms[form].foundset.search();
				}
			}
			globals.nfx_syncTabs();
		} else {
			databaseManager.saveData();
			databaseManager.refreshRecordFromDatabase(savedFoundset, -1);
			forms[form].foundset.loadRecords(savedFoundset);
			savedFoundset = null;
		}
		clearedOrReduced = null;
	}
}

/**
 * @properties={typeid:24,uuid:"B4E15037-43DF-4097-8B1B-DC016A960589"}
 */
function launchExport(form) {
	var bk = globals.nfx_customActiomName;
	globals.nfx_customActiomName = form;
	excelExport();
	globals.nfx_customActiomName = bk;
}

/**
 *
 * @properties={typeid:24,uuid:"C6F07FB9-5DF5-457F-B233-D0F0F55E617D"}
 */
function excelExport() {
	var form = globals.nfx_selectedProgram();
	
	if(form == "albero_progetti_full" || form == "albero_progetti_lite"){
		if(globals.vc2_currentProgetto == null ||globals.vc2_currentVersione == null) {
			plugins.dialogs.showErrorDialog("Error","Progetto o versione mancanti!", "Ok");
			return;
		}
	}

	if (forms[form].nfx_customExport && typeof forms[form].nfx_customExport == "function") {
		var goAhead = forms[form].nfx_customExport();
		if (!goAhead) {
			return;
		}
	}

	//application.showFormInDialog(forms.nfx_interfaccia_popup_excel, null, null, null, null, "Export", true, false, "export_popup", true);
	
	var formq = forms.nfx_interfaccia_popup_excel;
	var window = application.createWindow("export_popup",JSWindow.MODAL_DIALOG);
	window.title="Export";
	window.showTextToolbar(false);
	window.resizable=true;
	formq.controller.show(window);
	
	if (forms.nfx_interfaccia_popup_excel.canExport) {
		if (databaseManager.getFoundSetCount(forms[form].foundset) > 100 && plugins.dialogs.showQuestionDialog("Export", "L'estrazione dei " + databaseManager.getFoundSetCount(forms[form].foundset) + " record richiesti potrebbe richiedere molto tempo, continuare?", "Sì", "No") == "No") {
			return;
		}
		var columns = (forms[form].nfx_excelExport && typeof forms[form].nfx_excelExport == "function") ? forms.nfx_interfaccia_popup_excel.choices.concat(forms[form].nfx_excelExport()) : forms.nfx_interfaccia_popup_excel.choices;
		var buffer = "";
		var max = databaseManager.getFoundSetCount(forms[form].foundset);
		var fs = forms[form].foundset.duplicateFoundSet();
		for (var l = 0; l < columns.length; l++) {
			buffer += (l != columns.length - 1) ? columns[l].label + "\t" : columns[l].label + "\n";
		}
		globals.nfx_progressBarShow(max);
		for (var i = 1; i <= max; i++) {
			fs.setSelectedIndex(i);
			for (var j = 0; j < columns.length; j++) {
				var value = fs[columns[j].field];
				if (!value) {
					value = null;
				}
				if (typeof value == "string") {
					value = value.replace(/\n/g, " ");
					value = value.replace(/\t/g, " ");
				}
				if (typeof value == "number") {
					/*
					 * necessario perchè excel interpreta i numeri al contrario di servoy:
					 * "," separa le migliaia e "." separa i decimali
					 */
					
					//SAuc
					//value = (columns[j].format) ? utils.numberFormat(value, columns[j].format).replace(/,/g, "{comma}").replace(/\./g, ",").replace(/{comma}/g, ".") : value;
					
					var num=Number(value);
					
					if(columns[j].format){
						
						value=utils.numberFormat(num, columns[j].format);
						value=value.replace(/,/g, "{comma}").replace(/\./g, ",").replace(/{comma}/g, ".") ;
						
					}
					
					
				}
				if (typeof value == "object" && columns[j].format) {
					//SAuc
					//value = utils.dateFormat(value, columns[j].format);
					
					var date=new Date(value);
					
					value =  utils.dateFormat(date, columns[j].format);
	
					
				}
				buffer += (j != columns.length - 1) ? value + "\t" : value + "\n";
			}
			globals.nfx_progressBarStep();
		}
		globals.utils_saveAsTXT(buffer, "export-" + utils.dateFormat(new Date(), "yyyyMMdd") + ".xls");
	}
	/*
	 var form = forms.nfx_interfaccia_pannello_body.mainForm;

	 if(forms[form].nfx_customExport && typeof forms[form].nfx_customExport == "function"){
	 var goAhead = forms[form].nfx_customExport();
	 if(!goAhead){
	 return;
	 }
	 }

	 application.showFormInDialog(forms.nfx_interfaccia_popup_excel,null,null,null,null,"Export",true,false,"export_popup",true);
	 if(forms.nfx_interfaccia_popup_excel.canExport)
	 {
	 var columns = forms.nfx_interfaccia_popup_excel.choices;

	 var extra = null;
	 if(forms[form].nfx_excelExport && typeof forms[form].nfx_excelExport == "function"){
	 extra = forms[form].nfx_excelExport();
	 }

	 var buffer = "";
	 var select = "";

	 if(databaseManager.getFoundSetCount(forms[form].foundset) > 100 && plugins.dialogs.showQuestionDialog("Export","L'estrazione dei " + databaseManager.getFoundSetCount(forms[form].foundset) + " record richiesti potrebbe richiedere molto tempo, continuare?","Sì","No") == "No"){
	 return;
	 }
	 var dataProviders = new Array();
	 for(var i=0;i<columns.length;i++){
	 buffer += columns[i] + "\t";
	 var name = columns[i];
	 name = name.substr(0,1).toLowerCase() + name.substr(1);
	 name = name.replace(/ /g,"_");
	 select += (i != columns.length - 1) ? name + "," : name;
	 }

	 if(extra){
	 for(var i=0;i<extra.length;i++){
	 buffer += extra[i].titolo + "\t";
	 }
	 }

	 buffer += "\n";
	 var query = databaseManager.getSQL(forms[form].foundset).toUpperCase().replace(globals.utils_getIdDataProvider(form).toUpperCase(),select.toUpperCase());
	 var args = databaseManager.getSQLParameters(forms[form].foundset);
	 var d = databaseManager.getDataSetByQuery(forms[form].controller.getServerName(),query,args,-1);
	 var maxRow = d.getMaxRowIndex();
	 var maxColumn = d.getMaxColumnIndex();

	 for(var a=1;a<=maxColumn;a++){
	 dataProviders.push(d.getColumnAsArray(a));
	 }
	 var f = forms[form].foundset.duplicateFoundSet();
	 globals.nfx_progressBarShow(maxRow);
	 for(var i=0;i<maxRow;i++){
	 for(var j=0;j<maxColumn;j++){
	 if(dataProviders[j][i]){
	 var value = dataProviders[j][i];
	 if(typeof value == "string"){
	 value = value.replace(/\n/g," ");
	 value = value.replace(/\t/g," ");
	 }
	 if(value.constructor === Date){
	 value = utils.dateFormat(value,"dd/MM/yyyy")
	 }
	 buffer += value + "\t";
	 }else{
	 buffer += "\t";
	 }
	 }
	 if(extra)
	 {
	 f.setSelectedIndex(i + 1);
	 for(var z=0;z<extra.length;z++)
	 {
	 if(f[extra[z].relazione] && f[extra[z].relazione].getSize() > 0)
	 {
	 f[extra[z].relazione].setSelectedIndex(1);
	 var value = f[extra[z].relazione][extra[z].campo];
	 if(typeof value == "string")
	 {
	 value = value.replace(/\n/g," ");
	 value = value.replace(/\r/g," ");
	 value = value.replace(/\t/g," ");
	 }
	 if(value && value.constructor === Date)
	 {
	 value = utils.dateFormat(value,"dd/MM/yyyy - w")
	 }
	 buffer += value + "\t";
	 }
	 else
	 {
	 buffer += "\t";
	 }
	 }
	 }
	 buffer += "\n";
	 globals.nfx_progressBarStep();
	 }
	 globals.utils_saveAsTXT(buffer,"export-" + utils.dateFormat(new Date(),"yyyyMMdd") + ".xls");
	 }
	 */
}

/**
 *
 * @properties={typeid:24,uuid:"F8253CFB-7654-40DD-B8EA-854A228A23CA"}
 */
function find(formIN) {
	if (mode == "find") {
		return;
	}

	var form = formIN || forms.nfx_interfaccia_pannello_body.mainForm;

	if (forms[form].nfx_search && typeof forms[form].nfx_search == "function") {
		//SAuc
		//return forms[form].nfx_search();
		forms[form].nfx_search();
		return;
		
	}

	if (forms[form].nfx_preSearch && typeof forms[form].nfx_preSearch == "function") {
		forms[form].nfx_preSearch();
	}

	disableNavigationTree();
	saveCancelMode();
	forms.nfx_interfaccia_pannello_header.elements.formName.text = globals.nfx_getTitleFormName(form) + " - Modalità RICERCA";
	mode = "find";
	forms[form].controller.find();
//	JStaffa riforzato il saveCancelMode che disabilita i bottoni in ricerca
	saveCancelMode();
	forms.nfx_interfaccia_pannello_body.syncTabs();
}

/**
 *
 * @properties={typeid:24,uuid:"A637C370-05CF-4DA9-B880-E62812045E34"}
 * @AllowToRunInFind
 */
function findAll() {
	globals.nfx_setBusy(true);

	var form = forms.nfx_interfaccia_pannello_body.mainForm
	forms[form].controller.loadAllRecords();
	databaseManager.refreshRecordFromDatabase(forms[form].foundset, -1);
	sortDefault();

	globals.nfx_setBusy(false);
}

/**
 *
 * @properties={typeid:24,uuid:"7B78E23E-7C55-4983-B12D-6DABA7779EEC"}
 */
function findCancel() {
	globals.nfx_setBusy(true);

	var form = forms.nfx_interfaccia_pannello_body.mainForm
	forms[form].controller.loadRecords(savedSql, savedSqlParams);
	globals.nfx_syncTabs();
	enableCorrectButtons();
	forms.nfx_interfaccia_pannello_header.elements.formName.text = globals.nfx_getTitleFormName(forms.nfx_interfaccia_pannello_body.mainForm);
	enableNavigationTree();
	mode = null;

	if (forms[form].nfx_postSearchOnCancel && typeof forms[form].nfx_postSearchOnCancel == "function") {
		forms[form].nfx_postSearchOnCancel();
	}

	globals.nfx_setBusy(false);
}

/**
 *
 * @properties={typeid:24,uuid:"60EED55B-B54C-4E3F-B06B-AFD53871BF1C"}
 */
function findModes() {
	if (mode == "find") {
		var risposta = plugins.dialogs.showQuestionDialog("Scegli modalità", "Selezionare la modalità di ricerca desiderata", "Normale", "Ridotta", "Estesa");
		if (!risposta || risposta == "Normale") {
			findSave(null);
			return;
		}
		if (risposta == "Ridotta") {
			findSave(1);
			return;
		}
		if (risposta == "Estesa") {
			findSave(2);
			return;
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"375F4D00-34FE-4C6A-B84B-C99FF282EEC3"}
 * @AllowToRunInFind
 */
function findSave(arg) {
	globals.nfx_setBusy(true);

	var i = null;
	var form = forms.nfx_interfaccia_pannello_body.mainForm;
	i = forms[form].elements.allnames.length;
	
//	for (out = i; i--;) {
//		try {
//			var dataProviderMain = forms[form].elements[forms[form].elements.allnames[i]].getDataProviderID();
//			if (dataProviderMain) {
//				var valueMain = forms[form][dataProviderMain];
//				if (valueMain) {
//					forms[form][dataProviderMain] = adjustValue(valueMain);
//				}
//			}
//		} catch (e) {
//			//do nothing
//		}
//	}

	
	for (i; i--;) {
		try {
			var dataProviderMain = forms[form].elements[forms[form].elements.allnames[i]].getDataProviderID();
			if (dataProviderMain) {
				var valueMain = forms[form][dataProviderMain];
				if (valueMain) {
					forms[form][dataProviderMain] = adjustValue(valueMain);
				}
			}
		} catch (e) {
			//do nothing
		}
	}

//SAuc
//	if (forms[form].nfx_related && forms[form].nfx_related.length) {
//		i = forms[form].nfx_related.length;
//		for (var out = i; i--;) {
//			var related = forms[form].nfx_related[i];
//			var j = forms[related].elements.allnames.length;
//			for (out2 = j; j--;) {
//				try {
//					var dataProviderRelated = forms[related].elements[forms[related].elements.allnames[j]].getDataProviderID();
//					if (dataProviderRelated) {
//						var valueRelated = forms[related][dataProviderRelated];
//						if (valueRelated) {
//							forms[related][dataProviderRelated] = adjustValue(valueRelated);
//						}
//					}
//				} catch (e) {
//					//do nothing
//				}
//			}
//		}
//	}

	if (forms[form].nfx_related && forms[form].nfx_related.length) {
		i = forms[form].nfx_related.length;
		for (i; i--;) {
			var related = forms[form].nfx_related[i];
			var j = forms[related].elements.allnames.length;
			for (j; j--;) {
				try {
					var dataProviderRelated = forms[related].elements[forms[related].elements.allnames[j]].getDataProviderID();
					if (dataProviderRelated) {
						var valueRelated = forms[related][dataProviderRelated];
						if (valueRelated) {
							forms[related][dataProviderRelated] = adjustValue(valueRelated);
						}
					}
				} catch (e) {
					//do nothing
				}
			}
		}
	}
	
	if (arg) {
		if (arg == 1) {
			try {
				forms[form].controller.search(false, true);
			} catch (e) {
				if (e.message.search("expected - got CLOB") != -1) {
					//TODO: inserire messaggio appropriato
				}
			}
		}
		if (arg == 2) {
			try {
				forms[form].controller.search(false, false);
			} catch (e) {
				if (e.message.search("expected - got CLOB") != -1) {
					//come sopra
				}
			}
		}
	} else {
		try {
			forms[form].controller.search();
		} catch (e) {
			if (e.message.search("expected - got CLOB") != -1) {
				//come sopra
			}
		}
	}
	if (forms[form].controller.getMaxRecordIndex() == 0) {
//		JStaffa ripiazzo anche qui lo stesso metodo..
		saveCancelMode();
		forms[form].controller.find();
		plugins.dialogs.showInfoDialog("Nessuna corrispondenza", "Non è stato trovato alcun record corrispondente ai criteri desiderati", "Ok");
		mode = null;
		find(form);
	} else {
		enableCorrectButtons();
		forms.nfx_interfaccia_pannello_header.elements.formName.text = globals.nfx_getTitleFormName(forms.nfx_interfaccia_pannello_body.mainForm);
		enableNavigationTree();
		mode = null;
	}

	if (forms[form].nfx_postSearch && typeof forms[form].nfx_postSearch == "function") {
		forms[form].nfx_postSearch();
	}

	//globals.repaintAllButtons();
	globals.nfx_setBusy(false);
}

/**
 *
 * @properties={typeid:24,uuid:"C095011C-9319-4952-93B9-6BDF72762AA0"}
 */
function findWrap() {
	globals.nfx_setBusy(true);
	var form = forms.nfx_interfaccia_pannello_body.mainForm;
	savedSql = databaseManager.getSQL(forms[form].foundset);
	savedSqlParams = databaseManager.getSQLParameters(forms[form].foundset);
	//application.output('nfx_interfaccia_pannello_buttons.findWrap() savedSql: ' + savedSql + ';\nsavedSqlParams: ' + savedSqlParams,LOGGINGLEVEL.INFO);
	find(form);
	globals.nfx_setBusy(false);
}

/**
 *
 * @properties={typeid:24,uuid:"4672EAD2-FC28-443E-B716-3BE1769A6C6D"}
 */
function hideNavButtons() {
	for (var i = 0; i < elements.allnames.length; i++) {
		elements[elements.allnames[i]].visible = false;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"DFAA7EB5-7DFB-4BEE-AC3C-A2B5E1914FB9"}
 */
function navFirst() {
	globals.nfx_setBusy(true);
	var form = forms.nfx_interfaccia_pannello_body.mainForm
	forms[form].controller.setSelectedIndex(1);
	globals.nfx_setBusy(false);
}

/**
 *
 * @properties={typeid:24,uuid:"5C8E33FC-0587-41E6-80E2-3D8D1DD2C57D"}
 */
function navLast() {
	globals.nfx_setBusy(true);
	var form = forms.nfx_interfaccia_pannello_body.mainForm;
	forms[form].foundset.getRecord(databaseManager.getFoundSetCount(forms[form].foundset));
	forms[form].controller.setSelectedIndex(databaseManager.getFoundSetCount(forms[form].foundset));
	globals.nfx_setBusy(false);
}

/**
 *
 * @properties={typeid:24,uuid:"00FFBB6A-284A-473E-9C8A-ECD83567EAA0"}
 */
function navNext() {
	globals.nfx_setBusy(true);
	var form = forms.nfx_interfaccia_pannello_body.mainForm
	var index = forms[form].controller.getSelectedIndex() + 1;
	if (forms[form].foundset.getRecord(index)) {
		forms[form].controller.setSelectedIndex(index);
	}
	globals.nfx_setBusy(false);
	return ;
}

/**
 *
 * @properties={typeid:24,uuid:"BC875D04-EEAD-4747-A43B-1D385895BBAB"}
 */
function navPrevious() {
	globals.nfx_setBusy(true);
	var form = forms.nfx_interfaccia_pannello_body.mainForm
	if (forms[form].controller.getSelectedIndex() > 1) {
		forms[form].controller.setSelectedIndex(forms[form].controller.getSelectedIndex() - 1);
	}
	globals.nfx_setBusy(false);
	return;
}

/**
 *
 * @properties={typeid:24,uuid:"CCACB2A0-BA6F-427C-8B6F-EF938855D3C4"}
 */
function pannelloButtonsOnShow() {
	if (forms.nfx_interfaccia_pannello_body.mainForm) {
		enableCorrectButtons();
		if (globals.nfx_isDynamic(forms.nfx_interfaccia_pannello_body.mainForm)) {
			updatePosition();
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"FC119612-442C-46E9-BF0B-5503A40F810D"}
 */
function popupReport() {
	
	var form = forms.nfx_interfaccia_pannello_body.mainForm
	
	//SAuc
	//application.showFormInDialog(forms[globals.nfx_getBaseName(form) + "_report"]);
	
	var formq = forms[globals.nfx_getBaseName(form) + "_report"];
	var window = application.createWindow("Window",JSWindow.MODAL_DIALOG);
	
	formq.controller.show(window);
	
}

/**
 *
 * @properties={typeid:24,uuid:"90727DF0-0194-4832-8BF6-E501BB0A1D99"}
 * @AllowToRunInFind
 */
function reduceFoundset(form) {
	clearedOrReduced = null;
	//il form è nella parte alta ed è una tabella
	if (form == globals.nfx_selectedProgram() && globals.nfx_isDynamic(form) != "record") {
		savedFoundset = forms[form].foundset.duplicateFoundSet();
		clearedOrReduced = "main";
	}
	//in form è un correlato
	if (form == globals.nfx_selectedTab()) {
		clearedOrReduced = "related";
	}
	if (clearedOrReduced) {
		var idDataProvider = globals.utils_getIdDataProvider(form);
		if (idDataProvider) {
			var idToFind = forms[form].foundset[idDataProvider];
			if (forms[form].foundset.find()) {
				forms[form].foundset[idDataProvider] = idToFind;
				forms[form].foundset.search();
			}
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"DDE8261E-F225-4CB5-B288-91C4F853A8FD"}
 */
function remove() {
	//FS
	/** @type {String} */	
	var form=arguments[5];
	
	globals.nfx_setBusy(true);
	if (globals.nfx_canRemoveRecord(form) && forms[form].foundset.getSize() != 0) {
		databaseManager.setAutoSave(false);
		var success = databaseManager.acquireLock(forms[form].foundset, forms[form].controller.getSelectedIndex());

		if (!success) {
			plugins.dialogs.showErrorDialog("Operazione non riuscita!", "Questo record è bloccato da un altro utente", "Ok");
			globals.nfx_setBusy(false);
			return -1;
		}

		var risposta = plugins.dialogs.showQuestionDialog("Rimozione", "Sei sicuro di voler rimuovere il record selezionato?", "Sì", "No");
		if (risposta == "Sì") {
			/*
			 * TIPO: function
			 * È possibile defnire un metodo che esegua operazioni preliminari all'eliminazione di un record,
			 * se questo ritorna il valore '-1' l'eliminazione fallisce
			 */
			if (forms[form].nfx_preRemove && typeof forms[form].nfx_preRemove == "function") {
				var result = forms[form].nfx_preRemove();
				if (result == -1) {
					globals.nfx_setBusy(false);
					return -1;
				}
			}

			//forms[form].controller.deleteRecord();
			try{
				var rec = forms[form].foundset.getSelectedRecord();
			forms[form].foundset.deleteRecord();
			}catch(Exc){
				plugins.dialogs.showWarningDialog("Attenzione", "Operazione non consentita.", "Ok");
				findCancel();
				return 1;
			}
			
			application.output("Deleted : "+rec);
			application.output("By : "+globals.nfx_user);
			application.output("Time delete : "+new Date());

			/*
			 * TIPO: function
			 * È possibile defnire un metodo che esegua operazioni successive all'eliminazione di un record,
			 * se questo ritorna il valore '-1' l'eliminazione fallisce
			 */
			if (forms[form].nfx_postRemove && typeof forms[form].nfx_postRemove == "function") {
				var result1 = forms[form].nfx_postRemove();
				if (result1 == -1) {
					globals.nfx_setBusy(false);
					return -1;
				}
			}

			databaseManager.saveData();
			updatePosition();
		}

		databaseManager.releaseAllLocks();
		databaseManager.setAutoSave(true);
	} else {
		plugins.dialogs.showWarningDialog("Attenzione", "Operazione non consentita su " + form + ".", "Ok");
	}

	globals.nfx_setBusy(false);
	return -1;
}

/**
 *
 * @properties={typeid:24,uuid:"AC1E045D-3B2E-4C3E-A36A-F34A1BACBE2A"}
 */
function removeRelated() {
//	var main = forms.nfx_interfaccia_pannello_body.mainForm;
//	var relatedSelected = forms.nfx_interfaccia_pannello_body.relatedSelected;
//	var menuItems = new Array();
//	if (globals.nfx_canRemoveRecord(main) && forms[main].foundset.getSize() != 0) {
//		menuItems.push(plugins.popupmenu.createMenuItem(globals.nfx_getTitleFormName(main), remove));
//		menuItems[menuItems.length - 1].setMethodArguments(main);
//	}
//	if (relatedSelected && globals.nfx_canRemoveRecord(relatedSelected) && forms[relatedSelected].foundset.getSize() != 0) {
//		menuItems.push(plugins.popupmenu.createMenuItem(globals.nfx_getTitleFormName(relatedSelected), remove));
//		menuItems[menuItems.length - 1].setMethodArguments(relatedSelected);
//	}
//	if (menuItems.length == 0) {
//		menuItems.push(plugins.popupmenu.createMenuItem("Impossibile rimuovere record", null));
//	}
//	plugins.popupmenu.showPopupMenu(elements.remove, menuItems);

	
	//SAuc
	var main = forms.nfx_interfaccia_pannello_body.mainForm;
	var relatedSelected = forms.nfx_interfaccia_pannello_body.relatedSelected;
	var menu = plugins.window.createPopupMenu();
	if (globals.nfx_canRemoveRecord(main) && forms[main].foundset.getSize() != 0) {
		var item = menu.addMenuItem(globals.nfx_getTitleFormName(main),remove);
		item.methodArguments = [main];
		//application.output("Arguments 1 : "+item.methodArguments);
	}
	if (relatedSelected && globals.nfx_canRemoveRecord(relatedSelected) && forms[relatedSelected].foundset.getSize() != 0) {
		var item2 = menu.addMenuItem(globals.nfx_getTitleFormName(relatedSelected),remove);
		item2.methodArguments = [relatedSelected];
		//application.output("Arguments 2 : "+item2.methodArguments);
		}
		if (menu.getItemCount() == 0) {
			var item3 = menu.addMenuItem("Impossibile rimuovere record",null);
			//application.output("Arguments 3 : "+item3.methodArguments);
		}
		menu.show(elements.add);
	
	
}

/**
 *
 * @properties={typeid:24,uuid:"DB84A6A7-3694-4538-B671-3F2049658F6B"}
 */
function removeSpecialButtons() {
	var formName = controller.getName();
	var toRemove = new Array();

	if (elements.sk0) {
		toRemove.push("sk0");
	}
	if (elements.sk1) {
		toRemove.push("sk1");
	}
	if (elements.sk2) {
		toRemove.push("sk2");
	}
	if (elements.sk3) {
		toRemove.push("sk3");
	}
	if (elements.sk4) {
		toRemove.push("sk4");
	}
	if (elements.sk5) {
		toRemove.push("sk5");
	}
	if (elements.sk6) {
		toRemove.push("sk6");
	}
	if (elements.sk7) {
		toRemove.push("sk7");
	}
	if (elements.sk8) {
		toRemove.push("sk8");
	}
	if (elements.sk9) {
		toRemove.push("sk9");
	}
	forms.nfx_interfaccia_pannello_middle.elements.buttons.removeAllTabs();

	var form = solutionModel.getForm(formName);

	for (var i = 0; i < toRemove.length; i++) {
		form.removeLabel(toRemove[i]);
		forms["nfx_interfaccia_pannello_body"][toRemove[i] + "_method"] = null;
	}

	forms.nfx_interfaccia_pannello_middle.elements.buttons.addTab(forms[formName]);

	application.updateUI();
	
try{
	var success = history.removeForm(formName);
	success = solutionModel.removeForm(formName);
	forms[formName].controller.recreateUI();
}catch(e){}
	
//	if (formName != null) {
//		
//		if (success) {
//			
//		}
//		
//		application.output('solutionModel ' + success);
//		if (forms[formName]) {
//
//			application.output('esiste ancora');
//			success = solutionModel.removeForm(formName);
//			//application.closeAllWindows();
//		}
//		
//
//	}

}

/**
 *
 * @properties={typeid:24,uuid:"2025D45E-5374-4B43-9199-7D3978F03177"}
 */
function runValidations(formIN) {
	var form = formIN || workingForm;
	/*
	 * TIPO: function
	 * È possibile defnire un metodo che controlli l'effettiva correttezza dei dati
	 * secondo regole definite, se questo ritorna il valore '-1' l'inserimento fallisce
	 */
	if (forms[form].nfx_validate && typeof forms[form].nfx_validate == "function") {
		var result = forms[form].nfx_validate()
		if (result == -1) {
			return -1;
		}
	}
	/*
	 * TIPO: function
	 * Metodo che definisce le operazioni da compiere appena dopo la valodazione dei dati
	 */
	if (forms[form].nfx_postValidate && typeof forms[form].nfx_postValidate == "function") {
		var result1 = forms[form].nfx_postValidate();
		if (result1 == -1) {
			return -1;
		}
	}
	return 0;
}

/**
 *@param {JSEvent} event
 * @properties={typeid:24,uuid:"771F01F4-4E4C-41CF-904C-F87BA4D71AD2"}
 */
function save(event) {
	if (mode == "add") {
		addSave(null);
		globals.nfx_syncTabs();
		return;
	}
	if (mode == "edit") {
		editSave(null);
		globals.nfx_syncTabs();
		return;
	}
	if (mode == "find") {
		findSave(null);
		return;
	}

	
}

/**
 *
 * @properties={typeid:24,uuid:"67C9F0F9-863B-448C-AD7F-1F060DD205E1"}
 */
function saveCancelMode() {
	for (var i = 0; i < elements.allnames.length; i++) {
		elements[elements.allnames[i]].enabled = false;
	}

	elements.save.enabled = true;
	elements.cancel.enabled = true;
}

/**
 *
 * @properties={typeid:24,uuid:"D75AC2BB-1EB5-45A1-889B-AB5D07611AAC"}
 */
function setSpecialButton(method, icon, tooltip, x, name) {

	var formName = controller.getName();

	forms.nfx_interfaccia_pannello_middle.elements.buttons.removeAllTabs();
	//history.removeForm(formName);

	//forms[formName].controller.recreateUI();

	var container = solutionModel.getForm(formName);

	var button = container.newButton(null, x, 3, 24, 24, globals.nfx_SKmethodProxy);
	button.name = name;
	button.transparent = true;
	button.borderType = 'EmptyBorder, 0, 0, 0, 0';
	button.showClick = false;
	if (method) {
		forms["nfx_interfaccia_pannello_body"][name + "_method"] = method;
	}
	if (icon) {
		button.imageMedia = solutionModel.getMedia(icon);
		button.mediaOptions = SM_MEDIAOPTION.REDUCE;
	}
	if (tooltip) {
		button.toolTipText = tooltip;
	}

	forms.nfx_interfaccia_pannello_middle.elements.buttons.addTab(forms[formName]);

	//application.updateUI();
	try{
	var success = history.removeForm(formName);
	success = solutionModel.removeForm(formName);
	forms[formName].controller.recreateUI();
}catch(e){}
//	if (formName != null) {
//		var success = history.removeForm(formName);
//		if (success) {
//			solutionModel.removeForm(formName);
//		}
//		success = solutionModel.removeForm(formName);
//		//application.output('solutionModel ' + success);
//		if (forms[formName]) {
//
//			application.output('esiste ancora');
//			success = solutionModel.removeForm(formName);
//			application.closeAllWindows();
//		}
//		forms[formName].controller.recreateUI();
//	}
	
	return;

}

/**
 *
 * @properties={typeid:24,uuid:"5C3BB6D2-6DCF-4692-AFCD-59E730B0E8EB"}
 */
function showNavButton() {
	for (var i = 0; i < elements.allnames.length; i++) {
		elements[elements.allnames[i]].visible = true;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"D663907F-0918-49D1-A1C7-5177F5CCF34A"}
 */
function showNavButtonOnlyRelated() {
	hideNavButtons();

	elements.add.visible = true;
	elements.remove.visible = true;

	elements.edit.visible = true;

	elements.xexport.visible = true;

	elements.save.visible = true;
	elements.cancel.visible = true;
}

/**
 *
 * @properties={typeid:24,uuid:"BC6E3C23-A812-46FF-B90E-3417344AF869"}
 */
function silentEditSave(formIN) {
	var form = (formIN) ? formIN : workingForm;

	var valid = runValidations(form);
	if (valid == -1) {
		return -1;
	}
	databaseManager.saveData();
	forms[form].controller.readOnly = true;

	/*
	 * TIPO: function
	 * È possibile defnire un metodo che esegua operazioni successive alla modifica di un record.
	 */
	if (forms[form].nfx_postEdit && typeof forms[form].nfx_postEdit == "function") {
		forms[form].nfx_postEdit();
	}
	
	return 0;
}

/**
 *
 * @properties={typeid:24,uuid:"CD3964CF-E20E-4442-BB4B-A5644266377C"}
 */
function sort() {
	var form = forms.nfx_interfaccia_pannello_body.mainForm

	//SAuc
	//application.showFormInDialog(forms.nfx_interfaccia_popup_sort, null, null, null, null, "Ordinameto", true, false, "sort_popup", true);

	var formq =forms.nfx_interfaccia_popup_sort;
	var window = application.createWindow("sort_popup",JSWindow.MODAL_DIALOG);
	window.title="Ordinamento";
	window.showTextToolbar(false);
	window.resizable=true;
	formq.controller.show(window);
	
	
	if (forms.nfx_interfaccia_popup_sort.orderCriteria) {
		globals.nfx_setBusy(true);
		forms[form].controller.sort(forms.nfx_interfaccia_popup_sort.orderCriteria);
		globals.nfx_setBusy(false);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"F47CCF6C-E541-4CE4-B426-DDA296DDE655"}
 */
function sortDefault() {
	globals.nfx_setBusy(true);

	var form = forms.nfx_interfaccia_pannello_body.mainForm
	/*
	 * TIPO: string ("nome_campo orientamento")
	 * Attibuto che identifica l'eventuale ordinamento base del foundset
	 */
	if (forms[form].nfx_orderBy) {
		forms[form].controller.sort(forms[form].nfx_orderBy);
	} else {
		forms[form].controller.sort(globals.utils_getIdDataProvider(form) + " desc");
	}

	globals.nfx_setBusy(false);
}

/**
 *
 * @properties={typeid:24,uuid:"FAE82FB9-5320-4F1E-8BC0-15A15B1950BF"}
 */
function toDtl() {
	var form = forms.nfx_interfaccia_pannello_body.mainForm
	if (forms[form.replace("_table", "_record")].toViewOnBack)
		forms[form.replace("_table", "_record")].toViewOnBack = forms[form].controller.getSelectedIndex();
	if (forms[form.replace("_table", "_record")].firstVisit)
		forms[form.replace("_table", "_record")].firstVisit = forms[form].firstVisit;
	forms.nfx_interfaccia_pannello_body.elements.main.removeAllTabs();
	forms.nfx_interfaccia_pannello_body.elements.main.addTab(forms[form.replace("_table", "_record")]);
	forms.nfx_interfaccia_pannello_body.mainForm = form.replace("_table", "_record");
}

/**
 *
 * @properties={typeid:24,uuid:"5A85D2F9-8A43-4075-BEDA-7933C8CB0BAD"}
 */
function toTbl() {
	var form = forms.nfx_interfaccia_pannello_body.mainForm
	if (forms[form.replace("_record", "_table")].toViewOnBack)
		forms[form.replace("_record", "_table")].toViewOnBack = forms[form].controller.getSelectedIndex();
	if (forms[form.replace("_record", "_table")].firstVisit)
		forms[form.replace("_record", "_table")].firstVisit = forms[form].firstVisit;
	forms.nfx_interfaccia_pannello_body.elements.main.removeAllTabs();
	forms.nfx_interfaccia_pannello_body.elements.main.addTab(forms[form.replace("_record", "_table")]);
	forms.nfx_interfaccia_pannello_body.mainForm = form.replace("_record", "_table");
}

/**
 *
 * @properties={typeid:24,uuid:"3459E46F-4CAB-4A6A-9E1A-00C56A9D1613"}
 */
function updatePosition() {
	var form = forms.nfx_interfaccia_pannello_body.mainForm;
	if(forms[form])
	elements.position.text = forms[form].controller.getSelectedIndex() + "/" + databaseManager.getFoundSetCount(forms[form].foundset) + ":" + databaseManager.getTableCount(forms[form].foundset);
}

/**
 *
 * @properties={typeid:24,uuid:"CB33357E-39A0-4E40-A2DE-6A617C545334"}
 */
function tryToSavedata() {
	if(!databaseManager.saveData()){
			application.updateUI(500);
			if(!databaseManager.saveData()){
			plugins.dialogs.showErrorDialog("Errore","Errore nel salvataggio","Ok");
				return -1;
			}else{
				return 1;
			}
		}else{
			return 1;
		}

//databaseManager.saveData();
//return 1;

	}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"46FE3ADD-B5A5-43F0-BF7C-0BE357ACC2FC"}
 */
function nfx_helpView(event) {
	if (globals.nfx_getTitleFormName(globals.nfx_selectedProgram()) || globals.nfx_selectedProgram() == "application_main_form") {
		forms.nfx_interfaccia_popup_help.openPopup(false);
	}
}

/**
 * Perform the element right-click action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"C87066A9-8DE4-4DEC-8AE0-235A5E2D746D"}
 */
function nfx_helpEdit(event) {
	var editingHelp = null;
	var messageHelp = "Aggiungere/Modificare sezione \"Help\" per ";
	//Check veramente brutto per vedere se è la home page...
	if (globals.nfx_getTitleFormName(globals.nfx_selectedProgram())) {
		editingHelp = "\"" + globals.nfx_getTitleFormName(globals.nfx_selectedProgram()) + "\"";
		messageHelp += editingHelp + "?";
	} else if (globals.nfx_selectedProgram() == "application_main_form") {
		editingHelp = "Pagina principale";
		messageHelp += "la pagina principale?"
	}
	//Inizio parte di editing
	if (globals["isPowerUser"](globals.nfx_user) && editingHelp) {
		var message = messageHelp;
		if (plugins.dialogs.showQuestionDialog(editingHelp, message, "Sì", "No") == "Sì") {
			forms.nfx_interfaccia_popup_help.openPopup(true);
		}
	}
}
