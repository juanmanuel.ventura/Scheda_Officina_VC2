/**
 *
 * @properties={typeid:24,uuid:"5539283F-EFCA-4B82-8341-EAD96B2112AF"}
 */
function pannelloUpOnLoad() {
	elements.busy.visible = false;
	elements.formName.text = "VIRTUALCAR 2.0 - nfx";
	elements.dateWeek.text = utils.dateFormat(new Date(), "dd-MMM-yyyy") + " - Week: " + utils.dateFormat(new Date(), "w");
}

/**
 *
 * @properties={typeid:24,uuid:"B8B576EE-3D93-41DC-859B-E1740A65F9FA"}
 */
function printFormInfo() {
	//application.output(globals.nfx_getTitleFormName(forms.nfx_interfaccia_pannello_body.mainForm) + " = " + forms.nfx_interfaccia_pannello_body.mainForm);
	if (forms.nfx_interfaccia_pannello_body.relatedSelected) {
		//application.output(globals.nfx_getTitleFormName(forms.nfx_interfaccia_pannello_body.relatedSelected) + " = " + forms.nfx_interfaccia_pannello_body.relatedSelected);
	}
}

/**
 * @properties={typeid:24,uuid:"08EFF424-2926-4A18-9277-9A085657E730"}
 */
function logout() {
	if (plugins.dialogs.showQuestionDialog("Logout?", "Scollegare " + globals.nfx_user + "?", "Sì", "No") == "Sì") {
		security.logout();
	}
}
