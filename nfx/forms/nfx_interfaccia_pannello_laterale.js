/**
 *
 * @properties={typeid:24,uuid:"0998D8D4-313C-47CE-A0DC-D6BA9B5EA696"}
 */
function setSplitPane() {

	//SAuc

	var _context = controller.getFormContext();
	/** @type {String} */
	var _window = _context.getValue(1, 1);
	var _windowObject = application.getWindow(_window);

	var height = _windowObject.getHeight();

	//
	//	var delta = (Math.round(application.getWindowHeight() / 3) > 300) ? Math.round(application.getWindowHeight() / 3) : 300;
	//	var location = application.getWindowHeight() - delta;
	//	globals.utils_setSplitPane({   splitpane : elements.split,
	//								   leftComponent : elements.tree,
	//								   rightComponent : elements.gadgets,
	//								   resizeWeight : 1,
	//								   dividerLocation : location,
	//								   orientation : 0,
	//								   dividerSize : 1,
	//								   continuousLayout : 0});

	var delta = (Math.round(height / 3) > 300) ? Math.round(height / 3) : 300;
	var location = height - delta;
	globals.utils_setSplitPane({
		splitpane: elements.split,
		leftComponent: elements.tree,
		rightComponent: elements.gadgets,
		resizeWeight: 1,
		dividerLocation: location,
		orientation: 0,
		dividerSize: 1,
		continuousLayout: 0
	});

}
