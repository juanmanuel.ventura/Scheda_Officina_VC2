dataSource:"db:/ferrari/k8_dummy",
items:[
{
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"127B464F-994F-4572-810C-24FBC185DB64",
location:"10,3",
mediaOptions:2,
name:"back",
onActionMethodID:"8BC0B989-BE5F-43CF-8DC3-F54C0DF14BC2",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"10055532-1FA3-4A2C-A01C-97B88EA2E0F0"
},
{
anchors:11,
borderType:"SpecialMatteBorder,1.0,0.0,0.0,0.0,#696969,#000000,#000000,#000000,0.0,",
horizontalAlignment:4,
location:"0,29",
mediaOptions:14,
name:"c",
size:"100,8",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"2D4D37F8-458E-4952-9725-708CDBDA45A4"
},
{
anchors:15,
beanClassName:"javax.swing.JTree",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_16\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JTree\"> \
  <object class=\"javax.swing.tree.DefaultTreeModel\"> \
   <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
    <void property=\"userObject\"> \
     <string>JTree<\/string> \
    <\/void> \
    <void method=\"add\"> \
     <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
      <void property=\"userObject\"> \
       <string>colors<\/string> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>blue<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>violet<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>red<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>yellow<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
     <\/object> \
    <\/void> \
    <void method=\"add\"> \
     <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
      <void property=\"userObject\"> \
       <string>sports<\/string> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>basketball<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>soccer<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>football<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>hockey<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
     <\/object> \
    <\/void> \
    <void method=\"add\"> \
     <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
      <void property=\"userObject\"> \
       <string>food<\/string> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>hot dogs<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>pizza<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>ravioli<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>bananas<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
     <\/object> \
    <\/void> \
   <\/object> \
  <\/object> \
  <void property=\"background\"> \
   <object class=\"java.awt.Color\"> \
    <int>216<\/int> \
    <int>234<\/int> \
    <int>254<\/int> \
    <int>255<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"cellRenderer\"> \
   <void property=\"location\"> \
    <object class=\"java.awt.Point\"> \
     <int>-45<\/int> \
     <int>-20<\/int> \
    <\/object> \
   <\/void> \
  <\/void> \
  <void property=\"name\"> \
   <string>tree<\/string> \
  <\/void> \
  <void property=\"rootVisible\"> \
   <boolean>false<\/boolean> \
  <\/void> \
  <void property=\"rowHeight\"> \
   <int>20<\/int> \
  <\/void> \
  <void property=\"showsRootHandles\"> \
   <boolean>true<\/boolean> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:2,
location:"10,50",
name:"tree",
size:"80,80",
typeid:12,
uuid:"56ECE4E4-73F7-426A-B409-FA2CD9362B40"
},
{
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"40905D44-A323-4D4C-9CCA-CC832C1E545D",
location:"40,3",
mediaOptions:2,
name:"next",
onActionMethodID:"9C617020-35BF-45CB-ACF6-FAEA3C08F669",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"78032150-0668-43AF-82BB-E39C7EA4ECBD"
},
{
background:"#d8eafe",
height:140,
partType:5,
typeid:19,
uuid:"86178F81-7A24-494A-AAE0-2E35AB1A6C3F"
},
{
anchors:11,
formIndex:1,
imageMediaID:"06681961-53B4-4528-A0D1-33E5DDCAC16F",
location:"0,30",
mediaOptions:6,
size:"100,10",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"86D28000-6ACF-4B3B-8857-82B9C83638DA"
},
{
anchors:15,
beanClassName:"javax.swing.JScrollPane",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_16\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JScrollPane\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>230<\/int> \
    <int>100<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <object class=\"javax.swing.border.EmptyBorder\"> \
    <object class=\"java.awt.Insets\"> \
     <int>0<\/int> \
     <int>0<\/int> \
     <int>0<\/int> \
     <int>0<\/int> \
    <\/object> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>scroll<\/string> \
  <\/void> \
  <void property=\"opaque\"> \
   <boolean>false<\/boolean> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"0,37",
name:"scroll",
size:"100,103",
typeid:12,
uuid:"9EA3CD4E-8B58-436F-8D12-CDE36001AA0E"
},
{
anchors:11,
formIndex:-1,
imageMediaID:"DC52426B-B15E-47B6-A325-65212EF6567C",
location:"0,0",
mediaOptions:6,
name:"b",
size:"100,29",
tabSeq:-1,
typeid:7,
uuid:"C0C7154B-6195-4BC0-B54F-B740B485D2B3"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
horizontalAlignment:0,
imageMediaID:"4E205FA0-C666-48F9-A27F-935D355C24CD",
location:"70,3",
mediaOptions:2,
name:"configurator",
onActionMethodID:"E9C4977E-4561-49ED-84AF-A31A1B65BE46",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"D85190B3-8E2A-4B2E-913A-782A8C003532",
verticalAlignment:0
}
],
name:"nfx_interfaccia_pannello_tree",
navigatorID:"-1",
onLoadMethodID:"D113557D-ACBB-424D-97E4-24E0CAEC6339",
onShowMethodID:"BDB06761-4098-42E0-A797-B08A206B2BCB",
paperPrintScale:100,
scrollbars:36,
showInMenu:true,
size:"100,140",
styleName:"keeneight",
typeid:3,
uuid:"15E4175B-6E87-4489-BA4B-292E315D8E18"