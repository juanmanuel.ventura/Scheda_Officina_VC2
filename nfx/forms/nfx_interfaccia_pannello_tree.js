/**
 * @properties={typeid:35,uuid:"26A4423E-9D4F-42F0-BF33-97DE9989FFE9",variableType:-4}
 */
var names = new Array();

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"802FD25F-E714-4BA3-AE91-009D6A1BBBC2"}
 */
var next = null;

/**
 * @properties={typeid:35,uuid:"686757ED-6239-459C-83EC-D08BE1BA7F59",variableType:-4}
 */
var pannelloSxLoaded = false;

/**
 * @properties={typeid:35,uuid:"4E9CDD3D-88FE-4803-A027-6705DADFDAB8",variableType:-4}
 */
var permissions = new Array();

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"85CF64FD-BDBA-4EA0-96A4-2A690B5B58FD"}
 */
var previous = null;

/**
 * @properties={typeid:35,uuid:"C4AB4A61-377B-4B96-BD38-31628FB26CBC",variableType:-4}
 */
var programs = new Array();

/**
 * @properties={typeid:35,uuid:"68C31F12-B875-4DE3-B7DE-F6AB548A50CF",variableType:-4}
 */
var titles = new Array();

/**
 *
 * @properties={typeid:24,uuid:"E9C4977E-4561-49ED-84AF-A31A1B65BE46"}
 */
function customize() {

	//application.showFormInDialog(forms.nfx_interfaccia_popup_filltree,null,null,null,null,"Personalizza albero",true,false,"tree_popup",true);

	var formq = forms.nfx_interfaccia_popup_filltree;
	var window = application.createWindow("Personalizza albero", JSWindow.MODAL_DIALOG);
	window.title = "Personalizza albero";
	window.showTextToolbar(false);
	window.resizable = true;
	formq.controller.show(window);

	if (forms.nfx_interfaccia_popup_filltree.filltree) {
		fillTree(null, null);

		forms.nfx_interfaccia_pannello_body.pannelloDxOnLoad();
		forms.nfx_interfaccia_pannello_header.pannelloUpOnLoad();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"9A07F1B7-5781-4D75-B134-91A2CCBFF449"}
 */
function fillTree(nodein, tre) {
	//	JStaffa tolto javaimporter
	//	var imported = new JavaImporter(Packages.javax.swing.tree);
	/** @type {javax.swing.tree.DefaultTreeModel} */
	var model = elements.tree.getModel();

	/** @type {javax.swing.tree.DefaultMutableTreeNode} */
	var currentNode = null

	if (nodein) {
		currentNode = nodein;
	} else {
		currentNode = model.getRoot();
		currentNode.removeAllChildren();
	}
	//	JStaffa aggiunta dichiarazione alla variabile
	/** @type {javax.swing.tree.DefaultMutableTreeNode} */
	var treeValues = tre || forms.nfx_interfaccia_popup_filltree.choices;

	/** @type {javax.swing.tree.DefaultMutableTreeNode} */
	var node = null;

	for (var i in treeValues.children) {

		if (treeValues.children[i].type == "folder") {
			//			JStaffa
			//			with (imported) {
			//				node = new DefaultMutableTreeNode(treeValues.children[i].name);
			node = new Packages.javax.swing.tree.DefaultMutableTreeNode(treeValues.children[i].name);
			//			}

			fillTree(node, treeValues.children[i]);
		} else {
			//			JStaffa
			//			with (imported) {
			//				node = new DefaultMutableTreeNode( (treeValues.children[i].alias) ? treeValues.children[i].alias : globals.nfx_getTitleFormName(treeValues.children[i].name));
			node = new Packages.javax.swing.tree.DefaultMutableTreeNode( (treeValues.children[i].alias) ? treeValues.children[i].alias : globals.nfx_getTitleFormName(treeValues.children[i].name));
			//			}
		}

		model.insertNodeInto(node, currentNode, currentNode.getChildCount());
		model.reload();
	}
	model.reload();
}

/**
 *
 * @properties={typeid:24,uuid:"C8E45BC8-5B91-4E7B-8EF8-2B8AB9F773FD"}
 */
function getLabel(tree, value, isSelected, isExpanded, isLeaf, row, cellHasFocus) {

	var url = null;

	url = new java.net.URL("media:///treefolder.png");
	var folder = new Packages.javax.swing.ImageIcon(url);
	url = new java.net.URL("media:///treeleaf.png");
	var leaf = new Packages.javax.swing.ImageIcon(url);

	var icon = (isLeaf) ? leaf : folder;

	var label = new Packages.javax.swing.JLabel(value, icon, 2);
	if (isSelected) {
		label.setForeground(java.awt.SystemColor.textHighlightText);
		label.setBackground(java.awt.SystemColor.textHighlight);
		label.setOpaque(true);
	} else {
		label.setForeground(java.awt.SystemColor.textText);
		label.setBackground(java.awt.SystemColor.text);
		label.setOpaque(false);
	}

	return label;
}

/**
 *
 * @properties={typeid:24,uuid:"8BC0B989-BE5F-43CF-8DC3-F54C0DF14BC2"}
 */
function goBack() {

	if (previous) {
		globals.nfx_setBusy(true);
		application.updateUI();
		next = forms.nfx_interfaccia_pannello_body.elements.main.getTabFormNameAt(1);

		//  JStaffa inizio
		globals.nextTree = next;
		//  JStaffa fine

		globals.nfx_goTo(previous);
		previous = null;
	globals.nfx_setBusy(false);
	application.updateUI();		
	}

	return;
}

/**
 *
 * @properties={typeid:24,uuid:"9C617020-35BF-45CB-ACF6-FAEA3C08F669"}
 */
function goForward() {
	
	// JStaffa
	// if (next) {
	//  previous = forms.nfx_interfaccia_pannello_body.elements.main.getTabFormNameAt(1);
	//  globals.nfx_goTo(next);
	//  next = null;
	// }
	if (globals.nextTree) {
		globals.nfx_setBusy(true);
		application.updateUI();
		previous = forms.nfx_interfaccia_pannello_body.elements.main.getTabFormNameAt(1);
		globals.nfx_goTo(globals.nextTree);
		globals.nextTree = null;
		globals.nfx_setBusy(false);
		application.updateUI();
	}


	return;
}

/**
 *
 * @properties={typeid:24,uuid:"F247513D-C8E4-49F2-B666-B4E982A7F71D"}
 */
function goTo(name) {
	globals.nfx_setBusy(true);
	forms.nfx_interfaccia_pannello_body.elements.main.removeAllTabs();
	forms.nfx_interfaccia_pannello_body.mainForm = name;
	forms.nfx_interfaccia_pannello_body.elements.main.addTab(forms[name]);

}

/**
 *
 * @properties={typeid:24,uuid:"D113557D-ACBB-424D-97E4-24E0CAEC6339"}
 */
function pannelloSxOnLoad() {

	//SAuc
	//var e=forms.allnames;
	var array = solutionModel.getForms();

	var nomi = new Array();

	for (var e = 0; e < array.length; e++) {
		nomi[e] = array[e].name;
	}

	elements.tree.setCellRenderer(globals.nfx_getTreeIconRenderer());

	var ds = databaseManager.getDataSetByQuery("nfx", "select nome from nfx_forms", [], -1);
	var fs = databaseManager.getFoundSet("nfx", "nfx_forms");

	//	var namesFromDB = ds.getColumnAsArray(1);
	//	for(var i=0;i<forms.allnames.length;i++){
	//		if(namesFromDB.indexOf(forms.allnames[i]) == -1){
	//			fs.newRecord();
	//			fs.nome = forms.allnames[i];
	//			fs.titolo = (forms[forms.allnames[i]].nfx_getTitle && typeof forms[forms.allnames[i]].nfx_getTitle == "function") ? forms[forms.allnames[i]].nfx_getTitle() : null;
	//			fs.principale = (forms[forms.allnames[i]].nfx_isProgram && typeof forms[forms.allnames[i]].nfx_isProgram == "function" && forms[forms.allnames[i]].nfx_isProgram()) ? 1 : 0;
	//			fs.permessi = (forms[forms.allnames[i]].nfx_defaultPermissions && typeof forms[forms.allnames[i]].nfx_defaultPermissions == "function") ? forms[forms.allnames[i]].nfx_defaultPermissions() : "%";
	//			databaseManager.saveData();
	//		}
	//	}

	var namesFromDB = ds.getColumnAsArray(1);
	
//	var rem = new Array();
//	rem.push("schede_collaudo_record");
//	
//	for(var i=0;i<rem.length;i++){
//		var torem=rem[i];
//		for(var i2=0;i2<namesFromDB.length;i2++){
//			
//		}
//	}
	
	
	for (var i = 0; i < nomi.length; i++) {
		if (namesFromDB.indexOf(nomi[i]) == -1) {
			fs.newRecord();
			fs.nome = nomi[i];
			fs.titolo = (forms[nomi[i]].nfx_getTitle && typeof forms[nomi[i]].nfx_getTitle == "function") ? forms[nomi[i]].nfx_getTitle() : null;
			fs.principale = (forms[nomi[i]].nfx_isProgram && typeof forms[nomi[i]].nfx_isProgram == "function" && forms[nomi[i]].nfx_isProgram()) ? 1 : 0;
			fs.permessi = (forms[nomi[i]].nfx_defaultPermissions && typeof forms[nomi[i]].nfx_defaultPermissions == "function") ? forms[nomi[i]].nfx_defaultPermissions() : "%";
			databaseManager.saveData();
		}
	}

	updateFormsInfo();

	var vp = elements.scroll.getViewport();
	//SAuc
	//vp.add(elements.tree);
	vp.add(java.awt.Component(elements.tree));

	var model = elements.tree.getModel();
	/** @type {javax.swing.tree.DefaultMutableTreeNode} */
	var root = model.getRoot();
	root.removeAllChildren();
	//FS
	/** @type {javax.swing.event.TreeSelectionListener} */
	var showFormAsTreeSelectionListener = showForm;
	//dep elements.tree.addTreeSelectionListener(showForm);
	elements.tree.addTreeSelectionListener(showFormAsTreeSelectionListener);
}

/**
 * @properties={typeid:24,uuid:"9A3F5EB6-C28A-4728-AB37-8EB9025B7F8B"}
 */
function updateFormsInfo() {
	var dsUpdated = databaseManager.getDataSetByQuery("nfx", "select nome,titolo,principale,permessi from nfx_forms", [], -1);
	names = dsUpdated.getColumnAsArray(1);
	titles = dsUpdated.getColumnAsArray(2);
	programs = dsUpdated.getColumnAsArray(3);
	permissions = dsUpdated.getColumnAsArray(4);
}

/**
 * @properties={typeid:24,uuid:"BDB06761-4098-42E0-A797-B08A206B2BCB"}
 */
function pannelloSxOnShow() {

	//SAuc
	//var cantCustomize = databaseManager.getDataSetByQuery(controller.getServerName(), "select CANT_CUSTOMIZE_TREE from K8_GESTIONE_UTENZE where USER_SERVOY = ?", [globals.nfx_user], 1);

	var srv = controller.getDataSource().split('/')[1];
	var cantCustomize = databaseManager.getDataSetByQuery(srv, "select CANT_CUSTOMIZE_TREE from K8_GESTIONE_UTENZE where USER_SERVOY = ?", [globals.nfx_user], 1);

	if (cantCustomize.getValue(1, 1)) {
		elements.configurator.visible = false;
	}
	globals.nfx_navigatorTreeAutoUpdate();
	fillTree(null, null);

	pannelloSxLoaded = true;
}

/**
 * @properties={typeid:24,uuid:"3994DCD5-1A17-4F9C-8BDA-894506BC4D0A"}
 */
function showForm() {
	
	globals.nfx_setBusy(true);
	
	globals.nfx_setUserActive();
	
	globals.openFormTime = null;
	globals.openFormTime = new Date();
	

	var selectionPath = elements.tree.getSelectionPath();
	var path = (selectionPath) ? selectionPath.getPath() : null;

	var treeObj = forms.nfx_interfaccia_popup_filltree.getObject(null, null, path);
	
	if (treeObj && treeObj.type == "node") {
		
		if(treeObj.name == "schede_officina_record" ||
		treeObj.name == "schede_officina_table" ||
		treeObj.name == "schede_collaudo_record" ||
		treeObj.name == "schede_collaudo_table" ||
		treeObj.name == "anagrafica_coefficienti_table" ||
		treeObj.name == "anagrafica_coefficienti_record" ||
		treeObj.name == "anagrafica_disegni_baan_cqc_table" ||
		treeObj.name == "veicolo_montaggio_vetture" ||
		treeObj.name == "montaggio_vetture_container_s5" ){
		plugins.dialogs.showInfoDialog("Impossibile","Form disabilitato! (Ti consigliamo di rimuoverlo dall'albero di navigazione) sarai reindirizzato al form precedente", "Ok");
		previous = forms.nfx_interfaccia_pannello_body.elements.main.getTabFormNameAt(1);
		if(previous != null){
		goTo(previous);}
		try{
		elements.tree.setSelectionPath(null);
		}catch(Exc){}
		next = null;
		globals.nfx_setBusy(false);
		return;
	}
		
		
		
		
		if (forms.nfx_interfaccia_pannello_body.elements.main.getMaxTabIndex() != 0)
			previous = forms.nfx_interfaccia_pannello_body.elements.main.getTabFormNameAt(1);
		goTo(treeObj.name);
		next = null;
	}
	globals.nfx_setBusy(false);
	return;
}

/**
 * @param formName
 * @properties={typeid:24,uuid:"580E0648-253A-4A67-8E4F-E51E2FDEE77B"}
 */
function updateUI(formName) {
	history.removeForm(formName);
	solutionModel.removeForm(formName);
	forms[formName].foundset.loadAllRecords();
	application.updateUI();
//	if (formName != null) {
//		var success = history.removeForm(formName);
//		if (success) {
//			solutionModel.removeForm(formName);
//		}
//		success = solutionModel.removeForm(formName);
//		//application.output('solutionModel ' + success);
//		if (forms[formName]) {
//
//			//application.output('esiste ancora');
//			success = solutionModel.removeForm(formName);
//			//application.closeAllWindows();
//		}
//		application.updateUI();
//		forms[formName].foundset.loadAllRecords();
//		//forms[formName].controller.recreateUI();
//
//	}
}
