dataSource:"db:/ferrari/k8_dummy",
items:[
{
anchors:7,
beanClassName:"javax.swing.JScrollPane",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_17\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JScrollPane\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>260<\/int> \
    <int>230<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <object class=\"javax.swing.border.EtchedBorder\"> \
    <int>0<\/int> \
    <null/> \
    <null/> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>scrollChoices<\/string> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:-2,
location:"260,30",
name:"scrollChoices",
size:"260,230",
typeid:12,
uuid:"0E2D81D5-6C03-45D2-9380-3B11421B84D3"
},
{
beanClassName:"javax.swing.JList",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.5.0_19\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JList\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>listChoices<\/string> \
  <\/void> \
  <void property=\"selectionMode\"> \
   <int>0<\/int> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"330,120",
name:"listChoices",
size:"80,80",
typeid:12,
uuid:"114A8E87-2779-4213-A8E5-DF46A8E59003"
},
{
anchors:6,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"85582DF7-017C-4741-98E4-7C3C1142DF33",
location:"460,267",
mediaOptions:2,
onActionMethodID:"52E66E17-36C2-48A4-969F-D41926C6CEC5",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"177DF307-9192-4630-AF59-A8DDA43F4487"
},
{
anchors:6,
imageMediaID:"A7F75852-4358-41CF-98A5-8DE77DB9D239",
location:"300,267",
mediaOptions:14,
onActionMethodID:"5587EED6-1E5D-420C-A5E6-E36D95E9C2AC",
showClick:false,
size:"24,24",
toolTipText:"Carica selezione",
transparent:true,
typeid:7,
uuid:"3E776CB0-4025-47BA-BF14-C7BE1B121AB4"
},
{
location:"10,10",
mediaOptions:14,
size:"80,20",
tabSeq:-1,
text:"Opzioni",
transparent:true,
typeid:7,
uuid:"5DB0C295-C495-471A-AF53-57DA7B7524C4"
},
{
anchors:3,
location:"260,10",
mediaOptions:14,
size:"80,20",
tabSeq:-1,
text:"Scelte",
transparent:true,
typeid:7,
uuid:"65471B7B-8C30-483D-8DFB-FF932C5FD9BE"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"E6C052AF-2185-445F-932A-5F615B6333EC",
location:"228,200",
mediaOptions:2,
onActionMethodID:"C347E083-2337-47E5-BF3F-7F468C2C361F",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"7EBC60FA-3469-47BE-8A47-5EF5D04BED21"
},
{
anchors:6,
imageMediaID:"834DCE94-EDB6-434B-966C-90C204918A70",
location:"270,267",
mediaOptions:10,
onActionMethodID:"AB537140-5E8B-4108-BDBC-748233FBFCF7",
showClick:false,
size:"24,24",
toolTipText:"Salva selezione",
transparent:true,
typeid:7,
uuid:"7EFFAC30-920C-4E05-B479-7C608D3F3CB0"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"6AD2E909-E7A4-491A-92E5-D7E2F229B865",
location:"228,160",
mediaOptions:2,
onActionMethodID:"A5A56697-6266-4FD0-AD4A-415D83C2E250",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"9E594326-E25E-46FD-ABED-766DD1776A84"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"127B464F-994F-4572-810C-24FBC185DB64",
location:"228,90",
mediaOptions:2,
onActionMethodID:"2466D5AD-A5DD-409C-BD82-466630A3D941",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"ACB6C19C-81CD-4E55-AF3B-73874BEA02AB"
},
{
beanClassName:"javax.swing.JList",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.5.0_19\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JList\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>listOptions<\/string> \
  <\/void> \
  <void property=\"selectionMode\"> \
   <int>0<\/int> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"70,120",
name:"listOptions",
size:"80,80",
typeid:12,
uuid:"BB844132-EC20-4D51-AF21-73D828827D0B"
},
{
anchors:6,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"0746A208-4E27-45D4-BABE-FB96F24D516F",
location:"490,267",
mediaOptions:2,
onActionMethodID:"DECF0147-7DDA-44AD-ABFC-4F8564299F0B",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"C2A072B9-6BA7-4819-8A9D-CAEA476DA525"
},
{
height:300,
partType:5,
typeid:19,
uuid:"CB786742-73D0-4715-80D6-5F7CC9E15977"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"5804BE64-BD66-4BC7-B44A-3314D54EA001",
location:"228,130",
mediaOptions:2,
onActionMethodID:"ADF7FC82-D50D-4716-87D3-2E3BE649EEDB",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"CCF7B91B-8C18-4E6D-A296-792EB02EC0F1"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"40905D44-A323-4D4C-9CCA-CC832C1E545D",
location:"228,60",
mediaOptions:2,
onActionMethodID:"420DB0C3-F80A-4781-AFCD-980E6FBA2EBB",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"CE5B60FC-1B0D-494C-8875-4E4280B3A719"
},
{
anchors:15,
beanClassName:"javax.swing.JScrollPane",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_17\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JScrollPane\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>210<\/int> \
    <int>230<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <object class=\"javax.swing.border.EtchedBorder\"> \
    <int>0<\/int> \
    <null/> \
    <null/> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>scrollOptions<\/string> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:-1,
location:"10,30",
name:"scrollOptions",
size:"210,230",
typeid:12,
uuid:"E5AFEFA8-E7D2-4066-BC11-B3B394B7B2E5"
}
],
name:"nfx_interfaccia_popup_excel",
navigatorID:"-1",
onHideMethodID:"30425BE2-BE61-42E8-86BD-86EE75A20C12",
onLoadMethodID:"D13B3CFB-79CF-4C32-BD94-1B9F013E7E9C",
onShowMethodID:"A3398B38-695B-4C9A-B0EB-A22BA8DF0225",
paperPrintScale:100,
showInMenu:true,
size:"530,300",
styleName:"keeneight",
typeid:3,
uuid:"EB063A1E-C4E6-42E3-8734-F84D4204AD1F"