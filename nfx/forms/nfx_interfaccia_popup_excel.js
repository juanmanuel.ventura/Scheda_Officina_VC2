/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"139ECFA8-FC63-46BB-A54B-4C1F1B5A361E"}
 */
var canExport = null;

/**
 * @properties={typeid:35,uuid:"45F5A64C-DCC2-4D44-B3D3-AC58FAE57037",variableType:-4}
 */
var choices = [];

/**
 * @properties={typeid:35,uuid:"4EF6D5C8-7085-4957-96DB-F72DF33C1C54",variableType:-4}
 */
var choicesBK = new Array();

/**
 * @properties={typeid:35,uuid:"9C5F4D0E-B980-4B19-8876-14B831C47564",variableType:-4}
 */
var labels = new Array();

/**
 * @properties={typeid:24,uuid:"07B8F90D-CBD6-425C-913E-BF5E53815B2F"}
 */
function dcAdd(e) {
	if (e.getClickCount() == 2) {
		add();
	}
}

/**
 * @properties={typeid:24,uuid:"891F2548-F6DF-4CB3-95B3-049796FC9431"}
 */
function dcRemove(e) {
	if (e.getClickCount() == 2) {
		remove();
	}
}

/**
 * @properties={typeid:24,uuid:"D13B3CFB-79CF-4C32-BD94-1B9F013E7E9C"}
 */
function onLoad() {
	elements.listOptions.addMouseListener(java.awt.event.MouseListener(dcAdd));
	elements.listChoices.addMouseListener(java.awt.event.MouseListener(dcRemove));
}

/**
 * @properties={typeid:24,uuid:"A3398B38-695B-4C9A-B0EB-A22BA8DF0225"}
 * @AllowToRunInFind
 */
function onShow() {

	canExport = null;

	choicesBK = choices.slice(null, null);

	var vpOptions = elements.scrollOptions.getViewport();
	vpOptions.add(java.awt.Component(elements.listOptions));

	var vpChoices = elements.scrollChoices.getViewport();
	vpChoices.add(java.awt.Component(elements.listChoices));

	labels = getFormLabels(globals.nfx_selectedProgram());

	elements.listOptions.setListData(getArraryLabels(labels, true));
	elements.listChoices.setListData(getArraryLabels(choices, false));

	/*
	 Commentato, che è furbo avere nel codice centinaia di righe commentate,
	 così chi poi legge il codice non ha difficoltà...

	 canExport = null;

	 choicesBK = choices.slice();

	 var vpOptions = elements.scrollOptions.getViewport();
	 vpOptions.add(elements.listOptions);

	 var vpChoices = elements.scrollChoices.getViewport();
	 vpChoices.add(elements.listChoices);

	 var form = forms.nfx_interfaccia_pannello_body.elements.main.getTabFormNameAt(1);

	 labels = new Array();
	 for(var i=0;i<forms[form].alldataproviders.length;i++)
	 {
	 if(forms[form].alldataproviders[i].search("id") == -1
	 && forms[form].alldataproviders[i].search("pk") == -1
	 && forms[form].alldataproviders[i].search("fk") == -1
	 && forms[form].alldataproviders[i].search("path") == -1
	 && forms[form].alldataproviders[i].search("figli") == -1
	 && forms[form].alldataproviders[i].search("timestamp") == -1
	 && forms[form].alldataproviders[i].search("conto") == -1
	 && forms[form].alldataproviders[i].search("doc_count") == -1
	 && forms[form].alldataproviders[i].search("utente_creazione") == -1
	 && forms[form].alldataproviders[i].search("utente_modifica") == -1
	 && forms[form].alldataproviders[i].search("livello") == -1
	 && forms[form].alldataproviders[i].search("project") == -1
	 && forms[form].alldataproviders[i].search("str") == -1
	 && forms[form].alldataproviders[i].search("tableName") == -1
	 && forms[form].alldataproviders[i].search("icon") == -1
	 && forms[form].alldataproviders[i].search("tree") == -1
	 && forms[form].alldataproviders[i].search("k8") == -1){
	 var label = forms[form].alldataproviders[i];
	 label = label.replace(/_/g," ")
	 label = label.substr(0,1).toUpperCase() + label.substr(1);
	 labels.push(label);
	 }
	 }

	 elements.listOptions.setListData(labels);
	 elements.listChoices.setListData(choices);
	 */
}

/**
 * @properties={typeid:24,uuid:"30425BE2-BE61-42E8-86BD-86EE75A20C12"}
 */
function onHide() {
	if (!canExport) {
		choices = choicesBK.slice(null, null);
	}
}

/**
 * @properties={typeid:24,uuid:"9BEF85AF-FE2D-4BBC-8544-5EC0F950B836"}
 */
function getFormLabels(form) {
	var formFields = forms[form].elements.allnames;
	var formLabels = solutionModel.getForm(form).getLabels();
	var tempLabels = new Array()
	for (var i = 0; i < formFields.length; i++) {
		for (var j = 0; j < formLabels.length; j++) {
			if (formFields[i] == formLabels[j].labelFor && formLabels[j].text) {
				tempLabels.push({
					label: formLabels[j].text,
					field: forms[form].elements[formFields[i]].getDataProviderID(),
					format: forms[form].elements[formFields[i]].format
				});
			}
		}
	}
	return tempLabels;
}

/**
 * @properties={typeid:24,uuid:"9D575C68-3292-4A66-8364-3E215EE25018"}
 */
function getArraryLabels(a, sort) {
	var ls = new Array();
	for (var i = 0; i < a.length; i++) {
		ls.push(a[i].label);
	}
	return (sort) ? ls.sort() : ls;
}

/**
 * @properties={typeid:24,uuid:"AD5C8C2C-9B1E-499B-8ED6-90F3B80EBC73"}
 */
function getObj(l, a) {
	for (var i = 0; i < a.length; i++) {
		if (a[i].label == l) {
			return a[i];
		}
	}
	return null;
}

/**
 * @properties={typeid:24,uuid:"420DB0C3-F80A-4781-AFCD-980E6FBA2EBB"}
 */
function add() {
	if (elements.listOptions.getSelectedValue() && !getObj(elements.listOptions.getSelectedValue(), choices)) {
		choices.push(getObj(elements.listOptions.getSelectedValue(), labels));
		elements.listChoices.setListData(getArraryLabels(choices, false));
	}
}

/**
 * @properties={typeid:24,uuid:"2466D5AD-A5DD-409C-BD82-466630A3D941"}
 */
function remove() {
	if (elements.listChoices.getSelectedValue()) {
		choices.splice(choices.indexOf(getObj(elements.listChoices.getSelectedValue(), choices)), 1);
		elements.listChoices.setListData(getArraryLabels(choices, false));
	}
}

/**
 * @properties={typeid:24,uuid:"A5A56697-6266-4FD0-AD4A-415D83C2E250"}
 */
function moveDown() {
	if (elements.listChoices.getSelectedValue() && choices.indexOf(getObj(elements.listChoices.getSelectedValue(), choices)) != choices.length - 1) {
		var index = choices.indexOf(getObj(elements.listChoices.getSelectedValue(), choices));
		var app = choices[index + 1];
		choices[index + 1] = choices[index];
		choices[index] = app;
		elements.listChoices.setListData(getArraryLabels(choices, false));
		elements.listChoices.setSelectedIndex(index + 1);
	}
}

/**
 * @properties={typeid:24,uuid:"ADF7FC82-D50D-4716-87D3-2E3BE649EEDB"}
 */
function moveUp() {
	if (elements.listChoices.getSelectedValue() && choices.indexOf(getObj(elements.listChoices.getSelectedValue(), choices)) != 0) {
		var index = choices.indexOf(getObj(elements.listChoices.getSelectedValue(), choices));
		var app = choices[index - 1];
		choices[index - 1] = choices[index];
		choices[index] = app;
		elements.listChoices.setListData(getArraryLabels(choices, false));
		elements.listChoices.setSelectedIndex(index - 1);
	}
}

/**
 * @properties={typeid:24,uuid:"C347E083-2337-47E5-BF3F-7F468C2C361F"}
 */
function clear() {
	choices = new Array();
	elements.listChoices.setListData(getArraryLabels(choices, false));
}

/**
 * @properties={typeid:24,uuid:"52E66E17-36C2-48A4-969F-D41926C6CEC5"}
 */
function save() {
	canExport = true;

	//application.closeFormDialog();

	var w = controller.getWindow();
	w.destroy();
}

/**
 * @properties={typeid:24,uuid:"DECF0147-7DDA-44AD-ABFC-4F8564299F0B"}
 */
function cancel() {
	canExport = null;

	//application.closeFormDialog();

	var w = controller.getWindow();
	w.destroy();
}

/**
 * @properties={typeid:24,uuid:"AB537140-5E8B-4108-BDBC-748233FBFCF7"}
 */
function saveSelection() {
	forms.nfx_interfaccia_gestione_salvataggi.save(choices, "export_" + globals.nfx_selectedProgram());
}

/**
 * @properties={typeid:24,uuid:"5587EED6-1E5D-420C-A5E6-E36D95E9C2AC"}
 */
function loadSelection() {
	var value = forms.nfx_interfaccia_gestione_salvataggi.load("export_" + globals.nfx_selectedProgram());
	if (value) {
		choicesBK = choices.slice(null, null);
		choices = value;
		elements.listChoices.setListData(getArraryLabels(choices, false));
	}
}
