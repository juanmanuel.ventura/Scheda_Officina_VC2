dataSource:"db:/ferrari/k8_dummy",
items:[
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"40905D44-A323-4D4C-9CCA-CC832C1E545D",
location:"228,60",
mediaOptions:2,
onActionMethodID:"9DF08706-53BF-465D-98D1-1C607717364B",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"04399D7A-1687-4CB4-B362-F6447E56E120"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"E6C052AF-2185-445F-932A-5F615B6333EC",
location:"228,200",
mediaOptions:2,
onActionMethodID:"47C3F4D1-ACCB-4C08-978E-3FC7F9FCB2B7",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"0D80294E-D8E6-4D68-ABC5-147817C4C3A6"
},
{
anchors:6,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"203C0E85-25E0-4CAE-82F1-D30DEC01ABDE",
location:"201,271",
mediaOptions:2,
onActionMethodID:"50C5896B-3CBF-4BAA-BDEF-E5D7CE6519D1",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"20,20",
transparent:true,
typeid:7,
uuid:"18AB331E-6F0E-4FE6-9352-07628E3B4009"
},
{
anchors:14,
borderType:"EtchedBorder,0,null,null",
formIndex:-4,
lineSize:1,
location:"8,268",
shapeType:1,
size:"214,24",
transparent:true,
typeid:21,
uuid:"1A47E783-5158-4EA1-A386-DA6B05A1464C"
},
{
anchors:7,
beanClassName:"javax.swing.JScrollPane",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_17\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JScrollPane\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>260<\/int> \
    <int>230<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <object class=\"javax.swing.border.EmptyBorder\"> \
    <object class=\"java.awt.Insets\"> \
     <int>0<\/int> \
     <int>0<\/int> \
     <int>0<\/int> \
     <int>0<\/int> \
    <\/object> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>scrollTree<\/string> \
  <\/void> \
  <void property=\"verifyInputWhenFocusTarget\"> \
   <boolean>false<\/boolean> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:-2,
location:"260,30",
name:"scrollTree",
size:"260,210",
typeid:12,
uuid:"1E485F99-FDB2-489D-AE5C-4C952BDE031E"
},
{
beanClassName:"javax.swing.JTree",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_15\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JTree\"> \
  <object class=\"javax.swing.tree.DefaultTreeModel\"> \
   <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
    <void property=\"userObject\"> \
     <string>JTree<\/string> \
    <\/void> \
    <void method=\"add\"> \
     <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
      <void property=\"userObject\"> \
       <string>colors<\/string> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>blue<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>violet<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>red<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>yellow<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
     <\/object> \
    <\/void> \
    <void method=\"add\"> \
     <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
      <void property=\"userObject\"> \
       <string>sports<\/string> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>basketball<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>soccer<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>football<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>hockey<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
     <\/object> \
    <\/void> \
    <void method=\"add\"> \
     <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
      <void property=\"userObject\"> \
       <string>food<\/string> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>hot dogs<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>pizza<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>ravioli<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>bananas<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
     <\/object> \
    <\/void> \
   <\/object> \
  <\/object> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"cellRenderer\"> \
   <void property=\"location\"> \
    <object class=\"java.awt.Point\"> \
     <int>-45<\/int> \
     <int>-16<\/int> \
    <\/object> \
   <\/void> \
  <\/void> \
  <void property=\"name\"> \
   <string>tree<\/string> \
  <\/void> \
  <void property=\"rootVisible\"> \
   <boolean>false<\/boolean> \
  <\/void> \
  <void property=\"showsRootHandles\"> \
   <boolean>true<\/boolean> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"360,120",
name:"tree",
size:"80,80",
typeid:12,
uuid:"236F0FFD-32B7-469D-8C38-3EC917751677"
},
{
beanClassName:"javax.swing.JList",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_11\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JList\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"doubleBuffered\"> \
   <boolean>true<\/boolean> \
  <\/void> \
  <void property=\"dragEnabled\"> \
   <boolean>true<\/boolean> \
  <\/void> \
  <void property=\"name\"> \
   <string>listOptions<\/string> \
  <\/void> \
  <void property=\"selectionMode\"> \
   <int>0<\/int> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"70,120",
name:"listOptions",
size:"80,80",
typeid:12,
uuid:"28EE106D-DC20-4846-88A3-6626FF003433"
},
{
anchors:6,
dataProviderID:"alias",
location:"260,240",
name:"alias",
onActionMethodID:"49FFD396-7E92-4CEC-8479-671BE2687653",
size:"260,20",
text:"Alias",
typeid:4,
uuid:"458C6A00-B4C1-4BD4-BFC7-03047C357FFF"
},
{
anchors:15,
borderType:"EtchedBorder,0,null,null",
formIndex:-3,
lineSize:1,
location:"8,28",
shapeType:1,
size:"214,234",
transparent:true,
typeid:21,
uuid:"50D8B76C-E269-4F22-81BE-C5CCF7E394CF"
},
{
anchors:15,
beanClassName:"javax.swing.JScrollPane",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_17\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JScrollPane\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>210<\/int> \
    <int>210<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <object class=\"javax.swing.border.EmptyBorder\"> \
    <object class=\"java.awt.Insets\"> \
     <int>0<\/int> \
     <int>0<\/int> \
     <int>0<\/int> \
     <int>0<\/int> \
    <\/object> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>scrollOptions<\/string> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:-1,
location:"10,30",
name:"scrollOptions",
size:"210,210",
typeid:12,
uuid:"6A4EC543-9DBC-4876-9F65-AD09E93AC5CC"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"5804BE64-BD66-4BC7-B44A-3314D54EA001",
location:"228,130",
mediaOptions:2,
name:"up",
onActionMethodID:"D1A377CF-1A52-4032-B897-46B7A05675B6",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"7CED9772-8D95-4690-B569-C507BF22EB5C"
},
{
anchors:3,
location:"510,0",
mediaOptions:14,
onActionMethodID:"4923E999-3BAD-4770-B535-052D94962C3D",
showClick:false,
showFocus:false,
size:"20,20",
transparent:true,
typeid:7,
uuid:"8086CF2F-DB45-4121-A051-FEDB01382193"
},
{
height:300,
partType:5,
typeid:19,
uuid:"83006DD2-E61E-447F-9D8C-FFD3D465FF27"
},
{
anchors:14,
dataProviderID:"filter",
location:"10,240",
name:"filter1",
onActionMethodID:"FABAE7EC-28F6-4D97-876C-726EE3721BAA",
size:"190,20",
text:"Filter",
typeid:4,
uuid:"8B08555E-8A89-4A13-8619-BAFB835D3284"
},
{
anchors:3,
location:"260,10",
mediaOptions:14,
size:"80,20",
tabSeq:-1,
text:"Scelte",
transparent:true,
typeid:7,
uuid:"A033C9E0-7F6F-40F7-8BF9-09FF461E5A6E"
},
{
anchors:7,
borderType:"EtchedBorder,0,null,null",
formIndex:-5,
lineSize:1,
location:"258,28",
shapeType:1,
size:"264,234",
transparent:true,
typeid:21,
uuid:"B3ABDA2A-2388-4057-9D3D-C1349FFA1A86"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"127B464F-994F-4572-810C-24FBC185DB64",
location:"228,90",
mediaOptions:2,
onActionMethodID:"AAFB6025-53E3-4262-829C-A32EBB441EF9",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"B69D02CA-5AF4-4BEF-87D2-84D04BA58DA0"
},
{
location:"10,10",
mediaOptions:14,
size:"80,20",
tabSeq:-1,
text:"Opzioni",
transparent:true,
typeid:7,
uuid:"DACBAE96-032D-4289-B828-6D94D5829875"
},
{
anchors:6,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"0746A208-4E27-45D4-BABE-FB96F24D516F",
location:"490,267",
mediaOptions:2,
onActionMethodID:"A96A31FD-0670-4762-81A6-F58E2CABFDD1",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"DC108741-2072-4474-99F2-87C51EC894B4"
},
{
anchors:6,
imageMediaID:"618F9AA0-E8E0-4019-A9D8-472CC462832F",
location:"201,241",
mediaOptions:14,
onActionMethodID:"FABAE7EC-28F6-4D97-876C-726EE3721BAA",
showClick:false,
size:"18,18",
transparent:true,
typeid:7,
uuid:"DE7C26C9-4ED1-40E4-A19C-41CE1B7E5B73"
},
{
anchors:14,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"free",
location:"10,270",
name:"free1",
onActionMethodID:"50C5896B-3CBF-4BAA-BDEF-E5D7CE6519D1",
size:"190,20",
text:"Free",
typeid:4,
uuid:"EFE43FBB-0C5F-4F75-A8F3-781D54854831"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"6AD2E909-E7A4-491A-92E5-D7E2F229B865",
location:"228,160",
mediaOptions:2,
name:"down",
onActionMethodID:"54E64E13-58CE-4DE4-AA0D-D23A9DF61628",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"F0A3855A-20E1-4483-9C7F-E6EC264F1876"
},
{
anchors:6,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"85582DF7-017C-4741-98E4-7C3C1142DF33",
location:"460,267",
mediaOptions:2,
onActionMethodID:"2D8F8D17-DE23-4DBB-BBEE-3F9E4B658532",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"F525B990-3499-4EA8-AAC5-281431B01051"
}
],
name:"nfx_interfaccia_popup_filltree",
navigatorID:"-1",
onHideMethodID:"1D50B59C-1AC4-4CCB-9D42-D00343661E00",
onLoadMethodID:"875B8EC0-70A0-4EF5-891E-BCF2103A068C",
onShowMethodID:"D5BB155F-FE02-42AB-A327-9A509CDF0D56",
paperPrintScale:100,
showInMenu:true,
size:"530,300",
styleName:"keeneight",
typeid:3,
uuid:"F4EE40FC-EE05-439C-ADB2-B84DC66F8E45"