/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"FF65B7B4-7A38-428E-B523-D74FCEF5C08E"}
 */
var alias = null;

/**
 *@type {Object}
 * @properties={typeid:35,uuid:"CFE103BF-33A2-438C-83BE-AD897384CE5E",variableType:-4}
 */
var choices = new Object();

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A97191BF-406D-4067-BC11-D7F5A498946B"}
 */
var choicesBK = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"4C8E1060-A570-460A-85C9-9F38C19B4DB9"}
 */
var defaultTree = "{\"children\":{\"0\":{\"children\":{\"0\":{\"children\":{\"0\":{\"type\":\"node\",\"name\":\"anagrafica_disegni_record\"},\"1\":{\"type\":\"node\",\"name\":\"anagrafica_progetti_record\"},\"2\":{\"type\":\"node\",\"name\":\"anagrafica_vetture_record\"}},\"type\":\"folder\",\"name\":\"Anagrafiche\"},\"1\":{\"children\":{\"0\":{\"type\":\"node\",\"name\":\"segnalazioni_anomalie_record\"},\"1\":{\"type\":\"node\",\"name\":\"segnalazioni_chiuse_con_sk_record\"},\"2\":{\"type\":\"node\",\"name\":\"schede_anomalie_record\"},\"3\":{\"children\":{\"0\":{\"type\":\"node\",\"name\":\"gestione_anomalie_istogramma\"},\"1\":{\"type\":\"node\",\"name\":\"gestione_anomalie_alberini\"}},\"type\":\"folder\",\"name\":\"Viste grafiche\"}},\"type\":\"folder\",\"name\":\"Anomalie\"},\"2\":{\"children\":{\"0\":{\"type\":\"node\",\"name\":\"distinte_progetto_albero\"}},\"type\":\"folder\",\"name\":\"Distinte\"},\"3\":{\"children\":{\"0\":{\"type\":\"node\",\"name\":\"anagrafica_coefficienti_record\"},\"1\":{\"type\":\"node\",\"name\":\"anagrafica_percorsi_record\"}},\"type\":\"folder\",\"name\":\"Affidabilità \"}},\"type\":\"folder\",\"name\":\"Direzione Tecnica\"}},\"type\":\"folder\",\"index\":{\"0\":\"Direzione Tecnica\",\"1\":\"Anagrafiche\",\"2\":\"anagrafica_disegni_record\",\"3\":\"anagrafica_progetti_record\",\"4\":\"anagrafica_vetture_record\",\"5\":\"Anomalie\",\"6\":\"segnalazioni_anomalie_record\",\"7\":\"schede_anomalie_record\",\"8\":\"Viste grafiche\",\"9\":\"gestione_anomalie_istogramma\",\"10\":\"gestione_anomalie_alberini\",\"11\":\"Distinte\",\"12\":\"distinte_progetto_albero\",\"13\":\"Affidabilità \",\"14\":\"anagrafica_coefficienti_record\",\"15\":\"anagrafica_percorsi_record\",\"16\":\"segnalazioni_chiuse_con_sk_record\"}}";

/**
 * @properties={typeid:35,uuid:"22499439-7913-4434-AA67-8F8E059A9946",variableType:-4}
 */
var dragRecognizer;

/**@type {java.awt.dnd.DragSource}
 * @properties={typeid:35,uuid:"5C0F9D9F-CAF6-49A3-8915-00200055BDE3",variableType:-4}
 */
var dragSource;

/**
 * @properties={typeid:35,uuid:"14F74A50-7225-4D3A-AE8B-D01F2D131644",variableType:-4}
 */
var dropTarget;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"FD825868-BD4D-4528-B3B5-F9FA65B1BA2C"}
 */
var filltree = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"62D65B17-2747-4994-BA07-B06163028C30"}
 */
var filter = null;

/**
 * @properties={typeid:35,uuid:"37C8072D-ADD2-4853-989A-A2E85291BA53",variableType:-4}
 */
var first = true;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C8A5C8B3-8369-45E7-8119-C3A11E39EC47"}
 */
var free = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D09FDB8A-E7AF-4E80-A15B-D57E07C7C633"}
 */
var selectedNode = null;

/**
 * @properties={typeid:35,uuid:"4CFAAFA0-4B79-4A9C-B116-C04E14EC2E23",variableType:-4}
 */
var selectedPath = null;

/**
 * @properties={typeid:35,uuid:"7FCA398C-4F81-4BE6-9ADB-D872D170848A",variableType:-4}
 */
var titles = new Array();

/**
 *
 * @properties={typeid:24,uuid:"E9146747-33C5-45BF-911F-F98D1CD850B9"}
 */
function add(s, p, n) {
	var selectedValue = s;
	var sp = p || selectedPath;
	var sn = n || selectedNode;

	//if(choices.index.indexOf(globals.nfx_getNameFromTitle(selectedValue)) == -1)
	//{
	if (!sn) {
		choices.children.push({ name: globals.nfx_getNameFromTitle(selectedValue), alias: null, type: "node" });
		choices.index.push(globals.nfx_getNameFromTitle(selectedValue));
		fillTreeMethod(null, null);
	} else {
		var insertObject = getObject(null, null, sp);

		if (insertObject.type == "folder") {
			insertObject.children.push({ name: globals.nfx_getNameFromTitle(selectedValue), alias: null, type: "node" });
			choices.index.push(globals.nfx_getNameFromTitle(selectedValue));
			//var pathToExpand = elements.tree.getSelectionPath();
			fillTreeMethod(null, null);

		}
	}
	selectedNode = null;
	selectedPath = null;
	//}
}

/**
 *
 * @properties={typeid:24,uuid:"50C5896B-3CBF-4BAA-BDEF-E5D7CE6519D1"}
 */
function addFree() {
	if (free && choices.index.indexOf(free) == -1) {
		if (!selectedNode) {
			choices.children.push({ name: free, type: "folder", children: [] });
			choices.index.push(free);
			free = null
			fillTreeMethod(null, null);
		} else {
			var insertObject = getObject(null, null, null);

			if (insertObject.type == "folder") {
				insertObject.children.push({ name: free, type: "folder", children: [] });
				choices.index.push(free);
				free = null;
				fillTreeMethod(null, null);
			}
		}
		selectedNode = null;
		selectedPath = null;
	} else {
		plugins.dialogs.showErrorDialog("Errore", "L'etichetta \"" + free + "\" è già presente nell'albero", "Ok");
	}
}

/**
 *
 * @properties={typeid:24,uuid:"9DF08706-53BF-465D-98D1-1C607717364B"}
 */
function addWrap() {
	add(elements.listOptions.getSelectedValue(), null, null);
}

/**
 *
 * @properties={typeid:24,uuid:"A96A31FD-0670-4762-81A6-F58E2CABFDD1"}
 */
function cancel() {
	filltree = null;

	//application.closeFormDialog();

	var w = controller.getWindow();
	w.destroy();
}

/**
 *
 * @properties={typeid:24,uuid:"E21CF45F-A70B-410C-A218-325D405164D3"}
 */
function changeInsertnode() {
	var node = elements.tree.getLastSelectedPathComponent();
	var path = elements.tree.getSelectionPath();

	if (node) {
		selectedNode = node.getUserObject();
		selectedPath = path.getPath();
	} else {
		selectedNode = null;
		selectedPath = null;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"47C3F4D1-ACCB-4C08-978E-3FC7F9FCB2B7"}
 */
function clear() {
	choices = {
		children: [],
		index: [],
		type: "folder"
	};
	selectedNode = null;
	selectedPath = null;
	fillTreeMethod(null, null);
}

/**
 *
 * @properties={typeid:24,uuid:"FFA2B557-A637-4701-84BF-83D98C1C791A"}
 */
function dcAdd(e) {
	if (e.getClickCount() == 2) {
		//addWrap();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"C6753AB3-FCC1-4270-A3E8-1A727A76F85D"}
 */
function dd(dtde, p) {
	var path = elements.tree.getPathForRow(elements.tree.getRowForLocation(p.x, p.y));
	if (!path) {
		dtde.rejectDrag();
		return;
	}
	try {
		var obj = getObject(null, null, path.getPath());
		if (obj.type == "folder") {
			dtde.acceptDrag(dtde.getDropAction());
		} else {
			dtde.rejectDrag();
		}
	} catch (e) {
		dtde.rejectDrag();
	}
}

/**
 *@param {java.awt.dnd.DropTargetDropEvent} dtde
 * @properties={typeid:24,uuid:"B71C6B7F-66B0-45D9-B797-B0A1901A461A"}
 */
function dp(dtde) {
	var p = dtde.getLocation();
	try {
		var draggedElement = elements.listOptions.getSelectedValue();
		//application.output(elements.tree.getPathForLocation(p.x, p.y).getPath());
		//FS
		/** @type {javax.swing.tree.DefaultMutableTreeNode} */
		var treeAsDefaultMutableTreeNode = elements.tree.getPathForLocation(p.x, p.y).getLastPathComponent();
		//dep 		add(draggedElement,
		//			elements.tree.getPathForLocation(p.x, p.y).getPath(),
		//			elements.tree.getPathForLocation(p.x, p.y).getLastPathComponent().getUserObject());
		add(draggedElement,
			elements.tree.getPathForLocation(p.x, p.y).getPath(),
			treeAsDefaultMutableTreeNode.getUserObject());
		dtde.dropComplete(true);
	} catch (e) {
		dtde.dropComplete(false);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"DF2C531B-1D6E-45F6-958E-018DD0C738F1"}
 */
function dragEnter(e) {
	dragOver(e);
}

/**
 *@param{java.awt.dnd.DropTargetEvent} e
 * @properties={typeid:24,uuid:"6CDC8888-341D-4BAD-B8BD-A1D4E815C4F9"}
 */
function dragExit(e) {
	var dtde = e;
	//	JStaffa aggiunto output per variabile non utilizzata
	application.output('nfx nfx_interfaccia_popup_filltree.dragExit(e) dtde: ' + dtde, LOGGINGLEVEL.INFO);
}

/**
 *
 * @properties={typeid:24,uuid:"476F1797-4E1D-4456-9063-5F70838D28D8"}
 */
function dragGestureRecognized(str) {
	dragSource.startDrag(str, java.awt.dnd.DragSource.DefaultMoveNoDrop, globals.nfx_tree_getVoidTransferableImpl(), dragRecognizer);
}

/**
 *@param {java.awt.dnd.DropTargetDragEvent} e
 * @properties={typeid:24,uuid:"8A1DC7DC-70D0-493E-B94A-28665046391B"}
 */
function dragOver(e) {
	/** @type {java.awt.dnd.DropTargetDragEvent} */
	var dtde = e;
	/** @type {javax.swing.JTree} */
	var tree = elements.tree;
	//dep	var point = globals.nfx_autoscrollTree(dtde, elements.tree);
	var point = globals.nfx_autoscrollTree(dtde, tree);

	dd(dtde, point);
}

/**
 *
 * @properties={typeid:24,uuid:"AC88F2D1-A7D3-487C-ADEF-BDDAEBDF5ABE"}
 */
function drop(w) {
	var dtde = w;
	dp(dtde);
}

/**
 *
 * @properties={typeid:24,uuid:"F687CA63-EE1A-49A5-B40C-4E65B30EC566"}
 */
function fillTreeMethod(e, r) {
	//	JStaffa aggiunto cast a javax.swing.tree.DefaultTreeModel
	/** @type {javax.swing.tree.DefaultTreeModel} */
	var model = elements.tree.getModel();

	//	JStaffa tolto javaimporter
	//	var imported = new JavaImporter(Packages.javax.swing.tree);

	//	JStaffa aggiunta dichiarazione di variabile
	/** @type {javax.swing.tree.DefaultMutableTreeNode} */
	var currentNode = null;

	if (e) {
		currentNode = e;
	} else {
		currentNode = model.getRoot();
		currentNode.removeAllChildren();
	}

	var treeValues = r || choices;

	for (var i in treeValues.children) {

		var node = null;

		if (treeValues.children[i].type == "folder") {
			//			with (imported) {
			node = new Packages.javax.swing.tree.DefaultMutableTreeNode(treeValues.children[i].name);
			//			}

			fillTreeMethod(node, treeValues.children[i]);
		} else {
			//			with (imported) {
			node = new Packages.javax.swing.tree.DefaultMutableTreeNode( (treeValues.children[i].alias) ? treeValues.children[i].alias : globals.nfx_getTitleFormName(treeValues.children[i].name));
			//			}
		}

		model.insertNodeInto(node, currentNode, currentNode.getChildCount());
		model.reload();
	}
	model.reload();
}

/**
 *
 * @properties={typeid:24,uuid:"481A0CA6-35AF-4C38-8D73-FC68B7004150"}
 */
function filterList() {
	if (filter == "") {
		filter = null;
		application.updateUI();
	}

	return globals.utils_filterList(filter, titles);
}

/**
 *
 * @properties={typeid:24,uuid:"FABAE7EC-28F6-4D97-876C-726EE3721BAA"}
 */
function filterWrap() {
	elements.listOptions.setListData(filterList());
}

/**
 *
 * @properties={typeid:24,uuid:"6A812E70-F425-4FED-89CE-85C8830297A1"}
 */
function getFather(tree, ind) {
	var treeValues = tree || choices;
	var index = ind || 1;

	for (var i in treeValues.children) {
		var title = null;
		if (treeValues.children[i].type == "folder") {
			title = treeValues.children[i].name;
		} else {
			title = (treeValues.children[i].alias) ? treeValues.children[i].alias : globals.nfx_getTitleFormName(treeValues.children[i].name);
		}

		if (title == selectedPath[index]) {
			if (index == selectedPath.length - 2) {
				return treeValues.children[i];
			} else {
				return getFather(treeValues.children[i], index + 1);
			}
		}
	}
	return null;
}

/**
 *
 * @properties={typeid:24,uuid:"C09CD392-3974-4A35-8309-2A52417EC894"}
 */
function getObject(trVls, ind, se) {
	var treeValues = trVls || choices;
	var index = ind || 1;
	var s = se || selectedPath;

	if (!s) return null;

	for (var i in treeValues.children) {
		var title = null;
		if (treeValues.children[i].type == "folder") {
			title = treeValues.children[i].name;
		} else {
			title = (treeValues.children[i].alias) ? treeValues.children[i].alias : globals.nfx_getTitleFormName(treeValues.children[i].name);
		}

		if (title == s[index].toString()) {
			if (index == s.length - 1) {
				return treeValues.children[i];
			} else {
				return getObject(treeValues.children[i], index + 1, s);
			}
		}
	}

	return null;
}

/**
 *
 * @properties={typeid:24,uuid:"E6FD6494-48FF-4DF8-B0E7-C1204163DE4A"}
 */
function moveDown(node, path) {

	if (node && path) {
		var moved = getObject(null, null, null);
		var father = getFather(null, null);

		var working = (choices.children.indexOf(moved) != -1) ? choices.children : father.children;

		if (working.indexOf(moved) != working.length - 1) {
			var index = working.indexOf(moved);
			var app = working[index + 1];
			working[index + 1] = working[index];
			working[index] = app;
			fillTreeMethod(null, null);
			selectedNode = null;
			selectedPath = null;
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"54E64E13-58CE-4DE4-AA0D-D23A9DF61628"}
 */
function moveDownWrap() {
	moveDown(selectedNode, selectedPath);
}

/**
 *
 * @properties={typeid:24,uuid:"CCC29654-4FF6-4692-A2E6-91C17BCF20D0"}
 */
function moveUp(node, path) {

	if (node && path) {
		var moved = getObject(null, null, null);
		var father = getFather(null, null);

		var working = (choices.children.indexOf(moved) != -1) ? choices.children : father.children;

		if (working.indexOf(moved) != 0) {
			var index = working.indexOf(moved);
			var app = working[index - 1];
			working[index - 1] = working[index];
			working[index] = app;
			fillTreeMethod(null, null);
			selectedNode = null;
			selectedPath = null;
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"D1A377CF-1A52-4032-B897-46B7A05675B6"}
 */
function moveUpWrap() {
	moveUp(selectedNode, selectedPath);
}

/**
 *
 * @properties={typeid:24,uuid:"1D50B59C-1AC4-4CCB-9D42-D00343661E00"}
 */
function onHide() {
	if (!filltree) {
		choices = forms.nfx_json_serializer.parse(choicesBK, null);
		rebuildTree(null);
	}
	filter = null;
}

/**
 *
 * @properties={typeid:24,uuid:"875B8EC0-70A0-4EF5-891E-BCF2103A068C"}
 */
function onLoad() {

	//SAuc

	var query = "select tree_value from k8_gestione_utenze where user_servoy = ?";

	//var tree = databaseManager.getDataSetByQuery(controller.getServerName(), query, [globals.nfx_user], 1);

	var srv = controller.getDataSource().split('/')[1];
	var tree = databaseManager.getDataSetByQuery(srv, query, [globals.nfx_user], 1);

	choices = (tree.getValue(1, 1)) ? forms.nfx_json_serializer.parse(tree.getValue(1, 1), null) : forms.nfx_json_serializer.parse(defaultTree, null);
//	var Remo = new Array();
//	Remo.push("schede_collaudo_record");
////	Remo.push("");
////	Remo.push("");
////	Remo.push("");
//
//	for(var g=0;g<Remo.length;g++){
//		var rem = Remo[g];
//		for(var i=0;i<choices.children.length;i++){
//			if(rem == choices.children[i].name){
//				//choices.children.splice(i,1,choices.children[i]);
//				var remov = getObject(null, i, null);
//				removeIndexesFrom(remov);
//			}
//		}
//	}
	rebuildTree(null);

	var vpOptions = elements.scrollOptions.getViewport();
	vpOptions.add(java.awt.Component(elements.listOptions));
	elements.listOptions.addMouseListener(java.awt.event.MouseListener(dcAdd));

	var vpTree = elements.scrollTree.getViewport();
	vpTree.add(java.awt.Component(elements.tree));
	elements.tree.setCellRenderer(globals.nfx_getTreeIconRenderer());
	//FS
	/** @type {javax.swing.event.TreeSelectionListener} */
	var changeInsertnodeAsTreeSelectionListener = changeInsertnode;
	//dep	elements.tree.addTreeSelectionListener(changeInsertnode);
	elements.tree.addTreeSelectionListener(changeInsertnodeAsTreeSelectionListener);
	globals.nfx_tree_setupDnD(elements.listOptions, elements.tree, forms[controller.getName()], null);
}

/**
 *
 * @properties={typeid:24,uuid:"D5BB155F-FE02-42AB-A327-9A509CDF0D56"}
 */
function onShow() {
	
	
	if (first) {
		elements.scrollOptions.visible = false;
		elements.scrollTree.visible = false;

		//		var allTitles = forms.nfx_interfaccia_pannello_tree.titles.slice();
		//		var allNames = forms.nfx_interfaccia_pannello_tree.names.slice();
		//		var allPrograms = forms.nfx_interfaccia_pannello_tree.programs.slice();
		//		var allPermissions = forms.nfx_interfaccia_pannello_tree.permissions.slice();

		var allTitles = forms.nfx_interfaccia_pannello_tree.titles;
		var allNames = forms.nfx_interfaccia_pannello_tree.names;
		var allPrograms = forms.nfx_interfaccia_pannello_tree.programs;
		var allPermissions = forms.nfx_interfaccia_pannello_tree.permissions;

		titles = new Array();
		globals.nfx_progressBarShow(allTitles.length)
		for (var i = 0; i < allTitles.length; i++) {
			//FS
			//dep  if (forms.allnames.indexOf(allNames[i]) != -1 && allTitles[i] && allPrograms[i]) {
			/** @type {Array<String>} */			
			var allFormNames =solutionModel.getForms().map(function (jsForm) { return jsForm.name });
			if (allFormNames.indexOf(allNames[i]) != -1 && allTitles[i] && allPrograms[i]) {

				if (allPermissions[i] == "%") {
					titles.push(allTitles[i]);
				} else {
					var p = (allPermissions[i] == "^") ? globals["nfx_developers"].concat(globals["vc2_powerUsers"]) : globals["nfx_developers"].concat(globals["vc2_powerUsers"]).concat(allPermissions[i].split("|"));
					if (p.indexOf(globals.nfx_user) != -1) {
						titles.push(allTitles[i]);
					}
				}
			}
			globals.nfx_progressBarStep();
		}
		titles.sort();

		elements.scrollOptions.visible = true;
		elements.scrollTree.visible = true;
		first = null;
	}

	filltree = null;
	free = null;

	choicesBK = forms.nfx_json_serializer.stringify(choices, null, null);

	elements.listOptions.setListData(filterList());
	fillTreeMethod(null, null);

	selectedNode = null;
	selectedPath = null;
	
	
//	var Remo = new Array();
//	Remo.push("schede_collaudo_record");
////	Remo.push("");
////	Remo.push("");
////	Remo.push("");
//
//	for(var g=0;g<Remo.length;g++){
//		var rem = Remo[g];
//		for(var i=0;i<choices.children.length;i++){
//			if(rem == choices.children[i].name){
//				var remov = getObject(null, i, null);
//				removeIndexesFrom(remov);
//			}
//		}
//	}
//	
//	var remov = getObject(null, null, null);
//	removeIndexesFrom(remov);
//	
	
}

/**
 *
 * @properties={typeid:24,uuid:"4923E999-3BAD-4770-B535-052D94962C3D"}
 */
function printTreeStr() {
	var treeStr = forms.nfx_json_serializer.stringify(choices, null, null);
	application.output(treeStr);
}

/**
 *
 * @properties={typeid:24,uuid:"6A8C2973-35DE-48B7-8512-F7290FA6CFD4"}
 */
function rebuildTree(s) {
	var working = s || choices;
	var backup = null;

	if (working == choices) {
		backup = new Array();
		for (var i in working.index) {
			backup.push(working.index[i]);
		}

		working.index = new Array();
		for (var i2 in backup) {
			working.index.push(backup[i2]);
		}
	}

	if (working.type == "folder") {
		backup = new Array();
		for (var i3 in working.children) {
			rebuildTree(working.children[i3]);
			backup.push(working.children[i3]);
		}

		working.children = new Array();
		for (var i4 in backup) {
			working.children.push(backup[i4]);
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"71322E22-2DBE-47D8-A3E7-10D99913E5E6"}
 */
function remove(node, path) {

	if (node && path) {
		var removed = getObject(null, null, null);
		var father = getFather(null, null);

		var working = (choices.children.indexOf(removed) != -1) ? choices.children : father.children;

		removeIndexesFrom(removed);
		working.splice(working.indexOf(removed), 1);

		fillTreeMethod(null, null);
		selectedNode = null;
		selectedPath = null;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"93EF2660-36BF-483D-AAA5-569EA9ECD673"}
 */
function removeIndexesFrom(from) {

	if (from.type == "node") {
		choices.index.splice(choices.index.indexOf(from.name), 1);
	} else {
		choices.index.splice(choices.index.indexOf(from.name), 1);
		for (var i in from.children) {
			removeIndexesFrom(from.children[i]);
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"AAFB6025-53E3-4262-829C-A32EBB441EF9"}
 */
function removeWrap() {
	remove(selectedNode, selectedPath);
}

/**
 *
 * @properties={typeid:24,uuid:"2D8F8D17-DE23-4DBB-BBEE-3F9E4B658532"}
 */
function save() {
	if (choices.children.length == 0) {
		var risposta = plugins.dialogs.showQuestionDialog("Uscita", "Sei sicuro di voler l'albero vuoto?", "Sì", "No");
		if (risposta == "Sì") {
			filltree = 0;
			saveTree();

			//application.closeFormDialog();

			var w = controller.getWindow();
			w.destroy();
		}
	} else {
		filltree = 0;
		saveTree();

		//application.closeFormDialog();

		var we = controller.getWindow();
		we.destroy();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"367401ED-A6CB-4644-81BD-5DFC0E07A769"}
 * @AllowToRunInFind
 */
function saveTree() {
	//	JStaffa
	//	var users = databaseManager.getFoundSet(controller.getServerName(), "k8_gestione_utenze");
	var users = databaseManager.getFoundSet(databaseManager.getDataSourceServerName(controller.getDataSource()), "k8_gestione_utenze");

	if (users.find()) {
		users.user_servoy = globals.nfx_user;
		users.search();
	}

	users.tree_backup = users.tree_value;
	//	JStaffa
	//	users.tree_value = (forms.nfx_json_serializer.stringify(choices, null, null) != forms.nfx_json_serializer.stringify({ children: [], index: [], type: "folder" }, null, null)) ? forms.nfx_json_serializer.stringify(choices) : null;
	users.tree_value = (forms.nfx_json_serializer.stringify(choices, null, null) != forms.nfx_json_serializer.stringify({ children: [], index: [], type: "folder" }, null, null)) ? forms.nfx_json_serializer.stringify(choices, null, null) : null;
}

/**
 *
 * @properties={typeid:24,uuid:"C9AF6C98-0392-4295-8DEC-66A21157D97B"}
 */
function testExpanded() {
	var e = elements.tree.getExpandedDescendants(new Packages.javax.swing.tree.TreePath(elements.tree.getModel().getRoot()));
	application.output(e);
	while (e && e.hasMoreElements()) {
		var obj = e.nextElement();
		application.output("Oggetto vettore: " + obj);
	}

	application.output(utils.stringMD5HashBase64("Ramo"));
}

/**
 * @properties={typeid:24,uuid:"63933DFA-9B59-4E31-83E0-BE982431A405"}
 */
function setAlias() {
	if (selectedNode && alias) {
		var obj = getObject(null, null, null);
		if (obj.type == "node") {
			obj.alias = alias;
		}
		if (obj.type == "folder") {
			choices.index[choices.index.indexOf(obj.name)] = alias;
			obj.name = alias;
		}
		fillTreeMethod(null, null);
		selectedNode = null;
		selectedPath = null;
		alias = null;
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"49FFD396-7E92-4CEC-8479-671BE2687653"}
 */
function setAliasWrap(event) {
	setAlias();
}
