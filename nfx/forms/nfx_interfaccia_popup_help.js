/**
 * @properties={typeid:35,uuid:"8BCBD5EF-0969-46AF-820E-ABC60BE953D3",variableType:-4}
 */
var edit_mode = false;

/**
 * @properties={typeid:35,uuid:"72D713C1-7707-4144-B348-FA3BE80658AD",variableType:-4}
 */
var saved = false;

/**
 * @properties={typeid:24,uuid:"7676ED48-EA52-4B5E-BB78-5A25158E0D89"}
 */
function openPopup(editMode) {
	globals.nfx_setBusy(true);

	var server = databaseManager.getDataSourceServerName(controller.getDataSource());
	var table = databaseManager.getDataSourceTableName(controller.getDataSource());
	databaseManager.addTableFilterParam(server, table, "nome", "=", globals.nfx_selectedProgram(), "form_restriction");

	saved = false;
	databaseManager.setAutoSave(false);

	edit_mode = editMode;
	elements.help_text.editable = edit_mode;
	elements.save.enabled = edit_mode;

	globals.nfx_setBusy(false);

	controller.show("Help");
}

/**
 * @properties={typeid:24,uuid:"D9DC94C6-DAC2-48CA-A6A5-4D12EBCD4F51"}
 */
function save() {
	saved = true;

	//application.closeFormDialog();

	var w = controller.getWindow();
	w.destroy();
}

/**
 * Handle hide window.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} allow hide
 *
 * @properties={typeid:24,uuid:"12899573-280B-4B39-8DCA-13D5D458BAEF"}
 */
function onHide(event) {
	globals.nfx_setBusy(true);

	if (!saved) {
		databaseManager.revertEditedRecords();
		databaseManager.saveData();
	}

	databaseManager.setAutoSave(true);

	var server = databaseManager.getDataSourceServerName(controller.getDataSource());
	databaseManager.removeTableFilterParam(server, "form_restriction");

	globals.nfx_setBusy(false);
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"03ACFC3E-4D71-4DA8-8BA9-FE49782D02C4"}
 */
function operation(event) {
//SAuc
//	var items = new Array();
//	if (help_aname) {
//		var d = plugins.popupmenu.createMenuItem("Download", download);
//		items.push(d);
//	}
//	if (edit_mode) {
//		var u = plugins.popupmenu.createMenuItem("Upload", upload);
//		items.push(u);
//	}
//	if (help_aname && edit_mode) {
//		var r = plugins.popupmenu.createMenuItem("Elimina", remove);
//		items.push(r);
//	}
//	if (!items.length) {
//		var n = plugins.popupmenu.createMenuItem("Nessuna operazione possibile...", null);
//		items.push(n);
//	}
//	plugins.popupmenu.showPopupMenu(elements.menu, items);

	var menu = plugins.window.createPopupMenu();

	if (help_aname) {
		var item = menu.addMenuItem("Download", download);
		application.output(item);
	}
	if (edit_mode) {
		var item2 = menu.addMenuItem("Upload", upload);
		application.output(item2);
	}
	if (help_aname && edit_mode) {
		var item3 = menu.addMenuItem("Elimina", remove);
		application.output(item3);
	}
	if (menu.getItemCount()==0) {
		var item4 = menu.addMenuItem("Nessuna operazione possibile...", null);
		application.output(item4);
	}
	
	menu.show(elements.menu);
	
}

/**
 * @properties={typeid:24,uuid:"E6701A95-B774-4F9D-B391-0AD6FB058C95"}
 */
function download() {
	globals.utils_saveAs(help_attch, help_aname);
}

/**
 * @properties={typeid:24,uuid:"3E90DCA6-8E51-437C-A07C-D55302149D21"}
 */
function upload() {
	if (help_aname && plugins.dialogs.showQuestionDialog("Upload", "Caricando un nuovo allegato il file " + help_aname + " andrà perso, continuare?", "Sì", "No") == "No") {
		return;
	}
	var openFile = plugins.file.showFileOpenDialog(1, null, false, ["txt", "pdf", "doc", "xls", "ppt", "pps", "avi", "mpg", "mpeg", "mov", "wmv", "jpg", "jpeg", "bmp", "png", "gif", "mp3", "wav", "wma", "tif", "tiff"]);
	if (openFile) {
		var file = plugins.file.convertToJSFile(openFile);
		help_aname = file.getName();
		help_attch = plugins.file.readFile(file);
	}
}

/**
 * @properties={typeid:24,uuid:"1A24D646-F30E-4E47-BC89-58B899D9C7EC"}
 */
function remove() {
	if (plugins.dialogs.showQuestionDialog("Elimina", "Eliminare il file " + help_aname + "?", "Sì", "No") == "Sì") {
		help_aname = null;
		help_attch = null;
	}
}
