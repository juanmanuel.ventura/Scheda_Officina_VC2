/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"3AC9AFAB-3742-41D0-81BA-5B77F8BB7F09"}
 */
var password = null;

/**
 *
 * @properties={typeid:24,uuid:"9CBBBEA1-8CB6-43D3-8EE5-7D081518330B"}
 */
function enter() {
	elements.enterbutton.enabled = false;
	if (globals.nfx_user && password) {
		elements.accessLabel.fgcolor = "#DDDDDD";
		elements.accessLabel.text = "Accesso in corso, attendere ...";
		application.updateUI(100);
		
//		JStaffa non usata..
//		var pwd = utils.stringMD5HashBase64(password);
		password = null;
		
//		JStaffa
//		var query = "select user_id,password_value,email from " + forms.nfx_interfaccia_gestione_utenze.controller.getTableName() + " where user_servoy = ?";
		var query = "select user_id,password_value,email from " + forms.nfx_interfaccia_gestione_utenze.controller.getDataSource().split('/')[2] + " where user_servoy = ?";

//		JStaffa
//		var userData = databaseManager.getDataSetByQuery(controller.getServerName(), query, [globals.nfx_user], 1);
		var userData = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, [globals.nfx_user], 1);

		var uid = userData.getValue(1, 1);
		
//		JStaffa non usata..
//		var pwdToCheck = userData.getValue(1, 2);

		//if(pwd == pwdToCheck){
		if (true) {
			var succes = security.login(globals.nfx_user, uid, ["users"]);
			if (!succes) {
				elements.error.fgcolor = "#FF0000";
				elements.error.text = "Login fallito - Internal problem";
				return;
			}
			globals.nfx_email = userData.getValue(1, 3);
			forms.nfx_interfaccia_gestione_accessi.controller.newRecord();
			forms.nfx_interfaccia_gestione_accessi.db_action = "login";
			databaseManager.saveData();
		} else {
			elements.error.fgcolor = "#FF0000";
			elements.error.text = "Username e/o Password errati.";
			elements.accessLabel.text = "";
			elements.enterbutton.enabled = true;
			application.updateUI(100);
		}
	} else {
		elements.error.fgcolor = "#FFFF00";
		elements.error.text = "Dati incompleti.";
		elements.accessLabel.text = "";
		elements.enterbutton.enabled = true;
		application.updateUI(100);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"4770947E-6CB4-40E1-8151-626D11BAD99F"}
 */
function exit() {
	application.exit();
}

/**
 *
 * @properties={typeid:24,uuid:"A8AFE9DA-3890-4A37-8B68-6240BF2141A6"}
 */
function onShow() {
	elements.nfx_user.requestFocus();
}
