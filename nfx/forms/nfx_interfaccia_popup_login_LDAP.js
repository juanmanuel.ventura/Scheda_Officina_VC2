/**
 * @type {String}
 * @properties={typeid:35,uuid:"1BD15FCA-8F06-4966-A0F4-736481ACA26A"}
 */
var fail = "";

/**
 * @type {String}
 * @properties={typeid:35,uuid:"C05D1ED8-BDA5-4889-809A-FFA75C4A9681"}
 */
var failMessage = "Con l'ultimo aggiornamento di <b>VirtualCar2</b> è stata introdotta una nuova modalità di login che si appoggia al sistema LDAP di <b>Ferrari</b>, per autenticarsi è quindi necessario inserire: <ul style='list-style-type:disc;'><li><span style='font-weight:bold; text-decoration:underline;'>Nome Utente</span>: questo campo resta invariato, inserisca pure l'utenza utilizzata fino a oggi.</li><li><span style='font-weight:bold; text-decoration:underline;'>Password</span>: da oggi questo campo andrà compilato con la Sua <b style='color:red;'>password di rete Ferrari</b> (<i>ovvero quella che utilizza per accedere agli altri servizi quali posta elettronica etc...</i>).</li></ul>";

/**
 * @properties={typeid:35,uuid:"8D585055-152E-4B30-BC15-CF0947258284",variableType:-4}
 */
var infoLDAP = {
	host: "oraoid.unix.ferlan.it",
	login: "DC=gt,cn=AD,cn=Users,dc=ferlan,dc=it"
};

/**
 * @type {String}
 * @properties={typeid:35,uuid:"B0465DD4-6E84-4BBC-BB7D-419835B743B3"}
 */
var login = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"E640D402-65F0-4A0A-A220-F8A61E230475"}
 */
var password = null;

/**
 * @properties={typeid:24,uuid:"A5710274-B4AB-46DE-AB1D-0D90B250D72F"}
 */
function loginOnLoad() {
	globals.nfx_hideMenu();
	//globals.nfx_registerPlugins();
	fail = "<html><body><table style='border-width:1px; border-style:solid; border-color:red; padding-top:5px;'><tr><td style='text-align:right;'><img src=media:///warning16.png /></td><td style='color:red; font-size:10px; font-weight:bold;'>Attenzione!<td></tr><tr><td colspan=2 style='color:white; font-size:8px; text-align:justify; margin-left:7px; margin-bottom:5px;'>" + failMessage + "</td></tr></table></body></html>";
	elements.fail.visible = false;
}

/**
 * @properties={typeid:24,uuid:"18F3BD6C-A49A-420F-81C3-503194BAE6D8"}
 */
function loginOnShow() {
	elements.login.requestFocus();
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"4A8CC33F-4D2C-4BC6-971E-CC8F45A228DB"}
 */
function enter(event) {
	try {
		enterLDAP(event);
	} catch (er) {
		setError(er, 0);
	}
}

/**
 * @param event
 * @properties={typeid:24,uuid:"5221EADE-FCA1-41DA-A5A8-BF84C415E785"}
 * @AllowToRunInFind
 */
function enterLDAP(event) {
	//	elements.fail.visible = false;
	//	elements.enterbutton.enabled = false;
	//	elements.accessLabel.fgcolor = "#DDDDDD";
	//	elements.accessLabel.text = "Accesso in corso, attendere ...";
	//	application.updateUI();
	//
	//	//metodo
	//
	//	var log=security.authenticate("login_solution","loginUser",[login,password]);
	//
	//	if(log!=null){
	//
	//		application.output(log);
	//
	//	}
}

/**
 * @properties={typeid:24,uuid:"0122ED88-1442-4EF7-A590-9CE5A1579006"}
 */
function setError(text, color) {
	elements.error.fgcolor = color || "#FF0000";
	elements.error.text = text || "Login fallito (errore interno)";
	elements.accessLabel.text = "";
	elements.enterbutton.enabled = true;
	elements.fail.visible = true;
	application.updateUI();
}

/**
 * @properties={typeid:24,uuid:"61FCF98F-5B56-468C-BB58-55A7B39B01F4"}
 */
function local_getConnection(_host, _login, _password) {
	//	var _connection = plugins.it2be_ldapclient.getConnection(_host);
	//	_connection.c(_login);
	//	_connection.d(_password);
	//	return _connection;
}
