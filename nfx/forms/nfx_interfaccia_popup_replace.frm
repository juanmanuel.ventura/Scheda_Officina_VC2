dataSource:"db:/ferrari/k8_dummy",
items:[
{
beanClassName:"javax.swing.JList",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_15\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JList\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>list<\/string> \
  <\/void> \
  <void property=\"selectionMode\"> \
   <int>0<\/int> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"20,20",
name:"list",
size:"80,80",
typeid:12,
uuid:"3053A910-12B5-44D4-9713-419663B672EB"
},
{
height:300,
partType:5,
typeid:19,
uuid:"3879DEFF-DD02-42F2-B275-51BEBBC513FC"
},
{
anchors:6,
imageMediaID:"85582DF7-017C-4741-98E4-7C3C1142DF33",
location:"390,260",
mediaOptions:14,
onActionMethodID:"AA90BC87-5AF4-4AAC-AD5E-03522E178A92",
showClick:false,
size:"20,20",
transparent:true,
typeid:7,
uuid:"516444D6-20EE-45BB-B2D0-0566720224AC"
},
{
labelFor:"replacingFunction1",
location:"250,90",
mediaOptions:14,
size:"80,20",
styleClass:"form",
tabSeq:-1,
text:"Funzione",
transparent:true,
typeid:7,
uuid:"5CB0C343-76A0-4357-938E-CE03ABD33C25"
},
{
anchors:15,
dataProviderID:"replacingFunction",
displayType:1,
location:"250,110",
name:"replacingFunction1",
onActionMethodID:"-1",
onFocusGainedMethodID:"DA4801CB-890D-4685-97FD-015D24BBE620",
size:"170,140",
text:"Replacingfunction",
typeid:4,
uuid:"701705EA-AA17-441D-8540-6572DC52B622"
},
{
labelFor:"replacingValue1",
location:"250,40",
mediaOptions:14,
size:"80,20",
styleClass:"form",
tabSeq:-1,
text:"Valore",
transparent:true,
typeid:7,
uuid:"83E4B754-38DF-474C-B674-66B24E2539A7"
},
{
anchors:11,
dataProviderID:"replacingValue",
location:"250,60",
name:"replacingValue1",
onActionMethodID:"-1",
onFocusGainedMethodID:"D72535A1-F07E-4613-BF6D-159FD889A39B",
size:"170,20",
text:"Replacingvalue",
typeid:4,
uuid:"842B05EF-9FBC-49D2-966D-0DC1B1A001DD"
},
{
anchors:13,
beanClassName:"javax.swing.JScrollPane",
formIndex:-1,
location:"10,10",
name:"scroll",
size:"210,280",
typeid:12,
uuid:"CCADC4CD-19A9-45C9-B7C1-DE934AEFB0F3"
},
{
anchors:15,
borderType:"TitledBorder,Replace,4,0,DialogInput.plain,1,12,#333333",
lineSize:1,
location:"230,10",
name:"box",
size:"210,280",
transparent:true,
typeid:21,
uuid:"F9C25BD2-101B-4E26-B1C4-F62A15BAD544"
}
],
name:"nfx_interfaccia_popup_replace",
navigatorID:"-1",
onLoadMethodID:"899B0A44-5FB3-49CB-B035-4334A367CBF9",
onShowMethodID:"F4066D64-66DE-4905-952A-1CFC30BB1181",
paperPrintScale:100,
showInMenu:true,
size:"450,300",
styleName:"keeneight",
titleText:"Replace",
typeid:3,
uuid:"8B74DF4A-1941-46A7-BF1A-DA28227A941D"