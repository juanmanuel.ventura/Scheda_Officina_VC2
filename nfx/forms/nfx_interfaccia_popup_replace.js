/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"85120369-7109-4A92-83AC-52207A249B1C"}
 */
var replacingField = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"6391C06B-2B4F-4574-BFC0-82DC0D38C174"}
 */
var replacingForm = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B54F660A-08D1-4367-AC32-8F1F2580EA4B"}
 */
var replacingFunction = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"6CDA42F2-9C9D-4EC0-AA3D-A9A4959243F6"}
 */
var replacingValue = null;

/**
 *
 * @properties={typeid:24,uuid:"22429E89-1B3C-4971-B33C-4CE3BDE8F877"}
 */
function deHumanize(name) {

	if (name) {
		name = name.toLowerCase()
		name = name.replace(/ /g, "_");
	}
	return name;
}

/**
 *
 * @properties={typeid:24,uuid:"DA4801CB-890D-4685-97FD-015D24BBE620"}
 */
function functionFocusGained() {
	replacingValue = null;
}

/**
 *
 * @properties={typeid:24,uuid:"D9B5D16D-6A43-4120-BE3E-1272023DCC1B"}
 */
function humanize(name) {

	if (name) {
		name = name.replace(/_/g, " ");
		name = name.substr(0, 1).toUpperCase() + name.substr(1);
	}
	return name;
}

/**
 *
 * @properties={typeid:24,uuid:"899B0A44-5FB3-49CB-B035-4334A367CBF9"}
 */
function onLoad() {
	var vp = elements.scroll.getViewport();
	//SAuc
	//vp.add(elements.list);
	vp.add(java.awt.Component(elements.list));

	//elements.list.addMouseListener(selectField);
	elements.list.addMouseListener(java.awt.event.MouseListener(selectField));

}

/**
 *
 * @properties={typeid:24,uuid:"F4066D64-66DE-4905-952A-1CFC30BB1181"}
 */
function onShow(form) {
	replacingField = null;
	replacingValue = null;
	replacingFunction = null;
	elements.replacingValue1.enabled = false;
	elements.replacingFunction1.enabled = false;

	replacingForm = forms.nfx_interfaccia_pannello_buttons.replacingForm;

	var labels = new Array();
	for (var i = 0; i < forms[replacingForm].elements.allnames.length; i++) {
		if (forms[form].nfx_fieldMask && typeof forms[form].nfx_fieldMask == "object" && forms[form].nfx_fieldMask.indexOf(forms[replacingForm].elements.allnames[i]) == -1) {
			labels.push(humanize(forms[replacingForm].elements.allnames[i]));
		}
	}

	elements.list.setListData(labels);
}

/**
 *
 * @properties={typeid:24,uuid:"AA90BC87-5AF4-4AAC-AD5E-03522E178A92"}
 */
function save() {
	var max = databaseManager.getFoundSetCount(forms[replacingForm].foundset);
	var fs = forms[replacingForm].foundset.duplicateFoundSet();
	if (replacingValue && plugins.dialogs.showQuestionDialog("Raplace", "La modifica verrà applicata al campo \"" + humanize(replacingField) + "\" di tutti i " + max + " record selezionati. Procedere?", "Sì", "No") == "Sì") {
		globals.nfx_progressBarShow(max);
		for (var i = 1; i <= max; i++) {
			fs.setSelectedIndex(i);
			fs[replacingField] = replacingValue;
			globals.nfx_progressBarStep();
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"E831C74F-38A0-4299-ADD3-08907882E6BA"}
 */
function selectField() {
	if (elements.list.getSelectedValue()) {
		replacingField = deHumanize(elements.list.getSelectedValue());
		elements.replacingValue1.enabled = true;
		elements.replacingFunction1.enabled = true;
		elements.replacingValue1.requestFocus(true);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"D72535A1-F07E-4613-BF6D-159FD889A39B"}
 */
function valueFocusGained() {
	replacingValue = forms[replacingForm].foundset[replacingField];
	replacingFunction = null;
}
