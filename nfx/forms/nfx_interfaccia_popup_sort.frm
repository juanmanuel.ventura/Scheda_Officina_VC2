dataSource:"db:/ferrari/k8_dummy",
items:[
{
anchors:15,
beanClassName:"javax.swing.JScrollPane",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_17\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JScrollPane\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>210<\/int> \
    <int>230<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <object class=\"javax.swing.border.EtchedBorder\"> \
    <int>0<\/int> \
    <null/> \
    <null/> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>scrollOptions<\/string> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:-1,
location:"10,30",
name:"scrollOptions",
size:"210,230",
typeid:12,
uuid:"0741DC73-A9EE-4FA6-A51F-38C8428990E3"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"5804BE64-BD66-4BC7-B44A-3314D54EA001",
location:"228,130",
mediaOptions:2,
onActionMethodID:"8713E34A-8FE0-4D1F-8F5E-EC4720A38C58",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"1090271A-9D76-46E7-B532-ADE9032AF4DB"
},
{
anchors:7,
beanClassName:"javax.swing.JScrollPane",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_17\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JScrollPane\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>260<\/int> \
    <int>230<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <object class=\"javax.swing.border.EtchedBorder\"> \
    <int>0<\/int> \
    <null/> \
    <null/> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>scrollChoices<\/string> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:-2,
location:"260,30",
name:"scrollChoices",
size:"260,230",
typeid:12,
uuid:"15641715-B0FD-4B0E-A067-26079BB5CADE"
},
{
height:300,
partType:5,
typeid:19,
uuid:"41D667B2-E3C3-4C4A-8F0B-39FCB20A5E68"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"127B464F-994F-4572-810C-24FBC185DB64",
location:"228,90",
mediaOptions:2,
onActionMethodID:"4DB79E4E-25CE-4DC4-96DB-6EF284A8D864",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"50EF5111-DE50-45E9-A68A-EFD01E4375C6"
},
{
location:"10,10",
mediaOptions:14,
size:"80,20",
tabSeq:-1,
text:"Opzioni",
transparent:true,
typeid:7,
uuid:"6E56EDDA-C3DD-4D07-B4E2-EB6D8B10DEF6"
},
{
beanClassName:"javax.swing.JList",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.5.0_19\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JList\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>listOptions<\/string> \
  <\/void> \
  <void property=\"selectionMode\"> \
   <int>0<\/int> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"70,120",
name:"listOptions",
size:"80,80",
typeid:12,
uuid:"766F121D-80B7-447F-B4AA-18CBD20FF060"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"40905D44-A323-4D4C-9CCA-CC832C1E545D",
location:"228,60",
mediaOptions:2,
onActionMethodID:"FF32A9E1-8A4B-48CF-940C-370CB1A094AB",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"8D88D190-0E9E-4071-B77E-C586BE9B7C0A"
},
{
anchors:3,
location:"260,10",
mediaOptions:14,
size:"80,20",
tabSeq:-1,
text:"Scelte",
transparent:true,
typeid:7,
uuid:"935D7BAA-6AC0-47B5-8163-0FF12134A280"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"6AD2E909-E7A4-491A-92E5-D7E2F229B865",
location:"228,160",
mediaOptions:2,
onActionMethodID:"4CE441B0-3070-4A1B-BD66-942DBFB6A38E",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"D2F872D0-E67B-4538-82D5-C64C70F399F6"
},
{
anchors:6,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"85582DF7-017C-4741-98E4-7C3C1142DF33",
location:"460,267",
mediaOptions:2,
onActionMethodID:"EFA9AF3C-2280-463C-B64F-E4D70A7DAB8E",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"F19B138A-ED1D-4875-BA41-299597E15464"
},
{
beanClassName:"javax.swing.JList",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.5.0_19\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JList\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>listChoices<\/string> \
  <\/void> \
  <void property=\"selectionMode\"> \
   <int>0<\/int> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"330,120",
name:"listChoices",
size:"80,80",
typeid:12,
uuid:"F1A618EB-A78C-4D1E-BB35-FCA8498C7560"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"E6C052AF-2185-445F-932A-5F615B6333EC",
location:"228,200",
mediaOptions:2,
onActionMethodID:"EADE8D84-C92E-4051-ADD7-4534935F8566",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"F6BC1C58-05F6-4804-983E-8A2BA6838F53"
},
{
anchors:6,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"0746A208-4E27-45D4-BABE-FB96F24D516F",
location:"490,267",
mediaOptions:2,
onActionMethodID:"2F75EA65-06EC-4379-843D-9A2A4DCBC782",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"FDDF9C21-286B-40D0-AB23-A6C9163B5014"
}
],
name:"nfx_interfaccia_popup_sort",
navigatorID:"-1",
onHideMethodID:"6DD2116F-1A60-4AF0-8034-78CCBB4E0E20",
onLoadMethodID:"11304805-432B-41B5-813F-52C8B432EBF4",
onShowMethodID:"1A6BB3EE-9EF0-4EE3-8E19-C7CD4FFC6AF1",
paperPrintScale:100,
showInMenu:true,
size:"530,300",
styleName:"keeneight",
typeid:3,
uuid:"171336D2-3D00-4A43-B03B-E3947A968887"