/**
 * @properties={typeid:35,uuid:"73A393BE-1CC9-4325-BCEF-570E25CA0468",variableType:-4}
 */
var choices = [];

/**
 * @properties={typeid:35,uuid:"89E88DD7-D326-4738-8F33-B41E5055180B",variableType:-4}
 */
var choicesBK = new Array();

/**
 * @properties={typeid:35,uuid:"06D569DC-2652-42C4-B4BD-9D18BD489B12",variableType:-4}
 */
var labels = new Array();

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"3526EC20-E0D0-41FB-ADD7-85F8145828BD"}
 */
var orderCriteria = null;

/**
 *
 * @properties={typeid:24,uuid:"FF32A9E1-8A4B-48CF-940C-370CB1A094AB"}
 */
function add() {
	if (elements.listOptions.getSelectedValue() && choices.indexOf(elements.listOptions.getSelectedValue() + " | desc") == -1 && choices.indexOf(elements.listOptions.getSelectedValue() + " | asc") == -1) {
		choices.push(elements.listOptions.getSelectedValue() + " | desc");
		elements.listChoices.setListData(choices);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"2F75EA65-06EC-4379-843D-9A2A4DCBC782"}
 */
function cancel() {
	orderCriteria = null;

	//application.closeFormDialog();

	var w = controller.getWindow();
	w.destroy();
}

/**
 *
 * @properties={typeid:24,uuid:"EADE8D84-C92E-4051-ADD7-4534935F8566"}
 */
function clear() {
	choices = new Array();
	elements.listChoices.setListData(choices);
}

/**
 *
 * @properties={typeid:24,uuid:"E71930CB-A40F-469B-B8D5-B48E19790152"}
 */
function dcAdd(e) {

	if (e.getClickCount() == 2) {
		add();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"113C0D15-CCCB-49E7-83F6-863CF7FB4C81"}
 */
function dcChangeOrientation(e) {

	if (e.getClickCount() == 2 && elements.listChoices.getSelectedValue()) {
		var value = choices[choices.indexOf(elements.listChoices.getSelectedValue())].split(" | ");
		if (value[1] == "desc") {
			choices[choices.indexOf(elements.listChoices.getSelectedValue())] = choices[choices.indexOf(elements.listChoices.getSelectedValue())].replace("desc", "asc");
		} else {
			choices[choices.indexOf(elements.listChoices.getSelectedValue())] = choices[choices.indexOf(elements.listChoices.getSelectedValue())].replace("asc", "desc");
		}
		elements.listChoices.setListData(choices);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"4CE441B0-3070-4A1B-BD66-942DBFB6A38E"}
 */
function moveDown() {
	if (elements.listChoices.getSelectedValue() && choices.indexOf(elements.listChoices.getSelectedValue()) != choices.length - 1) {
		var index = choices.indexOf(elements.listChoices.getSelectedValue());
		var app = choices[index + 1];
		choices[index + 1] = choices[index];
		choices[index] = app;
		elements.listChoices.setListData(choices);
		elements.listChoices.setSelectedIndex(index + 1);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"8713E34A-8FE0-4D1F-8F5E-EC4720A38C58"}
 */
function moveUp() {
	if (elements.listChoices.getSelectedValue() && choices.indexOf(elements.listChoices.getSelectedValue()) != 0) {
		var index = choices.indexOf(elements.listChoices.getSelectedValue());
		var app = choices[index - 1];
		choices[index - 1] = choices[index];
		choices[index] = app;
		elements.listChoices.setListData(choices);
		elements.listChoices.setSelectedIndex(index - 1);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"6DD2116F-1A60-4AF0-8034-78CCBB4E0E20"}
 */
function onHide() {
	if (!orderCriteria) {
		choices = choicesBK.slice(null, null);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"11304805-432B-41B5-813F-52C8B432EBF4"}
 */
function onLoad() {
	//SAuc
	//elements.listOptions.addMouseListener(dcAdd);
	//elements.listChoices.addMouseListener(dcChangeOrientation);

	elements.listOptions.addMouseListener(java.awt.event.MouseListener(dcAdd));
	elements.listChoices.addMouseListener(java.awt.event.MouseListener(dcChangeOrientation));
}

/**
 *
 * @properties={typeid:24,uuid:"1A6BB3EE-9EF0-4EE3-8E19-C7CD4FFC6AF1"}
 */
function onShow() {
	orderCriteria = null;

	choicesBK = choices.slice(null, null);

	var vpOptions = elements.scrollOptions.getViewport();
	//SAuc
	//vpOptions.add(elements.listOptions);

	vpOptions.add(java.awt.Component(elements.listOptions))

	var vpChoices = elements.scrollChoices.getViewport();
	//SAuc
	//vpChoices.add(elements.listChoices);

	vpChoices.add(java.awt.Component(elements.listChoices));

	var form = forms.nfx_interfaccia_pannello_body.elements.main.getTabFormNameAt(1);

	labels = new Array();
	for (var i = 0; i < forms[form].elements.allnames.length; i++) {
		var label = forms[form].elements.allnames[i];
		label = label.replace(/_/g, " ")
		label = label.substr(0, 1).toUpperCase() + label.substr(1);
		labels.push(label);
	}

	elements.listOptions.setListData(labels);
	elements.listChoices.setListData(choices);
}

/**
 *
 * @properties={typeid:24,uuid:"4DB79E4E-25CE-4DC4-96DB-6EF284A8D864"}
 */
function remove() {
	if (elements.listChoices.getSelectedValue()) {
		choices.splice(choices.indexOf(elements.listChoices.getSelectedValue()), 1);
		elements.listChoices.setListData(choices);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"EFA9AF3C-2280-463C-B64F-E4D70A7DAB8E"}
 */
function save() {
	orderCriteria = "";
	for (var i = 0; i < choices.length; i++) {
		var criteria = choices[i].split(" |");
		criteria[0] = criteria[0].substr(0, 1).toLowerCase() + criteria[0].substr(1);
		criteria[0] = criteria[0].replace(/ /g, "_");
		criteria = criteria[0] + criteria[1];
		orderCriteria += criteria;
		if (i != choices.length - 1)
			orderCriteria += ",";
	}

	//application.closeFormDialog();

	var w = controller.getWindow();
	w.destroy();
}
