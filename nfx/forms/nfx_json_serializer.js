/**
 * @properties={typeid:35,uuid:"8A1F04AB-DDFF-45C2-B81B-BD788371E363",variableType:-4}
 */
var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;

/**
 * @properties={typeid:35,uuid:"5AA32E5C-4A6B-48DA-B6A7-A038F12E72FA",variableType:-4}
 */
var escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;

/**
 * @properties={typeid:35,uuid:"D20985E8-79EB-4D36-BDDF-B5C5860FC3BF",variableType:-4}
 */
var gap = null;

/**
 * @properties={typeid:35,uuid:"75F4BB7F-C84D-4E14-8F8B-E2CF8BBC42C7",variableType:-4}
 */
var indent = null;

/**
 * @properties={typeid:35,uuid:"3E2D9AB3-07F2-4AEE-994B-74D29B9BF76D",variableType:-4}
 */
var meta = { // table of character substitutions
	'\b': '\\b',
	'\t': '\\t',
	'\n': '\\n',
	'\f': '\\f',
	'\r': '\\r',
	'"': '\\"',
	'\\': '\\\\'
};

/**
 * @properties={typeid:35,uuid:"979A92DD-3303-47BE-A56C-B8C2030BD4D4",variableType:-4}
 */
var rep = null;

/**
 * @param {Object} value
 * @param {Object} [replacer]
 * @param {Object} [space]
 * @properties={typeid:24,uuid:"B3B6F010-8B18-4688-94B6-F0BD209A84ED"}
 */
function stringify(value, replacer, space) {
	// The stringify method takes a value and an optional replacer, and an optional
	// space parameter, and returns a JSON text. The replacer can be a function
	// that can replace values, or an array of strings that will select the keys.
	// A default replacer method can be provided. Use of the space parameter can
	// produce text that is more easily readable.

	var i;
	gap = '';
	indent = '';

	// If the space parameter is a number, make an indent string containing that
	// many spaces.

	if (typeof space === 'number') {
		for (i = 0; i < space; i += 1) {
			indent += ' ';
		}

		// If the space parameter is a string, it will be used as the indent string.

	} else if (typeof space === 'string') {
		indent = space;
	}

	// If there is a replacer, it must be a function or an array.
	// Otherwise, throw an error.

	rep = replacer;
	if (replacer && typeof replacer !== 'function' && (typeof replacer !== 'object' || typeof replacer.length !== 'number')) {
		throw new Error('JSON.stringify');
	}

	// Make a fake root object containing our value under the key of ''.
	// Return the result of stringifying the value.

	return str('', { '': value });
}

/**
 * @properties={typeid:24,uuid:"AE36D6FA-5F8C-4CB4-AE30-92BBD9C7054A"}
 */
function str(key, holder) {

	// Produce a string from holder[key].

	var i, // The loop counter.
		k, // The member key.
		v, // The member value.
		length,
		mind = gap,
		partial,
		value = holder[key];

	// If the value has a toJSON method, call it to obtain a replacement value.

	if (value && typeof value === 'object') {
		try {
			value = value.toJSON(key);
		} catch (e) {
			//application.output(e.message);
		}
	}

	if (value && value.constructor === Date) {
		value = dateToJSON(value, key);
	}

	// If we were called with a replacer function, then call the replacer to
	// obtain a replacement value.

	if (typeof rep === 'function') {
		value = rep.call(holder, key, value);
	}

	// What happens next depends on the value's type.

	switch (typeof value) {
	case 'string':
		return quote(value);

	case 'number':

		// JSON numbers must be finite. Encode non-finite numbers as null.

		//			JStaffa
		//			return isFinite(value) ? String(value) : 'null';
		return isFinite(utils.stringToNumber(value)) ? String(value) : 'null';

	case 'boolean':
	case 'null':

		// If the value is a boolean or null, convert it to a string. Note:
		// typeof null does not produce 'null'. The case is included here in
		// the remote chance that this gets fixed someday.

		return String(value);

	// If the type is 'object', we might be dealing with an object or an array or
	// null.

	case 'object':

		// Due to a specification blunder in ECMAScript, typeof null is 'object',
		// so watch out for that case.

		if (!value) {
			return 'null';
		}

		// Make an array to hold the partial results of stringifying this object value.

		gap += indent;
		partial = [];

		// Is the value an array?

		if (Object.prototype.toString.apply(value) === '[object Array]') {

			// The value is an array. Stringify every element. Use null as a placeholder
			// for non-JSON values.

			length = value.length;
			for (i = 0; i < length; i += 1) {
				partial[i] = str(i, value) || 'null';
			}

			// Join all of the elements together, separated with commas, and wrap them in
			// brackets.

			v = partial.length === 0 ? '[]' : gap ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']' : '[' + partial.join(',') + ']';
			gap = mind;
			return v;
		}

		// If the replacer is an array, use it to select the members to be stringified.

		if (rep && typeof rep === 'object') {
			length = rep.length;
			for (i = 0; i < length; i += 1) {
				k = rep[i];
				if (typeof k === 'string') {
					v = str(k, value);
					if (v) {
						partial.push(quote(k) + (gap ? ': ' : ':') + v);
					}
				}
			}
		} else {

			// Otherwise, iterate through all of the keys in the object.

			for (k in value) {
				if (Object.hasOwnProperty.call(value, k)) {
					v = str(k, value);
					if (v) {
						partial.push(quote(k) + (gap ? ': ' : ':') + v);
					}
				}
			}
		}

		// Join all of the member texts together, separated with commas,
		// and wrap them in braces.

		v = partial.length === 0 ? '{}' : gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}' : '{' + partial.join(',') + '}';
		gap = mind;
		return v;
	}
	return null;
}

/**
 * @properties={typeid:24,uuid:"CA892401-B15B-4692-938E-E276A427D311"}
 */
function quote(string) {

	// If the string contains no control characters, no quote characters, and no
	// backslash characters, then we can safely slap some quotes around it.
	// Otherwise we must also replace the offending characters with safe escape
	// sequences.

	escapable.lastIndex = 0;
	return escapable.test(string) ? '"' + string.replace(escapable, function(a) {
			var c = meta[a];
			return typeof c === 'string' ? c : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
		}) + '"' : '"' + string + '"';
}

/**@param {String} text
 * @param {String} [reviver]
 * @properties={typeid:24,uuid:"8FBB3536-A767-4D42-AEA7-F1C173022CE7"}
 */
function parse(text, reviver) {

	// The parse method takes a text and an optional reviver function, and returns
	// a JavaScript value if the text is a valid JSON text.

	var j;

	function walk(holder, key) {

		// The walk method is used to recursively walk the resulting structure so
		// that modifications can be made.

		var k, v, value = holder[key];
		if (value && typeof value === 'object') {
			for (k in value) {
				if (Object.hasOwnProperty.call(value, k)) {
					v = walk(value, k);
					if (v !== undefined) {
						value[k] = v;
					} else {
						delete value[k];
					}
				}
			}
		}
		return reviver.call(holder, key, value);
	}

	// Parsing happens in four stages. In the first stage, we replace certain
	// Unicode characters with escape sequences. JavaScript handles many characters
	// incorrectly, either silently deleting them, or treating them as line endings.

	cx.lastIndex = 0;
	if (cx.test(text)) {
		text = text.replace(cx, function(a) {
				return '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
			});
	}

	// In the second stage, we run the text against regular expressions that look
	// for non-JSON patterns. We are especially concerned with '()' and 'new'
	// because they can cause invocation, and '=' because it can cause mutation.
	// But just to be safe, we want to reject all unexpected forms.

	// We split the second stage into 4 regexp operations in order to work around
	// crippling inefficiencies in IE's and Safari's regexp engines. First we
	// replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
	// replace all simple value tokens with ']' characters. Third, we delete all
	// open brackets that follow a colon or comma or that begin the text. Finally,
	// we look to see that the remaining characters are only whitespace or ']' or
	// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

	if (/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

		// In the third stage we use the eval function to compile the text into a
		// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
		// in JavaScript: it can begin a block or an object literal. We wrap the text
		// in parens to eliminate the ambiguity.

		j = eval('(' + text + ')');

		// In the optional fourth stage, we recursively walk the new structure, passing
		// each name/value pair to a reviver function for possible transformation.

		return typeof reviver === 'function' ? walk({ '': j }, '') : j;
	}

	// If the text is not JSON parseable, then a SyntaxError is thrown.

	throw new SyntaxError('JSON.parse');
}

/**
 * @properties={typeid:24,uuid:"52D40123-1D04-4E43-A3F6-D28C0E3D27A9"}
 */
function dateToJSON(date, key) {
	return date.getUTCFullYear() + '-' + f(date.getUTCMonth() + 1) + '-' + f(date.getUTCDate()) + 'T' + f(date.getUTCHours()) + ':' + f(date.getUTCMinutes()) + ':' + f(date.getUTCSeconds()) + 'Z';
}

/**
 * @properties={typeid:24,uuid:"83FCBAD6-1F80-4247-995D-571369AEA87A"}
 */
function f(n) {
	// Format integers to have at least two digits.
	return n < 10 ? '0' + n : n;
}
