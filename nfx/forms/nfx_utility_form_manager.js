/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"4E1B4D01-B6DB-48E1-989F-B8DA46398496"}
 */
var nfx_orderBy = "titolo asc";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"37C8DAFF-265D-4AA0-B56B-CA857A13F61F"}
 */
var nfx_related = null;

/**
 * @properties={typeid:24,uuid:"7CCBD744-A53B-4631-BB34-514316FDE9E9"}
 */
function nfx_defineAccess(){
	return [false,true,true];
}

/**
 * @properties={typeid:24,uuid:"82E299AC-5932-48FE-A1CC-5A77CE24BF69"}
 */
function nfx_sks(){
	return [{icon: "refresh2.png", tooltip:"Riporta le impostazioni del form a quelle predefinite", method:"reset"}];
}

/**
 * @properties={typeid:24,uuid:"D4942E23-D814-4645-8121-EB9147E3950F"}
 */
function reset(){
	if(forms[nome]){
		var f = forms[nome];
		titolo = (f.nfx_getTitle && typeof f.nfx_getTitle == "function") ? f.nfx_getTitle() : null;
		principale = (f.nfx_isProgram && typeof f.nfx_isProgram == "function" && f.nfx_isProgram()) ? 1 : 0;
		permessi = (f.nfx_defaultPermissions && typeof f.nfx_defaultPermissions == "function") ? f.nfx_defaultPermissions() : "%";
	}else{
		foundset.deleteRecord();
	}
	databaseManager.saveData();
}
