dataSource:"db:/nfx/nfx_forms",
items:[
{
imageMediaID:"E6C052AF-2185-445F-932A-5F615B6333EC",
location:"170,40",
mediaOptions:2,
name:"clear",
onActionMethodID:"F81671FA-D58B-41AA-95F7-AE019884E640",
showClick:false,
size:"20,20",
tabSeq:-2,
transparent:true,
typeid:7,
uuid:"2F7D609A-1434-49EC-BC36-8D40405107A0"
},
{
imageMediaID:"81A07451-3060-4F42-84A4-136B9107003B",
location:"140,40",
mediaOptions:10,
name:"remove",
onActionMethodID:"3DD04C21-A2A6-466E-9C0E-0F25FF8664E5",
showClick:false,
size:"20,20",
tabSeq:-2,
transparent:true,
typeid:7,
uuid:"4ECE8E89-B54F-480C-9014-F83D7C4A3E0D"
},
{
imageMediaID:"CD8D63BB-A07B-44F1-8C6C-607ADB33202D",
location:"140,40",
mediaOptions:10,
name:"add",
onActionMethodID:"23DEBDBA-E3C6-41AB-941A-B06E3AF47D36",
showClick:false,
size:"20,20",
tabSeq:-2,
transparent:true,
typeid:7,
uuid:"8C65DC03-4124-41C6-BF84-256A75C20840"
},
{
height:70,
partType:5,
typeid:19,
uuid:"C7C0E54D-859F-4D46-B37F-F9793175259F"
},
{
dataProviderID:"user",
displayType:2,
editable:false,
location:"10,10",
name:"user",
onDataChangeMethodID:"4BB3D9FD-8803-4F0C-A95F-D392CEEF4AD1",
size:"190,20",
tabSeq:-2,
text:"User",
typeid:4,
uuid:"D8C1E832-DF83-4C7C-8A32-2F3CE1288D7D",
valuelistID:"7BC4B9B5-5A21-44CD-9E71-D1F8C56F9DA8"
},
{
beanClassName:"javax.swing.JPanel",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_18\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JPanel\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"doubleBuffered\"> \
   <boolean>false<\/boolean> \
  <\/void> \
  <void property=\"name\"> \
   <string>bean_963<\/string> \
  <\/void> \
  <void property=\"opaque\"> \
   <boolean>false<\/boolean> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"10,40",
name:"dummy",
size:"20,20",
typeid:12,
uuid:"DD94C0A2-0BA5-4197-8158-F35D3CD187F0"
}
],
name:"nfx_utility_form_manager_popup",
navigatorID:"-1",
onShowMethodID:"B45FFF50-CCE3-43A1-978F-B09D333872F3",
paperPrintScale:100,
showInMenu:true,
size:"210,70",
styleName:"keeneight",
typeid:3,
uuid:"DE27DFED-33BC-4972-9EF0-A7B43069E3AE"