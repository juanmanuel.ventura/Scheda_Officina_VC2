/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"FF35FCB1-432C-42CA-B10A-F79B8FF453E3"}
 */
var user = null;

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"B45FFF50-CCE3-43A1-978F-B09D333872F3"}
 */
function onShowForm(firstShow, event) {
	reset();
}

/**
 * @properties={typeid:24,uuid:"AF963573-202B-4112-95FB-F088ECE6885E"}
 */
function reset() {
	user = null;
	elements.dummy.requestDefaultFocus();
	elements.add.visible = false;
	elements.remove.visible = false;
	elements.clear.enabled = (permessi != "%" && permessi != "^") ? true : false;
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"4BB3D9FD-8803-4F0C-A95F-D392CEEF4AD1"}
 */
function onDataChange(oldValue, newValue, event) {
	if (newValue) {
		if (permessi.indexOf(user) == -1) {
			elements.add.visible = true;
			elements.remove.visible = false;
		} else {
			elements.add.visible = false;
			elements.remove.visible = true;
		}
	} else {
		reset();
	}
	return true;
}

/**
 * @properties={typeid:24,uuid:"23DEBDBA-E3C6-41AB-941A-B06E3AF47D36"}
 */
function add() {
	if (permessi == "%" || permessi == "^") {
		permessi = user;
	} else {
		permessi += "|" + user;
	}
	reset();

}

/**
 * @properties={typeid:24,uuid:"3DD04C21-A2A6-466E-9C0E-0F25FF8664E5"}
 */
function remove() {
	permessi = permessi.replace(user, "").replace(/^\|/, "").replace(/\|\|/g, "|").replace(/\|$/, "");
	if (permessi == "") {
		permessi = (forms[nome].nfx_defaultPermissions && typeof forms[nome].nfx_defaultPermissions == "function") ? forms[nome].nfx_defaultPermissions() : "%";
	}
	reset();

}

/**
 * @properties={typeid:24,uuid:"F81671FA-D58B-41AA-95F7-AE019884E640"}
 */
function clear() {
	permessi = (forms[nome].nfx_defaultPermissions && typeof forms[nome].nfx_defaultPermissions == "function") ? forms[nome].nfx_defaultPermissions() : "%";
	reset();
}
