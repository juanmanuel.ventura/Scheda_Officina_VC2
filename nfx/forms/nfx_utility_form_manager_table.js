/**
 * @properties={typeid:24,uuid:"6A3167B4-D2B4-4A68-BD53-6F47CF2809EF"}
 */
function nfx_getTitle() {
	return "Gestione Form";
}

/**
 * @properties={typeid:24,uuid:"588EF130-C37B-420B-A4C6-0A86ABABDEF9"}
 */
function nfx_isProgram() {
	return true;
}

/**
 * @properties={typeid:24,uuid:"F857166C-D202-45F6-96ED-1891FB58A2C1"}
 */
function nfx_defaultPermissions() {
	return "^";
}

/**
 * @properties={typeid:24,uuid:"63340AFA-53A5-406F-AD4C-6F90857944F2"}
 */
function openManager() {
	if (principale) {

		//application.showFormInDialog(forms.nfx_utility_form_manager_popup,null,null,null,null,"Permessi",false,false,"permissions_popup",true);

		var formq = forms.nfx_utility_form_manager_popup;
		var window = application.createWindow("permissions_popup", JSWindow.MODAL_DIALOG);
		window.title="Permessi";
		window.showTextToolbar(false);
		window.resizable=false;
		formq.controller.show(window);

		databaseManager.saveData();
	}
}

/**
 * @properties={typeid:24,uuid:"D57062E0-BE69-4062-A53E-EB4E7DB2BC51"}
 */
function nfx_postEdit() {
	forms.nfx_interfaccia_pannello_tree.updateFormsInfo();
	forms.nfx_interfaccia_popup_filltree.first = true;
}
