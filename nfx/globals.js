/**
 * @properties={typeid:35,uuid:"D17E4AF1-55AE-4AEE-8776-D5D84C5D182B",variableType:-4}
 */
var preNum = null;

/**
 * @type {Date}
 * @properties={typeid:35,uuid:"8F176BE2-AC01-45E6-BC74-D7D81DC38DF0",variableType:93}
 */
var openFormTime = new Date();

/**@type {Date}
 * @properties={typeid:35,uuid:"B81BED1C-423F-466D-9E28-BC1F7404F27D",variableType:93}
 */
var openedFormTime = new Date();

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"58EB9850-802C-4E4C-8975-4621B6C4639B",variableType:4}
 */
var infinityrepaint = 0;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"ACCD975B-16C2-4CD5-811B-324693A4B903",variableType:4}
 */
var _maxSKS = 10;

/**
 * @properties={typeid:35,uuid:"CB1D9369-9F36-47D0-A0C6-B30DA188CC4C",variableType:-4}
 */
var firstClick = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"93D822C1-07BA-44A8-A3DA-4FB977142E34"}
 */
var nfx_customActiomName = "";

/**
 * @properties={typeid:35,uuid:"D50115A3-4584-440D-83EC-EB9D0CF199C2",variableType:-4}
 */
var nfx_developers = ["sauciello", "dfrau"];

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"512907CC-6BAB-4C47-B936-35CA23DA81BE"}
 */
var nfx_email = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"4D77F969-E7E3-4E4A-8D2C-5792236FD976",variableType:93}
 */
var nfx_lastActivity = null;

/**@type {Packages.javax.swing.tree.DefaultMutableTreeNode}
 * @properties={typeid:35,uuid:"9BD75595-2174-4A95-9EDC-2128FFC7A8B5",variableType:-4}
 */
var nfx_lastNavigatorTreeNodeResult = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"4932A924-56C1-4C8B-9CE1-65E44B0F7035",variableType:4}
 */
var nfx_minPasswordLength = 8;

/**
 * @properties={typeid:35,uuid:"EB11F822-D89A-4633-9226-87CC3A216224",variableType:-4}
 */
var nfx_udpPacketReceivedCallbacks = [];

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"076FDE99-9794-44F8-9EF4-BBA876402D82",variableType:4}
 */
var nfx_udpPort = 2828;

/**
 * JStaffa aggiunto cast a string della var nfx_user
 * @type {String}
 * @properties={typeid:35,uuid:"C3894E3D-0D78-420B-8DCD-059BE9D0DFDE"}
 */
var nfx_user = null;

/**
 * @properties={typeid:35,uuid:"E461EF26-7468-414D-BFE6-C2E8DD54BFBF",variableType:-4}
 */
var windowprog = null;

/**JStaffa creata variabile
 * @properties={typeid:35,uuid:"5EFCF398-AF32-49FA-B279-43B3C28963D3",variableType:-4}
 */
var nextTree = null;

/**
 * @properties={typeid:35,uuid:"BD54E6F2-C5A8-4D61-B584-C8BFBD5CAB13",variableType:-4}
 */
var formqprog = null;

/**
 * @properties={typeid:35,uuid:"FD826DD2-127D-4356-97EA-9FB9423E865F",variableType:-4}
 */
var arrayNomi = new Array();

/**
 * @properties={typeid:35,uuid:"631FA087-E930-4FE2-BC1D-2893A1081606",variableType:-4}
 */
var firstExpand = true;

/**
 * @properties={typeid:35,uuid:"149EF4FE-A5F2-4A3B-947F-7A28978581C2",variableType:-4}
 */
var allnodes = new Array();

/**
 * @properties={typeid:35,uuid:"269FD858-C192-473B-A31F-5BC65537DFE1",variableType:-4}
 */
var relations = new Array();

/**
 * @properties={typeid:35,uuid:"9CAB6CC5-4F33-4A91-A4F1-A973F52CA486",variableType:-4}
 */
var nameAllRel = new Array();

/**
 *
 * @properties={typeid:24,uuid:"2E2E7C6A-7912-41A9-A75D-D1276C8D7B48"}
 */
function nfx_addSpecialButton(a, b, c) {
	/*
	 * @author: Omar Brescianini
	 *
	 * Permette di definire un bottone con una funzione personalizzata ed
	 * aggiungerlo alla barra di navigazione.
	 */
	var icon = a || null;
	var tooltip = b || null;
	var method = c || null;

	forms.nfx_interfaccia_pannello_buttons.addSpecialButton(icon, tooltip, method);

}

/**
 *
 * @properties={typeid:24,uuid:"524DDB27-DECE-49DC-90C6-5407D75B8CD8"}
 */
function nfx_addUdpPacketReceivedCallback(a) {
	if (typeof a === 'function') {
		globals.nfx_udpPacketReceivedCallbacks.push(a);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"41532D2F-0A78-40F4-94B5-5280AE4E14B9"}
 */
function nfx_applicationOnClose() {
	//	var f = forms.nfx_interfaccia_gadget_chat;
	//	if (f.logged) {
	//		f.logout();
	//	} else {
	//		globals.nfx_setUserInactive();
	//	}

	globals.nfx_setUserInactive();
	
	var pk = databaseManager.getDataSetByQuery('nfx', "select max( nfx_acces_id) from NFX_ACCESS", [], -1).getValue(1,1);
	
	forms.nfx_interfaccia_gestione_accessi.controller.newRecord();
	forms.nfx_interfaccia_gestione_accessi.nfx_acces_id = pk;	
	forms.nfx_interfaccia_gestione_accessi.login_name = nfx_user;
	forms.nfx_interfaccia_gestione_accessi.date_time = new Date();
	forms.nfx_interfaccia_gestione_accessi.db_action = "logout";
	databaseManager.saveData();
	application.output("Logout : " + globals.nfx_user);
}

/**
 *
 * @properties={typeid:24,uuid:"6EA7A45E-7D4E-4741-B96B-FBC8CBC6E07E"}
 * @AllowToRunInFind
 */
function nfx_applicationOnOpen() {
	
	

	application.putClientProperty(APP_UI_PROPERTY.VALUELIST_MAX_ROWS, 1000);

	preNum = new Array();

	globals.nfx_user = security.getUserName();

	/** @type {JSFoundSet<db:/ferrari/k8_gestione_utenze>} */
	var users = databaseManager.getFoundSet('ferrari', 'k8_gestione_utenze');

	if (users.find()) {

		users.user_servoy = globals.nfx_user;

		var found = users.search();

		if (found > 0) {

			globals.nfx_email = users.email;
			application.output("Login : " + globals.nfx_user);

		}
	}

	globals["vc2_setSystemPaths"]();

	//Start UDP
	globals.nfx_addUdpPacketReceivedCallback(globals.nfx_defaultUdpCallback);
	plugins.udp.startSocket(globals.nfx_udpPort, globals.nfx_udpPacketReceived);

	getNodeName();

	relations = solutionModel.getRelations(null);
	for (var i3 = 0; i3 < globals.relations.length; i3++) {
		nameAllRel[i3] = globals.relations[i3].name;
	}

	firstExpand = false;

}

/**
 * @properties={typeid:24,uuid:"2E7A7A96-D463-4272-BEA1-3971D02D9A7C"}
 */
function getNodeName() {
	
	allnodes = new Array();

	try {
		arrayNomi = new Array();

		/** @type {javax.swing.tree.DefaultTreeModel} */
		var tree = forms.nfx_interfaccia_pannello_tree.elements.tree.getModel();

		/** @type {javax.swing.tree.DefaultMutableTreeNode} */
		var nodo = tree.getRoot();

		var i = 0;
		var InitialSituation = new Array();
		while (nodo != null) {
			InitialSituation[i] = forms.nfx_interfaccia_pannello_tree.elements.tree.isExpanded(i);
			//forms.nfx_interfaccia_pannello_tree.elements.tree.expandRow(i);
			var nameBack = nodo.userObject;
			var name = nodo.userObject;
			var nameFromTitle = nfx_getNameFromTitle(name);
			if (nameFromTitle == null) {
				arrayNomi.push(nameBack, i);
				i++;
				if (firstExpand) {
					//forms.nfx_interfaccia_pannello_tree.elements.tree.collapseRow(i);
				}
			} else {
				arrayNomi.push(nameFromTitle, i);
				i++;
			}
			allnodes.push(nodo);
			nodo = nodo.getNextNode();

			if (arrayNomi[0] == "JTree") {
				//arrayNomi = new Array();
				//InitialSituation = new Array();
				//allnodes = new Array();
				i = 0;
			}
		}
	} catch (e) {
		application.output("Errore Indici");
	}

	return;
}

/**
 *@param {java.awt.dnd.DropTargetDragEvent} dtde
 *@param{Packages.javax.swing.JTree} tree
 * @properties={typeid:24,uuid:"E9F838C5-A4ED-4328-83F7-E58BC5B9A6AB"}
 */
function nfx_autoscrollTree(dtde, tree) {
	var p = dtde.getLocation();
	var pointedRow = tree.getClosestRowForLocation(p.x, p.y);
	var bounds = tree.getBounds();
	var margin = tree.getRowHeight();
	var row = (p.y + bounds.y <= margin ? pointedRow < 1 ? 0 : pointedRow - 1 : pointedRow < tree.getRowCount() - 1 ? pointedRow + 1 : pointedRow);
	tree.scrollRowToVisible(row);
	return p;
}

/**
 *
 * @properties={typeid:24,uuid:"5DE177B9-1AE6-4D70-BB60-6FE0CB9A325D"}
 */
function nfx_canAddRecord(form) {
	/*
	 * @author: Omar Brescianini
	 *
	 * @return type: boolean
	 *
	 * Controlla che sul form passato in ingresso sia abilitata l'aggiunta di record
	 */

	var main = forms.nfx_interfaccia_pannello_body.mainForm;
	var relatedSelected = forms.nfx_interfaccia_pannello_body.relatedSelected;

	if (form == relatedSelected) {
		if (forms[main].nfx_defineAccess && typeof forms[main].nfx_defineAccess == "function") {
			var permissions = forms[main].nfx_defineAccess()
			if (permissions[3]) {
				return false;
			}
		}
	}

	/*
	 * TIPO: function
	 * TIPO DI RITORNO: [boolean,boolean,boolean]
	 *
	 * Funzione che definisce i permessi sul form:
	 * - "return[0] == true" = possibilitÃ  di aggiunta
	 * - "return[1] == true" = possibilitÃ  di cancellazione
	 * - "return[2] == true" = possibilitÃ  di modifica
	 */
	if (forms[form] && forms[form].nfx_defineAccess && typeof forms[form].nfx_defineAccess == "function") {
		var permissions1 = forms[form].nfx_defineAccess();
		if (permissions1[0] == true) {
			return true;
		}
	}
	return null;
}

/**
 *@param {String} form
 * @properties={typeid:24,uuid:"874708AA-9CFA-466D-A62D-FA72345068D8"}
 */
function nfx_canEditRecord(form) {
	/*
	 * @author: Omar Brescianini
	 *
	 * @return type: boolean
	 *
	 * Controlla che sul form passato in ingresso sia abilitata la modifica di record
	 */
	var main = forms.nfx_interfaccia_pannello_body.mainForm;
	var relatedSelected = forms.nfx_interfaccia_pannello_body.relatedSelected;

	if (form == relatedSelected) {
		if (forms[main].nfx_defineAccess && typeof forms[main].nfx_defineAccess == "function") {
			var permissions = forms[main].nfx_defineAccess()
			if (permissions[3]) {
				return false;
			}
		}
	}

	/*
	 * TIPO: function
	 * TIPO DI RITORNO: [boolean,boolean,boolean]
	 *
	 * Funzione che definisce i permessi sul form:
	 * - "return[0] == true" = possibilità di aggiunta
	 * - "return[1] == true" = possibilità di cancellazione
	 * - "return[2] == true" = possibilità di modifica
	 */
	if (forms[form] && forms[form].nfx_defineAccess && typeof forms[form].nfx_defineAccess == "function") {
		var permissions1 = forms[form].nfx_defineAccess();
		if (permissions1[2] == true) {
			return true;
		}
	}
	return null;
}

/**
 *@param {String} form
 * @properties={typeid:24,uuid:"A808EA73-3373-4EC4-97B8-C44FFAC10CE4"}
 */
function nfx_canRemoveRecord(form) {
	/*
	 * @author: Omar Brescianini
	 *
	 * @return type: boolean
	 *
	 * Controlla che sul form passato in ingresso sia abilitata l'eliminazione di record
	 */

	var main = forms.nfx_interfaccia_pannello_body.mainForm;
	var relatedSelected = forms.nfx_interfaccia_pannello_body.relatedSelected;

	if (form == relatedSelected) {
		if (forms[main].nfx_defineAccess && typeof forms[main].nfx_defineAccess == "function") {
			var permissions = forms[main].nfx_defineAccess()
			if (permissions[3]) {
				return false;
			}
		}
	}

	/*
	 * TIPO: function
	 * TIPO DI RITORNO: [boolean,boolean,boolean]
	 *
	 * Funzione che definisce i permessi sul form:
	 * - "return[0] == true" = possibilità di aggiunta
	 * - "return[1] == true" = possibilità di cancellazione
	 * - "return[2] == true" = possibilità di modifica
	 */
	if (forms[form] && forms[form].nfx_defineAccess && typeof forms[form].nfx_defineAccess == "function") {
		var permissions1 = forms[form].nfx_defineAccess();
		if (permissions1[1] == true) {
			return true;
		}
	}
	return null;
}

/**
 *
 * @properties={typeid:24,uuid:"DCE198A8-5980-4138-9890-811304228509"}
 */
function nfx_clearUdpPacketReceivedCallback() {
	globals.nfx_udpPacketReceivedCallbacks = [];
}

/**
 *
 * @properties={typeid:24,uuid:"556C8501-9CB7-4EE8-98DF-51B8DCFBFCE2"}
 */
function nfx_defaultUdpCallback(message, userIp) {
	var c = forms.nfx_interfaccia_gadget_chat;
	c.updateList();
	var name = c.getNameFromIp(userIp);
	var user = c.getObject(name);
	if (user) {
		user.chat += "<p style='margin:0;padding:2px;text-align:left;color:#000000;'>" + message + "</p>";
		if (user.user == c.chatWith) {
			c.setBoardContent(user.chat);
		} else {
			user.unread = true;
		}
	} else {
		application.output("L'utente non e' stato trovato...");
	}
}

/**
 *
 * @properties={typeid:24,uuid:"1034CDF5-20A1-41EF-A9BE-0FDD63C71538"}
 */
function nfx_getBaseName(name) {
	/*
	 * @author: Omar Brescianini
	 *
	 * @return type: string
	 *
	 * Restituisce il nome di un form DINAMICO privandolo del suffisso
	 */

	/** @type {String} */
	if (name) {
		var split = name.split("_");
		var suffix = split[split.length - 1];
		if (suffix == "record" || suffix == "list" || suffix == "table") {
			return name.replace("_" + suffix, "");
		}
		return name;
	} else {
		return null;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"E83F0A36-0835-4199-BA06-79E5A2690313"}
 */
function nfx_getNameFromTitle(title) {
	var names = forms.nfx_interfaccia_pannello_tree.names;
	var titles = forms.nfx_interfaccia_pannello_tree.titles;

	var index = titles.indexOf(title);
	if (index != -1) {
		return names[index];
	} else {
		//application.output("Nessun nome associato a: " + title);
		return null;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"2A9DBAD1-254B-4E17-A042-08EAF2A469D8"}
 */
function nfx_getNavigatorTreeNode(t, programName, currentNode) {
	//FS
	/** @type {Packages.javax.swing.tree} */
	var node = null;

	if (!currentNode) {
		nfx_lastNavigatorTreeNodeResult = null;
		//FS
		/** @type {Packages.javax.swing.tree} */
		var model = t.getModel();
		currentNode = model.getRoot();
	}
	for (var i = 0; i < currentNode.getChildCount(); i++) {
		node = currentNode.getChildAt(i);
		if (node.getUserObject() == programName) {
			globals.nfx_lastNavigatorTreeNodeResult = node;
			return null;
		}

		if (node.getChildCount() > 0 && !nfx_lastNavigatorTreeNodeResult) {
			nfx_getNavigatorTreeNode(t, programName, node);
		}
	}
	if (nfx_lastNavigatorTreeNodeResult) {
		return nfx_lastNavigatorTreeNodeResult;
	}
	return null;
}

/**
 *
 *@return {String}
 * @properties={typeid:24,uuid:"350CC7FB-EE18-4843-87C0-E00D774F865B"}
 */
function nfx_getTitleFormName(name) {

	var names = forms.nfx_interfaccia_pannello_tree.names;
	var titles = forms.nfx_interfaccia_pannello_tree.titles;

	var index = names.indexOf(name);
	if (index != -1) {
		return titles[index];
	} else {
		//application.output("Nessun titolo associato a: " + name);
		return null;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"9A71B9A4-8DFB-46D8-9B9C-9D2C6EDEE80E"}
 */
function nfx_getTreeIconRenderer() {
	var tcrImpl = { getTreeCellRendererComponent: forms.nfx_interfaccia_pannello_tree.getLabel };
	return new Packages.javax.swing.tree.TreeCellRenderer(tcrImpl);
}

/**
 *@param {String} go
 * @properties={typeid:24,uuid:"E70D3D27-ED3A-4CAB-9026-08DAF255711C"}
 * @AllowToRunInFind
 */
function nfx_goTo(go) {

	globals.nfx_setBusy(true);

	getNodeName();

	/*
	 Accetta il titolo o il nome del form
	 */
	if (!go) return;
	var t = forms.nfx_interfaccia_pannello_tree.elements.tree;
	var name = go;
	if (forms.nfx_interfaccia_pannello_body.elements.main.getMaxTabIndex() != 0 && name == forms.nfx_interfaccia_pannello_body.elements.main.getTabFormNameAt(1)) {
		return;
	}
	/*Prova a rendere trasparente il formato:
	 titolo => diretto
	 nome_form => prende il titolo da se
	 */
	try {
		name = forms[name].nfx_getTitle();
	} catch (e) { /*do nothing*/
	}
	;
	nfx_getNavigatorTreeNode(t, name, null);
	if (globals.nfx_lastNavigatorTreeNodeResult) {

		var Path = nfx_lastNavigatorTreeNodeResult.getPath();

		var example = t.getSelectionPath();

		if (example == null) {
			t.setSelectionRow(0);
			example = t.getSelectionPath();
		}

		while (example.getParentPath()) {
			example = example.getParentPath();
		}

		var nodiDaAdd = new Array();

		for (var i = 1; i < Path.length; i++) {
			var daCerc = Path[i].userObject;
			for (var i3 = 0; i3 < allnodes.length; i3++) {
				if (daCerc == allnodes[i3].userObject) {
					nodiDaAdd.push(allnodes[i3]);
				}
			}
		}
		for (var i2 = 0; i2 < nodiDaAdd.length; i2++) {
			example = example.pathByAddingChild(nodiDaAdd[i2]);
		}

		t.setSelectionPath(example);
		application.updateUI();
		globals.nfx_setBusy(false);
		return;
	} else {
		plugins.dialogs.showInfoDialog("Form irraggiungibile", "Il form \"" + name + "\" non è presente nell'albero di navigazione.\nPer utilizzare questa funzionalità  è necessario aggiungerlo", "Ok");
		globals.nfx_setBusy(false);
		return;
	}

}

/**
 *
 * @properties={typeid:24,uuid:"60F0C554-77FF-408E-8D65-46AD86936DE3"}
 * @AllowToRunInFind
 */
function nfx_goToProgramAndRecord(program, recordId, idFieldName) {

	/** @type {JSForm} */
	var f = forms[program];

	if (f.foundset.selectRecord(recordId)) {
		return nfx_goTo(f.nfx_getTitle());
	} else if (f.controller.find()) {
		f[idFieldName] = recordId;
		f.controller.search(true);
		if (f.foundset.getSize() > 0) {
			return nfx_goTo(f.nfx_getTitle());
		} else {
			return -1;
		}
	}

	return null;
}

/**
 * @properties={typeid:24,uuid:"A1B31180-5675-4465-9343-088EF28E9EDD"}
 */
function nfx_checkAndAddGoto(form) {
	if (!nfx_isFormInNavigatorTree(form)) {
		nfx_navigatorTreeAutoAdd(form);
	}
	//SAuc
	//globals.alternate = true;
	//globals.toGoTo = form;
	globals.nfx_goTo(form);
	//globals.alternate = false;
	return;
}

/**
 * @properties={typeid:24,uuid:"FA2BB268-F65F-4AA4-9416-E47C23919099"}
 */
function nfx_isFormInNavigatorTree(form) {
	var _c = nfx_getNavigatorTreeContent();
	return (_c.index.indexOf(form) != -1) ? true : false;
}

/**@return {Object}
 * @properties={typeid:24,uuid:"19594016-335A-4A12-BEB6-33AE45642352"}
 */
function nfx_getNavigatorTreeContent() {
	return forms.nfx_interfaccia_popup_filltree.choices;
}

/**@param {String} form
 * @properties={typeid:24,uuid:"C0585C81-5E59-49E0-90DE-7ABE0F6945BC"}
 */
function nfx_navigatorTreeAutoAdd(form) {

	var _c = nfx_getNavigatorTreeContent();
	var _folder = "Auto-aggiunte " + utils.dateFormat(new Date(), "dd-MM-yyyy");
	_c.children.push({ children: [{ alias: null, name: form, type: "node" }], name: _folder, type: "folder" });
	_c.index.push(_folder, form);
	nfx_navigatorTreeSaveAndApply();
}

/**
 * @properties={typeid:24,uuid:"4AC7C217-027C-41A1-81FB-3843D901FE12"}
 */
function nfx_getNavigatorTreeAddingForm() {
	return ["gestione_account_record", "report_anomalie_container", "albero_progetti_lite", "albero_progetti_full"];
}

/**
 * @properties={typeid:24,uuid:"84551B72-0BE7-449F-A160-6690794C042B"}
 */
function nfx_navigatorTreeAutoUpdate() {
	//Inizializzazione dello storico auto-aggiunte
	var _autoAddedTreeNodes = new Array();
	if (!forms.nfx_interfaccia_gestione_salvataggi.load("auto_added_tree_nodes")) {
		forms.nfx_interfaccia_gestione_salvataggi.save(_autoAddedTreeNodes, "auto_added_tree_nodes");
	} else {
		_autoAddedTreeNodes = forms.nfx_interfaccia_gestione_salvataggi.load("auto_added_tree_nodes");
	}

	//Aggiornamento navigator-tree + storico
	var _forms = nfx_getNavigatorTreeAddingForm();
	var _content = nfx_getNavigatorTreeContent();
	var _folder = "Auto-aggiunte " + utils.dateFormat(new Date(), "dd-MM-yyyy");
	var _object = { children: [], name: _folder, type: "folder" }
	var _index = [_folder];

	var _somethingToAdd = 0;

	//prova
	if (_autoAddedTreeNodes == '' || _autoAddedTreeNodes == null)_autoAddedTreeNodes = new Array();

	for (var i = 0; i < _forms.length; i++) {
		if (_autoAddedTreeNodes.indexOf(_forms[i]) == -1) {
			_autoAddedTreeNodes.push(_forms[i]);
			if (!nfx_isFormInNavigatorTree(_forms[i])) {
				_object.children.push({ alias: null, name: _forms[i], type: "node" });
				_index.push(_forms[i]);
				_somethingToAdd++;
			}
		}
	}

	if (_somethingToAdd) {
		_content.children.push(_object);
		for (var i2 = 0; i2 < _index.length; i2++) {
			_content.index.push(_index[i2]);
		}
		nfx_navigatorTreeSaveAndApply();
	}

	forms.nfx_interfaccia_gestione_salvataggi.save(_autoAddedTreeNodes, "auto_added_tree_nodes");
}

/**
 * @properties={typeid:24,uuid:"1DB4313D-E9D5-4B20-A680-E51020A21E30"}
 */
function nfx_navigatorTreeSaveAndApply() {
	forms.nfx_interfaccia_popup_filltree.saveTree();
	forms.nfx_interfaccia_pannello_tree.fillTree(null, null);
}

/**
 *
 * @properties={typeid:24,uuid:"924CA607-8971-403B-BA4C-946F0FDADA5D"}
 */
function nfx_hideMenu() {
	if (!application.isInDeveloper()) {
		//		JStaffa sistemati i deprecati
		//		plugins.kioskmode.setMenuVisible(false);
		//		plugins.kioskmode.setToolBarVisible(false);
		plugins.window.setToolBarAreaVisible(false);
		plugins.window.getMenuBar().setVisible(false);
	}
	//	var _w = Math.max(application.getWindowWidth(), 1280);
	//	var _h = Math.max(application.getWindowHeight(), 720);
	//	application.setWindowSize(_w, _h);

	//	JStaffa
	try {
		var activeWindow = application.getActiveWindow();
		var _w = Math.max(activeWindow.getWidth(), 1280);
		var _h = Math.max(activeWindow.getHeight(), 720);
		activeWindow.setSize(_w, _h);
	} catch (Ex) {
	}
}

/**
 *
 * @properties={typeid:24,uuid:"0330FA9B-8E40-4838-B477-89E80B91DC13"}
 */
function nfx_hideRelated() {
	/*
	 * @author: Omar Brescianini
	 *
	 * Metodo che definisce la logica di visualizzazione di un form quando
	 * questo non ha definito alcun record correlato
	 */

	//	JStaffa
	var activeWindows = application.getActiveWindow();

	forms.nfx_interfaccia_pannello_body.elements.related.removeAllTabs();
	//forms.nfx_interfaccia_pannello_body.elements.related.setSize(forms.nfx_interfaccia_pannello_body.elements.related.getWidth(),0);
	//forms.nfx_interfaccia_pannello_body.elements.main.setSize(forms.nfx_interfaccia_pannello_body.elements.related.getWidth(),forms.nfx_interfaccia_pannello_base.elements.body.getHeight());

	//	JStaffa
	//	forms.nfx_interfaccia_pannello_body.elements.main_split_pane.dividerLocation = application.getWindowHeight();
	forms.nfx_interfaccia_pannello_body.elements.main_split_pane.dividerLocation = activeWindows.getHeight();

	forms.nfx_interfaccia_pannello_body.elements.main_split_pane.setEnabled(false);
	//application.updateUI(100);
}

/**
 *
 * @properties={typeid:24,uuid:"D7733CDB-6B91-4C0B-A279-E35CBF56BD61"}
 */
function nfx_isDynamic(name) {
	/*
	 * @author: Omar Brescianini
	 *
	 * @return type: string
	 *
	 * Verifica il tipo di un form passato in ingresso
	 * Torna null se è di tipo statico (grafici, alberi etc etc)
	 */

	/** @type {String} */
	var split = name.split("_");
	var type = split[split.length - 1];
	if (type == "record" || type == "list" || type == "table") {
		return type;
	}
	return null;
}

/**
 *
 * @param {JSEvent} frm the event that triggered the action
 * @properties={typeid:24,uuid:"7621013B-45FC-454C-88BE-EF2DC5A5C0E0"}
 */
function nfx_onHide(frm) {
	/*
	 * @author: Omar Brescianini
	 *
	 * Metodo che definisce la logica base di nascondimento di un
	 * form all'interno di NFX
	 */

	//var form = application.getMethodTriggerFormName();

	var form = frm.getFormName();

	if (form == forms.nfx_interfaccia_pannello_body.mainForm) {
		nfx_onHide_asMain(form);
	} else {
		nfx_onHide_asRelated();
	}

	/*
	 * TIPO: function
	 * Se all'interno del form viene definito 'nfx_onHide' questo viene eseguito
	 * in modo da poter utilizzare all'occorenza una logica diversa da quella
	 * definita da NFX
	 */
	if (forms[form].nfx_onHide && typeof forms[form].nfx_onHide == "function") {
		forms[form].nfx_onHide();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"F28B162C-9209-4665-82AA-C6CABDD81509"}
 */
function nfx_onHide_asMain(form) {
	/*
	 * @author: Omar Brescianini
	 *
	 * Metodo che definisce la logica di nascondimento di un form
	 * all'interno di NFX quando questo è visualizzato come principale
	 */

	/*
	 * TIPO: array
	 * Attributo che definisce gli eventuali form correlati a quello mostrato
	 */
	if (forms[form].nfx_related) {
		forms.nfx_interfaccia_pannello_body.elements.related.removeAllTabs();
	}

	forms.nfx_interfaccia_pannello_buttons.hideNavButtons();
	forms.nfx_interfaccia_pannello_buttons.removeSpecialButtons();

	/*
	 * TIPO: integer
	 * Se definito nel form, questo attributo permette di tenere traccia della scheda
	 * visualizzata in maniera da poterla riproporre al ritorno su form
	 */
	if (forms[form].nfx_toViewOnBack) {
		forms[form].nfx_toViewOnBack = forms[form].controller.getSelectedIndex();
	}

	forms.nfx_interfaccia_pannello_body.mainForm = null;
	forms.nfx_interfaccia_pannello_body.relatedSelected = null;
}

/**
 *
 * @properties={typeid:24,uuid:"59453FBA-F267-4641-BDE4-7023131CC106"}
 */
function nfx_onHide_asRelated() {
	/*
	 * @author: Omar Brescianini
	 *
	 * Metodo che definisce la logica di nascondimento di un form
	 * all'interno di NFX quando questo è visualizzato come related
	 */

	forms.nfx_interfaccia_pannello_body.relatedSelected = null;
}

/**
 *
 * @param {JSEvent} frm the event that triggered the action
 * @properties={typeid:24,uuid:"2859D255-6F8C-4ACE-B502-1E941A5C31EF"}
 */
function nfx_onRecordSelection(frm) {
	/*
	 * @author: Omar Brescianini
	 *
	 * Metodo che definisce la logica base di selezione di un
	 * record all'interno di NFX
	 */

	//SAuc
	//var form = application.getMethodTriggerFormName();

	var form = frm.getFormName();

	if (form == forms.nfx_interfaccia_pannello_body.mainForm) {
		nfx_onRecordSelection_asMain();
	} else {
		nfx_onRecordSelection_asRelated(form);
	}

	/*
	 * TIPO: function
	 * Se all'interno del form viene definito 'nfx_onRecordSelection' questo viene eseguito
	 * in modo da poter utilizzare all'occorenza una logica diversa da quella
	 * definita da NFX
	 */
	if (forms[form].nfx_onRecordSelection && typeof forms[form].nfx_onRecordSelection == "function") {
		forms[form].nfx_onRecordSelection();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"E2BBC7CC-FB75-421F-B5FB-948580DC1F81"}
 */
function nfx_onRecordSelection_asMain() {
	/*
	 * @author: Omar Brescianini
	 *
	 * Metodo che definisce la logica base di selezione di un
	 * record all'interno di NFX, quando questo appertiene a
	 * un form visualizzato come principale
	 */

	forms.nfx_interfaccia_pannello_buttons.enableCorrectButtons();
	forms.nfx_interfaccia_pannello_buttons.updatePosition();
	forms.nfx_interfaccia_pannello_body.syncTabs();
}

/**
 *
 * @properties={typeid:24,uuid:"189FAF73-0A59-458D-8F76-87F3BF871EEF"}
 */
function nfx_onRecordSelection_asRelated(form) {
	/*
	 * @author: Omar Brescianini
	 *
	 * Metodo che definisce la logica base di selezione di un
	 * record all'interno di NFX, quando questo appertiene a
	 * un form visualizzato come related
	 */

	/*
	 * TIPO: string ("nome_campo orientamento")
	 * Attibuto che identifica l'eventuale ordinamento base del foundset
	 */
	if (forms[form].nfx_orderBy && forms.nfx_interfaccia_pannello_buttons.mode != "add") {
		forms[form].controller.sort(forms[form].nfx_orderBy);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"A4E5EAFC-62DD-48C2-B314-BC9A9204E61B"}
 */
function nfx_onSearchCmd() {
	forms.nfx_interfaccia_pannello_buttons.findSave(null);
}

/**
 * @properties={typeid:24,uuid:"700A4D3A-3194-420B-BA01-E772AC229FB8"}
 */
function repaintAllButtons() {

	var dueVolte = false;
	application.updateUI();
	var form = forms.nfx_interfaccia_pannello_body.mainForm;
	if(forms[form]){
	if (forms[form].foundset) {
		databaseManager.refreshRecordFromDatabase(forms[form].foundset,-1);
		if (forms[form].foundset._records_) {
			if (forms[form].foundset._records_.length == 0) {
				forms[form].foundset.loadAllRecords();
				//application.output("Ricaricati tutti i record");
				dueVolte = true;
			}
		}
		
		forms.nfx_interfaccia_pannello_buttons.savedSql = databaseManager.getSQL(forms[form].foundset);
		forms.nfx_interfaccia_pannello_buttons.savedSqlParams = databaseManager.getSQLParameters(forms[form].foundset);
		var selected = forms[form].foundset._selection_;
		forms[form].controller.loadRecords(forms.nfx_interfaccia_pannello_buttons.savedSql, forms.nfx_interfaccia_pannello_buttons.savedSqlParams);
		try {
			forms[form].foundset.setSelectedIndex(selected);
			globals.nfx_syncTabs();
		} catch (except) {
			forms[form].foundset.setSelectedIndex(0);
		}
		
		
	}
	}
	
	if (dueVolte == true){
		dueVolte = false;
		infinityrepaint++;
		if (infinityrepaint > 5) {
			infinityrepaint = 0;
			return;
		} else {
			repaintAllButtons();
		}
	} else {
		return;
	}
}

/**
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} form the event that triggered the action
 * @properties={typeid:24,uuid:"BCCAF39F-5866-42AB-9E3E-7563D9D8EA46"}
 */
function nfx_onShow(firstShow, form) {
	/*
	 * @author: Omar Brescianini
	 *
	 * Metodo che definisce la logica base di visualizzazione di un
	 * form all'interno di NFX
	 */

	//var form = application.getMethodTriggerFormName();

	globals.nfx_setBusy(true);

	var formName = form.getFormName();

	application.output("Opening form : " + formName + " by : " + nfx_user);
	application.output("At : " + new Date());
	
	if(formName == "schede_anomalie_descrizione_non_inviate_record"){
		forms.nfx_interfaccia_pannello_body.mainForm=formName;
	}

	if (formName == forms.nfx_interfaccia_pannello_body.mainForm) {
		forms.nfx_interfaccia_pannello_header.elements.formName.text = globals.nfx_getTitleFormName(forms.nfx_interfaccia_pannello_body.mainForm);
		//application.updateUI();
		nfx_onShow_asMain(formName);
	} else {
		nfx_onShow_asRelated(formName);

	}
	//application.updateUI();
	/*
	 * TIPO: function
	 * Se all'interno del form viene definito 'nfx_onShow' questo viene eseguito
	 * in modo da poter utilizzare all'occorenza una logica diversa da quella
	 * definita da NFX
	 */
	if (forms[formName].nfx_onShow && typeof forms[formName].nfx_onShow == "function") {
		forms[formName].nfx_onShow();
	}

	repaintAllButtons();

	globals.nfx_setBusy(false);

	globals.openedFormTime = null;
	globals.openedFormTime = new Date;

	var seconds = 0;
	if(openFormTime==null){
		application.output("Time lost");
	}else{
	seconds = (globals.openedFormTime.getTime() - globals.openFormTime.getTime()) / 1000;
	}
	application.output("Opening form time : +" + seconds + " sec.");
	
	globals.openFormTime = null;
	return;

}

/**
 *
 * @properties={typeid:24,uuid:"88B9089C-604B-439A-94CB-6B668896971A"}
 */
function nfx_onShow_asMain(form) {
	/*
	 * @author: Omar Brescianini
	 *
	 * Metodo che definisce la logica di visualizzazione di un form
	 * all'interno di NFX quando questo è visualizzato come principale
	 */

	//globals.nfx_setBusy(true);

	//SAuc
	//
	//	var pre = forms.nfx_interfaccia_pannello_tree.elements.tree.selectionRows;
	//	preNum.push(form, pre);
	//
	//	var lengthPre = preNum.length;
	//
	//	if (lengthPre > 4) {
	//		var Str1 = preNum[preNum.length - 4];
	//		var Str2 = preNum[preNum.length - 3];
	//		var Str3 = preNum[preNum.length - 2];
	//		var Str4 = preNum[preNum.length - 1];
	//
	//		preNum = new Array();
	//
	//		preNum.push(Str1);
	//		preNum.push(Str2);
	//		preNum.push(Str3);
	//		preNum.push(Str4);
	//
	//	}

	if (nfx_isDynamic(form)) {
		//per prevenire modifiche accidentali
		forms[form].controller.readOnly = true;

		forms.nfx_interfaccia_pannello_buttons.updatePosition();
		forms.nfx_interfaccia_pannello_buttons.showNavButton();
		forms.nfx_interfaccia_pannello_buttons.enableCorrectButtons();

		/*
		 * TIPO: integer
		 * Se definito nel form, questo attributo permette di tenere traccia della scheda
		 * visualizzata in maniera da poterla riproporre al ritorno su form
		 */
		if (forms[form].nfx_toViewOnBack) {
			forms[form].foundset.getRecord(forms[form].nfx_toViewOnBack);
			forms[form].controller.setSelectedIndex(forms[form].nfx_toViewOnBack);
		}

		/*
		 * TIPO: string ("nome_campo orientamento")
		 * Attibuto che identifica l'eventuale ordinamento base del foundset
		 */
		if (forms[form].nfx_orderBy) {
			forms[form].controller.sort(forms[form].nfx_orderBy);
		} else if (globals.utils_getIdDataProvider(form)) {
			forms[form].controller.sort(globals.utils_getIdDataProvider(form) + " desc");
		}

		/*
		 * TIPO: array
		 * Attributo che definisce gli eventuali form correlati a quello mostrato
		 */
		if (forms[form].nfx_related) {
			nfx_showRelated(form);
		} else {
			nfx_hideRelated();
		}
	} else {
		forms[form].controller.readOnly = false;

		/*
		 * TIPO: array
		 * Attributo che definisce gli eventuali form correlati a quello mostrato
		 */
		if (forms[form].nfx_related) {
			forms.nfx_interfaccia_pannello_buttons.showNavButtonOnlyRelated();
			forms.nfx_interfaccia_pannello_buttons.enableCorrectButtons();

			nfx_showRelated(form);
		} else {
			forms.nfx_interfaccia_pannello_buttons.hideNavButtons();

			nfx_hideRelated();
		}
	}

	if (forms[form].nfx_sks && typeof forms[form].nfx_sks == "function") {
		var sks = forms[form].nfx_sks();
		if (sks && sks.length) {
			var max = (sks.length < _maxSKS + 1) ? sks.length : _maxSKS;
			for (var i = 0; i < max; i++) {
				if (typeof (sks[i]) == "object") {
					nfx_addSpecialButton(sks[i].icon, sks[i].tooltip, sks[i].method);
				}
			}

		}
	}
	forms.nfx_interfaccia_pannello_header.elements.formName.text = globals.nfx_getTitleFormName(form);

	//globals.nfx_setBusy(false);
}

/**
 *
 * @properties={typeid:24,uuid:"623FD78A-9B87-485D-84DF-47501B2F6122"}
 */
function nfx_onShow_asRelated(form) {
	
	if(globals.openFormTime == null)
	globals.openFormTime = new Date();
	
	/*
	 * @author: Omar Brescianini
	 *
	 * Metodo che definisce la logica di visualizzazione di un form
	 * all'interno di NFX quando questo è visualizzato come related
	 */

	/*
	 * TIPO: string ("nome_campo orientamento")
	 * Attibuto che identifica l'eventuale ordinamento base del foundset
	 */

	if (forms[form].nfx_orderBy) {
		forms[form].controller.sort(forms[form].nfx_orderBy);
	} else {
		forms[form].controller.sort(globals.utils_getIdDataProvider(form) + " desc");
	}

	forms.nfx_interfaccia_pannello_body.relatedSelected = form;
	//application.output('nfx globals.nfx_onShow_asRelated(form) relatedSelected: ' + form, LOGGINGLEVEL.INFO);
}

/**
 *
 * @properties={typeid:24,uuid:"0198AB7E-3D20-4936-9C12-255D89FDCFC2"}
 */
function nfx_progressBarShow(prg) {
	if (prg) {
		var pb = forms.nfx_interfaccia_popup_progress.elements.progress;
		pb.setMinimum(0);
		pb.setMaximum(prg);
		pb.setValue(0);

		//SAuc
		//application.showFormInDialog(forms.nfx_interfaccia_popup_progress, null, null, null, 55, "Attendere...", false, false, "Progress Bar", false);

		formqprog = forms.nfx_interfaccia_popup_progress;
		windowprog = application.createWindow("Progress Bar", JSWindow.DIALOG);
		windowprog.showTextToolbar(false);
		//windowprog.setSize(0,55)
		windowprog.title = "Attendere...";
		windowprog.showTextToolbar(false);
		windowprog.resizable = false;
		formqprog.controller.show(windowprog);

		//application.output('Mostro progress bar');

		application.updateUI();

	}
}

/**
 *
 * @properties={typeid:24,uuid:"624875C1-6BF8-47AA-9AC0-B25B3E3B8EF1"}
 */
function nfx_progressBarStep() {
	var pb = forms.nfx_interfaccia_popup_progress.elements.progress;
	pb.setValue(pb.getValue() + 1);
	try {
		if (pb.getValue() % 1000 == 0) application.updateUI();
	} catch (e) {
		//do nothing...
	}

	if (pb.getValue() == pb.getMaximum()) {

		application.updateUI();

		//windowprog.destroy();

		//application.output('Distruggo progress bar');
		//		JStaffa
		//		application.closeFormDialog("Progress Bar");
		var activeWindow = application.getActiveWindow();
		if (activeWindow) activeWindow.hide();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"820D7F89-0994-4252-8601-DFEB0B4B8540"}
 */
function nfx_selectedProgram() {
	return (!nfx_customActiomName) ? forms.nfx_interfaccia_pannello_body.mainForm : nfx_customActiomName;
}

/**
 *
 * @properties={typeid:24,uuid:"A26ABCBD-3D4E-48C8-BCA0-B057F46B43D5"}
 */
function nfx_selectedTab() {
	return forms.nfx_interfaccia_pannello_body.relatedSelected;
}

/**
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"A93F6620-901C-47D4-B3D4-30A60584B44F"}
 */
function nfx_setUserActive() {
	/*
	 Segnala che l'utente e' attivo, e in quale minuto, salvando su database
	 l'ora di ultima attivita, una volta al minuto.
	 TODO: dovrebbe controllare giorno e anno, ma il tradeoff e' accettabile
	 */
	var currentActivity = new Date();
	//application.output("Attività : "+ currentActivity);
	var m = currentActivity.getMinutes();
	var h = currentActivity.getHours();
	if (!globals.nfx_lastActivity || ! (globals.nfx_lastActivity.getMinutes() == m && globals.nfx_lastActivity.getHours() == h)) {
		//application.output("Utente attivo, aggiorno ora: " + new Date());
		//		var f = forms.nfx_interfaccia_gestione_utenze;
		/** @type {JSFoundSet<db:/ferrari/k8_gestione_utenze>} */
		var f = databaseManager.getFoundSet('ferrari', 'k8_gestione_utenze');
		if (f) {
			f.loadAllRecords();
			if (f.find()) {
				f.user_servoy = globals.nfx_user;
				f.search();
				if (f.getSize() > 0) {
					f.last_activity = new Date();
					f.last_ip = application.getIPAddress();
					databaseManager.saveData(f.getRecord(1));
				}
				globals.nfx_lastActivity = new Date();
			}
		}
	}
}

/**
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"A3DBC815-FEBE-4A40-ABE4-B006DCC915D3"}
 */
function nfx_setUserInactive() {
	/*
	 * Segnala che l'utente non e' piu attivo. Per esempio dopo aver chiuso il client
	 */
	//	var f = forms.nfx_interfaccia_gestione_utenze;

	/** @type {JSFoundSet<db:/ferrari/k8_gestione_utenze>} */
	var f = databaseManager.getFoundSet('ferrari', 'k8_gestione_utenze');

	f.loadAllRecords();
	if (f.find()) {
		f.user_servoy = globals.nfx_user;
		f.search();
		if (f.getSize() > 0) {
			f.last_activity = new Date();
			f.last_ip = application.getIPAddress();
			databaseManager.saveData(f.getRecord(1));
		}
		globals.nfx_lastActivity = new Date();
		//application.output(nfx_lastActivity);
	}
	return;
}

/**
 * @param {String} form
 * @properties={typeid:24,uuid:"708A074D-D4B3-4411-804E-D1F5E67141DA"}
 */
function nfx_showRelated(form) {

	globals.nfx_setBusy(true);

	/*
	 * @author: Omar Brescianini
	 *
	 * Metodo che definisce la logica di visualizzazione di un form quando
	 * questo ha definiti dei record correlati
	 */

	//SAuc

	var f = solutionModel.getForm(form);
	//	JStaffa per sostituire height si fa con un ciclo e si sommano le altezze dalle parti
	//	var height = f.height;
	//	application.output("f.height deprecated è : " + height);
	var fParts = f.getParts();
	var fHeight = 0;
	for (var j = 0; j < fParts.length; j++) {
		fHeight = fHeight + fParts[j].height;
	}
	var height = fHeight;
	//application.output('nfx globals.nfx_showRelated(form) height: ' + height, LOGGINGLEVEL.INFO);

	forms.nfx_interfaccia_pannello_body.elements.main_split_pane.setEnabled(true);
	forms.nfx_interfaccia_pannello_body.elements.main_split_pane.dividerLocation = height + 10;
	//application.updateUI(100);

	forms.nfx_interfaccia_pannello_body.elements.related.removeAllTabs();
	for (var i = 0; i < forms[form].nfx_related.length; i++) {
		if (!forms[forms[form].nfx_related[i]]) {
			throw "Il form " + forms[form].nfx_related[i] + " non esiste!";
		}

		//		JStaffa
		//		var from = forms[form].controller.getTableName();
		//		var to = forms[forms[form].nfx_related[i]].controller.getTableName();
		var from = forms[form].controller.getDataSource().split('/')[2];
		var to = forms[forms[form].nfx_related[i]].controller.getDataSource().split('/')[2];

		/*
		 * TIPO: string
		 * Attributo che permetta la pesonalizzazione del nome dato alla relazione che lega
		 * due tabelle
		 */
		var modificator = (forms[forms[form].nfx_related[i]].nfx_relationModificator && typeof forms[forms[form].nfx_related[i]].nfx_relationModificator == "function" && forms[forms[form].nfx_related[i]].nfx_relationModificator()) ? "$" + forms[forms[form].nfx_related[i]].nfx_relationModificator() : "";
		var relationName = from + "_to_" + to + modificator;
		//application.output("-------------------------------------");
		application.output("START globals relazione caricata " + relationName);
		//application.output("-------------------------------------");
		var relatedTitle = globals.nfx_getTitleFormName(forms[form].nfx_related[i]);
		var dataS = solutionModel.getForm(form).dataSource;
		var e = solutionModel.getRelations(dataS);
		var nameRel = new Array();
		for (var i2 = 0; i2 < e.length; i2++) {
			nameRel[i2] = e[i2].name;
		}

		//if (forms[form].allrelations.indexOf(relationName) != -1 || globals.allrelations.indexOf(relationName) != -1) {
		if (nameRel.indexOf(relationName) != -1 || globals.nameAllRel.indexOf(relationName) != -1) {
			forms.nfx_interfaccia_pannello_body.elements.related.addTab(forms[forms[form].nfx_related[i]], relatedTitle, relatedTitle, relatedTitle, null, null, null, relationName);
			application.output(relationName + ": relazione OK");
			//application.output("-------------------------------------");
		} else {
			forms.nfx_interfaccia_pannello_body.elements.related.addTab(forms[forms[form].nfx_related[i]], relatedTitle, relatedTitle, relatedTitle);
			application.output(relationName + ": relazione KO");
			//application.output("-------------------------------------");

		}

		forms[forms[form].nfx_related[i]].controller.readOnly = (nfx_isDynamic(forms[form].nfx_related[i])) ? true : false;
	}

	forms.nfx_interfaccia_pannello_body.syncTabs();

}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"4DD274A0-0CF5-494B-9F95-3AD9F9435654"}
 */
function nfx_SKmethodProxy(event) {

	var form = forms.nfx_interfaccia_pannello_body.mainForm;

	//var button = application.getMethodTriggerElementName();

	var button = event.getElementName();

	var method = forms["nfx_interfaccia_pannello_body"][button + "_method"];

	//application.output(form + ' ' + button + ' ' + method);

	if (forms[form][method]) {

		var result = forms[form][method]();
		return result;
	}
	var e = null;
	return e;

}

/**
 *
 * @properties={typeid:24,uuid:"AB73E0D6-2204-422B-B804-42B10DFDCE5A"}
 */
function nfx_syncTabs() {
	forms.nfx_interfaccia_pannello_body.syncTabs()
}

/**
 *
 * @properties={typeid:24,uuid:"CEC55596-5F44-4D6F-8E67-3A6A8217B9D9"}
 */
function nfx_tree_createDragDelegateImpl(del) {
	return {
		dragGestureRecognized: del.dragGestureRecognized
	};
}

/**
 *
 * @properties={typeid:24,uuid:"8D451464-6C6A-4A2F-85A2-9CB3B6C61A09"}
 */
function nfx_tree_createDropDelegateImpl(del) {
	var delImpl = {
		dragEnter: del.dragEnter,
		dragOver: del.dragOver,
		dragExit: del.dragExit,
		dragDropEnd: del.dropEnd,
		dropActionChanged: del.dropActionChanged,
		drop: del.drop
	}

	/*
	 void	dragEnter(DropTargetDragEvent dtde)
	 Called while a drag operation is ongoing, when the mouse pointer enters the operable part of the drop site for the DropTarget registered with this listener.
	 void	dragExit(DropTargetEvent dte)
	 Called while a drag operation is ongoing, when the mouse pointer has exited the operable part of the drop site for the DropTarget registered with this listener.
	 void	dragOver(DropTargetDragEvent dtde)
	 Called when a drag operation is ongoing, while the mouse pointer is still over the operable part of the drop site for the DropTarget registered with this listener.
	 void	drop(DropTargetDropEvent dtde)
	 Called when the drag operation has terminated with a drop on the operable part of the drop site for the DropTarget registered with this listener.
	 void	dropActionChanged(DropTargetDragEvent dtde)
	 Called if the user has modified the current drop gesture.
	 */
	return delImpl;
}

/**
 *
 * @properties={typeid:24,uuid:"0EEE9E7A-4386-499D-B5AA-9C105E107759"}
 */
function nfx_tree_getVoidTransferableImpl() {
	/* empty transferable implementation */
	return new java.awt.datatransfer.Transferable({
		getTransferDataFlavors: function() { },
		isDataFlavorSupported: function() {
			return true;
		},
		getTransferData: function() { }
	});
}

/**
 *
 * @properties={typeid:24,uuid:"953E52E1-F2CF-4384-A917-64704F63607D"}
 */
function nfx_tree_setupDnD(sourceTree, targetTree, delegate, drp) {

	var dropDelegate = drp || delegate;

	delegate["dragSource"] = new java.awt.dnd.DragSource();
	delegate["dragRecognizer"] = delegate["dragSource"].createDefaultDragGestureRecognizer(sourceTree,
		java.awt.dnd.DnDConstants.ACTION_COPY,
		new java.awt.dnd.DragGestureListener(nfx_tree_createDragDelegateImpl(delegate))
	);
	dropDelegate["dropTarget"] = new java.awt.dnd.DropTarget(targetTree,
		new java.awt.dnd.DropTargetListener(nfx_tree_createDropDelegateImpl(dropDelegate))
	);
}

/**
 *
 * @properties={typeid:24,uuid:"F2E664E1-1FF0-47B2-B7B2-6E3E69E84060"}
 */
function nfx_udpPacketReceived() {
	var packet = plugins.udp.getReceivedPacket();

	while (packet) {
		var message = packet.readUTF();
		var user = packet.getHost();
		for (var i = 0; i < globals.nfx_udpPacketReceivedCallbacks.length; i++) {
			globals.nfx_udpPacketReceivedCallbacks[i] = (message, user);
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"C9F60EB6-A3B1-460B-9134-F88CE467E51F"}
 */
function utils_clone(e) {
	return e;
}

/**
 *
 * @properties={typeid:24,uuid:"B5110AA3-714E-4D96-8A4D-E13637402443"}
 */
function utils_defaultValidationMethod(form, allFields) {
	if (form && allFields) {
		//		JStaffa
		//		var tableName = forms[form].controller.getTableName();
		//		var tableName = forms[form].controller.getDataSource().split('/')[2];
		var missing = [];

		//		JStaffa
		//		for (f in allFields){
		for (var f in allFields) {
			if (!forms[form].foundset[f]) {
				missing.push(f);
			}
		}

		if (missing.length > 0) {
			var message = "Compilare i campi obbligatori:\n\n";
			var i = 1;
			//			JStaffa
			//			for each(mf in missing){
			for each (var mf in missing) {
				message += i++ + ". Inserire " + allFields[mf] + "\n";
			}
			plugins.dialogs.showErrorDialog("Errori nella compilazione", message, "Ok");
			return -1;
		} else {
			return 0;
		}
	} else {
		throw "Worng Parameters";
	}
}

/**
 *
 * @properties={typeid:24,uuid:"4ECFFA23-A7EA-44D8-B89A-2A35887D81F9"}
 * @AllowToRunInFind
 */
function utils_filterList(filter, list) {

	if (!list) {
		throw "Worng Parameters";
	}
	if (filter) {
		var upperFilter = filter.toUpperCase();
		var filtered = new Array();
		for (var i = 0; i < list.length; i++) {
			/** @type {String} */
			var upperList = list[i].toUpperCase();
			if (upperList.search(upperFilter) != -1) {
				filtered.push(list[i]);
			}
		}
		return filtered;
	} else {
		return list;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"D3B0D8E4-127A-403E-BDDC-45AEFAB7638F"}
 */
function utils_getDateIntervalAsArray(date1, date2) {

	//Mi accerto che i parametri ci siamo e siano date
	if (!date1 || date1.constructor !== Date) throw "Parametro assente o di tipo non data";
	if (!date2 || date2.constructor !== Date) throw "Parametro assente o di tipo non data";

	//Creo due variabili "di lavoro" per annullare l'eventuale differenza di orari
	var work1 = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
	var work2 = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());

	if (work2 == work1) return new Array(work1);
	if (work2 > work1) {
		var dates = new Array(work1);
		var i = 1;
		do {
			var nextDate = new Date(work1.getFullYear(), work1.getMonth(), work1.getDate() + i);
			dates.push(nextDate);
			i++;
		} while (nextDate < work2);

		return dates;
	} else {
		return new Array();
	}
}

/**
 * @SuppressWarnings(deprecated)
 * @properties={typeid:24,uuid:"1DC42330-32C9-4230-B348-EC7590602160"}
 */
function utils_getIdDataProvider(form) {
	// JStaffa
	// var table = databaseManager.getTable(forms[form].controller.getServerName(),forms[form].controller.getTableName());
	if (form && forms[form].controller.getDataSource()) {
		var table = databaseManager.getTable(forms[form].controller.getDataSource().split('/')[1], forms[form].controller.getDataSource().split('/')[2]);
		//application.output('nfx globals.utils_getIdDataProvider() serverName: ' + forms[form].controller.getDataSource().split('/')[1] + '; tableName: ' + forms[form].controller.getDataSource().split('/')[2]);
		return (table) ? table.getRowIdentifierColumnNames()[0] : null;
	} else return null;

}

/**
 *
 * @properties={typeid:24,uuid:"0D7B0B59-FCA0-468D-8892-AE1920D22D1E"}
 */
function utils_howManySaturdays(dates) {
	var num = 0;
	for (var i = 0; i < dates.length; i++) {
		if (dates[i].getDay() == 6) num++;
	}
	return num;
}

/**
 *
 * @properties={typeid:24,uuid:"C2DF855A-297F-45DA-90D2-0600E446BD7E"}
 */
function utils_howManySundays(dates) {

	var num = 0;
	for (var i = 0; i < dates.length; i++) {
		if (dates[i].getDay() == 0) num++;
	}
	return num;
}

/**
 *
 * @properties={typeid:24,uuid:"E392487B-E207-417A-826A-E6C1AB0D45AA"}
 */
function utils_howManyWorkingDays(dates) {
	var num = 0;
	for (var i = 0; i < dates.length; i++) {
		if (dates[i].getDay() > 0 && dates[i].getDay() < 6) num++;
	}
	return num;
}

/**
 *
 * @properties={typeid:24,uuid:"864D09CA-BF4A-4EF8-A124-6ED0345BCBC8"}
 */
function utils_isDoubleClick() {
	if (firstClick && (new Date() - firstClick) > 1000) {
		firstClick = null;
	}
	if (!firstClick) {
		firstClick = new Date();
		return null;
	} else {
		if ( (new Date() - firstClick) < 300) {
			firstClick = null;
			return true;
		} else {
			firstClick = null;
			return null;
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"427A5FBD-1B08-4772-9C55-B7F5E8B11338"}
 */
function utils_saveAs(toWrite, toWriteName) {

	var fileName = plugins.file.showFileSaveDialog(toWriteName);
	if (fileName) {
		var file = plugins.file.createFile(fileName);
		if (!file.exists()) {
			plugins.file.writeFile(fileName, toWrite);
		} else {
			var result = plugins.dialogs.showQuestionDialog("Salva", "Il file è già presente, sovrascriverlo?", "Sì", "No");
			if (result == "Sì") {
				plugins.file.writeFile(fileName, toWrite);
			} else {
				utils_saveAs(toWrite, toWriteName);
			}
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"5CD9E17A-3121-4D62-A8B2-725D56A1B592"}
 */
function utils_saveAsPng(toWrite, toWriteName) {

	var fileName = plugins.file.showFileSaveDialog(toWriteName);
	if (fileName) {
		/** @type {java.io.File} */
		var file = java.io.File(fileName);
		if (!file.exists()) {
			Packages.javax.imageio.ImageIO.write(toWrite, "png", file);
		} else {
			var result = plugins.dialogs.showQuestionDialog("Salva", "Il file è già presente, sovrascriverlo?", "Sì", "No");
			if (result == "Sì") {
				Packages.javax.imageio.ImageIO.write(toWrite, "png", file);
			} else {
				utils_saveAsPng(toWrite, toWriteName);
			}
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"35189D0E-8CA5-4027-B9CD-BCD6FBC99C9B"}
 */
function utils_saveAsTXT(toWrite, toWriteName) {

	var fileName = plugins.file.showFileSaveDialog(toWriteName);
	if (fileName) {
		var file = plugins.file.createFile(fileName);
		if (!file.exists()) {
			plugins.file.writeTXTFile(fileName, toWrite);
		} else {
			var result = plugins.dialogs.showQuestionDialog("Salva", "Il file è già presente, sovrascriverlo?", "Sì", "No");
			if (result == "Sì") {
				plugins.file.writeTXTFile(fileName, toWrite);
			} else {
				utils_saveAsTXT(toWrite, toWriteName);
			}
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"E90167C6-79DC-45DC-90D7-7252C46708DB"}
 */
function utils_setSplitPane(p) {
	p.splitpane.setDoubleBuffered(true);
	p.splitpane.border = Packages.javax.swing.BorderFactory.createEmptyBorder(); //Assigns an empty border to all splitpanes
	p.splitpane.leftComponent = p.leftComponent;
	p.splitpane.rightComponent = p.rightComponent;
	p.splitpane.dividerLocation = p.dividerLocation || 200;
	p.splitpane.orientation = p.orientation || 0;
	p.splitpane.dividerSize = p.dividerSize || 3;
	p.splitpane.setOneTouchExpandable(p.setOneTouchExpandable || false);
	p.splitpane.setContinuousLayout(p.setContinuousLayout || false);

}

/**
 * @properties={typeid:24,uuid:"0820186B-290C-4484-AC9B-FB7A373900D3"}
 */
function utils_printFoundset(fs) {
	application.output(databaseManager.getSQL(fs));
	application.output(databaseManager.getSQLParameters(fs));
}

/**
 * @properties={typeid:24,uuid:"5A75B816-7B79-4D9E-A3D7-614EF6F1CAF0"}
 */
function utils_stringToDate(str) {
	if (str) {
		if (str == "01/01/1970") return new Date(0);
		if (str.length == 10) {
			var day = str.slice(0, 2);
			var month = str.slice(3, 5);
			var year = str.slice(6);
			var date = new Date(year + "/" + month + "/" + day);
			return (date != new Date(0)) ? date : null;
		}
	}
	return null;
}

/**
 * @properties={typeid:24,uuid:"D8947A63-B8CA-42AD-8889-6AAE53ED4371"}
 */
function utils_sortNumbers(a, b) {

	return a - b;
}

/**
 * @properties={typeid:24,uuid:"44F50DA6-F6D3-4CC2-986F-BC88FDC073F1"}
 */
function setAccessFor(usersList) {
	var users = globals.nfx_developers.concat(usersList);
	return (users.indexOf(globals.nfx_user) != -1) ? true : false;
}

/**
 * @properties={typeid:24,uuid:"62F96E2E-EF6D-4CEB-89A9-3B3AFAB2D795"}
 */
function utils_openDefaultExcelImport() {
	//FS
	// x = plugins.menubar.getMenuIndexByText("File");
	// var menu = plugins.menubar.getMenu(index);
	// var subIndex = menu.getItemIndexByText("Importa");
	// var subMenu = menu.getSubMenu(subIndex);

	//	JStaffa senza settare un index (era dichiarato ma non c'era), il programma esplode; con 0 sembra funzionare tutto
	var index = 0;

	var x = plugins.window.getMenuBar().getMenuIndexByText("File");
	var menu = plugins.window.getMenuBar().getMenu(index);
	application.output('nfx globals.utils_openDefaultExcelImport() var x: ' + x + '; menu.text: ' + menu.text);
	var subIndex = menu.getItemIndexByText("Importa");
	var subMenu = menu.getMenu(subIndex);
	var item = subMenu.getItem(0);
	item.doClick();
}

/**
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"F1688CB4-B6B8-47DF-86A3-D1B8E38B53D4"}
 */
function utils_valueListCheck(oldValue, newValue, event) {
	var mode = forms.nfx_interfaccia_pannello_buttons.mode;
	if (mode == "add" || mode == "edit") {
		var form = solutionModel.getForm(event.getFormName());
		var elem = form.getField(event.getElementName());
		var check = application.getValueListItems(elem.valuelist.name).getColumnAsArray(2);
		if (check.indexOf(newValue) != -1) {
			return true;
		} else {
			return false;
		}
	}
	return null;
}

/**
 * @properties={typeid:24,uuid:"CF2E567A-78B1-4088-8AB8-7368A83DFC18"}
 */
function nfx_toJSON(string) {
	return forms.nfx_json_serializer.parse(string, null);
}

/**
 * @properties={typeid:24,uuid:"523EB8F2-4D59-4D90-A286-90D4EB7B815E"}
 */
function nfx_toSting(json) {
	return forms.nfx_json_serializer.stringify(json, null, null);
}

/**
 * @properties={typeid:24,uuid:"5070CF4C-B0AF-4C7A-A9AE-73DEEE23C66A"}
 */
function nfx_launchExport(form) {
	forms.nfx_interfaccia_pannello_buttons.launchExport(form);
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 *
 * @properties={typeid:24,uuid:"09CA0CD0-D4E9-4F66-BBA5-C1B2605C7F0A"}
 */
function nfx_trim(oldValue, newValue, event) {
	//	JS
	//	forms[event.getFormName()].foundset[event.getSource().getDataProviderID()] = utils.stringTrim(newValue);
	if(newValue)
	forms[event.getFormName()].foundset[event.getSource().getDataProviderID()] = utils.stringTrim(newValue.toString());
}

/**
 * @properties={typeid:24,uuid:"646FA826-9DB3-4CCD-B078-9BC5EAD0464C"}
 */
function nfx_registerPlugins() {
	//		plugins.it2be_ldapclient.register("lr7n1thsd7uhi7f43rspi6i48njhps5c3b32j2gc98prt1rpmi08h09i5uafhtso6shud10boh8phn7586t3cvjv9o448aq51qhdanvfma297pqebhu7glh5st1g78env7o7asuij5oo5duhe8i8af7hgisot15pur92mseo8e3oqdp3gfeoi36p1jvk2ftp6kfvnpo5trfd5ngretfusssuhseog226fjsnu274gv03l76ujsn6l5l4g4r3v2cn536vr4od8dlc9l00nqilu5073qkiviiflnnjqi0hfdm174sfipuf4j1gjs5gmuc2bd880vvqngel5rj5ettgek7rg9iblomp904tit9c084la0glp08f0ovu0lbj8lkv6uuu41902j1ked1v1f6cf5cg9h943786qo8qukuio281gjumdhs6g8ov7f033s5450flh68qq06eosubgipn3lpg5q5hne7f2oq0hh9n18tc6kpied780v0fgk9dgioq75oq7jo9itakq3jgr1vapts80n38mifcjpnkgv3r9h8n3fdm851g7h0tdr8lgqqgte6e0gav1uo5kle1q7isrknvsd4dsbjnhodtcp042u7vap55jt3hak0us2r6fjv5bh6pmoor83gh1scegs91v0klm70l370i3eg76foc2iqdiv5vflqcsrsk8pbeubmkhfr4q237agkpe4k101sbqndtkbtv6phugh75gj05v5slced1g237gp9lj3lp9v9rqffk29irunp6rnn1hj18tp3d7o9v7rg9ro718sujena3g9tnrac");
	plugins.it2be_ldapclient.register("4BB8N0EF3BEV9GIP81QD4M7L442E5DLR6VM48I3G99M34CMPONK0H8RC1R2AC492K7NREQ8EDP7BO79KV4EFFD9LGLPF942CR9PJ6ISS8THV7IV51M24A1CLQE9NLBODLM0GPM2K0D08SNBCBO9IPFURVFD9L04QJ5JVJCC57R0DVIRS7JG91J46QMMVJQN60341BC65O8JQUGPBJTI4G0JHSF42BCIRBUR2VHBBHLKODIEBRTV1I3QACV8B1A7TLFEQ7NCSVCR6AFQ9V07NP7T4RUCHVTFJ4SEJK8MLACC00FUODT74DVHT209O10Q7CMIL4D73A633NBS5GQV0Q2TG6HD5C0GNKUQEP66J6UUJ4D36NT94V7QO7VTGPM11TJVL0HBNGHF8CF6JAU9BTU2PFU71QIJIIIPAF9AFF2S3ANL61KTN1M6CETC0HL4CIDKR5KT81GIRQIG5AP78JJ2NIKNDKH47I5VFR69HQANHL5UQ2FCO9Q64O5GQCJ58BOKEQ1F9JPF4VCUVVQS4UL7KJNGPA8AMIG15SNBHDSNAMK2LK383QQI83R0A7PA444J6MIKNAR3B8HNNNGNGF2B20CH51LMG0NGKCNLR9BTPPGOG24HMTCVQD110E5H5SRQFGAAUEKDU90517D50K134Q607F894AFB7C8SDKVCHBBGT3CRA9C9OEA6GQV6JCNRP575AHTQN4G030OSJ816EPNI211SEDK0NJJ8E4BS1BLE80RO9E6A9DKD1QESO8AQDMUMISUNKC39V3FHS7KFL7SE4T9IURPO1QSGJJ7792RSA9A5");
	application.output("Plugin registered successfully");
}

/**
 * @properties={typeid:24,uuid:"D9BE3C21-1414-4D8B-99DE-5B5FAA2FBF22"}
 */
function nfx_setBusy(busy) {
	forms.nfx_interfaccia_pannello_header.elements.busy.visible = busy;
	if (busy) {
		application.updateUI();
	}
}

/**
 * @properties={typeid:24,uuid:"5C48F073-01CC-45A6-8215-B287D2F9D82F"}
 */
function utils_getJColor(_JSColor) {
	if (!_JSColor) _JSColor = application.showColorChooser();
	try {
		return java.awt.Color.decode(_JSColor);
	} catch (e) {
		//TODO: cosa faccio se non sceglie?
	}

	return null;
}

/**
 * @properties={typeid:24,uuid:"FBF0F8EB-24B5-4C96-8A94-F6AB3875F926"}
 */
function utils_getJFont(_JSFont) {
	if (!_JSFont) _JSFont = application.showFontChooser();
	try {
		var _JFont = _JSFont.split(",");
		return java.awt.Font(_JFont[0], parseInt(_JFont[1]), parseInt(_JFont[2]));
	} catch (e) {
		//TODO: cosa faccio se non sceglie?
	}

	return null;
}

/**
 * @properties={typeid:24,uuid:"D8187CF8-E9AA-4806-959D-2CF80795138F"}
 */
function nfx_isTreeLoaded() {
	return forms.nfx_interfaccia_pannello_tree.pannelloSxLoaded;
}
