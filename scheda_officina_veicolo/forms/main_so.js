/**
 * @properties={typeid:35,uuid:"46F45B3E-83FA-4019-8FCC-1157898F1999",variableType:-4}
 */
var operator = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1E3DB6BB-4AE0-4A2B-A75A-DF94586524C2",variableType:-4}
 */
var ruolo = [];

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"7E7064DD-A31F-4CE4-B4D8-3077613F07D1"}
 * @AllowToRunInFind
 */
function onLoad(event) {

	application.output(globals.messageLog + "START main_so.onLoad()", LOGGINGLEVEL.INFO);

	//Cerco ruolo e lo setto nell'array

	/** @type {JSFoundSet<db:/ferrari/k8_gestione_utenze>} */
	var users = databaseManager.getFoundSet('ferrari', 'k8_gestione_utenze');
	if (users.find()) {
		users.user_servoy = security.getUserName();
		var found = users.search();

		if (found > 0) {
			/** @type {JSFoundSet<db:/ferrari/so_gestione_ruoli>} */
			var manageRoles = databaseManager.getFoundSet('ferrari', 'so_gestione_ruoli');

			if (manageRoles.find()) {
				manageRoles.fk_gestione_utenze_id = users.k8_gestione_utenze_id;
				var foundRol = manageRoles.search();
				if (foundRol > 0) {
					for (var i = 0; i < foundRol; i++)
						ruolo.push(manageRoles.getRecord(i).fk_ruolo_id.toString());
				}
			}

		} else {
			/** @type {JSFoundSet<db:/ferrari/so_operatori>} */
			var op = databaseManager.getFoundSet('ferrari', 'so_operatori');

			if (op.find()) {
				op.username = security.getUserName();
				var foundOP = op.search();
				if (foundOP > 0) {
					/** @type {JSFoundSet<db:/ferrari/so_ruoli>} */
					var rol = databaseManager.getFoundSet('ferrari', 'so_ruoli');

					if (rol.find()) {
						rol.nome_ruolo = 'Operaio';
						var FoundOPID = rol.search();
						if (FoundOPID > 0) {
							ruolo.push(rol.so_ruolo_id.toString());
						}
					}

				}

			}
		}

	}

	if (ruolo) {
		//		application.output('Ruoli : '+ruolo);
		for (var i2 = 0; i2 < ruolo.length; i2++) {
			/** @type {JSFoundSet<db:/ferrari/so_ruoli>} */
			var RuoliFS = databaseManager.getFoundSet('ferrari', 'so_ruoli');
			if (RuoliFS.find()) {
				RuoliFS.so_ruolo_id = ruolo[i2];
				var research = RuoliFS.search()
				if (research > 0) {
					ruolo[i2] = RuoliFS.nome_ruolo;
				}
			}
		}
		application.output('Ruoli : ' + ruolo);
	}

	goToAll(event);

	_super.updateButton(1, "Home");

	globals.setData();
	globals.setUser();

	updateUI(event);

	application.output(globals.messageLog + "STOP main_so.onLoad()", LOGGINGLEVEL.INFO);

}

/**
 *@param {JSEvent} event
 * @properties={typeid:24,uuid:"38CE57BC-1AF9-4374-8621-BC88CE27593B"}
 */
function goToDetail(event) {

	//carico il tab dettaglio

	application.output(globals.messageLog + "START main_so.goToDetail()", LOGGINGLEVEL.INFO);

	_super.goToTab("so_dettaglio");

	updateUI(event);

	application.output(globals.messageLog + "STOP main_so.goToDetail()", LOGGINGLEVEL.INFO);

}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"B23D7C4C-B8CE-472B-B827-8F0F81A099F5"}
 */
function goToAll(event) {

	//carico il tab lista

	application.output(globals.messageLog + "START main_so.goToAll()", LOGGINGLEVEL.INFO);

	_super.goToTab("so_list");

	updateUI(event);

	application.output(globals.messageLog + "STOP main_so.goToAll()", LOGGINGLEVEL.INFO);

}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"97EFADAE-8016-4687-8915-D8A21A2324B6"}
 */
function onActionButtonHome(event) {

	//Torna alla pagina iniziale con tutte le schede

	application.output(globals.messageLog + "START main_so.onActionButtonHome()", LOGGINGLEVEL.INFO);

	goToAll(event);

	updateUI(event);

	application.output(globals.messageLog + "STOP main_so.onActionButtonHome()", LOGGINGLEVEL.INFO);

}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"955D0946-26F5-47D8-B424-AD7DDE0B3000"}
 */
function onActionNextRecord(event) {

	//Mi sposto in avanti nel foundset
	//Aggiorno il navigator
	//Sincronizzo i tab

	application.output(globals.messageLog + "START main_scheda_officina_veicolo.onActionNextRecord()", LOGGINGLEVEL.INFO);

	controller.setSelectedIndex(_super.selected + 1);
	foundset.getRecord(_super.selected + 1);

	updateUI(event);

	application.output(globals.messageLog + "STOP main_scheda_officina_veicolo.onActionNextRecord()", LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"C77DA196-EE4B-472B-8099-FE682F520111"}
 */
function onActionBackRecord(event) {

	//Mi sposto indietro nel foundset
	//Aggiorno il navigator
	//Sincronizzo i tab

	application.output(globals.messageLog + "START main_so.onActionBackRecord()", LOGGINGLEVEL.INFO);

	controller.setSelectedIndex(_super.selected - 1);
	foundset.getRecord(_super.selected - 1);

	updateUI(event);

	application.output(globals.messageLog + "STOP main_so.onActionBackRecord()", LOGGINGLEVEL.INFO);
}

/**
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"5C1393B7-1AC4-4A9A-AB16-AD3CDDBD7458"}
 */
function goToFirstRecord(event) {

	//Mi sposto al primo record
	//Aggiorno il navigator
	//Sincronizzo i tab

	application.output(globals.messageLog + "START main_so.goToFirstRecord()", LOGGINGLEVEL.INFO);

	controller.setSelectedIndex(1);
	foundset.getRecord(1);

	updateUI(event);

	application.output(globals.messageLog + "STOP main_so.goToFirstRecord()", LOGGINGLEVEL.INFO);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"6371F6A9-ED8B-4021-ABDF-FCAD7B327069"}
 */
function goToLastRecord(event) {

	//Mi sposto all'ultimo record
	//Aggiorno il navigator
	//Sincronizzo i tab

	application.output(globals.messageLog + "START main_so.goToLastRecord()", LOGGINGLEVEL.INFO);

	controller.setSelectedIndex(databaseManager.getFoundSetCount(foundset));
	foundset.getRecord(databaseManager.getFoundSetCount(foundset));

	updateUI(event);

	application.output(globals.messageLog + "STOP main_so.goToLastRecord()", LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"29D15328-5346-4B77-AA8F-5FB423BF6A5D"}
 */
function onActionSearchButton(event) {

	//Preparo l'interfaccia grafica alla ricerca
	//disabilito il pannello inferiore
	//nascondo tutti i bottoni
	//vado in find sul foundset

	application.output(globals.messageLog + "START main_so.onActionSearchButton()", LOGGINGLEVEL.INFO);
	_super.goToTab("so_dettaglio");
	if (foundset.find()) {
	}

	updateUI(event);

	application.output(globals.messageLog + "STOP main_so.onActionSearchButton()", LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"609232F3-4884-40D5-AE5F-BF8D5228784A"}
 * @AllowToRunInFind
 */
function onActionSearch(event) {

	//Cerco effettivamente nel foundset
	//Abilito i vari bottoni/pannelli
	//sincronizzo i tab
	//aggiorno il navigator

	application.output(globals.messageLog + "START main_so.onActionSearch()", LOGGINGLEVEL.INFO);

	foundset.search();

	updateUI(event);

	application.output(globals.messageLog + "STOP main_so.onActionSearch()", LOGGINGLEVEL.INFO);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"2C7CB47E-83B5-4B9A-A93E-3BE1C4A89C9B"}
 */
function onActionModifica(event) {

	//disabilito i tasti lasciando solo salva e annulla , è possibile modificare.

	application.output(globals.messageLog + "START main_so.onActionModifica()", LOGGINGLEVEL.INFO);

	goToDetail(event);

	_super.startEditing(event);

	updateUI(event);

	application.output(globals.messageLog + "STOP main_so.onActionModifica()", LOGGINGLEVEL.INFO);

}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"153846E6-66D4-410A-8B6B-AE9C63571D89"}
 */
function onShow(firstShow, event) {

	application.output(globals.messageLog + "START main_so.onShow()", LOGGINGLEVEL.INFO);

	updateUI(event);

	application.output(globals.messageLog + "STOP main_so.onShow()", LOGGINGLEVEL.INFO);

}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"36E30110-4AEB-4F0D-A830-83E5E332F3CC"}
 */
function onActionHome(event) {
	application.output(globals.messageLog + "START main_so.onActionHome()", LOGGINGLEVEL.INFO);

	goToAll(event);

	updateUI(event);

	application.output(globals.messageLog + "STOP main_so.onActionHome()", LOGGINGLEVEL.INFO);

}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"50B49E18-3723-4E99-801A-17FB7000AA25"}
 * @AllowToRunInFind
 */
function onActionDeleteSchedaOfficina(event) {

	//Porta al metodo che elimina il record selezionato e tutti i record correlati

	var answer = globals.DIALOGS.showWarningDialog("Conferma", "Si è sicuri di eliminare il record selezionato e tutti i correlati (Attività,Software e Documenti)?", "Si", "No");
	if (answer == 'Si') {
		application.output(globals.messageLog + "START main_so.onActionDeleteSchedaOfficina()", LOGGINGLEVEL.INFO);

		//globals.DIALOGS.showInfoDialog("DELETE INFO", "before officina dettaglio ", " ok");
		//forms.so_dettaglio.deleteSelectedRecord(event);
		//globals.DIALOGS.showInfoDialog("DELETE INFO", " after officina  dettaglio", " ok");

		//globals.DIALOGS.showInfoDialog("DELETE INFO", "before officina ", " ok");
		//foundset.deleteRecord();
		//globals.DIALOGS.showInfoDialog("DELETE INFO", " after officina  ", " ok");

		updateUI(event);

		application.output(globals.messageLog + "STOP main_so.onActionDeleteSchedaOfficina()", LOGGINGLEVEL.INFO);
	}
}

/**
 * Create new record in form details.
 * @param {JSEvent} event the event that triggered the action
 *
 *
 * @properties={typeid:24,uuid:"6EA77CBA-744C-4684-93A5-35FB20003F25"}
 */
function onNewRecord(event) {

	application.output(globals.messageLog + "START main_so.onNewRecord()", LOGGINGLEVEL.INFO);

	goToDetail(event);

	_super.newRecord(event);

	updateUI(event);

	application.output(globals.messageLog + "START main_so.onNewRecord()", LOGGINGLEVEL.INFO);

}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"AF3F42FD-A3EC-49FA-86B7-690FF4F96F63"}
 */
function updateUI(event) {

	application.output(globals.messageLog + "START main_so.updateUI()", LOGGINGLEVEL.INFO);

	_super.updateUI(event);
	forms.so_dettaglio.syncTabs(event);
	_super.updateCount(foundset.getSelectedIndex(), databaseManager.getFoundSetCount(foundset));

	application.output(globals.messageLog + "STOP main_so.updateUI()", LOGGINGLEVEL.INFO);

}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"7122BA13-A3FB-40F5-B1A3-F6AD64E88052"}
 */
function onActionSalva(event) {

	application.output(globals.messageLog + "START main_so.onActionSalva()", LOGGINGLEVEL.INFO);

	_super.saveEditing(event);
	updateUI(event);

	application.output(globals.messageLog + "STOP main_so.onActionSalva()", LOGGINGLEVEL.INFO);

}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"A2DCB09E-D6C7-4AA9-A3BB-2A5BA7964E77"}
 */
function onActionAnnulla(event) {

	application.output(globals.messageLog + "START main_so.onActionAnnulla()", LOGGINGLEVEL.INFO);

	_super.stopEditing(event);
	updateUI(event);

	application.output(globals.messageLog + "STOP main_so.onActionAnnulla()", LOGGINGLEVEL.INFO);

}
