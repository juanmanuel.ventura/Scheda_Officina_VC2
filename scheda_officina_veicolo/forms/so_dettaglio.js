/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A8915EB7-2766-4B1F-9C47-154E8F322062"}
 */
var numero_scheda = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"398B10AE-B200-4DBD-A4EF-AB222ECC55EE",variableType:8}
 */
var complete = null;

/**
 * @properties={typeid:35,uuid:"2585CC99-D93F-44E6-90BB-BC520D1000C6",variableType:-4}
 */
var tabHeight = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"E2FDDB71-D551-405B-BE3D-19CC04BCA659"}
 */
var rel = '';

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"57C4FB75-D138-413B-AC03-B1944CEF67F6",variableType:4}
 */
var tabInd = 0;

/**
 * @properties={typeid:35,uuid:"B0DC3176-5FCB-45A6-9983-7562540DDDF5",variableType:-4}
 */
var edit = false;

/**
 * @properties={typeid:35,uuid:"A83E8008-0EFA-46C8-9D0D-22C2C887263D",variableType:-4}
 */
var current = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"069C7A85-8853-4EDB-BF84-4C7BDF900D99",variableType:4}
 */
var widthPanel = 0;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"3E52DAE5-16B4-4B32-A947-28A8A1C1AF41"}
 */
function onLoad(event) {

	application.output(globals.messageLog + "START so_dettaglio.onLoad() ", LOGGINGLEVEL.INFO);

	updateUI(event);

	application.output(globals.messageLog + "STOP so_dettaglio.onLoad() ", LOGGINGLEVEL.INFO);
}

/**
 * Callback method when form is resized.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"1968D0E0-7B72-4EC1-832B-0AB653B888D1"}
 */
function onResize(event) {
	application.output(globals.messageLog + "START so_dettaglio.onResize() ", LOGGINGLEVEL.INFO);

	updateUI(event);

	application.output(globals.messageLog + "STOP so_dettaglio.onResize() ", LOGGINGLEVEL.INFO);

}

/**
 *
 * @param previousIndex
 * @param event
 *
 * @properties={typeid:24,uuid:"78787EC3-AEEE-4AFD-8C05-79CE0B7C4C32"}
 */
function onTabChange(previousIndex, event) {

	//Sul cambio della selezione del tab
	//se il tab selezionato è il secondo 'esecuzione attivita'
	//nascondo i tasti aggiungi e rimuovi
	//Se è in edit , non lo faccio cambiare
	//Ogni cambio di tab , seleziono il primo record della totalità

	application.output(globals.messageLog + "START so_dettaglio.onTabChange()", LOGGINGLEVEL.INFO);
	//	if (edit == true) {
	//		elements.tabless.tabIndex = current;
	//		elements.buttonPiu.visible = false;
	//		elements.buttonMeno.visible = false;
	//		elements.buttonPiu.visible = false;
	//		elements.buttonMeno.visible = false;
	//
	//	} else {
	//		elements.buttonPiu.visible = true;
	//		elements.buttonMeno.visible = true;
	//		if (elements.tabless.tabIndex == 2) {
	//			elements.buttonPiu.visible = false;
	//			elements.buttonMeno.visible = false;
	//		}
	//
	//	}
	//	tabInd = elements.tabless.tabIndex;

	updateUI(event);

	application.output(globals.messageLog + "STOP so_dettaglio.onTabChange() current index : " + elements.tabless.tabIndex, LOGGINGLEVEL.INFO);

}

/**
 * @param event
 *
 * @properties={typeid:24,uuid:"F51385DA-9C2B-4FFE-8E34-35BE7A534ABA"}
 * @AllowToRunInFind
 */
function syncTabs(event) {

	//sincronizza tutti i record dei tab sottostanti , visibilità e non visibilità dei bottoni
	//sinronizzazione di attività e operazioni

	application.output(globals.messageLog + "START so_dettaglio.syncTabs() ", LOGGINGLEVEL.INFO);

	if (foundset) {
		if (scheda_officina_to_scheda_officina_attivita)
			if (foundset.scheda_officina_to_scheda_officina_attivita.find()) {
				foundset.scheda_officina_to_scheda_officina_attivita.fk_schede_officina = foundset.so_scheda_officina_id;
				foundset.scheda_officina_to_scheda_officina_attivita.search();
			}

		if (scheda_officina_to_scheda_officina_esecuzione_attivita)
			if (foundset.scheda_officina_to_scheda_officina_esecuzione_attivita.find()) {
				foundset.scheda_officina_to_scheda_officina_esecuzione_attivita.fk_schede_officina = foundset.so_scheda_officina_id;
				foundset.scheda_officina_to_scheda_officina_esecuzione_attivita.search();
			}

		if (scheda_officina_to_scheda_officina_documenti)
			if (foundset.scheda_officina_to_scheda_officina_documenti.find()) {
				foundset.scheda_officina_to_scheda_officina_documenti.table_name_fk = 'so_schede_officina';
				foundset.scheda_officina_to_scheda_officina_documenti.record_fk = foundset.so_scheda_officina_id;
				foundset.scheda_officina_to_scheda_officina_documenti.search();
			}

		if (scheda_officina_to_scheda_officina_software)
			if (foundset.scheda_officina_to_scheda_officina_software.find()) {
				foundset.scheda_officina_to_scheda_officina_software.table_name_fk = 'so_schede_officina'
				foundset.scheda_officina_to_scheda_officina_software.record_fk = foundset.so_scheda_officina_id;
				foundset.scheda_officina_to_scheda_officina_software.search();
			}

	}

	forms.so_tab_attivita_right.sincro();
	forms.so_tab_esecuzione_left.sincro();
	forms.so_tab_esecuzione_right.sincro();

//	forms.so_tab_attivita.verify(event);
//	forms.so_tab_attivita_right.verify(event);

	updateUI(event);

	application.output(globals.messageLog + "STOP so_dettaglio.syncTabs() ", LOGGINGLEVEL.INFO);

}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"205BFA0D-50E1-4D14-A708-81562349594F"}
 */
function onShow(firstShow, event) {
	application.output(globals.messageLog + "START so_dettaglio.onShow() ", LOGGINGLEVEL.INFO);

	updateUI(event);

	application.output(globals.messageLog + "STOP so_dettaglio.onShow() ", LOGGINGLEVEL.INFO);

}

/**
 * @properties={typeid:24,uuid:"EBE0DAAF-63B1-4222-9D3D-7DFFDF022766"}
 */
function dataValidation() {

	//Controllo dei dati , restituisce un array con i campi mancanti.

	application.output(globals.messageLog + "START so_dettaglio.dataValidation() ", LOGGINGLEVEL.INFO);

	var array = [];

	//	if (progetto == null) array.push('Progetto');
	//	if (commessa == null) array.push('Commessa');
	//	if (vettura == null) array.push('Vettura');

	application.output(globals.messageLog + "STOP so_dettaglio.dataValidation() missing field : " + array, LOGGINGLEVEL.INFO);

	return array;

}

/**
 * TODO generated, please specify type and doc for the params
 * @param {JSEvent} event the event that triggered the action *
 * @properties={typeid:24,uuid:"6BB68592-9759-4DDB-B945-7C0129B484FC"}
 */
function updateUI(event) {

	application.output(globals.messageLog + "START so_dettaglio.updateUI() ", LOGGINGLEVEL.INFO);

	_super.updateUI(event);

	application.output(globals.messageLog + "STOP so_dettaglio.updateUI() ", LOGGINGLEVEL.INFO);

}
