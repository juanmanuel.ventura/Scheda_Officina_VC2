/**
 * @param event
 *
 * @properties={typeid:24,uuid:"9802DD7E-6FA9-4412-BEF7-3F6A0365A2B0"}
 */
function onActionGoTo(event) {

	//Azione bottone di ogni record , visualizzazione del dettaglio , sincronizza i tab

	application.output(globals.messageLog + "START so_list.onActionGoTo() ", LOGGINGLEVEL.INFO);

	forms.main_so.goToDetail(event);

	updateUI(event);

	application.output(globals.messageLog + "STOP so_list.onActionGoTo() ", LOGGINGLEVEL.INFO);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"6D9289A5-381C-43AA-924A-C892A9CA23F0"}
 */
function updateUI(event) {
	application.output(globals.messageLog + "START so_list.updateUI() ", LOGGINGLEVEL.INFO);

	_super.updateUI(event);

	application.output(globals.messageLog + "STOP so_list.updateUI() ", LOGGINGLEVEL.INFO);

}
