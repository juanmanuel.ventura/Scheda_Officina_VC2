/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"D8975903-8B11-4D09-B74B-A7D12548D9E9",variableType:4}
 */
var selectedRec = 1;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"4A9A1311-3C55-4DBD-8A53-F117C2BA1363"}
 */
function onLoad(event) {

	application.output(globals.messageLog + "START so_software.onLoad() ", LOGGINGLEVEL.INFO);

	_super.updateUI(event);

	application.output(globals.messageLog + "STOP so_software.onLoad() ", LOGGINGLEVEL.INFO);

}

/**
 * Callback method when form is resized.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"F33DBA00-02D6-4A40-874D-79E3EBAC175B"}
 */
function onResize(event) {
	application.output(globals.messageLog + "START so_software.onResize() ", LOGGINGLEVEL.INFO);

	_super.updateUI(event);

	application.output(globals.messageLog + "STOP so_software.onResize() ", LOGGINGLEVEL.INFO);

}
