/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"327D11EB-0C8C-4145-B765-BD571F3A7E41"}
 */
function onLoad(event) {

	//Dichiaro la grandezza della form di sinsitra del tab

	application.output(globals.messageLog + "START so_tab_attivita.onLoad() ", LOGGINGLEVEL.INFO);

	elements.split.leftFormMinSize = 830;

	application.output(globals.messageLog + "STOP so_tab_attivita.onLoad() ", LOGGINGLEVEL.INFO);

}

/**
 * Callback method when form is resized.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"F855D295-D759-47FD-A7DE-EE7C28433DC2"}
 */
function onResize(event) {

	//Dichiaro la grandezza della form di sinsitra del tab

	application.output(globals.messageLog + "START so_tab_attivita.onResize() ", LOGGINGLEVEL.INFO);

	elements.split.leftFormMinSize = 830;

	application.output(globals.messageLog + "STOP so_tab_attivita.onResize() ", LOGGINGLEVEL.INFO);

}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"D20E776E-2755-40D6-BBCF-FB686DEFF76B"}
 */
function onShow(firstShow, event) {

	//Se non ho record , nascondo tab e bottoni degli operatori (interno)

	application.output(globals.messageLog + "START so_tab_attivita.onShow() ", LOGGINGLEVEL.INFO);

	if (forms.so_tab_attivita_left.foundset.getSize() == 0) {
		elements.split.visible = false;
	} else {
		elements.split.visible = true;
	}


	application.output(globals.messageLog + "STOP so_tab_attivita.onShow() ", LOGGINGLEVEL.INFO);

}

/**
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"C3F875A9-BF5D-4B71-BF8E-6E3ACA064BF7"}
 */
function verify(event) {

	//Nascondo il pannello se non ho record al suo interno

	application.output(globals.messageLog + "START so_tab_attivita.verify() ", LOGGINGLEVEL.INFO);

//	if (forms.so_tab_attivita_left.foundset.getSize() == 0) {
//		elements.split.visible = false;
//	} else {
//		elements.split.visible = true;
//	}
	application.output(globals.messageLog + "STOP so_tab_attivita.verify() ", LOGGINGLEVEL.INFO);

}
