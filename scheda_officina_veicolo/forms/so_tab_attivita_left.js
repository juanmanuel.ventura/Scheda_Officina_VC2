/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"7867B61A-3F31-45A9-AFB2-9BEF8DF4D4C7",variableType:4}
 */
var selectedRec = 1;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"65297F5C-045B-4168-8F1A-01BEA9DEE97F"}
 */
function onLoad(event) {

	application.output(globals.messageLog + "START so_tab_attivita_left.onLoad() ", LOGGINGLEVEL.INFO);

	_super.updateUI(event);
	
	application.output(globals.messageLog + "STOP so_tab_attivita_left.onLoad() ", LOGGINGLEVEL.INFO);

}

/**
 * Callback method when form is resized.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"ADCAFB0E-5E15-46C6-971C-59CB8AA76B50"}
 */
function onResize(event) {
	application.output(globals.messageLog + "START so_tab_attivita_left.onResize() ", LOGGINGLEVEL.INFO);

	_super.updateUI(event);
	
	application.output(globals.messageLog + "STOP so_tab_attivita_left.onResize() ", LOGGINGLEVEL.INFO);

}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"4CBA591D-FFD4-45C9-8784-130D4748B721"}
 */
function onShow(firstShow, event) {

	application.output(globals.messageLog + "START so_tab_attivita_left.onShow() ", LOGGINGLEVEL.INFO);

	forms.so_tab_attivita_right.sincro();

	_super.updateUI(event);
	
	application.output(globals.messageLog + "STOP so_tab_attivita_left.onShow() ", LOGGINGLEVEL.INFO);

}

/**
 * Handle record selected.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"818E758B-48EF-4F09-88A8-3A002DA90823"}
 */
function onRecordSelection(event) {

	application.output(globals.messageLog + "START so_tab_attivita_left.onRecordSelection() ", LOGGINGLEVEL.INFO);

	forms.so_tab_attivita_right.controller.loadRecords(righe_scheda_officina_to_righe_scheda_officina_op);
	
	_super.updateUI(event);

	application.output(globals.messageLog + "STOP so_tab_attivita_left.onRecordSelection() ", LOGGINGLEVEL.INFO);

}
