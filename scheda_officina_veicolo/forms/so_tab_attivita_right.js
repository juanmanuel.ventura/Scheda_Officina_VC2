/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"B6F7DC9D-FAF2-4CF4-BFF0-ABED6164197E",variableType:4}
 */
var selectedRecord = 1;

/**
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"D5B2F8F4-A22E-493D-9C70-6E06809D6940"}
 */
function sincro() {

	//Sincronizzo attività-operatori

	try {

		application.output(globals.messageLog + "START so_tab_attivita_right.sincro() ", LOGGINGLEVEL.INFO);

		var rec = forms.so_tab_attivita_left.foundset.getSelectedRecord();
		if (foundset.find()) {
			foundset.fk_righe_schede_officina = rec.k8_righe_schede_officina_id;
			foundset.search();
		}

		application.output(globals.messageLog + "STOP so_tab_attivita_right.sincro() PK K8_RIGHE_SCHEDE_OFFICINA : " + rec.k8_righe_schede_officina_id, LOGGINGLEVEL.INFO);

	} catch (exc) {
	}
}

/**
 * 
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"C8B1290F-DA82-434E-B419-0A27409C5C0C"}
 */
function onRecordSelection(event) {

	application.output(globals.messageLog + "START so_tab_attivita_right.onRecordSelection() ", LOGGINGLEVEL.INFO);

	selectedRecord = foundset.getSelectedIndex();

	application.output(globals.messageLog + "STOP so_tab_attivita_right.onRecordSelection() Record Selected : " + selectedRecord, LOGGINGLEVEL.INFO);

}

/**
 * @param {JSEvent} event the event that triggered the action *
 * @properties={typeid:24,uuid:"108F4A33-4158-4F2C-B490-9C3F86F722F2"}
 */
function verify(event) {

	//se non abbiamo attività , nascondo i tast aggiungi e rimuovi operatori

	application.output(globals.messageLog + "START so_tab_attivita_right.verify() ", LOGGINGLEVEL.INFO);

//	if (forms.so_tab_attivita_left.foundset.getSize() > 0) {
//		forms.so_tab_attivita.elements.buttonDelOP.visible = false;
//		forms.so_tab_attivita.elements.buttonMenoOP.visible = true;
//		forms.so_tab_attivita.elements.buttonPiuOP.visible = true;
//		forms.so_tab_attivita.elements.buttonSaveOP.visible = false;
//	} else {
//		forms.so_tab_attivita.elements.buttonDelOP.visible = false;
//		forms.so_tab_attivita.elements.buttonMenoOP.visible = false;
//		forms.so_tab_attivita.elements.buttonPiuOP.visible = false;
//		forms.so_tab_attivita.elements.buttonSaveOP.visible = false;
//	}

	application.output(globals.messageLog + "STOP so_tab_attivita_right.verify() ", LOGGINGLEVEL.INFO);

}
