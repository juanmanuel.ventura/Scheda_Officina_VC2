/**
 * @properties={typeid:35,uuid:"90D6BD48-F630-48E0-A399-856201BCD57B",variableType:-4}
 */
var insert = false;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"63B00459-9F13-40D0-BE05-8A9E71F8771A"}
 */
function onLoad(event) {

	application.output(globals.messageLog + "START so_tab_esecuzione.onLoad() ", LOGGINGLEVEL.INFO);

	elements.split.leftFormMinSize = 250;

	application.output(globals.messageLog + "STOP so_tab_esecuzione.onLoad() ", LOGGINGLEVEL.INFO);

}

/**
 * Callback method when form is resized.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"AB4FFA52-ABBC-47D2-AE28-E95C9E362FB6"}
 */
function onResize(event) {

	application.output(globals.messageLog + "START so_tab_esecuzione.onResize() ", LOGGINGLEVEL.INFO);

	elements.split.leftFormMinSize = 250;

	application.output(globals.messageLog + "STOP so_tab_esecuzione.onResize() ", LOGGINGLEVEL.INFO);

}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"B1464845-6F70-4816-8E69-AF72423016F0"}
 */
function onShow(firstShow, event) {

	application.output(globals.messageLog + "START so_tab_esecuzione.onShow() ", LOGGINGLEVEL.INFO);

	if (forms.so_tab_esecuzione_left.foundset.getSize() == 0) {
		elements.split.visible = false;
	} else {
		elements.split.visible = true;
	}

	forms.so_tab_esecuzione_left.sincro();

	application.output(globals.messageLog + "STOP so_tab_esecuzione.onShow() ", LOGGINGLEVEL.INFO);

}

/**
 * @param {JSEvent} event the event that triggered the action *
 * @properties={typeid:24,uuid:"D89DDA43-F78C-4A66-86B2-413B64A264E5"}
 */
function verify(event) {

	//Se non ho record , nascondo il tab

	application.output(globals.messageLog + "START so_tab_esecuzione.verify() ", LOGGINGLEVEL.INFO);

//	if (forms.so_tab_esecuzione_left.foundset.getSize() == 0) {
//		elements.split.visible = false;
//	} else {
//		elements.split.visible = true;
//	}
//
//	forms.so_tab_esecuzione_left.sincro();
//	

	application.output(globals.messageLog + "STOP so_tab_esecuzione.verify() ", LOGGINGLEVEL.INFO);

}
