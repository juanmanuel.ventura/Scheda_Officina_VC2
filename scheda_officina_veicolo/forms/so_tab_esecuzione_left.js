/**
 * Handle record selected.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"914C0F83-EAC5-4335-8472-9AAF64787DD6"}
 * @AllowToRunInFind
 */
function onRecordSelection(event) {

	//carica gli operatori in base all'attività selezionata

	application.output(globals.messageLog + "START so_tab_esecuzione_left.onRecordSelection() ", LOGGINGLEVEL.INFO);

	forms.so_tab_esecuzione_right.controller.loadRecords(righe_scheda_officina_to_righe_scheda_officina_op);

	application.output(globals.messageLog + "STOP so_tab_esecuzione_left.onRecordSelection() ", LOGGINGLEVEL.INFO);

}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"38348C9E-E7C7-4005-BAC2-4719350C065C"}
 * @AllowToRunInFind
 */
function onLoad(event) {
	application.output(globals.messageLog + "START so_tab_esecuzione_left.onLoad() ", LOGGINGLEVEL.INFO);

	application.output(globals.messageLog + "STOP so_tab_esecuzione_left.onLoad() ", LOGGINGLEVEL.INFO);

}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"9B95D5A4-C0CE-4FEA-B89C-86D7EF51BE46"}
 * @AllowToRunInFind
 */
function onShow(firstShow, event) {

	application.output(globals.messageLog + "START so_tab_esecuzione_left.onShow() ", LOGGINGLEVEL.INFO);

	application.output(globals.messageLog + "STOP so_tab_esecuzione_left.onShow() ", LOGGINGLEVEL.INFO);

}

/**
 *
 * @properties={typeid:24,uuid:"C971F8CE-3BB3-4646-B901-638D714C12D2"}
 * @AllowToRunInFind
 */
function sincro() {

	//Sincronizza attività - operatori

	try {

		application.output(globals.messageLog + "START so_tab_esecuzione_left.sincro() ", LOGGINGLEVEL.INFO);

		var rec = forms.so_dettaglio.foundset.getSelectedRecord();
		if (foundset.find()) {
			foundset.fk_schede_officina = rec.so_scheda_officina_id;
			foundset.search();
		}

		application.output(globals.messageLog + "STOP so_tab_esecuzione_left.sincro() on PK SO_SCHEDE_OFFICINA : " + rec.so_scheda_officina_id, LOGGINGLEVEL.INFO);

	} catch (exc) {
	}
}
