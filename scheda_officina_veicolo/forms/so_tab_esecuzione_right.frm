dataSource:"db:/ferrari/k8_righe_schede_officina_op",
items:[
{
location:"110,5",
size:"100,20",
styleClass:"base",
text:"Data ",
transparent:true,
typeid:7,
uuid:"115CD701-AE6C-4A07-BEFC-78750832B86A"
},
{
dataProviderID:"ora_fine",
location:"320,35",
size:"100,20",
typeid:4,
uuid:"1EB659AC-564B-45F6-B5D8-B88269BE70F2"
},
{
dataProviderID:"ora_inizio",
location:"215,35",
size:"100,20",
typeid:4,
uuid:"2527C3FE-2663-47A0-B8F5-570A5151C739"
},
{
dataProviderID:"esecutore",
format:"|U",
location:"3,35",
size:"100,20",
typeid:4,
uuid:"2B128634-B820-4AEB-A114-90DB1ED636AD"
},
{
dataProviderID:"descrizione_lavoro",
location:"425,35",
size:"200,20",
typeid:4,
uuid:"3837FEB6-050A-417A-921A-962B400DE504"
},
{
dataProviderID:"data_esecuzione",
displayType:5,
location:"110,35",
size:"100,20",
typeid:4,
uuid:"3FB71308-07D5-4A9C-90F1-289EBDF6937B"
},
{
height:60,
partType:5,
typeid:19,
uuid:"56DCC447-A25D-4BD2-BE0D-372984746DAC"
},
{
location:"320,5",
size:"100,20",
styleClass:"base",
text:"Ora fine",
transparent:true,
typeid:7,
uuid:"59C233E5-CFD8-413E-B9E3-3F3FA260B6B7"
},
{
location:"425,5",
size:"200,20",
styleClass:"base",
text:"Descrizione Lavoro",
transparent:true,
typeid:7,
uuid:"637C727C-3B42-452E-8F3A-630390E59C10"
},
{
location:"215,5",
size:"100,20",
styleClass:"base",
text:"Ora inizio",
transparent:true,
typeid:7,
uuid:"9096D141-7DCF-48A4-AC17-8DEDA9A565B0"
},
{
location:"5,5",
size:"98,20",
styleClass:"base",
text:"Esecutore",
transparent:true,
typeid:7,
uuid:"A95A5C2F-6EDA-49C6-B995-6108D2F0526B"
},
{
location:"630,5",
size:"100,20",
styleClass:"base",
text:"Segnalazione",
transparent:true,
typeid:7,
uuid:"CA5043CF-BE54-4DE7-9FE8-BAC991441CF6"
},
{
dataProviderID:"stato",
location:"735,35",
size:"80,20",
typeid:4,
uuid:"CA520E31-5D79-4FE8-AE2A-4190A6C1C4DF"
},
{
dataProviderID:"segnalazione",
location:"630,35",
size:"100,20",
typeid:4,
uuid:"ED2F2FB3-C53A-4638-AF29-D795B201B8F6"
},
{
height:30,
partType:1,
typeid:19,
uuid:"F2CC1675-4227-44F0-9D18-17C2B0AEB353"
},
{
location:"735,5",
styleClass:"base",
text:"Stato",
transparent:true,
typeid:7,
uuid:"F4F5F17C-1F57-42D2-BD19-BFABDF7F0A3D"
}
],
name:"so_tab_esecuzione_right",
onRecordSelectionMethodID:"35EF2912-527C-4902-A951-A3AD4490190A",
showInMenu:true,
size:"850,60",
styleClass:"base",
styleName:"ferrari_web_css",
transparent:true,
typeid:3,
uuid:"6D542F9F-3EF6-4599-9279-2D0BDF840E29",
view:1