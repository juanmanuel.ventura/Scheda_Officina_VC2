/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"B750AEAB-5D62-4036-B482-2129E5A8CA4A",variableType:4}
 */
var selectedRecord = 1;

/**
 * Handle record selected.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"35EF2912-527C-4902-A951-A3AD4490190A"}
 */
function onRecordSelection(event) {

	//aggiorno la variabile del record selezionato , ci serve nel delete record

	application.output(globals.messageLog + "START so_tab_esecuzione_right.onRecordSelection() ", LOGGINGLEVEL.INFO);

	selectedRecord = foundset.getSelectedIndex();

	application.output(globals.messageLog + "STOP so_tab_esecuzione_rightonRecordSelection() Selected record Righe : " + selectedRecord, LOGGINGLEVEL.INFO);

}

/**
 * @AllowToRunInFind
 *
 * @properties={typeid:24,uuid:"09517006-1CAF-448A-92B5-DB8418AA3557"}
 */
function sincro() {

	//sincronizzo attività - operatori

	try {

		application.output(globals.messageLog + "START so_tab_esecuzione_right.sincro() ", LOGGINGLEVEL.INFO);

		var rec = forms.so_tab_esecuzione_left.foundset.getSelectedRecord();
		if (foundset.find()) {
			foundset.fk_righe_schede_officina = rec.k8_righe_schede_officina_id;
			foundset.search();
		}

		application.output(globals.messageLog + "STOP so_tab_esecuzione_right.sincro() PK_RIGHE_SCHEDE_OFFICINA : " + rec.k8_righe_schede_officina_id, LOGGINGLEVEL.INFO);

	} catch (exc) {
	}

}
