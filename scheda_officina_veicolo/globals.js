/**
 * @properties={typeid:24,uuid:"EF1EFC3C-F7CD-41EE-8A0A-BBD4959E9CEC"}
 */
function exitEditMode() {
	
}

/**
 * Callback method for when solution is opened.
 * When deeplinking into solutions, the argument part of the deeplink url will be passed in as the first argument
 * All query parameters + the argument of the deeplink url will be passed in as the second argument
 * For more information on deeplinking, see the chapters on the different Clients in the Deployment Guide.
 *
 * @param {String} arg startup argument part of the deeplink url with which the Client was started
 * @param {Object<Array<String>>} queryParams all query parameters of the deeplink url with which the Client was started
 *
 * @properties={typeid:24,uuid:"D5771E18-7444-471B-AC44-8E6627E728BD"}
 */
function onSolutionOpen(arg, queryParams) {
	
	application.output(globals.messageLog+"START globals.onSolutionOpen() ",LOGGINGLEVEL.INFO);	
	
	if (application.getApplicationType() == APPLICATION_TYPES.WEB_CLIENT) {
		  application.putClientProperty(APP_WEB_PROPERTY.WEBCLIENT_TEMPLATES_DIR, 'custom');
		  application.putClientProperty(APP_UI_PROPERTY.TABLEVIEW_WC_DEFAULT_SCROLLABLE, true);
		  application.putClientProperty(APP_UI_PROPERTY.TABLEVIEW_WC_SCROLLABLE_KEEP_LOADED_ROWS, true);
		 }
	
	application.output(globals.messageLog+"STOP globals.onSolutionOpen() ",LOGGINGLEVEL.INFO);		 	
}
