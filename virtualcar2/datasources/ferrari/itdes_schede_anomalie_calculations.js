/**
 *
 * @properties={type:12,typeid:36,uuid:"1752C462-C996-42DE-81C2-14B6D900061B"}
 */
function descrizione_estesa_str()
{
	var ext_desc = "";
	// La relazione utilizzata
	var r = itdes_schede_anomalie_to_k8_anomalie_desc_full; 
	if (r && r["sort"]){
		r.sort("nr_riga asc");
		for (var i = 1; i <= r.getSize(); i++){
			r.setSelectedIndex(i);
			ext_desc += r.descrizione;
		}
	}
	return ext_desc;
}

/**
 *
 * @properties={type:4,typeid:36,uuid:"B935D6B9-6F58-429A-AC9F-AC34C20BDC56"}
 */
function doc_count()
{
	return itdes_schede_anomalie_to_k8_documenti.getSize();
}

/**
 *
 * @properties={type:6,typeid:36,uuid:"65A0E1B2-1F30-4290-8388-A495EA8C977A"}
 */
function frequenza()
{
	return itdes_schede_anomalie_to_k8_anomalie_vetture.getSize();
}

/**
 *
 * @properties={type:-4,typeid:36,uuid:"6622E1A6-237D-43B1-95B8-753564F2557D"}
 */
function icon_criticita()
{
	return globals.vc2_icon_criticita(indice_criticita);
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"3A9F913E-E47A-4921-8C6A-2642A02BDE1B"}
 */
function icona_tipo_anomalia()
{
	if (!tipo_anomalia){
		return null;
	}
	if (tipo_anomalia == "Processo Fornitore"){
		return "media:///processo_fornitore.png"
	}else if(tipo_anomalia == "Processo Interno"){
		return "media:///processo_interno.png"
	}else if(tipo_anomalia == "Progetto"){
		return "media:///progetto.png"
	}else if(tipo_anomalia == "In Diagnosi"){
		return "media:///in_diagnosi.png"
	}else{
		return null;
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"4727C3F4-96A0-4145-BD3E-2BF8F291B019"}
 */
function numero_disegno_str()
{
	if (codice_disegno){
		return codice_disegno.toString();
	}	
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"F047F2A2-55C3-463F-8BA8-4D3F2FE5A77D"}
 */
function row_color()
{
	if (indice_criticita == "Rosso"){
		return "#FF4444";
	}else if (indice_criticita == "Verde"){
		return "#44FF44";
	}else if (indice_criticita == "Giallo"){
		return "#FFFF00";
	}else{
		return "#FFFFFF";
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"D4D51204-C15A-4293-B1A0-FC5B92A6A1D8"}
 */
function short_description()
{
	return globals.utils_smartTruncate(k8_anomalie_to_k8_anomalie_desc_full.testo, 65);
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"AB173C47-8178-4DC6-8C25-B997AD8086A9"}
 */
function table_name()
{
return "k8_schede_anomalie";
}
