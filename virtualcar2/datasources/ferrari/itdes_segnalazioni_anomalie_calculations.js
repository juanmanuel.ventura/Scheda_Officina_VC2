/**
 *
 * @properties={type:12,typeid:36,uuid:"B66A12CD-F54B-4FB0-ABA9-EEE979AA3BF0"}
 */
function descrizione_estesa_str()
{
	return "";
}

/**
 *
 * @properties={type:4,typeid:36,uuid:"C798725A-A460-437E-A43A-5FFDC91CD8E4"}
 */
function doc_count()
{
	if (itdes_segnalazioni_anomalie_to_k8_documenti)
		return itdes_segnalazioni_anomalie_to_k8_documenti.getSize();
	else
		return 0;
}

/**
 *
 * @properties={type:6,typeid:36,uuid:"4F650936-3A82-4E56-8DA9-F2E59F55DD12"}
 */
function frequenza()
{
	if (itdes_segnalazioni_anomalie_to_k8_anomalie_vetture)
		return itdes_segnalazioni_anomalie_to_k8_anomalie_vetture.getSize();
	else
		return 0;
}

/**
 *
 * @properties={typeid:36,uuid:"B50C015B-2461-42F8-AB24-DBDB552C7104"}
 */
function icon_criticita()
{
	return globals.vc2_icon_criticita(indice_criticita);
}

/**
 *
 * @properties={typeid:36,uuid:"C7339E59-4918-44B3-8BA8-092D7B1CC395"}
 */
function icona_tipo_anomalia()
{
	if (!tipo_anomalia){
		return null;
	}
	if (tipo_anomalia == "Processo Fornitore"){
		return "media:///processo_fornitore.png"
	}else if(tipo_anomalia == "Processo Interno"){
		return "media:///processo_interno.png"
	}else if(tipo_anomalia == "Progetto"){
		return "media:///progetto.png"
	}else if(tipo_anomalia == "In Diagnosi"){
		return "media:///in_diagnosi.png"
	}else{
		return null;
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"755F2BAB-0277-47C6-8BD4-A033AAAE66C9"}
 */
function numero_disegno_str()
{
	if (codice_disegno){
		return codice_disegno.toString();
	}	
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"6AEE55B9-8A2D-440E-927D-FB025376558D"}
 */
function shortDesc()
{
	return globals.utils_smartTruncate(itdes_segn_anomalie_to_k8_segn_desc_full.testo, 65);
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"87B200E2-D67B-4A88-832F-1EED54167627"}
 */
function tableName()
{
return "k8_segnalazioni_anomalie";
}
