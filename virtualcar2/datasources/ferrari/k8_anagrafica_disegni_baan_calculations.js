/**
 *
 * @properties={type:12,typeid:36,uuid:"9E5F70B8-1880-47DC-849E-B687DB30934B"}
 */
function numero_disegno_as400()
{
	if(k8_anagrafica_disegni_baan_to_k8_jt_as400_baan && k8_anagrafica_disegni_baan_to_k8_jt_as400_baan.numero_disegno)
		return k8_anagrafica_disegni_baan_to_k8_jt_as400_baan.numero_disegno.toString();
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"7800ACA2-B892-4C1C-8481-04B7196901A8"}
 */
function tableName()
{
return "k8_anagrafica_disegni_baan";
}
