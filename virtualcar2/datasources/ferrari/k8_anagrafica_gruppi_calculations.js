/**
 *
 * @properties={typeid:36,uuid:"FBB61A71-F649-4103-8ADB-5A5BC51FADC1"}
 * @AllowToRunInFind
 */
function anomalie_in_diagnosi()
{
	if (k8_progetto_to_anomalie_to_tipo.find()){
		k8_progetto_to_anomalie_to_tipo.tipo_anomalia = "In Diagnosi";
		k8_progetto_to_anomalie_to_tipo.search();
	}
	var n = k8_progetto_to_anomalie_to_tipo.getSize();
	k8_progetto_to_anomalie_to_tipo.loadAllRecords();
	return n;
}

/**
 *
 * @properties={typeid:36,uuid:"8AF48FA0-1AE5-4888-820C-5E2E9C2443F4"}
 * @AllowToRunInFind
 */
function anomalie_processo_fornitore()
{
	if (k8_progetto_to_anomalie_to_tipo.find()){
		k8_progetto_to_anomalie_to_tipo.tipo_anomalia = "Processo Fornitore";
		k8_progetto_to_anomalie_to_tipo.search();
	}
	var n = k8_progetto_to_anomalie_to_tipo.getSize();
	k8_progetto_to_anomalie_to_tipo.loadAllRecords();
	return n;
}

/**
 *
 * @properties={typeid:36,uuid:"E3F03612-4DEA-4A6E-9E72-C25A26E633C4"}
 * @AllowToRunInFind
 */
function anomalie_processo_interno()
{
	if (k8_progetto_to_anomalie_to_tipo.find()){
		k8_progetto_to_anomalie_to_tipo.tipo_anomalia = "Processo Interno";
		k8_progetto_to_anomalie_to_tipo.search();
	}
	var n = k8_progetto_to_anomalie_to_tipo.getSize();
	k8_progetto_to_anomalie_to_tipo.loadAllRecords();
	return n;
}

/**
 *
 * @properties={typeid:36,uuid:"B7BA3005-6CAB-49F4-BEA8-428413E9547D"}
 * @AllowToRunInFind
 */
function anomalie_progetto()
{
	if (k8_progetto_to_anomalie_to_tipo.find()){
		k8_progetto_to_anomalie_to_tipo.tipo_anomalia = "Progetto";
		k8_progetto_to_anomalie_to_tipo.search();
	}
	var n = k8_progetto_to_anomalie_to_tipo.getSize();
	k8_progetto_to_anomalie_to_tipo.loadAllRecords();
	return n;
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"CCC38F13-3859-4D71-B696-828BEDF61A8D"}
 */
function descrizione_estesa()
{
	var desc = gruppo + " " + descrizione //+ " (" + k8_progetto_to_anomalie_to_tipo.getSize() + ")"
//	desc += " D: (" + anomalie_in_diagnosi +")";
//	desc += " Pr: (" + anomalie_progetto +")";
//	desc += " Pi: (" + anomalie_processo_interno +")";
//	desc += " Pf: (" + anomalie_processo_fornitore +")";
	return desc;
}
