/**
 *
 * @properties={type:12,typeid:36,uuid:"3402DE7C-EFBC-47E2-85F6-2DFE573D0791"}
 */
function codice_vettura_str()
{
	return (codice_vettura) ? codice_vettura.toString() : null;
}
