/**
 *
 * @properties={type:12,typeid:36,uuid:"EC18D977-566F-487E-B343-B0303730EFA5"}
 */
function codice_disegno_str()
{
	if (codice_disegno){
		return codice_disegno.toString();
	}else{
		return "";
	}
}

/**
 *
 * @properties={typeid:36,uuid:"660D6D45-5DCC-4E8C-BC6D-378159B2B8C4"}
 */
function descrizione_estesa_str()
{
	var ext_desc = "";
	// La relazione utilizzata
	var r = tipo_scheda == "ANOMALIA" ? k8_anomlie_tree_to_k8_anomalie_scheda_descrizione : k8_anomlie_tree_to_k8_anomalie_segnalazione_descrizione;
	var text = r.testo ? r.testo : "Nessuna Descrizione";
	return text;
}

/**
 *
 * @properties={typeid:36,uuid:"5DF65E06-FAA8-413E-A8A2-F8C28E5ECF90"}
 */
function icona_tipo_anomalia()
{
	if (!tipo_anomalia){
		return null;
	}
	if (tipo_anomalia == "Processo Fornitore"){
		return "media:///processo_fornitore.png"
	}else if(tipo_anomalia == "Processo Interno"){
		return "media:///processo_interno.png"
	}else if(tipo_anomalia == "Progetto"){
		return "media:///progetto.png"
	}else if(tipo_anomalia == "In Diagnosi"){
		return "media:///in_diagnosi.png"
	}else{
		return null;
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"BB0AF518-ED08-4011-8D14-9F8D153C29EB"}
 */
function numero_scheda_str()
{
	if (tipo_scheda == "ANOMALIA"){
		return numero_scheda;
	}else{
		return numero_segnalazione;
	}
}
