/**
 * @properties={type:-4,typeid:36,uuid:"21B813FE-B461-4CD3-888B-A5D892CF0452"}
 */
function tree_icon()
{
	if(status && status != "?"){
		if(status == "+"){
			return "media:///treeclose.png";
		}
		if(status == "-"){
			return "media:///treeopen.png";
		}
		return "media:///treeleaf.png";
	}
}

/**
 * @properties={type:12,typeid:36,uuid:"0D74B4B3-3679-4E6A-A25A-B15CE3589E3F"}
 */
function status()
{
	if(conteggio_figli > 0){
		if(status == "-"){
			return "-";
		}else{
			return "+";
		}
	}else{
		return "O";
	}
}

/**
 * @properties={type:12,typeid:36,uuid:"F9F473AB-8A77-478B-8D84-8A1F8A78A005"}
 */
function descrizione_tree()
{
	var str = "";
	if(descrizione && livello - 1){
		for(var i=0;i<livello - 1;i++){
			str += "          ";
		}
	}
	str += descrizione;
	str += (fake_component) ? " **" : "";
	return  str;
}
