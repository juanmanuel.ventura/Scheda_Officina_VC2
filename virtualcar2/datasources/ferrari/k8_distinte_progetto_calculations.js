/**
 *
 * @properties={type:-4,typeid:36,uuid:"00BDF948-4C63-47BB-8EA0-CE5CA7443940"}
 */
function tree_icon()
{
	if(status && status != "?"){
		if(status == "+"){
			return "media:///treeclose.png";
		}
		if(status == "-"){
			return "media:///treeopen.png";
		}
		return "media:///treeleaf.png";
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"D2C380BD-90C1-40AD-B75C-570EC7D99A7F"}
 */
function status()
{
	if(conteggio_figli > 0){
		if(status == "-"){
			return "-";
		}else{
			return "+";
		}
	}else{
		return "O";
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"CDAB9C22-F8E0-4305-8267-48E54D4817EE"}
 */
function descrizione_tree()
{
	if(descrizione && livello - 1){
		var str = "";
		for(var i=0;i<livello - 1;i++){
			str += "          ";
		}
		str += descrizione;
		return str;
	}
	return descrizione;
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"C42C03BD-3528-4E52-893B-BF1E71805BDB"}
 */
function anomalia_associata()
{
	/*
	if (k8_distinte_progetto_to_k8_schede_anomalie.getSize() > 0){
		return "Diretta"
	}else if (k8_distinte_progetto_to_k8_anomalie_disegni.getSize() > 0){
		return "Indiretta";
	}else{
		return "Nessuna";
	}
	*/
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"C1A2BCC3-6D73-4D8A-A5E6-7B22A1183909"}
 */
function codice_baan_str()
{
	if(k8_distinte_progetto_to_k8_jt_as400_baan.codice_articolo_baan)
		return k8_distinte_progetto_to_k8_jt_as400_baan.codice_articolo_baan.toString();
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"B8597B9C-290A-4207-9735-1CB483DB6E88"}
 */
function coefficiente_impiego_str()
{
	if (coefficiente_impiego){
		return coefficiente_impiego.toString();
	}	
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"430DA736-4DC8-42EA-9917-DA89B99DDCF8"}
 */
function doc_count()
{
	return k8_distinte_progetto_to_k8_documenti.getSize();
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"F5864136-4576-41F4-85F3-E07AD7968940"}
 */
function esponente_str()
{
	if (esponente){
		var e = esponente.toString();
			return e;
	}else{
			return "0";
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"B3BFDA07-8225-4011-B7D9-3B3888E83C65"}
 */
function numero_anomalie()
{
	//var localCount = conto_anomalie || 0; //k8_distinte_progetto_to_k8_schede_anomalie_aperte.getSize();
	//var args = path_id + '%'; 
	//var childCount = globals.contaSchedeFigli([args]) || 0 ;
	
	//var totalCount = localCount + childCount;
	return conto_anomalie + " (" + conto_anomalie_figli + ")";
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"FFAACE8D-6D19-4AEB-9C13-89627910019B"}
 */
function numero_disegno_str()
{
	if (numero_disegno){
		return numero_disegno.toString();
	}	
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"BAFF88EB-D7B4-455D-87C4-618A6D0A41CB"}
 */
function numero_modifiche()
{
	/*var args = path_id + '%'; 
	
	var localCount = conto_schede_mod; //k8_distinte_progetto_to_k8_schede_modifica.getSize();
	var childCount = globals.contaModificheFigli([args]) || 0 ;
	
	var totalCount = localCount + childCount; */
	return conto_schede_mod + " (" + conto_schede_mod_figli + ")";
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"E6EADA29-83AD-436B-BCA1-D99CD3E8F4D7"}
 */
function numero_segnalazioni()
{
	/*
	var localCount = conto_segnalazioni; //k8_distinte_progetto_to_k8_segnalazioni_anomalie_aperte.getSize();
	var args = path_id + '%';
	var childCount = globals.contaSegnalazioniFigli([args]) || 0 ;

	var totalCount = localCount + childCount; */
	return conto_segnalazioni + " (" + conto_segnalazioni_figli + ")";
	
}

/**
 * @properties={typeid:36,uuid:"FECDE129-6380-4BB4-BD99-AB16035B8A80"}
 */
function pathToRoot()
{
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"20AEB9CF-A239-4629-B4CF-620E2A0AA64D"}
 */
function projectTreePath()
{
	/*
	var vNumeroDisegno = codice_padre;
	var vPk = k8_distinte_progetto_id;
	var vVettura 		= codice_vettura;
	var vSelectionPath	= new Array();
	var vPathString		= "";
	vSelectionPath.unshift(vPk);
	while(vNumeroDisegno)
	{
		var vQuery = "SELECT CODICE_PADRE, K8_DISTINTE_PROGETTO_ID FROM K8_DISTINTE_PROGETTO WHERE NUMERO_DISEGNO = ? AND CODICE_VETTURA = ?"
		var vDataSet	= databaseManager.getDataSetByQuery("ferrari",vQuery,[vNumeroDisegno, vVettura],-1)
		
		var vPk = vDataSet.getValue(1,2);
		vNumeroDisegno	= vDataSet.getValue(1,1);
		vSelectionPath.unshift(vPk);
	}
	vSelectionPath.push(numero_disegno);
	//Salta di proposito il primo che e' il codice progetto
	for (var i=0;i< vSelectionPath.length; i++){
		vPathString = vPathString + vSelectionPath[i];
		if (i != vSelectionPath.length -1){
			vPathString += "-";
		}
	}
	return vPathString;
	*/
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"BC840B88-2F17-429A-9ADA-21456A289432"}
 */
function projectTreePathNames()
{
	/*
	var vPk				= k8_distinte_progetto_id
	var vSelectionPath	= "";
	while(vPk)
	{
		var vQuery = "SELECT codice_padre , descrizione from k8_distinte_progetto WHERE numero_disegno = ?"
		var vDataSet	= databaseManager.getDataSetByQuery("ferrari",vQuery,[vPk],-1);
		var vParentPk		= vDataSet.getValue(1,1);
		var vDescription	= vDataSet.getValue(1,2);
		vSelectionPath		= ">"+vDescription+ vSelectionPath;
		if(vParentPk)
		{
			vPk = vParentPk;
		}
		else
		{
			vPk = null;
		}
	}
	return vSelectionPath.substring(1);
	*/
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"8FC9FFD0-F61E-4054-B5F4-11DD29D73E57"}
 */
function tableName()
{
	return "k8_distinte_progetto";
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"8B48AC80-004E-42E6-8B5B-52A24B5530B8"}
 */
function treeIconAffi()
{
	/*
	var a = null;
	var s = null;
	if (affidabilita){
		a = affidabilita.toLowerCase();
	}
	if (significativita){
		s = significativita.toLowerCase();
	}
	if (a == "si"){
		if (s == "si"){
			return "media:///significativita_si.png";
		}else if (s == "ni"){
			return "media:///significativita_ni.png";
		}else if (s == "no"){
			return "media:///significativita_no.png";
		}else{
			return "media:///significativita_null.png";
		}
	}
	else if (utils.hasRecords(k8_distinte_progetto$figli))
	{
		return "media:///treefolder.png";
	}
	else
	{
		return "media:///treeleaf.png";
	}
	*/
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"AE842B46-F7D0-463A-A518-AA12DBDFB102"}
 */
function treeIconURL()
{
	/*
	var code = "";

	if(k8_distinte_progetto_to_k8_time_line.getSize() > 0){
		code += "1";
	}else{
		code += "0";
	}

	if(k8_distinte_progetto_to_k8_tag.getSize() > 0){
		code += "1";
	}else{
		code += "0";
	}

	if(k8_distinte_progetto_to_k8_documenti.getSize() > 0){
		code += "1";
	}else{
		code += "0";
	}

	if(code != "000"){
		return "media:///iconaAlbero_" + code + ".png";
	}else if(utils.hasRecords(k8_distinte_progetto$figli)){
		return "media:///treefolder.png";
	}else{
		return "media:///treeleaf.png";
	}
	*/
}
