/**
 *
 * @properties={type:12,typeid:36,uuid:"EC7C746A-527B-476D-A24E-E1AE284E8210"}
 */
function descrizione_cpl(){
	if(codice_cpl){
		return k8_distinte_progetto_core_to_k8_anagrafica_disegni$descrizione_complessivo.descrizione;
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"87E71076-3AB5-4BC8-94E1-799E2C358760"}
 */
function disegno_str(){
	if(numero_disegno){
		return numero_disegno.toString();
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"BBEBC6FD-CA5C-40BF-A663-08051EFF3F45"}
 */
function complessivo() {
	if (codice_cpl) {
		return codice_cpl + " - " + k8_distinte_progetto_core_to_k8_anagrafica_disegni$descrizione_complessivo.descrizione;
	}
}

/**
 *
 * @properties={type:6,typeid:36,uuid:"E539E352-393D-4A31-A062-958033126857"}
 */
function codice_cpl(){
	try{
		var index_start = path_numero_disegno.indexOf("/79") + 1;
		if(index_start){
			var index_end = path_numero_disegno.indexOf("(",index_start);
			return  utils.stringToNumber(path_numero_disegno.substring(index_start,index_end));
		}
		}
		catch (e){
			//application.output("Eccezione : "+e);
		}
	return null;
}

/**
 * @properties={type:-4,typeid:36,uuid:"300F86DD-890E-4B80-BD94-1160D3B86B21"}
 */
function tree_icon(){
	if(status && status != "?"){
		if(status == "+"){
			return "media:///treeclose.png";
		}
		if(status == "-"){
			return "media:///treeopen.png";
		}
		return "media:///treeleaf.png";
	}
}

/**
 * @properties={type:12,typeid:36,uuid:"29518B98-1EE7-4A91-9E6B-0EE606BC2B81"}
 */
function status(){
	if(conteggio_figli > 0){
		if(status == "-"){
			return "-";
		}else{
			return "+";
		}
	}else{
		return "O";
	}
}

/**
 * @properties={type:12,typeid:36,uuid:"00FDAF5A-F7CD-4607-97C7-9A5807A7779A"}
 */
function descrizione_tree(){
	if(descrizione && livello - 1){
		var str = "";
		for(var i=0;i<livello - 1;i++){
			str += "          ";
		}
		str += descrizione;
		return str;
	}
	return descrizione;
}
