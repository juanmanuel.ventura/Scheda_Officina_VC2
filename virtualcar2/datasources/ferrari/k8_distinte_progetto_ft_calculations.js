/**
 *
 * @properties={type:12,typeid:36,uuid:"A06E7290-3E9B-422C-A839-0BF68D1AF1C8"}
 */
function esponente_str()
{
	if (esponente){
		return esponente.toString();
	}else{
		return "0";
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"0C8880F6-981D-4429-A24A-1C4192E00D37"}
 */
function numero_disegno_str()
{
	if (numero_disegno){
		return numero_disegno.toString();
	}	
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"2FA52B4A-031B-4FF8-B7EA-41B889A1DE6C"}
 */
function tableName()
{
return 'k8_distinte_progetto';
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"C77A2E8B-0416-4669-B8F2-DA26D780CBC0"}
 */
function treeIconAffi()
{
	var a = null;
	var s = null;
	if (affidabilita){
		a = affidabilita.toLowerCase();
	}
	if (significativita){
		s = significativita.toLowerCase();
	}
	if (a == "si"){
		if (s == "si"){
			return "media:///significativita_si.png";
		}else if (s == "ni"){
			return "media:///significativita_ni.png";
		}else if (s == "no"){
			return "media:///significativita_no.png";
		}else{
			return "media:///significativita_null.png";
		}
	}
	else if (utils.hasRecords(k8_distinte_progetto$figli)){
		return "media:///treefolder.png";
	}else{
		return "media:///treeleaf.png";
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"5F97614C-9715-4349-BA3A-F0CC92194C40"}
 */
function treeIconURL()
{
	var code = "";

	if(k8_distinte_progetto_ft_to_k8_time_line.getSize() > 0){
		code += "1";
	}else{
		code += "0";
	}

	if(k8_distinte_progetto_ft_to_k8_tag.getSize() > 0){
		code += "1";
	}else{
		code += "0";
	}

	if(k8_distinte_progetto_ft_to_k8_documenti.getSize() > 0){
		code += "1";
	}else{
		code += "0";
	}

	if(code != "000"){
		return "media:///iconaAlbero_" + code + ".png";
	}else if(utils.hasRecords(k8_distinte_progetto$figli)){
		return "media:///treefolder.png";
	}else{
		return "media:///treeleaf.png";
	}
}
