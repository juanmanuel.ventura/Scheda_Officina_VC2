/**
 * @properties={typeid:36,uuid:"EF940C5D-E846-4D3D-BC1D-8014EB173988"}
 */
function selected()
{
}

/**
 * Calculate the row background color.
 *
 * @param {Number} index row index
 * @param {Boolean} selected is the row selected
 * @param {String} elementType element type
 * @param {String} dataProviderID element data provider
 * @param {Boolean} edited is the record edited
 *
 * @returns {Color} row background color
 *
 * @properties={type:12,typeid:36,uuid:"5E6DFD7D-3A14-4BDE-AD65-5458C4A8D1CB"}
 */
function rowBGColorCalc(index, selected, elementType, dataProviderID, edited)
{
	return (!selected && k8_distinte_riciclabilita_to_k8_distinta_baan_esponenti$esponente.esponente > esponente) ? "#FF0000" : null;
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"60847911-CE35-4904-94B6-952E1098E991"}
 */
function descrizione_tree()
{
	/*
	if(descrizione && livello - 1){
		var str = "";
		for(var i=0;i<livello - 1;i++){
			str += "          ";
		}
		str += descrizione;
		return str;
	}
	return descrizione;
	*/
}

/**
 *
 * @properties={type:-4,typeid:36,uuid:"72D31AF0-B488-4C40-8F50-E7B78E46D6E9"}
 */
function tree_icon()
{
	/*
	return "media:///treeleaf.png";
	*/
}
