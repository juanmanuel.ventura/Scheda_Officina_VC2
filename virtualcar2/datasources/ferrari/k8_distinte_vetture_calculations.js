/**
 *
 * @properties={type:6,typeid:36,uuid:"87A6D6F9-71E6-44FD-85D5-1F37AEE31F00"}
 */
function avanzamento()
{
	if(stato != "Guasto" && (significativita == "Si" || significativita == "Ni"))
	{
		var coefficienti = k8_distinte_vetture_to_k8_distinte_vetture_coeff;
		if(coefficienti)
		{
			var prodotto = 1;
			for(var i=1;i<=coefficienti.getSize();i++)
			{
				coefficienti.setSelectedIndex(i);
				prodotto *= (coefficienti.affaticamento && coefficienti.k8_distinte_vetture_coeff_to_k8_distinte_progetto_coeff.f_rilevante == "Rilevante") ? coefficienti.affaticamento : 1;
			}
			var risultato = Math.pow(prodotto,1/coefficienti.getSize());
			return risultato;
		}
	}
	else
	{
		return null;
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"E6CDBCF4-E8D5-4E90-81CF-CE146D2A3210"}
 */
function doc_count()
{
	//return k8_distinte_vetture_to_k8_documenti.getSize();
}

/**
 *
 * @properties={type:6,typeid:36,uuid:"4744E141-E1C8-44A2-B88F-367EF386E693"}
 */
function km_totali()
{
	/*
	var queryDV = "select sum(KM_PERCORSI) from K8_COEFFICIENTI_SU_COMPONENTI where PROGETTO = ? and VETTURA = ? and K8_DISTINTE_VETTURE_ID = ?";
	var queryM  = "select sum(KM_PERCORSI) from K8_COEFFICIENTI_SU_MONTAGGI where PROGETTO = ? and VETTURA = ? and FK_COMPONENTE = ?";
	var dv = databaseManager.getDataSetByQuery("ferrari",queryDV,[progetto,vettura,k8_distinte_vetture_id],1);
	var m = databaseManager.getDataSetByQuery("ferrari",queryM,[progetto,vettura,k8_distinte_vetture_id],1);
	var risultato = 0;
	risultato += (dv.getValue(1,1)) ? dv.getValue(1,1) : 0;
	risultato += (m.getValue(1,1)) ? m.getValue(1,1) : 0;
	return risultato;
	*/
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"7A01E827-0703-44CF-98A2-9C2F3CABE756"}
 */
function tableName()
{
return "k8_distinte_vetture";
}

/**
 *
 * @properties={type:93,typeid:36,uuid:"8A6FB3AB-94F9-4B11-862B-5C496A93B2F4"}
 */
function ultimo_collaudo()
{
	if(k8_distinte_vetture_to_k8_schede_collaudo)
	{
		var sc = k8_distinte_vetture_to_k8_schede_collaudo.duplicateFoundSet();
		sc.loadAllRecords();
		sc.sort("data desc");
		sc.setSelectedIndex(1);
		return sc.data;
	}
	else
	{
		return null;
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"F0308665-E2D0-4F7B-9241-F6420068BBC1"}
 */
function ultimo_collaudo_html()
{
	var oggi = new Date;
	var uc = utils.dateFormat(ultimo_collaudo,"dd/MM/yyyy");
	var style = (oggi - ultimo_collaudo > 86400000) ? " style='color:#FFFFFF;background-color:#FF0000'" : " style='color:#000000;background-color:#00FF00'";
	return "<html><body><p" + style + ">" + uc + "</p></body></html>";
}

/**
 * @properties={type:-4,typeid:36,uuid:"6299CC30-5068-4459-AE51-A1C57739BD9A"}
 */
function tree_icon()
{
	if(status && status != "?"){
		if(status == "+"){
			return "media:///treeclose.png";
		}
		if(status == "-"){
			return "media:///treeopen.png";
		}
		return "media:///treeleaf.png";
	}
}

/**
 * @properties={type:12,typeid:36,uuid:"45C26702-CE07-4881-B657-FC4CE615406E"}
 */
function status()
{
	if(conteggio_figli > 0){
		if(status == "-"){
			return "-";
		}else{
			return "+";
		}
	}else{
		return "O";
	}
}

/**
 * @properties={type:12,typeid:36,uuid:"BF2CF547-DFEF-406E-B0F8-E84D68A50AB4"}
 */
function descrizione_tree()
{
	if(descrizione && livello - 1){
		var str = "";
		for(var i=0;i<livello - 1;i++){
			str += "          ";
		}
		str += descrizione;
		return str;
	}
	return descrizione;
}
