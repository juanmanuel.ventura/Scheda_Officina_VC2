/**
 *
 * @properties={type:6,typeid:36,uuid:"D28FBF69-1904-458F-9ACA-8016B01E8C03"}
 */
function affaticamento()
{
	var a = (totale) ? (valore*100) / totale : null;
	return a;
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"F4AD5283-45CC-4EC7-90A9-890004231E94"}
 */
function affaticamento_html()
{
	if(affaticamento)
	{
		var a = (affaticamento).toFixed(2) + "%";
		var style = (k8_distinte_vetture_coeff_to_k8_distinte_progetto_coeff.f_rilevante == "Rilevante") ? style = "font-weight:bold" : "";
		return "<html><body><p style='text-align:right;" + style + "'>" + a + "</p></body></html>";
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"DF9D99A1-CB82-4C24-A3A7-1E570F4D19C7"}
 */
function coefficiente_html()
{
	var style = "";
	if(k8_distinte_vetture_coeff_to_k8_distinte_progetto_coeff && k8_distinte_vetture_coeff_to_k8_distinte_progetto_coeff.f_rilevante == "Rilevante") style = " style='font-weight:bold'";
	return "<html><body><p" + style + ">" + coefficiente + "</p></body></html>";
}

/**
 *
 * @properties={type:6,typeid:36,uuid:"3EBA2305-4D29-484F-9ED9-E26082421B3E"}
 * @AllowToRunInFind
 */
function totale()
{
	var vetture = k8_distinte_vetture_coeff_to_k8_distinte_vetture.duplicateFoundSet();
	if(vetture && vetture.missione)
	{
		var missioni = null;
		var missioni_coeff = null;
		missioni = vetture.k8_distinte_vetture_to_k8_missioni.duplicateFoundSet();
		missioni.loadAllRecords();
		if(missioni.find())
		{
			missioni.missione = missione;
			missioni.search();
		}
		missioni_coeff = missioni.k8_missioni_to_k8_missioni_coefficienti.duplicateFoundSet();
		missioni_coeff.loadAllRecords();
		if(missioni_coeff.find())
		{
			missioni_coeff.coefficiente = coefficiente;
			missioni_coeff.search();
		}
		var scarto = missioni.k8_missioni_to_k8_missioni_coefficienti.scarto_riferimento;
		if(scarto && scarto < 0,9)
		{
			missioni.loadAllRecords();
			if(missioni.find())
			{
				missioni.f_riferimento = "Riferimento";
				missioni.search();
			}
			missioni_coeff = missioni.k8_missioni_to_k8_missioni_coefficienti.duplicateFoundSet();
			missioni_coeff.loadAllRecords();
			if(missioni_coeff.find())
			{
				missioni_coeff.coefficiente = coefficiente;
				missioni_coeff.search();
			}
		}
		return missioni_coeff.totale_missione;
	}
	else
	{
		return null;
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"80E85E2F-FF60-4062-B3F0-76B055A1C01F"}
 */
function valore_html()
{
	var v = (valore) ? valore.toFixed(5) : valore;
	var style = (k8_distinte_vetture_coeff_to_k8_distinte_progetto_coeff && k8_distinte_vetture_coeff_to_k8_distinte_progetto_coeff.f_rilevante == "Rilevante") ? style = "font-weight:bold" : "";
	return "<html><body><p style='text-align:right;" + style + "'>" + v + "</p></body></html>";
}
