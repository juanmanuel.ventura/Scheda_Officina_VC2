/**
 *
 * @properties={type:-4,typeid:36,uuid:"B69C5F65-21A8-4708-9783-254A2294295C"}
 */
function file() {
	//FS
	/** @type {String} */
	var filePath = path;
	//dep	if(path){
	//		var local = plugins.file.readFile(path);
	//		return local;
	//	}
	if (filePath) {
		var local = plugins.file.readFile(filePath);
		return local;
	}
	return "media:///cancel.png";
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"346B847E-892E-429D-8154-F189F3A110C0"}
 */
function file_size() {
	if (file != "media:///cancel.png") {
		var units = [" B", " KB", " MB", " GB", " TB"];
		var size = plugins.file.getFileSize(path);
		var i = 0;
		while (size > 1024) {
			size /= 1024;
			i++;
		}
		return size.toFixed(2) + units[i];
	}
	return null;
}

/**
 *
 * @properties={type:93,typeid:36,uuid:"77F8B065-8A2D-49BB-9F9E-0BA5D2226437"}
 */
function last_mod() {
	if (file != "media:///cancel.png") {
		var date = plugins.file.getModificationDate(path);
		return date;
	}
	return null;
}

/**
 *
 * @properties={type:-4,typeid:36,uuid:"6652E483-238B-4A7D-ABBD-FA4AF696A45A"}
 */
function last_mod_icon() {
	if (last_mod) {
		var ref = (timestamp_modifica) ? timestamp_modifica : timestamp_creazione;
		return (last_mod - ref > 60000) ? "media:///Rosso.png" : "media:///Verde.png";
	}
	return null;
}

/**
 *
 * @properties={type:-4,typeid:36,uuid:"C060A681-1F27-48DB-AC43-9129297E530D"}
 */
function path() {
	if (file_name) {
		var nas_path = forms["documenti_nas"].nas_path;
		var file_path = nas_path + progetto + "/" + table_fk + "/" + k8_documenti_nas_id.toString() + "_" + file_name;
		var f = new java.io.File(file_path);
		return (f.exists() && f.isFile() && f.canRead()) ? file_path : null;
	}
	return null;
}
