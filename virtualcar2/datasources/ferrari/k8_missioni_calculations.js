/**
 *
 * @properties={type:6,typeid:36,uuid:"34C06AFF-1573-45C1-8932-4B7507CD5A40"}
 */
function totale_giorni()
{
	var fs = k8_missioni_to_k8_missioni_km;
	var max = fs.getSize();
	var sum = 0;
	for(var i=1;i<=max;i++)
	{
		fs.setSelectedIndex(i);
		if(fs.giorni)
		{
			sum += fs.giorni;
		}
	}
	return sum;
}

/**
 *
 * @properties={type:6,typeid:36,uuid:"763CE2EF-64B6-45EA-B886-7DF730E4FE40"}
 */
function totale_km()
{
	var fs = k8_missioni_to_k8_missioni_km;
	var max = fs.getSize();
	var sum = 0;
	for(var i=1;i<=max;i++)
	{
		fs.setSelectedIndex(i);
		if(fs.km)
		{
			sum += fs.km;
		}
	}
	return sum;
}
