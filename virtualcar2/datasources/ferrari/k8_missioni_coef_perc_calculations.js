/**
 *
 * @properties={type:6,typeid:36,uuid:"891A76CA-F9BD-4D37-9C4C-3BCD17F80E39"}
 */
function valore()
{
	if(k8_missioni_coef_perc_to_k8_missioni_km.km && k8_missioni_coef_perc_to_k8_jt_coefficienti_percorsi.valore)
	{
		var num = k8_missioni_coef_perc_to_k8_missioni_km.km * k8_missioni_coef_perc_to_k8_jt_coefficienti_percorsi.valore ;
		var snum = num.toFixed(2);  
		return snum;
	}
	return 0;
}
