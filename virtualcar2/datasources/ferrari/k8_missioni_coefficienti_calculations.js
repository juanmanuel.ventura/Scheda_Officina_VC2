/**
 *
 * @properties={type:12,typeid:36,uuid:"DD26D0E1-7D07-4D15-8641-C24B9263285A"}
 */
function scarto_riferimento_html()
{
	var sc = "";
	var color = "";
	if(scarto_riferimento)
	{
		sc = (scarto_riferimento * 100).toFixed(2) + "%";
		if(scarto_riferimento < 0.5){
			color = "";
		}else if(scarto_riferimento < 0.85){
			color = " style='background-color:#FFFF00'";
		}else if(scarto_riferimento < 1.3){
			color = " style='background-color:#00FF00'";
		}else{
			color = " style='background-color:#FF0000'";
		}
	}
	return "<html><body><p" + color + ">" + sc + "</p></body></html>";
}
