/**
 *
 * @properties={type:12,typeid:36,uuid:"0804E4C5-C1DE-4A85-884E-946A7D5B1B66"}
 */
function km_totali()
{
	var sc = k8_montaggi_to_k8_schede_collaudo;
	var sum = 0;
	for(var i=1;i<=sc.getSize();i++)
	{
		sum += (sc.km_percorsi) ? sc.km_percorsi : 0;
	}
	return sum;
}
