/**
 *
 * @properties={type:12,typeid:36,uuid:"B092DCBA-E487-44CD-97A6-64938CB40A65"}
 */
function rowBgColorCalc(index,selected)
{
	if(selected){
		var defaultHighlight = java.awt.SystemColor.textHighlight;
		var r = java.lang.Integer.toHexString(defaultHighlight.getRed());
		var g = java.lang.Integer.toHexString(defaultHighlight.getGreen());
		var b = java.lang.Integer.toHexString(defaultHighlight.getBlue());
		return "#" + r + b + g;
	}else{
		return (scheda_creata == 1) ? "#FF9800" : null;
	}
}
