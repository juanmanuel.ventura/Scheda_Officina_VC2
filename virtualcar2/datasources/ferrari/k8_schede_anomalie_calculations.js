/**
 *
 * @properties={type:93,typeid:36,uuid:"AC916835-67F0-492A-A7C8-51E2BF38A01E"}
 */
function data_introduzione_calc(){
	if(data_introduzione){
		return data_introduzione;
	}
	if(k8_schede_anomalie_to_k8_schede_modifica && k8_schede_anomalie_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica){
		return k8_schede_anomalie_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica.data_introduzione_modifica_1;
	}
	return null;
}

/**
 *
 * @properties={type:6,typeid:36,uuid:"4F7E6C63-21EA-4C9D-AD5C-34BCE7AF45CF"}
 */
function numero_assembly_calc(){
	if(numero_assembly){
		return numero_assembly;
	}
	if(k8_schede_anomalie_to_k8_schede_modifica && k8_schede_anomalie_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica){
		return k8_schede_anomalie_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica.numero_assembly_1;
	}
	return null;
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"CC6E2E0E-F535-4B64-9FD2-B04BC0194527"}
 */
function lettere_modifica(){
	if(!k8_schede_anomalie_to_k8_schede_modifica || !k8_schede_anomalie_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica) return "0/0";
	var _i = k8_schede_anomalie_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica.getSelectedIndex();
	var _t = k8_schede_anomalie_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica.getSize();
	return _i + "/" + _t;
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"CF6F8F4C-DD22-4E7A-91C2-411D44DC6003"}
 */
function stato_segnalazione(){
	if(!ente_segnalazione_anomalia && !numero_segnalazione){
		return "ASSENTE";
	}
}

/**
 *
 * @properties={type:93,typeid:36,uuid:"D24F53DD-F1B3-42D7-A415-DC6BEE35F802"}
 */
function data_chiusura(){
	if(!data_chiusura && stato_anomalia == "CHIUSA" && modification_date_ska){
		return modification_date_ska;
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"9B44E5CF-45E2-4490-BDC9-A1D836660BB8"}
 */
function utente_chiusura_scheda(){
	if(!utente_chiusura_scheda && stato_anomalia == "CHIUSA" && modification_user_ska){
		return modification_user_ska;
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"021FC80E-C114-491C-B1E8-AE69FB609956"}
 */
function flag_nuova(){
	if(data_assegnazione_leader){
		var delta = (new Date() - data_assegnazione_leader > 604800000);
		return (delta) ? "0" : "1";
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"674A6258-F003-42BC-9A39-976C79A63AB2"}
 */
function k8_analisi(){
	return globals.vc2_createExportString(k8_schede_anomalie_to_k8_anomalie_anal_full);
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"BAD75E0D-A1A1-492C-AA21-C76384B7A755"}
 */
function k8_descrizione(){
	return globals.vc2_createExportString(k8_schede_anomalie_to_k8_anomalie_desc_full);
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"4262B240-9580-446E-B15A-B307077A5A43"}
 */
function k8_provvedimento(){
	return globals.vc2_createExportString(k8_schede_anomalie_to_k8_anomalie_provv_full);
}

/**
 *
 * @properties={type:4,typeid:36,uuid:"F53FAB46-481C-4B94-9F4E-B24E40D1497B"}
 */
function doc_count(){
	return k8_schede_anomalie_to_k8_documenti.getSize();
}

/**
 *
 * @properties={type:6,typeid:36,uuid:"48F63CB1-3A8F-4EC4-8F46-BC44839637C2"}
 */
function frequenza(){
	return k8_schede_anomalie_to_k8_anomalie_vetture.getSize();
}

/**
 *
 * @properties={type:-4,typeid:36,uuid:"323BBD62-6443-4E99-99F0-A19199D2F0CA"}
 */
function icon_criticita(){
	return globals.vc2_icon_criticita(indice_criticita);
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"9A811261-7410-4E83-AE50-D4976C6711F5"}
 */
function icona_tipo_anomalia(){
	if (!tipo_anomalia){
		return null;
	}
	if (tipo_anomalia == "Processo Fornitore"){
		return "media:///processo_fornitore.png"
	}else if(tipo_anomalia == "Processo Interno"){
		return "media:///processo_interno.png"
	}else if(tipo_anomalia == "Progetto"){
		return "media:///progetto.png"
	}else if(tipo_anomalia == "In Diagnosi"){
		return "media:///in_diagnosi.png"
	}else{
		return null;
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"B8F2FCE7-84DE-4CA3-8DB9-4DFAA99FC7B4"}
 */
function numero_disegno_str(){
	if (codice_disegno){
		return codice_disegno.toString();
	}	
}
