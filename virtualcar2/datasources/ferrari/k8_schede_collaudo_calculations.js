/**
 *
 * @properties={type:93,typeid:36,uuid:"C901ED27-ABE4-4BC7-B788-12A16DBA3EF4"}
 */
function durata_prova()
{
	if(ora_fine && ora_inizio){
		return ora_fine - ora_inizio - 3600000;
	}else{
		return null;
	}
}

/**
 *
 * @properties={type:6,typeid:36,uuid:"F8280B90-BAFC-44F7-9BA4-3BA756CA2A1E"}
 */
function km_percorsi()
{
	if(odometro_fine > odometro_inizio){
		var delta = odometro_fine - odometro_inizio;
		if(!udm || udm == "km"){
			return delta;
		}else{
			return delta * 1.609;
		}
	}else{
		return null;
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"8D2C7CCA-D3DB-4363-B616-1A103B95E268"}
 */
function tableName()
{
	return "k8_schede_collaudo";
}

/**
 *
 * @properties={type:6,typeid:36,uuid:"5EF220BC-FC84-4092-AEC5-C03D1D896DA0"}
 */
function turno_per_sincronia()
{
	return 1;
}
