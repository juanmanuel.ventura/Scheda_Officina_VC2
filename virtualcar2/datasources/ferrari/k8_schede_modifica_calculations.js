/**
 *
 * @properties={type:4,typeid:36,uuid:"0426E6EF-38E6-4E14-8DDF-6BF6DC4B47A3"}
 */
function numero_disegno_int(){
	return codice_disegno;
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"C9B12A30-0A53-417F-BA32-D75942FD0296"}
 */
function lettere_modifica(){
	if(!k8_schede_modifica_to_k8_lettere_modifica) return "0/0";
	var _i = k8_schede_modifica_to_k8_lettere_modifica.getSelectedIndex();
	var _t = k8_schede_modifica_to_k8_lettere_modifica.getSize();
	return _i + "/" + _t;
}

/**
 *
 * @properties={type:4,typeid:36,uuid:"DF1B9DF8-474F-43FE-87EA-F8790D5140D2"}
 */
function annullata(){
	return (stato_scheda == "CHIUSA" && data_annul_sk_prop_mod) ? 1 : 0;
}

/**
 *
 * @properties={type:4,typeid:36,uuid:"190D04D7-E9F8-465C-9291-01EF8D56A121"}
 */
function complessivo(){
	if(complessivi_interessati == 1){
		return k8_schede_modifica_to_k8_schede_modifica_agg_soppr.k8_schede_modifica_agg_soppr_to_k8_distinte_progetto_core.codice_cpl;
	}
}

/**
 *
 * @properties={type:4,typeid:36,uuid:"7265494F-7822-4F44-A926-01FEFA1B039B"}
 */
function complessivi_interessati(){
	if(k8_schede_modifica_to_k8_schede_modifica_agg_soppr && k8_schede_modifica_to_k8_schede_modifica_agg_soppr.getSize()){
		var rel = k8_schede_modifica_to_k8_schede_modifica_agg_soppr.duplicateFoundSet();
		var distinct = new Array();
		for(var i=1;i<=rel.getSize();i++){
			var rec = rel.getRecord(i);
			var cpl = rec.k8_schede_modifica_agg_soppr_to_k8_distinte_progetto_core.codice_cpl;
			if(cpl && distinct.indexOf(cpl) == -1){
				distinct.push(cpl);
			}
		}
		return distinct.length;
	}else{
		return 0;
	}
}
