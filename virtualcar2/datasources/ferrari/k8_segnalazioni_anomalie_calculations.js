/**
 *
 * @properties={type:12,typeid:36,uuid:"3BB0D57A-DD61-47DE-BBC5-31B8813CA649"}
 */
function flag_nuova(){
	if(data_assegnazione_leader){
		var delta = (new Date() - data_assegnazione_leader > 604800000);
		return (delta) ? "0" : "1";
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"A9463E3C-D192-46B2-8749-66A6DA834E88"}
 */
function k8_analisi(){
	return globals.vc2_createExportString(k8_segnalazioni_anomalie_to_k8_segn_anal_full);
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"E9B40C0D-DB22-4B9E-B802-59DC99DA6748"}
 */
function k8_descrizione(){
	return globals.vc2_createExportString(k8_segnalazioni_anomalie_to_k8_segn_desc_full);
}

/**
 *
 * @properties={type:4,typeid:36,uuid:"18CB743A-A213-497E-B2E8-38F336C60619"}
 */
function doc_count(){
	if (k8_segnalazioni_anomalie_to_k8_documenti)
		return k8_segnalazioni_anomalie_to_k8_documenti.getSize();
	else
		return 0;
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"084E0601-47F2-4CD3-A7F6-75AC0BDE2BA3"}
 */
function frequenza(){
	if (k8_segnalazioni_anomalie_to_k8_anomalie_vetture)
		return k8_segnalazioni_anomalie_to_k8_anomalie_vetture.getSize();
	else
		return 0;
}

/**
 *
 * @properties={type:-4,typeid:36,uuid:"05D829E7-1DC9-4CBB-9BAC-82FA63CCF6A9"}
 */
function icon_criticita(){
	return globals.vc2_icon_criticita(indice_criticita);
}

/**
 *
 * @properties={typeid:36,uuid:"CFB33710-847D-4F82-B264-D510706814E7"}
 */
function icona_tipo_anomalia(){
	if (!tipo_anomalia){
		return null;
	}
	if (tipo_anomalia == "Processo Fornitore"){
		return "media:///processo_fornitore.png"
	}else if(tipo_anomalia == "Processo Interno"){
		return "media:///processo_interno.png"
	}else if(tipo_anomalia == "Progetto"){
		return "media:///progetto.png"
	}else if(tipo_anomalia == "In Diagnosi"){
		return "media:///in_diagnosi.png"
	}else{
		return null;
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"0C36D5EA-02F1-43F2-8015-D6CBFA7F978C"}
 */
function numero_disegno_str(){
	if (codice_disegno){
		return codice_disegno.toString();
	}	
}
