/**
 *
 * @properties={type:93,typeid:36,uuid:"6381A08D-45A4-4787-896A-77FEF9A9E418"}
 */
function data_introduzione_calc(){
	if(data_introduzione){
		return data_introduzione;
	}
	if(k8_segnalazioni_full_to_k8_schede_modifica && k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica){
		return k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica.data_introduzione_modifica_1;
	}
	return null;
}

/**
 *
 * @properties={type:6,typeid:36,uuid:"A8EFA1D1-31FB-47F2-A360-F4EFF4E86B27"}
 */
function numero_assembly_calc(){
	if(numero_assembly){
		return numero_assembly;
	}
	if(k8_segnalazioni_full_to_k8_schede_modifica && k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica){
		return k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica.numero_assembly_1;
	}
	return null;
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"B8CB072A-7533-47A5-A989-85BB37E25758"}
 */
function lettere_modifica(){
	if(!k8_segnalazioni_full_to_k8_schede_modifica || !k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica) return "0/0";
	var _i = k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica.getSelectedIndex();
	var _t = k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica.getSize();
	return _i + "/" + _t;
}

/**
 *
 * @properties={type:6,typeid:36,uuid:"5EAC6DEB-053E-4A81-A96C-2BD44EFFF942"}
 */
function k8_documents_key(){
	return k8_anomalie_id;
}

/**
 *
 * @properties={type:93,typeid:36,uuid:"A54871BF-3394-4C25-A947-BC558822ACB5"}
 */
function data_chiusura_segnalazione(){
	if(!data_chiusura_segnalazione && stato_segnalazione == "CHIUSA" && modification_date_sa){
		return modification_date_sa;
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"4A1FFC57-2D54-44E8-B371-EF1D904694EB"}
 */
function utente_chiusura_segnalazione(){
	if(!utente_chiusura_segnalazione && stato_segnalazione == "CHIUSA" && modification_user_sa){
		return modification_user_sa;
	}
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"0E106715-E937-42CC-A263-C4B36DE005CE"}
 */
function stato_anomalia_fake(){
	if(ente && numero_scheda){
		return stato_anomalia;
	}
}

/**
 * @properties={type:12,typeid:36,uuid:"13AE0101-1826-44C1-B003-10EE3992AAC5"}
 */
function flag_nuova(){
	if(data_assegnazione_leader){
		var delta = (new Date() - data_assegnazione_leader > 604800000);
		return (delta) ? "0" : "1";
	}
}

/**
 * @properties={type:12,typeid:36,uuid:"E7717307-108F-4104-B43E-2F7E3F3EBF56"}
 */
function k8_analisi(){
	return globals.vc2_createExportString(k8_segnalazioni_full_to_k8_segn_anal_full);
}

/**
 * @properties={type:12,typeid:36,uuid:"D06F3611-A46A-4C0F-8D65-ABD365709D77"}
 */
function k8_descrizione(){
	return globals.vc2_createExportString(k8_segnalazioni_full_to_k8_segn_desc_full);
}

/**
 * @properties={type:4,typeid:36,uuid:"A791F683-D00D-467E-89DC-6FBDAC8A174A"}
 */
function doc_count(){
	if (k8_segnalazioni_full_to_k8_documenti_nas)
		return k8_segnalazioni_full_to_k8_documenti_nas.getSize();
	else
		return 0;
}

/**
 * @properties={type:6,typeid:36,uuid:"048A62E0-44C9-461C-AA18-27A8F3788D2B"}
 */
function frequenza(){
	if (k8_segnalazioni_full_to_k8_anomalie_vetture)
		return k8_segnalazioni_full_to_k8_anomalie_vetture.getSize();
	else
		return 0;
}

/**
 * @properties={type:-4,typeid:36,uuid:"78D7B59A-D64B-4688-BFCA-A73B61CE3B0A"}
 */
function icon_criticita(){
	return globals.vc2_icon_criticita(indice_criticita);
}

/**
 * @properties={type:12,typeid:36,uuid:"05326E0A-2569-4ED3-B7C9-26B58B65E8B2"}
 */
function icona_tipo_anomalia(){
	if (!tipo_anomalia){
		return null;
	}
	if (tipo_anomalia == "Processo Fornitore"){
		return "media:///processo_fornitore.png"
	}else if(tipo_anomalia == "Processo Interno"){
		return "media:///processo_interno.png"
	}else if(tipo_anomalia == "Progetto"){
		return "media:///progetto.png"
	}else if(tipo_anomalia == "In Diagnosi"){
		return "media:///in_diagnosi.png"
	}else{
		return null;
	}
}

/**
 * @properties={type:12,typeid:36,uuid:"5F7D2E25-746C-498F-B0DF-6CB8CF1E55A4"}
 */
function numero_disegno_str(){
	if (codice_disegno){
		return codice_disegno.toString();
	}	
}
