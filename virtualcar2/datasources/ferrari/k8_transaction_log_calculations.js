/**
 *
 * @properties={type:-4,typeid:36,uuid:"10287783-D36E-4438-AABC-BF9B178137A7"}
 */
function result_icon(){
	return (result) ? "media:///red_dot.png" : "media:///green_dot.png";
}

/**
 *
 * @properties={type:12,typeid:36,uuid:"E1CBE1D0-ADC4-4984-958A-AA45BE96A61B"}
 */
function message4human(){
	return vc2_message.replace(/[{}]/g,"").replace(/:/g,"\t:\t").replace(/,/g,",\n");
}
