/**
 * @properties={typeid:35,uuid:"848FD83B-71AB-4022-B5B1-E55F9EFB1477",variableType:-4}
 */
var _field = null;

/**
 * @properties={typeid:35,uuid:"8DDCF681-5D37-449E-8679-850B10BB0EBB",variableType:-4}
 */
var _form = null;

/**
 * @properties={typeid:24,uuid:"83937C68-204E-4662-BCDA-9BB74F5D6A43"}
 */
function openPopup(_fo,_fi,_p,_v){
	globals.vc2_currentProgetto = _p;
	globals.vc2_currentVersione = _v;
	
	_form = _fo;
	_field = _fi;
	
	var mode = forms.nfx_interfaccia_pannello_buttons.mode;
	var _e = (mode == "add" || mode == "edit") ? true : false;
	
	forms._vc2_bomViewer_tableBOM.elements.select.enabled = _e;
	forms._vc2_bomViewer_tableHISTORY.elements.select.enabled = _e;
	forms._vc2_bomViewer_tableFUTURE.elements.select.enabled = _e;
	
	forms._vc2_bomViewer_contentList.reset();
	forms._vc2_bomViewer_contentCheck.reset();

	controller.show("bomViewer");
	
}

/**
 * Perform the element default action.
 * 
 * @properties={typeid:24,uuid:"BE7245F3-7F61-47B0-B6EA-5B9D3701F3FB"}
 */
function select(_data) {
	
	forms[_form].foundset[_field] = _data;
	
	//application.closeFormDialog("bomViewer");
	
	var w=controller.getWindow();
	w.destroy();

}
