/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"92322E04-F78E-45A3-B52A-C02DF5A65D23",variableType:93}
 */
var _check = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"2515A1B4-6D10-4B99-BDA6-9421BDC87CE3"}
 */
var _desc = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"2EE2673C-83D3-4569-9B36-524B34BA65B4"}
 */
var _draw = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"519BD16C-465F-4D3E-9623-90F05892E469"}
 */
var _exp = "";

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"3DBEB216-D436-4F68-816A-9A9A331C96BC"}
 */
function onShowForm(firstShow, event) {
	forms._vc2_bomViewer_tableBOM.elements.tree.enabled = false;
	filterData();
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"53BC45FE-D6B2-4158-8306-EB16B7A124B3"}
 */
function onDataChange(oldValue, newValue, event) {
	filterData();
	
	return true
}

/**
 * @properties={typeid:24,uuid:"2A40EFA8-506B-4768-8E65-5C17CBE72948"}
 */
function reset(){
	_draw = "";
	_exp = "";
	_desc = "";
	_check = new Date();
}

/**
 * @properties={typeid:24,uuid:"B91EEBC7-4233-445F-9D58-4FAA7943317A"}
 * @AllowToRunInFind
 */
function filterData(){
	var _fs = null;
	_fs = forms._vc2_bomViewer_tableBOM.foundset.duplicateFoundSet();
	_fs.loadRecords("SELECT K8_DISTINTE_PROGETTO_ID FROM K8_DISTINTE_PROGETTO WHERE CODICE_VETTURA = " + globals.vc2_currentVersione + " AND TO_CHAR(TIMESTAMP_CREAZIONE,'YYYYMMDD') <= '" + utils.dateFormat(_check,"yyyyMMdd") + "'");
	if(_fs.find()){
		_fs.numero_disegno = _draw;
		_fs.esponente = _exp;
		_fs.descrizione = "#%" + _desc + "%";
		_fs.search(false,true);
	}
	_fs.sort("path_numero_disegno asc");
	forms._vc2_bomViewer_tableBOM.controller.loadRecords(_fs);
	
	_fs = forms._vc2_bomViewer_tableHISTORY.foundset.duplicateFoundSet();
	_fs.loadRecords("SELECT K8_DISTINTE_PROGETTO_ID FROM K8_DISTINTE_PROGETTO_STORICO WHERE CODICE_VETTURA = " + globals.vc2_currentVersione + " AND  TO_CHAR(TIMESTAMP_CREAZIONE,'YYYYMMDD') <= '" + utils.dateFormat(_check,"yyyyMMdd") + "' AND TO_CHAR(HISTORY_CREAZIONE,'YYYYMMDD') > '" + utils.dateFormat(_check,"yyyyMMdd") + "'");
	if(_fs.find()){
		_fs.numero_disegno = _draw;
		_fs.esponente = _exp;
		_fs.descrizione = "#%" + _desc + "%";
		_fs.search(false,true);
	}
	_fs.sort("path_numero_disegno asc");
	forms._vc2_bomViewer_tableHISTORY.controller.loadRecords(_fs);
	
	_fs = forms._vc2_bomViewer_tableFUTURE.foundset.duplicateFoundSet();
	_fs.loadRecords("SELECT K8_DISTINTE_PROGETTO_ID FROM K8_DISTINTE_PROGETTO WHERE CODICE_VETTURA = " + globals.vc2_currentVersione + " AND  TO_CHAR(TIMESTAMP_CREAZIONE,'YYYYMMDD') > '" + utils.dateFormat(_check,"yyyyMMdd") + "'");
	if(_fs.find()){
		_fs.numero_disegno = _draw;
		_fs.esponente = _exp;
		_fs.descrizione = "#%" + _desc + "%";
		_fs.search(false,true);
	}
	_fs.sort("path_numero_disegno asc");
	forms._vc2_bomViewer_tableFUTURE.controller.loadRecords(_fs);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"D22FADBA-0F40-4DB3-A839-0F646070422F"}
 */
function excelExport(event) {
	var _d = plugins.file.showDirectorySelectDialog();
	if(!_d)return;
	
	var _q = null;
	var _a = null;
	var _ds = null;
	
	_q = databaseManager.getSQL(forms._vc2_bomViewer_tableBOM.foundset).replace("K8_DISTINTE_PROGETTO_ID","to_char(NUMERO_DISEGNO,'999999999999') DISEGNO,to_char(ESPONENTE,'999999999999') \"ESP.\",DESCRIZIONE");
	_a = databaseManager.getSQLParameters(forms._vc2_bomViewer_tableBOM.foundset);
	
	_ds = databaseManager.getDataSetByQuery("ferrari",_q,_a,-1);
	if(_ds.getMaxRowIndex() > 0) plugins.file.writeTXTFile(_d + "\\distinta_" + globals.vc2_currentVersione + "_" + utils.dateFormat(_check,"yyyyMMdd") + ".xls",_ds.getAsText("\t","\n","",true));
	
	_q = databaseManager.getSQL(forms._vc2_bomViewer_tableHISTORY.foundset).replace("K8_DISTINTE_PROGETTO_ID","to_char(NUMERO_DISEGNO,'999999999999') DISEGNO,to_char(ESPONENTE,'999999999999') \"ESP.\",DESCRIZIONE,to_char(HISTORY_CREAZIONE,'DD/MM/YYYY') DATA");
	_a = databaseManager.getSQLParameters(forms._vc2_bomViewer_tableHISTORY.foundset);
	
	_ds = databaseManager.getDataSetByQuery("ferrari",_q,_a,-1);
	if(_ds.getMaxRowIndex() > 0) plugins.file.writeTXTFile(_d + "\\distinta_rimozioni_" + globals.vc2_currentVersione + "_" + utils.dateFormat(_check,"yyyyMMdd") + ".xls",_ds.getAsText("\t","\n","",true));
	
	_q = databaseManager.getSQL(forms._vc2_bomViewer_tableFUTURE.foundset).replace("K8_DISTINTE_PROGETTO_ID","to_char(NUMERO_DISEGNO,'999999999999') DISEGNO,to_char(ESPONENTE,'999999999999') \"ESP.\",DESCRIZIONE,to_char(TIMESTAMP_CREAZIONE,'DD/MM/YYYY') DATA");
	_a = databaseManager.getSQLParameters(forms._vc2_bomViewer_tableFUTURE.foundset);
	
	_ds = databaseManager.getDataSetByQuery("ferrari",_q,_a,-1);
	if(_ds.getMaxRowIndex() > 0) plugins.file.writeTXTFile(_d + "\\distinta_aggiunte_" + globals.vc2_currentVersione + "_" + utils.dateFormat(_check,"yyyyMMdd") + ".xls",_ds.getAsText("\t","\n","",true));
}
