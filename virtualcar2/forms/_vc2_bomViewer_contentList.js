/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"4D60D759-6E1E-4863-BB1A-963722CF3A0A"}
 */
var _desc = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A96F3893-4C57-4D8C-8D24-61D3F6481DC9"}
 */
var _draw = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"5F369E56-7A26-4F5C-915D-F6368E354964"}
 */
var _exp = "";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"65EBDCD9-C48C-4ECC-AB33-5B7EE0E733B2",variableType:4}
 */
var _tree = 0;

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"F400F39D-4BD0-4089-92F2-9C69539C69DE"}
 */
function onShowForm(firstShow, event) {
	forms._vc2_bomViewer_tableBOM.elements.tree.enabled = true;
	filterData();
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"60011B43-FE27-4BFF-9EF4-905FA9A79C26"}
 */
function onDataChange(oldValue, newValue, event) {
	filterData();
	
	return true
}

/**
 * @properties={typeid:24,uuid:"A02FE7D6-18DA-4762-B376-3202E25C55A0"}
 */
function reset(){
	_draw = "";
	_exp = "";
	_desc = "";
}

/**
 * @properties={typeid:24,uuid:"D61E850A-CC74-4243-B7EF-3932EF9D02BE"}
 * @AllowToRunInFind
 */
function filterData(){
	var _fs = null;
	_fs = forms._vc2_bomViewer_tableBOM.foundset.duplicateFoundSet();
	if(_fs.find()){
		_fs.codice_vettura = globals.vc2_currentVersione;
		_fs.numero_disegno = _draw;
		_fs.esponente = _exp;
		_fs.descrizione = "#%" + _desc + "%";
		_fs.search();
	}
	_fs.sort("path_numero_disegno asc");
	forms._vc2_bomViewer_tableBOM.controller.loadRecords(_fs);
	
	_fs = forms._vc2_bomViewer_tableHISTORY.foundset.duplicateFoundSet();
	if(_fs.find()){
		_fs.codice_vettura = globals.vc2_currentVersione;
		_fs.numero_disegno = _draw;
		_fs.esponente = _exp;
		_fs.descrizione = "#%" + _desc + "%";
		_fs.search();
	}
	_fs.sort("path_numero_disegno asc");
	forms._vc2_bomViewer_tableHISTORY.controller.loadRecords(_fs);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"106E3AF8-277F-44E1-B716-E401818AE261"}
 * @AllowToRunInFind
 */
function resetTree(event) {
	reset();
	filterData();
	var _fs = forms._vc2_bomViewer_tableBOM.foundset.duplicateFoundSet();
	if(_fs.find()){
		_fs.codice_vettura = globals.vc2_currentVersione;
		_fs.livello = 0;
		_fs.search();
	}
	forms._vc2_bomViewer_tableBOM.controller.loadRecords(_fs);
}
