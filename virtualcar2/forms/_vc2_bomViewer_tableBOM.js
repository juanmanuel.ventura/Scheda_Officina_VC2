/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"D6D725C8-7EC3-457C-B0A0-2FD5FE43BF53"}
 */
function onLoad(event) {
	controller.readOnly = true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"FD233D41-DCBE-43F7-930F-4A5DDCAD5A1A"}
 * @AllowToRunInFind
 */
function openBranch(event) {
	var _p = foundset.path_id;
	var _l = foundset.livello;
	var _s = databaseManager.getFoundSetCount(foundset);
	var _i = foundset.getSelectedIndex();
	var _fs = foundset.duplicateFoundSet();
	if(_fs.find()){
		_fs.codice_vettura = globals.vc2_currentVersione;
		_fs.path_id = _p + "/%";
		_fs.livello = _l + 1;
		_fs.search(false,false);
	}
	if(databaseManager.getFoundSetCount(_fs) == _s && _fs.find()){ //allora l'albero va chiuso
		_fs.codice_vettura = globals.vc2_currentVersione;
		_fs.path_id = "!=" + _p + "/%";
		_fs.search(false,true);
	}
	_fs.sort("path_numero_disegno asc");
	_fs.setSelectedIndex(_i);
	controller.loadRecords(_fs);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"BBF9647B-D982-439E-B12A-37CC054D57C7"}
 */
function select(event) {
	forms._vc2_bomViewer_container.select(utils.numberFormat(numero_disegno,"#############"));
}
