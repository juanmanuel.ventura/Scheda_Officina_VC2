/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"8D92226A-71E4-40CC-BADD-63303A500C6E"}
 */
function onLoad(event) {
	controller.readOnly = true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"2D357B7F-D7F6-46DB-A009-CFF8A5C63038"}
 */
function select(event) {
	forms._vc2_bomViewer_container.select(utils.numberFormat(numero_disegno,"#############"));
}
