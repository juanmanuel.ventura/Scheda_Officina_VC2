/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"B9D2E351-449E-4111-B33F-5341A2F2B571"}
 */
function onLoad(event) {
	controller.readOnly = true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"6F875FBF-61C6-4661-ACC2-310DC0E3D480"}
 */
function select(event) {
	forms._vc2_bomViewer_container.select(utils.numberFormat(numero_disegno,"#############"));
}
