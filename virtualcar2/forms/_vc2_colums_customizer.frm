items:[
{
height:300,
partType:5,
typeid:19,
uuid:"174B1614-F4BD-493B-8024-C9FBD8067BE1"
},
{
beanClassName:"javax.swing.JList",
location:"330,120",
name:"ch_list",
size:"80,80",
typeid:12,
uuid:"258AEB0A-D39E-4F0D-A5DD-5BFDD6F91653"
},
{
anchors:7,
beanClassName:"javax.swing.JScrollPane",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_02\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JScrollPane\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>260<\/int> \
    <int>230<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <object class=\"javax.swing.border.EtchedBorder\"> \
    <int>0<\/int> \
    <null/> \
    <null/> \
   <\/object> \
  <\/void> \
  <void property=\"focusable\"> \
   <boolean>false<\/boolean> \
  <\/void> \
  <void property=\"name\"> \
   <string>ch_scroll<\/string> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:-3,
location:"265,28",
name:"ch_scroll",
size:"264,234",
typeid:12,
uuid:"2B0A79A7-8A61-407B-81D3-6CB62F4C2B3A"
},
{
anchors:6,
imageMediaID:"85582DF7-017C-4741-98E4-7C3C1142DF33",
location:"460,267",
mediaOptions:10,
name:"_set",
onActionMethodID:"20172418-7A0E-4D26-A6B6-734C67165595",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"33C1153A-34F2-4789-8583-0EB280CF072F"
},
{
anchors:3,
imageMediaID:"127B464F-994F-4572-810C-24FBC185DB64",
location:"228,90",
mediaOptions:10,
name:"_del",
onActionMethodID:"C8A42DBA-2839-43C1-807A-9B7399B71DCF",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"53CF976A-6981-4DE3-AB71-DF81EABB11E5"
},
{
anchors:3,
imageMediaID:"E6C052AF-2185-445F-932A-5F615B6333EC",
location:"228,200",
mediaOptions:10,
name:"_clear",
onActionMethodID:"C8A42DBA-2839-43C1-807A-9B7399B71DCF",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"6CC09FA4-E8AD-472C-8405-6883BA3A8122"
},
{
location:"260,10",
mediaOptions:14,
size:"80,20",
tabSeq:-1,
text:"Scelte",
transparent:true,
typeid:7,
uuid:"71EBA078-FDE2-4333-9C09-5E0568F57375"
},
{
location:"10,10",
mediaOptions:14,
size:"80,20",
tabSeq:-1,
text:"Opzioni",
transparent:true,
typeid:7,
uuid:"7AE15474-B4E3-4B0B-9443-C51A2D7F7B78"
},
{
anchors:3,
imageMediaID:"5804BE64-BD66-4BC7-B44A-3314D54EA001",
location:"228,130",
mediaOptions:10,
name:"_up",
onActionMethodID:"C8A42DBA-2839-43C1-807A-9B7399B71DCF",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"7C889B0B-9B61-410B-A55C-75925B1EAD57"
},
{
anchors:15,
borderType:"EtchedBorder,0,null,null",
formIndex:-5,
lineSize:1,
location:"8,28",
shapeType:1,
size:"214,234",
transparent:true,
typeid:21,
uuid:"90DA6026-F9C3-43AA-A40C-358DABD75501"
},
{
anchors:14,
dataProviderID:"_filter",
location:"10,240",
name:"_filter",
onActionMethodID:"55492670-F1E4-4AC4-8F45-CE8C2DC95B50",
size:"190,20",
text:"Filter",
typeid:4,
uuid:"A2A27CEE-8A06-44EA-8B07-4C008500C671"
},
{
anchors:3,
imageMediaID:"40905D44-A323-4D4C-9CCA-CC832C1E545D",
location:"228,60",
mediaOptions:10,
name:"_add",
onActionMethodID:"C8A42DBA-2839-43C1-807A-9B7399B71DCF",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"A67DBCD4-88E7-4719-BEF8-F844179A01B4"
},
{
anchors:15,
beanClassName:"javax.swing.JScrollPane",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_02\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JScrollPane\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>210<\/int> \
    <int>230<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <null/> \
  <\/void> \
  <void property=\"name\"> \
   <string>op_scroll<\/string> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:-2,
location:"10,30",
name:"op_scroll",
size:"210,210",
typeid:12,
uuid:"AFC1709A-1C7E-4FB4-85B0-95A9A7329FFA"
},
{
anchors:3,
imageMediaID:"6AD2E909-E7A4-491A-92E5-D7E2F229B865",
location:"228,160",
mediaOptions:10,
name:"_down",
onActionMethodID:"C8A42DBA-2839-43C1-807A-9B7399B71DCF",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"C169034C-6E72-42D1-876F-3BA45D923BF4"
},
{
anchors:6,
imageMediaID:"618F9AA0-E8E0-4019-A9D8-472CC462832F",
location:"201,241",
mediaOptions:14,
onActionMethodID:"55492670-F1E4-4AC4-8F45-CE8C2DC95B50",
showClick:false,
showFocus:false,
size:"18,18",
transparent:true,
typeid:7,
uuid:"F1A5AEBE-75A4-4CE8-B1DF-74E7C15B892E"
},
{
anchors:6,
imageMediaID:"834DCE94-EDB6-434B-966C-90C204918A70",
location:"270,267",
mediaOptions:10,
name:"_profiles",
onActionMethodID:"18FB6A7D-9CB4-47FF-A414-86CF1D2C4CFE",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"F4F66D31-80A7-43B5-8996-F639D14E421F"
},
{
beanClassName:"javax.swing.JList",
location:"70,120",
name:"op_list",
size:"80,80",
typeid:12,
uuid:"FFC024E9-E0F1-4473-8552-445E9C3C61D2"
}
],
name:"_vc2_colums_customizer",
navigatorID:"-1",
onLoadMethodID:"33BD0D31-8A0E-4828-807A-F5642AFC3467",
paperPrintScale:100,
showInMenu:true,
size:"530,300",
styleName:"keeneight",
titleText:"Personalizza colonne",
typeid:3,
uuid:"0968C3F4-89A2-4B02-9E4A-CBAD545B0193"