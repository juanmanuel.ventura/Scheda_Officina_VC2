/** @type {Array} *
 * @properties={typeid:35,uuid:"3FA7E45F-C532-46A4-942B-895AAB7460DC",variableType:-4}
 */
var _choices = [];

/**
 * @properties={typeid:35,uuid:"99EA41E7-BB27-4EC1-B7F8-87D542683750",variableType:-4}
 */
var _field = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A09B0D62-B8F7-41C9-A986-6A394F52268C"}
 */
var _filter = "";

/**
 * @type {String}
 * @properties={typeid:35,uuid:"0D50A215-BA95-451E-980D-64731312CBE0"}
 */
var _form = null;

/**
 * @properties={typeid:35,uuid:"58CD47E5-ECC5-4237-8774-7C5B3CC4FE42",variableType:-4}
 */
var _options = [];

/**@type {Object}
 * @properties={typeid:35,uuid:"6573E264-81ED-495D-9A9E-3949DEC259A1",variableType:-4}
 */
var _profiles = { };

/**
 * @properties={typeid:24,uuid:"2AC1EAA1-0024-4A61-9256-2EC2ADC68FB8"}
 */
function opnePopup(_op, _ch, _fo, _fi) {
	_options = [];
	for (var _v in _op)
		_options.push(_v);
	elements.op_list.setListData(_options.sort());

	_choices = _ch.slice();
	elements.ch_list.setListData(_choices);

	_form = _fo;
	_field = _fi;

	_filter = "";

	_profiles = { };

	if (!forms.nfx_interfaccia_gestione_salvataggi.load(_form + "_customColumns" + "_default")) forms.nfx_interfaccia_gestione_salvataggi.save(_choices, _form + "_customColumns" + "_default");

	controller.show("colums");
}

/**
 * @param {JSEvent} event
 *
 * @properties={typeid:24,uuid:"C8A42DBA-2839-43C1-807A-9B7399B71DCF"}
 */
function work(event) {
	var _f = event.getElementName();
	switch (_f) {
	case "_add":
		if (elements.op_list.getSelectedValue()) {
			_choices.push(elements.op_list.getSelectedValue());
			elements.ch_list.setListData(_choices);
			elements.ch_list.setSelectedIndex(_choices.length - 1);
		}
		break;
	case "_del":
		if (elements.ch_list.getSelectedIndex()) {
			var _idx3 = elements.ch_list.getSelectedIndex();
			_choices.splice(_idx3, 1);
			elements.ch_list.setListData(_choices);
			if (_choices.length) elements.ch_list.setSelectedIndex(_idx3);
		}
		break;
	case "_up":
		//FS
		//application.output("indice " + elements.ch_list.getSelectedIndex());
		//dep if (elements.ch_list.getSelectedIndex() != 0) {
		if (elements.ch_list.getSelectedIndex() > 0) {
			var _idx1 = elements.ch_list.getSelectedIndex();
			var _app1 = _choices[_idx1 - 1];
			_choices[_idx1 - 1] = _choices[_idx1];
			_choices[_idx1] = _app1;
			elements.ch_list.setListData(_choices);
			elements.ch_list.setSelectedIndex(_idx1 - 1);
		}
		break;
	case "_down":
		//application.output("indice " + elements.ch_list.getSelectedIndex());
		if (elements.ch_list.getSelectedIndex() != _choices.length - 1 && elements.ch_list.getSelectedIndex() != -1) {
			var _idx2 = elements.ch_list.getSelectedIndex();
			var _app2 = _choices[_idx2 + 1];
			_choices[_idx2 + 1] = _choices[_idx2];
			_choices[_idx2] = _app2;
			elements.ch_list.setListData(_choices);
			elements.ch_list.setSelectedIndex(_idx2 + 1);
		}
		break;
	case "_clear":
		_choices = [];
		elements.ch_list.setListData(_choices);
		break;
	default:
		throw "ERROR: unknown button pressed";
	}
}

/**
 * @param {JSEvent} event
 *
 * @properties={typeid:24,uuid:"55492670-F1E4-4AC4-8F45-CE8C2DC95B50"}
 */
function filter(event) {
	var _list = globals.utils_filterList(_filter, _options);
	elements.op_list.setListData(_list.sort());
	elements.op_list.setSelectedIndex(0);
	if (_list.length) elements.op_list.setSelectedIndex(0);
}

/**
 * @param {JSEvent} event
 *
 * @properties={typeid:24,uuid:"20172418-7A0E-4D26-A6B6-734C67165595"}
 */
function set(event) {
	forms[_form][_field] = _choices.slice(0);

	//application.closeFormDialog("colums");

	var w = controller.getWindow();
	w.destroy();

}

/**
 * @param {JSEvent} event
 *
 * @properties={typeid:24,uuid:"18FB6A7D-9CB4-47FF-A414-86CF1D2C4CFE"}
 */
function profiles(event) {
	//FS
	//dep 	var _m = [];
	var menu = plugins.window.createPopupMenu();	
	_profiles = forms.nfx_interfaccia_gestione_salvataggi.load(_form + "_customColumns" + "_profiles");
	if (!_profiles || forms.nfx_json_serializer.stringify(_profiles) == forms.nfx_json_serializer.stringify({ })) {
		_profiles = { };
		//dep	_m.push(plugins.popupmenu.createMenuItem("<Nessun profilo>"));
		menu.addMenuItem("<Nessun profilo>");
	} else {
		//for(_p in _profiles){
		for (var _p in _profiles) {
			//var _l = plugins.popupmenu.createMenuItem("Carica",loadProfile);
			//var _l = menu.addMenuItem("Carica", loadProfile);
			var subMenu=menu.addMenu(_p);
			var _l=subMenu.addMenuItem("Carica", loadProfile);
			//dep _l.setMethodArguments(_p);
			_l.methodArguments = [_p];
			//dep	var _d = plugins.popupmenu.createMenuItem("Cancella", deleteProfile);
			var _d = subMenu.addMenuItem("Cancella", deleteProfile);
			//dep	_d.setMethodArguments(_p);
			_d.methodArguments = [_p];
			//dep	_m.push(plugins.popupmenu.createMenuItem(_p, [_l, _d]));
		}
	}
	//dep _m.push(plugins.popupmenu.createMenuItem("-"));
	menu.addSeparator();
	//dep 	var _md = [];
	var secondSubMenu = menu.addMenu("Default");
	secondSubMenu.addMenuItem("Carica", loadDefault);
	secondSubMenu.addMenuItem("Salva", saveDefault);
	//dep	_md.push(plugins.popupmenu.createMenuItem("Carica", loadDefault));
	//dep 	_md.push(plugins.popupmenu.createMenuItem("Salva", saveDefault));
	//dep _m.push(plugins.popupmenu.createMenuItem("Default", _md));
	menu.addMenuItem("Nuovo profilo...", newProfile);
	//dep 	_m.push(plugins.popupmenu.createMenuItem("Nuovo profilo...", newProfile));
	//dep 	plugins.popupmenu.showPopupMenu(elements._profiles, _m);
	menu.show(elements._profiles);
}

/**@param {Number} index   
 * @param {Number} parentIndex
 * @param {Boolean} isSelected
 * @param {String} parentText
 * @param {String} menuText
 * @param {String} _p
 * @properties={typeid:24,uuid:"E825E0CF-0954-471B-8EC9-2D7A45222DC1"}
 */
function loadProfile(index,parentIndex,isSelected,parentText,menuText,_p) {
	_choices = _profiles[_p].slice();
	elements.ch_list.setListData(_choices);
}

/**@param {Number} index   
* @param {Number} parentIndex
* @param {Boolean} isSelected
* @param {String} parentText
* @param {String} menuText
* @param {String} _p
 * @properties={typeid:24,uuid:"932618A0-0AA7-4C60-827F-1486A38EB79A"}
 */
function deleteProfile(index,parentIndex,isSelected,parentText,menuText,_p) {
	if (plugins.dialogs.showQuestionDialog("Eliminare?", "Eliminare il profilo \"" + _p + "\"?", "Sì", "No") == "Sì") {
		delete _profiles[_p];
		forms.nfx_interfaccia_gestione_salvataggi.save(_profiles, _form + "_customColumns" + "_profiles");
	}
}

/**
 * @properties={typeid:24,uuid:"6C098F7C-9649-4A53-8215-99BF4A3E0C8E"}
 */
function loadDefault() {
	_choices = forms.nfx_interfaccia_gestione_salvataggi.load(_form + "_customColumns" + "_default");
	elements.ch_list.setListData(_choices);
}

/**
 * @properties={typeid:24,uuid:"7BF1DACB-513F-447E-A0E6-B7CF5C0B2F0D"}
 */
function saveDefault() {
	if (plugins.dialogs.showQuestionDialog("Salvare?", "Salvare la selezione corrente come default?", "Sì", "No") == "Sì") {
		forms.nfx_interfaccia_gestione_salvataggi.save(_choices, _form + "_customColumns" + "_default");
	}
}

/**
 * @properties={typeid:24,uuid:"857747D3-8F51-48B4-ACE2-998341CA8E4F"}
 */
function newProfile() {
	var _n = plugins.dialogs.showInputDialog("Nuovo profilo", "Inserisci un nome per il nuovo profilo");
	if (_n) {
		if (_profiles[_n] && plugins.dialogs.showQuestionDialog("Sovrascrivere?", "Il profilo \"" + _n + "\" è già presente, sovrascrivere?", "Sì", "No") == "Sì") {
			_profiles[_n] = _choices;
			forms.nfx_interfaccia_gestione_salvataggi.save(_profiles, _form + "_customColumns" + "_profiles");
		} else {
			_profiles[_n] = _choices;
			forms.nfx_interfaccia_gestione_salvataggi.save(_profiles, _form + "_customColumns" + "_profiles");
		}
	}
}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"33BD0D31-8A0E-4828-807A-F5642AFC3467"}
 */
function onLoad(event) {
	//	JStaffa

	//		elements.op_scroll.getViewport().add(elements.op_list);
	//		elements.ch_scroll.getViewport().add(elements.ch_list);
	//
	elements.op_scroll.getViewport().add(java.awt.Component(elements.op_list));
	//
	elements.ch_scroll.getViewport().add(java.awt.Component(elements.ch_list));
	//FS
	//	/** @type {java.awt.Component} */
	//	var opListComponent = elements.op_list;
	//	/** @type {java.awt.Component} */
	//	var chListComponent = elements.ch_list;
	//	elements.op_scroll.getViewport().add(opListComponent);
	//	elements.ch_scroll.getViewport().add(chListComponent);
}

/**
 * @properties={typeid:24,uuid:"91B38CB7-0A77-4EE8-AA39-083B376B0B8A"}
 */
function getArraryLabels(a) {
	var ls = new Array();
	for (var i = 0; i < a.length; i++) {
		ls.push(a[i].label);
	}

}
