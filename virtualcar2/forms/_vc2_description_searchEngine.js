/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"982E42F5-05E8-47C0-9095-1AC2C5E66488"}
 */
var _form = "";

/** @type {Array} *
 * @properties={typeid:35,uuid:"18FEB113-FE59-4431-899C-8B0FB0684385",variableType:-4}
 */
var _lConfigFields = {p: null, v: null, s: null, d: null};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"22891E18-B280-4839-8B2D-444DC9F8092E"}
 */
var _lDescription = "";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"AF56C927-91A5-41A4-8897-8312224088C0",variableType:4}
 */
var _lEnabled = 0;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"4073116F-73AF-400F-B6B4-A35CACD9E0F5"}
 */
var _lStatus = "";

/**
 * @properties={typeid:24,uuid:"106F666A-11D9-41AF-894C-84B8769CBFF5"}
 */
function openPopup(f,cf){
	_lDescription = "";
	_lEnabled = 0;
	resetFilters();
	
	_form = f;
	_lConfigFields = cf;
	
	controller.show("descriptionSearch");
}

/**
 * @properties={typeid:24,uuid:"A5DA279D-F16E-48DE-94D2-186F549BC3EE"}
 * @AllowToRunInFind
 */
function closePopup(event){
	if(_lDescription){
		var _operation = event.getElementName();
		/** @type {JSFoundSet} */
				
		var _fs = forms[_form].foundset.duplicateFoundSet();

		if(_fs.find()){
			
			_fs[_lConfigFields.p] = (globals.vc2_currentProgetto) ? globals.vc2_currentProgetto : "!^";
			_fs[_lConfigFields.v] = (globals.vc2_currentVersione) ? utils.numberFormat(globals.vc2_currentVersione,"############") : "!^";
			_fs[_lConfigFields.s] = (_lStatus) ? _lStatus : "!^";
			_fs[_lConfigFields.d] = "#%" + _lDescription + "%";
			
			switch(_operation){
				case "_full":
					_fs.search();
					break;
				case "_red":
					_fs.search(false,true);
					break;
				case "_ext":
					_fs.search(false,false);
					break;
				default:
					throw "ERROR: unknown button pressed";
			}
		}
		
		if(_fs.getSize()){
			forms[_form].foundset.loadRecords(_fs);
			
			//application.closeFormDialog("descriptionSearch");
			
			var w=controller.getWindow();
			w.destroy();
		}else{
			plugins.dialogs.showInfoDialog("Nessuna corrispondenza","Non è stato trovato alcun record corrispondente ai criteri desiderati","Ok");
			elements._lDescription.requestFocus();
			elements._lDescription.selectAll();
		}
	}else{
		plugins.dialogs.showErrorDialog("Descrizione","Per effettuare una ricerca è necessario specificare un criterio nel campo \"Descrizione\"","Ok");
		elements._lDescription.requestFocus();
	}
}

/**
 * @properties={typeid:24,uuid:"D80682F6-F1F6-43A8-A70C-B9F20A4C908A"}
 */
function resetFilters(){
	var _enable = (_lEnabled) ? true : false;
	
	globals.vc2_currentProgetto = "";
	elements._lProject.enabled = _enable;
	globals.vc2_currentVersione = null;
	elements._lVersion.enabled = _enable;
	_lStatus = "";
	elements._lStatus.enabled = _enable;
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"2FB56856-6075-4027-A3AA-3ED7DDFC9DEA"}
 */
function onEnabledChange(oldValue, newValue, event) {
	resetFilters();
	return true
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"BC9849DF-CB7C-4EC9-818D-33F4208A58C7"}
 */
function onShowForm(firstShow, event) {
	elements._lDescription.requestFocus();
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"201FC895-45C9-48DF-98D8-5D0823631D77"}
 */
function onProgettoChange(oldValue, newValue, event) {
	globals.vc2_currentVersione = null;
	return true
}
