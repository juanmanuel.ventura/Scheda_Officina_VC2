/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"256184D0-48D5-4C98-A0AF-B72D5E1FC0B1"}
 */
var _d = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"49EE8A7D-6087-4F7B-87AA-26433363FA41"}
 */
var _p = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"64EB5AA1-9020-4C62-84E9-747AB4F3CA18"}
 */
var _v = "";

/**
 * @properties={typeid:24,uuid:"46DFCE7B-9D3E-43AE-8F5F-54D6C2E74071"}
 */
function openPopup(){
	_v = "";
	_d = "";
	
	var _pFS = forms.anagrafica_progetti.foundset.duplicateFoundSet();
	var _pDS = databaseManager.getDataSetByQuery(	"ferrari",
													databaseManager.getSQL(_pFS).replace("K8_ANAGRAFICA_PROGETTI_ID","PROGETTO"),
													databaseManager.getSQLParameters(_pFS),
													-1);
	_p = _pDS.getColumnAsArray(1).sort().join("\n");
	
	controller.show("experimentManagement");
}

/**
 * @param {JSEvent} event the event that triggered the action
 * 
 * @properties={typeid:24,uuid:"45821BE8-7648-4A32-86F7-07672317567F"}
 */
function addExperimentWrap(event){
	addExperiment();
}

/**
 * @properties={typeid:24,uuid:"EEFECCE7-ADD0-4FE5-BEB0-1F701CA65042"}
 * @AllowToRunInFind
 */
function addExperiment(){
	var _pFS = forms.anagrafica_progetti.foundset.duplicateFoundSet();
	var _r = null;
	for(var i=1;i<=_pFS.getSize();i++){
		_r = _pFS.getRecord(i).k8_anagrafica_progetti_to_k8_anagrafica_progetti_ver.duplicateFoundSet();
		if(_r.find()){
			_r.codice_vettura = utils.stringToNumber(_v);
			if(_r.search() == 0){
				_r.newRecord();
				_r.codice_vettura = utils.stringToNumber(_v);
				_r.versione = _d;
				_r.tipo_versione = "EXP";
				databaseManager.saveData(_r);
			}
		}
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * 
 * @properties={typeid:24,uuid:"D8F8612A-4B08-44BF-A90F-6A28146D7568"}
 */
function delExperimentWrap(event){
	delExperiment()
}

/**
 * @properties={typeid:24,uuid:"82223C67-724F-41B8-88DE-D7AC30B09E28"}
 * @AllowToRunInFind
 */
function delExperiment(){
	var _pFS = forms.anagrafica_progetti.foundset.duplicateFoundSet();
	var _r = null;
	for(var i=1;i<=_pFS.getSize();i++){
		_r = _pFS.getRecord(i).k8_anagrafica_progetti_to_k8_anagrafica_progetti_ver.duplicateFoundSet();
		if(_r.find()){
			_r.codice_vettura = utils.stringToNumber(_v);
			if(_r.search() != 0){
				_r.deleteAllRecords();
				databaseManager.saveData(_r);
			}
		}
	}
}
