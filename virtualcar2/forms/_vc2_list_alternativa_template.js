/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"E7BB989B-BA12-45D2-9236-D3F758B0F033"}
 */
var nascosto = '';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"EDE9E49C-D9F2-4615-8EEB-F6AE55D0CD03"}
 */
var testo2 = null;

/**
 * @properties={typeid:24,uuid:"851E2B54-654D-4926-864F-1C07222CF5E7"}
 */
function nfx_onShow(){
	setPosition();
}

/**
 * @properties={typeid:24,uuid:"DEEA658F-52BA-446C-9E1D-E51A5D6DC325"}
 */
function nfx_onRecordSelection(){
	setPosition();
}

/**
 * @properties={typeid:24,uuid:"6C5A7FF7-9E7C-4F11-8875-143102BAD476"}
 */
function goUp() {
	var y = elements.testo2.getScrollY() - 40;
	elements.testo2.setScroll(0,y);
}

/**
 * @properties={typeid:24,uuid:"8EC2382F-58E3-4DC1-8A79-93E504248AAB"}
 */
function goDown() {
	var y = elements.testo2.getScrollY() + 40;
	elements.testo2.setScroll(0,y);
}

/**
 * @properties={typeid:24,uuid:"0209597B-6EDF-491B-B508-DFB66DFDF670"}
 */
function goNext(){
	if(controller.getSelectedIndex() < controller.getMaxRecordIndex()){
		controller.setSelectedIndex(controller.getSelectedIndex() + 1);
	}
}

/**
 * @properties={typeid:24,uuid:"DE66462F-72C2-41DC-9A32-B08E483CDAC4"}
 */
function goPrevious(){
	if(controller.getSelectedIndex() > 1){
		controller.setSelectedIndex(controller.getSelectedIndex() - 1);
	}
}

/**
 * @properties={typeid:24,uuid:"B7570247-B762-4007-B32F-7DB05C329B74"}
 */
function setPosition(){
	elements.position.text = controller.getSelectedIndex() + "/" + controller.getMaxRecordIndex();
}

/**
 * @properties={typeid:24,uuid:"C1C00EA5-C9D0-49A3-96FA-A5F96430D484"}
 */
function hideOne(){
	if("Sì" == plugins.dialogs.showQuestionDialog("Conferma","Vuoi nascondere questo elemento? (per ripristinarlo sarà necessario contattare un amministratore)","Sì","No")){
		nascosto = "1";
		controller.loadRecords();
		setPosition();
	}
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"42C9CAF5-D4AC-4540-8C0D-9AE60759DEA3"}
 */
function showAllPopup(event) {
	
	var f = forms[event.getFormName()].foundset.duplicateFoundSet();
	f.sort(globals.utils_getIdDataProvider(event.getFormName()) + " asc");
	var n = databaseManager.getFoundSetCount(f);
	if(n > 1){
		var s = "";
		
		for(var i=1;i<=n;i++){
			/** @type {JSRecord} */
			var r = f.getRecord(i);
			s += "Creazione: {\\b " + r.utente_creazione + "}";
			s += (r.timestamp_creazione) ? " ({\\b " + utils.dateFormat(r.timestamp_creazione,"dd/MM/yyyy") + "})" : "";
			s += (r.testo2) ? "\\par " + r.testo.replace(/\n/g,"\\par ") + "\\par " : "\\par {\\i<nessun testo - si consiglia di nascondere questa pagina>}\\par ";
		}
		forms._vc2_show_all_popup.temp = s;
		
		//application.showFormInDialog(forms._vc2_show_all_popup,null,null,400,600,"Visualizzatore",true,false,"show_all",false);
	
		var formq = forms._vc2_show_all_popup;
		var window = application.createWindow("show_all",JSWindow.MODAL_DIALOG);
		window.title="Visualizzatore";
		window.setSize(400,600);
		window.resizable=true;
		formq.controller.show(window);
	
	}
}
