/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"2A1B8AFC-3D2B-483F-9632-30438C56E7ED"}
 */
var temp = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"EE32B174-C51E-4962-9AE4-0A248B016518"}
 */
var text = "";

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"56A6409D-647A-42D0-8EBF-2C548CEE5EA5"}
 */
function onShowForm(firstShow, event) {
	text = 	"{\\rtf1\\ansi{" + temp + "}}";
}

/**
 * Handle hide window.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} allow hide
 *
 * @properties={typeid:24,uuid:"1B5982EE-0906-410E-8DC4-51A23FFE7D55"}
 */
function onHide(event) {
	text = "";
	return true;
}
