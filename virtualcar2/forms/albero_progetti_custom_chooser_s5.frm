dataSource:"db:/ferrari/k8_dummy",
items:[
{
beanClassName:"javax.swing.JList",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.5.0_19\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JList\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>listChoices<\/string> \
  <\/void> \
  <void property=\"selectionMode\"> \
   <int>0<\/int> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"330,120",
name:"listChoices",
size:"80,80",
typeid:12,
uuid:"076D74A0-5D97-4ABF-B1A2-D1B1FFF9E4AB"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"127B464F-994F-4572-810C-24FBC185DB64",
location:"228,90",
mediaOptions:2,
onActionMethodID:"A1EB395D-527B-445C-A7B9-DB35D2B18AD6",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"1B89EC7C-4777-4DD2-8AA0-D9E24E225E3C"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"40905D44-A323-4D4C-9CCA-CC832C1E545D",
location:"228,60",
mediaOptions:2,
onActionMethodID:"C1605FE3-AE7D-479D-A6F9-78C7AD4B7772",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"36736644-8BC6-4944-BC55-B9CEEC93E5A6"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"E6C052AF-2185-445F-932A-5F615B6333EC",
location:"228,200",
mediaOptions:2,
onActionMethodID:"5828748F-FF3E-4D69-95FA-55770B9EB3CB",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"46A9D3FF-6412-4B32-92E9-8310653B04B5"
},
{
anchors:6,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"0746A208-4E27-45D4-BABE-FB96F24D516F",
location:"490,267",
mediaOptions:2,
onActionMethodID:"57AECA45-11C5-43E1-A16E-2BA61A72E7A9",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"71214191-CDB0-40B7-B407-2CDA33F0DA38"
},
{
location:"10,10",
mediaOptions:14,
size:"80,20",
tabSeq:-1,
text:"Opzioni",
transparent:true,
typeid:7,
uuid:"7F5E7E1E-E66B-401C-95CE-D47904146E7F"
},
{
anchors:3,
location:"260,10",
mediaOptions:14,
size:"80,20",
tabSeq:-1,
text:"Scelte",
transparent:true,
typeid:7,
uuid:"80D96AE3-20FC-40B2-B04A-04100F891FC8"
},
{
anchors:15,
beanClassName:"javax.swing.JScrollPane",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_17\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JScrollPane\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>210<\/int> \
    <int>230<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <object class=\"javax.swing.border.EtchedBorder\"> \
    <int>0<\/int> \
    <null/> \
    <null/> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>scrollOptions<\/string> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:-1,
location:"10,30",
name:"scrollOptions",
size:"210,230",
typeid:12,
uuid:"8D58EDD2-4924-45DC-8508-576896A66989"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"5804BE64-BD66-4BC7-B44A-3314D54EA001",
location:"228,130",
mediaOptions:2,
onActionMethodID:"1CF68096-81B9-4ECA-B795-8F3E2A16E504",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"95C9E0D4-7E94-4FE5-B127-C5CFEFC743F4"
},
{
height:300,
partType:5,
typeid:19,
uuid:"9A13720D-A196-4CD8-8670-71A142B86496"
},
{
anchors:7,
beanClassName:"javax.swing.JScrollPane",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_17\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JScrollPane\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>260<\/int> \
    <int>230<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <object class=\"javax.swing.border.EtchedBorder\"> \
    <int>0<\/int> \
    <null/> \
    <null/> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>scrollChoices<\/string> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:-2,
location:"260,30",
name:"scrollChoices",
size:"260,230",
typeid:12,
uuid:"B55073E9-4040-4CC5-915F-B6A4090B196F"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"6AD2E909-E7A4-491A-92E5-D7E2F229B865",
location:"228,160",
mediaOptions:2,
onActionMethodID:"1BEDD5A1-A392-4EEB-A054-2E47B069D069",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"C0F0A452-5EDF-48F7-A4B9-A475960F9445"
},
{
anchors:6,
borderType:"EmptyBorder,0,0,0,0",
imageMediaID:"85582DF7-017C-4741-98E4-7C3C1142DF33",
location:"460,267",
mediaOptions:2,
onActionMethodID:"8E66A7E6-BDEE-4222-AC65-C43A083D8A37",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"24,24",
transparent:true,
typeid:7,
uuid:"EAE52A48-5206-47DF-851B-05797008D21B"
},
{
beanClassName:"javax.swing.JList",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.5.0_19\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JList\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>listOptions<\/string> \
  <\/void> \
  <void property=\"selectionMode\"> \
   <int>0<\/int> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"70,120",
name:"listOptions",
size:"80,80",
typeid:12,
uuid:"F3CCD2B1-4B6A-4B1A-9DAB-F907747324FF"
}
],
name:"albero_progetti_custom_chooser_s5",
navigatorID:"-1",
onHideMethodID:"04DB9694-3500-459C-9351-23004381459E",
onLoadMethodID:"2E2E2D8E-0ED8-4D5F-AA3B-E30A99B2414D",
onShowMethodID:"0FB17733-EDDC-4429-82D1-8EFD7038FCAF",
paperPrintScale:100,
showInMenu:true,
size:"530,300",
styleName:"keeneight",
typeid:3,
uuid:"4C11338A-55BC-4DB0-A953-46C940B614E4"