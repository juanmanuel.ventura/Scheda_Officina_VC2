/**
 * @properties={typeid:35,uuid:"7809D897-89C8-4E4E-B51A-E6AF027EA3E7",variableType:-4}
 */
var choices = [];

/**
 * @properties={typeid:35,uuid:"4C610DFC-9110-477F-BDDE-03B6679B8B5C",variableType:-4}
 */
var choicesBK = new Array();

/**
 * @properties={typeid:35,uuid:"1D42A1F6-211F-4397-A969-282F1FF6497C",variableType:-4}
 */
var options = new Array();

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"586C15B4-CFF8-41C3-83F0-61F9CC6F70CE"}
 */
var repaint = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"2903593D-33A4-4CB2-96AD-1C63819A92D2"}
 */
var tree_container = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"EFE642E4-FD60-4E01-ADE4-867AF970AE9E"}
 */
var tree_content = forms.albero_progetti_custom_content_s5.controller.getName();

/**
 *
 * @properties={typeid:24,uuid:"20B1761A-9AC7-44D5-9B70-38DFC0C5F403"}
 */
function dcAdd(e)
{
	if(e.getClickCount() == 2)
	{
		add();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"C1605FE3-AE7D-479D-A6F9-78C7AD4B7772"}
 */
function add()
{
	if(elements.listOptions.getSelectedValue() && choices.indexOf(elements.listOptions.getSelectedValue()) == -1)
	{
		choices.push(elements.listOptions.getSelectedValue());
		elements.listChoices.setListData(choices);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"2EF80D80-207C-4D56-8704-662329ED1226"}
 */
function dcRemove(e)
{

	if(e.getClickCount() == 2)
	{
		remove();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"A1EB395D-527B-445C-A7B9-DB35D2B18AD6"}
 */
function remove()
{
	if(elements.listChoices.getSelectedValue())
	{
		choices.splice(choices.indexOf(elements.listChoices.getSelectedValue()),1);
		elements.listChoices.setListData(choices);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"1CF68096-81B9-4ECA-B795-8F3E2A16E504"}
 */
function moveUp()
{
	if(elements.listChoices.getSelectedValue() && choices.indexOf(elements.listChoices.getSelectedValue()) != 0)
	{
		var index = choices.indexOf(elements.listChoices.getSelectedValue());
		var app = choices[index - 1];
		choices[index - 1] = choices[index];
		choices[index] = app;
		elements.listChoices.setListData(choices);
		elements.listChoices.setSelectedIndex(index - 1);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"1BEDD5A1-A392-4EEB-A054-2E47B069D069"}
 */
function moveDown()
{
	if(elements.listChoices.getSelectedValue() && choices.indexOf(elements.listChoices.getSelectedValue()) != choices.length-1)
	{
		var index = choices.indexOf(elements.listChoices.getSelectedValue());
		var app = choices[index + 1];
		choices[index + 1] = choices[index];
		choices[index] = app;
		elements.listChoices.setListData(choices);
		elements.listChoices.setSelectedIndex(index + 1);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"5828748F-FF3E-4D69-95FA-55770B9EB3CB"}
 */
function clear()
{
	choices = new Array();
	elements.listChoices.setListData(choices);
}

/**
 *
 * @properties={typeid:24,uuid:"8E66A7E6-BDEE-4222-AC65-C43A083D8A37"}
 */
function save()
{
	forms.nfx_interfaccia_gestione_salvataggi.save(choices,tree_container);
	repaint = true;
	
	//application.closeFormDialog();
	
	var w=controller.getWindow();
	w.destroy();
}

/**
 *
 * @properties={typeid:24,uuid:"57AECA45-11C5-43E1-A16E-2BA61A72E7A9"}
 */
function cancel()
{
	repaint = null;
	
	//application.closeFormDialog();
	
	var w=controller.getWindow();
	w.destroy();
	
}

/**
 * @properties={typeid:24,uuid:"0DB7FE0B-152C-4EDE-81EF-F18172DD7CFC"}
 */
function getLabels()
{
	var lbls = new Array();
	for(var i=0;i<options.length;i++){
		lbls.push(options[i].label || options[i].dataprovider);
	}
	return lbls;
}

/**
 * @properties={typeid:24,uuid:"174238D2-9ECE-4AD8-89EC-9942C9A6E73A"}
 */
function getColumns()
{
	var clmns = new Array();
	for(var i=0;i<choices.length;i++){
		clmns.push(getColumn(choices[i]));
	}
	return clmns;
}

/**
 * @properties={typeid:24,uuid:"3BFED76A-D625-4A22-9F43-FA7B3A41B507"}
 */
function getColumn(label)
{
	for(var i=0;i<options.length;i++){
		if(options[i].label == label) return options[i];
	}
	return null;
}

/**
 *
 * @properties={typeid:24,uuid:"2E2E2D8E-0ED8-4D5F-AA3B-E30A99B2414D"}
 */
function onLoad()
{
	var baan_rel = "k8_distinte_progetto_core_to_k8_distinta_baan_esponenti.k8_distinta_baan_esponenti_to_k8_distinta_baan.";
	
	options = [
	
	{dataprovider: "k8_distinte_progetto_core_to_k8_distinte_progetto_conteggi.segnalazioni", label: "N° Segnalazioni"},
	{dataprovider: "k8_distinte_progetto_core_to_k8_distinte_progetto_conteggi.schede", label: "N° Schede"},
	{dataprovider: "k8_distinte_progetto_core_to_k8_distinte_progetto_conteggi.modifiche", label: "N° Modifiche"},
	{dataprovider: "k8_distinte_progetto_core_to_k8_distinte_progetto.unita_misura_peso", label: "UDM Peso (AS400)"},
	{dataprovider: "k8_distinte_progetto_core_to_k8_distinte_progetto.peso", label: "Peso (AS400)"},
	{dataprovider: "k8_distinte_progetto_core_to_k8_distinte_progetto.peso_unitario", label: "Peso unitario (AS400)"},
	{dataprovider: "k8_distinte_progetto_core_to_k8_distinte_progetto.descrizione_materiale", label: "Materiale (AS400)"},
	{dataprovider: "k8_distinte_progetto_core_to_k8_distinte_progetto.descrizione_legame", label: "Legame (AS400)"},
	{dataprovider: baan_rel + "codice_articolo_baan", label: "Codice BaaN"},
	{dataprovider: "k8_distinte_progetto_core_to_k8_distinta_baan_esponenti.peso_target", label: "Peso Target (BaaN)"},
	{dataprovider: "k8_distinte_progetto_core_to_k8_distinta_baan_esponenti.peso_effettivo", label: "Peso Effettivo (BaaN)"},
	{dataprovider: baan_rel + "tipo_articolo", label: "Tipo (BaaN)"},
	{dataprovider: baan_rel + "fornitore", label: "Fornitore (BaaN)"},
	{dataprovider: baan_rel + "udm", label: "UDM (BaaN)"},
	{dataprovider: baan_rel + "quantita", label: "Q.ta (BaaN)"},
	{dataprovider: baan_rel + "peso_anagrafica", label: "Peso Anagrafica (BaaN)"},
	{dataprovider: baan_rel + "tipologia", label: "Tipologia (BaaN)"},
	{dataprovider: "k8_distinte_progetto_core_to_k8_distinte_progetto.note_as400", label: "Note (AS400)"},
	{dataprovider: "complessivo", label: "Complessivo (79...)"}
	
	];
//	JStaffa
//	elements.listOptions.addMouseListener(dcAdd);
//	elements.listChoices.addMouseListener(dcRemove);
	elements.listOptions.addMouseListener(java.awt.event.MouseListener(dcAdd));
	elements.listChoices.addMouseListener(java.awt.event.MouseListener(dcRemove));
}

/**
 *
 * @properties={typeid:24,uuid:"0FB17733-EDDC-4429-82D1-8EFD7038FCAF"}
 */
function onShow()
{
	tree_container = forms[tree_content].tree_container;
	repaint = null;

	choicesBK = choices.slice(null,null);
	
	var vpOptions = elements.scrollOptions.getViewport();
//	JStaffa
//	vpOptions.add(elements.listOptions);
	vpOptions.add(java.awt.Component(elements.listOptions));

	var vpChoices = elements.scrollChoices.getViewport();
//	JStaffa
//	vpChoices.add(elements.listChoices);
	vpChoices.add(java.awt.Component(elements.listChoices));

	elements.listOptions.setListData(getLabels());
	elements.listChoices.setListData(choices);
}

/**
 *
 * @properties={typeid:24,uuid:"04DB9694-3500-459C-9351-23004381459E"}
 */
function onHide()
{
	if(!repaint)
	{
		choices = choicesBK.slice(null,null);
	}
}
