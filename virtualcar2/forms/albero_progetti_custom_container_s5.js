/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"309B2B5B-66CC-45DC-958F-5A3BF75E78C2"}
 */
var tree_chooser = forms.albero_progetti_custom_chooser_s5.controller.getName();

/**
 * @properties={typeid:35,uuid:"14292C8C-4502-494B-A362-C4D7ED46B665",variableType:-4}
 */
var tree_content = forms.albero_progetti_custom_content_s5.controller.getName();

/**
 * @properties={typeid:24,uuid:"707E9827-0032-451A-8DEA-8F0ECC5A38F4"}
 */
function writeMessage(m)
{
	var message = m || "";
	
	elements.message.text = message;
	elements.message_shadow.text = message;
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 *
 * @properties={typeid:24,uuid:"A51A5B84-2102-487D-93FE-32D7E919B71A"}
 */
function onVersioneChange(oldValue, newValue, event) {
	forms[tree_content].setRoot(k8_dummy_to_k8_distinte_progetto_core$root.descrizione,
						 		k8_dummy_to_k8_distinte_progetto_core$root.path_id,
						 		k8_dummy_to_k8_distinte_progetto_core$root.numero_disegno);
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 *
 * @properties={typeid:24,uuid:"DA61F64F-5E3A-48E7-B755-08E9B1D9A3F8"}
 */
function onProgettoChange(oldValue, newValue, event) {
	globals.vc2_currentVersione = null;
	onVersioneChange(null,null,null);
}

/**
 * @properties={typeid:24,uuid:"292A70BF-4B44-447E-A3D6-9F39C772D771"}
 */
function nfx_customExport()
{
	forms[tree_content].customExportWrap();
}

/**
 * @properties={typeid:24,uuid:"8473EE22-81DD-4171-85FC-DCB54639528A"}
 */
function customizeWrap()
{
	forms[tree_content].customize();
	controller.recreateUI();
}

/**
 * @properties={typeid:24,uuid:"B07D3578-59C8-476C-A79E-B3AB6ABDDE09"}
 */
function nfx_onShow(){
	forms[tree_content].tree_container = controller.getName();
	var vero=forms[tree_chooser].choices = forms.nfx_interfaccia_gestione_salvataggi.load(controller.getName());
	if(vero){
	//if(forms[tree_chooser].choices = forms.nfx_interfaccia_gestione_salvataggi.load(controller.getName())){
		forms[tree_content].removeColumns();
		forms[tree_content].addColumns();
	}else{
		forms[tree_chooser].choices = new Array();
	}
}
