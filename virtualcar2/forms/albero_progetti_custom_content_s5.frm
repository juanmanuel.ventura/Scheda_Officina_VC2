dataSource:"db:/ferrari/k8_distinte_progetto_core",
initialSort:"path_id asc",
items:[
{
anchors:11,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"esponente",
editable:false,
horizontalAlignment:0,
location:"520,90",
name:"esponente",
size:"40,20",
text:"Esponente",
transparent:true,
typeid:4,
uuid:"1E197F5E-D8A2-489F-A205-BD69E1080607"
},
{
anchors:11,
labelFor:"numero_disegno",
location:"420,60",
mediaOptions:14,
size:"100,20",
styleClass:"table",
tabSeq:-1,
text:"Disegno",
typeid:7,
uuid:"310D28F6-694B-4D19-B250-5F7B7288A26E"
},
{
anchors:11,
labelFor:"esponente",
location:"520,60",
mediaOptions:14,
size:"40,20",
styleClass:"table",
tabSeq:-1,
text:"Esp.",
typeid:7,
uuid:"45DC540C-7459-437C-ADB7-0B2E0CA64552"
},
{
anchors:11,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"numero_disegno",
editable:false,
format:"############",
horizontalAlignment:0,
location:"420,90",
name:"numero_disegno",
size:"100,20",
text:"Numero Disegno",
transparent:true,
typeid:4,
uuid:"53DE1F32-C94C-4D04-9062-F89543182C5C"
},
{
anchors:11,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"coefficiente_impiego",
editable:false,
formIndex:13,
horizontalAlignment:0,
location:"560,90",
name:"coefficiente_impiego",
size:"40,20",
text:"Coefficiente Impiego",
transparent:true,
typeid:4,
uuid:"6119D6A1-F405-43DD-ABE9-C67D42BE8BD9"
},
{
anchors:11,
formIndex:-2,
lineSize:1,
location:"5,20",
roundedRadius:20,
shapeType:2,
size:"590,20",
typeid:21,
uuid:"66B9E260-ABAA-43B2-BC67-6A67FD901D2C"
},
{
anchors:11,
formIndex:12,
labelFor:"coefficiente_impiego",
location:"560,60",
mediaOptions:14,
size:"40,20",
styleClass:"table",
tabSeq:-1,
text:"Q.ta",
typeid:7,
uuid:"72C9B767-3AB6-4825-8927-A8739FD809CB"
},
{
anchors:11,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"filter",
formIndex:6,
location:"20,20",
name:"filter",
onActionMethodID:"4E507B25-9635-4120-8350-A1A7076B02B8",
size:"540,20",
text:"Filter",
transparent:true,
typeid:4,
uuid:"85E5C2DF-EA15-4CC1-B994-360B18076B84"
},
{
foreground:"#cfcfcf",
labelFor:"tree_icon",
location:"0,60",
mediaOptions:14,
size:"20,20",
styleClass:"table",
tabSeq:-1,
typeid:7,
uuid:"8D6A1934-9290-442C-B3CA-348A6E6FEED4"
},
{
anchors:3,
formIndex:7,
imageMediaID:"618F9AA0-E8E0-4019-A9D8-472CC462832F",
location:"570,22",
mediaOptions:14,
size:"16,16",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"902964A5-313F-4B41-9454-59C36F631A11"
},
{
formIndex:9,
horizontalAlignment:0,
imageMediaID:"04E7B05D-5640-4173-AED4-14BA40D59557",
location:"0,40",
mediaOptions:2,
onActionMethodID:"1D319727-415E-44CB-AE90-9CEF1A683E2E",
showClick:false,
size:"20,20",
transparent:true,
typeid:7,
uuid:"A857CE81-63A3-4F7B-8532-B89310E42A43",
verticalAlignment:0
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"rootText",
editable:false,
formIndex:14,
location:"20,40",
name:"rootText",
size:"560,20",
transparent:true,
typeid:4,
uuid:"ACC54C09-7EC9-4BC3-9CF8-2BF149ECE0D5"
},
{
height:60,
partType:1,
typeid:19,
uuid:"AEF9CED4-9BB4-4CBF-AB80-1BD6FCE79D26"
},
{
height:110,
partType:5,
typeid:19,
uuid:"AFF9107B-DFCB-45E5-B737-77E897F48B2F"
},
{
anchors:3,
formIndex:10,
imageMediaID:"0746A208-4E27-45D4-BABE-FB96F24D516F",
location:"582,42",
mediaOptions:14,
onActionMethodID:"90EB65A1-28F3-4A70-97A0-9625DB6087D3",
showClick:false,
size:"16,16",
transparent:true,
typeid:7,
uuid:"B20B5B8E-D3B8-4C19-B4AE-E52B4F0BE30C"
},
{
anchors:11,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"descrizione_tree",
editable:false,
location:"20,90",
name:"descrizione_tree",
size:"400,20",
text:"Descrizione Tree",
transparent:true,
typeid:4,
uuid:"B50555C9-64A6-4A43-B6D1-3E7ED2986464"
},
{
anchors:11,
labelFor:"descrizione_tree",
location:"20,60",
mediaOptions:14,
size:"400,20",
styleClass:"table",
tabSeq:-1,
text:"Descrizione",
typeid:7,
uuid:"C92DC2F8-6978-4CED-9552-AF4FC2600EF2"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"tree_icon",
displayType:9,
editable:false,
location:"0,90",
name:"tree_icon",
onActionMethodID:"F4BBC067-D59F-4551-8DD1-3A4F59109ACE",
scrollbars:36,
size:"20,20",
text:"Tree Icon",
transparent:true,
typeid:4,
uuid:"DED49DF0-26E8-4EB1-80AE-CFE9812F7FE7"
},
{
anchors:11,
horizontalAlignment:0,
location:"0,0",
mediaOptions:14,
name:"header",
size:"600,20",
styleClass:"form",
tabSeq:-1,
text:"AS400",
transparent:true,
typeid:7,
uuid:"F5A2B1B4-7FE7-43D7-BB17-E66D24802E9B"
}
],
name:"albero_progetti_custom_content_s5",
navigatorID:"-1",
onHideMethodID:"61A716C7-5A43-4563-85C3-06886E784823",
onLoadMethodID:"3265D8E5-FD7E-4B41-85C6-ACF2A160F99A",
onRecordSelectionMethodID:"16B75128-BCA3-4F02-8CE1-BEE66C2AF67D",
onRenderMethodID:"DCBF3F2C-4563-4EC1-987C-2FB1A82C1218",
paperPrintScale:100,
showInMenu:true,
size:"600,110",
styleName:"keeneight",
typeid:3,
uuid:"965BC86D-B7B1-49E8-9254-B6C5ADDA55CC",
view:3