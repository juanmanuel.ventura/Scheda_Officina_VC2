/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C1D71897-5929-44C5-949B-8858D57493EE"}
 */
var filter = "";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"086DD0D8-5F55-42AE-B841-8EBE4B14BBB5",variableType:4}
 */
var lastPosition = 600;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"C3919FB4-E508-4264-9B53-3E3EC6CB6AD3",variableType:4}
 */
var rootNumber = 0;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D9C66C21-4CE8-4695-9FE9-4722436B05A0"}
 */
var rootPathID = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B207A1E1-9BE5-4C85-9E01-4796EDDE709C"}
 */
var rootText = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"0480E021-7AF0-4478-A394-CAABB7A8473C"}
 */
var tree_chooser = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B85FCB3C-C182-42B6-A98A-B4677EC9D5E8"}
 */
var tree_container = null;

/**
 * @properties={typeid:24,uuid:"6680FBC3-4288-4CA8-A107-8404CE366C17"}
 */
function addColumns() {
	/** @type {Array} */
	var columns = forms[tree_chooser].getColumns();
	controller.setDesignMode(true);
	for (var i = 0; i < columns.length; i++) {
		var dataprovider = columns[i].dataprovider;
		var label = columns[i].label;
		var form = solutionModel.getForm(controller.getName());
		form.view = JSForm.LOCKED_TABLE_VIEW;
		var field = form.newField(dataprovider, JSField.TEXT_FIELD, lastPosition, 90, 140, 20);
		field.name = dataprovider + "_$ADDED$";
		field.editable = false;
		field.transparent = true;
		field.borderType = "EmptyBorder, 0, 0, 0, 0";
		field.anchors = SM_ANCHOR.NORTH | SM_ANCHOR.WEST | SM_ANCHOR.EAST;
		var label2 = form.newLabel(label, lastPosition, 60, 140, 20);
		label2.labelFor = dataprovider + "_$ADDED$";
		label2.styleClass = "table";
		label2.transparent = false;
		label2.anchors = SM_ANCHOR.NORTH | SM_ANCHOR.WEST | SM_ANCHOR.EAST;
		lastPosition += 140;
	}
	controller.setDesignMode(false);
}

/**
 * @properties={typeid:24,uuid:"F4C2563E-5B16-49AF-8B72-53FB35AA583D"}
 */
function removeColumns() {
	var remove = new Array();
	for (var i = 0; i < elements.allnames.length; i++) {
		if (elements.allnames[i].indexOf("_$ADDED$") != -1) remove.push(elements.allnames[i]);
	}
	controller.setDesignMode(true);
	var form = solutionModel.getForm(controller.getName());
	for (var i2 = 0; i2 < remove.length; i2++) {
		form.removeField(remove[i2]);
	}
	controller.setDesignMode(false);
	lastPosition = 600;
}

/**
 * @properties={typeid:24,uuid:"F4BBC067-D59F-4551-8DD1-3A4F59109ACE"}
 */
function workWrap() {
	time(work);
}

/**
 * @properties={typeid:24,uuid:"DF759B56-4DFE-46FE-99A2-3994AD6605EE"}
 */
function work() {
	if (status != "O") {
		if (status == "+") {
			expand();
			status = "-";
		} else {
			collapse();
			status = "?";
		}
	}
}

/**
 * @properties={typeid:24,uuid:"2431068E-B67A-4A03-B61F-FB98C60594E9"}
 * @AllowToRunInFind
 */
function expand() {
	var index = controller.getSelectedIndex();
	var cv = codice_vettura;
	var nd = numero_disegno;
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		ufs.codice_vettura = cv;
		ufs.codice_padre = nd;
		ufs.search(false, false);
	}
	controller.loadRecords(ufs);
	controller.setSelectedIndex(index);
}

/**
 * @properties={typeid:24,uuid:"3C32D49A-5452-447D-A616-49F57FB1468A"}
 * @AllowToRunInFind
 */
function collapse() {
	var index = controller.getSelectedIndex();
	var pid = path_id;
	var ufs = foundset.unrelate();
	for (var i = 1; i <= ufs.getSize(); i++) {
		var record = ufs.getRecord(i);
		if (record.path_id.search(pid) != -1) record.status = "?";
	}
	if (ufs.find()) {
		ufs.path_id = "!" + pid + "/%";
		ufs.search(false, true);
	}
	controller.loadRecords(ufs);
	controller.setSelectedIndex(index);
}

/**
 * @properties={typeid:24,uuid:"1D319727-415E-44CB-AE90-9CEF1A683E2E"}
 */
function isolateWrap() {
	if (globals.vc2_currentVersione) {
		time(isolate);
	}

}

/**
 * @properties={typeid:24,uuid:"3D2AE8CE-1CEA-49B5-A2F8-2247BB21C0C0"}
 * @AllowToRunInFind
 */
function isolate() {
	var pid = path_id;
	var d = descrizione;
	var cv = codice_vettura;
	var nd = numero_disegno;
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		ufs.codice_vettura = cv;
		ufs.codice_padre = nd;
		ufs.search();
	}
	controller.loadRecords(ufs);
	setRoot(d, pid, nd);
}

/**
 * @properties={typeid:24,uuid:"90EB65A1-28F3-4A70-97A0-9625DB6087D3"}
 */
function releaseWrap() {
	if (globals.vc2_currentVersione) {
		time(release);
	}
}

/**
 * @properties={typeid:24,uuid:"E3F0C1FF-3FD1-4461-B850-8C20B2DE6991"}
 */
function release() {
	/** @type {JSFoundSet} */
	var fs = k8_dummy_to_k8_distinte_progetto_core;
	for (var i = 1; i <= fs.getSize(); i++) {
		var record = fs.getRecord(i);
		record.status = "?";
	}
	databaseManager.recalculate(fs);
	controller.loadRecords(fs);
	setRoot(k8_dummy_to_k8_distinte_progetto_core$root.descrizione,
		k8_dummy_to_k8_distinte_progetto_core$root.path_id,
		k8_dummy_to_k8_distinte_progetto_core$root.numero_disegno);
	controller.sort("path_id asc");
}

/**
 * @properties={typeid:24,uuid:"4E507B25-9635-4120-8350-A1A7076B02B8"}
 */
function filterTreeWrap() {
	if (globals.vc2_currentVersione) {
		time(filterTree);
	}
}

/**
 * @properties={typeid:24,uuid:"D606A46B-EEAE-4E11-A052-5D17F730B14F"}
 * @AllowToRunInFind
 */
function filterTree() {
	//var ufs = foundset.unrelate();
	var ufs = foundset.unrelate();

	if (ufs.find()) {
		if (filter) {
			ufs.path_id = rootPathID + "/%";
			try {
				ufs.numero_disegno = java.lang.Double.parseDouble(filter);
			} catch (e) {
				ufs.descrizione = "#" + filter;
			}
		} else {
			ufs.codice_vettura = globals.vc2_currentVersione;
			ufs.codice_padre = rootNumber;
		}
		ufs.search();
	}
	controller.loadRecords(ufs);
}

/**
 * @properties={typeid:24,uuid:"0C9EB51C-E71D-4D5D-863E-29002FBCBE66"}
 */
function setRoot(label, pid, number) {
	rootText = label;
	rootPathID = pid;
	rootNumber = number
}

/**
 * @properties={typeid:24,uuid:"39D29ECD-DDE0-4819-96C3-BD8EFC12EC13"}
 */
function time(func) {
	//tempo di esecuzione di una funzione senza paramenti
	if (func && typeof func == "function") {
		forms[tree_container].writeMessage("...operazione in corso");
		application.updateUI();
		var start = new Date();
		var result = func();
		var end = new Date();
		forms[tree_container].writeMessage( ( (end - start) / 1000).toFixed(2) + "s");
		return result;
	}
	return null;
}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"3265D8E5-FD7E-4B41-85C6-ACF2A160F99A"}
 */
function onLoad(event) {
	tree_chooser = forms.albero_progetti_custom_chooser_s5.controller.getName();
}

/**
 * Handle record selected.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"16B75128-BCA3-4F02-8CE1-BEE66C2AF67D"}
 */
function onRecordSelected(event) {
	globals.vc2_progettiCurrentNode = k8_distinte_progetto_id;
	globals.vc2_progettiCurrentNodePath = path_id + "%";
	globals.vc2_progettiCurrentNodeNumeroDisegno = globals.vc2_currentDisegno = numero_disegno;
	globals.vc2_progettiCurrentNodeEsponente = esponente;
		
}

/**
 * Handle hide window.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 *
 * @properties={typeid:24,uuid:"61A716C7-5A43-4563-85C3-06886E784823"}
 */
function onHide(event) {

	globals.vc2_progettiCurrentNode = -1;
	globals.vc2_progettiCurrentNodePath = null;
	globals.vc2_progettiCurrentNodeNumeroDisegno = globals.vc2_currentDisegno = -1;
	globals.vc2_progettiCurrentNodeEsponente = -1;

	filter = null;
}

/**
 * @properties={typeid:24,uuid:"3CE8A33F-2410-49D9-87BB-648156A3E66B"}
 */
function customExportWrap() {
	var buffer = time(customExport);
	globals.utils_saveAsTXT(buffer, "exportAlberoProgetti-" + utils.dateFormat(new Date(), "yyyyMMdd") + ".xls");
}

/**
 * @properties={typeid:24,uuid:"38E1CBD9-15BC-444E-A2F2-8EEABF1090E3"}
 */
function customExport() {
	//SAuc
	//var query = "select lpad(descrizione, 5*livello + length(descrizione), '     ')||'\t'||numero_disegno||'\t'||esponente||'\t'||coefficiente_impiego||'\t'||path_numero_disegno||'\n' from " + controller.getTableName() + " where path_id like ? order by path_id asc";

	var query2 = "select lpad(descrizione, 5*livello + length(descrizione), '     ')||'\t'||numero_disegno||'\t'||esponente||'\t'||coefficiente_impiego||'\t'||path_numero_disegno||'\n' from " + databaseManager.getDataSourceTableName(controller.getDataSource()) + " where path_id like ? order by path_id asc";

	//if(query == query2) application.output('uguali!');

	var args = [rootPathID + "%"];
	//SAuc
	//var ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,-1);

	var ds2 = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query2, args, -1);

	var buffer = "\tNumero disegno\tEsp.\tQ.ta\tN. Segnalazioni\tN. Schede\tN. Schede Mod.\tPath numero disegno\n" + ds2.getColumnAsArray(1).join("");
	return buffer;
}

/**
 * @properties={typeid:24,uuid:"EB7FFA7E-F03E-45D7-977C-5A10B076CA26"}
 */
function customize() {

	//application.showFormInDialog(forms[tree_chooser],null,null,null,null,"Scelta colonne",true,false,"chooser_popup",true);

	var formq = forms[tree_chooser];
	var window = application.createWindow("Scelta colonne", JSWindow.MODAL_DIALOG);
	window.title = "Scelta colonne";
	window.resizable = true;
	formq.controller.show(window);

	if (forms[tree_chooser].repaint) {
		removeColumns();
		addColumns();
	}
}

/**
 * Called before the form component is rendered.
 *
 * @param {JSRenderEvent} event the render event
 *
 * @private
 *
 * @properties={typeid:24,uuid:"DCBF3F2C-4563-4EC1-987C-2FB1A82C1218"}
 */
function onRender(event) {
	// TODO FS cambio il colore del testo ed imposto il grassetto sul record selezionato
	if(event.isRecordSelected() && event.getRenderable().getName()!='header' && event.getRenderable().getName()!='rootText')
	{
		event.getRenderable().fgcolor = '#339eff';
		
		event.getRenderable().font='Verdana,1,11';
	}
	else{
		event.getRenderable().fgcolor = '#000000';
	}
	
}
