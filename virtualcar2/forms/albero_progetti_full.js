/**
 * @properties={typeid:35,uuid:"BC018395-6FB1-4311-9BA2-1C30D5BB6092",variableType:-4}
 */
var nfx_related = ["distinte_progetto_dettaglio_record","codici_disegni_baan_esponenti_table","tag_table","delibere_table","distinte_progetto_albero_segnalazioni_anomalie_table","distinte_progetto_albero_schede_anomalie_table","distinte_progetto_albero_schede_modifica_table","soluzioni_tampone_cicli_record","distinte_progetto_albero_vetture_table"];

/**
 * @properties={typeid:24,uuid:"3FE370F3-5E88-4FD5-B026-2878F75DA48F"}
 */
function nfx_getTitle(){
	return "Albero distinte progetti (FULL)"; 
}

/**
 * @properties={typeid:24,uuid:"92C9FC3C-9959-4552-AF35-EE8A8FB8EC2E"}
 */
function nfx_isProgram(){
	return true;
}
