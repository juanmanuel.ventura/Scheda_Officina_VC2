/**
 * @properties={typeid:35,uuid:"BB12AE5E-CC1F-4B86-96C2-DE407CFD755F",variableType:-4}
 */
var nfx_related = ["distinte_progetto_dettaglio_record"];

/**
 * @properties={typeid:24,uuid:"63552D2D-C39E-46AB-828D-A434B33F58F2"}
 */
function nfx_getTitle(){
	return "Albero distinte progetti (LITE)"; 
}

/**
 * @properties={typeid:24,uuid:"A389B17A-667D-4D6E-8C29-47B985F1129F"}
 */
function nfx_isProgram(){
	return true;
}
