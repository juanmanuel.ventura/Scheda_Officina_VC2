/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F3443E09-155C-4034-B442-5404937B533C"}
 */
var nfx_orderBy = "coefficiente";

/**
 * @properties={typeid:35,uuid:"2DB7CF03-A508-46AF-93AE-082EB849B43F",variableType:-4}
 */
var nfx_related = ["jt_coefficienti_percorsi_for_coefficienti_table"];

/**
 *
 * @properties={typeid:24,uuid:"90267208-4C32-44CE-9B64-2F2EEB0B46BC"}
 */
function nfx_defineAccess()
{
	return [true, true, true];
}
