/**
 *
 * @properties={typeid:24,uuid:"074F6399-8B02-4B70-8878-B7E8D2EC4E07"}
 */
function nfx_getTitle() {
	return "Anagrafica Coefficienti";
}

/**
 *
 * @properties={typeid:24,uuid:"52A626BC-C8A1-415B-9E2C-602B5AC08CBC"}
 */
function nfx_isProgram() {
	return true;
}
