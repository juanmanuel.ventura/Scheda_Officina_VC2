/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C34E2994-2A3E-462E-B670-1B7B47D775EA"}
 */
var nfx_orderBy = "";

/**
 * @properties={typeid:35,uuid:"6D50ADD9-91F2-419A-93E4-BE562B1F7B1A",variableType:-4}
 */
var nfx_related = ["time_line_table", "segnalazioni_anomalie_table", "schede_anomalie_table", "schede_modifica_record", "anagrafica_disegni_dettaglio", "tag_table"];

/**
 *
 * @properties={typeid:24,uuid:"37540CE7-0121-4131-A302-4A62A00A6AC5"}
 */
function nfx_defineAccess()
{
	return [true, true, true];
}
