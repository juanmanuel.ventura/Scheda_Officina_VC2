/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A404A36C-4DB1-426C-910B-14264606FB6B"}
 */
var nfx_orderBy = "progetto desc, codice_articolo_baan desc";

/**
 * @properties={typeid:35,uuid:"8E8A9297-2933-4769-854F-8EE3C932A9F5",variableType:-4}
 */
var nfx_related = ["time_line_table", "tag_table", "documenti_record"];

/**
 *
 * @properties={typeid:24,uuid:"952769DF-0FC6-44D7-8B39-A18C00670062"}
 */
function nfx_defineAccess()
{
	return [true, true, true];
}
