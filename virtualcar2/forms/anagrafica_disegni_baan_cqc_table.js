/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"924D241D-BC23-46CB-9AE1-2E40C18DE2DC"}
 */
var nfx_relatedi = null;

/**
 *
 * @properties={typeid:24,uuid:"E92A9BCA-946B-49EA-B7DE-2ABB0E889F93"}
 */
function nfx_getTitle(){
	return "Scheda CQC";
}

/**
 *
 * @properties={typeid:24,uuid:"BF194004-9546-45E8-B2D7-03E1F0E8059C"}
 */
function nfx_isProgram(){
	return true;
}
