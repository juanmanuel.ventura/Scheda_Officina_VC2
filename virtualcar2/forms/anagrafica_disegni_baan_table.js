/**
 *
 * @properties={typeid:24,uuid:"B10F54F7-4E6D-4C21-838C-E9BACF2719B5"}
 */
function nfx_getTitle()
{

	return "Anagrafica Disegni Baan - Tabella";
}

/**
 *
 * @properties={typeid:24,uuid:"B5BBEB30-FFED-4B05-B901-F08AD5178A85"}
 */
function nfx_isProgram()
{
	return true;
}
