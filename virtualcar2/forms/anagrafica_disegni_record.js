/**
 *
 * @properties={typeid:24,uuid:"1C076EE0-1150-487C-88F1-58A0C5A257F4"}
 */
function nfx_getTitle()
{

	return "Anagrafica Disegni AS400";
}

/**
 *
 * @properties={typeid:24,uuid:"5519B407-AF41-4AE8-95C6-2EF8E6C60724"}
 */
function nfx_isProgram()
{
	return true;
}
