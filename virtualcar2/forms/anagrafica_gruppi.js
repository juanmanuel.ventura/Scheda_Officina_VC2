/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"26797A1A-CE52-496C-94D7-D3F3B559312A"}
 */
var nfx_orderBy = "gruppo asc";

/**
 *
 * @properties={typeid:24,uuid:"FB07226C-8D6F-4922-83C3-4E1ABED7CD8B"}
 */
function nfx_defineAccess()
{
	return [false,false,false];
}
