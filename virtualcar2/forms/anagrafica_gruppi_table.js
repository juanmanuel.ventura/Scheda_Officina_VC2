/**
 *
 * @properties={typeid:24,uuid:"25B37947-8DFB-4A09-99F1-94DEF0E7BAC8"}
 */
function nfx_getTitle()
{
	return "Tabella Anagrafica Gruppi";
}

/**
 *
 * @properties={typeid:24,uuid:"27EAB12A-818B-4170-8A19-8BBD3602E7D7"}
 */
function nfx_isProgram()
{
	return true;
}
