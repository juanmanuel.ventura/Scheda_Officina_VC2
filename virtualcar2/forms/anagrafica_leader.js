/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D00579A1-4376-4A9C-95E2-5484EED0CB19"}
 */
var nfx_orderBy = "leader asc";

/**
 * @properties={typeid:24,uuid:"44E8BC77-4A67-4CCE-95C0-5ABF50CB3CA4"}
 */
function nfx_defineAccess(){
	return [false,false,false,true];
}
