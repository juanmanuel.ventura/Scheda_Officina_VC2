/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"8ED74391-ED85-4C09-8D75-92E2684C97B3"}
 */
var nfx_orderBy = "progetto asc, f_riferimento";

/**
 * @properties={typeid:35,uuid:"A781C52A-62CA-449C-B6FA-91A780465FF1",variableType:-4}
 */
var nfx_related = ["anagrafica_missioni_km_table","anagrafica_missioni_note_record"];

/**
 *
 * @properties={typeid:24,uuid:"1029422F-3ED0-43AE-BEFF-611A65D32D86"}
 */
function nfx_defineAccess()
{
	return [true, true, true];
}
