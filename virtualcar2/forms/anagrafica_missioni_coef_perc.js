/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"8963056B-EE7B-4ABE-B897-6DE16C23B4F9"}
 */
var nfx_orderBy = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"7CEA8106-3C9A-447C-9997-300FF41AE3BB"}
 */
var nfx_related = null;

/**
 *
 * @properties={typeid:24,uuid:"810D86D3-C4D6-4E70-9719-8BD6BCED5258"}
 */
function nfx_defineAccess()
{
	return [true,true,true];

}
