/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A22CDEE4-A823-41EA-A652-7EF06B484593"}
 */
var nfx_orderBy = "coefficiente asc";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C05EB874-5BB3-460F-AF52-FC4FBFDDB999"}
 */
var nfx_related = null;

/**
 *
 * @properties={typeid:24,uuid:"538F3192-70C3-4E46-B9C1-1E5D30275ACB"}
 */
function nfx_defineAccess()
{
	return [false,false,false];

}
