/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"8459211F-CF2F-40E0-A419-8A7013FCD8E6"}
 */
var nfx_orderBy = "percorso asc";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"E1E12516-F63D-4A3D-9E08-DCE3DDC73B97"}
 */
var nfx_related = null;

/**
 *
 * @properties={typeid:24,uuid:"AD085579-90FB-4D83-9527-AE3507672434"}
 */
function nfx_defineAccess()
{
	return [false,false,false,true];

}
