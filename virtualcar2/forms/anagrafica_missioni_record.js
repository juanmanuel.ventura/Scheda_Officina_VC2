/**
 *
 * @properties={typeid:24,uuid:"ED97B465-7D95-46B8-86C0-474E824740CA"}
 */
function calcolaCoefficienti() {
	try{
	calcolaMissioniCoefPerc();
	calcolaMissioniCoefficienti();
	plugins.dialogs.showInfoDialog("Operazione completata", "Generazione coefficienti completata con successo", "Ok");
	}catch (Exc){
		application.output(Exc.toString());
		plugins.dialogs.showErrorDialog("Errore","Operazione fallita");
		var pb = forms.nfx_interfaccia_popup_progress.elements.progress;
		pb.getValue() == pb.getMaximum();
		globals.nfx_progressBarStep();
		databaseManager.revertEditedRecords();
		return;
		
	}
}

/**
 *
 * @properties={typeid:24,uuid:"687B03EE-259F-487F-AADB-EE57DF134F5B"}
 */
function calcolaMissioniCoefficienti() {
	//	var fsCoefficienti = databaseManager.getDataSetByQuery(controller.getServerName(),"select coefficiente from k8_coefficienti order by coefficiente",[],-1);
	//	var countCoefficienti = databaseManager.getDataSetByQuery(controller.getServerName(),"select count(*) from k8_coefficienti",[],1);

	var fsCoefficienti = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), "select coefficiente from k8_coefficienti order by coefficiente", [], -1);
	var countCoefficienti = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), "select count(*) from k8_coefficienti", [], 1);

	if (k8_missioni_to_k8_missioni_coefficienti.getSize() > 0) {
		k8_missioni_to_k8_missioni_coefficienti.deleteAllRecords();
	}
	globals.nfx_progressBarShow(countCoefficienti.getValue(1, 1) - 1);
	for (var i = 1; i <= countCoefficienti.getValue(1, 1); i++) {
		forms.anagrafica_missioni_coefficienti.controller.newRecord();
		forms.anagrafica_missioni_coefficienti.progetto = progetto;
		forms.anagrafica_missioni_coefficienti.missione = missione;
		if(fsCoefficienti.getValue(i, 1))
		forms.anagrafica_missioni_coefficienti.coefficiente = fsCoefficienti.getValue(i, 1);
		else application.output("fsCoefficienti.getValue(i, 1) PERSO");

		var valore = 0;
		for (var j = 1; j <= forms.anagrafica_missioni_coefficienti.k8_missioni_coefficienti_to_k8_missioni_coef_perc.getSize(); j++) {
			forms.anagrafica_missioni_coefficienti.k8_missioni_coefficienti_to_k8_missioni_coef_perc.setSelectedIndex(j);
			if (forms.anagrafica_missioni_coefficienti.k8_missioni_coefficienti_to_k8_missioni_coef_perc.valore) {
				valore += forms.anagrafica_missioni_coefficienti.k8_missioni_coefficienti_to_k8_missioni_coef_perc.valore;
			}
		}
		forms.anagrafica_missioni_coefficienti.totale_missione = valore;

		if (!f_riferimento) {
			var query = "select MC.TOTALE_MISSIONE from  K8_MISSIONI_COEFFICIENTI MC inner join K8_MISSIONI M on MC.PROGETTO = M.PROGETTO and MC.MISSIONE = M.MISSIONE where MC.PROGETTO = ? and MC.COEFFICIENTE = ? and M.F_RIFERIMENTO is not NULL";
			var args = [progetto, fsCoefficienti.getValue(i, 1)];
			//			JStaffa
			//			var ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,1);
			var ds = databaseManager.getDataSetByQuery(controller.getDataSource().split('/')[1], query, args, 1);

			forms.anagrafica_missioni_coefficienti.scarto_riferimento = valore / ds.getValue(1, 1);
		} else {
			forms.anagrafica_missioni_coefficienti.scarto_riferimento = null;
		}
		databaseManager.saveData();
		globals.nfx_progressBarStep();
	}
	databaseManager.refreshRecordFromDatabase(k8_missioni_to_k8_missioni_coefficienti, -1);
}

/**
 * @properties={typeid:24,uuid:"7B2C9FCC-E159-45BE-AB2E-9B2D5D47CF08"}
 */
function calcolaMissioniCoefPerc() {
//	JStaffa deprecato sostituito con quello sotto
//	var fsCoef_Perc = databaseManager.getFoundSet(controller.getServerName(),"k8_jt_coefficienti_percorsi");
//	/** @type {JSFoundSet} */
//	var fsCoef_Perc = databaseManager.getFoundSet(databaseManager.getDataSourceServerName(controller.getDataSource()),"k8_jt_coefficienti_percorsi");

	/** @type {JSFoundSet<db:/ferrari/k8_jt_coefficienti_percorsi>} */
	var fsCoef_Perc = databaseManager.getFoundSet('ferrari', 'k8_jt_coefficienti_percorsi');
	fsCoef_Perc.loadAllRecords();
	var countCoef_Perc = databaseManager.getFoundSetCount(fsCoef_Perc);

	globals.nfx_progressBarShow(countCoef_Perc - 1);
	if (k8_missioni_to_k8_missioni_coef_perc.getSize() > 0) {
		k8_missioni_to_k8_missioni_coef_perc.deleteAllRecords();
	}
	for (var i = 1; i <= countCoef_Perc; i++) {
		fsCoef_Perc.setSelectedIndex(i);
		k8_missioni_to_k8_missioni_coef_perc.newRecord();
		if(fsCoef_Perc.coefficiente)
		k8_missioni_to_k8_missioni_coef_perc.coefficiente = fsCoef_Perc.coefficiente;
		else application.output("fsCoef_Perc.coefficiente PERSO");
		if(fsCoef_Perc.percorso)
		k8_missioni_to_k8_missioni_coef_perc.percorso = fsCoef_Perc.percorso;
		else application.output("fsCoef_Perc.percorso PERSO");
		databaseManager.saveData();
		globals.nfx_progressBarStep();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"4C562048-BBB2-4C6A-9FC4-829A2C55BBF1"}
 */
function nfx_getTitle() {
	return "Anagrafica Missioni";
}

/**
 *
 * @properties={typeid:24,uuid:"390E8DCA-1BD7-40E4-800F-1F0BCC75D900"}
 */
function nfx_isProgram() {
	return true;
}

/**
 *
 * @properties={typeid:24,uuid:"86089161-648C-4F08-AFFB-0EA1EB2E9467"}
 */
function nfx_sks() {
	return [{ icon: "warning.png", tooltip: "!!!BETA!!! - Calcola coefficienti", method: "calcolaCoefficienti" }];
}
