/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"EAD0030C-371B-498E-958B-F685CF3D5041"}
 */
var nfx_orderBy = "percorso";

/**
 * @properties={typeid:35,uuid:"A249C5B5-7105-4948-AB8D-BC86F021852F",variableType:-4}
 */
var nfx_related = null;

/**
 *
 * @properties={typeid:24,uuid:"9EA627DF-C17E-460B-83EC-D711D8DDFECF"}
 */
function nfx_defineAccess()
{
	return [true, true, true];
}
