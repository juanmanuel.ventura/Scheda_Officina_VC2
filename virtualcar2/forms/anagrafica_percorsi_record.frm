dataSource:"db:/ferrari/k8_percorsi",
extendsID:"1A1AF9F9-7326-4BAF-88CA-A4FBB8D34153",
items:[
{
labelFor:"percorso",
location:"10,10",
mediaOptions:14,
size:"60,20",
styleClass:"form",
tabSeq:-1,
text:"Percorso",
transparent:true,
typeid:7,
uuid:"03F7C275-9C3B-47AD-B716-BB64AF59D70A"
},
{
labelFor:"descrizione",
location:"100,10",
mediaOptions:14,
size:"80,20",
styleClass:"form",
tabSeq:-1,
text:"Descrizione",
transparent:true,
typeid:7,
uuid:"13B3127D-84C1-4814-BA6B-00A9FE891B20"
},
{
anchors:3,
labelFor:"colore_grafici",
location:"640,10",
mediaOptions:14,
size:"80,20",
styleClass:"form",
tabSeq:-1,
text:"Colore grafici",
transparent:true,
typeid:7,
uuid:"81170E69-4204-4EA9-A9EC-CADA4548F0A5"
},
{
borderType:"EtchedBorder,0,null,null",
dataProviderID:"percorso",
horizontalAlignment:0,
location:"10,30",
name:"percorso",
size:"80,20",
text:"Percorso",
typeid:4,
uuid:"8FADDE51-0093-4DDB-8B85-29FD40352168"
},
{
height:55,
partType:5,
typeid:19,
uuid:"982D441B-6740-415B-90C9-6E59E5766A4A"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"descrizione",
location:"100,30",
name:"descrizione",
size:"530,20",
text:"Descrizione",
typeid:4,
uuid:"9F3FC90B-CACF-460E-AD6C-06D00B42FA5A"
},
{
anchors:3,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"colore_grafici",
location:"640,30",
name:"colore_grafici",
size:"140,20",
text:"Colore Grafici",
typeid:4,
uuid:"BFDD78F7-67BD-4AF7-A6A8-BAD873E73C8A"
}
],
name:"anagrafica_percorsi_record",
paperPrintScale:100,
scrollbars:4,
showInMenu:true,
size:"794,55",
styleName:"keeneight",
typeid:3,
uuid:"F6E49062-93D9-471B-A6D8-C5E293632B30"