dataSource:"db:/ferrari/k8_percorsi",
extendsID:"1A1AF9F9-7326-4BAF-88CA-A4FBB8D34153",
items:[
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"colore_grafici",
location:"640,30",
name:"colore_grafici",
size:"140,20",
styleClass:"table",
text:"Colore Grafici",
typeid:4,
uuid:"11F0BB4E-C985-453B-9E15-5C89BF3DA6D6"
},
{
anchors:3,
labelFor:"colore_grafici",
location:"640,10",
mediaOptions:14,
size:"110,20",
styleClass:"table",
tabSeq:-1,
text:"Colore grafici",
typeid:7,
uuid:"21B9A9A4-F65B-477A-9930-647D51D44B0C"
},
{
height:480,
partType:5,
typeid:19,
uuid:"74B5C776-0664-4DB2-83C7-2C7307852D47"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"descrizione",
location:"90,30",
name:"descrizione",
size:"540,20",
styleClass:"table",
text:"Descrizione",
typeid:4,
uuid:"7D84D4F8-BC82-43DD-A7E7-949C01BF1B11"
},
{
labelFor:"descrizione",
location:"90,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Descrizione",
typeid:7,
uuid:"A75E0399-4D5E-4C57-8E8E-A71377B5DE2F"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"percorso",
horizontalAlignment:0,
location:"10,30",
name:"percorso",
size:"70,20",
styleClass:"table",
text:"Percorso",
typeid:4,
uuid:"B01C6B14-2E1F-4B93-9AA3-D22AF9083288"
},
{
labelFor:"percorso",
location:"10,10",
mediaOptions:14,
size:"70,20",
styleClass:"table",
tabSeq:-1,
text:"Percorso",
typeid:7,
uuid:"E6292BE5-9DFD-4204-B0AC-3321E7A989C5"
}
],
name:"anagrafica_percorsi_table",
paperPrintScale:100,
showInMenu:true,
size:"794,480",
styleName:"keeneight",
typeid:3,
uuid:"6033ABA0-1EAA-44D9-9FE2-D5287C78750B",
view:3