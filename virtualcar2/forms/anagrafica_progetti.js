/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F2C3C58E-6A7C-4CB8-958E-CD44038D0AE0"}
 */
var nfx_orderBy = "progetto desc";

/**
 * @properties={typeid:35,uuid:"C1AFA987-9BFC-49D2-AA6F-386501D03687",variableType:-4}
 */
var nfx_permissions = [true, true, true];

/**
 * @properties={typeid:35,uuid:"995D9168-7D38-4D51-9C14-E02CA208D267",variableType:-4}
 */
var nfx_related = ["anagrafica_progetti_ver_table","anagrafica_vetture_table","date_riferimento_table","anagrafica_leader_table","documenti_record","time_line_table","tag_table"];

/**
 *
 * @properties={typeid:24,uuid:"AF5B7AA5-7E54-4D36-BD59-29029CD09E08"}
 */
function nfx_defineAccess(){
	return [true, true, true];
}

/**
 * @properties={typeid:24,uuid:"B92808CD-6B46-4386-A5A8-A2BFC3917D0C"}
 */
function nfx_sks(){
	return [{icon: "gear.png.png", 	tooltip:"Gestisci esprimenti", 	method:"openExperimentManagement"}]; 
}

/**
 * @properties={typeid:24,uuid:"65A0C989-31CF-421A-8F94-1B0C2528103B"}
 */
function openExperimentManagement(){
	forms._vc2_experimentManager_container.openPopup();
}
