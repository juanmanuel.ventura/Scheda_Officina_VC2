/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"7BFF6C2E-B271-490A-821D-350D69E00C01"}
 */
var nfx_orderBy = "codice_vettura asc";

/**
 *
 * @properties={typeid:24,uuid:"BD06585E-2D6E-4038-8748-D2F774E08EC1"}
 */
function nfx_defineAccess()
{
	return [true, true, true];
}
