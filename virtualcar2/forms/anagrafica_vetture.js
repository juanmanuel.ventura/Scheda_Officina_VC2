/**
 * @properties={typeid:35,uuid:"837D8C1C-0478-4F6A-970C-BAC8A7C7F70E",variableType:-4}
 */
var cs = { icon: "smartfolder.png", tooltip: "Crea schede collaudo", method: "creaSchedeCollaudo" };

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"AFE84035-1E5D-40F9-A67B-08FF3F8D666E"}
 */
var nfx_orderBy = "progetto desc, codice_vettura desc";

/**
 * @properties={typeid:35,uuid:"2D8A3110-683A-4B40-AC53-EE7BB1AA7087",variableType:-4}
 */
var nfx_related = ["pianificazione_micro_table", "documenti_record", "time_line_table", "tag_table"];

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"5259DD98-7286-4BBE-A4EF-55C6DDB45510",variableType:4}
 */
var turn = 0;

/**
 *
 * @properties={typeid:24,uuid:"64A5793C-F0E4-4A32-B0CE-717A4073E0E2"}
 * @AllowToRunInFind
 */
function creaSchede(form) {
	//FS
	//corpo creazione schede
	databaseManager.saveData();
	//application.output('form.getFoundset ' + form.getFoundset());
	//var fs = new foundset(form.getFoundset());
	/** @type {JSFoundSet<db:/ferrari/k8_pianificazione_micro>} */
	var fs = form.foundset.duplicateFoundSet();

	if (fs.find()) {
		fs.crea_scheda = 1;
		fs.scheda_creata = 0;
		fs.search();
	}
	var ok = 0;
	for (var i = 1; i <= fs.getSize(); i++) {
		var fs_rec = fs.getRecord(i);
		var result = forms.schede_collaudo.createScheda(fs_rec);
		if (result != -1) {
			fs_rec.scheda_creata = 1;
			ok++;
		}
	}
	plugins.dialogs.showInfoDialog("Completato", ok + " schede su " + fs.getSize() + " create correttamente", "Ok");

	form.disableCheck();
	forms.nfx_interfaccia_pannello_buttons.enableNavigationTree();
	forms.nfx_interfaccia_pannello_buttons.enableCorrectButtons();
}

/**
 *
 * @properties={typeid:24,uuid:"08C94A4B-6BB4-4BBC-96A6-2C1C74C3EAEB"}
 */
function creaSchedeCollaudo() {
	if (forms.nfx_interfaccia_pannello_body.relatedSelected == forms.pianificazione_micro_table.controller.getName()) {
		var form = forms.pianificazione_micro_table;

		if (turn == 0) {
			selezionaSchede(form);
			turn = 1;
		} else {
			creaSchede(form);
			turn = 0;
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"A44A2371-4561-4E0B-8BF2-491F87EBE049"}
 */
function nfx_defineAccess() {
	return [true, true, true];
}

/**
 *
 * @properties={typeid:24,uuid:"20B1893F-E767-4729-A1AB-84370224FFC3"}
 */
function nfx_sks() {
	return [cs];
}

/**
 *
 * @properties={typeid:24,uuid:"A1CB33F8-B9FB-48E9-9B74-D68AE5813B32"}
 */
function selezionaSchede(form) {

	form.enableCheck();
	forms.nfx_interfaccia_pannello_buttons.disableNavigationTree();
	forms.nfx_interfaccia_pannello_buttons.disableButtons(["sk0"]);
	plugins.dialogs.showInfoDialog("Info", "Selezionare le pianificazioni desidarate e ripremere il bottone per avviarne la creazione", "Ok");
}
