/**
 *
 * @properties={typeid:24,uuid:"0B5CF767-E4C3-4A29-BF4F-4D76A8E52805"}
 */
function nfx_defineAccess()
{
	return [true,true,true];
}

/**
 *
 * @properties={typeid:24,uuid:"1B8166C5-9FBC-4474-B503-007BEC9EADEA"}
 */
function nfx_isProgram()
{
	return false;
}
