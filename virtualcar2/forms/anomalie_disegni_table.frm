dataSource:"db:/ferrari/k8_anomalie_disegni",
extendsID:"B7C3FB20-2497-4F62-B6C9-288E5585AEC6",
items:[
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"note",
location:"640,30",
name:"note",
size:"400,20",
styleClass:"table",
text:"Note",
typeid:4,
uuid:"26E1A007-F6B8-415C-9265-3275958C3050"
},
{
labelFor:"note",
location:"640,10",
mediaOptions:14,
size:"160,20",
styleClass:"table",
tabSeq:-1,
text:"Note",
typeid:7,
uuid:"4CC0190E-F188-4E4B-904F-09396D0B1981"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"k8_anomalie_disegni_to_k8_anagrafica_disegni.descrizione",
location:"280,30",
name:"descrizione",
size:"350,20",
styleClass:"table",
text:"Descrizione",
typeid:4,
uuid:"6467209F-19BD-44C5-AD07-1B4D9B5D9B8D"
},
{
labelFor:"descrizione",
location:"280,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Descrizione",
typeid:7,
uuid:"6E69C0F0-F17A-4A39-A515-79DFFE4886BD"
},
{
labelFor:"esponente",
location:"140,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Esponente",
typeid:7,
uuid:"B127C73D-94AD-413D-9C3C-7E0B13FF94E9"
},
{
labelFor:"numero_disegno",
location:"10,10",
mediaOptions:14,
size:"110,20",
styleClass:"table",
tabSeq:-1,
text:"N° disegno",
typeid:7,
uuid:"C86D87E9-0740-4490-933D-05B349EF0DBD"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"esponente",
location:"140,30",
name:"esponente",
size:"130,20",
styleClass:"table",
text:"Esponente",
typeid:4,
uuid:"D84333D7-EB62-4A62-A4E7-79F1F431C15D"
},
{
height:480,
partType:5,
typeid:19,
uuid:"DA8B8678-633A-4A73-9015-F6DEE75ADB35"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"numero_disegno",
location:"10,30",
name:"numero_disegno",
size:"120,20",
styleClass:"table",
text:"Numero Disegno",
typeid:4,
uuid:"DE7CC78E-9A26-4776-B74F-9AF779E30982"
}
],
name:"anomalie_disegni_table",
paperPrintScale:100,
showInMenu:true,
size:"1050,480",
styleName:"keeneight",
typeid:3,
uuid:"7D8F9817-9CE5-4AE7-BC68-4B42E7D4E162",
view:3