/**
 *
 * @properties={typeid:24,uuid:"4EC52D37-2B0E-441D-98EA-CCFA4DE935A2"}
 */
function nfx_defineAccess()
{
	return [true,true,true];
}

/**
 *
 * @properties={typeid:24,uuid:"6BC7DC16-76DA-4443-98B0-F2DBB5B5BBBB"}
 */
function nfx_isProgram()
{
	return false;
}
