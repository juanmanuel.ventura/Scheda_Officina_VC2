/**
 *
 * @properties={typeid:24,uuid:"97BC7EF4-4235-444D-88EF-F8427450E611"}
 */
function nfx_defineAccess()
{
	return [true,true,true];
}

/**
 *
 * @properties={typeid:24,uuid:"A3AF9748-2C5A-4B8E-B3F7-01DB85149B15"}
 */
function nfx_isProgram()
{
	return false;
}
