/**
 *
 * @properties={typeid:24,uuid:"57A95474-596A-43B0-ABEC-BBA22A837A3B"}
 */
function nfx_defineAccess()
{
	return [true,true,true];
}

/**
 *
 * @properties={typeid:24,uuid:"85F7AB2F-4131-4231-B4BE-A942426165C0"}
 */
function nfx_isProgram()
{
	return false;
}
