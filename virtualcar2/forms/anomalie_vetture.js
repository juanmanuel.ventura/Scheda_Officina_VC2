/**
 *
 * @properties={typeid:24,uuid:"0D8BC94F-20D8-44E7-A580-EE2153E5D779"}
 */
function nfx_defineAccess()
{
	return [true,true,true];
}

/**
 *
 * @properties={typeid:24,uuid:"5B75651E-D074-4C2D-89DF-A5FCF32D5625"}
 */
function nfx_isProgram()
{
	return false;
}
