dataSource:"db:/ferrari/k8_release_notes",
items:[
{
dataProviderID:"version_number",
fontType:"Verdana,0,18",
foreground:"#ffffff",
location:"300,30",
mediaOptions:14,
size:"90,40",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"001C89D6-DB01-441D-8983-D85129E27EFC"
},
{
beanClassName:"javax.swing.JLabel",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_23\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JLabel\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>160<\/int> \
    <int>30<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>copyrightLabel<\/string> \
  <\/void> \
  <void property=\"text\"> \
   <string>Keeneight S.r.l. 2009 - 2011<\/string> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"20,450",
name:"copyrightLabel",
size:"160,30",
typeid:12,
uuid:"083D4479-93A6-4C74-9C2E-A253E396F316"
},
{
imageMediaID:"04BC7326-F6F9-4B87-9E4E-3FC4BFDBA1E5",
location:"20,20",
mediaOptions:14,
size:"270,36",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"20B87A3C-D941-463A-AB92-659B4F99B1C4"
},
{
imageMediaID:"CF960B4A-D99B-436C-B5FA-F29E3ABE51E4",
location:"30,70",
mediaOptions:14,
size:"270,250",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"251C0624-772F-4583-B059-D6291CE41C32"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"timestamp_creazione",
editable:false,
foreground:"#000000",
format:"dd-MM-yyyy",
horizontalAlignment:0,
location:"390,120",
name:"timestamp_creazione",
size:"140,20",
text:"Timestamp Creazione",
transparent:true,
typeid:4,
uuid:"25C53EF6-E369-4503-8211-37884FC9F82B"
},
{
anchors:15,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"release_notes_text",
displayType:7,
editable:false,
foreground:"#000000",
location:"310,150",
name:"release_notes_text",
scrollbars:32,
size:"320,320",
text:"Release Notes Text",
transparent:true,
typeid:4,
uuid:"4FD79C93-3BA2-4CE1-B9E7-75FEA7AD5ABB"
},
{
anchors:15,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"globals.vc2_testMessage",
displayType:7,
editable:false,
location:"0,120",
name:"vc2_testMessage",
size:"640,240",
text:"Vc2 Testmessage",
transparent:true,
typeid:4,
uuid:"7B617AFC-BBF2-4DE4-80A9-E43498812754"
},
{
foreground:"#000000",
location:"310,120",
mediaOptions:14,
size:"120,20",
tabSeq:-1,
text:"Ultimo Update:",
transparent:true,
typeid:7,
uuid:"B342153A-365E-4DA5-B50B-3BDAFB2C3CFD"
},
{
background:"#c0c0c0",
height:480,
partType:5,
typeid:19,
uuid:"D3AE69B4-3D43-4608-A341-01684FFB95F3"
}
],
name:"application_main_form",
navigatorID:"-1",
onHideMethodID:"7621013B-45FC-454C-88BE-EF2DC5A5C0E0",
onRecordSelectionMethodID:"2859D255-6F8C-4ACE-B502-1E941A5C31EF",
onSearchCmdMethodID:"A4E5EAFC-62DD-48C2-B314-BC9A9204E61B",
onShowMethodID:"BCCAF39F-5866-42AB-9E3E-7563D9D8EA46",
paperPrintScale:100,
showInMenu:true,
size:"640,480",
styleName:"keeneight",
typeid:3,
uuid:"2F50B1DD-3C37-4B0A-A2F1-2A31E5075E14"