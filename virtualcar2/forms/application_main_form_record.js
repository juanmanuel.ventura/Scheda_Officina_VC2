/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"614EFDCC-E2D7-413E-ADF6-87FD57A7BEEC"}
 */
var nfx_orderBy = "timestamp_creazione desc";

/**
 * @properties={typeid:24,uuid:"0A0E6981-94CE-4D26-9A0D-762DA76A3A0F"}
 */
function nfx_defineAccess(){
	return [true, false, true];
}

/**
 *
 * @properties={typeid:24,uuid:"22D1B78A-74EC-4150-BDDA-1422B2FF3C37"}
 */
function nfx_getTitle(){
	return "Gestione Aggiornamenti";
}

/**
 *
 * @properties={typeid:24,uuid:"0F58D8F5-25E0-47BC-96E2-DEBD9BED55FD"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"2F77DA65-5188-4CB6-8D62-FBFDA6D5BCF6"}
 */
function nfx_defaultPermissions(){
	return "^";
}
