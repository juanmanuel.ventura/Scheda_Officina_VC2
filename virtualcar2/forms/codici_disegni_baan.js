/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"43EAC7E8-4EC9-408E-803C-BEC592AB36DA"}
 */
var nfx_orderBy = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"036BCEA1-4B40-4A2D-9EE4-7EFD97F81989"}
 */
var nfx_related = null;

/**
 *
 * @properties={typeid:24,uuid:"7FB9B683-51F3-4ED0-859B-6EF405C3C153"}
 */
function nfx_defineAccess()
{
	return [false,false,false,true];

}

/**
 *
 * @properties={typeid:24,uuid:"93107236-9467-450C-87A6-3D4DE1D9CC5E"}
 */
function README()
{
	/*
	 * Questo è il prototipo di un form per NFX.
	 * Ciò che è NECESSARIO definire in questo tipo di form è:
	 * 
	 * - nfx_orderBy: è una variabile che definisce l'ordinamento iniziale del foundset del form, va
	 *   specificato in una stringa nel formato "nome_campo1 asc/desc, nome_campo2 asc/desc,...,nome_campoN asc/desc"
	 * - nfx_related: è un array di stringe in cui vanno specificati i nomi degli eventuali form correlati
	 *   a quello corrente nel formato ["nome_form1","nome_form2",...,"nome_formN"]
	 * - nfx_defineAccess: questo metodo definisce se l'utente può aggiungere/togliere/modificare
	 *   record, in più aggiungendo un utimo "true" si andrà ad overridare il metodo nfx_defineAccess
	 *   dei form a questo correlati, impedendo qualsiasi operazione
	 * 
	 * É ora possibile definire form "figli" che estendono quello appena creato ereditandone le caratteristiche,
	 * in questi form (se non ci sono vanno definiti qui) è NECESSARIO definire:
	 * 
	 * - nfx_getTitle: è un metodo che torna una stringa che sarà il "titolo" del form nell'albero di navigazione, nei
	 *   tabpanel e in tutte quelle situazione in cui serve un "nome umano"
	 * - nfx_isProgram: è un metodo che torna un booleano che identifica la possibilità di inserire il form
	 *   in questione nell'albero di navigazione
	 * 
	 * Vi è inoltre la possibilità di definere numerosi metodi atti a personalizzare il comportamento di NFX per adeguarsi
	 * alle più disparate esigenze. Purtroppo non esiste ancora una lista completa di questi metodi, ma per un'idea generale
	 * rimando alla lettura dei file globals.js e nfx_interfaccia_pannello_buttons_method.js, dove è possibile ritrovare
	 * le chiamate a questi metodi e un breve commento agli stessi.
	 */
}
