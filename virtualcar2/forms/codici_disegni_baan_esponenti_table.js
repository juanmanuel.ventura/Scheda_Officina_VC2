/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"70E21923-AEEE-4FA0-B32C-9CC8A1577524"}
 */
var nfx_orderby = "esponente desc";

/**
 * @properties={typeid:24,uuid:"00245941-5617-4223-897B-8949E562EE64"}
 */
function nfx_getTitle(){
	return "Codici BaaN";
}
