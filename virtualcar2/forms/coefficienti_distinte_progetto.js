/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"10B12E49-170C-45BC-94FD-4039A6136947"}
 */
var nfx_orderBy = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"E5A95089-4C15-4BA1-9418-826ED1CA7D64"}
 */
var nfx_related = null;

/**
 *
 * @properties={typeid:24,uuid:"744DFED5-F177-440B-A470-DB68C8EBAE87"}
 */
function nfx_defineAccess(){
	return [false,false,false];
}

/**
 *
 * @properties={typeid:24,uuid:"C05BFC4C-9C47-40B7-8D99-0C7215132676"}
 */
function nfx_relationModificator(){
	if(forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero.controller.getName()){
		return "alberoprogetti";
	}else{
		return null;
	}
}
