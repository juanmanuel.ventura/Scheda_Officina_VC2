/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C468B9BD-626E-454D-8BF0-CA749C8E77C7"}
 */
var nfx_orderBy = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"BC8EB200-7289-4926-ACEF-C93D4484FA67"}
 */
var nfx_related = null;

/**
 *
 * @properties={typeid:24,uuid:"4AE03161-127F-486F-A639-813A5B557410"}
 */
function nfx_defineAccess()
{
	return [false,false,false];

}
