/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1BC8510F-9658-4D04-8A64-D2DD2BED4416"}
 */
var field = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"68091F90-6C59-4E5C-8A53-7620D6A2DFDF"}
 */
var form = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"96A489AF-5573-42B0-8C79-8969DF4428E2"}
 */
var group = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"EB128821-2D83-40E0-A757-8651C8EAE5FA"}
 */
var model = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"9BEEA06B-B262-46B8-A255-D4DA4CCFB17F"}
 */
var smart = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F68E5139-3895-443A-ADF0-427FF1E0D3BF"}
 */
var subgroup = null;

/**
 * @properties={typeid:35,uuid:"E6973B71-C83A-4F32-AB26-010418230D39",variableType:-4}
 */
var filters = null;

/**
 * @properties={typeid:35,uuid:"11A7C904-1222-4FDA-A037-C4DCD9DF4790",variableType:-4}
 */
var check = null;

/**
 * @properties={typeid:24,uuid:"8A931F77-CB0A-4859-8A19-8C3D46196A26"}
 */
function openPopup(m,fo,fi){
	if(model != m || form != fo || field != fi){
		form = fo;
		field = fi;
		model = m;
		group = "";
		subgroup = "";
		smart = "";
		filterData();
	}
	controller.readOnly = true;
	elements.group.readOnly = false;
	elements.subgroup.readOnly = false;
	elements.smart.readOnly = false;
	controller.show("components");
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 *
 * @properties={typeid:24,uuid:"4D702850-938D-4632-8221-0BB9BD7F0C85"}
 */
function onGroupChange(oldValue, newValue, event) {
	globals.vc2_componentiCurrentGroup = newValue;
	subgroup = "";
	filterData();
}

/**
 * @properties={typeid:24,uuid:"0C168F57-C01D-4E98-861B-E961BBEB3D2C"}
 * @AllowToRunInFind
 */
function filterData(){
	filters = {codice_modello	:	model,
	           gruppo			:	group,
	           sottogruppo		:	subgroup};
	
	if(foundset.find()){
		for(var f in filters){
			if(filters[f]){
				foundset[f] = filters[f];
			}
		}
		
		if(smart){
			try{
				check = java.lang.Integer.parseInt(smart);
				foundset.codice = "%" + smart + "%";
			}catch(e){
				foundset.descrizione = "#%" + smart + "%";
			}
		}
		foundset.search();
	}
}

/**
 * @properties={typeid:24,uuid:"6B6E5B88-FA85-48F5-90CF-3246E03361A4"}
 */
function saveAndClose(){
	forms[form][field] = codice;
	
	//application.closeFormDialog();
	
	var w=controller.getWindow();
	w.destroy();
}
