/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"0078FDC5-750E-452D-A403-CAC9D2134FAB"}
 */
var field = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"608D68BF-7FC8-4809-9CC3-6BD7EF42D218"}
 */
var form = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"6CC8BDCB-9718-43C4-AAE3-084A3B8B8C37"}
 */
var smart = "";

/**
 * @properties={typeid:24,uuid:"7E3B4DBC-6883-4AF2-9A58-F79A5041AEEC"}
 */
function openPopup(fo, fi) {
	setData(fo, fi);
	controller.show("difetti");
}

/**
 * @properties={typeid:24,uuid:"D2DBBB74-3456-4028-B6E2-7E63F1354FCF"}
 */
function setData(fo, fi) {
	if (form != fo || field != fi) {
		form = fo;
		field = fi;
		smart = "";
		filterData();
	}
	controller.readOnly = true;
	elements.smart.readOnly = false;

	var mode = forms.nfx_interfaccia_pannello_buttons.mode;
	elements.choose.enabled = (mode == "add" || mode == "edit") ? true : false;
}

/**
 * @properties={typeid:24,uuid:"5A687891-F878-4E0F-A9B4-6A15EE0C8409"}
 * @AllowToRunInFind
 */
function filterData() {
	if (foundset.find()) {
		foundset.descrizione = "#%" + smart + "%";
		if (!foundset.search() && foundset.find()) {
			foundset.codice_difetto = smart;
			foundset.search();
		}
	}
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"9A32D112-9794-45EC-B4C2-A3DA5C1B0286"}
 */
function save(event) {
	forms[form][field] = codice_difetto;
	//	FS
	//if(application.isFormInDialog(controller.getName())){
	if (application.getActiveWindow().getType() == JSWindow.DIALOG) {
		//application.output("START defect_chooser.save ",LOGGINGLEVEL.DEBUG);
		//application.closeFormDialog("difetti");
		//dep	var _context = forms["difetti"].controller.getFormContext();
		var _context = forms["defect_chooser"].controller.getFormContext();
		/** @type {String} */
		var _window = _context.getValue(1, 1);
		//application.output("context form "+controller.getName() +" "+_window);
		if (application.getWindow(_window)) {
			var _windowObject = application.getWindow(_window);
			_windowObject.hide();
		}
	}
	//application.output("STOP defect_chooser.save ",LOGGINGLEVEL.DEBUG);
}
