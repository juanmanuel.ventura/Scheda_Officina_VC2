/**
 * @properties={typeid:35,uuid:"7CDE55BD-0A57-47A3-BFBE-C5314BDDBAE6",variableType:-4}
 */
var nfx_related = ["distinte_progetto_dettaglio_record","codici_disegni_baan_esponenti_table","tag_table", "delibere_table", "segnalazioni_anomalie_table", "schede_anomalie_table", "schede_modifica_table", "soluzioni_tampone_cicli_record", "distinte_vetture_table", "distinte_progetto_children_table", "distinte_progetto_note_record"];

/**
 *
 * @properties={typeid:24,uuid:"3F35BC26-CFC6-435E-80D1-7AD580329702"}
 * @AllowToRunInFind
 */
function nfx_customExport()
{
	if(plugins.dialogs.showQuestionDialog("AS400","Si desidera esportare una distinta base in formato as400?","Sì","No") == "No")
	{
		return "continue";
	}
	else
	{
		//FS
		
//		var dp = databaseManager.getFoundSet(controller.getServerName(),controller.getTableName());
		var dp = databaseManager.getFoundSet(databaseManager.getDataSourceServerName(controller.getDataSource()),databaseManager.getDataSourceTableName(controller.getDataSource()));

		if(dp.find())
		{
			dp.pogetto = progetto;
			dp.codice_vettura = codice_vettura;
			dp.search();
		}
		dp.sort("livello asc");
		var count = databaseManager.getFoundSetCount(dp);
		if(plugins.dialogs.showQuestionDialog("Export","L'estrazione dei " + count + " record richiesti potrebbe richiedere molto tempo, continuare?","Sì","No") == "No")
		{
			return null;
		}
		var buffer = "SLVDIS\tSCDDIS\tSDEDIS\tSESPON\n";
		globals.nfx_progressBarShow(count);
		//dp.getRecord(count);
		for(var i=1;i<=count;i++)
		{
			dp.setSelectedIndex(i);
			buffer += dp.livello + "\t" + dp.numero_disegno + "\t" + dp.descrizione + "\t" + dp.esponente + "\n";
			globals.nfx_progressBarStep();
		}
		globals.utils_saveAsTXT(buffer,"DB_AS400_" + progetto + ".xls");
		return null;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"8E71DB5A-7B98-44BB-B110-D05FC25153A8"}
 */
function nfx_defineAccess()
{
	return [true,true,true];
}

/**
 *
 * @properties={typeid:24,uuid:"8F8FC573-80A1-49EF-B656-C044220E0DE2"}
 */
function nfx_relationModificator()
{

	if(forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero.controller.getName())
	{
		return "alberoprogetti";
	}
	else
	{
		return null;
	}

}
