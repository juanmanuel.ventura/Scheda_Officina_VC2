/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"14AFDCE8-94AB-4D09-BDF7-D51C89B6D4AA"}
 */
var buffer = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F7E63FEA-7AD8-4F07-9F02-BC14F0728CDF"}
 */
var filterDescription = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"5959EA7F-968C-45F1-A72D-1AF40B02230A"}
 */
var filterDraw = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"16AFE471-EF3E-4A2A-BD22-BEE154C9252D"}
 */
var filterTag = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"152D704D-0234-4D2B-BEA3-2ABF4CA9D0D6"}
 */
var initialLevel = null;

/**
 * @properties={typeid:35,uuid:"69DEF8D7-9551-4865-BF1E-E2F5DA98E689",variableType:-4}
 */
var nfx_related = ["distinte_progetto_dettaglio_record", "tag_table", "delibere_table", "distinte_progetto_albero_segnalazioni_anomalie_table", "distinte_progetto_albero_schede_anomalie_table", "distinte_progetto_albero_schede_modifica_table", "soluzioni_tampone_cicli_record", "distinte_progetto_albero_vetture_table"];

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"9B098998-2CAA-412C-9DA3-C82FBE579555"}
 */
var path = null;

/**
 * @properties={typeid:35,uuid:"0CC3B1EC-DB72-422D-8C04-975D83261448",variableType:-4}
 */
var showPathDraw = false;

/**
 *
 * @properties={typeid:24,uuid:"E6503C84-792E-4A73-A002-3FF1095A3C92"}
 */
function changeRoot() {
	if (filterDraw && !filterDescription && !filterTag) {

		var currentVersione = java.lang.Double.parseDouble(globals.vc2_currentVersione.toString());
		var codiceVersione = currentVersione.toLocaleString();

		var query = "select k8_distinte_progetto_id from k8_distinte_progetto where progetto = ? and codice_vettura = ?";
		var args = [globals.vc2_currentProgetto,
			codiceVersione];

		if (filterDraw) {
			query += " and numero_disegno like ?";
			args.push("%" + filterDraw + "%");
		}
		//FS

		//dep		var dataSet = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,-1);
		var dataSet = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, -1);

		if (dataSet.getMaxRowIndex() == 0) {
			plugins.dialogs.showWarningDialog("Attenzione", "Non sono presenti componenti che corrispondono ai criteri di ricerca", "Ok");
		} else {
			if (dataSet.getMaxRowIndex() == 1) {
				controller.loadRecords(dataSet);

				elements.progettiTreeView.removeAllRoots();
				elements.progettiTreeView.addRoots(foundset);
			} else {
				return false;
			}
		}

		return true;
	} else {
		return false;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"F3B65696-0039-486D-B7BC-80F362E0E59F"}
 */
function customExportFiltred() {
	if (globals.vc2_progettiCurrentNode == -1) {
		plugins.dialogs.showWarningDialog("Attenzione", "Selezionare un elemento dall'albero per effettuare l'export", "Ok");
		return null;
	} else {
		var tfs = foundset.duplicateFoundSet();
		tfs.loadRecords("select k8_distinte_progetto_id from k8_distinte_progetto where path_id like ?", [globals.vc2_progettiCurrentNodePath]);
		var count = databaseManager.getFoundSetCount(tfs);
		var notExport = null;
		if (count > 100 && !filterDescription && !filterTag) {
			notExport = (plugins.dialogs.showQuestionDialog("Export", "L'estrazione dei " + count + " record richiesti potrebbe richiedere molto tempo, continuare?", "Sì", "No") == "No") ? true : null;
		}
		if (!notExport) {
			globals.nfx_progressBarShow(count);
			initialLevel = globals.vc2_progettiCurrentNodeFoundset.livello;

			buffer = "\t";
			buffer += "Numero disegno" + "\t";
			buffer += "Esponente" + "\t";
			buffer += "Quantita" + "\t";
			buffer += "Cod. BAAN" + "\t";
			buffer += "N. Segnalazioni" + "\t";
			buffer += "N. Schede" + "\t";
			buffer += "N. Modifiche" + "\t";
			buffer += "Path numero disegno" + "\t";
			//buffer += "Path descrizione" + "\t";
			buffer += "\n";

			globals.utils_descendTreeNode(globals.vc2_progettiCurrentNodeFoundset, "k8_distinte_progetto$figli", printTreeLine);
			globals.utils_saveAsTXT(buffer, "exportAlberoProgetti-" + utils.dateFormat(new Date(), "yyyyMMdd") + ".xls");
		}
	}

	return null;
}

/**
 *
 * @properties={typeid:24,uuid:"3C867101-8825-4F57-ADF5-7DEE61242E2A"}
 */
function hideTree() {
	elements.progettiTreeView.visible = false;
	elements.warning.visible = true;
	elements.filterDescription1.enabled = false;
	elements.filterDraw1.enabled = false;
	elements.filterTag1.enabled = false;
	resetVariables();
}

/**
 *
 * @properties={typeid:24,uuid:"1F709CA7-6961-483E-9B03-47F6571EB0CC"}
 */
function nfx_customExport() {
	var fileds = ["to_char(numero_disegno)", "to_char(esponente)", "to_char(coefficiente_impiego)", "codice_baan_str", "numero_segnalazioni", "numero_anomalie", "numero_modifiche", "path_numero_disegno"];
	var labels = ["Numero disegno", "Esponente", "Quantita", "Cod. BAAN", "N. Segnalazioni", "N. Schede", "N. Modifiche", "Path numero disegno"];
	return (!filterDescription && !filterDraw && !filterTag) ? globals.vc2_progettiExportFull(fileds, labels) : customExportFiltred();
}

/**
 *
 * @properties={typeid:24,uuid:"70BA2EA5-A41B-4C04-8AD9-CA06DBBCB6BA"}
 */
function nfx_getTitle() {
	return "Distinte Progetto - Albero";
}

/**
 *
 * @properties={typeid:24,uuid:"8A9E4E01-E0FD-4BCE-8737-B4859CFBF076"}
 */
function nfx_isProgram() {
	return false;
}

/**
 *
 * @properties={typeid:24,uuid:"689048DD-A41B-4091-9C7B-CF36388281B1"}
 */
function nfx_onHide() {
	//FS
	//dep	databaseManager.removeTableFilterParam(controller.getServerName(),"FILTRO");
	databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), "FILTRO");

}

/**
 *
 * @properties={typeid:24,uuid:"102AB110-4514-4662-99F8-D3B70252275E"}
 */
function nfx_onShow() {
	progettiTreeViewSetRoots();
}

/**
 *
 * @properties={typeid:24,uuid:"78CCE4B3-7A58-42E0-B631-017C54B297FF"}
 * @AllowToRunInFind
 */
function onClick(value) {

	if (controller.find()) {
		k8_distinte_progetto_id = value;
		controller.search();
	}

	globals.vc2_progettiCurrentNode = k8_distinte_progetto_id;
	globals.vc2_progettiCurrentNodeNumeroDisegno = numero_disegno;
	globals.vc2_progettiCurrentNodePath = path_id + "%";
	globals.vc2_progettiCurrentNodeFoundset = foundset.duplicateFoundSet();

	globals.nfx_syncTabs();

	setPath();
}

/**
 *
 * @properties={typeid:24,uuid:"31EF8E2C-7596-4D6A-993D-A7AB9E589C4A"}
 */
function onCurrentProgettoChange() {
	//FS
	//dep	databaseManager.removeTableFilterParam(controller.getServerName(),"FILTRO");
	databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), "FILTRO");
	resetVariables();
	globals.vc2_currentVersione = null;
	application.updateUI();
	if (globals.vc2_currentProgetto == null) {
		elements.vc2_currentVersione.enabled = false;
	} else {
		elements.vc2_currentVersione.enabled = true;
	}
	progettiTreeViewSetRoots();
}

/**
 *
 * @properties={typeid:24,uuid:"00F8FBD9-E6BF-45C4-9397-140376B6EC83"}
 */
function onCurrentVersioneChange() {
	//FS
	//dep 	databaseManager.removeTableFilterParam(controller.getServerName(), "FILTRO");
	databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), "FILTRO");
	resetVariables();
	progettiTreeViewSetRoots();
}

/**
 *
 * @properties={typeid:24,uuid:"F9BABC88-0520-47F6-9C4D-CB7AF37D353B"}
 */
function onLoad() {

	//SAuc
	//	var prova1=controller.getServerName();
	//	var prova2=controller.getTableName();

	var Server = databaseManager.getDataSourceServerName(controller.getDataSource());
	var Table = databaseManager.getDataSourceTableName(controller.getDataSource());

	//var progettiTreeViewBinding = elements.progettiTreeView.createBinding(controller.getServerName(),controller.getTableName());
	var progettiTreeViewBinding = elements.progettiTreeView.createBinding(Server, Table);
	progettiTreeViewBinding.setTextDataprovider("descrizione");
	progettiTreeViewBinding.setNRelationName("k8_distinte_progetto$figli");
	progettiTreeViewBinding.setMethodToCallOnClick(onClick, "k8_distinte_progetto_id");
	//progettiTreeViewBinding.setMethodToCallOnRightClick(onClick,"k8_distinte_progetto_id");
	progettiTreeViewBinding.setImageURLDataprovider("treeIconURL");

	//	elements.progettiTreeView.createColumn(controller.getServerName(),controller.getTableName()	,"Numero Disegno","numero_disegno_str");
	//	elements.progettiTreeView.createColumn(controller.getServerName(),controller.getTableName() ,"Esp.","esponente_str");
	//	elements.progettiTreeView.createColumn(controller.getServerName(),controller.getTableName() ,"Qta","coefficiente_impiego_str");
	//	elements.progettiTreeView.createColumn(controller.getServerName(),controller.getTableName()	,"Cod. BAAN","codice_baan_str");
	//	elements.progettiTreeView.createColumn(controller.getServerName(),controller.getTableName()	,"N° Segnalazioni","numero_segnalazioni");
	//	elements.progettiTreeView.createColumn(controller.getServerName(),controller.getTableName()	,"N° Schede","numero_anomalie");
	//	elements.progettiTreeView.createColumn(controller.getServerName(),controller.getTableName()	,"N° Modifiche","numero_modifiche");

//		JStaffa
//		elements.progettiTreeView.createColumn(Server, Table, "Numero Disegno", "numero_disegno_str");
//		elements.progettiTreeView.createColumn(Server, Table, "Esp.", "esponente_str");
//		elements.progettiTreeView.createColumn(Server, Table, "Qta", "coefficiente_impiego_str");
//		elements.progettiTreeView.createColumn(Server, Table, "Cod. BAAN", "codice_baan_str");
//		elements.progettiTreeView.createColumn(Server, Table, "N° Segnalazioni", "numero_segnalazioni");
//		elements.progettiTreeView.createColumn(Server, Table, "N° Schede", "numero_anomalie");
//		elements.progettiTreeView.createColumn(Server, Table, "N° Modifiche", "numero_modifiche");
	elements.progettiTreeView.createColumn(controller.getDataSource(), "Numero Disegno", "numero_disegno_str");
	elements.progettiTreeView.createColumn(controller.getDataSource(), "Esp.", "esponente_str");
	elements.progettiTreeView.createColumn(controller.getDataSource(), "Qta", "coefficiente_impiego_str");
	elements.progettiTreeView.createColumn(controller.getDataSource(), "Cod. BAAN", "codice_baan_str");
	elements.progettiTreeView.createColumn(controller.getDataSource(), "N° Segnalazioni", "numero_segnalazioni");
	elements.progettiTreeView.createColumn(controller.getDataSource(), "N° Schede", "numero_anomalie");
	elements.progettiTreeView.createColumn(controller.getDataSource(), "N° Modifiche", "numero_modifiche");

		
}

/**
 *
 * @properties={typeid:24,uuid:"16880360-4884-488B-B43B-56906F46EDF3"}
 */
function printTreeLine(r) {

	var indentation = "";
	for (var i = 0; i < r.livello - initialLevel; i++) {
		indentation += "     ";
	}

	buffer += indentation + r.descrizione + "\t";
	buffer += r.numero_disegno_str + "\t";
	buffer += r.esponente_str + "\t";
	buffer += r.coefficiente_impiego_str + "\t";
	buffer += r.codice_baan_str + "\t";
	buffer += r.numero_segnalazioni + "\t";
	buffer += r.numero_anomalie + "\t";
	buffer += r.numero_segnalazioni + "\t";
	buffer += r.path_numero_disegno + "\t";
	//buffer += r.path_descrizione + "\t";
	buffer += "\n";
	globals.nfx_progressBarStep();
}

/**
 *
 * @properties={typeid:24,uuid:"062CFEE7-E8CA-47FE-9BB4-50D08066BFA1"}
 * @AllowToRunInFind
 */
function progettiTreeViewSetRoots() {
	if (globals.vc2_currentProgetto && globals.vc2_currentVersione) {
		if (controller.find()) {
			livello = 0;
			progetto = globals.vc2_currentProgetto;
			codice_vettura = java.lang.Double.parseDouble(globals.vc2_currentVersione.toString());
			controller.search();
		}
		elements.progettiTreeView.removeAllRoots();
		elements.progettiTreeView.addRoots(foundset);

		showTree();
	} else {
		globals.vc2_progettiCurrentNode = -1;
		globals.vc2_progettiCurrentNodeNumeroDisegno = -1;
		globals.vc2_progettiCurrentNodePath = null;
		globals.vc2_progettiCurrentNodeFoundset = null;

		hideTree();
	}

	setPath();
}

/**
 *
 * @properties={typeid:24,uuid:"7EEF9E1A-C9BC-440C-B575-09FE83F29B12"}
 */
function resetVariables() {
	showPathDraw = false;

	filterDescription = null;
	filterTag = null;
	filterDraw = null;
}

/**
 *
 * @properties={typeid:24,uuid:"245FA62B-0545-49EC-AC2C-0B0E353ACBA6"}
 */
function setPath() {
	if (globals.vc2_progettiCurrentNode == -1) {
		path = "Selezionare un componente...";
	} else {
		path = (showPathDraw) ? "Percorso: " + path_numero_disegno : "Percorso: " + path_descrizione;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"360A5C1C-B22A-447A-8D0B-F5EDBEA42B7D"}
 */
function setPathOnAction() {
	showPathDraw = !showPathDraw;
	setPath();
}

/**
 *
 * @properties={typeid:24,uuid:"B0B6FA2D-70F5-4342-9111-6FD9613978BE"}
 */
function showTree() {
	elements.progettiTreeView.visible = true;
	elements.warning.visible = false;
	elements.filterDescription1.enabled = true;
	elements.filterDraw1.enabled = true;
	elements.filterTag1.enabled = true;
}

/**
 *
 * @properties={typeid:24,uuid:"550F9E96-9AF0-4485-AB10-48EC45D9F0EA"}
 * @AllowToRunInFind
 */
function upDateFilter() {
	if (filterDescription == "") {
		filterDescription = null;
		application.updateUI();
	}

	if (filterDraw == "") {
		filterDraw = null;
		application.updateUI();
	}

	if (filterTag == "") {
		filterTag = null;
		application.updateUI();
	}

	if (filterDescription || filterDraw || filterTag) // qui verrà aggiunta il controllo sulla presenza di altri eventuali filtri in OR
	{
		//FS
		//dep		databaseManager.removeTableFilterParam(controller.getServerName(), "FILTRO");
		databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), "FILTRO");

		var idsToKeep = new Array();

		var currentVersione = java.lang.Double.parseDouble(String(globals.vc2_currentVersione));
		var codiceVersione = currentVersione.toLocaleString();

		var queryPath = "select path_id from k8_distinte_progetto where progetto = ? and codice_vettura = ?";
		var args = [globals.vc2_currentProgetto,
			codiceVersione];

		var words = null

		if (filterDraw) {
			var proLike = "%/" + filterDraw + "%";
			queryPath += " and path_numero_disegno like ?";
			args.push(proLike);
		}

		if (filterDescription) {
			words = filterDescription.split(" ");
			for (var i = 0; i < words.length; i++) {
				queryPath += " and upper(descrizione) like ?";
				var descLike = "%" + words[i].toUpperCase() + "%";
				args.push(descLike);
			}
		}

		if (filterTag) {
			queryPath = queryPath.replace("k8_distinte_progetto", "k8_distinte_progetto inner join k8_tag on k8_distinte_progetto_id = record_fk and '" + tableName + "' = table_name_fk");
			words = filterTag.split(" ");
			for (var i2 = 0; i2 < words.length; i2++) {
				queryPath += " and upper(tag) like ?";
				var tagLike = "%" + words[i2].toUpperCase() + "%";
				args.push(tagLike);
			}
		}
		//FS
		//dep  	var dataSet = databaseManager.getDataSetByQuery(controller.getServerName(), queryPath, args, -1);
		var dataSet = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), queryPath, args, -1);
		
		//SAuc
		//var path = dataSet.getColumnAsArray(1);

		path = dataSet.getColumnAsArray(1);
		
		if (path.length != 0) {
			for (var i3 = 0; i3 < path.length; i3++) {
				if (path[i3]) {
					//if(idsToKeep.length > 200) break; //serve per limitare i tempi d'attesa
					var idsTmp = path[i3].split("/");
					for (var j = 1; j < idsTmp.length; j++) {
						//if(idsToKeep.length > 200) break; //serve per limitare i tempi d'attesa
						if (idsToKeep.indexOf(idsTmp[j]) == -1) idsToKeep.push(idsTmp[j]);
						if (j == idsTmp.length - 1) {
							var like = "%/" + idsTmp[j] + "/%"

							var queryLike = "select k8_distinte_progetto_id from k8_distinte_progetto where progetto = ? and codice_vettura = ? and path_id like ?";
							args = [globals.vc2_currentProgetto,
							codiceVersione,
							like];

							//dep						dataSet = databaseManager.getDataSetByQuery(controller.getServerName(), queryLike, args, -1);
							dataSet = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), queryLike, args, -1);

							for (var k = 1; k <= dataSet.getMaxRowIndex(); k++) {
								if (idsToKeep.indexOf(dataSet.getValue(k, 1)) == -1) idsToKeep.push(dataSet.getValue(k, 1));
								if (idsToKeep.length > 200) break; //serve per limitare i tempi d'attesa
							}
						}
					}
				}
			}

//			application.output(idsToKeep.length);
			if (idsToKeep.length <= 200) // è il massino consentito dalla clausula "IN"
			{
				//FS
//dep				databaseManager.addTableFilterParam(controller.getServerName(), controller.getTableName(), "k8_distinte_progetto_id", "IN", idsToKeep, "FILTRO");
				databaseManager.addTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()) ,databaseManager.getDataSourceTableName(controller.getDataSource()) , "k8_distinte_progetto_id", "IN", idsToKeep, "FILTRO");

				if (controller.find()) {
					livello = 0;
					progetto = globals.vc2_currentProgetto;
					codice_vettura = java.lang.Double.parseDouble(String(globals.vc2_currentVersione));
					controller.search();
				}

				elements.progettiTreeView.removeAllRoots();
				elements.progettiTreeView.addRoots(foundset);
			} else {
				if (!changeRoot())
					plugins.dialogs.showWarningDialog("Attenzione", "Selezionare criteri di ricerca più restrittivi", "Ok");
			}
		} else {
			plugins.dialogs.showWarningDialog("Attenzione", "Non sono presenti componenti che corrispondono ai criteri di ricerca", "Ok");
		}
	} else {
		//FS
		//dep		databaseManager.removeTableFilterParam(controller.getServerName(), "FILTRO");
		databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), "FILTRO");

		controller.loadAllRecords();

		if (controller.find()) {
			livello = 0;
			progetto = globals.vc2_currentProgetto;
			codice_vettura = java.lang.Double.parseDouble(String(globals.vc2_currentVersione));
			controller.search();
		}
		elements.progettiTreeView.removeAllRoots();
		elements.progettiTreeView.addRoots(foundset);
	}
}
