dataSource:"db:/ferrari/k8_distinte_progetto_ft",
items:[
{
anchors:15,
beanClassName:"com.servoy.extensions.beans.dbtreeview.table.DBTreeTableView",
formIndex:-2,
location:"10,70",
name:"progettiTreeView",
size:"680,370",
typeid:12,
uuid:"048A554B-5F9A-424A-BD5D-74DD04F96DFD"
},
{
anchors:3,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"filterDescription",
location:"150,43",
name:"filterDescription1",
onActionMethodID:"E6E8A1AA-B563-4824-8800-93ACD41C4F1B",
size:"100,20",
text:"Filterdescription",
typeid:4,
uuid:"0545C657-C6FD-4C8C-B0A0-7071CE2A7906"
},
{
anchors:3,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"filterDraw",
location:"360,43",
name:"filterDraw1",
onActionMethodID:"E6E8A1AA-B563-4824-8800-93ACD41C4F1B",
size:"100,20",
text:"Filterdraw",
typeid:4,
uuid:"0DC6BCED-42D6-4A06-8F7F-C8DF2E7EE285"
},
{
anchors:3,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"filterTag",
location:"570,43",
name:"filterTag1",
onActionMethodID:"E6E8A1AA-B563-4824-8800-93ACD41C4F1B",
size:"100,20",
text:"Filtertag",
typeid:4,
uuid:"2CB42306-1D61-40B6-91BE-C260116FE9CF",
valuelistID:"040D260D-DF02-41B5-8942-3D1591313A99"
},
{
borderType:"EtchedBorder,0,null,null",
dataProviderID:"globals.vc2_currentProgetto",
displayType:2,
editable:false,
location:"80,10",
name:"vc2_currentProgetto",
onDataChangeMethodID:"CC65AE76-0B7E-4F1A-981A-EF59A3396EBB",
size:"100,20",
text:"Vc2 Currentprogetto",
typeid:4,
uuid:"331EF0FE-7975-4D39-8D21-F89C18FBE592",
valuelistID:"8DBD3071-35BE-4D55-B40F-F9B6FB9F8DD6"
},
{
height:480,
partType:5,
typeid:19,
uuid:"75A8A87D-AFB7-4531-ADE9-9420F8785FBE"
},
{
anchors:3,
horizontalAlignment:4,
labelFor:"filterDescription1",
location:"60,43",
mediaOptions:14,
size:"80,20",
styleClass:"form",
tabSeq:-1,
text:"Descrizione",
transparent:true,
typeid:7,
uuid:"768035E1-4688-438B-857E-F62C9F7594D5"
},
{
anchors:14,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"path",
displayType:1,
editable:false,
fontType:"Verdana,0,10",
location:"10,440",
name:"path1",
onActionMethodID:"072303D0-9982-40E5-BA4A-BC8689D661C1",
scrollbars:32,
size:"680,30",
text:"Path",
typeid:4,
uuid:"7B0669B9-E1F6-4DAA-8D24-2EE6219961EE"
},
{
anchors:11,
borderType:"TitledBorder,Filtri,0,0,DialogInput.plain,1,12,#333333",
formIndex:-4,
lineSize:1,
location:"10,30",
size:"680,40",
transparent:true,
typeid:21,
uuid:"7BA8ED0B-04E7-442C-974C-1BD66BBDA595"
},
{
anchors:3,
horizontalAlignment:4,
labelFor:"filterTag1",
location:"480,43",
mediaOptions:14,
size:"80,20",
styleClass:"form",
tabSeq:-1,
text:"TAG",
transparent:true,
typeid:7,
uuid:"86058A34-DC5A-460B-AD3E-205A78C5B383"
},
{
anchors:15,
formIndex:-3,
horizontalAlignment:0,
location:"10,80",
mediaOptions:14,
name:"warning",
size:"680,350",
styleClass:"form",
tabSeq:-1,
text:"Per visualizzare l'albero selezionare un progetto e una versione",
transparent:true,
typeid:7,
uuid:"98519E02-DE9E-421B-8828-94F9ED1FA88F",
verticalAlignment:0
},
{
labelFor:"vc2_currentProgetto",
location:"10,10",
mediaOptions:14,
size:"60,20",
styleClass:"form",
tabSeq:-1,
text:"Progetto",
transparent:true,
typeid:7,
uuid:"A7709ADD-8C6F-48C2-B5F5-678A433613AF"
},
{
borderType:"EtchedBorder,0,null,null",
dataProviderID:"globals.vc2_currentVersione",
displayType:2,
editable:false,
location:"270,10",
name:"vc2_currentVersione",
onDataChangeMethodID:"B45A0C8A-6B72-49B4-8BE6-ED7CB8712167",
size:"320,20",
text:"Vc2 Currentversione",
typeid:4,
uuid:"ABDF148D-AAEC-464C-8990-7D0CD1D60A18",
valuelistID:"3CD28F78-CDC1-4709-AC32-02348CA4B79A"
},
{
anchors:3,
horizontalAlignment:4,
labelFor:"filterDraw1",
location:"270,43",
mediaOptions:14,
size:"80,20",
styleClass:"form",
tabSeq:-1,
text:"N° disegno",
transparent:true,
typeid:7,
uuid:"C4A199DE-DB56-43D3-9494-BFF3C76D38F9"
},
{
labelFor:"vc2_currentVersione",
location:"200,10",
mediaOptions:14,
size:"60,20",
styleClass:"form",
tabSeq:-1,
text:"Versione",
transparent:true,
typeid:7,
uuid:"E176002F-D08D-4EC6-8F94-9024B8B405F1"
}
],
name:"distinte_progetto_albero_lite",
namedFoundSet:"separate",
navigatorID:"-1",
onHideMethodID:"7621013B-45FC-454C-88BE-EF2DC5A5C0E0",
onLoadMethodID:"2480801A-8EA1-41F0-8A1E-57B747CC1E33",
onShowMethodID:"BCCAF39F-5866-42AB-9E3E-7563D9D8EA46",
paperPrintScale:100,
showInMenu:true,
size:"700,480",
styleName:"keeneight",
typeid:3,
uuid:"6BAD8E49-D51C-48B2-B11F-7DDAA6895E9F"