/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1D210DA7-B295-4F0A-B1AD-CDA0B7167F37"}
 */
var buffer = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"7C882C78-2290-444C-8ABC-C664F8F44D07"}
 */
var filterDescription = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"175BA15A-A95E-4248-B409-673751609CDC"}
 */
var filterDraw = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A49D4D70-D92F-4F03-9F04-F29FD0ED11CA"}
 */
var filterTag = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"47E78C7F-0661-4FF1-A174-B660AFBC5702"}
 */
var initialLevel = null;

/**
 * @properties={typeid:35,uuid:"B76F7C05-EC61-4BC9-BE2E-B381FF77C071",variableType:-4}
 */
var nfx_related = ["distinte_progetto_dettaglio_record", "delibere_table", "tag_table", "soluzioni_tampone_cicli_record"];

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"E4D0D6C5-43F5-4AD5-94CF-484877EE0603"}
 */
var path = null;

/**
 * @properties={typeid:35,uuid:"50EE73D9-8723-4F98-A5D5-7E7A75C78D7E",variableType:-4}
 */
var showPathDraw = false;

/**
 *
 * @properties={typeid:24,uuid:"A3C4595E-EB73-45A6-A4F1-5C3439605598"}
 */
function changeRoot() {
	if (filterDraw && !filterDescription && !filterTag) {
		var currentVersione = java.lang.Double.parseDouble(String(globals.vc2_currentVersione));
		var codiceVersione = currentVersione.toLocaleString();

		var query = "select k8_distinte_progetto_id from k8_distinte_progetto_ft where progetto = ? and codice_vettura = ?";
		var args = [globals.vc2_currentProgetto,
			codiceVersione];

		if (filterDraw) {
			query += " and numero_disegno = ?";
			args.push(filterDraw);
		}

		//var dataSet = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,-1);

		var dataSet = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, -1);

		if (dataSet.getMaxRowIndex() == 0) {
			plugins.dialogs.showWarningDialog("Attenzione", "Non sono presenti componenti che corrispondono ai criteri di ricerca", "Ok");
		} else {
			if (dataSet.getMaxRowIndex() == 1) {
				controller.loadRecords(dataSet);

				elements.progettiTreeView.removeAllRoots();
				elements.progettiTreeView.addRoots(foundset);
			} else {
				return false;
			}
		}

		return true;
	} else {
		return false;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"3D565C09-09E5-4DBF-B28B-C4CB95CE808D"}
 */
function customExportFiltred() {
	if (globals.vc2_progettiCurrentNode == -1) {
		plugins.dialogs.showWarningDialog("Attenzione", "Selezionare un elemento dall'albero per effettuare l'export", "Ok");
		return;
	} else {
		var tfs = foundset.duplicateFoundSet();
		tfs.loadRecords("select k8_distinte_progetto_id from k8_distinte_progetto_ft where path_id like ?", [globals.vc2_progettiCurrentNodePath]);
		var count = databaseManager.getFoundSetCount(tfs);
		var notExport = null;
		if (count > 100 && !filterDescription && !filterTag) {
			notExport = (plugins.dialogs.showQuestionDialog("Export", "L'estrazione dei " + count + " record richiesti potrebbe richiedere molto tempo, continuare?", "Sì", "No") == "No") ? true : null;
		}
		if (!notExport) {
			globals.nfx_progressBarShow(count);
			initialLevel = globals.vc2_progettiCurrentNodeFoundset.livello;

			buffer = "\t";
			buffer += "Numero disegno" + "\t";
			buffer += "Esponente" + "\t";
			buffer += "Path numero disegno" + "\t";
			//buffer += "Path descrizione" + "\t";
			buffer += "\n";

			globals.utils_descendTreeNode(globals.vc2_progettiCurrentNodeFoundset, "k8_distinte_progetto_ft$figli", printTreeLine);
			globals.utils_saveAsTXT(buffer, "exportAlberoProgetti-" + utils.dateFormat(new Date(), "yyyyMMdd") + ".xls");
		}
	}

	return;
}

/**
 *
 * @properties={typeid:24,uuid:"372572A1-F98D-471D-9D61-4D0B991776F6"}
 */
function hideTree() {
	elements.progettiTreeView.visible = false;
	elements.warning.visible = true;
	elements.filterDescription1.enabled = false;
	elements.filterDraw1.enabled = false;
	elements.filterTag1.enabled = false;
	resetVariables();
}

/**
 *
 * @properties={typeid:24,uuid:"768DD308-07D8-41F6-BE89-C723BCD4F4BA"}
 */
function nfx_customExport() {
	var fields = ["to_char(numero_disegno)", "to_char(esponente)", "path_numero_disegno"];
	var labels = ["Numero disegno", "Esponente", "Path numero disegno"];
	return (!filterDescription && !filterDraw && !filterTag) ? globals.vc2_progettiExportFull(fields, labels) : customExportFiltred();
}

/**
 *
 * @properties={typeid:24,uuid:"07F8FEA1-0BA0-4409-AABC-D8E5B2C57DB8"}
 */
function nfx_getTitle() {
	return "Distinte Progetto - Albero Rapido";
}

/**
 *
 * @properties={typeid:24,uuid:"B16F1802-5808-489B-A311-822F055C1B71"}
 */
function nfx_isProgram() {
	return false;
}

/**
 *
 * @properties={typeid:24,uuid:"BD13A35B-3D43-40AE-9731-9BC63AB21D21"}
 */
function nfx_onHide() {
	//databaseManager.removeTableFilterParam(controller.getServerName(),"FILTRO");
	databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), "FILTRO");
}

/**
 *
 * @properties={typeid:24,uuid:"90359B1B-BEDD-4BB3-93F0-F4B92C2C2C84"}
 */
function nfx_onShow() {
	progettiTreeViewSetRoots();
}

/**
 *
 * @properties={typeid:24,uuid:"E94A954D-1D37-4804-B924-A70E44352211"}
 * @AllowToRunInFind
 */
function onClick(value) {

	if (controller.find()) {
		k8_distinte_progetto_id = value;
		controller.search();
	}

	globals.vc2_progettiCurrentNode = k8_distinte_progetto_id;
	globals.vc2_progettiCurrentNodeNumeroDisegno = numero_disegno;
	globals.vc2_progettiCurrentNodePath = path_id + "%";
	globals.vc2_progettiCurrentNodeFoundset = foundset.duplicateFoundSet();

	globals.nfx_syncTabs();

	setPath();
}

/**
 *
 * @properties={typeid:24,uuid:"CC65AE76-0B7E-4F1A-981A-EF59A3396EBB"}
 */
function onCurrentProgettoChange() {
	//databaseManager.removeTableFilterParam(controller.getServerName(),"FILTRO");
	databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), "FILTRO");
	resetVariables();
	globals.vc2_currentVersione = null;
	application.updateUI();
	if (globals.vc2_currentProgetto == null) {
		elements.vc2_currentVersione.enabled = false;
	} else {
		elements.vc2_currentVersione.enabled = true;
	}
	progettiTreeViewSetRoots();
}

/**
 *
 * @properties={typeid:24,uuid:"B45A0C8A-6B72-49B4-8BE6-ED7CB8712167"}
 */
function onCurrentVersioneChange() {
	//databaseManager.removeTableFilterParam(controller.getServerName(),"FILTRO");
	databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), "FILTRO");
	resetVariables();
	progettiTreeViewSetRoots();
}

/**
 *
 * @properties={typeid:24,uuid:"2480801A-8EA1-41F0-8A1E-57B747CC1E33"}
 */
function onLoad() {

	//FS
	//dep	var progettiTreeViewBinding = elements.progettiTreeView.createBinding(controller.getServerName(),controller.getTableName());
	var progettiTreeViewBinding = elements.progettiTreeView.createBinding(databaseManager.getDataSourceServerName(controller.getDataSource()), databaseManager.getDataSourceTableName(controller.getDataSource()));
	progettiTreeViewBinding.setTextDataprovider("descrizione");
	progettiTreeViewBinding.setNRelationName("k8_distinte_progetto_ft$figli");
	progettiTreeViewBinding.setMethodToCallOnClick(onClick, "k8_distinte_progetto_id");
	progettiTreeViewBinding.setImageURLDataprovider("treeIconURL");

	//dep	elements.progettiTreeView.createColumn(controller.getServerName(),controller.getTableName()	,"Numero Disegno","numero_disegno_str");
	//dep	elements.progettiTreeView.createColumn(controller.getServerName(),controller.getTableName() ,"Esp.","esponente_str");

	elements.progettiTreeView.createColumn(controller.getDataSource(), "Numero Disegno", "numero_disegno_str");
	elements.progettiTreeView.createColumn(controller.getDataSource(), "Esp.", "esponente_str");
}

/**
 *
 * @properties={typeid:24,uuid:"683CA320-3F2E-4334-A8C7-21EDB5CFE610"}
 */
function printTreeLine(r) {

	var indentation = "";
	for (var i = 0; i < r.livello - initialLevel; i++) {
		indentation += "     ";
	}

	buffer += indentation + r.descrizione + "\t";
	buffer += r.numero_disegno_str + "\t";
	buffer += r.esponente_str + "\t";
	buffer += r.path_numero_disegno + "\t";
	//buffer += r.path_descrizione + "\t";
	buffer += "\n";
	globals.nfx_progressBarStep();
}

/**
 *
 * @properties={typeid:24,uuid:"43297D67-F1F3-4C60-A9B0-50D210224AE6"}
 * @AllowToRunInFind
 */
function progettiTreeViewSetRoots() {
	if (globals.vc2_currentProgetto && globals.vc2_currentVersione) {
		if (controller.find()) {
			livello = 0;
			codice_vettura = java.lang.Double.parseDouble(String(globals.vc2_currentVersione));
			controller.search();
		}
		elements.progettiTreeView.removeAllRoots();
		elements.progettiTreeView.addRoots(foundset);

		showTree();
	} else {
		globals.vc2_progettiCurrentNode = -1;
		globals.vc2_progettiCurrentNodeNumeroDisegno = -1;
		globals.vc2_progettiCurrentNodePath = null;
		globals.vc2_progettiCurrentNodeFoundset = null;

		hideTree();
	}

	setPath();
}

/**
 *
 * @properties={typeid:24,uuid:"BA2480D5-9220-4718-BCA2-CE7176AD57E8"}
 */
function resetVariables() {
	showPathDraw = false;

	filterDescription = null;
	filterTag = null;
	filterDraw = null;
}

/**
 *
 * @properties={typeid:24,uuid:"0B0D5239-41FE-4ACC-A35C-328592FB863F"}
 */
function setPath() {
	if (globals.vc2_progettiCurrentNode == -1) {
		path = "Selezionare un componente...";
	} else {
		path = (showPathDraw) ? "Percorso: " + path_numero_disegno : "Percorso: " + path_descrizione;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"072303D0-9982-40E5-BA4A-BC8689D661C1"}
 */
function setPathOnAction() {
	showPathDraw = !showPathDraw;
	setPath();
}

/**
 *
 * @properties={typeid:24,uuid:"5D20AD24-3014-461B-A693-695DF5126146"}
 */
function showTree() {
	elements.progettiTreeView.visible = true;
	elements.warning.visible = false;
	elements.filterDescription1.enabled = true;
	elements.filterDraw1.enabled = true;
	elements.filterTag1.enabled = true;
}

/**
 *
 * @properties={typeid:24,uuid:"E6E8A1AA-B563-4824-8800-93ACD41C4F1B"}
 * @AllowToRunInFind
 */
function upDateFilter() {
	if (filterDescription == "") {
		filterDescription = null;
		application.updateUI();
	}

	if (filterDraw == "") {
		filterDraw = null;
		application.updateUI();
	}

	if (filterTag == "") {
		filterTag = null;
		application.updateUI();
	}

	if (filterDescription || filterDraw || filterTag) // qui verrà aggiunta il controllo sulla presenza di altri eventuali filtri in OR
	{
		//FS
		//dep		databaseManager.removeTableFilterParam(controller.getServerName(),"FILTRO");
		databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), "FILTRO");

		var idsToKeep = new Array();

		var currentVersione = java.lang.Double.parseDouble(String(globals.vc2_currentVersione));
		var codiceVersione = currentVersione.toLocaleString();

		var queryPath = "select path_id from k8_distinte_progetto_ft where progetto = ? and codice_vettura = ?";
		var args = [globals.vc2_currentProgetto,
			codiceVersione];

		var words = null;

		if (filterDraw) {
			var proLike = "%/" + filterDraw + "%";
			queryPath += " and path_numero_disegno like ?";
			args.push(proLike);
		}

		if (filterDescription) {
			words = filterDescription.split(" ");
			for (var i = 0; i < words.length; i++) {
				queryPath += " and upper(descrizione) like ?";
				var descLike = "%" + words[i].toUpperCase() + "%";
				args.push(descLike);
			}
		}

		if (filterTag) {
			queryPath = queryPath.replace("k8_distinte_progetto_ft", "k8_distinte_progetto_ft inner join k8_tag on k8_distinte_progetto_id = record_fk and '" + tableName + "' = table_name_fk");
			words = filterTag.split(" ");
			for (var i2 = 0; i2 < words.length; i2++) {
				queryPath += " and upper(tag) like ?";
				var tagLike = "%" + words[i2].toUpperCase() + "%";
				args.push(tagLike);
			}
		}
		//FS
		//dep		var dataSet = databaseManager.getDataSetByQuery(controller.getServerName(),queryPath,args,-1);
		var dataSet = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), queryPath, args, -1);
		
		
		//SAuc
		//var path = dataSet.getColumnAsArray(1);

		path = dataSet.getColumnAsArray(1);
		
		if (path.length != 0) {
			for (var i3 = 0; i3 < path.length; i3++) {
				if (path[i3]) {
					if (idsToKeep.length > 200) break; //serve per limitare i tempi d'attesa
					var idsTmp = path[i3].split("/");
					for (var j = 1; j < idsTmp.length; j++) {
						if (idsToKeep.length > 200) break; //serve per limitare i tempi d'attesa
						if (idsToKeep.indexOf(idsTmp[j]) == -1) idsToKeep.push(idsTmp[j]);
						if (j == idsTmp.length - 1) {
							var like = "%/" + idsTmp[j] + "/%"

							var queryLike = "select k8_distinte_progetto_id from k8_distinte_progetto_ft where progetto = ? and codice_vettura = ? and path_id like ?";
							args = [globals.vc2_currentProgetto,
							codiceVersione,
							like];
							//FS
							//dep							dataSet = databaseManager.getDataSetByQuery(controller.getServerName(), queryLike, args, -1);
							dataSet = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), queryLike, args, -1);
							for (var k = 1; k <= dataSet.getMaxRowIndex(); k++) {
								if (idsToKeep.indexOf(dataSet.getValue(k, 1)) == -1) idsToKeep.push(dataSet.getValue(k, 1));
								if (idsToKeep.length > 200) break; //serve per limitare i tempi d'attesa
							}
						}
					}
				}
			}

//			application.output(idsToKeep.length);
			if (idsToKeep.length <= 200) // è il massino consentito dalla clausula "IN"
			{
				//FS
				//dep	databaseManager.addTableFilterParam(controller.getServerName(), controller.getTableName(), "k8_distinte_progetto_id", "IN", idsToKeep, "FILTRO");
				databaseManager.addTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), databaseManager.getDataSourceTableName(controller.getDataSource()) , "k8_distinte_progetto_id", "IN", idsToKeep, "FILTRO");

				if (controller.find()) {
					livello = 0;
					progetto = globals.vc2_currentProgetto;
					codice_vettura = java.lang.Double.parseDouble(String(globals.vc2_currentVersione));
					controller.search();
				}

				elements.progettiTreeView.removeAllRoots();
				elements.progettiTreeView.addRoots(foundset);
			} else {
				if (!changeRoot())
					plugins.dialogs.showWarningDialog("Attenzione", "Selezionare criteri di ricerca più restrittivi", "Ok");
			}
		} else {
			plugins.dialogs.showWarningDialog("Attenzione", "Non sono presenti componenti che corrispondono ai criteri di ricerca", "Ok");
		}
	} else {
		//FS
//dep		databaseManager.removeTableFilterParam(controller.getServerName(), "FILTRO");
		databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), "FILTRO");

		controller.loadAllRecords();

		if (controller.find()) {
			livello = 0;
			progetto = globals.vc2_currentProgetto;
			codice_vettura = java.lang.Double.parseDouble(String(globals.vc2_currentVersione));
			controller.search();
		}
		elements.progettiTreeView.removeAllRoots();
		elements.progettiTreeView.addRoots(foundset);
	}
}
