/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1455F050-D34A-4D36-BB9B-C089922E1411"}
 */
var field = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C6DD399C-E298-449C-98F0-660D6B36C287"}
 */
var filterDescription = '';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C622C949-3BC6-421B-B488-D5B4FBD4958F"}
 */
var filterNumDis = '';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"8D863A27-9A44-474E-9B83-E272708E1698"}
 */
var from = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B6E5E084-423C-443E-9FEE-B62C1D62C3AA"}
 */
var n_disegno = null;

/**
 * @properties={typeid:35,uuid:"C4C955A2-61A8-4CB7-94C8-BE6DD3281B36",variableType:-4}
 */
var updateForm = true;

/**
 *
 * @properties={typeid:24,uuid:"4DEA9791-1ABC-4C22-8362-F96B23EF195B"}
 */
function choose()
{
	if (n_disegno){
		forms[from][field] = n_disegno;
		exit();
	}else{
		application.beep();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"92F8D115-F56B-48CA-BAE1-C0F5834CF913"}
 */
function exit()
{
	//application.closeFormDialog("treePicker");
	var w=controller.getWindow();
	w.destroy();
}

/**
 *
 * @properties={typeid:24,uuid:"6CBB2989-E630-4FF1-B0B6-2811038902A8"}
 * @AllowToRunInFind
 */
function filter(){
	if(globals.vc2_currentVersione){
		forms.distinte_progetto_albero_picker_search.search(filterDescription, filterNumDis);
	}else{
		plugins.dialogs.showWarningDialog("Versione","Prima di poter procedere è necessario impostare la versione desiderata.","Ok");
	}
}

/**
 * @properties={typeid:24,uuid:"6190CA98-A232-49DF-BE9F-C29E312F3691"}
 * @AllowToRunInFind
 */
function onClick(n_disegno){

	if(updateForm){
		forms.distinte_progetto_albero_picker_search.search(null,java.lang.Double.parseDouble(n_disegno));
	}
	updateForm = true;
}

/**
 *
 * @properties={typeid:24,uuid:"22DBC845-20E6-4A8D-8CF1-676A31B57661"}
 */
function onLoad()
{
	//FS
//dep	var progettiTreeViewBinding = elements.progettiTreeView.createBinding(controller.getServerName(),controller.getTableName());
	var progettiTreeViewBinding = elements.progettiTreeView.createBinding(databaseManager.getDataSourceServerName(controller.getDataSource()),databaseManager.getDataSourceTableName(controller.getDataSource()) );

	progettiTreeViewBinding.setTextDataprovider("descrizione");
	progettiTreeViewBinding.setNRelationName("k8_distinte_progetto$figli");
	progettiTreeViewBinding.setMethodToCallOnClick(onClick,"numero_disegno");

//dep	elements.progettiTreeView.createColumn(controller.getServerName(),controller.getTableName()	,"Numero Disegno","k8_distinte_progetto_core_to_k8_distinte_progetto.numero_disegno_str");
//dep 	elements.progettiTreeView.createColumn(controller.getServerName(),controller.getTableName() ,"Esp.","k8_distinte_progetto_core_to_k8_distinte_progetto.esponente_str");
	
	elements.progettiTreeView.createColumn(controller.getDataSource(),"Numero Disegno","k8_distinte_progetto_core_to_k8_distinte_progetto.numero_disegno_str");
	elements.progettiTreeView.createColumn(controller.getDataSource(),"Esp.","k8_distinte_progetto_core_to_k8_distinte_progetto.esponente_str");
}

/**
 *
 * @properties={typeid:24,uuid:"EE92321B-64E2-4DC1-BD23-92EDA33BAE05"}
 */
function onShow(){
	elements.progettiTreeView.visible = false;
	globals.vc2_currentVersione = null;
	filterDescription = filterNumDis = "";
	forms.distinte_progetto_albero_picker_search.reset();
}

/**
 *
 * @properties={typeid:24,uuid:"47E8F42F-42A9-4E35-A558-575BB285746A"}
 * @AllowToRunInFind
 */
function progettiTreeViewSetRoots()
{
	if(globals.vc2_currentProgetto && globals.vc2_currentVersione)
	{
		if(controller.find())
		{
			livello = 0;
			progetto = globals.vc2_currentProgetto;
			codice_vettura = java.lang.Double.parseDouble(globals.vc2_currentVersione.toString());
			controller.search();
		}
		elements.progettiTreeView.removeAllRoots();
		elements.progettiTreeView.addRoots(foundset);
	}
	elements.progettiTreeView.visible = true;
}

/**
 * Handle hide window.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 *
 * @properties={typeid:24,uuid:"DF995A94-2BAD-41D2-A7D1-E29C4DBB4E0D"}
 */
function onHide(event) {
	from = "";
	field = "";
}
