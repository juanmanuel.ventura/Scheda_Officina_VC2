/**
 * @properties={typeid:24,uuid:"B02653D1-E505-49CB-A63A-4763AAD61A6C"}
 */
function reset() {
	search(null, null);
}

/**
 * @properties={typeid:24,uuid:"29A64663-ABF5-4D4E-A5AF-A077FFD5654D"}
 * @AllowToRunInFind
 */
function search(d, n) {
	d = arguments[0];
	n = arguments[1];
	if (!d && !n) {
		if (controller.find()) {
			numero_disegno = "< 0";
			controller.search();
		}
	} else {
		if (controller.find()) {
			progetto = globals.vc2_currentProgetto;
			codice_vettura = java.lang.Double.parseDouble(String(globals.vc2_currentVersione));
			if (d) {
				descrizione = '%' + d.toUpperCase() + '%';
			}
			if (n) {
				numero_disegno = n;
				controller.search();
			}
		}
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"A55D95E3-D082-40F4-90FB-A3991B4472D5"}
 */
function setPathInPicker(event) {
	forms.distinte_progetto_albero_picker.n_disegno = numero_disegno;
	//FS controllo sul campo del DB altrimenti la split da un errore
	if (path_id) {
		var pathAr = path_id.split("/").slice(1);
		forms.distinte_progetto_albero_picker.updateForm = false;
		forms.distinte_progetto_albero_picker.elements.progettiTreeView.selectionPath = pathAr;
	}
}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"E7107DE7-6A28-40D9-8EAB-021266F2EABC"}
 */
function onLoad(event) {
	controller.readOnly = true;
}
