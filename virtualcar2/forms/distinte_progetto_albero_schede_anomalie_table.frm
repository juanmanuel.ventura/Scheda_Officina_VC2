dataSource:"db:/ferrari/k8_dist_sk_a",
extendsID:"4ED221C9-3932-40F6-9866-0A9EDCA84CE1",
items:[
{
anchors:11,
labelFor:"testo",
location:"280,10",
mediaOptions:14,
size:"770,20",
styleClass:"table",
tabSeq:-1,
text:"Testo",
typeid:7,
uuid:"1EE8D7BB-EAD0-48C7-96C3-A3FF0AD415E4"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"numero_scheda",
location:"40,30",
name:"numero_scheda",
size:"140,20",
text:"Numero Scheda",
typeid:4,
uuid:"377A53B8-08BF-4930-A611-ED747BCC842B"
},
{
anchors:11,
labelFor:"ente",
location:"190,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Ente",
typeid:7,
uuid:"69F686D7-E694-4D17-8C40-25B73BADBD08"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"testo",
location:"280,30",
name:"testo",
size:"770,20",
text:"Testo",
typeid:4,
uuid:"6C64FC2C-1E5B-4D4A-B971-D126F502276A"
},
{
labelFor:"go",
location:"10,10",
mediaOptions:14,
size:"20,20",
styleClass:"table",
tabSeq:-1,
typeid:7,
uuid:"85528E02-B0F5-4E3C-81B4-340FD135073D"
},
{
borderType:"EtchedBorder,0,null,null",
imageMediaID:"96BE4823-BFA6-4907-B72A-A32F537CB2EE",
location:"10,30",
mediaOptions:10,
name:"go",
onActionMethodID:"C0303A67-C18F-453F-924B-3AD18780712B",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
showClick:false,
showFocus:false,
size:"20,20",
toolTipText:"Vai alla scheda...",
transparent:true,
typeid:7,
uuid:"A97B8C8E-5EAF-40A1-A0D0-2FE86F6514F5"
},
{
anchors:11,
labelFor:"numero_scheda",
location:"40,10",
mediaOptions:14,
size:"140,20",
styleClass:"table",
tabSeq:-1,
text:"N° scheda",
typeid:7,
uuid:"B0810521-FD95-41C9-9C13-ADDFEBCAE7F8"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"ente",
location:"190,30",
name:"ente",
size:"80,20",
text:"Ente",
typeid:4,
uuid:"D7620B58-0697-46FE-A3EB-1AD3DCA55D05"
},
{
height:480,
partType:5,
typeid:19,
uuid:"F3B9C1B0-EEB0-41D0-9C86-4948ED33B4EB"
}
],
name:"distinte_progetto_albero_schede_anomalie_table",
paperPrintScale:100,
showInMenu:true,
size:"1060,480",
typeid:3,
uuid:"995B2EB4-1918-4EEB-AA9E-4A353031E491",
view:3