/**
 *
 * @properties={typeid:24,uuid:"28B298F2-942E-4CF6-B5A3-54F3692FA862"}
 */
function goToSegnalazioniAnomalie()
{
	//globals.alternate=true;
	
	//globals.toGoTo = "segnalazioni_anomalie_record";
	globals.nfx_goToProgramAndRecord("segnalazioni_anomalie_record",k8_anomalie_id,"k8_anomalie_id");
	
	//globals.alternate=false;
}

/**
 *
 * @properties={typeid:24,uuid:"94CF0A78-2BBE-47D3-AFE7-FDAE7E363DDE"}
 */
function nfx_getTitle()
{
	return "Segnalazioni anomalia";
}
