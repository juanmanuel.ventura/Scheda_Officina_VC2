dataSource:"db:/ferrari/k8_distinte_progetto",
items:[
{
labelFor:"vc2_currentProgetto",
location:"10,10",
mediaOptions:14,
size:"60,20",
styleClass:"form",
tabSeq:-1,
text:"Progetto",
transparent:true,
typeid:7,
uuid:"21B93BB8-1131-4A82-BE8F-DCEBDF59CE48"
},
{
borderType:"EtchedBorder,0,null,null",
dataProviderID:"globals.vc2_currentVersione",
displayType:2,
editable:false,
location:"270,10",
name:"vc2_currentVersione",
onDataChangeMethodID:"E9455E8F-9107-488E-85FD-DF337E19BF4C",
size:"320,20",
text:"Vc2 Currentversione",
typeid:4,
uuid:"49F90DCA-EAD1-4465-B2DE-F6CF0179D7DF",
valuelistID:"3CD28F78-CDC1-4709-AC32-02348CA4B79A"
},
{
anchors:3,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"filterTag",
displayType:10,
location:"570,43",
name:"filterTag1",
onActionMethodID:"5DB42FD7-E47F-477E-8FC8-6E9F9F45D275",
size:"100,20",
text:"Filtertag",
typeid:4,
uuid:"59D58F14-05CB-4451-AFEE-255A16E57587",
valuelistID:"040D260D-DF02-41B5-8942-3D1591313A99"
},
{
anchors:15,
beanClassName:"com.servoy.extensions.beans.dbtreeview.table.DBTreeTableView",
formIndex:-2,
location:"10,70",
name:"progettiTreeView",
size:"680,370",
typeid:12,
uuid:"6CAF5A75-6A0A-4F7C-822D-485862C32878"
},
{
anchors:3,
horizontalAlignment:4,
labelFor:"filterTag1",
location:"480,43",
mediaOptions:14,
size:"80,20",
styleClass:"form",
tabSeq:-1,
text:"TAG",
transparent:true,
typeid:7,
uuid:"85B3D7FE-BBF8-45EA-B5F7-1E2606E8A0EA"
},
{
labelFor:"vc2_currentVersione",
location:"200,10",
mediaOptions:14,
size:"60,20",
styleClass:"form",
tabSeq:-1,
text:"Versione",
transparent:true,
typeid:7,
uuid:"8B0705AE-C4A8-4D88-B1C5-2DB21102C097"
},
{
anchors:3,
horizontalAlignment:4,
labelFor:"filterDescription1",
location:"60,43",
mediaOptions:14,
size:"80,20",
styleClass:"form",
tabSeq:-1,
text:"Descrizione",
transparent:true,
typeid:7,
uuid:"8EE2FAC5-C0B9-4329-82F1-3DFF56B9D7AE"
},
{
height:480,
partType:5,
typeid:19,
uuid:"91D7638F-91F9-4B44-B7EF-B4620337AA93"
},
{
anchors:3,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"filterDescription",
location:"150,43",
name:"filterDescription1",
onActionMethodID:"5DB42FD7-E47F-477E-8FC8-6E9F9F45D275",
size:"100,20",
text:"Filterdescription",
typeid:4,
uuid:"954329DB-B6F9-4C0F-811E-6052A4B7EC88"
},
{
anchors:14,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"path",
displayType:1,
editable:false,
fontType:"Verdana,0,10",
location:"10,440",
name:"path1",
onActionMethodID:"79761824-7541-4D4F-88C7-95E9B17FDAD3",
scrollbars:32,
size:"680,30",
text:"Path",
typeid:4,
uuid:"AC6525A6-823A-4DC6-A62F-9BFF6A0162D1"
},
{
borderType:"EtchedBorder,0,null,null",
dataProviderID:"globals.vc2_currentProgetto",
displayType:2,
editable:false,
location:"80,10",
name:"vc2_currentProgetto",
onDataChangeMethodID:"794D5A3A-5462-45E6-B1C3-6FACE017B07C",
size:"100,20",
text:"Vc2 Currentprogetto",
typeid:4,
uuid:"ACB6320E-FCD8-41F8-AA96-1D18D0C87999",
valuelistID:"8DBD3071-35BE-4D55-B40F-F9B6FB9F8DD6"
},
{
anchors:3,
horizontalAlignment:4,
labelFor:"filterDraw1",
location:"270,43",
mediaOptions:14,
size:"80,20",
styleClass:"form",
tabSeq:-1,
text:"N° disegno",
transparent:true,
typeid:7,
uuid:"B0408A29-BAFD-47C8-A02B-0049F3A8E82F"
},
{
anchors:15,
formIndex:-3,
horizontalAlignment:0,
location:"10,80",
mediaOptions:14,
name:"warning",
size:"680,350",
styleClass:"form",
tabSeq:-1,
text:"Per visualizzare l'albero selezionare un progetto e una versione",
transparent:true,
typeid:7,
uuid:"B35EF025-7B37-473B-84BB-738991F3DBAE",
verticalAlignment:0
},
{
anchors:11,
borderType:"TitledBorder,Filtri,0,0,DialogInput.plain,1,12,#333333",
formIndex:-4,
lineSize:1,
location:"10,30",
size:"680,40",
transparent:true,
typeid:21,
uuid:"EE8C9285-3FF2-4BC8-836D-37323E224630"
},
{
anchors:3,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"filterDraw",
location:"360,43",
name:"filterDraw1",
onActionMethodID:"5DB42FD7-E47F-477E-8FC8-6E9F9F45D275",
size:"100,20",
text:"Filterdraw",
typeid:4,
uuid:"F648617E-1C84-4B21-AF77-68D4D35462AA"
}
],
name:"distinte_progetto_albero_weight_and_material",
namedFoundSet:"separate",
navigatorID:"-1",
onHideMethodID:"7621013B-45FC-454C-88BE-EF2DC5A5C0E0",
onLoadMethodID:"2D3E5159-BAA9-4DDC-BE63-53ECC5CAE95D",
onShowMethodID:"BCCAF39F-5866-42AB-9E3E-7563D9D8EA46",
paperPrintScale:100,
showInMenu:true,
size:"700,480",
styleName:"keeneight",
typeid:3,
uuid:"F1592ABE-7362-4E0F-869F-225627860730"