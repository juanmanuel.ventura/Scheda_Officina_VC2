/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"72378E3D-C853-4FDB-8E6F-7524639CD9C0"}
 */
var buffer = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"173C1D0E-82ED-405B-9D2D-69D1D6215175"}
 */
var filterDescription = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B213C2DB-1E5C-42C3-8EF7-CF0F37D62C10"}
 */
var filterDraw = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"92CE9F54-4651-4966-8D8D-110A6980675A"}
 */
var filterTag = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"74008A9B-2F86-47C3-A80B-12C456FE117D"}
 */
var initialLevel = null;

/**
 * @properties={typeid:35,uuid:"52CE324B-43AB-4B84-B330-680FFA238970",variableType:-4}
 */
var nfx_related = ["distinte_progetto_dettaglio_record", "tag_table", "delibere_table", "soluzioni_tampone_cicli_record"];

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"4518F815-00F1-42E0-9221-26B6B8E1A9F0"}
 */
var path = null;

/**
 * @properties={typeid:35,uuid:"9E2FB0E9-8E85-4153-989D-884DBB0257C1",variableType:-4}
 */
var showPathDraw = false;

/**
 *
 * @properties={typeid:24,uuid:"940F9FFF-2FF9-43F1-AD6D-9148DDE5A1CD"}
 */
function changeRoot() {
	if (filterDraw && !filterDescription && !filterTag) {
		var currentVersione = java.lang.Double.parseDouble(String(globals.vc2_currentVersione));
		var codiceVersione = currentVersione.toLocaleString();

		var query = "select k8_distinte_progetto_id from k8_distinte_progetto where progetto = ? and codice_vettura = ?";
		var args = [globals.vc2_currentProgetto,
			codiceVersione];

		if (filterDraw) {
			query += " and numero_disegno like ?";
			args.push("%" + filterDraw + "%");
		}
		//FS

		//dep		var dataSet = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,-1);
		var dataSet = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, -1);

		if (dataSet.getMaxRowIndex() == 0) {
			plugins.dialogs.showWarningDialog("Attenzione", "Non sono presenti componenti che corrispondono ai criteri di ricerca", "Ok");
		} else {
			if (dataSet.getMaxRowIndex() == 1) {
				controller.loadRecords(dataSet);

				elements.progettiTreeView.removeAllRoots();
				elements.progettiTreeView.addRoots(foundset);
			} else {
				return false;
			}
		}

		return true;
	} else {
		return false;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"C5D5B5EC-CB54-4BB1-9B84-F26E61D5DF3D"}
 */
function customExportFiltred() {
	if (globals.vc2_progettiCurrentNode == -1) {
		plugins.dialogs.showWarningDialog("Attenzione", "Selezionare un elemento dall'albero per effettuare l'export", "Ok");
		return;
	} else {
		var tfs = foundset.duplicateFoundSet();
		tfs.loadRecords("select k8_distinte_progetto_id from k8_distinte_progetto where path_id like ?", [globals.vc2_progettiCurrentNodePath]);
		var count = databaseManager.getFoundSetCount(tfs);
		var notExport = null;
		if (count > 100 && !filterDescription && !filterTag) {
			notExport = (plugins.dialogs.showQuestionDialog("Export", "L'estrazione dei " + count + " record richiesti potrebbe richiedere molto tempo, continuare?", "Sì", "No") == "No") ? true : null;
		}
		if (!notExport) {
			globals.nfx_progressBarShow(count);
			initialLevel = globals.vc2_progettiCurrentNodeFoundset.livello;

			buffer = "\t";
			buffer += "Numero disegno" + "\t";
			buffer += "Esponente" + "\t";
			buffer += "Quantita" + "\t";
			buffer += "Unita di misura" + "\t";
			buffer += "Peso unitario" + "\t";
			buffer += "Peso" + "\t";
			buffer += "Materiale" + "\t";
			buffer += "Path numero disegno" + "\t";
			buffer += "\n";

			globals.utils_descendTreeNode(globals.vc2_progettiCurrentNodeFoundset, "k8_distinte_progetto$figli", printTreeLine);
			globals.utils_saveAsTXT(buffer, "exportAlberoProgetti-" + utils.dateFormat(new Date(), "yyyyMMdd") + ".xls");
		}
	}

	return;
}

/**
 *
 * @properties={typeid:24,uuid:"7AD6F9C0-EF8D-457D-96FE-73AE08810372"}
 */
function hideTree() {
	elements.progettiTreeView.visible = false;
	elements.warning.visible = true;
	elements.filterDescription1.enabled = false;
	elements.filterDraw1.enabled = false;
	elements.filterTag1.enabled = false;
	resetVariables();
}

/**
 *
 * @properties={typeid:24,uuid:"A1397A43-65E2-4B0F-9B7A-BBE87344EE77"}
 */
function nfx_customExport() {
	var fields = ["to_char(numero_disegno)", "to_char(esponente)", "to_char(coefficiente_impiego)", "unita_misura_peso", "peso_unitario", "peso", "descrizione_materiale", "path_numero_disegno"];
	var labels = ["Numero disegno", "Esponente", "Quantita", "Unita di misura", "Peso unitario", "Peso", "Materiale", "Path numero disegno"];
	return (!filterDescription && !filterDraw && !filterTag) ? globals.vc2_progettiExportFull(fields, labels) : customExportFiltred();
}

/**
 *
 * @properties={typeid:24,uuid:"67ADD7E0-51D4-4F9D-A8AB-E8015C423002"}
 */
function nfx_getTitle() {
	return "Distinte Progetto - Peso/Materiale";
}

/**
 *
 * @properties={typeid:24,uuid:"FB31B347-B5B7-4F4E-B883-E449154A910F"}
 */
function nfx_isProgram() {
	return false;
}

/**
 *
 * @properties={typeid:24,uuid:"353E0BF9-9707-43B1-B043-56C30E52C631"}
 */
function nfx_onHide() {
	//FS
	//dep	databaseManager.removeTableFilterParam(controller.getServerName(),"FILTRO");
	databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), "FILTRO");

}

/**
 *
 * @properties={typeid:24,uuid:"E5CC2771-2D5D-42D9-A8AA-B2E961357171"}
 */
function nfx_onShow() {
	progettiTreeViewSetRoots();
}

/**
 *
 * @properties={typeid:24,uuid:"4DC4EF4C-F1FD-4612-BCF8-B33B224D4732"}
 * @AllowToRunInFind
 */
function onClick(value) {

	if (controller.find()) {
		k8_distinte_progetto_id = value;
		controller.search();
	}

	globals.vc2_progettiCurrentNode = k8_distinte_progetto_id;
	globals.vc2_progettiCurrentNodeNumeroDisegno = numero_disegno;
	globals.vc2_progettiCurrentNodePath = path_id + "%";
	globals.vc2_progettiCurrentNodeFoundset = foundset.duplicateFoundSet();

	globals.nfx_syncTabs();

	setPath();
}

/**
 *
 * @properties={typeid:24,uuid:"794D5A3A-5462-45E6-B1C3-6FACE017B07C"}
 */
function onCurrentProgettoChange() { 
	//FS
//dep	databaseManager.removeTableFilterParam(controller.getServerName(), "FILTRO");
	databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), "FILTRO");
	resetVariables();
	globals.vc2_currentVersione = null;
	application.updateUI();
	if (globals.vc2_currentProgetto == null) {
		elements.vc2_currentVersione.enabled = false;
	} else {
		elements.vc2_currentVersione.enabled = true;
	}
	progettiTreeViewSetRoots();
}

/**
 *
 * @properties={typeid:24,uuid:"E9455E8F-9107-488E-85FD-DF337E19BF4C"}
 */
function onCurrentVersioneChange() {
	//FS
//dep	databaseManager.removeTableFilterParam(controller.getServerName(), "FILTRO");
	databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), "FILTRO");

	resetVariables();
	progettiTreeViewSetRoots();
}

/**
 *
 * @properties={typeid:24,uuid:"2D3E5159-BAA9-4DDC-BE63-53ECC5CAE95D"}
 */
function onLoad() {
	//FS
//dep	var progettiTreeViewBinding = elements.progettiTreeView.createBinding(controller.getServerName(), controller.getTableName());
	var progettiTreeViewBinding = elements.progettiTreeView.createBinding(databaseManager.getDataSourceServerName(controller.getDataSource()),databaseManager.getDataSourceTableName(controller.getDataSource()));
	progettiTreeViewBinding.setTextDataprovider("descrizione");
	progettiTreeViewBinding.setNRelationName("k8_distinte_progetto$figli");
	progettiTreeViewBinding.setMethodToCallOnClick(onClick, "k8_distinte_progetto_id");
	progettiTreeViewBinding.setImageURLDataprovider("treeIconURL");

//dep	elements.progettiTreeView.createColumn(controller.getServerName(), controller.getTableName(), "Numero Disegno", "numero_disegno_str");
//dep	elements.progettiTreeView.createColumn(controller.getServerName(), controller.getTableName(), "Esp.", "esponente_str");
//dep	elements.progettiTreeView.createColumn(controller.getServerName(), controller.getTableName(), "Qta", "coefficiente_impiego_str");
//dep	elements.progettiTreeView.createColumn(controller.getServerName(), controller.getTableName(), "U. Mis.", "unita_misura_peso");
//dep	elements.progettiTreeView.createColumn(controller.getServerName(), controller.getTableName(), "P. Unitario", "peso_unitario");
//dep	elements.progettiTreeView.createColumn(controller.getServerName(), controller.getTableName(), "Peso", "peso");
//dep	elements.progettiTreeView.createColumn(controller.getServerName(), controller.getTableName(), "Materiale", "descrizione_materiale");

	
	elements.progettiTreeView.createColumn(controller.getDataSource(), "Numero Disegno", "numero_disegno_str");
	elements.progettiTreeView.createColumn(controller.getDataSource(), "Esp.", "esponente_str");
	elements.progettiTreeView.createColumn(controller.getDataSource(), "Qta", "coefficiente_impiego_str");
	elements.progettiTreeView.createColumn(controller.getDataSource(), "U. Mis.", "unita_misura_peso");
	elements.progettiTreeView.createColumn(controller.getDataSource(), "P. Unitario", "peso_unitario");
	elements.progettiTreeView.createColumn(controller.getDataSource(), "Peso", "peso");
	elements.progettiTreeView.createColumn(controller.getDataSource(), "Materiale", "descrizione_materiale");	
}

/**
 *
 * @properties={typeid:24,uuid:"B55F5DB1-1CD5-4D5C-B18E-9C84AE9E113B"}
 */
function printTreeLine(r) {

	var indentation = "";
	for (var i = 0; i < r.livello - initialLevel; i++) {
		indentation += "     ";
	}

	buffer += indentation + r.descrizione + "\t";
	buffer += r.numero_disegno_str + "\t";
	buffer += r.esponente_str + "\t";
	buffer += r.coefficiente_impiego_str + "\t";
	buffer += r.unita_misura_peso + "\t";
	buffer += r.peso_unitario + "\t";
	buffer += r.peso + "\t";
	buffer += r.descrizione_materiale + "\t";
	buffer += r.path_numero_disegno + "\t";
	buffer += "\n";
	globals.nfx_progressBarStep();
}

/**
 *
 * @properties={typeid:24,uuid:"AB018878-F879-41AC-A5F0-F8348D42FE9E"}
 * @AllowToRunInFind
 */
function progettiTreeViewSetRoots() {
	if (globals.vc2_currentProgetto && globals.vc2_currentVersione) {
		if (controller.find()) {
			livello = 0;
			progetto = globals.vc2_currentProgetto;
			codice_vettura = java.lang.Double.parseDouble(String(globals.vc2_currentVersione));
			controller.search();
		}
		elements.progettiTreeView.removeAllRoots();
		elements.progettiTreeView.addRoots(foundset);

		showTree();
	} else {
		globals.vc2_progettiCurrentNode = -1;
		globals.vc2_progettiCurrentNodeNumeroDisegno = -1;
		globals.vc2_progettiCurrentNodePath = null;
		globals.vc2_progettiCurrentNodeFoundset = null;

		hideTree();
	}

	setPath();
}

/**
 *
 * @properties={typeid:24,uuid:"C53540E6-DB7C-4788-A42A-93080EB5B358"}
 */
function resetVariables() {
	showPathDraw = false;

	filterDescription = null;
	filterTag = null;
	filterDraw = null;
}

/**
 *
 * @properties={typeid:24,uuid:"D26E95EB-6C04-4F14-9520-A087514C2D27"}
 */
function setPath() {
	if (globals.vc2_progettiCurrentNode == -1) {
		path = "Selezionare un componente...";
	} else {
		path = (showPathDraw) ? "Percorso: " + path_numero_disegno : "Percorso: " + path_descrizione;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"79761824-7541-4D4F-88C7-95E9B17FDAD3"}
 */
function setPathOnAction() {
	showPathDraw = !showPathDraw;
	setPath();
}

/**
 *
 * @properties={typeid:24,uuid:"475C8F63-5B20-4B2F-934E-583EAC39A4FD"}
 */
function showTree() {
	elements.progettiTreeView.visible = true;
	elements.warning.visible = false;
	elements.filterDescription1.enabled = true;
	elements.filterDraw1.enabled = true;
	elements.filterTag1.enabled = true;
}

/**
 *
 * @properties={typeid:24,uuid:"5DB42FD7-E47F-477E-8FC8-6E9F9F45D275"}
 * @AllowToRunInFind
 */
function upDateFilter() {
	if (filterDescription == "") {
		filterDescription = null;
		application.updateUI();
	}

	if (filterDraw == "") {
		filterDraw = null;
		application.updateUI();
	}

	if (filterTag == "") {
		filterTag = null;
		application.updateUI();
	}

	if (filterDescription || filterDraw || filterTag) // qui verrà aggiunta il controllo sulla presenza di altri eventuali filtri in OR
	{
		//FS
//dep		databaseManager.removeTableFilterParam(controller.getServerName(), "FILTRO");
		databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource())   , "FILTRO");

		var idsToKeep = new Array();

		var currentVersione = java.lang.Double.parseDouble(String(globals.vc2_currentVersione));
		var codiceVersione = currentVersione.toLocaleString();

		var queryPath = "select path_id from k8_distinte_progetto where progetto = ? and codice_vettura = ?";
		var args = [globals.vc2_currentProgetto,
			codiceVersione];

		var words = null;

		if (filterDraw) {
			var proLike = "%/" + filterDraw + "%";
			queryPath += " and path_numero_disegno like ?";
			args.push(proLike);
		}

		if (filterDescription) {
			words = filterDescription.split(" ");
			for (var i = 0; i < words.length; i++) {
				queryPath += " and upper(descrizione) like ?";
				var descLike = "%" + words[i].toUpperCase() + "%";
				args.push(descLike);
			}
		}

		if (filterTag) {
			queryPath = queryPath.replace("k8_distinte_progetto", "k8_distinte_progetto inner join k8_tag on k8_distinte_progetto_id = record_fk and '" + tableName + "' = table_name_fk");
			words = filterTag.split(" ");
			for (var i2 = 0; i2 < words.length; i2++) {
				queryPath += " and upper(tag) like ?";
				var tagLike = "%" + words[i2].toUpperCase() + "%";
				args.push(tagLike);
			}
		}
//FS

//dep		var dataSet = databaseManager.getDataSetByQuery(controller.getServerName(), queryPath, args, -1);
		var dataSet = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), queryPath, args, -1);

		
		//SAuc
		//var path = dataSet.getColumnAsArray(1);
		path = dataSet.getColumnAsArray(1);

		if (path.length != 0) {
			for (var i3 = 0; i3 < path.length; i3++) {
				if (path[i3]) {
					//if(idsToKeep.length > 200) break; //serve per limitare i tempi d'attesa
					var idsTmp = path[i3].split("/");
					for (var j = 1; j < idsTmp.length; j++) {
						//if(idsToKeep.length > 200) break; //serve per limitare i tempi d'attesa
						if (idsToKeep.indexOf(idsTmp[j]) == -1) idsToKeep.push(idsTmp[j]);
						if (j == idsTmp.length - 1) {
							var like = "%/" + idsTmp[j] + "/%"

							var queryLike = "select k8_distinte_progetto_id from k8_distinte_progetto where progetto = ? and codice_vettura = ? and path_id like ?";
							args = [globals.vc2_currentProgetto,
							codiceVersione,
							like];
//FS
//dep						dataSet = databaseManager.getDataSetByQuery(controller.getServerName(), queryLike, args, -1);
							dataSet = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), queryLike, args, -1);

							for (var k = 1; k <= dataSet.getMaxRowIndex(); k++) {
								if (idsToKeep.indexOf(dataSet.getValue(k, 1)) == -1) idsToKeep.push(dataSet.getValue(k, 1));
								if (idsToKeep.length > 200) break; //serve per limitare i tempi d'attesa
							}
						}
					}
				}
			}

//			application.output(idsToKeep.length);
			if (idsToKeep.length <= 200) // è il massino consentito dalla clausula "IN"
			{
				//FS
//dep				databaseManager.addTableFilterParam(controller.getServerName(), controller.getTableName(), "k8_distinte_progetto_id", "IN", idsToKeep, "FILTRO");
				databaseManager.addTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()),databaseManager.getDataSourceTableName(controller.getDataSource()), "k8_distinte_progetto_id", "IN", idsToKeep, "FILTRO");

				if (controller.find()) {
					livello = 0;
					progetto = globals.vc2_currentProgetto;
					codice_vettura = java.lang.Double.parseDouble(String(globals.vc2_currentVersione));
					controller.search();
				}

				elements.progettiTreeView.removeAllRoots();
				elements.progettiTreeView.addRoots(foundset);
			} else {
				if (!changeRoot())
					plugins.dialogs.showWarningDialog("Attenzione", "Selezionare criteri di ricerca più restrittivi", "Ok");
			}
		} else {
			plugins.dialogs.showWarningDialog("Attenzione", "Non sono presenti componenti che corrispondono ai criteri di ricerca", "Ok");
		}
	} else {
		//FS
//dep		databaseManager.removeTableFilterParam(controller.getServerName(), "FILTRO");
		databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), "FILTRO");
		controller.loadAllRecords();

		if (controller.find()) {
			livello = 0;
			progetto = globals.vc2_currentProgetto;
			codice_vettura = java.lang.Double.parseDouble(String(globals.vc2_currentVersione));
			controller.search();
		}
		elements.progettiTreeView.removeAllRoots();
		elements.progettiTreeView.addRoots(foundset);
	}
}
