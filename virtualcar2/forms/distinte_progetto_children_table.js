/**
 *
 * @properties={typeid:24,uuid:"69D1BD74-CB27-4B82-9014-00D48A82B6C8"}
 */
function nfx_getTitle()
{
	return "Figli";
}

/**
 * @properties={typeid:24,uuid:"89C2F6C1-E6F9-4F0A-9040-3AA8AF414871"}
 */
function nfx_relationModificator()
{
	return "figli";
}
