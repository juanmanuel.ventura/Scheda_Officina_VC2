/**
 *
 * @properties={typeid:24,uuid:"75DE1EF4-630C-4B9F-BD5D-B4A26B9D6084"}
 */
function nfx_defineAccess()
{
	return [false,false,true];
}

/**
 *
 * @properties={typeid:24,uuid:"5F9636DE-115A-4427-B7FE-CB158CD7E991"}
 */
function nfx_getTitle()
{
	return "Dettaglio";

}

/**
 *
 * @properties={typeid:24,uuid:"7BA41C54-8065-4299-B5EC-0820986D3B83"}
 */
function nfx_relationModificator()
{
	if(forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero.controller.getName())
	{
		return "alberoprogetti";
	}
	else
	{
		return null;
	}
}
