/**
 *
 * @properties={typeid:24,uuid:"E029C1B6-3450-4B1E-B41C-82FA7A2CE86B"}
 */
function nfx_getTitle()
{
	return "Note";
}

/**
 *
 * @properties={typeid:24,uuid:"6ADB8C2A-BD8D-4065-A6E8-69F5560513AD"}
 */
function nfx_isProgram()
{
	return false;
}
