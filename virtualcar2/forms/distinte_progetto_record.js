/**
 *
 * @properties={typeid:24,uuid:"0F4FF91F-52A8-4A7E-8848-6D10458B4D52"}
 */
function nfx_getTitle()
{
	return "Distinte Progetto";
}

/**
 *
 * @properties={typeid:24,uuid:"8E5AE857-F853-4783-9D02-4204897E0455"}
 */
function nfx_isProgram()
{
	return true;
}
