/**
 *
 * @properties={typeid:24,uuid:"83F5490B-B4F6-4CFB-B17D-0DE5E9B9F811"}
 */
function nfx_getTitle()
{
	return "Distinte Progetto - Tabella";
}

/**
 *
 * @properties={typeid:24,uuid:"AE091D15-2B58-4782-8EC7-B63DB476FEE2"}
 */
function nfx_isProgram()
{
	return true;
}
