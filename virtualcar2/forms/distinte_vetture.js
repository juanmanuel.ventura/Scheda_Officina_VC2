/**
 * @properties={typeid:35,uuid:"B50DDF50-9B51-4AC2-8FCB-936F812953BA",variableType:-4}
 */
var nfx_related = ["distinte_vetture_children_table", "distinte_vetture_note_record", "time_line_table", "tag_table"];

/**
 * @properties={typeid:35,uuid:"D4DFF028-C159-40BB-B514-19725C4694E3",variableType:-4}
 */
var rv = {icon: "refresh2.png", tooltip:"!!!BETA!!! - Refresh vettura", method:"refreshVetturaWrap"};

/**
 *
 * @properties={typeid:24,uuid:"DED04FD8-CFE4-4847-925A-722D21E25C3D"}
 */
function nfx_defineAccess()
{
	return [true,true,true,];
}

/**
 *
 * @properties={typeid:24,uuid:"839A649C-5AA2-4252-B9F1-9C8AAC1BE104"}
 */
function nfx_relationModificator()
{

	if(forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero.controller.getName())
	{
		return "alberoprogetti";
	}
	else
	{
		return null;
	}

}

/**
 *
 * @properties={typeid:24,uuid:"8F3C79A0-B510-4538-96D1-85DC6AC97591"}
 */
function nfx_sks()
{
	return [rv];
}

/**
 *
 * @properties={typeid:24,uuid:"35166393-07B9-4F9B-807D-93ECD5D7F038"}
 */
function refreshVettura(vet,pr)
{
	var v = vet || vettura;
	var p = pr || progetto;
	//FS
	
//dep	plugins.rawSQL.executeStoredProcedure(controller.getServerName(),'{call k8_refresh_coeff_vettura(?,?)}',[v, p],[0,0],0);
	plugins.rawSQL.executeStoredProcedure(databaseManager.getDataSourceServerName(controller.getDataSource()),'{call k8_refresh_coeff_vettura(?,?)}',[v, p],[0,0],0);

	databaseManager.refreshRecordFromDatabase(k8_distinte_vetture_to_k8_distinte_vetture_coeff,-1);
	globals.nfx_syncTabs();
}

/**
 *
 * @properties={typeid:24,uuid:"AB03D666-0542-4326-ABFC-CA1ADBD03950"}
 */
function refreshVetturaWrap()
{
	var inizio = new Date();
	refreshVettura(null,null);
	var fine = new Date();
	var tempo = fine - inizio;
	plugins.dialogs.showInfoDialog("Finito","Coefficienti per " + progetto + " vettura " + vettura + " aggiornati in: " + (tempo/1000).toFixed(2) + " secondi","Ok");
}
