dataSource:"db:/ferrari/k8_distinte_vetture",
extendsID:"6821519D-E265-434B-BDEC-6A89A9AFF03C",
items:[
{
anchors:12,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"valori",
displayType:4,
location:"100,200",
name:"valori1",
onActionMethodID:"-1",
onDataChangeMethodID:"5C161B90-0FF8-42EB-B6F6-BCB45EA3BFA2",
size:"20,20",
transparent:true,
typeid:4,
uuid:"0CC213CF-5C20-4A1F-92E3-662533375881"
},
{
anchors:12,
labelFor:"valori1",
location:"10,200",
mediaOptions:14,
size:"90,20",
styleClass:"form",
tabSeq:-1,
text:"Mostra Valori:",
transparent:true,
typeid:7,
uuid:"0E959427-8381-44FD-A6FF-5666443956AE"
},
{
anchors:12,
location:"110,250",
mediaOptions:14,
onActionMethodID:"AC52C193-A40F-47DA-8521-E52249CF2A58",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
size:"80,20",
text:"Salva",
typeid:7,
uuid:"18810CFF-F9FE-4848-8759-95FC7CDF6D30"
},
{
anchors:15,
beanClassName:"com.objectplanet.chart.ext.TimeLineChart",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_17\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"com.objectplanet.chart.ext.TimeLineChart\"> \
  <void property=\"background\"> \
   <object class=\"java.awt.Color\"> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>255<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>400<\/int> \
    <int>280<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"autoLabelSpacingOn\"> \
   <boolean>true<\/boolean> \
  <\/void> \
  <void property=\"autoTimeLabelsOn\"> \
   <boolean>true<\/boolean> \
  <\/void> \
  <void property=\"automaticRepaintOn\"> \
   <boolean>false<\/boolean> \
  <\/void> \
  <void property=\"chartData\"> \
   <void id=\"StringArray0\" property=\"seriesLabels\"/> \
   <void property=\"seriesLabels\"> \
    <object idref=\"StringArray0\"/> \
   <\/void> \
  <\/void> \
  <void property=\"focusable\"> \
   <boolean>false<\/boolean> \
  <\/void> \
  <void property=\"legendBoxSizeAsFont\"> \
   <boolean>true<\/boolean> \
  <\/void> \
  <void id=\"StringArray1\" property=\"legendLabels\"/> \
  <void property=\"legendLabels\"> \
   <object idref=\"StringArray1\"/> \
  <\/void> \
  <void property=\"legendOn\"> \
   <boolean>true<\/boolean> \
  <\/void> \
  <void property=\"lowerTime\"> \
   <string>01/01/1970<\/string> \
  <\/void> \
  <void property=\"name\"> \
   <string>chart<\/string> \
  <\/void> \
  <void property=\"requestFocusEnabled\"> \
   <boolean>false<\/boolean> \
  <\/void> \
  <void id=\"StringArray2\" property=\"seriesLabels\"/> \
  <void property=\"seriesLabels\"> \
   <object idref=\"StringArray2\"/> \
  <\/void> \
  <void property=\"timeFormatInput\"> \
   <string>dd-MM-yyyy<\/string> \
  <\/void> \
  <void property=\"timeFormatOut\"> \
   <string>dd/MM/yyyy<\/string> \
  <\/void> \
  <void property=\"zoomOn\"> \
   <boolean>true<\/boolean> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:-1,
location:"200,0",
name:"chart",
size:"400,280",
typeid:12,
uuid:"23C6C6E0-4DEC-460E-AC70-4B49975F4A6A"
},
{
dataProviderID:"bgColorUp",
displayType:2,
editable:false,
location:"80,90",
name:"bgColorUp1",
onDataChangeMethodID:"0C6EE47E-81E8-4C42-BBAB-3F27885C1C9B",
size:"110,20",
text:"Bgcolorup",
typeid:4,
uuid:"3A2EE7CD-65DC-430A-829B-386B536AF51C",
valuelistID:"4D85936C-004A-4DE4-A88F-7AA6B4871957"
},
{
anchors:15,
horizontalAlignment:0,
location:"240,60",
mediaOptions:6,
name:"warning",
showFocus:false,
size:"300,160",
styleClass:"form",
tabSeq:-1,
text:"Non sono presenti dati per disegnare il grafico",
transparent:true,
typeid:7,
uuid:"3CF02887-1BD2-4A3D-8D02-26D1F13C038C",
verticalAlignment:0
},
{
horizontalAlignment:4,
labelFor:"bgColorDown1",
location:"10,110",
mediaOptions:14,
size:"60,20",
styleClass:"form",
tabSeq:-1,
text:"Colore 2:",
transparent:true,
typeid:7,
uuid:"41E655F8-5C51-4830-B9F8-7F68694A3278"
},
{
anchors:12,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"linee",
displayType:4,
location:"100,220",
name:"linee1",
onDataChangeMethodID:"5C161B90-0FF8-42EB-B6F6-BCB45EA3BFA2",
size:"20,20",
transparent:true,
typeid:4,
uuid:"4869E18D-9FB5-4C43-8713-017728626636"
},
{
anchors:13,
borderType:"TitledBorder,Opzioni,4,0,DialogInput.plain,1,12,#333333",
formIndex:-3,
lineSize:1,
location:"0,0",
size:"200,280",
transparent:true,
typeid:21,
uuid:"6D8AE07C-BB4E-45A5-907F-9AD98E2705E4"
},
{
dataProviderID:"scala",
displayType:2,
editable:false,
horizontalAlignment:4,
location:"80,60",
name:"scala1",
onDataChangeMethodID:"5C161B90-0FF8-42EB-B6F6-BCB45EA3BFA2",
size:"110,20",
text:"Scala",
typeid:4,
uuid:"71A8C274-8806-48DA-BCFA-A74D3290599F",
valuelistID:"C37D9654-8480-4865-9D59-60B6391E9599"
},
{
horizontalAlignment:4,
labelFor:"bgColorUp1",
location:"10,90",
mediaOptions:14,
size:"60,20",
styleClass:"form",
tabSeq:-1,
text:"Colore 1:",
transparent:true,
typeid:7,
uuid:"8C68F2CC-DB4E-4792-BB7E-A4ACB7BE631F"
},
{
location:"170,30",
mediaOptions:14,
name:"titleFont",
onActionMethodID:"AB731F6D-8CA9-498B-8084-48AB735C64D3",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
size:"20,20",
text:"...",
typeid:7,
uuid:"8E209E78-0585-4053-A289-D6EAD999E34E"
},
{
height:480,
partType:5,
typeid:19,
uuid:"8F376D9A-D122-44A8-AA87-2E5DB6A35A46"
},
{
anchors:12,
labelFor:"linee1",
location:"10,220",
mediaOptions:14,
size:"90,20",
styleClass:"form",
tabSeq:-1,
text:"Mostra Griglia:",
transparent:true,
typeid:7,
uuid:"AF3ED92C-954F-4E61-B0D2-68D2730490D5"
},
{
dataProviderID:"bgColorDown",
displayType:2,
editable:false,
location:"80,110",
name:"bgColorDown1",
onDataChangeMethodID:"DB2D930E-740E-4537-A082-B2EFDE3C9B53",
size:"110,20",
text:"Bgcolordown",
typeid:4,
uuid:"E351E2B9-1FA2-4167-B6A3-9FD8BC0E8606",
valuelistID:"4D85936C-004A-4DE4-A88F-7AA6B4871957"
},
{
horizontalAlignment:4,
labelFor:"scala1",
location:"10,60",
mediaOptions:14,
size:"60,20",
styleClass:"form",
tabSeq:-1,
text:"Scala:",
transparent:true,
typeid:7,
uuid:"E6A05033-F585-439B-9A5F-35FF2EB2A4CA"
},
{
dataProviderID:"titolo",
location:"80,30",
name:"titolo1",
onDataChangeMethodID:"BE6C1DF0-7E7F-4A29-AD1A-2A32E4568817",
size:"90,20",
text:"Titolo",
typeid:4,
uuid:"E6D7772D-8C2B-402F-8464-95E2C6C97727"
},
{
horizontalAlignment:4,
labelFor:"titolo1",
location:"10,30",
mediaOptions:14,
size:"60,20",
styleClass:"form",
tabSeq:-1,
text:"Titolo:",
transparent:true,
typeid:7,
uuid:"E730776C-FAAE-40FC-A301-C9AD4D630A5D"
}
],
name:"distinte_vetture_chart",
onLoadMethodID:"CCAE05F1-7823-457A-AA8A-E0B3EB926FE2",
paperPrintScale:100,
showInMenu:true,
size:"600,480",
styleName:"keeneight",
typeid:3,
uuid:"8D5843BE-696F-40CF-BF75-21457EAF3570"