/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A36F4308-0676-4C49-A1AB-48E06EBA6595"}
 */
var bgColorDown = "Bianco";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1CF4B2E1-5AC4-4329-990D-3B9597C928EB"}
 */
var bgColorUp = "Bianco";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"AF0757D6-503A-4635-9218-6E86ECFB7189",variableType:4}
 */
var linee = 0;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"F36623F6-FD01-4FFE-8EA4-1E3E3FC14F8B",variableType:4}
 */
var scala = 604800;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F8D92A60-D9CE-4F00-8F40-BF4738F97422"}
 */
var titolo = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"71E75FF3-2706-447F-BBB0-C75EB3D16B8D",variableType:4}
 */
var valori = 0;

/**
 *@param {JSEvent} event
 * @properties={typeid:24,uuid:"AB731F6D-8CA9-498B-8084-48AB735C64D3"}
 */
function chooseFont(event) {
	//FS
	//dep	var element = application.getMethodTriggerElementName();
	var element = event.getElementName();
	/** @type {java.awt.chart} */
	var chart = elements.chart;
	var font = null;
	if (chart.getFont(element)) {
		/** @type {java.awt.Font} */		
		var oldFont = chart.getFont(element);
		font = application.showFontChooser(oldFont.getName() + "," + oldFont.getStyle() + "," + oldFont.getSize());
	} else {
		font = application.showFontChooser();
	}
	if (font) {
		var fontArray = font.split(",");
		var jFont = new java.awt.Font(fontArray[0], fontArray[1], fontArray[2]);
		chart.setFont(element, jFont);
		drawChart();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"5C161B90-0FF8-42EB-B6F6-BCB45EA3BFA2"}
 */
function drawChart() {
	/** @type {JSFoundSet} */
	var coefficienti = k8_distinte_vetture_to_k8_distinte_vetture_coeff;
	if (coefficienti && coefficienti.getSize() > 0) {
		resetChart();
		showChart(coefficienti);
	} else {
		hideChart();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"176CAD10-AC3D-49BB-9C15-B13429F0D272"}
 */
function getColor(bgColor) {

	var daRit = null;

	if (bgColor == "Bianco")
		daRit = java.awt.Color.WHITE;
	if (bgColor == "Azzurro")
		daRit = java.awt.Color.cyan;
	if (bgColor == "Rosa")
		daRit = java.awt.Color.pink;
	if (bgColor == "Verde")
		daRit = java.awt.Color.GREEN;
	if (bgColor == "Rosso")
		daRit = java.awt.Color.RED;
	if (bgColor == "Giallo")
		daRit = java.awt.Color.YELLOW;
	if (bgColor == "Blu")
		daRit = java.awt.Color.BLUE;

	return daRit;
}

/**
 *
 * @properties={typeid:24,uuid:"3A0AF5B1-B782-403B-B80D-D4EDEE3A920E"}
 */
function hideChart() {
	var chart = elements.chart;
	var warning = elements.warning;

	chart.visible = false;
	warning.visible = true;
	application.updateUI();
}

/**
 *
 * @properties={typeid:24,uuid:"C45BE81C-733A-4B06-B28E-DDACAD30A0D1"}
 */
function nfx_getTitle() {
	return "Grafico";

}

/**
 *
 * @properties={typeid:24,uuid:"9593D4F2-BB5D-4C8C-95E6-FE859BE03E17"}
 */
function nfx_isProgram() {

	return false;

}

/**
 *
 * @properties={typeid:24,uuid:"A8E9656F-6B4A-4F7F-97A5-4057274EBAA1"}
 */
function nfx_onRecordSelection() {
	drawChart();
}

/**
 *
 * @properties={typeid:24,uuid:"CCAE05F1-7823-457A-AA8A-E0B3EB926FE2"}
 */
function onLoad() {
	//FS
	/** @type {java.awt.chart} */
	var chart = elements.chart;
	chart.setLabelAngle("sampleLabelAngle", 290);
	chart.setTimeFormatInput("dd-MM-yyyy");
	chart.setTimeFormatOut("dd/MM/yyyy");
}

/**
 *
 * @properties={typeid:24,uuid:"E357ABA3-D590-43DD-B270-A3B861061B40"}
 */
function resetChart() {
	/** @type {java.awt.chart} */
	var chart = elements.chart;
	chart.setSeriesCount(0);
	chart.forceRepaint();
}

/**
 *@param {JSEvent} event
 * @properties={typeid:24,uuid:"AC52C193-A40F-47DA-8521-E52249CF2A58"}
 */
function saveAsImage(event) {
	//FS
	/** @type {java.awt.chart} */
	var chart =elements.chart;
	//var image = elements.chart.getImage(elements.chart.getWidth(), elements.chart.getHeight());
	var image = chart.getImage(chart.getWidth(),chart.getHeight());
	globals.utils_saveAsPng(image, "chart.png");
}

/**
 *
 * @properties={typeid:24,uuid:"BE6C1DF0-7E7F-4A29-AD1A-2A32E4568817"}
 */
function setChartTitle() {
	/** @type {java.awt.chart} */
	var chart = elements.chart;
	if (titolo) {
		chart.setTitleOn(true);
		chart.setTitle(titolo);
	} else {
		chart.setTitleOn(false);
	}
	drawChart();
}

/**
 *
 * @properties={typeid:24,uuid:"DB2D930E-740E-4537-A082-B2EFDE3C9B53"}
 */
function setColorDown() {
	/** @type {java.awt.chart} */
	var chart = elements.chart;
	//elements.chart.setChartBackground2(getColor(bgColorDown));
	chart.setChartBackground2(getColor(bgColorDown));
	drawChart();
}

/**
 *
 * @properties={typeid:24,uuid:"0C6EE47E-81E8-4C42-BBAB-3F27885C1C9B"}
 */
function setColorUp() {
	/** @type {java.awt.chart} */
	var chart = elements.chart;
	//elements.chart.setChartBackground(getColor(bgColorUp));
	chart.setChartBackground(getColor(bgColorUp));
	drawChart();
}

/**
 *@param {JSFoundSet} fe
 * @properties={typeid:24,uuid:"D5E1F07B-0244-4373-9838-215D9342DD66"}
 */
function showChart(fe) {
	//FS
	/** @type {JSFoundSet} */
	var coefficienti = fe.duplicateFoundSet();
	/** @type {java.awt.chart} */
	var chart = elements.chart;
	var warning = elements.warning;
	var queryMin = "select min(DATA) from K8_DISTINTE_VETTURE_COEFF_PROG where FK_DISTINTE_VETTURE_COEFF = ?";
	//dep	var dsMin = databaseManager.getDataSetByQuery(controller.getServerName(),queryMin,[coefficienti.k8_distinte_vetture_coeff_id],1);
	var dsMin = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), queryMin, [coefficienti.k8_distinte_vetture_coeff_id], 1);
	chart.setLowerTime(utils.dateFormat(dsMin.getValue(1, 1), "dd-MM-yyyy"));
	var queryMax = "select max(DATA) from K8_DISTINTE_VETTURE_COEFF_PROG where FK_DISTINTE_VETTURE_COEFF = ?";
	//dep	var dsMax = databaseManager.getDataSetByQuery(controller.getServerName(),queryMax,[coefficienti.k8_distinte_vetture_coeff_id],1);
	var dsMax = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), queryMax, [coefficienti.k8_distinte_vetture_coeff_id], 1);
	chart.setUpperTime(utils.dateFormat(dsMax.getValue(1, 1), "dd-MM-yyyy"));

	try {
		chart.setTimeScale(scala);
	} catch (e) {
		//application.output("Sono uscito sul try");
		plugins.dialogs.showErrorDialog("Errore", "La scala utilizzata è inadatta ai valori da rappresentare,\nSelezionare una scala più bassa.", "Ok");
		return;
	}

	chart.setSeriesCount(coefficienti.getSize());
	var legendLabels = new Array();
	var max = 0;
	for (var i = 1; i <= coefficienti.getSize(); i++) {
		coefficienti.setSelectedIndex(i);
		var query = "select to_char(DATA,'dd-MM-yyyy'), VALORE_PROGRESSIVO from K8_DISTINTE_VETTURE_COEFF_PROG where FK_DISTINTE_VETTURE_COEFF = ? order by DATA asc";
		//FS

		//dep		var ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,[coefficienti.k8_distinte_vetture_coeff_id],-1);
		var ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, [coefficienti.k8_distinte_vetture_coeff_id], -1);

		var timePlots = new Array();
		for (var j = 1; j <= ds.getMaxRowIndex(); j++) {
			timePlots.push(ds.getValue(j, 1) + "|" + ds.getValue(j, 2));
		}
		chart.setTimePlots(i - 1, timePlots);
		chart.setConnectedLinesOn(i - 1, true);
		
		if (coefficienti.k8_distinte_vetture_coeff_to_k8_distinte_progetto_coeff.f_rilevante == "Rilevante") {
			chart.setLineWidth(i - 1, 4);
		} else {
			chart.setLineWidth(i - 1, 2);
		}
		legendLabels.push(coefficienti.coefficiente);
		if (max < ds.getValue(ds.getMaxRowIndex(), 2)) max = ds.getValue(ds.getMaxRowIndex(), 2);
	}
	chart.setLegendLabels(legendLabels);
	max -= max % 100;
	max += 200;
	chart.setRange(0, max);

	//Impostazioni grafiche condizionate
	showValues();
	showGridLines();

	chart.forceRepaint();

	chart.visible = true;
	warning.visible = false;
	application.updateUI();
}

/**
 *
 * @properties={typeid:24,uuid:"621D7766-67CF-4385-8E32-E49333A9B547"}
 */
function showGridLines() {
	//FS
	/** @type {java.awt.chart} */
	var chart = elements.chart;
	if (linee == 1) {
		chart.setValueLinesOn(true);
		chart.setDefaultGridLinesOn(true);
	} else {
		chart.setValueLinesOn(false);
		chart.setDefaultGridLinesOn(false);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"E70E2380-B620-4644-A919-530A766A4FCC"}
 */
function showValues() {
	//FS
	/** @type {java.awt.chart} */
	var chart = elements.chart;
	if (valori == 1) {
		chart.setValueLabelsOn(true);
	} else {
		chart.setValueLabelsOn(false);
	}
}
