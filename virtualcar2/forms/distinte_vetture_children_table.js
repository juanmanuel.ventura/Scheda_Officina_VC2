/**
 *
 * @properties={typeid:24,uuid:"58443594-A97F-4DB8-80C1-D14B4D2A9C77"}
 */
function nfx_getTitle()
{
	return "Figli";
}

/**
 *
 * @properties={typeid:24,uuid:"080A9B9F-581D-457B-BDCF-82336DF66FDB"}
 */
function nfx_isProgram()
{
	return false;
}

/**
 * @properties={typeid:24,uuid:"6BB535B6-C127-4270-95DE-189DDE691732"}
 */
function nfx_relationModificator()
{
	return "figli";
}
