/**
 *
 * @properties={typeid:24,uuid:"51DE1A91-1B4F-4A7E-AF6B-BDEEB0FB5FC1"}
 */
function nfx_getTitle()
{
	return "Note";
}

/**
 *
 * @properties={typeid:24,uuid:"F22D10A2-F8C7-42BC-8DC6-3F821C1E70EE"}
 */
function nfx_isProgram()
{
	return false;
}
