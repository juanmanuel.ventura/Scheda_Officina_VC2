/**
 *
 * @properties={typeid:24,uuid:"57C1F110-DB02-47BD-8180-550DCDB44C2A"}
 */
function nfx_getTitle()
{
	return "Distinte Vetture - Tabella";
}

/**
 *
 * @properties={typeid:24,uuid:"A7CB1E9D-668A-496E-B1F9-38A8ACE1E0D4"}
 */
function nfx_isProgram()
{
	return true;
}
