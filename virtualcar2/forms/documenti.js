/**
 *
 * @properties={typeid:24,uuid:"59C72B7D-784A-4C95-9A2B-E9EDC47AB4E6"}
 */
function doThat()
{
	if(allegato)
		download();
	else
		upload();
}

/**
 *
 * @properties={typeid:24,uuid:"33F3386B-BD07-48F6-83D0-EAF0964A4E32"}
 */
function download()
{
	if(!forms.nfx_interfaccia_pannello_buttons.elements.save.enabled){
	var fileDownload = plugins.file.showFileSaveDialog(file_name);
	if(fileDownload)
	{
		plugins.file.writeFile(fileDownload,allegato);
	}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"CE2E065F-DBAD-4DD4-AB9F-47BA3344FF98"}
 */
function nfx_defineAccess()
{
	if(globals.nfx_selectedProgram() == controller.getName()){
		return [false,false,false];
	}
	if(globals.nfx_selectedProgram() == forms.albero_progetti_custom_container_s5.controller.getName() && globals.vc2_progettiCurrentNode == -1){
		return [false,true,true];
	}
	return [true,true,true];
}

/**
 *
 * @properties={typeid:24,uuid:"CD50945D-94FF-4F8A-81E8-212950E9A523"}
 */
function upload()
{

	var fileUpload = plugins.file.showFileOpenDialog(1,null,false,null);
	
	if (fileUpload)
	{	
		// Converto in JSFile per salvare il nome
		var jsFile = plugins.file.convertToJSFile(fileUpload);
		var fileName = jsFile.getName();
		file_name = fileName;
		
		// Salvo la dimensione
		var fileDimension = plugins.file.getFileSize(fileUpload);
		dimensione = fileDimension;
		
		// Salvo la data di modifica
		var fileDateModification = plugins.file.getModificationDate(fileUpload);		
		data_modifica_file = fileDateModification;
			
		// Salvo il File vero e proprio in una variabileù
		var realContent = plugins.file.readFile(fileUpload);
		allegato = realContent;
	}
}
