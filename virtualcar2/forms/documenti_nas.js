/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C24992BB-2B1F-4724-A010-8F2A6DCD6BEC"}
 */
var backup_path = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"7D797569-66A2-4D53-83ED-DEB1A4561525"}
 */
var nas_path = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"206A1893-42C3-4B0F-9DE4-B016A2EC3C04"}
 */
var nfx_orderBy = "k8_documenti_nas_id desc";

/**
 * @properties={typeid:35,uuid:"13FF26FB-5E99-415E-97FD-21B04BC03EE9",variableType:-4}
 */
var permitted_extension = ["txt", "pdf", "doc", "xls", "ppt", "pps", "avi", "mpg", "mpeg", "mov", "wmv", "jpg", "jpeg", "bmp", "png", "gif", "mp3", "wav", "wma", "tif", "tiff"];

/**
 * @properties={typeid:24,uuid:"82B5D886-C302-4482-A7FB-B95D9CDA3421"}
 */
function backup() {
	if (path) {
		var path_nas = plugins.file.convertToJSFile(nas_path);
		if (! (path_nas.exists() && path_nas.isDirectory() && path_nas.canRead() && path_nas.canWrite())) {
			plugins.dialogs.showErrorDialog("Errore", nas_path + " irrangiungibile o non configurato correttamente. Operazione annullata.", "Ok");
			return -1;
		}
		var path_temp = nas_path + "TEMP/";
		var path_local = plugins.file.convertToJSFile(path_temp);
		if (! (path_local.exists() && path_local.isDirectory())) {
			plugins.file.createFolder(path_local);
		}
		var path_backup = path_temp + "BKP_" + k8_documenti_nas_id.toString() + "_" + file_name;
		path_local = plugins.file.convertToJSFile(path_backup);
		var result = plugins.file.writeFile(path_local, file);
		if (!result) {
			plugins.dialogs.showErrorDialog("Errore", "Non è possibile scrivere il file in " + path_temp + ". Inserimento annullato.", "Ok");
			return -1;
		}
		backup_path = path_backup;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"08B42C07-6D44-454F-93C6-E4D641371F10"}
 */
function checkExtension(filename) {

	var e = filename.indexOf(".", 0);
	if (e != -1) {
		var i = permitted_extension.length;
		for (var out = i; out >= 0; out--) {

			try{
			if (filename.indexOf("." + permitted_extension[out], 0) != -1 || filename.indexOf("." + permitted_extension[out].toUpperCase(), 0) != -1) {
				return 1;
			}
			}catch(E){
				application.output("Estensione file non valida!");
			}
		}
		return -1;
	} else {
		return -1;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"5480E878-C29C-4381-907D-7E83667EC66F"}
 */
function copyToNAS() {
	var path_local = null;
	var local = plugins.file.showFileOpenDialog(1, null, false, null);
	if (local) {
		var fileName = local.getName();
		if (checkExtension(fileName) == -1) {
			plugins.dialogs.showErrorDialog("Tipo non consentito", "Il file " + fileName + " non può essere allegato. Operazione annullata.", "Ok");
			return -1;
		}
		var path_nas = plugins.file.convertToJSFile(nas_path);
		if (! (path_nas.exists() && path_nas.isDirectory() && path_nas.canRead() && path_nas.canWrite())) {
			plugins.dialogs.showErrorDialog("Errore", nas_path + " irrangiungibile o non configurato correttamente. Operazione annullata.", "Ok");
			return -1;
		}
		var path_project = nas_path + progetto + "/"
		path_local = plugins.file.convertToJSFile(path_project);
		if (! (path_local.exists() && path_local.isDirectory())) {
			plugins.file.createFolder(path_local);
		}
		var path_table = path_project + table_fk + "/";
		path_local = plugins.file.convertToJSFile(path_table);
		if (! (path_local.exists() && path_local.isDirectory())) {
			plugins.file.createFolder(path_local);
		}
		var content = optimizeUploadingFile(local);
		var path_file = path_table + k8_documenti_nas_id.toString() + "_" + fileName;
		path_local = plugins.file.convertToJSFile(path_file);
		var result = plugins.file.writeFile(path_local, content);
		if (!result) {
			plugins.dialogs.showErrorDialog("Errore", "Non è possibile scrivere il file in " + nas_path + ". Inserimento annullato.", "Ok");
			return -1;
		}
		file_name = fileName;
	} else {
		return -1;
	}
	
}

/**
 * @properties={typeid:24,uuid:"18ED2E9B-83B5-4529-AC54-B358CACF6B82"}
 */
function optimizeUploadingFile(_uploadingFile) {
	try {
		var _img = plugins.images.getImage(_uploadingFile);

		var _resizeFactor = 1280;
		var _resizeFactorX = _img.getWidth();
		var _resizeFactorY = _img.getHeight();

		if (Math.max(_resizeFactorX, _resizeFactorY) > _resizeFactor) {
			_resizeFactorX = (_img.getWidth() >= _img.getHeight()) ? _resizeFactor : _img.getWidth() * _resizeFactor / _img.getHeight();
			_resizeFactorY = (_img.getWidth() <= _img.getHeight()) ? _resizeFactor : _img.getHeight() * _resizeFactor / _img.getWidth();
		}

		return _img.resize(_resizeFactorX, _resizeFactorY).getData();
	} catch (e) {
		//It's not an Image file, return the original file
//		application.output(e);
		return plugins.file.readFile(_uploadingFile);
	}
}

/**
 * @return {Number}
 * @properties={typeid:24,uuid:"BDC5A927-9118-4D64-8CAD-05D50CC03BEB"}
 */
function deleteBackupFile() {
	var path_delete = plugins.file.convertToJSFile(backup_path);
	if (path_delete && path_delete.exists()) {
		var result = plugins.file.deleteFile(path_delete);
		if (!result) return -1;
		backup_path = null;
	} else {
		return -1;
	}

}

/**
 *
 * @properties={typeid:24,uuid:"5C6ABFDB-E408-4799-B87C-0CE4B152B3B9"}
 */
function deleteFromNAS() {
	if (path) {
		var result = plugins.file.deleteFile(path);
		if (!result) return -1;
	} else {
		return -1;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"796CCB5C-57D4-4771-9EC8-6BD5A51338FF"}
 */
function nfx_defineAccess() {
	if (globals.nfx_selectedProgram() == controller.getName()) {
		return [false, false, false];
	} else {
		return [true, true, true];
	}
}

/**
 *
 * @properties={typeid:24,uuid:"BADB724F-14EB-4BDB-959A-B61F8EE7A14A"}
 */
function nfx_postAddOnCancel() {
	deleteFromNAS();
}

/**
 *
 * @properties={typeid:24,uuid:"E40F781B-103C-4E1A-8BD4-4927776111B9"}
 */
function nfx_preAdd() {
	//FS
	//dep	k8_documenti_nas_id = databaseManager.getDataSetByQuery(controller.getServerName(),"select ZZ_UTILITY_SEQ.nextval from dual",[],-1).getValue(1,1);
	k8_documenti_nas_id = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), "select ZZ_UTILITY_SEQ.nextval from dual", [], -1).getValue(1, 1);
	return copyToNAS();
}

/**
 *
 * @properties={typeid:24,uuid:"C3B951A7-AFD3-47B6-9F23-E900B8A80D6A"}
 */
function nfx_preRemove() {
	deleteFromNAS();
}

/**
 *
 * @properties={typeid:24,uuid:"BC7A4BE3-C1BF-4278-9CF6-B46A3102C1FE"}
 */
function onLoad() {
	nas_path = globals.vc2_docPath;
}

/**
 *
 * @properties={typeid:24,uuid:"D096DAE0-B4D6-4A97-A116-435D3BE0BC58"}
 */
function restore() {
	var path_restore = plugins.file.convertToJSFile(backup_path);
	if (path_restore && path_restore.exists()) {
		var path_local = null;
		var path_nas = plugins.file.convertToJSFile(nas_path);
		if (! (path_nas.exists() && path_nas.isDirectory() && path_nas.canRead() && path_nas.canWrite())) {
			plugins.dialogs.showErrorDialog("Errore", nas_path + " irrangiungibile o non configurato correttamente. Operazione annullata.", "Ok");
			return -1;
		}
		var path_project = nas_path + progetto + "/"
		path_local = plugins.file.convertToJSFile(path_project);
		if (! (path_local.exists() && path_local.isDirectory())) {
			plugins.file.createFolder(path_local);
		}
		var path_table = path_project + table_fk + "/";
		path_local = plugins.file.convertToJSFile(path_table);
		if (! (path_local.exists() && path_local.isDirectory())) {
			plugins.file.createFolder(path_local);
		}
		var fileName = path_restore.getName().replace("BKP_", "");
		var content = plugins.file.readFile(path_restore);
		var path_file = path_table + fileName;
		path_local = plugins.file.convertToJSFile(path_file);
		var result = plugins.file.writeFile(path_local, content);
		if (!result) {
			plugins.dialogs.showErrorDialog("Errore", "Non è possibile scrivere il file in " + nas_path + ". Inserimento annullato.", "Ok");
			return -1;
		}
		file_name = fileName;
	} else {
		return -1;
	}
}
