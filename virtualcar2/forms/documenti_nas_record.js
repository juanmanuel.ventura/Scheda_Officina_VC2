/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D1D37443-7F5D-4802-AC54-A119737ADB6F"}
 */
var replaced = null;

/**
 *
 * @properties={typeid:24,uuid:"9BA35A55-EBD9-4BE7-A5FD-FE5B044330F8"}
 */
function download(){
	if(path){
		globals.utils_saveAs(file,file_name);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"163B9255-01B1-4A78-A951-4F07A0F0D343"}
 */
function nfx_getTitle(){
	return "Documenti";
}

/**
 *
 * @properties={typeid:24,uuid:"6978E527-BCD2-4A11-BC50-A9380B481DC3"}
 */
function nfx_isProgram(){
	return true;
}

/**
 *
 * @properties={typeid:24,uuid:"1B56F1CA-664D-4F77-B2D7-FB27D2730E31"}
 */
function nfx_onShow(){
	elements.recplace_content.enabled = false;
}

/**
 *
 * @properties={typeid:24,uuid:"3732ED30-2AEE-4915-9DE7-1D3BFF61A1FF"}
 */
function nfx_postEdit(){
	elements.recplace_content.enabled = false;
	if(replaced){
		deleteBackupFile();
		replaced = null;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"A26E21DC-F3DC-4F5F-90CE-C6A7801DD1A0"}
 */
function nfx_postEditOnCancel(){
	elements.recplace_content.enabled = false;
	if(replaced){
		deleteFromNAS();
		restore();
		deleteBackupFile();
		replaced = null;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"C6D59B96-7BF1-429B-9DBD-1E5A4DACB141"}
 */
function nfx_preEdit(){
	elements.recplace_content.enabled = true;
}

/**
 *
 * @properties={typeid:24,uuid:"57D46CB0-00D7-48A7-A20E-DFA6C4E1EC5F"}
 */
function open(){
	if(path){
		try{
			var desktop = java.awt.Desktop.getDesktop();
			desktop.open(new java.io.File(path));
		}catch(e){
			if(application.getOSName().toLowerCase().indexOf("windows") != -1){
				var runtime = java.lang.Runtime.getRuntime();
				runtime.exec("rundll32 SHELL32.DLL,ShellExec_RunDLL \"" + path.toString().replace(/\//g,"\\") + "\"");	
			}else{
				plugins.dialogs.showErrorDialog("Non supportato","La funzione di apertura diretta non è supportata su: " + application.getOSName(),"Ok");
			}
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"AE7AAB81-AE7A-43B6-B256-5B423C0118CB"}
 */
function replace(){
	if(plugins.dialogs.showQuestionDialog("Attenzione","Caricando un nuovo allegato, il precedente andrà perduto, procedere?","Sì","No") == "Sì"){
		backup();
		deleteFromNAS();
		copyToNAS();
		replaced = true;
	}
}
