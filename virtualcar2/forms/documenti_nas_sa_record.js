/**
 * @properties={typeid:24,uuid:"FE28C273-E2DC-47CE-A8D7-C034A111EDD2"}
 */
function nfx_getTitle(){
	return "Documenti (SA)";
}

/**
 * @properties={typeid:24,uuid:"F9CA56A5-B321-477D-BE9F-BF015405C4E1"}
 */
function nfx_relationModificator(){
	return "segn";
}
