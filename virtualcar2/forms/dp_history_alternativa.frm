dataSource:"db:/ferrari/k8_distinte_progetto_alt",
initialSort:"path_id asc",
items:[
{
formIndex:9,
horizontalAlignment:0,
imageMediaID:"410A3185-724A-4CD0-82A2-0CF8C55C8A85",
location:"0,40",
mediaOptions:2,
onActionMethodID:"01422B86-EA58-4291-82DE-1108B00953AB",
showClick:false,
size:"20,20",
transparent:true,
typeid:7,
uuid:"0A0FE593-1E17-4538-8A0E-02E0EC96E3EF",
verticalAlignment:0
},
{
anchors:11,
dataProviderID:"globals.vc2_currentAlternativa",
editable:false,
formIndex:12,
location:"0,0",
name:"vc2_currentAlternativa",
size:"460,20",
styleClass:"label",
text:"Vc2 Currentalternativa",
transparent:true,
typeid:4,
uuid:"0AC4E536-C5ED-4D13-A491-1BEBA1EA7C61"
},
{
anchors:11,
formIndex:11,
location:"20,40",
mediaOptions:14,
name:"root",
onRightClickMethodID:"A136BE77-9C32-4E60-9A91-DE162191C07F",
size:"420,20",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"0F0C3B2C-0AA7-470F-938E-478FA1170945"
},
{
anchors:11,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"filter",
formIndex:6,
location:"20,20",
name:"filter",
onActionMethodID:"01302A7F-93C7-4603-AFD1-5C5F22CF0B5B",
size:"400,20",
text:"Filter",
transparent:true,
typeid:4,
uuid:"133A99CF-556E-4055-8851-57F5007EF63A"
},
{
anchors:11,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"descrizione_tree",
editable:false,
location:"20,90",
name:"descrizione_tree",
onRightClickMethodID:"A136BE77-9C32-4E60-9A91-DE162191C07F",
size:"280,20",
text:"Descrizione Tree",
toolTipText:"%%k8_distinte_progetto_alt_id%%",
transparent:true,
typeid:4,
uuid:"3F9401F3-EA3E-4538-A0BB-9D5970BAF06D"
},
{
anchors:11,
labelFor:"descrizione_tree",
location:"20,60",
mediaOptions:14,
size:"280,20",
styleClass:"table",
tabSeq:-1,
text:"Descrizione",
typeid:7,
uuid:"5C3BC3A8-30E0-4FB1-AA02-E50DF5FA9E6B"
},
{
foreground:"#cfcfcf",
labelFor:"remove",
location:"440,60",
mediaOptions:14,
size:"20,20",
styleClass:"table",
tabSeq:-1,
typeid:7,
uuid:"63BB67F6-68B7-4D04-840E-94AB2F23B990"
},
{
height:110,
partType:5,
typeid:19,
uuid:"6D7D7E84-77FF-498B-92BF-A478AE5D88B1"
},
{
formIndex:13,
imageMediaID:"0746A208-4E27-45D4-BABE-FB96F24D516F",
location:"440,90",
mediaOptions:14,
name:"remove",
onActionMethodID:"BA7CF4E0-2AEA-42F4-996A-572F7712C893",
showClick:false,
size:"20,20",
transparent:true,
typeid:7,
uuid:"70321D0D-E8CF-4147-B36D-25590FE439F5"
},
{
anchors:3,
formIndex:7,
imageMediaID:"618F9AA0-E8E0-4019-A9D8-472CC462832F",
location:"430,22",
mediaOptions:14,
size:"16,16",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"7C026C18-AFDB-4287-A5B4-65631F5E3BA0"
},
{
anchors:11,
labelFor:"numero_disegno",
location:"300,60",
mediaOptions:14,
size:"100,20",
styleClass:"table",
tabSeq:-1,
text:"Disegno",
typeid:7,
uuid:"926744A1-EFC6-4E1C-9001-A0E2EB29DB2A"
},
{
anchors:11,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"esponente",
editable:false,
horizontalAlignment:0,
location:"400,90",
name:"esponente",
size:"40,20",
text:"Esponente",
transparent:true,
typeid:4,
uuid:"977DF569-D7E2-4A79-A0C9-59974EF86522"
},
{
anchors:11,
labelFor:"esponente",
location:"400,60",
mediaOptions:14,
size:"40,20",
styleClass:"table",
tabSeq:-1,
text:"Esp.",
typeid:7,
uuid:"A8890157-D18F-4A5A-B17E-2E10AF63389F"
},
{
height:60,
partType:1,
typeid:19,
uuid:"AA6CAF9A-85DB-49F4-A17B-E0E7FA49E384"
},
{
anchors:3,
formIndex:10,
imageMediaID:"0746A208-4E27-45D4-BABE-FB96F24D516F",
location:"442,42",
mediaOptions:14,
onActionMethodID:"BCEEB6C7-F972-4AE4-B6E8-A789F5FD0B2B",
showClick:false,
size:"16,16",
transparent:true,
typeid:7,
uuid:"B52983D4-6655-4EB8-9668-21A0A6400BB4"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"tree_icon",
displayType:9,
editable:false,
location:"0,90",
name:"tree_icon",
onActionMethodID:"E9888932-7D10-47AB-B46B-926FC22ADB68",
scrollbars:36,
size:"20,20",
text:"Tree Icon",
transparent:true,
typeid:4,
uuid:"BD497461-0BE2-4D58-8122-85389B04640A"
},
{
foreground:"#cfcfcf",
labelFor:"tree_icon",
location:"0,60",
mediaOptions:14,
size:"20,20",
styleClass:"table",
tabSeq:-1,
typeid:7,
uuid:"D6BD5680-751A-43B7-8D55-60E9F763584A"
},
{
anchors:11,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"numero_disegno",
editable:false,
format:"############",
horizontalAlignment:0,
location:"300,90",
name:"numero_disegno",
size:"100,20",
text:"Numero Disegno",
transparent:true,
typeid:4,
uuid:"E1F7B19D-E292-41BB-92D9-B3CFF9E49F19"
},
{
anchors:11,
formIndex:-2,
lineSize:1,
location:"5,20",
roundedRadius:20,
shapeType:2,
size:"450,20",
typeid:21,
uuid:"E536EFE5-015E-4E53-86A9-64CC3F96ED99"
}
],
name:"dp_history_alternativa",
navigatorID:"-1",
onDragMethodID:"D9E2D850-D756-479B-A89D-7393200D4038",
onDragOverMethodID:"100967F7-5A9B-461B-B7E9-39763B94F4E0",
onDropMethodID:"53DBBC9E-A81E-4C15-A5EB-DD8BE9EB2158",
onHideMethodID:"-1",
onLoadMethodID:"-1",
onRenderMethodID:"68EBBDDC-1044-45E5-AE67-BD2E0491EAC5",
paperPrintScale:100,
showInMenu:true,
size:"460,110",
styleName:"keeneight",
typeid:3,
uuid:"F79A1322-3FCD-4618-AAE7-686D29FDD0B5",
view:3