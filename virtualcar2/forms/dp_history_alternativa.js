/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"977F82C1-6224-423D-8FEF-35E44E80EA13"}
 */
var actionType = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"6B070F2C-89F3-405C-9CAA-15ACD58FC2FC"}
 */
var filter = "";

/**
 * @properties={typeid:35,uuid:"6B9342E6-3C27-496A-93B1-6F85CDF4CF8B",variableType:-4}
 */
var root = new Object();

/**
 * @properties={typeid:24,uuid:"E9888932-7D10-47AB-B46B-926FC22ADB68"}
 */
function workWrap() {
	time(work);
}

/**
 * @properties={typeid:24,uuid:"88CF2D31-1DFD-4107-AEBF-F0FE4B849FCA"}
 */
function work() {
	if (status != "O") {
		if (status == "+") {
			expand();
			status = "-";
		} else {
			collapse();
			status = "?";
		}
	}
}

/**
 * @properties={typeid:24,uuid:"E8A04881-4C28-4030-B837-4995F61CDF05"}
 * @AllowToRunInFind
 */
function expand() {
	var index = controller.getSelectedIndex();
	var pid = path_id;
	var l = livello;
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		ufs.path_id = pid + "/%";
		ufs.livello = l + 1;
		ufs.data_out = "^";
		ufs.search(false, false);
	}
	controller.loadRecords(ufs);
	controller.setSelectedIndex(index);
}

/**
 * @properties={typeid:24,uuid:"03445E8D-47DA-4A47-8A74-74B96CD98020"}
 * @AllowToRunInFind
 */
function collapse() {
	var index = controller.getSelectedIndex();
	var pid = path_id;
	var ufs = foundset.unrelate();
	for (var i = 1; i <= ufs.getSize(); i++) {
		var record = ufs.getRecord(i);
		if (record.path_id.search(pid) != -1) record.status = "?";
	}
	if (ufs.find()) {
		ufs.path_id = "!" + pid + "/%";
		ufs.search(false, true);
	}
	controller.loadRecords(ufs);
	controller.setSelectedIndex(index);
}

/**
 * @properties={typeid:24,uuid:"01422B86-EA58-4291-82DE-1108B00953AB"}
 */
function isolateWrap() {
	if (globals.vc2_currentAlternativa) {
		time(isolate);
	}
}

/**
 * @properties={typeid:24,uuid:"23E181BE-D6B2-4A57-84C5-11FE71969E6B"}
 * @AllowToRunInFind
 */
function isolate() {
	var d = descrizione;
	var pid = path_id;
	var nd = numero_disegno;
	var l = livello;
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		ufs.path_id = pid + "/%";
		ufs.livello = l + 1;
		ufs.search();
	}
	controller.loadRecords(ufs);
	setRoot(d, pid, nd, l);
}

/**
 * @properties={typeid:24,uuid:"BCEEB6C7-F972-4AE4-B6E8-A789F5FD0B2B"}
 */
function releaseWrap() {
	if (globals.vc2_currentAlternativa) {
		time(release);
	}
}

/**
 * @properties={typeid:24,uuid:"E5CA9BD8-C066-4F7F-B8AB-91EECB7F9791"}
 */
function release() {

	/** @type {JSFoundSet} */
	var fs = k8_dummy_to_k8_distinte_progetto_alt;
	for (var i = 1; i <= fs.getSize(); i++) {
		var record = fs.getRecord(i);
		record.status = "?";
	}
	databaseManager.recalculate(fs);
	controller.loadRecords(fs);
	setRoot(k8_dummy_to_k8_distinte_progetto_alt$root.descrizione,
		k8_dummy_to_k8_distinte_progetto_alt$root.path_id,
		k8_dummy_to_k8_distinte_progetto_alt$root.numero_disegno,
		k8_dummy_to_k8_distinte_progetto_alt$root.livello);
	controller.sort("path_id asc");
}

/**
 * @properties={typeid:24,uuid:"01302A7F-93C7-4603-AFD1-5C5F22CF0B5B"}
 */
function filterTreeWrap() {
	if (globals.vc2_currentAlternativa) {
		time(filterTree);
	}
}

/**
 * @properties={typeid:24,uuid:"24FAB30B-ACDC-460C-AE19-8B414863951D"}
 * @AllowToRunInFind
 */
function filterTree() {
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		if (filter) {
			ufs.path_id = root["rootPathID"] + "/%";
			try {
				ufs.numero_disegno = java.lang.Double.parseDouble(filter);
			} catch (e) {
				ufs.descrizione = "#" + filter;
			}
		} else {
			ufs.codice_vettura = globals.vc2_currentVersione;
			ufs.codice_padre = root["rootNumber"];
		}
		ufs.search();
	}
	controller.loadRecords(ufs);
}

/**
 * @properties={typeid:24,uuid:"C1B49B38-B387-41F3-B125-8B187FF8571E"}
 */
function setRoot(label, pid, number, level) {
	elements.root.text = label;
	root["rootPathID"] = pid;
	root["rootNumber"] = number;
	root["rootLevel"] = level;
}

/**
 * @properties={typeid:24,uuid:"59AA809B-0022-466F-95BB-02DDD7A26743"}
 */
function time(func) {
	//tempo di esecuzione di una funzione senza paramenti
	if (func && typeof func == "function") {
		forms.dp_history_container.writeMessage("...operazione in corso");
		application.updateUI();
		var start = new Date();
		var result = func();
		var end = new Date();
		forms.dp_history_container.writeMessage( ( (end - start) / 1000).toFixed(2) + "s");
		return result;
	}
	return null;
}

/**
 * Handle start of a drag, it can set the data that should be transfered and should return a constant which dragndrop mode/modes is/are supported.
 *
 * Should return a DRAGNDROP constant or a combination of 2 constants:
 * DRAGNDROP.MOVE if only a move can happen,
 * DRAGNDROP.COPY if only a copy can happen,
 * DRAGNDROP.MOVE|DRAGNDROP.COPY if a move or copy can happen,
 * DRAGNDROP.NONE if nothing is supported (drag should start).
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Number} DRAGNDROP.MOVE one of the DRAGNDROP constant (or a combination)
 *
 * @properties={typeid:24,uuid:"D9E2D850-D756-479B-A89D-7393200D4038"}
 */
function onDrag(event) {
	var src = event.getSource();
	var e = event.getElementName();
	if (src && e) {
		event.data = { from: controller.getName(), value: null };
		return DRAGNDROP.MOVE;
	}
	return DRAGNDROP.NONE
}

/**
 * Handle a drag over.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} true if a drop can happen on the element
 *
 * @properties={typeid:24,uuid:"100967F7-5A9B-461B-B7E9-39763B94F4E0"}
 */
function onDragOver(event) {
	if (globals.vc2_currentAlternativa) {
		var c = getClass(event.getSource());
		if (event.getModifiers() == DRAGNDROP.COPY && c == "[JavaClass com.servoy.j2db.TableView]") {
			actionType = "full";
			return true;
		}
		if (event.getModifiers() == DRAGNDROP.MOVE) {
			var e = event.getElementName();
			if (c == "[JavaClass com.servoy.j2db.TableView]" || e == "descrizione_tree") {
				actionType = "lite";
				return true;
			}
			return false;
		}
	}
	return false;
}

/**
 * Handle a drop.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} true if a drop could be made
 *
 * @properties={typeid:24,uuid:"53DBBC9E-A81E-4C15-A5EB-DD8BE9EB2158"}
 */
function onDrop(event) {
	/** @type {String} */
	var pk = event.getSource().toolTipText;
	forms.dp_history_container.writeMessage("...operazione in corso");
	application.updateUI();
	var start = new Date();

	if (event.data.from == forms.dp_history_alternativa.controller.getName()) {
		dropFromAlternativa(event, pk);
	} else {
		if (k8_dummy_to_k8_distinte_progetto_alt$root.getSize() == 0) {
			createAlternativa();
		}
		if (event.data.from == forms.dp_history_progetto.controller.getName()) {
			dropFromProgetti(event, pk);
		}
		if (event.data.from == forms.dp_history_anagrafica_as400.controller.getName()) {
			dropFromAnagrafica(event, pk);
		}
	}
	databaseManager.refreshRecordFromDatabase(foundset, -1);

	var end = new Date();
	forms.dp_history_container.writeMessage( ( (end - start) / 1000).toFixed(2) + "s");

	return true;

}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {String} pk
 * @properties={typeid:24,uuid:"00492D9F-80C1-4C12-9ECD-18EF8B923C93"}
 */
function dropFromProgetti(event, pk) {
	if (actionType == "full") {
		fullCopy(event);
	}
	if (actionType == "lite") {
		if (event.getModifiers() == DRAGNDROP.COPY) {
			liteCopy(event, pk, true);
		}
		if (event.getModifiers() == DRAGNDROP.MOVE) {
			liteCopy(event, pk, false);
		}
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {String}	pk
 * @properties={typeid:24,uuid:"83657B00-9381-466F-AAA6-98091B358A04"}
 */
function dropFromAlternativa(event, pk) {
	var query = "select K8_DISTINTE_PROGETTO_ID,PROGETTO,CODICE_VETTURA,CODICE_PADRE,NUMERO_DISEGNO,ESPONENTE,SEQUENZA,TIPO_ALLESTIMENTO,LIVELLO,DESCRIZIONE,COEFFICIENTE_IMPIEGO from K8_DISTINTE_PROGETTO_ALT where K8_DISTINTE_PROGETTO_ALT_ID = ?";
	var args = [k8_distinte_progetto_alt_id];
	//FS
	//dep	var ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,1);
	var ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);

	var values = ds.getRowAsArray(1);

	var e = event.getElementName();
	var oldParent = values[3];
	var oldLevel = values[8];
	if (e) {
		query = "select NUMERO_DISEGNO,LIVELLO from K8_DISTINTE_PROGETTO_ALT where K8_DISTINTE_PROGETTO_ALT_ID = ?";
		args = [pk.replace(/\./g, "")];
		//FS
		//dep	ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,1);
		ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);

		values[3] = ds.getValue(1, 1);
		values[8] = ds.getValue(1, 2) + 1;
	} else {
		values[3] = root["rootNumber"];
		values[8] = root["rootLevel"] + 1;
	}
	if (oldParent != values[3] && oldLevel != values[8]) {
		insertNode(values, null);
		childrenCopy(path_id, values[8] - oldLevel, true);
		//remove();
	}
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {String} pk
 * @properties={typeid:24,uuid:"5F2B7482-9A18-4977-A01C-68DABC5F2889"}
 */
function dropFromAnagrafica(event, pk) {
	var query = "select NUMERO_DISEGNO,ESPONENTE,DESCRIZIONE from K8_ANAGRAFICA_DISEGNI where K8_ANAGRAFICA_DISEGNI_ID = ?";
	var args = [event.data.value];
	//FS
	//dep	var ds = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1);
	var ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);

	var q = null;
	while (!q) {
		q = plugins.dialogs.showInputDialog("Quantità", "Inserire la quantità per: " + ds.getValue(1, 3), "1");
		try {
			q = java.lang.Double.parseDouble(String(q));
		} catch (e) {
			q = null;
		}
	}
	var values = [null, globals.vc2_currentProgetto, globals.vc2_currentVersione, null, ds.getValue(1, 1), ds.getValue(1, 2), "#", "#", null, ds.getValue(1, 3), q];

	var e = event.getElementName();
	if (e) {
		//devo identificare il record e trovarne disegno e livello
		query = "select NUMERO_DISEGNO,LIVELLO from K8_DISTINTE_PROGETTO_ALT where K8_DISTINTE_PROGETTO_ALT_ID = ?";
		args = [pk.replace(/\./g, "")];
		//FS

		//dep		ds = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1);
		ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);
		values[3] = ds.getValue(1, 1);
		values[8] = ds.getValue(1, 2) + 1;
	} else {
		//attacco sotto la root
		values[3] = root["rootNumber"];
		values[8] = root["rootLevel"] + 1;
	}
	insertNode(values, null);
}

/**
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"0DBB6005-DACC-41DD-AFDB-1CB6B1A93398"}
 */
function fullCopy(event) {
	checkRoots(event.data.value);
	var query = "select K8_DISTINTE_PROGETTO_ID,PROGETTO,CODICE_VETTURA,CODICE_PADRE,NUMERO_DISEGNO,ESPONENTE,SEQUENZA,TIPO_ALLESTIMENTO,LIVELLO,DESCRIZIONE,COEFFICIENTE_IMPIEGO from K8_DISTINTE_PROGETTO_CORE where PATH_ID like ? order by PATH_ID asc";
	var args = [event.data.value + "%"];
	//FS
	//dep	var ds = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, -1);
	var ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, -1);

	var size = ds.getMaxRowIndex();
	var data = { ds: ds, max: size };
	insertNodes(data);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @param {String}	pk
 * @param {Boolean} withChildren
 * @properties={typeid:24,uuid:"E51BE123-B72F-4BE5-A353-2399F250D1BD"}
 */
function liteCopy(event, pk, withChildren) {
	var query = "select K8_DISTINTE_PROGETTO_ID,PROGETTO,CODICE_VETTURA,CODICE_PADRE,NUMERO_DISEGNO,ESPONENTE,SEQUENZA,TIPO_ALLESTIMENTO,LIVELLO,DESCRIZIONE,COEFFICIENTE_IMPIEGO from K8_DISTINTE_PROGETTO_CORE where PATH_ID = ?";
	var args = [event.data.value];
	//FS
	//dep	var ds = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1);
	var ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);

	var values = ds.getRowAsArray(1);

	var e = event.getElementName();
	var oldLevel = values[8];
	if (e) {
		//devo identificare il record e trovarne disegno e livello
		query = "select NUMERO_DISEGNO,LIVELLO from K8_DISTINTE_PROGETTO_ALT where K8_DISTINTE_PROGETTO_ALT_ID = ?";
		args = [pk.replace(/\./g, "")];
		//FS
		//dep		ds = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1);
		ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);
		values[3] = ds.getValue(1, 1);
		values[8] = ds.getValue(1, 2) + 1;
	} else {
		//attacco sotto la root
		values[3] = root["rootNumber"];
		values[8] = root["rootLevel"] + 1;
	}
	insertNode(values, null);
	if (withChildren) childrenCopy(event.data.value, values[8] - oldLevel, false);
}

/**
 * @properties={typeid:24,uuid:"400E2088-97FF-4229-82FA-2E55FDBD1925"}
 */
function childrenCopy(pid, levelOffset, fromAlternativa) {

	var query = null;
	var skipCheck = null;

	if (!fromAlternativa) {
		query = "select K8_DISTINTE_PROGETTO_ID,PROGETTO,CODICE_VETTURA,CODICE_PADRE,NUMERO_DISEGNO,ESPONENTE,SEQUENZA,TIPO_ALLESTIMENTO,LIVELLO,DESCRIZIONE,COEFFICIENTE_IMPIEGO from K8_DISTINTE_PROGETTO_CORE where PATH_ID like ? order by PATH_ID asc";
		skipCheck = false;
	} else {
		query = "select K8_DISTINTE_PROGETTO_ID,PROGETTO,CODICE_VETTURA,CODICE_PADRE,NUMERO_DISEGNO,ESPONENTE,SEQUENZA,TIPO_ALLESTIMENTO,LIVELLO,DESCRIZIONE,COEFFICIENTE_IMPIEGO from K8_DISTINTE_PROGETTO_ALT where DATA_OUT is null and PATH_ID like ? order by PATH_ID asc";
		skipCheck = true;
	}
	var args = [pid + "/%"];
	//FS
	//dep	var ds = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, -1);
	var ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, -1);
	var size = ds.getMaxRowIndex();
	var line = null;
	globals.nfx_progressBarShow(size);
	for (var i = 1; i <= size; i++) {
		line = ds.getRowAsArray(i);
		line[8] = line[8] + levelOffset;
		insertNode(line, skipCheck);
		globals.nfx_progressBarStep();
	}
}

/**
 * @properties={typeid:24,uuid:"14BCFEC3-0F57-49A4-91A0-5912404097A5"}
 */
function createAlternativa() {
	var query = null;
	var args = null;

	query = "select K8_DISTINTE_PROGETTO_ID,PROGETTO,CODICE_VETTURA,CODICE_PADRE,NUMERO_DISEGNO,ESPONENTE,SEQUENZA,TIPO_ALLESTIMENTO,LIVELLO,DESCRIZIONE,COEFFICIENTE_IMPIEGO from K8_DISTINTE_PROGETTO_CORE where NUMERO_DISEGNO = ?";
	args = [globals.vc2_currentVersione];
	//FS
	//dep	var ds = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1);
	var ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);

	query = "insert into K8_DISTINTE_PROGETTO_ALT (K8_DISTINTE_PROGETTO_ID,PROGETTO,CODICE_VETTURA,CODICE_PADRE,NUMERO_DISEGNO,ESPONENTE,SEQUENZA,TIPO_ALLESTIMENTO,LIVELLO,DESCRIZIONE,COEFFICIENTE_IMPIEGO,ALTERNATIVA,DATA_IN,CONTEGGIO_FIGLI) values (?,?,?,?,?,?,?,?,?,?,?,?,sysdate,0)";
	args = ds.getRowAsArray(1).concat([globals.vc2_currentAlternativa]);
	//dep 	var result = plugins.rawSQL.executeSQL(controller.getServerName(),
	//		controller.getTableName(),
	//		query, args);
	var result = plugins.rawSQL.executeSQL(databaseManager.getDataSourceServerName(controller.getDataSource()),
		databaseManager.getDataSourceTableName(controller.getDataSource()),
		query, args);
	if (result) {
		query = "update K8_DISTINTE_PROGETTO_ALT set PATH_ID = '/'||K8_DISTINTE_PROGETTO_ALT_ID where DATA_OUT is null and CODICE_VETTURA = ? and ALTERNATIVA = ? and NUMERO_DISEGNO = ?";
		args = [globals.vc2_currentVersione, globals.vc2_currentAlternativa, globals.vc2_currentVersione];
		//FS
//dep		plugins.rawSQL.executeSQL(controller.getServerName(),
//			controller.getTableName(),
//			query, args);
		
		plugins.rawSQL.executeSQL(databaseManager.getDataSourceServerName(controller.getDataSource()),
			databaseManager.getDataSourceTableName(controller.getDataSource()),
			query, args);
		databaseManager.refreshRecordFromDatabase(k8_dummy_to_k8_distinte_progetto_alt$root, -1);
		setRoot(k8_dummy_to_k8_distinte_progetto_alt$root.descrizione,
			k8_dummy_to_k8_distinte_progetto_alt$root.path_id,
			k8_dummy_to_k8_distinte_progetto_alt$root.numero_disegno,
			k8_dummy_to_k8_distinte_progetto_alt$root.livello);
		controller.loadRecords(k8_dummy_to_k8_distinte_progetto_alt);
	} else {
		application.output(plugins.rawSQL.getException().getMessage());
	}
}

/**
 * @properties={typeid:24,uuid:"6F996CD4-6F44-458A-BE13-B0560A44853A"}
 */
function insertNodes(data) {
	//FS
	/** @type {JSDataSet} */	
	var ds = data.ds;
	var max = data.max;
	var line = null;
	globals.nfx_progressBarShow(max);
	for (var i = 1; i <= max; i++) {

		line = ds.getRowAsArray(i);
		insertNode(line, null);
		globals.nfx_progressBarStep();
	}
}

/**@param {Array} line
 * @param {Boolean} skipCheck
 * @properties={typeid:24,uuid:"82B791B7-117E-4E7E-8B07-5C16172110A4"}
 */
function insertNode(line, skipCheck) {
	var query = null;
	var args = null;

	var insert = null;

	if (skipCheck) {
		insert = true;
	} else {
		query = "select count(*) from K8_DISTINTE_PROGETTO_ALT where DATA_OUT is null and CODICE_VETTURA = ? and ALTERNATIVA = ? and  NUMERO_DISEGNO = ? and CODICE_PADRE = ? and SEQUENZA = ? and TIPO_ALLESTIMENTO = ? and LIVELLO = ?";
		args = [line[2], globals.vc2_currentAlternativa, line[4], line[3], line[6], line[7], line[8]];
		//FS
//dep		var ds = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1);
		var ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);
		insert = (ds.getValue(1, 1) == 0) ? true : false;
	}

	if (insert) {
		query = "insert into K8_DISTINTE_PROGETTO_ALT (K8_DISTINTE_PROGETTO_ID,PROGETTO,CODICE_VETTURA,CODICE_PADRE,NUMERO_DISEGNO,ESPONENTE,SEQUENZA,TIPO_ALLESTIMENTO,LIVELLO,DESCRIZIONE,COEFFICIENTE_IMPIEGO,ALTERNATIVA,DATA_IN,CONTEGGIO_FIGLI) values (?,?,?,?,?,?,?,?,?,?,?,?,sysdate,0)";
		args = line.concat([globals.vc2_currentAlternativa]);
//dep		var result = plugins.rawSQL.executeSQL(controller.getServerName(),
//			controller.getTableName(),
//			query, args);
		
		var result = plugins.rawSQL.executeSQL(databaseManager.getDataSourceServerName(controller.getDataSource()),
			databaseManager.getDataSourceTableName(controller.getDataSource()),
			query, args);
		if (result) {
			query = "update K8_DISTINTE_PROGETTO_ALT set CONTEGGIO_FIGLI = CONTEGGIO_FIGLI + 1 where DATA_OUT is null and CODICE_VETTURA = ? and ALTERNATIVA = ? and NUMERO_DISEGNO = ? and LIVELLO = ?";
			args = [globals.vc2_currentVersione, globals.vc2_currentAlternativa, line[3], line[8] - 1];
			//plugins.rawSQL.executeSQL(  controller.getServerName(),
			//							controller.getTableName(),
			//							query,args);
			query = "update K8_DISTINTE_PROGETTO_ALT set PATH_ID = concat((select PATH_ID from K8_DISTINTE_PROGETTO_ALT where DATA_OUT is null and CODICE_VETTURA = ? and ALTERNATIVA = ? and NUMERO_DISEGNO = ? and LIVELLO = ?),'/'||K8_DISTINTE_PROGETTO_ALT_ID) where DATA_OUT is null and CODICE_VETTURA = ? and ALTERNATIVA = ? and NUMERO_DISEGNO = ? and CODICE_PADRE = ? and SEQUENZA = ? and TIPO_ALLESTIMENTO = ? and LIVELLO = ?";
			args = [globals.vc2_currentVersione, globals.vc2_currentAlternativa, line[3], line[8] - 1, globals.vc2_currentVersione, globals.vc2_currentAlternativa, line[4], line[3], line[6], line[7], line[8]];
//dep			plugins.rawSQL.executeSQL(controller.getServerName(),
//				controller.getTableName(),
//				query, args);
			
			plugins.rawSQL.executeSQL(databaseManager.getDataSourceServerName(controller.getDataSource()),
				databaseManager.getDataSourceTableName(controller.getDataSource()),
				query, args);
			query = "select count(*) from K8_DISTINTE_PROGETTO_ALT where CODICE_VETTURA = ? and ALTERNATIVA = ? and NUMERO_DISEGNO = ? and ESPONENTE = ? and FAKE_COMPONENT is not null";
			args = [globals.vc2_currentVersione, globals.vc2_currentAlternativa, line[4], line[5]];
//dep			ds = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1);
			
			ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);
			if (ds.getValue(1, 1)) {
				query = "update K8_DISTINTE_PROGETTO_ALT set FAKE_COMPONENT = 1 where CODICE_VETTURA = ? and ALTERNATIVA = ? and NUMERO_DISEGNO = ? and ESPONENTE = ? and FAKE_COMPONENT is null";
//dep				plugins.rawSQL.executeSQL(controller.getServerName(),
//					controller.getTableName(),
//					query, args);
				
				plugins.rawSQL.executeSQL(databaseManager.getDataSourceServerName(controller.getDataSource()),
					databaseManager.getDataSourceTableName(controller.getDataSource()),
					query, args);
			}
		} else {
			application.output(plugins.rawSQL.getException().getMessage());
		}
	}
}

/**
 * @properties={typeid:24,uuid:"F6A447E1-1C91-4EBC-8684-A685CBA70103"}
 */
function checkRoots(idsStr) {
	var query = null;
	var args = null;
	var ds = null;

	/** @type {String} */
	var ids = idsStr.split("/");
	for (var i = 2; i < ids.length - 1; i++) {
		query = "select NUMERO_DISEGNO,CODICE_PADRE,SEQUENZA,TIPO_ALLESTIMENTO from K8_DISTINTE_PROGETTO_CORE where K8_DISTINTE_PROGETTO_ID = ?";
		args = [ids[i]];
		//FS		
//dep		ds = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1);
		ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);

		query = "select count(*) from K8_DISTINTE_PROGETTO_ALT where DATA_OUT is null and CODICE_VETTURA = ? and ALTERNATIVA = ? and NUMERO_DISEGNO = ? and CODICE_PADRE = ? and SEQUENZA = ? and TIPO_ALLESTIMENTO = ?";
		args = [globals.vc2_currentVersione, globals.vc2_currentAlternativa, ds.getValue(1, 1), ds.getValue(1, 2), ds.getValue(1, 3), ds.getValue(1, 4)];
//dep		ds = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1);

		ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);
		if (ds.getValue(1, 1) === 0) {
			query = "select K8_DISTINTE_PROGETTO_ID,PROGETTO,CODICE_VETTURA,CODICE_PADRE,NUMERO_DISEGNO,ESPONENTE,SEQUENZA,TIPO_ALLESTIMENTO,LIVELLO,DESCRIZIONE,COEFFICIENTE_IMPIEGO from K8_DISTINTE_PROGETTO_CORE where K8_DISTINTE_PROGETTO_ID = ?";
			args = [ids[i]];
//dep			ds = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1);
			ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);

			insertNode(ds.getRowAsArray(1), null);
		}
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"BA7CF4E0-2AEA-42F4-996A-572F7712C893"}
 */
function removeWrap(event) {
	time(remove);
}

/**
 * @properties={typeid:24,uuid:"E7620D27-75A9-4742-AE23-AAAA0202D9B4"}
 */
function remove() {
	collapse();
	var query = null;
	var args = null;
	//aggiorno il conteggio figli del padre
	query = "update K8_DISTINTE_PROGETTO_ALT set CONTEGGIO_FIGLI = CONTEGGIO_FIGLI - 1 where DATA_OUT is null and CODICE_VETTURA = ? and ALTERNATIVA = ? and NUMERO_DISEGNO = ? and LIVELLO = ?";
	args = [globals.vc2_currentVersione, globals.vc2_currentAlternativa, codice_padre, livello - 1];
	
	//FS
//dep	plugins.rawSQL.executeSQL(controller.getServerName(),
//		controller.getTableName(),
//		query, args);
	
	plugins.rawSQL.executeSQL(databaseManager.getDataSourceServerName(controller.getDataSource()),
		databaseManager.getDataSourceTableName(controller.getDataSource()),
		query, args);
	//cancello i record interessati (la cancellazione è logica)
	query = "update K8_DISTINTE_PROGETTO_ALT set DATA_OUT = sysdate where PATH_ID like ? and DATA_OUT is null";
	args = [path_id + "%"];
//dep	plugins.rawSQL.executeSQL(controller.getServerName(),
//		controller.getTableName(),
//		query, args);
	plugins.rawSQL.executeSQL(databaseManager.getDataSourceServerName(controller.getDataSource()),
		databaseManager.getDataSourceTableName(controller.getDataSource()),
		query, args);
	databaseManager.refreshRecordFromDatabase(foundset, -1);
}

/**
 * Perform the element right-click action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"A136BE77-9C32-4E60-9A91-DE162191C07F"}
 */
function onRightClick(event) {
	if (globals.vc2_currentAlternativa) {
		//FS
//dep	var item = plugins.popupmenu.createMenuItem("Aggiungi figlio", addChild, "media:///man_in.png");
//		item.setMethodArguments([event.getElementName()]);
//		plugins.popupmenu.showPopupMenu(elements[event.getElementName()], [item]);
		var menu = plugins.window.createPopupMenu();
		var item = menu.addMenuItem("Aggiungi figlio", addChild, "media:///man_in.png");
		item.methodArguments=[event.getElementName()];
		menu.show(elements[event.getElementName()]);
	}
}

/**
 * @properties={typeid:24,uuid:"55CEFF01-89E6-4EF3-B32D-DF8449FE9AAD"}
 */
function addChild(source) {
	var d = plugins.dialogs.showInputDialog("Descrizione", "Inserire la descrizione per il componente");

	if (d) {
		forms.dp_history_container.writeMessage("...operazione in corso");
		application.updateUI();
		var start = new Date();

		var values = [null, globals.vc2_currentProgetto, globals.vc2_currentVersione, null, -1, 0, "#", "#", null, d, 1];
		if (source == "root") {
			if (k8_dummy_to_k8_distinte_progetto_alt$root.getSize() == 0) {
				createAlternativa();
			}
			values[3] = root.rootNumber;
			values[8] = root.rootLevel + 1;
		}
		if (source == "descrizione_tree") {
			values[3] = numero_disegno;
			values[8] = livello + 1;
		}
		insertNode(values, null);
		var query = "update K8_DISTINTE_PROGETTO_ALT set FAKE_COMPONENT = 1,NUMERO_DISEGNO = (select NVL(max(NUMERO_DISEGNO),0) + 1 from K8_DISTINTE_PROGETTO_ALT where FAKE_COMPONENT = ?) where NUMERO_DISEGNO = ?";
//FS
//dep		plugins.rawSQL.executeSQL(controller.getServerName(),
//			controller.getTableName(),
//			query, [1, -1]);
		
		plugins.rawSQL.executeSQL(databaseManager.getDataSourceServerName(controller.getDataSource()),
			databaseManager.getDataSourceTableName(controller.getDataSource()),
			query, [1,-1]);
		databaseManager.refreshRecordFromDatabase(foundset, -1);

		var end = new Date();
		forms.dp_history_container.writeMessage( ( (end - start) / 1000).toFixed(2) + "s");
	}
}

/**
 * @properties={typeid:24,uuid:"70F2D6F2-0F59-4A7E-B330-204707DF5161"}
 */
function refreshChildrenCount() { }

/**
 * Called before the form component is rendered.
 *
 * @param {JSRenderEvent} event the render event
 *
 * @private
 *
 * @properties={typeid:24,uuid:"68EBBDDC-1044-45E5-AE67-BD2E0491EAC5"}
 */
function onRender(event) {

	if(event.isRecordSelected() && event.getRenderable().getName()!='vc2_currentAlternativa'&& event.getRenderable().getName()!='root'&& event.getRenderable().getName()!='filter'&& event.getRenderable().getName()!='tree_icon')
	{
		application.output("entrato in onRender dp_history_alternativa");
		event.getRenderable().fgcolor = '#339eff';		
		event.getRenderable().font='Verdana,1,11';
	}
	else{
		event.getRenderable().fgcolor = '#000000';
	}
}
