/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"7B2B4097-5CF7-4B95-A1CB-5FF013B6A2B7"}
 */
var filter = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B84D993F-0FB8-4C80-8321-CB84A0CBC59B"}
 */
var filterMessage = "";

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"118F68F5-724E-4AD7-BC90-A2EE42660A06"}
 */
function filterTableWrap(event) {
	time(filterTable);
}

/**
 * @properties={typeid:24,uuid:"4FF525B0-71E2-404A-82F3-104086C40894"}
 * @AllowToRunInFind
 */
function filterTable(){
	var filtered = null;
	if(controller.find()){
		if(filter){
			filtered = true;
			var split = filter.split(" ");
			try{
				var n = java.lang.Double.parseDouble(split[0]);
				numero_disegno = n
				filterMessage = "Filtri: disegno = " + n;
				try{
					var e = java.lang.Integer.parseInt(split[1]);
					esponente = e;
					filterMessage += ", esponente = " + e;
				}catch(e){
					/*
					 * questo caso si presenta quando viene imputato un numeno e successivamente
					 * una testo, pertanto si decide di valutare la stringa come chiave di ricerca
					 * per la descrizione
					 */
				}
			}catch(e){
				descrizione = "#" + filter;
				filterMessage = "Filtri: descrizione = " + filter;
			}
		}else{
			filtered = false;
			k8_anagrafica_disegni_id = "!^";
			filterMessage = "";
		}
		controller.search();
		if(filtered){
			filterMessage += " | " + databaseManager.getFoundSetCount(foundset) + " trovati";
		}
	}
}

/**
 * @properties={typeid:24,uuid:"50DBB134-2798-4B40-9031-E3A9DD936B98"}
 */
function time(func){
	//tempo di esecuzione di una funzione senza paramenti
	if(func && typeof func == "function"){
		forms.dp_history_container.writeMessage("...operazione in corso");
		application.updateUI();
		var start = new Date();
		var result = func();
		var end = new Date();
		forms.dp_history_container.writeMessage(((end - start) / 1000).toFixed(2) + "s");
		return result;
	}
	return result;
}

/**
 * Handle start of a drag, it can set the data that should be transfered and should return a constant which dragndrop mode/modes is/are supported.
 *
 * Should return a DRAGNDROP constant or a combination of 2 constants:
 * DRAGNDROP.MOVE if only a move can happen,
 * DRAGNDROP.COPY if only a copy can happen,
 * DRAGNDROP.MOVE|DRAGNDROP.COPY if a move or copy can happen,
 * DRAGNDROP.NONE if nothing is supported (drag should start).
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Number} DRAGNDROP.MOVE one of the DRAGNDROP constant (or a combination)
 *
 * @properties={typeid:24,uuid:"9A1303ED-493C-437C-A0BA-23D64FD7F8FC"}
 */
function onDrag(event) {
	var src = event.getSource();
	var e = event.getElementName();
	if(src && e){
		event.data = {from: controller.getName(), value: k8_anagrafica_disegni_id};
		return DRAGNDROP.MOVE;
	}
	return DRAGNDROP.NONE
}

/**
 * Called before the form component is rendered.
 *
 * @param {JSRenderEvent} event the render event
 *
 * @private
 *
 * @properties={typeid:24,uuid:"C5E60591-1C59-464F-9107-BEB4AF9CD611"}
 */
function onRender(event) {
	
	if(event.isRecordSelected() && event.getRenderable().getName()!='anagrafica_label' && event.getRenderable().getName()!='filterMessage' && event.getRenderable().getName()!='filter')
	{
		event.getRenderable().fgcolor = '#339eff';		
		event.getRenderable().font='Verdana,1,11';
	}
	else{
		event.getRenderable().fgcolor = '#000000';
	}
}
