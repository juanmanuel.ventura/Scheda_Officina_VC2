/**
 * @properties={typeid:24,uuid:"8207E4D8-9CEB-45AA-B5CA-55AEEE3FDF8E"}
 */
function nfx_getTitle() {
	return "DPH (BETA)";
}

/**
 * @properties={typeid:24,uuid:"78556C01-A532-4800-BB1C-F7D4427525B8"}
 */
function nfx_isProgram() {
	return true;
}

/**
 * @properties={typeid:24,uuid:"F6052254-204C-4DC6-A9F7-39A70A57D3E6"}
 */
function nfx_defaultPermissions() {
	return "^";
}

/**
 * @properties={typeid:24,uuid:"5C666E87-7048-42FF-9D63-C02F076AECE3"}
 */
function writeMessage(message) {

	elements.message.text = message;
	elements.message_shadow.text = message;
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"FA60BAA6-114D-499E-987F-62D4F7E6BB5A"}
 */
function onProgettoChange(oldValue, newValue, event) {
	globals.vc2_currentVersione = null;
	onVersioneChange(null, null, null);
	return true;
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"1689186E-7C17-489F-8027-737E9D9C4F22"}
 */
function onVersioneChange(oldValue, newValue, event) {

	//SAuc
	//	forms.dp_history_progetto.setRoot(k8_dummy_to_k8_distinte_progetto_core$root.descrizione,
	//		  k8_dummy_to_k8_distinte_progetto_core$root.path_id,
	//		  k8_dummy_to_k8_distinte_progetto_core$root.numero_disegno);

	/** @type {JSFoundSet} */
	var fs = k8_dummy_to_k8_distinte_progetto_core$root;

	forms.dp_history_progetto.setRoot(fs.descrizione, fs.path_id, fs.numero_disegno);
	globals.vc2_currentAlternativa = null;
	onAlternativaChange(null, null, null);
	return true;
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"DD1F386D-DC00-49EE-8E8F-F2371FDB7FB2"}
 */
function onAlternativaChange(oldValue, newValue, event) {
	forms.dp_history_alternativa.setRoot(k8_dummy_to_k8_distinte_progetto_alt$root.descrizione,
		k8_dummy_to_k8_distinte_progetto_alt$root.path_id,
		k8_dummy_to_k8_distinte_progetto_alt$root.numero_disegno,
		k8_dummy_to_k8_distinte_progetto_alt$root.livello);
	return true;
}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"317DA55A-F1D8-42AF-91BD-A355D9AC54E9"}
 */
function onLoad(event) {
	try{
	globals.utils_setSplitPane({
		splitpane: elements.split,
		leftComponent: elements.progetto,
		rightComponent: elements.alternativa,
		orientation: 1,
		//FS	
		//dep 	dividerLocation: (application.getWindowWidth() - 260) / 2});
		dividerLocation: (application.getWindow().getWidth() - 260) / 2});
	}catch(e){}

}
