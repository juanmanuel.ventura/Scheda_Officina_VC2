/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"00CBC693-A912-4BCF-B8B1-AD42A34C4B0E"}
 */
var filter = "";

/**
 * @properties={typeid:35,uuid:"9DCDA13E-52A6-4A57-8920-C30AC70C1227",variableType:-4}
 */
var fs_back = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"989CA65E-6ADD-4FED-A493-8E148964FB11",variableType:4}
 */
var rootNumber = 0;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"05A60FA0-A51F-47A9-8CC5-633D186CCBCC"}
 */
var rootPathID = "";

/**
 * @properties={typeid:24,uuid:"E8D6F498-D669-4A4F-AC30-505CE3CA3731"}
 */
function workWrap()
{
	time(work);
}

/**
 * @properties={typeid:24,uuid:"EE4E91D3-17AC-4265-ABB3-7AF7D0104C59"}
 */
function work()
{
	if(status != "O"){
		if(status == "+"){
			expand();
			status = "-";
		}else{
			collapse();
			status = "?";
		}
	}
}

/**
 * @properties={typeid:24,uuid:"CB1044D4-D62E-4481-886A-310002F7CC49"}
 * @AllowToRunInFind
 */
function expand()
{
	var index = controller.getSelectedIndex();
	var cv = codice_vettura;
	var nd = numero_disegno;
	var ufs = foundset.unrelate();
	if(ufs.find()){
		ufs.codice_vettura = cv;
		ufs.codice_padre = nd;
		ufs.search(false,false);
	}
	controller.loadRecords(ufs);
	controller.setSelectedIndex(index);
}

/**
 * @properties={typeid:24,uuid:"F0C2387B-4BBD-4CB5-B82A-C80D249E98EA"}
 * @AllowToRunInFind
 */
function collapse()
{
	var index = controller.getSelectedIndex();
	var pid = path_id;
	var ufs = foundset.unrelate();
	for(var i=1;i<=ufs.getSize();i++){
		var record = ufs.getRecord(i);
		if(record.path_id.search(pid) != -1) record.status = "?";
	}
	if(ufs.find()){
		ufs.path_id = "!" + pid + "/%";
		ufs.search(false,true);
	}
	controller.loadRecords(ufs);
	controller.setSelectedIndex(index);
}

/**
 * @properties={typeid:24,uuid:"B5B2C4FE-CE0B-40A9-9B66-56BF2D12BB1A"}
 */
function isolateWrap()
{
	if(globals.vc2_currentVersione){
		time(isolate);
	}
}

/**
 * @properties={typeid:24,uuid:"FB9D08FD-E351-4792-93F6-7E0A6F8D4B03"}
 * @AllowToRunInFind
 */
function isolate(){
	var pid = path_id;
	var d = descrizione;
	var cv = codice_vettura;
	var nd = numero_disegno;
	var ufs = foundset.unrelate();
	if(ufs.find()){
		ufs.codice_vettura = cv;
		ufs.codice_padre = nd;
		ufs.search();
	}
	controller.loadRecords(ufs);
	setRoot(d,pid,nd);
}

/**
 * @properties={typeid:24,uuid:"4CEF7661-F557-4CF0-A3A7-8AE706E438B6"}
 */
function releaseWrap()
{
	if(globals.vc2_currentVersione){
		time(release);
	}
}

/**
 * @properties={typeid:24,uuid:"D576B6CD-623C-4304-BBD5-923FB58E7FCF"}
 */
function release(){
	/** @type {JSFoundSet} */
	var fs = k8_dummy_to_k8_distinte_progetto_core;
	for(var i=1;i<=fs.getSize();i++){
		var record = fs.getRecord(i);
		record.status = "?";
	}
	databaseManager.recalculate(fs);
	controller.loadRecords(fs);
	setRoot(k8_dummy_to_k8_distinte_progetto_core$root.descrizione,
			k8_dummy_to_k8_distinte_progetto_core$root.path_id,
			k8_dummy_to_k8_distinte_progetto_core$root.numero_disegno);
	controller.sort("path_id asc");
}

/**
 * @properties={typeid:24,uuid:"3B6C0C1A-A904-4C3A-8D72-FC97CE9C0A70"}
 */
function filterTreeWrap()
{
	if(globals.vc2_currentVersione){
		time(filterTree);
	}
}

/**
 * @properties={typeid:24,uuid:"4AB7F5DD-7F8A-4D20-93F9-10B2527683AE"}
 * @AllowToRunInFind
 */
function filterTree(){
	var ufs = foundset.unrelate();
	if(ufs.find()){
		if(filter){
			ufs.path_id = rootPathID + "/%";
			try{
				ufs.numero_disegno = java.lang.Double.parseDouble(filter);
			}catch(e){
				ufs.descrizione = "#" + filter;
			}
		}else{
			ufs.codice_vettura = globals.vc2_currentVersione;
			ufs.codice_padre = rootNumber;
		}
		ufs.search();
		for(var i=1;i<=ufs.getSize();i++){
			var record = ufs.getRecord(i);
			record.status = "?";
		}
	}
	controller.loadRecords(ufs);
}

/**
 * @properties={typeid:24,uuid:"DCBA04C5-367D-4CAD-BA6D-14D5B7EEDD5D"}
 */
function setRoot(label,pid,number){
	elements.root.text = label;
	rootPathID = pid;
	rootNumber = number
}

/**
 * @properties={typeid:24,uuid:"B1122B48-AA3F-48EF-A16A-789B73A631A1"}
 */
function time(func){
	//tempo di esecuzione di una funzione senza paramenti
	if(func && typeof func == "function"){
		forms.dp_history_container.writeMessage("...operazione in corso");
		application.updateUI();
		var start = new Date();
		var result = func();
		var end = new Date();
		forms.dp_history_container.writeMessage(((end - start) / 1000).toFixed(2) + "s");
		return result;
	}
	return result;
}

/**
 * Handle start of a drag, it can set the data that should be transfered and should return a constant which dragndrop mode/modes is/are supported.
 *
 * Should return a DRAGNDROP constant or a combination of 2 constants:
 * DRAGNDROP.MOVE if only a move can happen,
 * DRAGNDROP.COPY if only a copy can happen,
 * DRAGNDROP.MOVE|DRAGNDROP.COPY if a move or copy can happen,
 * DRAGNDROP.NONE if nothing is supported (drag should start).
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Number} DRAGNDROP.MOVE one of the DRAGNDROP constant (or a combination)
 *
 * @properties={typeid:24,uuid:"228CE157-B8B7-4659-ABFE-CE6E0DE9D3EC"}
 */
function onDrag(event) {
	var src = event.getSource();
	var e = event.getElementName();
	if(src && e){
		event.data = {from: controller.getName(), value: path_id};
		return DRAGNDROP.MOVE|DRAGNDROP.COPY;
	}
	return DRAGNDROP.NONE
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"1F793482-FBEC-4E6E-ACE2-4EA90E5E7DC9"}
 */
function onShowForm(firstShow, event) {
	if(!firstShow && fs_back && globals.vc2_currentVersione == fs_back.versione){
		controller.loadRecords(fs_back.fs);
	}
}

/**
 * Handle hide window.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} allow hide
 *
 * @properties={typeid:24,uuid:"CC6B7C1B-A5D3-48D2-802D-88996DB0AB46"}
 */
function onHide(event) {
	if(globals.vc2_currentVersione){
		fs_back = {versione:	globals.vc2_currentVersione,
		           fs:			foundset.duplicateFoundSet()};
	}else{
		fs_back = null;
	}
	return true;
}

/**
 * Called before the form component is rendered.
 *
 * @param {JSRenderEvent} event the render event
 *
 * @private
 *
 * @properties={typeid:24,uuid:"C91EFE01-5968-4CAC-B3B2-5B2F41501515"}
 */
function onRender(event) {
	if(event.isRecordSelected() && event.getRenderable().getName()!='vc2_currentVersione'&& event.getRenderable().getName()!='root'&& event.getRenderable().getName()!='filter'&& event.getRenderable().getName()!='tree_icon')
	{
		event.getRenderable().fgcolor = '#339eff';		
		event.getRenderable().font='Verdana,1,11';
	}
	else{
		event.getRenderable().fgcolor = '#000000';
	}

}
