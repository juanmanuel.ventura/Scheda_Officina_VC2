dataSource:"db:/ferrari/k8_dummy",
items:[
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"showBAAN",
displayType:4,
location:"720,10",
name:"showBAAN",
onDataChangeMethodID:"D3D679BB-6F97-4A69-B3F0-F905DB9744F0",
size:"70,20",
tabSeq:-2,
text:"BAAN",
transparent:true,
typeid:4,
uuid:"130FFE77-0435-4B9B-A1AC-AEF15FDD7808"
},
{
anchors:15,
beanClassName:"javax.swing.JSplitPane",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_17\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JSplitPane\"> \
  <int>1<\/int> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>580<\/int> \
    <int>280<\/int> \
   <\/object> \
  <\/void> \
  <void method=\"add\"> \
   <object id=\"JButton0\" class=\"javax.swing.JButton\"> \
    <string>tasto destro<\/string> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <null/> \
  <\/void> \
  <void property=\"bottomComponent\"> \
   <object idref=\"JButton0\"/> \
  <\/void> \
  <void property=\"dividerLocation\"> \
   <int>97<\/int> \
  <\/void> \
  <void property=\"leftComponent\"> \
   <object class=\"javax.swing.JButton\"> \
    <string>tasto sinistro<\/string> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>split_out<\/string> \
  <\/void> \
  <void property=\"rightComponent\"> \
   <object idref=\"JButton0\"/> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"10,40",
name:"split_out",
size:"780,330",
tabSeq:-2,
typeid:12,
uuid:"1D0C9125-5FA5-4A22-9D8C-FAD75E7A0088"
},
{
anchors:12,
imageMediaID:"40905D44-A323-4D4C-9CCA-CC832C1E545D",
location:"10,380",
mediaOptions:14,
name:"show",
onActionMethodID:"F731E21B-6327-46CF-B848-32655ADE8684",
showClick:false,
size:"20,20",
tabSeq:-2,
transparent:true,
typeid:7,
uuid:"2DB21B6E-10AA-4C2A-87FF-D3269E98A5DA"
},
{
labelFor:"vc2_currentVersione",
location:"230,10",
mediaOptions:14,
size:"60,20",
styleClass:"form",
tabSeq:-1,
text:"Versione",
transparent:true,
typeid:7,
uuid:"3532B8EC-325C-4A56-85A3-D43AB7882DAC"
},
{
items:[
{
containsFormID:"7C838F69-9C90-43D5-A2BA-815584BDA196",
location:"540,212",
relationName:"k8_dummy_to_k8_distinte_progetto_core",
text:"dpr_as400",
typeid:15,
uuid:"74A2915A-5348-42C2-8204-F04BE2FA1049"
}
],
location:"300,210",
name:"as400",
printable:false,
size:"300,50",
typeid:16,
uuid:"3AD0A0AB-7F7B-4E46-A496-5981D8CE98FE"
},
{
labelFor:"vc2_currentProgetto",
location:"10,10",
mediaOptions:14,
size:"60,20",
styleClass:"form",
tabSeq:-1,
text:"Progetto",
transparent:true,
typeid:7,
uuid:"518C84C1-6AA1-46C5-9880-EC7E384634B8"
},
{
dataProviderID:"globals.vc2_currentProgetto",
displayType:2,
editable:false,
location:"80,10",
name:"vc2_currentProgetto",
onDataChangeMethodID:"29047F36-B0C5-4178-ABC8-03E8B6ABE7F7",
size:"140,20",
tabSeq:-2,
text:"Vc2 Currentprogetto",
transparent:true,
typeid:4,
uuid:"6F9C3FA6-F596-4836-9A50-3A53DD70F398",
valuelistID:"8DBD3071-35BE-4D55-B40F-F9B6FB9F8DD6"
},
{
anchors:11,
dataProviderID:"globals.vc2_currentVersione",
displayType:2,
editable:false,
location:"300,10",
name:"vc2_currentVersione",
onDataChangeMethodID:"58160966-0869-4767-B77C-6381189EF530",
size:"230,20",
tabSeq:-2,
text:"Vc2 Currentversione",
transparent:true,
typeid:4,
uuid:"7458C8AB-CFBB-46A3-A7FF-1D0DA5E066D4",
valuelistID:"EEBB6F8B-9C0E-4730-AF18-9A415FBF121B"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"showAS400",
displayType:4,
location:"640,10",
name:"showAS400",
onDataChangeMethodID:"D3D679BB-6F97-4A69-B3F0-F905DB9744F0",
size:"70,20",
tabSeq:-2,
text:"AS400",
transparent:true,
typeid:4,
uuid:"8AE588B4-F271-45B5-B9F4-EFF395C9A9D8"
},
{
anchors:14,
fontType:"Verdana,1,10",
foreground:"#ffff00",
formIndex:-4,
horizontalAlignment:4,
location:"40,380",
mediaOptions:14,
name:"message",
size:"752,20",
styleClass:"form",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"8ED127B6-C6F4-448D-BF06-0D3F5E351292"
},
{
anchors:12,
imageMediaID:"127B464F-994F-4572-810C-24FBC185DB64",
location:"10,380",
mediaOptions:14,
name:"hide",
onActionMethodID:"68BCFE66-3A8D-48EA-9704-BE0EB1C2A8B6",
showClick:false,
size:"20,20",
tabSeq:-2,
transparent:true,
typeid:7,
uuid:"9A5B7AFA-FAAE-49CF-ADD0-5788732ABCD2"
},
{
beanClassName:"javax.swing.JSplitPane",
location:"120,50",
name:"split_in",
size:"80,80",
tabSeq:-2,
typeid:12,
uuid:"A8B2643E-7743-4749-81AD-EAEC6E28ADA4"
},
{
beanClassName:"javax.swing.JSplitPane",
location:"120,140",
name:"split_baan",
size:"80,80",
tabSeq:-2,
typeid:12,
uuid:"C1666594-1F5C-420D-99D1-B5490A5945DE"
},
{
items:[
{
containsFormID:"6FB7A766-9BA5-4426-AF36-C3056AAD0ED3",
location:"490,152",
relationName:"k8_dummy_to_k8_distinta_baan_esponenti",
text:"dpr_baan_esponenti",
typeid:15,
uuid:"751D1F51-F6CF-4CB0-9263-5CF41D450C05"
}
],
location:"300,150",
name:"baan_esp",
printable:false,
size:"300,50",
tabSeq:-2,
typeid:16,
uuid:"D62C15D8-6C30-49BE-A64D-B54D8E0B217A"
},
{
anchors:14,
fontType:"Verdana,1,10",
formIndex:-3,
horizontalAlignment:4,
location:"41,381",
mediaOptions:14,
name:"message_shadow",
size:"753,20",
styleClass:"form",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"D74E53EC-25C8-4010-A35F-A781A2F3E490"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"showDPR",
displayType:4,
location:"540,10",
name:"showDPR",
onDataChangeMethodID:"D3D679BB-6F97-4A69-B3F0-F905DB9744F0",
size:"90,20",
text:"Riciclabilità",
transparent:true,
typeid:4,
uuid:"E3EE57C6-82E0-4EDC-80A0-A3471D1A08EE"
},
{
height:410,
partType:5,
typeid:19,
uuid:"F47D8353-416E-4A2A-AE28-C22D68AE33B2"
},
{
items:[
{
containsFormID:"C3012F31-38E4-4A9B-930B-96E086C2A7D7",
location:"520,272",
relationName:"k8_dummy_to_k8_distinte_riciclabilita",
text:"dpr_riciclabilita",
typeid:15,
uuid:"82E8C61D-D17D-44B9-B717-85B7C48EA519"
}
],
location:"300,270",
name:"dpr",
printable:false,
size:"300,50",
tabSeq:-2,
typeid:16,
uuid:"F9012AE2-A4A3-4AE7-AF11-65C4553DA02F"
},
{
items:[
{
containsFormID:"0EB9AB26-1180-4136-8E6F-7C1EC7952C64",
location:"540,92",
relationName:"k8_dummy_to_k8_distinta_baan",
text:"dpr_baan",
typeid:15,
uuid:"2CE85356-DA29-4378-84E7-A23B080B3251"
}
],
location:"300,90",
name:"baan",
printable:false,
size:"300,50",
tabSeq:-2,
typeid:16,
uuid:"F95C7C4C-DD43-41A3-9549-5E5AA5CEC47F"
}
],
name:"dpr_albero_container_s5",
navigatorID:"-1",
onHideMethodID:"7621013B-45FC-454C-88BE-EF2DC5A5C0E0",
onLoadMethodID:"EBC1C2FE-56E8-4DBD-B125-CA3A895804F2",
onRecordSelectionMethodID:"2859D255-6F8C-4ACE-B502-1E941A5C31EF",
onSearchCmdMethodID:"A4E5EAFC-62DD-48C2-B314-BC9A9204E61B",
onShowMethodID:"BCCAF39F-5866-42AB-9E3E-7563D9D8EA46",
paperPrintScale:100,
showInMenu:true,
size:"800,410",
styleName:"keeneight",
typeid:3,
uuid:"13B3DAC5-E137-463E-BBB4-A66486A58CFD"