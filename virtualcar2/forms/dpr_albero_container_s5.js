/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"D51D5309-1999-4BA4-A701-7DD709A18A5F",variableType:4}
 */
var showAS400 = 1;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"C625CDCB-6C8A-4D22-9435-0633745405B0",variableType:4}
 */
var showBAAN = 1;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"07818DAA-6315-49C7-8AFC-9B84DF47EFE5",variableType:4}
 */
var showDPR = 1;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"B0A0E6EC-B7FF-4257-9F60-50D1BC9561A8",variableType:4}
 */
var size = null;

/**
 * @properties={typeid:24,uuid:"F19CF25E-56B8-44D3-9683-4B49AD09C347"}
 */
function nfx_getTitle() {
	return "DBR (beta)";
}

/**
 * @properties={typeid:24,uuid:"89CA2C6A-9B92-40D9-B0DD-C7D55461965C"}
 */
function nfx_isProgram() {
	return true;
}

/**
 * @properties={typeid:24,uuid:"42A9EE46-4D26-4F33-ADA7-5981135518DB"}
 */
function nfx_defaultPermissions() {
	return "^";
}

/**
 * @properties={typeid:24,uuid:"EBC1C2FE-56E8-4DBD-B125-CA3A895804F2"}
 */
function onLoad() {

	//FS
	globals.utils_setSplitPane({
		splitpane: elements.split_baan,
		leftComponent: elements.baan,
		rightComponent: elements.baan_esp,
		orientation: 0,
		//dep	dividerLocation: application.getWindowHeight() / 2
		dividerLocation: application.getWindow().getHeight() / 2
	});

	globals.utils_setSplitPane({
		splitpane: elements.split_in,
		leftComponent: elements.as400,
		rightComponent: elements.split_baan,
		orientation: 1,
		//dep	dividerLocation: application.getWindowWidth() / 3

		dividerLocation: application.getWindow().getWidth() / 3

	});

	globals.utils_setSplitPane({
		splitpane: elements.split_out,
		leftComponent: elements.dpr,
		rightComponent: elements.split_in,
		orientation: 1,
		//dep	dividerLocation: application.getWindowWidth() / 3
		dividerLocation: application.getWindow().getWidth() / 3

	});
}

/**
 * @properties={typeid:24,uuid:"EFA21367-01BC-47E2-A7A7-D1A9A7D2B7CD"}
 */
function nfx_onShow() {
	hidePanel();
}

/**
 * @properties={typeid:24,uuid:"21D3AA75-4C49-47CB-9134-2A212FFB38BE"}
 */
function writeMessage(m) {
	var message = m || "";

	elements.message.text = message;
	elements.message_shadow.text = message;
}

/**
 * @properties={typeid:24,uuid:"68BCFE66-3A8D-48EA-9704-BE0EB1C2A8B6"}
 */
function hidePanel() {
	size = forms.nfx_interfaccia_pannello_base.elements.base_split_pane.dividerLocation;
	forms.nfx_interfaccia_pannello_base.elements.base_split_pane.dividerLocation = 0;
	elements.hide.visible = false;
	elements.show.visible = true;
}

/**
 * @properties={typeid:24,uuid:"F731E21B-6327-46CF-B848-32655ADE8684"}
 */
function showPanel() {
	var s = (size) ? size : 260;
	forms.nfx_interfaccia_pannello_base.elements.base_split_pane.dividerLocation = s;
	elements.hide.visible = true;
	elements.show.visible = false;
}

/**
 * @properties={typeid:24,uuid:"D3D679BB-6F97-4A69-B3F0-F905DB9744F0"}
 */
function onCheckChangeWrap(oldValue, newValue, event) {
	var status = showDPR + "" + showAS400 + "" + showBAAN;
	onCheckChange(status, true);
}

/**
 * @properties={typeid:24,uuid:"3BC6AFF4-98E2-46BB-BD05-571F9932A638"}
 */
function onCheckChange(status, fix) {

	switch (status) {
	case "000":
		{
			// niente
		}
		break;
	case "001":
		{
			if (fix) {
				onCheckChange("100", null);
			}
			elements.split_out.dividerLocation = 0;
			elements.split_in.dividerLocation = 0;
		}
		break;
	case "010":
		{
			if (fix) {
				onCheckChange("001", null);
			}
			elements.split_out.dividerLocation = 0;
			//FS
			controller.getWindow().getWidth()
			//dep		elements.split_in.dividerLocation = application.getWindowWidth();
			elements.split_in.dividerLocation = application.getWindow().getWidth();
		}
		break;
	case "011":
		{
			if (fix) {
				onCheckChange("010", null);
			}
			elements.split_out.dividerLocation = 0;
			//dep		elements.split_in.dividerLocation = application.getWindowWidth() / 2;
			elements.split_in.dividerLocation = application.getWindow().getWidth() / 2;

		}
		break;
	case "100":
		{
			//			elements.split_out.dividerLocation = application.getWindowWidth();
			//			elements.split_in.dividerLocation = application.getWindowWidth();
			elements.split_out.dividerLocation = application.getWindow().getWidth();
			elements.split_in.dividerLocation = application.getWindow().getWidth();
		}
		break;
	case "101":
		{
			if (fix) {
				onCheckChange("100", null);
			}
			//dep		elements.split_out.dividerLocation = (application.getWindowWidth() / 3) * 2;
			elements.split_out.dividerLocation = (application.getWindow().getWidth() / 3) * 2;
			elements.split_in.dividerLocation = 0;
		}
		break;
	case "110":
		{
			if (fix) {
				onCheckChange("100", null);
			}
			//dep		elements.split_out.dividerLocation = (application.getWindowWidth() / 3) * 2;
			//			elements.split_in.dividerLocation = application.getWindowWidth();

			elements.split_out.dividerLocation = (application.getWindow().getWidth() / 3) * 2;
			elements.split_in.dividerLocation = application.getWindow().getWidth();
		}
		break;
	case "111":
		{
			if (fix) {
				onCheckChange("100", null);
				onCheckChange("110", null);
			}
			//dep		elements.split_out.dividerLocation = application.getWindowWidth() / 3;
			//			elements.split_in.dividerLocation = application.getWindowWidth() / 3;

			elements.split_out.dividerLocation = application.getWindow().getWidth() / 3;
			elements.split_in.dividerLocation = application.getWindow().getWidth() / 3;
		}
		break;
	}
	application.updateUI();
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 *
 * @properties={typeid:24,uuid:"58160966-0869-4767-B77C-6381189EF530"}
 */
function onVersioneChange(oldValue, newValue, event) {

	//SAuc
	//	forms.dpr_as400.setRoot(k8_dummy_to_k8_distinte_progetto_core$root.descrizione,
	//		k8_dummy_to_k8_distinte_progetto_core$root.path_id,
	//		k8_dummy_to_k8_distinte_progetto_core$root.numero_disegno);
	//	forms.dpr_baan.setRoot(k8_dummy_to_k8_distinta_baan$root.descrizione,
	//		k8_dummy_to_k8_distinta_baan$root.path_id,
	//		k8_dummy_to_k8_distinta_baan$root.disegno);
	//	forms.dpr_riciclabilita.setRoot(k8_dummy_to_k8_distinte_riciclabilita$root.descrizione,
	//		k8_dummy_to_k8_distinte_riciclabilita$root.path_id,
	//		k8_dummy_to_k8_distinte_riciclabilita$root.numero_disegno);

	/** @type {JSFoundSet} */
	var fs = k8_dummy_to_k8_distinte_progetto_core$root;
	forms.dpr_as400.setRoot(fs.descrizione, fs.path_id, fs.numero_disegno);

	/** @type {JSFoundSet} */
	var fs2 = k8_dummy_to_k8_distinta_baan$root;
	forms.dpr_baan.setRoot(fs2.descrizione, fs2.path_id, fs2.disegno);

	/** @type {JSFoundSet} */
	var fs3 = k8_dummy_to_k8_distinte_riciclabilita$root;
	forms.dpr_riciclabilita.setRoot(fs3.descrizione, fs3.path_id, fs3.numero_disegno);
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"29047F36-B0C5-4178-ABC8-03E8B6ABE7F7"}
 */
function onProgettoChange(oldValue, newValue, event) {
	globals.vc2_currentVersione = null;
	onVersioneChange(null, null, null);
	return true;
}
