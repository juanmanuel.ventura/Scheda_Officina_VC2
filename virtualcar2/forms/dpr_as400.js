/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"83B72C9D-F87B-4300-B2FC-1DE837D6E7C0"}
 */
var filter = "";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"AE448500-432C-4F3B-9AFF-E1543D3A5B1C",variableType:4}
 */
var rootNumber = 0;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D1852619-F480-436B-83C5-56DC052B6B4B"}
 */
var rootPathID = "";

/**
 * @properties={typeid:24,uuid:"B333D047-8D57-4051-85FC-0FC16F49D164"}
 */
function workWrap() {
	time(work);
}

/**
 * @properties={typeid:24,uuid:"7CF60F9D-335F-45C2-BEDC-C0D26405DA86"}
 */
function work() {
	if (status != "O") {
		if (status == "+") {
			expand();
			status = "-";
		} else {
			collapse();
			status = "?";
		}
	}
}

/**
 * @properties={typeid:24,uuid:"F0621897-687D-4EB9-8F56-E7117C24A519"}
 * @AllowToRunInFind
 */
function expand() {
	var index = controller.getSelectedIndex();
	var cv = codice_vettura;
	var nd = numero_disegno;
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		ufs.codice_vettura = cv;
		ufs.codice_padre = nd;
		ufs.search(false, false);
	}
	controller.loadRecords(ufs);
	controller.setSelectedIndex(index);
}

/**
 * @properties={typeid:24,uuid:"2073AB2C-980A-417E-B17B-C8302CAA8A64"}
 * @AllowToRunInFind
 */
function collapse() {
	var index = controller.getSelectedIndex();
	var pid = path_id;
	var ufs = foundset.unrelate();
	for (var i = 1; i <= ufs.getSize(); i++) {
		var record = ufs.getRecord(i);
		if (record.path_id.search(pid) != -1) record.status = "?";
	}
	if (ufs.find()) {
		ufs.path_id = "!" + pid + "/%";
		ufs.search(false, true);
	}
	controller.loadRecords(ufs);
	controller.setSelectedIndex(index);
}

/**
 * @properties={typeid:24,uuid:"31C64BBC-B1BA-4850-9B7D-905BBFCD17FE"}
 */
function isolateWrap() {
	if (globals.vc2_currentVersione) {
		time(isolate);
	}
}

/**
 * @properties={typeid:24,uuid:"099A7C38-B63B-40C1-B097-0BE3E471835B"}
 * @AllowToRunInFind
 */
function isolate() {
	var pid = path_id;
	var d = descrizione;
	var cv = codice_vettura;
	var nd = numero_disegno;
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		ufs.codice_vettura = cv;
		ufs.codice_padre = nd;
		ufs.search();
	}
	controller.loadRecords(ufs);
	setRoot(d, pid, nd);
}

/**
 * @properties={typeid:24,uuid:"B4CB9071-23FD-4F0F-9198-CB394ACC7420"}
 */
function releaseWrap() {
	if (globals.vc2_currentVersione) {
		time(release);
	}
}

/**
 * @properties={typeid:24,uuid:"305D4F43-9DE0-439B-B916-C85460B3DD0D"}
 */
function release() {
	/** @type {JSFoundSet} */
	var fs = k8_dummy_to_k8_distinte_progetto_core;
	for (var i = 1; i <= fs.getSize(); i++) {
		var record = fs.getRecord(i);
		record.status = "?";
	}
	databaseManager.recalculate(fs);
	controller.loadRecords(fs);
	setRoot(k8_dummy_to_k8_distinte_progetto_core$root.descrizione,
		k8_dummy_to_k8_distinte_progetto_core$root.path_id,
		k8_dummy_to_k8_distinte_progetto_core$root.numero_disegno);
}

/**
 * @properties={typeid:24,uuid:"22CC55F2-7B40-4986-8827-7C2EB2679580"}
 */
function filterTreeWrap() {
	if (globals.vc2_currentVersione) {
		time(filterTree);
	}
}

/**
 * @properties={typeid:24,uuid:"FF58BCB8-81D4-46A1-B53A-44F805D5F21C"}
 * @AllowToRunInFind
 */
function filterTree() {
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		if (filter) {
			ufs.path_id = rootPathID + "/%";
			try {
				ufs.numero_disegno = java.lang.Double.parseDouble(filter);
			} catch (e) {
				ufs.descrizione = "#" + filter;
			}
		} else {
			ufs.codice_vettura = globals.vc2_currentVersione;
			ufs.codice_padre = rootNumber;
		}
		ufs.search();
	}
	controller.loadRecords(ufs);
}

/**
 * @properties={typeid:24,uuid:"EA60C451-2006-44F5-9000-916BF446E81D"}
 */
function setRoot(label, pid, number) {
	elements.root.text = label;
	rootPathID = pid;
	rootNumber = number
}

/**
 * @properties={typeid:24,uuid:"A24BF9A1-67D4-4733-BA20-71C53C2FBDC1"}
 */
function time(func) {
	//tempo di esecuzione di una funzione senza paramenti
	if (func && typeof func == "function") {
		forms.dpr_albero_container_s5.writeMessage("...operazione in corso");
		application.updateUI();
		var start = new Date();
		var result = func();
		var end = new Date();
		forms.dpr_albero_container_s5.writeMessage( ( (end - start) / 1000).toFixed(2) + "s");
		return result;
	}
	return null;
}

/**
 * Called before the form component is rendered.
 *
 * @param {JSRenderEvent} event the render event
 *
 * @private
 *
 * @properties={typeid:24,uuid:"E89C20D7-0429-423A-AF3B-DA7ED9F859C7"}
 */
function onRender(event) {
	if(event.isRecordSelected() && event.getRenderable().getName()!='AS400_label' && event.getRenderable().getName()!='root')
	{
		event.getRenderable().fgcolor = '#339eff';
		
		event.getRenderable().font='Verdana,1,11';
	}
	else{
		event.getRenderable().fgcolor = '#000000';
	}
}
