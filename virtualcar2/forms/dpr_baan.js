/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"52789B8E-56D8-4B4E-AF71-0FE0D23F21DC"}
 */
var filter = "";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"9213C808-883C-4F8F-9974-7095B4317A30",variableType:4}
 */
var rootNumber = 0;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"DB7DF6B9-5681-440E-9F81-32BDB09E0CC3"}
 */
var rootPathID = "";

/**
 * @properties={typeid:24,uuid:"A3996CD4-5795-41C9-AAE1-810C40B2B713"}
 */
function onRecordSelected() {
	globals.vc2_currentDisegno = disegno;
}

/**
 * @properties={typeid:24,uuid:"A36C549E-824C-4997-960D-A3573370AA8A"}
 */
function workWrap() {
	time(work);
}

/**
 * @properties={typeid:24,uuid:"13248508-27AE-4BE6-8DD7-8A7411CA779C"}
 */
function work() {
	if (status != "O") {
		if (status == "+") {
			expand();
			status = "-";
		} else {
			collapse();
			status = "?";
		}
	}
}

/**
 * @properties={typeid:24,uuid:"FDEF27D8-D811-4FC8-A2E2-4C9F5AAB9076"}
 * @AllowToRunInFind
 */
function expand() {
	var index = controller.getSelectedIndex();
	var cv = versione;
	var nd = disegno;
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		ufs.versione = cv;
		ufs.codice_padre = nd;
		ufs.search(false, false);
	}
	controller.loadRecords(ufs);
	controller.setSelectedIndex(index);
}

/**
 * @properties={typeid:24,uuid:"8FE273C7-ACCB-41C3-8830-1DC708ACC8A4"}
 * @AllowToRunInFind
 */
function collapse() {
	var index = controller.getSelectedIndex();
	var pid = path_id;
	var ufs = foundset.unrelate();
	for (var i = 1; i <= ufs.getSize(); i++) {
		var record = ufs.getRecord(i);
		if (record.path_id.search(pid) != -1) record.status = "?";
	}
	if (ufs.find()) {
		ufs.path_id = "!" + pid + "/%";
		ufs.search(false, true);
	}
	controller.loadRecords(ufs);
	controller.setSelectedIndex(index);
}

/**
 * @properties={typeid:24,uuid:"AFC07A54-2990-4534-8797-5658C62C84FE"}
 */
function isolateWrap() {
	if (globals.vc2_currentVersione) {
		time(isolate);
	}
}

/**
 * @properties={typeid:24,uuid:"E6062300-86F1-4DA0-A25D-F190F792CB55"}
 * @AllowToRunInFind
 */
function isolate() {
	var pid = path_id;
	var d = descrizione;
	var cv = versione;
	var nd = disegno;
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		ufs.versione = cv;
		ufs.codice_padre = nd;
		ufs.search();
	}
	controller.loadRecords(ufs);
	setRoot(d, pid, nd);
}

/**
 * @properties={typeid:24,uuid:"F3967588-F86B-46C7-BAA6-32846567FC00"}
 */
function releaseWrap() {
	if (globals.vc2_currentVersione) {
		time(release);
	}
}

/**
 * @properties={typeid:24,uuid:"C780EA66-2881-4E27-9396-F0B11D1653FE"}
 */
function release() {
	/** @type {JSFoundSet} */
	var fs = k8_dummy_to_k8_distinta_baan;
	for (var i = 1; i <= fs.getSize(); i++) {
		fs.setSelectedIndex(i);
		status = "?";
	}
	databaseManager.recalculate(fs);
	controller.loadRecords(fs);
	setRoot(k8_dummy_to_k8_distinta_baan$root.descrizione,
		k8_dummy_to_k8_distinta_baan$root.path_id,
		k8_dummy_to_k8_distinta_baan$root.disegno);
}

/**
 * @properties={typeid:24,uuid:"CAA8868D-8E86-4FD4-A5FB-44BCE5644BE8"}
 */
function filterTreeWrap() {
	if (globals.vc2_currentVersione) {
		time(filterTable);
	}
}

/**
 * @properties={typeid:24,uuid:"1E7D1FC5-D2F7-40DA-BA5B-E03B441EF4DD"}
 * @AllowToRunInFind
 */
function filterTable() {
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		ufs.vettura = globals.vc2_currentVersione.toLocaleString();
		if (filter) ufs.descrizione = "#" + filter;
		ufs.search();
	}
	controller.loadRecords(ufs);
}

/**
 * @properties={typeid:24,uuid:"E4CA09F6-C9CA-4C27-941C-797EE8C2A342"}
 * @AllowToRunInFind
 */
function filterTree() {
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		if (filter) {
			ufs.path_id = rootPathID + "/%";
			ufs.descrizione = "#" + filter;
		} else {
			ufs.versione = globals.vc2_currentVersione;
			ufs.codice_padre = rootNumber;
		}
		ufs.search();
	}
	controller.loadRecords(ufs);
}

/**
 * @properties={typeid:24,uuid:"E577F7F1-6AF4-4777-AB16-06E792449B2E"}
 */
function setRoot(label, pid, number) {
	elements.root.text = label;
	rootPathID = pid;
	rootNumber = number
}

/**
 * @properties={typeid:24,uuid:"F7C9E9AB-59C0-47B2-B78F-AE347A2947AF"}
 */
function time(func) {
	//tempo di esecuzione di una funzione senza paramenti
	if (func && typeof func == "function") {
		forms.dpr_albero_container_s5.writeMessage("...operazione in corso");
		application.updateUI();
		var start = new Date();
		var result = func();
		var end = new Date();
		forms.dpr_albero_container_s5.writeMessage( ( (end - start) / 1000).toFixed(2) + "s");
		return result;
	}
	return null;

}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"6F9CCCB1-9A4C-4E24-8B0A-9619226540A2"}
 */
function onLoad(event) {
	elements.isolate.enabled = elements.release.enabled = false;
}

/**
 * Called before the form component is rendered.
 *
 * @param {JSRenderEvent} event the render event
 *
 * @private
 *
 * @properties={typeid:24,uuid:"12A1A9BA-05CC-46CD-B558-8B14C23D333D"}
 */
function onRender(event) {
	if(event.isRecordSelected() && event.getRenderable().getName()!='BAAN_label' && event.getRenderable().getName()!='root')
	{
		event.getRenderable().fgcolor = '#339eff';
		
		event.getRenderable().font='Verdana,1,11';
	}
	else{
		event.getRenderable().fgcolor = '#000000';
	}
}
