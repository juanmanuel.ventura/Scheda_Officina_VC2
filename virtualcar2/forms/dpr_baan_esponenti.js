/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"C4204E67-E724-4BBE-AC21-84D4F27992F3"}
 */
function onLoad(event) {
	controller.readOnly = true;
}

/**
 * Handle start of a drag, it can set the data that should be transfered and should return a constant which dragndrop mode/modes is/are supported.
 *
 * Should return a DRAGNDROP constant or a combination of 2 constants:
 * DRAGNDROP.MOVE if only a move can happen,
 * DRAGNDROP.COPY if only a copy can happen,
 * DRAGNDROP.MOVE|DRAGNDROP.COPY if a move or copy can happen,
 * DRAGNDROP.NONE if nothing is supported (drag should start).
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Number} DRAGNDROP.MOVE one of the DRAGNDROP constant (or a combination)
 *
 * @properties={typeid:24,uuid:"4C66C804-CA5A-43FB-A892-2C42DF0B7A2B"}
 */
function onDrag(event) {
	var src = event.getSource();
	if(src){
		var baan = forms.dpr_baan.foundset.duplicateFoundSet();
		var data = {baan: null, exp: null};
		data.baan = baan.getRecord(baan.getSelectedIndex());
		data.exp = foundset.getRecord(foundset.getSelectedIndex());
		event.data = data;
		return DRAGNDROP.MOVE;
	}
	return DRAGNDROP.NONE
}
