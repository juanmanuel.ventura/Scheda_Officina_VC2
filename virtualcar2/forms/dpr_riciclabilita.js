/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"8CA34F92-DEDD-4405-A383-C04BF73353F0"}
 */
var filter = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"FB2128C4-1EB6-4F31-9BB4-7EA01EA8D74A"}
 */
var filterDate = "";

/**
 * @properties={typeid:35,uuid:"4387DCFC-36AE-49F0-8E41-3DD559BD28CC",variableType:-4}
 */
var menu = new Array();

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"8A07B543-89A7-41C3-A300-BBF1457A794E",variableType:4}
 */
var rootNumber = 0;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"2981A3CF-E4A2-4D8C-9541-5ABB6246E360"}
 */
var rootPathID = "";

/**
 * @properties={typeid:35,uuid:"AE4E6CB5-8F17-4352-9CE5-5BEB7C8F89DC",variableType:-4}
 */
var th = null;

/**
 * @properties={typeid:35,uuid:"31F35555-39BD-4BF6-B7C3-31F16F5585A7",variableType:-4}
 */
var thGo = null;

/**
 * @properties={typeid:35,uuid:"525A131F-784E-4509-8A3E-172E0BE963F9",variableType:-4}
 */
var status = null;

/**
 * Handle a drag over.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} true if a drop can happen on the element
 *
 * @properties={typeid:24,uuid:"239B9185-E613-4DAA-8334-FB337569D139"}
 */
function onDragOver(event) {
	var source = event.getSource();
	if (source && event.data) {
		return true;
	}
	return false;
}

/**
 * Handle a drop.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} true if a drop could be made
 *
 * @properties={typeid:24,uuid:"AE23C3E0-0667-4196-A0AB-B28903BF9A43"}
 */
function onDrop(event) {
	
	var baan = event.data.baan;
	var exp = event.data.exp;
	var ufs = foundset.unrelate();
	if(baan && exp && ufs){
		insertNode(ufs, baan, exp);	
	foundset.loadRecords(ufs);
	databaseManager.saveData();
	databaseManager.recalculate(k8_dummy_to_k8_distinte_riciclabilita);
	return true;
	}
	return false;
}

/**
 * @properties={typeid:24,uuid:"17EBAFBC-BDA5-4929-9DC2-E208356CC1AA"}
 */
function insertNode(fs, baan, exp) {

	fs.newRecord();

	//da baan
	fs.progetto = baan.progetto;
	fs.codice_padre = baan.codice_padre;
	fs.descrizione_padre = baan.descrizione_padre;
	fs.progressivo = baan.progressivo;
	fs.livello = baan.livello;
	fs.codice_componente = baan.codice_articolo_baan;
	fs.descrizione = baan.descrizione;
	fs.udm = baan.udm;
	fs.quantita_impiego = baan.quantita;
	fs.peso_anagrafica = baan.peso_anagrafica;
	fs.data_inizio = baan.data_inizio;
	fs.data_fine = baan.data_fine;
	fs.tipo = baan.tipo_articolo;
	fs.numero_disegno = baan.disegno;
	fs.peso_disegno = baan.peso_disegno;
	fs.codice_fornitore = baan.fornitore;
	fs.versione = baan.versione;
	fs.codice_vettura = baan.vettura;
	fs.origine = baan.origine;

	//da baan esponenti
	fs.esponente = exp.esponente;
	fs.peso_effettivo = exp.peso_effettivo;
	fs.peso_target = exp.peso_target;

	//aggiunti all'ultimo
	fs.tipologia = baan.tipologia;
	fs.peso_mds = null;
	fs.codice_mds = null;
	fs.accettato = null;
	fs.codice_mds = null;

	//questi boh...
	fs.unita_misura = "N";
	fs.unita_misura_materiale = "KG";
	fs.peso = null;

	//TODO: BISOGNA RICREARE I PATH
	fs.export_line = 1;

	fs.data_in = new Date();
}

/**
 * @properties={typeid:24,uuid:"D6EF585F-61DD-46D7-90F0-A49E627ADFA6"}
 */
function workWrap() {
	time(work);
}

/**
 * @properties={typeid:24,uuid:"EE6F3C47-9EF1-413C-B35A-E4558914ED55"}
 */
function work() {
	if (status != "O") {
		if (status == "+") {
			expand();
			status = "-";
		} else {
			collapse();
			status = "?";
		}
	}
}

/**
 * @properties={typeid:24,uuid:"BB29110C-2DA7-4892-B954-61C1354509BB"}
 * @AllowToRunInFind
 */
function expand() {
	var index = controller.getSelectedIndex();
	var cv = versione;
	var nd = numero_disegno;
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		ufs.versione = cv;
		ufs.codice_padre = nd;
		ufs.search(false, false);
	}
	controller.loadRecords(ufs);
	controller.setSelectedIndex(index);
}

/**
 * @properties={typeid:24,uuid:"68879CAD-9790-4745-AB42-0E5497014FE0"}
 * @AllowToRunInFind
 */
function collapse() {
	var index = controller.getSelectedIndex();
	var pid = path_id;
	var ufs = foundset.unrelate();
	for (var i = 1; i <= ufs.getSize(); i++) {
		ufs.setSelectedIndex(i);
		status = "?";
	}
	if (ufs.find()) {
		ufs.path_id = "!" + pid + "/%";
		ufs.search(false, true);
	}
	controller.loadRecords(ufs);
	controller.setSelectedIndex(index);
}

/**
 * @properties={typeid:24,uuid:"9F0F357E-401E-40E8-8853-16B413DDD5CC"}
 */
function isolateWrap() {
	time(isolate);
}

/**
 * @properties={typeid:24,uuid:"9EA7AB3E-1AAB-48FA-A802-3BC59A1E076B"}
 * @AllowToRunInFind
 */
function isolate() {
	var pid = path_id;
	var d = descrizione;
	var cv = versione;
	var nd = numero_disegno;
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		ufs.versione = cv;
		ufs.codice_padre = nd;
		ufs.search();
	}
	controller.loadRecords(ufs);
	setRoot(d, pid, nd);
}

/**
 * @properties={typeid:24,uuid:"B048E278-D38C-47E9-B63E-EE48E6F3EE9F"}
 */
function releaseWrap() {
	time(release);
}

/**
 * @properties={typeid:24,uuid:"5ECDC0FA-D967-426A-A80E-CAB2A5EB54D2"}
 */
function release() {
	/** @type {JSFoundSet} */
	var fs = k8_dummy_to_k8_distinte_riciclabilita;
	for (var i = 1; i <= fs.getSize(); i++) {
		fs.setSelectedIndex(i);
		status = "?";
	}
	databaseManager.recalculate(fs);
	controller.loadRecords(fs);
	setRoot(k8_dummy_to_k8_distinte_riciclabilita$root.descrizione,
		k8_dummy_to_k8_distinte_riciclabilita$root.path_id,
		k8_dummy_to_k8_distinte_riciclabilita$root.numero_disegno);
}

/**
 * @properties={typeid:24,uuid:"A07610F6-839F-46C3-AA17-427E0A059D1B"}
 */
function filterTreeWrap() {
	if (globals.vc2_currentVersione) {
		time(filterTable);
	}
}

/**
 * @properties={typeid:24,uuid:"CB8CBB35-6063-4A5B-85E1-142E9F548005"}
 * @AllowToRunInFind
 */
function filterTable() {
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		ufs.codice_vettura = globals.vc2_currentVersione.toLocaleString();
		if (filter) {
			try {
				ufs.numero_disegno = java.lang.Double.parseDouble(filter);
			} catch (e) {
				ufs.descrizione = "#" + filter;
			}
		}
		if (filterDate) {
			ufs.data_in = "<=#" + filterDate + "|dd/MM/yyyy";
			ufs.data_out = "^||>#" + filterDate + "|dd/MM/yyyy";
		} else {
			ufs.data_in = "!^";
			ufs.data_out = "^";
		}
		ufs.search();
		//globals.utils_printFoundset(ufs);
	}
	controller.loadRecords(ufs);
}

/**
 * @properties={typeid:24,uuid:"9153C8C6-8164-4B90-A2C7-51039048FD9C"}
 * @AllowToRunInFind
 */
function filterTree() {
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		if (filter) {
			ufs.path_id = rootPathID + "/%";
			try {
				ufs.numero_disegno = java.lang.Double.parseDouble(filter);
			} catch (e) {
				ufs.descrizione = "#" + filter;
			}
		} else {
			ufs.codice_vettura = globals.vc2_currentVersione;
			ufs.codice_padre = rootNumber;
		}
		ufs.search();
	}
	controller.loadRecords(ufs);
}

/**
 * @properties={typeid:24,uuid:"455F20C9-C4C7-4082-87B6-BF8061D39DEA"}
 */
function setRoot(label, pid, number) {
	elements.root.text = label;
	rootPathID = pid;
	rootNumber = number
}

/**
 * @properties={typeid:24,uuid:"2B309C05-4CAD-4EC8-A057-8A0F6D881BA1"}
 */
function time(func) {
	//tempo di esecuzione di una funzione senza paramenti
	if (func && typeof func == "function") {
		forms.dpr_albero_container_s5.writeMessage("...operazione in corso");
		application.updateUI();
		var start = new Date();
		var result = func();
		var end = new Date();
		forms.dpr_albero_container_s5.writeMessage( ( (end - start) / 1000).toFixed(2) + "s");
		return result;
	}
	return null;
}

/**
 * @properties={typeid:24,uuid:"E5D6B637-F790-4FC7-B942-C94AAB588CA8"}
 */
function remove() {
	data_out = new Date();
	databaseManager.saveData();
	databaseManager.refreshRecordFromDatabase(k8_dummy_to_k8_distinte_riciclabilita, -1);
	controller.loadRecords(k8_dummy_to_k8_distinte_riciclabilita);
}

/**
 * @properties={typeid:24,uuid:"A54CDE8A-2C02-4DA2-AAD8-270333876725"}
 */
function excelImportWrap() {
	//SAuc
	var from = arguments[5];
	if (from == "baan") {
		baanImport();
	}
	if (from == "imds") {
		imdsImport();
	}
}

/**
 * @properties={typeid:24,uuid:"789E97EC-6CA8-4EEB-A356-023EEA307AFA"}
 */
function baanImport() {
	controller.loadRecords(k8_dummy_to_k8_distinte_riciclabilita$nohistory);
	if (k8_dummy_to_k8_distinte_riciclabilita$nohistory.getSize() > 0) {
		if ("Sì" == plugins.dialogs.showQuestionDialog("Eliminare?", "Per portare a termine l'import della nuova DBR è necessario cancellare tutti i record esistenti, procedere?", "Sì", "No")) {
			k8_dummy_to_k8_distinte_riciclabilita$nohistory.deleteAllRecords();
		} else {
			controller.loadRecords(k8_dummy_to_k8_distinte_riciclabilita);
			plugins.dialogs.showInfoDialog("Annullato", "Import annullato", "Ok");
			return;
		}
	}
	globals.utils_openDefaultExcelImport();
	controller.loadRecords(k8_dummy_to_k8_distinte_riciclabilita);
	plugins.dialogs.showInfoDialog("Eseguito", "Import eseguito", "Ok");
}

/**
 * @properties={typeid:24,uuid:"9EC71D85-1453-452A-B0A2-9F0A84CE4ECC"}
 */
function imdsImport() {
	globals.utils_openDefaultExcelImport();
	//FS
	//dep	var tc = databaseManager.getTableCount(databaseManager.getFoundSet(controller.getServerName(),"k8_import"));
	var tc = databaseManager.getTableCount(databaseManager.getFoundSet(databaseManager.getDataSourceServerName(controller.getDataSource()), "k8_import"));

	if (tc > 0) {
		time(launchProcedure);
	}
}

/**
 * @properties={typeid:24,uuid:"07322DF7-23A0-452A-9FC2-E4FE81523C6D"}
 */
function launchProcedure() {
	//FS
	//dep	plugins.rawSQL.executeStoredProcedure(controller.getServerName(),"{call K8_IMPORT_MDS_DATA(?)}",[globals.vc2_currentVersione],[0],0);
	plugins.rawSQL.executeStoredProcedure(databaseManager.getDataSourceServerName(controller.getDataSource()), "{call K8_IMPORT_MDS_DATA(?)}", [globals.vc2_currentVersione], [0], 0);
	databaseManager.refreshRecordFromDatabase(foundset, -1);
}

/**
 * @properties={typeid:24,uuid:"6F84F705-C2D9-46A1-812E-862B7639F9EF"}
 */
function exportToFelisWrap() {
	if (globals.vc2_currentVersione) {
		var buffer = time(exportToFelis);
		globals.utils_saveAsTXT(buffer, "export-felis-" + utils.dateFormat(new Date(), "yyyyMMdd") + ".txt");
	}
}

/**
 * @properties={typeid:24,uuid:"19F64D58-0FD4-49CF-A62A-EBBC9415FB74"}
 */
function exportToFelis() {
	var query = "select 'xxxxxxxxxx\"\",\"\",\"'||replace(to_char(NUMERO_DISEGNO,'000000000'),' ','')||'M\",'||substr(concat(DESCRIZIONE,'                                    '),1,36)||'\",\"\",\",\"\",\"'||replace(to_char(QUANTITA_IMPIEGO,'00000.0000'),' ','')||'\",,,,,,\"\",\"\"\r\n' as export from K8_DISTINTE_RICICLABILITA where CODICE_VETTURA = ? and EXPORT_LINE = ? order by PATH_ID asc";
	var args = [globals.vc2_currentVersione, 1];
	//	var query = databaseManager.getSQL(foundset).replace("K8_DISTINTE_RICICLABILITA_ID", "'xxxxxxxxxx\"\",\"\",\"'||replace(to_char(NUMERO_DISEGNO,'000000000'),' ','')||'M\",'||substr(concat(DESCRIZIONE,'                                    '),1,36)||'\",\"\",\",\"\",\"'||replace(to_char(QUANTITA_IMPIEGO,'00000.0000'),' ','')||'\",,,,,,\"\",\"\"\r\n' as export").replace("where", "where EXPORT_LINE = ? and ");
	//	var args = [1].concat(databaseManager.getSQLParameters(foundset));
	//FS
	//dep	var ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,-1);
	var ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, -1);

	var max = ds.getMaxRowIndex();
	var exportDate = (filterDate) ? filterDate.replace(/\//g, "-") : utils.dateFormat(new Date(), "yyyy-MM-dd");
	var buffer = "\"#ORDER 1: " + globals.vc2_currentVersione + "                         " + exportDate + "                                   \" \"\"\r\n\r\n";
	for (var i = 1; i <= max; i++) {
		buffer += ds.getValue(i, 1);
	}
	return buffer;
}

/**
 * @properties={typeid:24,uuid:"00AF20BF-D708-4063-93C9-D5816FA7C5D5"}
 */
function fillUpWrap() {
	var result = time(fillUp);
	if (result) {
		var max = result.max;
		var gc = result.gc;
		var ngc = result.ngc;
		(gc == max) ? plugins.dialogs.showInfoDialog("Operazione terminata", gc + " record importati con successo.", "Ok") : plugins.dialogs.showWarningDialog("Operazione terminata", gc + "/" + max + " record importati, " + ngc + " hanno creato problemi.", "Ok");
	} else {
		plugins.dialogs.showWarningDialog("Attenzione", "Questa funzionalità è utilizzabile solo all'atto della creazione DBR", "Ok");
	}
}

/**
 * @properties={typeid:24,uuid:"67DFEA9A-1FE5-41D7-B7D8-D103B0DD3EF0"}
 */
function fillUp() {
	if (!globals.vc2_currentVersione || k8_dummy_to_k8_distinte_riciclabilita.getSize() > 0) {
		return null;
	}

	var query = null;
	var args = null;

	query = "select * from K8_COMPONENTI_COMUNI_BAAN_AS where VETTURA = ?";
	args = [globals.vc2_currentVersione];
	//FS
	//dep	var ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,-1);
	var ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, -1);

	var max = ds.getMaxRowIndex();
	query = "insert into K8_DISTINTE_RICICLABILITA (NUMERO_DISEGNO,CODICE_VETTURA,ESPONENTE,PROGETTO,CODICE_PADRE,DESCRIZIONE_PADRE,PROGRESSIVO,LIVELLO,CODICE_COMPONENTE,DESCRIZIONE,UDM,QUANTITA_IMPIEGO,PESO_ANAGRAFICA,DATA_INIZIO,DATA_FINE,TIPO,PESO_DISEGNO,CODICE_FORNITORE,VERSIONE,ORIGINE,TIPOLOGIA,PESO_TARGET,PESO_EFFETTIVO,UNITA_MISURA,UNITA_MISURA_MATERIALE,EXPORT_LINE,DATA_IN,DATA_CREAZIONE,UTENTE_CREAZIONE) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'N','KG',1,sysdate,sysdate,?)";
	globals.nfx_progressBarShow(max);
	var gc = 0;
	var ngc = 0;
	var r = null;
	for (var i = 1; i <= max; i++) {
		args = ds.getRowAsArray(i).concat([globals.nfx_user]);
		//dep		r = plugins.rawSQL.executeSQL(controller.getServerName(),
		//									  controller.getTableName(),
		//									  query,args);
		r = plugins.rawSQL.executeSQL(databaseManager.getDataSourceServerName(controller.getDataSource()),
			databaseManager.getDataSourceTableName(controller.getDataSource()),
			query, args);
		(r) ? gc++ : ngc++;
		globals.nfx_progressBarStep();
	}
	databaseManager.refreshRecordFromDatabase(k8_dummy_to_k8_distinte_riciclabilita, -1);
	controller.loadRecords(k8_dummy_to_k8_distinte_riciclabilita);
	return { max: max, gc: gc, ngc: ngc };
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"C8DCA7B7-1C47-4EA7-9EC9-71252381554A"}
 */
function setDate(event) {
	var filterCheck = globals.utils_clone(filterDate);
	var selectedDate = (filterDate) ? globals.utils_stringToDate(filterDate) : new Date();
	filterDate = utils.dateFormat(application.showCalendar(selectedDate, "dd/MM/yyyy"), "dd/MM/yyyy");
	if (!filterDate) {
		if (filterCheck && plugins.dialogs.showQuestionDialog("Annulla filtraggio", "Verrà rimosso il filtro per data, procedere?", "Sì", "No") == "Sì") {
			filterTreeWrap();
		} else {
			filterDate = filterCheck;
		}
	} else {
		filterTreeWrap();
	}

}

/**
 * @properties={typeid:24,uuid:"66CDAB50-F074-48AE-8D67-2CFCB6AEDCBA"}
 */
function createMenu() {
	//FS
	//dep	var text = plugins.popupmenu.createMenuItem("Testo",showTextFilter,"media:///text16.png");
	//	text.setMethodArguments(true);
	//	var date = plugins.popupmenu.createMenuItem("Data",showTextFilter,"media:///calendar16.png");
	//	date.setMethodArguments(false);
	//	var searches =  [text,date];
	//
	//	var xport = [plugins.popupmenu.createMenuItem("per FELIS",exportToFelisWrap,"media:///treeleaf.png")];
	//	var iport = [plugins.popupmenu.createMenuItem("da BAAN",excelImportWrap,"media:///treeleaf.png"),
	//	             plugins.popupmenu.createMenuItem("da IMDS",excelImportWrap,"media:///treeleaf.png")];
	//	iport[0].setMethodArguments("baan");
	//	iport[1].setMethodArguments("imds");
	//
	//
	//	var filters = [plugins.popupmenu.createMenuItem("Componenti comuni AS400-BaaN",fillUpWrap,"media:///db16.png")];
	//
	//	menu = [plugins.popupmenu.createMenuItem("Cerca",searches,"media:///find16.png"),
	//	        plugins.popupmenu.createMenuItem("Esporta",xport,"media:///aw-right16.png"),
	//	        plugins.popupmenu.createMenuItem("Importa",iport,"media:///aw-left16.png"),
	//	        plugins.popupmenu.createMenuItem("Filtri",filters,"media:///smartfolder.png")];

	menu = plugins.window.createPopupMenu();
	//primo subMenu
	
	var searchMenu = menu.addMenu("Cerca");
	searchMenu.setIcon("media:///find16.png");
	var text = searchMenu.addMenuItem("Testo", showTextFilter, "media:///text16.png");
	text.methodArguments = [true];
	var date = searchMenu.addMenuItem("Data", showTextFilter, "media:///calendar16.png")
	date.methodArguments = [false];
	//secondo subMenu
	var exportMenu = menu.addMenu("Esporta");
	exportMenu.setIcon("media:///aw-right16.png");
	exportMenu.addMenuItem("per FELIS", exportToFelisWrap, "media:///treeleaf.png");
	var importMenu = menu.addMenu("Importa");
	importMenu.setIcon("media:///aw-left16.png");
	var baan = importMenu.addMenuItem("da BAAN", excelImportWrap, "media:///treeleaf.png");
	baan.methodArguments = ["baan"];
	var imds = importMenu.addMenuItem("da IMDS", excelImportWrap, "media:///treeleaf.png")
	imds.methodArguments = ["imds"];
	var filtersMenu = menu.addMenu("Fltri");
	filtersMenu.setIcon("media:///smartfolder.png");
	filtersMenu.addMenuItem("Componenti comuni AS400-BaaN", fillUpWrap, "media:///db16.png");
}

/**@param {JSEvent} event
 * @properties={typeid:24,uuid:"0DBEAF8B-28C7-4A3E-91AC-F241CF1D6423"}
 */
function openMenu(event) {
	if (globals.vc2_currentVersione) {
		//FS
		//dep		plugins.popupmenu.showPopupMenu(elements.menu, menu);
		menu.show(elements.menu);

	}
}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"8348A985-5B10-4E6A-B11C-38A15C01918E"}
 */
function onLoad(event) {
	createMenu();
	//FS 
	showTextFilter(null,null,null,null,null,true);
	elements.isolate.enabled = elements.release.enabled = false;
}

/**@param {Number} index  
 * @param {Number} parentIndex
 * @param {Boolean} isSelected
 * @param {String} parentText
 * @param {String} menuText
 * @param {Boolean} showText
 * @properties={typeid:24,uuid:"B55C6B42-9D85-4131-94D6-9AED5321D1F1"}
 */
function showTextFilter(index,parentIndex,isSelected,parentText,menuText,showText) {
	elements.filter.visible = elements.filter_icon.visible = showText;
	elements.filterDate.visible = elements.filterDate_icon.visible = !showText;
}

/**
 * @properties={typeid:24,uuid:"30132128-5068-44D4-AB66-30CFBD8CEDF6"}
 */
function thRun() {
	while (thGo) {
		elements.filtered.visible = (filter || filterDate) ? true : false;
		elements.filtered.toolTipText = "Descrizione: " + (filter ? "abilitato" : "disabilitato") + " | Data: " + (filterDate ? "abilitato" : "disabilitato") + " | Click per rimuovere tutti i filtri...";

		//FS
		//dep	th.sleep(1000);
		java.lang.Thread.sleep(1000);
	}
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"F6B914A4-9A54-485B-93BA-0CCF6711B8E9"}
 */
function onShowForm(firstShow, event) {
	thGo = true;
	th = new java.lang.Thread(thRun);
	th.start();
}

/**
 * Handle hide window.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} allow hide
 *
 * @properties={typeid:24,uuid:"E712552D-D6CD-4024-86CA-CB97E147E01A"}
 */
function onHide(event) {
	thGo = null;
	return true;
}

/**
 * @properties={typeid:24,uuid:"E7733A98-04BC-4694-9476-0594749476D3"}
 */
function clearFilter() {
	filter = filterDate = null;
	filterTreeWrap();
}

/**
 * Called before the form component is rendered.
 *
 * @param {JSRenderEvent} event the render event
 *
 * @private
 *
 * @properties={typeid:24,uuid:"F638E98C-5B8C-4327-9BCD-AACBBB518AE8"}
 */
function onRender(event) {
	if(event.isRecordSelected() && event.getRenderable().getName()!='riciclabilità_label' && event.getRenderable().getName()!='root')
	{
		event.getRenderable().fgcolor = '#339eff';
		
		event.getRenderable().font='Verdana,1,11';
	}
	else{
		event.getRenderable().fgcolor = '#000000';
	}
}
