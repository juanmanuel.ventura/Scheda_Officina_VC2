dataSource:"db:/ferrari/k8_enti_validi",
initialSort:"ente asc",
items:[
{
height:70,
partType:5,
typeid:19,
uuid:"1AFC23A4-C0C8-4D32-971A-15F0DFCE7184"
},
{
height:100,
partType:8,
typeid:19,
uuid:"479D978F-7F2E-421B-AE72-AE8CA73BB317"
},
{
anchors:11,
labelFor:"descrizione",
location:"80,30",
mediaOptions:14,
size:"220,20",
styleClass:"table",
tabSeq:-1,
text:"Descrizione",
typeid:7,
uuid:"48018BB4-D594-4C2E-8407-8B95800565E0"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"descrizione",
editable:false,
location:"80,50",
name:"descrizione",
size:"220,20",
text:"Descrizione",
typeid:4,
uuid:"4A07F4E2-7510-4F17-AAB5-0606E15EA510"
},
{
imageMediaID:"B2359D26-06A5-4F80-A5AA-67C1EF72F90F",
location:"200,75",
mediaOptions:10,
name:"choose",
onActionMethodID:"68969756-525A-4464-A376-FAFA13DED252",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
size:"90,20",
text:"Scegli",
typeid:7,
uuid:"5215DAE7-7723-4E34-A43D-C765659634CA"
},
{
imageMediaID:"0C215A75-BAAC-42C1-93CF-CB0232329C0F",
location:"21,6",
mediaOptions:10,
size:"18,18",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"6B17EFB6-CBFF-434D-B9B8-D7F64B3A4727"
},
{
anchors:11,
labelFor:"ente",
location:"0,30",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Ente",
typeid:7,
uuid:"9FA724B0-D2A3-41BF-A156-B681A74105A9"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"ente",
editable:false,
location:"0,50",
name:"ente",
size:"80,20",
text:"Ente",
typeid:4,
uuid:"C8FB4FD2-7BD1-4A70-A4AF-59061DFBF0B3"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"smart",
location:"40,6",
name:"smart",
onDataChangeMethodID:"D4312693-D84D-4185-BC96-C8C385FFEACE",
size:"240,18",
text:"Smart",
transparent:true,
typeid:4,
uuid:"CC5787E5-B1A3-4959-8A3D-E8F7224BD29D"
},
{
formIndex:-1,
lineSize:1,
location:"5,5",
roundedRadius:20,
shapeType:2,
size:"290,20",
typeid:21,
uuid:"E4457298-DA04-4761-80B9-D85BA4D46C4D"
},
{
height:30,
partType:1,
typeid:19,
uuid:"ECC9FB80-47CB-42B4-AE2A-288EF3B089D2"
}
],
name:"ente_chooser",
navigatorID:"-1",
paperPrintScale:100,
showInMenu:true,
size:"300,100",
styleName:"keeneight",
titleText:"Scelta ente",
typeid:3,
uuid:"B8F48AED-482A-43EC-99BD-BE045D625306",
view:3