/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"8A22B1ED-5A9B-4437-B102-949034FAB2E4"}
 */
var field = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"E9CA017A-44BE-47C7-9543-56AA63EB000F"}
 */
var form = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"27ADA16D-43BB-4F8E-81AB-2C78A642E179"}
 */
var smart = "";

/**
 * @properties={typeid:24,uuid:"71565DBB-3B8C-433B-8B4E-D542D889A518"}
 */
function openPopup(fo,fi){
	setData(fo,fi);
	controller.show("enti");
}

/**
 * @properties={typeid:24,uuid:"89877B22-A08C-468E-B018-A81E808C199E"}
 */
function setData(fo,fi){
	if(form != fo || field != fi){
		form = fo;
		field = fi;
		smart = "";
		filterData();
	}
	controller.readOnly = true;
	elements.smart.readOnly = false;
	
	var mode = forms.nfx_interfaccia_pannello_buttons.mode;
	elements.choose.enabled = (mode == "add" || mode == "edit") ? true : false;
}

/**
 * @properties={typeid:24,uuid:"D4312693-D84D-4185-BC96-C8C385FFEACE"}
 * @AllowToRunInFind
 */
function filterData(){
	if(foundset.find()){
		foundset.ente = "#%" + smart;
		if(!foundset.search() && foundset.find()){
			foundset.descrizione = "#%" + smart + "%";
			foundset.search();
		}
	}	
}

/**@param{JSEvent} event
 * @properties={typeid:24,uuid:"68969756-525A-4464-A376-FAFA13DED252"}
 */
function save(event){
	forms[form][field] = ente;
	
//dep	if(application.isFormInDialog(controller.getName())){
//application.output("Tipo finestra "+application.getActiveWindow().getType()+ "JSWindow.DIALOG è la costante "+JSWindow.DIALOG);
if(application.getActiveWindow().getType()==JSWindow.DIALOG){
		//application.closeFormDialog("enti");
		var w=controller.getWindow();
		w.destroy();
	}
}
