/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C0806D8E-55FF-4E20-B97F-85263B5E4BA2"}
 */
var nfx_orderBy = "nfx_acces_id desc";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D6FA2936-6129-415A-AE42-9FF902B46C92"}
 */
var nfx_related = null;

/**
 *
 * @properties={typeid:24,uuid:"537FCFC2-A4F8-4226-B05B-AA506A7097D3"}
 */
function nfx_defineAccess(){
	return [false,false,false,true];
}
