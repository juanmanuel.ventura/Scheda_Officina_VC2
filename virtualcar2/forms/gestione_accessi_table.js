/**
 * @properties={typeid:24,uuid:"E1E1C37C-47A9-4F33-AE3F-40E36F78B213"}
 */
function nfx_getTitle(){
	return "Gestione Accessi";
}

/**
 * @properties={typeid:24,uuid:"EEDBD3BD-6ABC-4DA6-B37E-C356EA67983E"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"90450EF8-92B4-4C20-B098-780DE7B2A4A7"}
 */
function nfx_defaultPermissions(){
	return "^";
}
