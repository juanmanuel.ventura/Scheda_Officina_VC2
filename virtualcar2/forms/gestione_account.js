/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"D26A019F-3308-4E78-BFCD-9DD6A405AFA7",variableType:4}
 */
var activateWebServices = 0;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C47F70E9-E018-40F6-BB1A-F4A75C956489"}
 */
var nfx_orderBy = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F234EBAC-1BC2-4773-82B4-AAF191F0C7FB"}
 */
var nfx_related = null;

/**
 * @properties={typeid:35,uuid:"C7963B4A-27A0-4C79-8AB8-DE1ED394B348",variableType:-4}
 */
var tableFiltered = false;

/**
 * @properties={typeid:24,uuid:"BA9C8EE3-E6A3-477D-987E-D601126C1DE8"}
 */
function nfx_onShow(){
	if(!globals.isPowerUser(globals.nfx_user)){
		var server = databaseManager.getDataSourceServerName(controller.getDataSource());
		var table = databaseManager.getDataSourceTableName(controller.getDataSource());
		databaseManager.addTableFilterParam(server,table,"user_servoy","=",globals.nfx_user,"user_restriction");
		tableFiltered = true;
	}
}

/**
 * @properties={typeid:24,uuid:"8D2AD12F-F0CA-4CD8-9B0F-F19B5FFC3572"}
 */
function nfx_onHide(){
	if(tableFiltered){
		var server = databaseManager.getDataSourceServerName(controller.getDataSource());
		databaseManager.removeTableFilterParam(server,"user_restriction");
		tableFiltered = false;
	}
}

/**
 * @properties={typeid:24,uuid:"A4E68A49-534D-469D-8553-692636E16377"}
 */
function nfx_defineAccess(){
	return [false,false,true,true];
}

/**
 * @properties={typeid:24,uuid:"3B1322CE-5A10-4104-89D1-A5FEEDEAB295"}
 */
function nfx_validate(){
	var allFields = {"user_as400"	:	"Nome utente - AS400",
	                 "ente_as400"	:	"Ente"};
	
	return globals.utils_defaultValidationMethod(controller.getName(),allFields);
}

/**
 * @properties={typeid:24,uuid:"A1A3C201-745B-4C12-8D1B-3849ADCC17F0"}
 */
function nfx_postValidate(){
	return sendToService();
}

/**
 * @properties={typeid:24,uuid:"017D03C6-23F5-402B-83EB-D74C5B03D237"}
 */
function sendToService(){
	if(activateWebServices == 0){
		return (transmitData() != -1) ? 1 : -1;
	}else{
		return 1;
	}
}

/**
 * @properties={typeid:24,uuid:"FFABE244-CF29-48DF-8F2A-BFFBB0E70FAE"}
 */
function transmitData(){
	var data = packData();
//FS
	
//dep	var p = plugins.http.getPoster(globals.vc2_serviceLocationCambioEnte,"gestione_account");
	var client = plugins.http.createNewHttpClient();
	var postReq=client.createPostRequest(globals.vc2_serviceLocationCambioEnte);

//dep	p.setCharset("ISO-8859-1");
	postReq.setCharset("ISO-8859-1");
	for(var s in data){
//dep		p.addParameter(s,data[s]);
		postReq.addHeader(s,data[s])
	}
//dep	var _code = p.doPost();
	var res = postReq.executeRequest();
	application.output(res);
	var response = null;
//dep	if(_code == 200){
	if(res!=null){
	if (res.getStatusCode() == 200){
//dep		response = globals.vc2_parseResponse(foundset, p.getPageData(), "ESSANO", ["SAUSER","SACENT","SANOMI"]);
		response = globals.vc2_parseResponse(foundset, res.getResponseBody(), "ESSANO", ["SAUSER","SACENT","SANOMI"]);

	}else{
		// Codice Errore WSNET1: errore nella connessione di rete il servizio non ha risposto 200.
		plugins.dialogs.showErrorDialog("Dati segnalazione non trasmessi","Il servizio AS400 non e' al momento disponibile. (WSNET1)","Ok");
		globals.vc2_lastErrorLog = "Codice: " + res.getStatusCode() + " - Il servizio AS400 non e' al momento disponibile. (WSNET1)";
		response = -1;
	}
	}
	//Inizio LOG
	var table = databaseManager.getDataSourceTableName(controller.getDataSource());
	var table_key = k8_gestione_utenze_id;
	var logic_key = user_as400;
	globals.vc2_logTransactionToAS400(table,table_key,logic_key,data,globals.vc2_lastErrorLog,response,globals.vc2_getLogOperation());
	//Fine LOG
	return response;
}

/**
 * @properties={typeid:24,uuid:"5FD7730B-6DCA-4BDE-90DC-0BE432EB669F"}
 */
function packData(){
	var usr = globals.vc2_getUserInfo(null);
	
	var data = {'BRAND'		: azienda, //Azienda: non abbligatorio
	            'SACTIP'	: "T", //Obbligatorio: fisso a T
	            'SAVEEX'	: "E", //Modalità: obbligatorio, V(verifica) o E(esecuzione)
	            'SAOP'		: "U", //Operazione: obbligatorio, fisso a U (update)
	            'SAUSER'	: user_as400, //Nome utente da associare (obbligatorio)
	            'SACENT'	: ente_as400, //Ente da associare (obbligatorio)
	            'SANOMI'	: usr.user_as400, //Utente che effettua la chiamata (obbligatorio)
	            'SADATI'	: globals.utils_dateFormatAS400(new Date()), //Data di modifica, valorizzato con la data al momento dell'operazione
	            'SALING'	: "I" //Lingua: non obbligatorio, E(inglese) o I(italiano)
	           };
	return data;
}
