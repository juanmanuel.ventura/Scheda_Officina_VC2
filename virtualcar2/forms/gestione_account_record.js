/**
 * @properties={typeid:24,uuid:"8C12AE9D-19B5-43CC-9220-F2D7AB6AF3BF"}
 */
function nfx_getTitle() {
	return "Gestione account";
}

/**
 * @properties={typeid:24,uuid:"155AF717-3DD5-4ECA-A647-3BEBB7EEDF1A"}
 */
function nfx_isProgram() {
	return true;
}

/**
 * @properties={typeid:24,uuid:"AABCAFEF-9252-4D4A-A890-93652FE9EF2E"}
 */
function local_onLoad() {
	elements.entiList.visible = false;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"A0173FFF-B3D0-4EA2-9DE4-69F6B9578C59"}
 */
function openEntiList(event) {
	if (!elements.entiList.visible) {
		forms.ente_chooser.setData(controller.getName(), "ente_as400");
		elements.entiList.visible = true;
	} else {
		elements.entiList.visible = false;
	}
}

/**
 * @properties={typeid:24,uuid:"D7125A29-795C-4CB6-A659-441825879D74"}
 */
function nfx_preEdit() {
	elements.entiList.visible = false;
}

/**
 * @properties={typeid:24,uuid:"4F6FA708-A565-4D95-BDA5-386161F65DB8"}
 */
function nfx_postEdit() {
	elements.entiList.visible = false;
}

/**
 * @properties={typeid:24,uuid:"FF5EA8A4-1355-4682-BDCE-C4CDCF8778BB"}
 */
function nfx_postEditOnCancel() {
	elements.entiList.visible = false;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"2C386365-0CC7-4B11-ADC1-121F399FC883"}
 */
function showFullDescription(event) {
	/** @type {JSFoundSet} */
	var rel = k8_gestione_utenze_to_k8_enti;
	var item = null;
	if (rel) {
		
		var txt = rel.descrizione;
		
		var menu = plugins.window.createPopupMenu();
		
		if (txt) {
			//FS

			//dep			item = plugins.popupmenu.createMenuItem(txt,copyToClipboard);
			//dep			item.setMethodArguments([txt]);
			
			item = menu.addMenuItem(txt, copyToClipboard);
			item.methodArguments = [txt];
		} else {
			
			//dep			item = plugins.popupmenu.createMenuItem("Descrizione non presente...",null);
			item = menu.addMenuItem("Descrizione non presente...", null);
		}
		//dep		plugins.popupmenu.showPopupMenu(elements.descrizione,[item]);
		menu.show(elements.descrizione);
	}
}

/**
 * @properties={typeid:24,uuid:"8DBB6DE1-D90C-4C1C-91AC-55949C83A2C7"}
 */
function copyToClipboard(txt) {
	application.setClipboardContent(txt);
}
