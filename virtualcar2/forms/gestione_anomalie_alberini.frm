dataSource:"db:/ferrari/k8_dummy",
items:[
{
anchors:3,
formIndex:7,
imageMediaID:"4E205FA0-C666-48F9-A27F-935D355C24CD",
location:"570,30",
mediaOptions:10,
onActionMethodID:"643D9B1E-6A6E-48C3-BAB3-A377410FE127",
showClick:false,
size:"20,20",
transparent:true,
typeid:7,
uuid:"01A709F1-1D55-459E-BF76-C0798B79A08A"
},
{
height:490,
partType:5,
typeid:19,
uuid:"131CE10F-FC3F-49A2-8D50-C06B5AED3D94"
},
{
anchors:3,
formIndex:21,
imageMediaID:"5804BE64-BD66-4BC7-B44A-3314D54EA001",
location:"560,130",
mediaOptions:14,
name:"option_up",
onActionMethodID:"83938A31-AA85-4747-9766-4750B76A2870",
showClick:false,
size:"20,20",
transparent:true,
typeid:7,
uuid:"23205085-1540-41BC-910E-C3B2DD509121"
},
{
anchors:3,
beanClassName:"javax.swing.JScrollPane",
formIndex:22,
location:"380,70",
name:"option_scroll",
size:"170,190",
typeid:12,
uuid:"2FF556D9-4509-4CE2-B680-541924503A8E"
},
{
anchors:11,
fontType:"Verdana,1,10",
foreground:"#ffff00",
formIndex:-4,
horizontalAlignment:0,
location:"440,30",
mediaOptions:14,
name:"message",
size:"91,20",
styleClass:"form",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"30115269-DE12-4D7F-ADC9-5C03F9B7841E"
},
{
borderType:"EtchedBorder,0,null,null",
dataProviderID:"globals.vc2_currentProgetto",
displayType:2,
editable:false,
location:"10,30",
name:"vc2_currentProgetto",
onDataChangeMethodID:"72C1060F-D600-4425-9DCA-B71EA758C5FF",
size:"100,20",
text:"Vc2 Currentprogetto",
typeid:4,
uuid:"7BE63213-4BC2-48CB-8E35-F562B3DB8BD0",
valuelistID:"8DBD3071-35BE-4D55-B40F-F9B6FB9F8DD6"
},
{
anchors:14,
dataProviderID:"filter",
formIndex:3,
location:"391,461",
name:"filter1",
onActionMethodID:"96F1F906-F9EF-44DE-B4EF-497AD23E7170",
size:"178,18",
text:"Filter",
typeid:4,
uuid:"7C6D794A-1B98-4143-AE28-061EB0C5D81D"
},
{
borderType:"EtchedBorder,0,null,null",
dataProviderID:"globals.vc2_currentTipoScheda",
displayType:2,
editable:false,
location:"120,30",
name:"vc2_currentTipoScheda",
onDataChangeMethodID:"72C1060F-D600-4425-9DCA-B71EA758C5FF",
size:"140,20",
text:"Vc2 Currenttiposcheda",
typeid:4,
uuid:"808E113D-3A2D-463D-97D8-7C996D70C0AC",
valuelistID:"9BF9B4EC-3A95-4AFD-9572-B38015242AE5"
},
{
labelFor:"vc2_currentGruppoFunzionale",
location:"270,10",
mediaOptions:14,
size:"160,20",
styleClass:"form",
tabSeq:-1,
text:"Gruppo funzionale",
transparent:true,
typeid:7,
uuid:"85504C4A-4AE0-4AEA-B54C-59E36D3C396A"
},
{
beanClassName:"javax.swing.JList",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_17\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JList\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>list<\/string> \
  <\/void> \
  <void property=\"selectionMode\"> \
   <int>0<\/int> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:1,
location:"410,70",
name:"list",
size:"80,80",
typeid:12,
uuid:"925A7059-FD3E-4435-AA8A-3D6D482CC170"
},
{
beanClassName:"javax.swing.JTree",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_11\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JTree\"> \
  <object class=\"javax.swing.tree.DefaultTreeModel\"> \
   <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
    <void property=\"userObject\"> \
     <string>JTree<\/string> \
    <\/void> \
    <void method=\"add\"> \
     <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
      <void property=\"userObject\"> \
       <string>colors<\/string> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>blue<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>violet<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>red<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>yellow<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
     <\/object> \
    <\/void> \
    <void method=\"add\"> \
     <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
      <void property=\"userObject\"> \
       <string>sports<\/string> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>basketball<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>soccer<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>football<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>hockey<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
     <\/object> \
    <\/void> \
    <void method=\"add\"> \
     <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
      <void property=\"userObject\"> \
       <string>food<\/string> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>hot dogs<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>pizza<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>ravioli<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
      <void method=\"add\"> \
       <object class=\"javax.swing.tree.DefaultMutableTreeNode\"> \
        <void property=\"userObject\"> \
         <string>bananas<\/string> \
        <\/void> \
       <\/object> \
      <\/void> \
     <\/object> \
    <\/void> \
   <\/object> \
  <\/object> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"cellRenderer\"> \
   <void property=\"location\"> \
    <object class=\"java.awt.Point\"> \
     <int>-45<\/int> \
     <int>-16<\/int> \
    <\/object> \
   <\/void> \
  <\/void> \
  <void property=\"name\"> \
   <string>bean_356<\/string> \
  <\/void> \
  <void property=\"rootVisible\"> \
   <boolean>false<\/boolean> \
  <\/void> \
  <void property=\"showsRootHandles\"> \
   <boolean>true<\/boolean> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"20,70",
name:"tree",
size:"80,80",
typeid:12,
uuid:"95460AAB-FA0F-4DA8-BC1D-4906DE3D6197"
},
{
anchors:13,
beanClassName:"javax.swing.JScrollPane",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_20\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JScrollPane\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>380<\/int> \
    <int>420<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <object class=\"javax.swing.border.EtchedBorder\"> \
    <int>0<\/int> \
    <null/> \
    <null/> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>scroll<\/string> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"10,60",
name:"scroll",
size:"380,420",
typeid:12,
uuid:"98ACC6B7-C74F-43F3-9B94-669A11B8E130"
},
{
anchors:15,
beanClassName:"javax.swing.JScrollPane",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_17\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JScrollPane\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>200<\/int> \
    <int>400<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"border\"> \
   <object class=\"javax.swing.border.EtchedBorder\"> \
    <int>0<\/int> \
    <null/> \
    <null/> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>scrollList<\/string> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:-6,
location:"390,60",
name:"scrollList",
size:"200,400",
typeid:12,
uuid:"A3A8B945-0856-438C-8AB9-97F66207EA2A"
},
{
anchors:6,
formIndex:4,
imageMediaID:"618F9AA0-E8E0-4019-A9D8-472CC462832F",
location:"571,461",
mediaOptions:10,
name:"filter_icon",
size:"18,18",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"C53B59BB-FC50-47FF-9573-CF00F99CD2A0"
},
{
borderType:"EtchedBorder,0,null,null",
dataProviderID:"globals.vc2_currentGruppoFunzionale",
displayType:2,
editable:false,
location:"270,30",
name:"vc2_currentGruppoFunzionale",
onDataChangeMethodID:"72C1060F-D600-4425-9DCA-B71EA758C5FF",
size:"160,20",
text:"Vc2 Currentgruppofunzionale",
typeid:4,
uuid:"C923262B-CA65-4E0C-809F-DA4F70BE3905",
valuelistID:"50DC6D6A-1FD6-4A3B-8DC0-D12CE8C7CF99"
},
{
anchors:11,
fontType:"Verdana,1,10",
formIndex:-3,
horizontalAlignment:0,
location:"441,31",
mediaOptions:14,
name:"message_shadow",
size:"92,20",
styleClass:"form",
tabSeq:-1,
transparent:true,
typeid:7,
uuid:"CC2DC549-4615-4BED-BE56-2122F97364A2"
},
{
anchors:3,
formIndex:20,
imageMediaID:"6AD2E909-E7A4-491A-92E5-D7E2F229B865",
location:"560,160",
mediaOptions:14,
name:"option_down",
onActionMethodID:"6CB44525-400E-4796-B6A6-8E9A569F75F6",
showClick:false,
size:"20,20",
transparent:true,
typeid:7,
uuid:"D0F337E7-173B-4EDC-89AD-E27B529001A0"
},
{
anchors:3,
background:"#e2e2e2",
formIndex:16,
lineSize:1,
location:"360,60",
name:"option_panel",
roundedRadius:20,
shapeType:2,
size:"230,210",
typeid:21,
uuid:"DAB2706B-8DC0-4721-9D21-8A54D916C79A"
},
{
anchors:15,
formIndex:-9,
horizontalAlignment:0,
location:"10,70",
mediaOptions:14,
name:"warning",
size:"580,400",
styleClass:"form",
tabSeq:-1,
text:"Per visualizzare l'albero selezionare un progetto",
transparent:true,
typeid:7,
uuid:"E92D1591-F89C-4FFA-B722-2BE6AA7E2294"
},
{
labelFor:"vc2_currentProgetto",
location:"10,10",
mediaOptions:14,
size:"100,20",
styleClass:"form",
tabSeq:-1,
text:"Progetto",
transparent:true,
typeid:7,
uuid:"E97BC30C-F723-4727-AA29-C8A6D48E58C4"
},
{
labelFor:"vc2_currentTipoScheda",
location:"120,10",
mediaOptions:14,
size:"140,20",
styleClass:"form",
tabSeq:-1,
text:"Tipo",
transparent:true,
typeid:7,
uuid:"ED7C12F3-4A2B-45BA-A0CD-8F02492D84BD"
},
{
beanClassName:"javax.swing.JList",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_17\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JList\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>150<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>bean_634<\/string> \
  <\/void> \
  <void property=\"selectionMode\"> \
   <int>0<\/int> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:23,
location:"390,80",
name:"option_list",
size:"150,80",
typeid:12,
uuid:"F3107A60-E8CE-4CE0-B407-EC5CAD886B57"
},
{
anchors:3,
formIndex:24,
imageMediaID:"D7F5482D-BC8B-4FFF-A530-8838D0C0518D",
location:"540,30",
mediaOptions:14,
name:"download",
onActionMethodID:"BF54C1A6-B877-44C1-A0F6-C8AF6BF9CD8F",
showClick:false,
size:"20,20",
transparent:true,
typeid:7,
uuid:"F74EAE02-F7CC-4C74-85C5-F13FA143A4E9"
}
],
name:"gestione_anomalie_alberini",
navigatorID:"-1",
onHideMethodID:"7621013B-45FC-454C-88BE-EF2DC5A5C0E0",
onLoadMethodID:"BA35CA86-20B0-4A17-8B4D-49148FB61F5A",
onShowMethodID:"BCCAF39F-5866-42AB-9E3E-7563D9D8EA46",
paperPrintScale:100,
showInMenu:true,
size:"600,490",
styleName:"keeneight",
typeid:3,
uuid:"237A91E1-D9E2-440E-AE74-776E64A1B67D"