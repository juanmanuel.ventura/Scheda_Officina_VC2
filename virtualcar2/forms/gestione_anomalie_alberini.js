/**
 * @properties={typeid:35,uuid:"74D89986-34A8-459C-8421-82E4A290CD18",variableType:-4}
 */
var branches = new Array();

/**
 * @properties={typeid:35,uuid:"CAA7D37E-F1CA-4A87-9DC7-D9C914492587",variableType:-4}
 */
var branchesBKP = null;

/**
 * @properties={typeid:35,uuid:"99431F82-2646-48D7-ADA1-BC1BC8927A20",variableType:-4}
 */
var comboboxBKP = { progetto: 0, tipo: 0, gruppo: 0 };

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"23BB3A4C-4547-4D66-9CDF-043DB719A6A7"}
 */
var filter = null;

/**
 * @properties={typeid:35,uuid:"CD17161C-9B8D-480C-815F-683C85A5DA1B",variableType:-4}
 */
var listIds = new Array();

/**
 * @properties={typeid:35,uuid:"880F89C4-88BB-48EB-8EE0-35E4043A00FC",variableType:-4}
 */
var listValues = new Array();

/**@type {Array}
 * @properties={typeid:35,uuid:"66106854-CE09-4509-B4CE-9BB5DA22A834",variableType:-4}
 */
var nodeMap = new Array();

/**
 * @properties={typeid:35,uuid:"15511550-9E84-4740-89A2-E8D28A49FFD3",variableType:-4}
 */
var SA = {
	mod: "'Ente: ' || a.ente_segnalazione_anomalia || ' - Segnalazione numero: ' || a.numero_segnalazione || ' , segnalato da: ' || a.segnalato_da || ' (leader: ' || a.leader || ')', a.k8_anomalie_id",
	order: " order by a.ente_segnalazione_anomalia asc, a.numero_segnalazione desc"
};

/**
 * @properties={typeid:35,uuid:"3A8EFBAF-49AC-430E-A3A2-8845F9454245",variableType:-4}
 */
var SKA = {
	mod: "'Ente: ' || a.ente || ' - Scheda numero: ' || a.numero_scheda || ' , segnalato da: ' || a.segnalato_da || ' (leader: ' || a.leader || ')', a.k8_anomalie_id",
	order: " order by a.ente asc, a.numero_scheda desc"
};

/**
 * @properties={typeid:35,uuid:"DCF4D9EF-1E65-49E7-BAAC-D145BB3AA8A7",variableType:-4}
 */
var SKM = {
	mod: "'Ente: ' || m.ente_proposta_modifica || ' - Proposta numero: ' || m.nr_proposta_modifica || ', leader: ' || m.leader || ', causale SKM: ' || m.causale_modifica || ' (' || (select trim(MCDCAU) from XCH_SRCAU00F_IN where MCCCAU = m.causale_modifica) || ')', m.k8_schede_modifica_id",
	order: " order by m.ente_proposta_modifica asc, m.nr_proposta_modifica desc"
};

/**
 *
 * @properties={typeid:24,uuid:"7A8E2B1E-0398-4C2F-9EE1-7670B3BB22FE"}
 */
function addAnomaliaAssociata(nodo, chiuse, que, args) {

	/** @type {String} */
	var query = que.replace("!", "");
	var index = args.indexOf("EX_SEGNALAZIONE");
	//FS da chiedere

	//dep	var imported = new JavaImporter(Packages.javax.swing.tree);

	//	JStaffa sostituisco ai JavaImporter la loro dichiarazione
	//	var imported = new JavaImporter(Packages.javax.swing.tree);

	/** @type {javax.swing.tree.DefaultTreeModel} */
	var model = elements.tree.getModel();
	/** @type {javax.swing.tree.DefaultMutableTreeNode} */
	var node = null;
	/** @type {JSDataSet} */
	var nSegnChiuseSenzaScheda = null;
	/** @type {JSDataSet} */
	var nSegnChiuseConScheda = null;

	if (globals.vc2_currentTipoScheda == "SEGNALAZIONE" || !globals.vc2_currentTipoScheda) {
		args[index] = "SEGNALAZIONE";
		//FS
		//dep		nSegnChiuseSenzaScheda = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1);
		nSegnChiuseSenzaScheda = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource())
			, query, args, 1);

		//		JStaffa
		//		with (imported) {
		//			node = new DefaultMutableTreeNode("senza Scheda anomalia [" + nSegnChiuseSenzaScheda.getValue(1, 1) + "]");
		node = new Packages.javax.swing.tree.DefaultMutableTreeNode("senza Scheda anomalia [" + nSegnChiuseSenzaScheda.getValue(1, 1) + "]");
		//		}
		var mod = SA.mod;
		var order = SA.order;
		nodeMapAdd(node, query.replace("count(*)", mod) + order, args);

		addNewBranch(node, query, args, 0, "SEGNALAZIONE");

		model.insertNodeInto(node, nodo, nodo.getChildCount());
		model.reload();

		args[index] = "ANOMALIA";
		//dep		nSegnChiuseConScheda = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1);
		nSegnChiuseConScheda = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);

		//		JStaffa
		//		with (imported) {
		//			node = new DefaultMutableTreeNode("con Scheda anomalia [" + nSegnChiuseConScheda.getValue(1, 1) + "]");
		node = new Packages.javax.swing.tree.DefaultMutableTreeNode("con Scheda anomalia [" + nSegnChiuseConScheda.getValue(1, 1) + "]");
		//		}
		mod = SKA.mod;
		order = SKA.order;
		nodeMapAdd(node, query.replace("count(*)", mod) + order, args);

		model.insertNodeInto(node, nodo, nodo.getChildCount());
	} else {
		args[index] = "ANOMALIA";
		//dep		nSegnChiuseConScheda = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1);

		nSegnChiuseConScheda = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);
		//		JStaffa
		//		with (imported) {
		//			node = new DefaultMutableTreeNode("Schede anomalia [" + nSegnChiuseConScheda.getValue(1, 1) + "]");
		node = new Packages.javax.swing.tree.DefaultMutableTreeNode("Schede anomalia [" + nSegnChiuseConScheda.getValue(1, 1) + "]");
		//		}
		mod = SKA.mod;
		order = SKA.order;
		nodeMapAdd(node, query.replace("count(*)", mod) + order, args);

		model.insertNodeInto(node, nodo, nodo.getChildCount());
	}

	query += " and a.stato_anomalia = ?";
	model.insertNodeInto(nSegnChiuseConSchedaApertaNode(query, args), node, node.getChildCount());
	model.insertNodeInto(nSegnChiuseConSchedaChiusaNode(query, args), node, node.getChildCount());
	model.reload();
}

/**
 *
 * @properties={typeid:24,uuid:"04030DE2-11F5-41A8-80BE-901EF9C4075A"}
 */
function addModificaAssociata(nodo, chiuse, query, args) {
	/** @type {String} */
	var querySkModifica = query.replace("k8_anomalie a", "k8_anomalie a inner join k8_schede_modifica m on a.numero_scheda = m.nr_scheda_anomalia and a.ente = m.ente_inserimento_anomalia");
	querySkModifica += " and a.numero_scheda is not null";
	//FS
	//dep	var nConSchedaModifica = databaseManager.getDataSetByQuery(controller.getServerName(), querySkModifica, args, 1);
	var nConSchedaModifica = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), querySkModifica, args, 1);

	var nSenzaSchedaModifica = chiuse - nConSchedaModifica.getValue(1, 1);

	//	JStaffa sostituisco ai JavaImporter la loro dichiarazione
	//	var imported = new JavaImporter(Packages.javax.swing.tree);
	/** @type {javax.swing.tree.DefaultTreeModel} */
	var model = elements.tree.getModel();
	/** @type {javax.swing.tree.DefaultMutableTreeNode} */
	var node = null;

	//	with (imported) {
	//		node = new DefaultMutableTreeNode("senza Scheda MODIFICA [" + nSenzaSchedaModifica + "]");
	node = new Packages.javax.swing.tree.DefaultMutableTreeNode("senza Scheda MODIFICA [" + nSenzaSchedaModifica + "]");
	//	}

	model.insertNodeInto(node, nodo, nodo.getChildCount());
	model.reload();

	//	JStaffa
	//	with (imported) {
	//		node = new DefaultMutableTreeNode("con Scheda MODIFICA [" + nConSchedaModifica.getValue(1, 1) + "]");
	node = new Packages.javax.swing.tree.DefaultMutableTreeNode("con Scheda MODIFICA [" + nConSchedaModifica.getValue(1, 1) + "]");
	//	}
	var mod = SKM.mod;
	var order = SKM.order;
	nodeMapAdd(node, querySkModifica.replace("count(*)", mod) + order, args);

	model.insertNodeInto(node, nodo, nodo.getChildCount());

	model.insertNodeInto(nSchedeModificheAperteNode(querySkModifica, args), node, node.getChildCount());
	model.insertNodeInto(nSchedeModificheApprovateNode(querySkModifica, args), node, node.getChildCount());
	model.insertNodeInto(nSchedeModificheAnnullateNode(querySkModifica, args), node, node.getChildCount());
	model.reload();
}

/**
 * @properties={typeid:24,uuid:"E8899AF9-F88D-4D90-BAFB-83A68074D1D5"}
 */
function addNewBranch(nodo, query, args, index, formtype) {
	if (branches[index] && branches[index].enabled) {
		var branch = branches[index].branch;
		var type = branches[index].type;

		var mod = null;
		var order = null;

		if (formtype == "SEGNALAZIONE") {
			mod = SA.mod;
			order = SA.order;
		}
		if (formtype == "ANOMALIA") {
			mod = SKA.mod;
			order = SKA.order;
		}
		if (formtype == "MODIFICA") {
			mod = SKM.mod;
			order = SKM.order;
		}

		//SAuc
		///** @type {JSDataSet} */
		//var species = databaseManager.getDataSetByQuery(controller.getServerName(),"select distinct trim(" + branch + ") from k8_anomalie",null,-1);
		/** @type {JSDataSet} */
		var species = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), "select distinct trim(" + branch + ") from k8_anomalie", null, -1);
		//		JStaffa aggiungendo una funzione vuota come parametro della sort, non esplode..
		var functInesistent = new Function();
		species = species.getColumnAsArray(1);
		if (type == "number") {
			species.sort(sortNumbers);
		} else {
			species.sort(functInesistent(), false);
			//			species.sort(0,false);
		}
		species.reverse();

		query += " and a." + branch + " = ?";

		for (var i = 0; i < species.length; i++) {

			var count = null;

			if (species[i]) {
				args.push(species[i]);

				//SAuc

				//count = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,1);

				count = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);

				var identifier = null;
				//var imported = null;
				/** @type {javax.swing.tree.DefaultTreeModel} */
				var model = null;
				/** @type {javax.swing.tree.DefaultMutableTreeNode} */
				var node = null;

				if (count.getValue(1, 1) != 0) {
					identifier = branches[index].labelWith;
					var vlist = branches[index].vlist;
					//					JStaffa sostituisco ai JavaImporter la loro dichiarazione
					//					imported = new JavaImporter(Packages.javax.swing.tree);
					//FS
					//dep				var label = (vlist) ? databaseManager.getDataSetByQuery(controller.getServerName(), vlist, [species[i]], 1).getValue(1, 1) : species[i].toString();
					var label = (vlist) ? databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), vlist, [species[i]], 1).getValue(1, 1) : species[i].toString();

					//					JStaffa
					//					with (imported) {
					//						node = new DefaultMutableTreeNode(identifier + label + " [" + count.getValue(1, 1) + "]");
					node = new Packages.javax.swing.tree.DefaultMutableTreeNode(identifier + label + " [" + count.getValue(1, 1) + "]");
					//					}
					nodeMapAdd(node, query.replace("count(*)", mod) + order, args);

					addNewBranch(node, query, args, index + 1, formtype);
					model = elements.tree.getModel();
					model.insertNodeInto(node, nodo, nodo.getChildCount());
					model.reload();
				}
				args.pop();
			} else {
				//FS
				/** @type {String} */			
				var queryNoBranch = query.replace(" and a." + branch + " = ?", " and a." + branch + " is null");
				//FS
				//dep			count = databaseManager.getDataSetByQuery(controller.getServerName(), queryNoBranch, args, 1);
				count = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), queryNoBranch, args, 1);

				if (count.getValue(1, 1) != 0) {
					identifier = branches[index].labelNull;
					//					JStaffa sostituisco ai JavaImporter la loro dichiarazione
					//					imported = new JavaImporter(Packages.javax.swing.tree);

					//					with (imported) {
					//						node = new DefaultMutableTreeNode(identifier + "[" + count.getValue(1, 1) + "]");
					node = new Packages.javax.swing.tree.DefaultMutableTreeNode(identifier + "[" + count.getValue(1, 1) + "]");
					//					}
					nodeMapAdd(node, queryNoBranch.replace("count(*)", mod) + order, args);

					addNewBranch(node, queryNoBranch, args, index + 1, formtype);
					model = elements.tree.getModel();
					model.insertNodeInto(node, nodo, nodo.getChildCount());
					model.reload();
				}
			}
		}
	}
}

/**@param {JSEvent} event
 * @properties={typeid:24,uuid:"643D9B1E-6A6E-48C3-BAB3-A377410FE127"}
 */
function customize(event) {
	if (elements.option_panel.visible) {
		hideOptionPanel();
		if (forms.nfx_json_serializer.stringify(branches, null, null) != branchesBKP) {
			forms.nfx_interfaccia_gestione_salvataggi.save(branches, controller.getName());
			onUpdateCombobox(null);
		} else {
			onUpdateCombobox(true);
		}
	} else {
		branchesBKP = forms.nfx_json_serializer.stringify(branches, null, null);
		elements.option_list.setListData(getLabels());
		showOptionPanel();
	}
}

/**@param {java.awt.event.MouseEvent} e
 * @properties={typeid:24,uuid:"A9A85278-2685-41FE-B64C-291807E3AE4C"}
 */
function dcAction(e) {
	if (e.getClickCount() == 2) {
		moveLast();
	}

}

/**
 * @properties={typeid:24,uuid:"DCB61B94-4543-4EEA-8D69-E480482ED711"}
 */
function disableBox() {
	globals.vc2_currentTipoScheda = null;
	elements.vc2_currentTipoScheda.enabled = false;
	globals.vc2_currentGruppoFunzionale = null;
	elements.vc2_currentGruppoFunzionale.enabled = false;
}

/**
 * @properties={typeid:24,uuid:"F98F1AAE-2FB6-421A-9DF8-35005E006044"}
 */
function drawTree(query, args) {
	nodeMap = new Array();
	elements.list.setListData(new Array());
	filter = null;

	var nModNode = nMod(query, args);

	//Questa parte di query trasforma k8_anomalie in k8_segnalazioni_full
	query += " and a.tipo_scheda != ? and a.ente_segnalazione_anomalia is not null and a.numero_segnalazione != ?";
	args.push("EX_SEGNALAZIONE", 0);
	//FS

	//dep	var nSegn = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1);
	var nSegn = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);

	/** @type {javax.swing.tree.DefaultTreeModel} */
	var model = elements.tree.getModel();
	/** @type {javax.swing.tree.DefaultMutableTreeNode} */
	var root = model.getRoot();
	root.removeAllChildren();

	if (globals.vc2_currentTipoScheda == "SEGNALAZIONE" || !globals.vc2_currentTipoScheda) {
		//		JStaffa sostituisco ai JavaImporter la loro dichiarazione
		//		var imported = new JavaImporter(Packages.javax.swing.tree);
		//		with (imported) {
		//			var nSegnNode = new DefaultMutableTreeNode("Segnalazioni [" + nSegn.getValue(1, 1) + "]");
		var nSegnNode = new Packages.javax.swing.tree.DefaultMutableTreeNode("Segnalazioni [" + nSegn.getValue(1, 1) + "]");
		//		}

		model.insertNodeInto(nSegnNode, root, root.getChildCount());

		query += " and a.stato_segnalazione = ?";
		model.insertNodeInto(nSegnAperteNode(query, args), nSegnNode, nSegnNode.getChildCount());
		model.insertNodeInto(nSegnChiuseNode(query, args), nSegnNode, nSegnNode.getChildCount());
		if (!globals.vc2_currentTipoScheda) {
			model.insertNodeInto(nModNode, root, root.getChildCount());
		}
	} else if (globals.vc2_currentTipoScheda == "ANOMALIA") {
		addAnomaliaAssociata(root, 0, query, args);
	} else if (globals.vc2_currentTipoScheda == "MODIFICA") {
		model.insertNodeInto(nModNode, root, root.getChildCount());
	}

	model.reload();
	showTree();
}

/**
 * @properties={typeid:24,uuid:"A86784FC-1727-4790-9404-8C63C0FCE1F8"}
 */
function nMod(query, args) {
	query = query.replace("k8_anomalie a", "k8_schede_modifica m").replace(/a\./g, "m.").replace("codice_modello", "modello");
	//FS
	//dep	var num = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1).getValue(1, 1);

	var num = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1).getValue(1, 1);
	//	JStaffa sostituisco ai JavaImporter la loro dichiarazione
	//	var imported = new JavaImporter(Packages.javax.swing.tree);
	//	with (imported) {
	//		var node = new DefaultMutableTreeNode("Modifiche [" + num + "]");
	var node = new Packages.javax.swing.tree.DefaultMutableTreeNode("Modifiche [" + num + "]");
	//	}
	var mod = SKM.mod;
	var order = SKM.order;
	nodeMapAdd(node, query.replace("count(*)", mod) + order, args);

	var customBranches = [{ field: "stato_scheda", list: null }, { field: "causale_modifica", list: "select 'causale SKM: ' || N_ || ' - ' || trim(D_) from (select MCCCAU N_,MCDCAU D_ from XCH_SRCAU00F_IN where MCCCAU = ? group by MCCCAU,MCDCAU)" }];
	addFilter(node, query, args, customBranches, 0);
	return node;
}

/**
 * @properties={typeid:24,uuid:"872F62B8-B93A-471A-B596-86F1D30BAE78"}
 */
function addFilter(root, query, args, customBranches, index) {
	if (customBranches[index].field) {
		//		JStaffa rimosso javaimporter
		//		var imported = new JavaImporter(Packages.javax.swing.tree);

		var queryGroup = query.replace("count(*)", "m." + customBranches[index].field + ",count(*)") + " group by m." + customBranches[index].field;
		var querySingle = query + " and m." + customBranches[index].field + " = ?";
		//FS
		//dep	var ds = databaseManager.getDataSetByQuery(controller.getServerName(), queryGroup, args, -1);
		var ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), queryGroup, args, -1);

		var max = ds.getMaxRowIndex();
		for (var i = 1; i <= max; i++) {
			if (ds.getValue(i, 2) != 0) {

				//dep	var label = (customBranches[index].list) ? databaseManager.getDataSetByQuery(controller.getServerName(), customBranches[index].list, [ds.getValue(i, 1)], 1).getValue(1, 1) : ds.getValue(i, 1);
				var label = (customBranches[index].list) ? databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), customBranches[index].list, [ds.getValue(i, 1)], 1).getValue(1, 1) : ds.getValue(i, 1);
				//				JStaffa
				//				with (imported) {
				//				var node = new DefaultMutableTreeNode(label + " [" + ds.getValue(i, 2) + "]");
				var node = new Packages.javax.swing.tree.DefaultMutableTreeNode(label + " [" + ds.getValue(i, 2) + "]");
				//				}
				var mod = SKM.mod;
				var order = SKM.order;
				nodeMapAdd(node, querySingle.replace("count(*)", mod) + order, args.concat([ds.getValue(i, 1)]));

				if (customBranches[index + 1]) {
					addFilter(node, querySingle, args.concat([ds.getValue(i, 1)]), customBranches, index + 1)
				}
				/** @type {javax.swing.tree.DefaultTreeModel} */				
				var treeAsDefaultTree = elements.tree.getModel();
				//elements.tree.getModel().insertNodeInto(node, root, root.getChildCount());
				treeAsDefaultTree.insertNodeInto(node, root, root.getChildCount());

			}
		}
	}
}

/**
 * @properties={typeid:24,uuid:"7DF9117E-5AF4-4779-BF93-075C81E17E5D"}
 */
function enableBox() {
	elements.vc2_currentTipoScheda.enabled = true;
	elements.vc2_currentGruppoFunzionale.enabled = true;
}

/**
 * @properties={typeid:24,uuid:"9C17FF44-4599-48C0-834A-F1409EBB8E13"}
 * @AllowToRunInFind
 */
function filterList() {
	if (filter == "") {
		filter = null;
		application.updateUI();
	}
	var list = listValues;
	if (filter) {
		var upperFilter = filter.toUpperCase();
		var filtered = new Array();
		for (var i = 0; i < list.length; i++) {
			var upperList = list[i].toUpperCase();
			if (upperList.search(upperFilter) != -1) {
				filtered.push(list[i]);
			}
		}
		return filtered;
	} else {
		return list;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"96F1F906-F9EF-44DE-B4EF-497AD23E7170"}
 */
function filterWrap() {
	elements.list.setListData(filterList());
}

/**
 * @properties={typeid:24,uuid:"83A35ABA-224C-41EC-823B-F1C5CE02A727"}
 */
function getFirstUnselected() {
	for (var i = 0; i < branches.length; i++) {
		if (!branches[i].enabled) return i;
	}
	return null;
}

/**
 * @properties={typeid:24,uuid:"9405ADBA-3B32-4E39-88DA-AAB44B29842E"}
 */
function getLabel(list, value, index, isSelected, cellHasFocus) {

	var check = (branches[index].enabled) ? new Packages.javax.swing.JCheckBox(value, true) : new Packages.javax.swing.JCheckBox(value, false);

	if (isSelected) {
		check.setForeground(java.awt.SystemColor.textHighlightText);
		check.setBackground(java.awt.SystemColor.textHighlight);
		check.setOpaque(true);
	} else {
		check.setForeground(java.awt.SystemColor.textText);
		check.setBackground(java.awt.SystemColor.text);
		check.setOpaque(false);
	}

	return check;
}

/**
 * @properties={typeid:24,uuid:"709548E5-1E25-46C0-83CD-A9B2E5D3132A"}
 */
function getLabels() {
	var labels = new Array();
	for (var i = 0; i < branches.length; i++) {
		labels.push(branches[i].labelHuman);
	}
	return labels;
}

/**
 * @AllowToRunInFind
 *
 *
 * @param{java.awt.event.MouseEvent} e
 *
 * @properties={typeid:24,uuid:"3218AD13-B606-4D02-B4B1-638E697C33D0"}
 */
function gotoForm(e) {

	if (e.getClickCount() == 2) {
		var label = elements.list.getSelectedValue();
		if (label) {
			var index = listValues.indexOf(label);
			var id = listIds[index];
			if (label.search("Segnalazione") != -1) {
				//globals.alternate = true;
				//globals.toGoTo = "segnalazioni_anomalie_record";
				globals.nfx_goToProgramAndRecord("segnalazioni_anomalie_record", id, "k8_anomalie_id");
				//globals.alternate = false;
				return;
			}
			if (label.search("Scheda") != -1) {
				//globals.alternate = true;
				//globals.toGoTo = "schede_anomalie_record";
				globals.nfx_goToProgramAndRecord("schede_anomalie_record", id, "k8_anomalie_id");
				//globals.alternate = false;
				return;
			}
			if (label.search("Proposta") != -1) {
				//globals.alternate = true;
				//globals.toGoTo = "schede_modifica_record";
				globals.nfx_goToProgramAndRecord("schede_modifica_record", id, "k8_schede_modifica_id");
				//globals.alternate = false;
				return;
			}
		}
	}
}

/**@param {java.awt.event.MouseEvent} e
 * @properties={typeid:24,uuid:"1A54FBE0-472F-4081-9E49-327C3AE41979"}
 */
function gotoFormWrap(e) {
	gotoForm(e);
}

/**
 * @properties={typeid:24,uuid:"9FAFD3F9-1F25-485A-B054-3084F8C3089B"}
 * @AllowToRunInFind
 */
function hideOptionPanel() {
	var i = elements.allnames.length;

	//FS
	//	for (var out = i; i--;) {
	//		if (elements.allnames[i].search("option_") != -1) {
	//			elements[elements.allnames[i]].visible = false;
	//			elements[elements.allnames[i]].enabled = false;
	//		} else {
	//			elements[elements.allnames[i]].enabled = true;
	//		}
	//
	//	}

	//SAuc

	for (var out = i; out--;) {
		if (elements.allnames[out].search("option_") != -1) {
			elements[elements.allnames[out]].visible = false;
			elements[elements.allnames[out]].enabled = false;
		} else {
			elements[elements.allnames[out]].enabled = true;
		}

	}

}

/**
 *
 * @properties={typeid:24,uuid:"9BF8D33A-A00D-42C7-AD6E-BBF65E2FA89D"}
 */
function hideTree() {
	elements.scroll.visible = false;
	elements.scrollList.visible = false;
	elements.filter1.visible = false;
	elements.filter_icon.visible = false;
	elements.download.enabled = false;
	elements.warning.visible = true;
}

/**
 *@param {JSEvent} event
 * @properties={typeid:24,uuid:"6CB44525-400E-4796-B6A6-8E9A569F75F6"}
 */
function moveDown(event) {
	var index = elements.option_list.getSelectedIndex();
	var value = elements.option_list.getSelectedValue();
	if (value && index < branches.length - 1) {
		var app = branches[index + 1];
		branches[index + 1] = branches[index];
		branches[index] = app;
		elements.option_list.setListData(getLabels());
		elements.option_list.setSelectedIndex(index + 1);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"53ADE70C-95B3-4FEF-AA01-4D559CD3DF7B"}
 */
function moveLast() {
	var index = elements.option_list.getSelectedIndex();
	var value = elements.option_list.getSelectedValue();
	if (value) {
		var app = branches[index];
		if (app.enabled) {
			app.enabled = false;
			branches.splice(index, 1);
			branches.push(app);
		} else {
			app.enabled = true;
			branches.splice(index, 1);
			if (getFirstUnselected() != null) {
				branches.splice(getFirstUnselected(), 0, app);
			} else {
				branches.push(app);
			}

		}
		elements.option_list.setListData(getLabels());
	}
}

/**
 *@param {JSEvent} event
 * @properties={typeid:24,uuid:"83938A31-AA85-4747-9766-4750B76A2870"}
 */
function moveUp(event) {
	var index = elements.option_list.getSelectedIndex();
	var value = elements.option_list.getSelectedValue();
	if (value && index > 0) {
		var app = branches[index - 1];
		branches[index - 1] = branches[index];
		branches[index] = app;
		elements.option_list.setListData(getLabels());
		elements.option_list.setSelectedIndex(index - 1);
	}
}

/**
 * @properties={typeid:24,uuid:"C512A983-415D-4752-B670-BD6B78B7CDB7"}
 */
function nfx_getTitle() {
	return "Gestione anomalie - Alberini";
}

/**
 *
 * @properties={typeid:24,uuid:"96B89CF5-0017-4D3B-834E-2C69C1F01212"}
 */
function nfx_isProgram() {
	return true;
}

/**
 * @properties={typeid:24,uuid:"318AF7AA-6CEE-4ACE-9B36-E04B0D3A0C6C"}
 */
function nfx_onHide() {
	comboboxBKP.progetto = globals.vc2_currentProgetto;
	comboboxBKP.tipo = globals.vc2_currentTipoScheda;
	globals.vc2_currentTipoScheda = (globals.vc2_currentTipoScheda != "MODIFICA") ? globals.vc2_currentTipoScheda : null;
	comboboxBKP.gruppo = globals.vc2_currentGruppoFunzionale;

	hideOptionPanel();
}

/**
 *
 * @properties={typeid:24,uuid:"7D869484-8245-40C0-9748-6897ED97A58F"}
 */
function nfx_onShow() {
	if (comboboxBKP.progetto != globals.vc2_currentProgetto || comboboxBKP.tipo != globals.vc2_currentTipoScheda || comboboxBKP.gruppo != globals.vc2_currentGruppoFunzionale) {
		onUpdateCombobox(null);
	} else {
		onUpdateCombobox(true);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"55074225-A86B-4B19-BC83-FB4C5215F88F"}
 */
function nodeMapAdd(s, q, a) {

	nodeMap.push({ hash: s.hashCode(), query: q, args: forms.nfx_json_serializer.stringify(a, null, null) });
}

/**
 *
 * @properties={typeid:24,uuid:"47447BF1-A4E5-4C20-8CF7-B41AAA32CEF6"}
 */
function nSchedeModificheAperteNode(query, args) {

	var queryAperte = query + " and m.data_approv_sk_prop_mod is null and m.data_annul_sk_prop_mod is null";
	//FS
	//dep	var nSchedeModificheAperte = databaseManager.getDataSetByQuery(controller.getServerName(), queryAperte, args, 1);
	var nSchedeModificheAperte = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), queryAperte, args, 1);

	//	JStaffa sostituisco ai JavaImporter la loro dichiarazione
	//	var imported = new JavaImporter(Packages.javax.swing.tree);

	//	with (imported) {
	//		var node = new DefaultMutableTreeNode("Aperte [" + nSchedeModificheAperte.getValue(1, 1) + "]");
	var node = new Packages.javax.swing.tree.DefaultMutableTreeNode("Aperte [" + nSchedeModificheAperte.getValue(1, 1) + "]");
	//	}

	var mod = SKM.mod;
	var order = SKM.order;
	nodeMapAdd(node, queryAperte.replace("count(*)", mod) + order, args);

	addNewBranch(node, queryAperte, args, 0, "MODIFICA");

	return node;
}

/**
 * @properties={typeid:24,uuid:"F1BF9531-5B8E-4770-AF13-0C73EB30C3A9"}
 */
function nSchedeModificheAnnullateNode(query, args) {

	var queryAnnullate = query + " and m.data_annul_sk_prop_mod is not null";
	//FS

	//dep	var nSchedeModificheAnnullate = databaseManager.getDataSetByQuery(controller.getServerName(), queryAnnullate, args, 1);
	var nSchedeModificheAnnullate = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), queryAnnullate, args, 1);

	//	JStaffa sostituisco ai JavaImporter la loro dichiarazione
	//	var imported = new JavaImporter(Packages.javax.swing.tree);

	//	with (imported) {
	//		var node = new DefaultMutableTreeNode("Annullate [" + nSchedeModificheAnnullate.getValue(1, 1) + "]");
	var node = new Packages.javax.swing.tree.DefaultMutableTreeNode("Annullate [" + nSchedeModificheAnnullate.getValue(1, 1) + "]");
	//	}
	var mod = SKM.mod;
	var order = SKM.order;
	nodeMapAdd(node, queryAnnullate.replace("count(*)", mod) + order, args);

	return node;
}

/**
 *
 * @properties={typeid:24,uuid:"5FC4C9C7-E15E-4162-90B5-A94D7A975A46"}
 */
function nSchedeModificheApprovateNode(query, args) {

	var queryApprovate = query + " and m.data_approv_sk_prop_mod is not null";
	//FS

	//dep	var nSchedeModificheApprovate = databaseManager.getDataSetByQuery(controller.getServerName(), queryApprovate, args, 1);
	var nSchedeModificheApprovate = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), queryApprovate, args, 1);

	//	JStaffa sostituisco ai JavaImporter la loro dichiarazione
	//	var imported = new JavaImporter(Packages.javax.swing.tree);

	//	with (imported) {
	//		var node = new DefaultMutableTreeNode("Approvate [" + nSchedeModificheApprovate.getValue(1, 1) + "]");
	var node = new Packages.javax.swing.tree.DefaultMutableTreeNode("Approvate [" + nSchedeModificheApprovate.getValue(1, 1) + "]");
	//	}
	var mod = SKM.mod;
	var order = SKM.order;
	nodeMapAdd(node, queryApprovate.replace("count(*)", mod) + order, args);

	return node;
}

/**
 *
 * @properties={typeid:24,uuid:"DDED4039-9DAD-4586-81AA-27C874A8FA68"}
 */
function nSegnAperteNode(query, args) {

	query = query.replace(" and a.stato_segnalazione = ?", " and a.stato_segnalazione != ?")
	args.push("CHIUSA");

	//FS
	//dep	var nSegnAperte = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1);
	var nSegnAperte = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);

	//	JStaffa sostituisco ai JavaImporter la loro dichiarazione
	//	var imported = new JavaImporter(Packages.javax.swing.tree);

	//	with (imported) {
	//		var node = new DefaultMutableTreeNode("Aperte [" + nSegnAperte.getValue(1, 1) + "]");
	var node = new Packages.javax.swing.tree.DefaultMutableTreeNode("Aperte [" + nSegnAperte.getValue(1, 1) + "]");
	//	}
	var mod = SA.mod;
	var order = SA.order;
	nodeMapAdd(node, query.replace("count(*)", mod) + order, args);

	addNewBranch(node, query, args, 0, "SEGNALAZIONE");

	args.pop();

	return node;
}

/**
 *
 * @properties={typeid:24,uuid:"5F8FB309-5A66-4904-A296-F1B616743922"}
 */
function nSegnChiuseConSchedaApertaNode(query, args) {

	args.push("APERTA");
	//FS

	//dep	var nSegnAperte = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1);
	var nSegnAperte = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);

	//	JStaffa sostituisco ai JavaImporter la loro dichiarazione
	//	var imported = new JavaImporter(Packages.javax.swing.tree);

	//	with (imported) {
	//		var node = new DefaultMutableTreeNode("Aperte [" + nSegnAperte.getValue(1, 1) + "]");
	var node = new Packages.javax.swing.tree.DefaultMutableTreeNode("Aperte [" + nSegnAperte.getValue(1, 1) + "]");
	//	}
	var mod = SKA.mod;
	var order = SKA.order;
	nodeMapAdd(node, query.replace("count(*)", mod) + order, args);

	addNewBranch(node, query, args, 0, "ANOMALIA");

	args.pop();

	return node;
}

/**
 *
 * @properties={typeid:24,uuid:"8D075055-5D89-41A7-AF08-8B07A210FE7C"}
 */
function nSegnChiuseConSchedaChiusaNode(query, args) {

	args.push("CHIUSA");
	//FS
	//dep	var nSegnChiuse = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1);
	var nSegnChiuse = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);

	//	JStaffa sostituisco ai JavaImporter la loro dichiarazione
	//	var imported = new JavaImporter(Packages.javax.swing.tree);

	//	with (imported) {
	//		var node = new DefaultMutableTreeNode("Chiuse [" + nSegnChiuse.getValue(1, 1) + "]");
	var node = new Packages.javax.swing.tree.DefaultMutableTreeNode("Chiuse [" + nSegnChiuse.getValue(1, 1) + "]");
	//	}
	var mod = SKA.mod;
	var order = SKA.order;
	nodeMapAdd(node, query.replace("count(*)", mod) + order, args);

	addModificaAssociata(node, nSegnChiuse.getValue(1, 1), query, args);

	args.pop();

	return node;
}

/**
 *
 * @properties={typeid:24,uuid:"C1A1ED9B-F502-4089-9172-256812FC0A64"}
 */
function nSegnChiuseNode(query, args) {

	args.push("CHIUSA");
	//FS
	//dep	var nSegnChiuse = databaseManager.getDataSetByQuery(controller.getServerName(), query, args, 1);
	var nSegnChiuse = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, args, 1);

	//	JStaffa sostituisco ai JavaImporter la loro dichiarazione
	//	var imported = new JavaImporter(Packages.javax.swing.tree);

	//	with (imported) {
	//		var node = new DefaultMutableTreeNode("Chiuse [" + nSegnChiuse.getValue(1, 1) + "]");
	var node = new Packages.javax.swing.tree.DefaultMutableTreeNode("Chiuse [" + nSegnChiuse.getValue(1, 1) + "]");
	//	}

	addAnomaliaAssociata(node, nSegnChiuse.getValue(1, 1), query, args);

	args.pop();

	return node;
}

/**
 *
 * @properties={typeid:24,uuid:"1076086F-E9E3-4274-A592-48FF0918511C"}
 */
function onClick() {
	var sp = elements.tree.getLastSelectedPathComponent();
	if (sp) {
		var hash = sp.hashCode();
		var i = nodeMap.length;
		for (var out = i; i--;) {
			if (hash == nodeMap[i].hash) {
				writeMessage("generazione lista in corso...");
				//FS
				/** @type {String} */
				var q = nodeMap[i].query;
				//FS
				/** @type {Array} */
				var a = forms.nfx_json_serializer.parse(nodeMap[i].args);
				//dep				var ds = databaseManager.getDataSetByQuery(controller.getServerName(), q, a, -1);
				var ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), q, a, -1);

				listValues = ds.getColumnAsArray(1);
				listIds = ds.getColumnAsArray(2);
				elements.list.setListData(filterList());
				writeMessage(null);
			}
		}
		//  JStaffa aggiunto output per variabile non utilizzata
		//application.output('virtualcar2 gestione_anomalie_alberini.onClick() out: ' + out, LOGGINGLEVEL.INFO);
	}
}

/**
 * @param{java.awt.event.MouseEvent} e
 * @properties={typeid:24,uuid:"4ED4C9CB-1ECC-45FB-B293-DF20D844AAF0"}
 */
function onClickWrap(e) {
	onClick();
}

/**
 *
 * @properties={typeid:24,uuid:"BA35CA86-20B0-4A17-8B4D-49148FB61F5A"}
 */
function onLoad() {
	var lcrImpl = { getListCellRendererComponent: getLabel };
	elements.option_list.setCellRenderer(new Packages.javax.swing.ListCellRenderer(lcrImpl));

	var vp = elements.option_scroll.getViewport();
	//	JStaffa
	//FS
	//dep vp.add(elements.option_list);
	/** @type {java.awt.Component} */	
	var optionListAsComponent = elements.option_list;
	vp.add(optionListAsComponent);
	//	vp.add(java.awt.Component(elements.option_list));

	//	JStaffa
	//FS
	//dep	elements.option_list.addMouseListener(dcAction);
	/** @type {java.awt.event.MouseListener} */
	var dcActionMouseListener = dcAction;
	//	elements.option_list.addMouseListener(java.awt.event.MouseListener(dcAction));
	//	elements.list.addMouseListener(java.awt.event.MouseAdapter(dcAction));
	elements.option_list.addMouseListener(dcActionMouseListener);

	hideOptionPanel();

	var branchesDefault = [{ branch: "punteggio_demerito", labelHuman: "Demerito", labelWith: "con demerito ", labelNull: "senza demerito ", type: "number", enabled: true, vlist: null }, { branch: "attribuzione", labelHuman: "Causale Anomalia", labelWith: "", labelNull: "senza causale ", type: "string", enabled: true, vlist: null }, { branch: "tipo_anomalia", labelHuman: "Tipo Anomalia", labelWith: "", labelNull: "senza tipologia ", type: "string", enabled: true, vlist: "select distinct trim(MPDSEG) from XCH_SRTSE00F_IN where MPCSEG = ?" }, { branch: "sottotipo_anomalia", labelHuman: "Sottotipo Anomalia", labelWith: "", labelNull: "sottotipo non impostato ", type: "string", enabled: true, vlist: null }, { branch: "dettaglio_tipo_anomalia", labelHuman: "Dettaglio (tipo Anomalia)", labelWith: "", labelNull: "senza dettaglio ", type: "string", enabled: true, vlist: null }, { branch: "gestione_anomalia", labelHuman: "Gestione", labelWith: "", labelNull: "gestione non definita", type: "string", enabled: false, vlist: null }, { branch: "codice_versione", labelHuman: "Versione", labelWith: "", labelNull: "senza versione ", type: "string", enabled: false, vlist: null }, { branch: "segnalato_da", labelHuman: "Segnalatore", labelWith: "", labelNull: "senza segnalatore ", type: "string", enabled: false, vlist: null }, { branch: "gruppo_funzionale", labelHuman: "Gruppo Funzionale", labelWith: "", labelNull: "senza gruppo", type: "string", enabled: false, vlist: "select trim(VALORE_2) from K8_LISTE_VALORI where VALUELIST_NAME = 'Gruppi' and VALORE_1 = ?" }, { branch: "sottogruppo_funzionale", labelHuman: "Sottogruppo Funzionale", labelWith: "", labelNull: "senza sottogruppo", type: "string", enabled: false, vlist: null }, { branch: "leader", labelHuman: "Leader", labelWith: "", labelNull: "senza leader ", type: "string", enabled: false, vlist: null }, { branch: "tipo_oggetto_prova", labelHuman: "Tipo oggetto prova", labelWith: "", labelNull: "nessun oggetto prova ", type: "string", enabled: false, vlist: null }];

	var branchesLoaded = forms.nfx_interfaccia_gestione_salvataggi.load(controller.getName());

	branches = (branchesLoaded) ? branchesLoaded : branchesDefault;
	elements.tree.setCellRenderer(globals.nfx_getTreeIconRenderer());
	/** @type {javax.swing.event.TreeSelectionListener} */
	var treeSelectionListener = onClickWrap;
	elements.tree.addTreeSelectionListener(treeSelectionListener);
	//	JStaffa
	//	elements.list.addMouseListener(gotoFormWrap);
	//FS
	/** @type {java.awt.event.MouseListener} */
	var goToFormMouseListener = gotoFormWrap;
	elements.list.addMouseListener(goToFormMouseListener);
	vp = elements.scroll.getViewport();
	//	JStaffa
	//	vp.add(elements.tree);
	vp.add(java.awt.Component(elements.tree));
	var lvp = elements.scrollList.getViewport();
	//	JStaffa
	//	lvp.add(elements.list);
	lvp.add(java.awt.Component(elements.list));
	/** @type {javax.swing.tree.DefaultTreeModel} */
	var model = elements.tree.getModel();
	/** @type {javax.swing.tree.DefaultMutableTreeNode} */
	var root = model.getRoot();
	root.removeAllChildren();
	model.reload();
}

/**
 *
 * @properties={typeid:24,uuid:"856E7ED1-E715-4584-BC4B-D7DB3D3A9E31"}
 */
function onUpdateCombobox(com) {
	var query = "select count(*) from k8_anomalie a where a.codice_modello = ?";
	var args = new Array();
	if (globals.vc2_currentProgetto) {
		args.push(globals.vc2_currentProgetto);
		enableBox();
		if (globals.vc2_currentGruppoFunzionale) {
			query += " and a.gruppo_funzionale = ?";
			args.push(globals.vc2_currentGruppoFunzionale);
		}
		var nodraw = com;
		if (!nodraw) {
			writeMessage("generazione albero in corso...");
			//Arrivato qui ho query e args in linea con i valori selezionati
			drawTree(query, args);
			writeMessage(null);
		}
	} else {
		disableBox();
		hideTree();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"72C1060F-D600-4425-9DCA-B71EA758C5FF"}
 */
function onUpdateComboboxWrap() {
	globals.nfx_setBusy(true);
	onUpdateCombobox(null);
	globals.nfx_setBusy(false);
}

/**
 *
 * @properties={typeid:24,uuid:"F188A0D5-3213-4284-A367-35BA38EF0C94"}
 * @AllowToRunInFind
 */
function showOptionPanel() {
	var i = elements.allnames.length;
	//FS
	for (var out = i; out--;) {

		//dep		if (elements.allnames[i] != "Body") {
		//			if (elements.allnames[i].search("option_") != -1) {
		//				elements[elements.allnames[i]].visible = true;
		//				elements[elements.allnames[i]].enabled = true;
		//			} else {
		//				elements[elements.allnames[i]].enabled = false;
		//			}
		//		}
		if (elements.allnames[out] != "Body") {
			if (elements.allnames[out].search("option_") != -1) {
				elements[elements.allnames[out]].visible = true;
				elements[elements.allnames[out]].enabled = true;
			} else {
				elements[elements.allnames[out]].enabled = false;
			}
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"C061406B-F5E1-4F27-8E9B-80562AC44EAA"}
 */
function showTree() {
	elements.scroll.visible = true;
	elements.scrollList.visible = true;
	elements.filter1.visible = true;
	elements.filter_icon.visible = true;
	elements.download.enabled = true;
	elements.warning.visible = false;
}

/**
 * @properties={typeid:24,uuid:"6874789C-7AE8-4AA7-BE9F-A40751F550CB"}
 */
function sortNumbers(a, b) {
	return a - b;
}

/**
 * @properties={typeid:24,uuid:"058703B1-9FEF-4355-AD5F-5999E0A10BB0"}
 */
function writeMessage(e) {
	var message = e || "";
	elements.message.text = message;
	elements.message_shadow.text = message;
	application.updateUI();
}

/**
 * Perform the element default action.
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"BF54C1A6-B877-44C1-A0F6-C8AF6BF9CD8F"}
 */
function treeToPic(event) {
	var tree = elements.tree;
	var bi = new java.awt.image.BufferedImage(tree.getWidth(), tree.getHeight(), java.awt.image.BufferedImage.TYPE_INT_RGB);
	var g = bi.createGraphics();
	tree.printAll(g);
	globals.utils_saveAsPng(bi, "alberini-" + utils.dateFormat(new Date(), "yyyyMMdd") + ".png");
}
