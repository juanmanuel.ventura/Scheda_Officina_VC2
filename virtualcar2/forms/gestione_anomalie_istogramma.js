/**
 * @properties={typeid:35,uuid:"0A4DB6A9-BA57-42FB-B26E-740F867AFD87",variableType:-4}
 */
var args = new Array();

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A44C738F-F46A-4C6F-9E98-5588A3D89BFF"}
 */
var barLabelFont = "Arial";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"0647FBA7-A888-40AF-9915-FBAFC0A4D542",variableType:4}
 */
var barLabelSize = 12;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"AD922CCB-ED61-4240-913A-05DB928FC1D1"}
 */
var barLabelStyle = "Normal";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"8A8EDAF1-CFF9-424E-BB16-F0971FD4FAD0"}
 */
var bgColorDown = '';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1425D609-CFE4-48E2-8B5D-587A817EEB8F"}
 */
var bgColorUp = '';

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"196BD29B-2902-49B6-981B-EC4CDA5DBB92",variableType:8}
 */
var maxValue = 0;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D448F53B-8FC7-4943-851E-F6949DE29228"}
 */
var realQuery = '';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"86A01DB0-D05C-4C54-B497-18E2EBD00C29"}
 */
var titolo = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"15E872B6-3904-4884-9AAF-821423C09272"}
 */
var titoloFont = "Arial";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"CBB13312-FFE7-42FB-BAAE-C9061122D5F0",variableType:4}
 */
var titoloSize = 25;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A66827C2-86AC-42A3-9C89-A69B645A75C4"}
 */
var titoloStyle = "Normal";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"870B654F-588A-48E8-BF42-20C7F22E969D"}
 */
var valueLabelFont = "Arial";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"5297B0F8-12D0-4C9A-A7E5-45BBA46F76DE",variableType:4}
 */
var valueLabelSize = 12;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"3AFE3106-0BEF-44D6-922A-E11199DDE033"}
 */
var valueLabelStyle = "Normal";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"810E8BA1-67C2-45BC-A9C1-03CCB799C551"}
 */
var vetturaManual = null;

/**@type {String}
 * @properties={typeid:35,uuid:"9EF87B5B-A094-4ADB-92F2-422D074ED852"}
 */
var serverName = databaseManager.getDataSourceServerName(controller.getDataSource());

/**
 *
 * @properties={typeid:24,uuid:"EFC78E74-1E14-4F7C-8390-CAB6C88A45F7"}
 */
function drawChart() {
	if (globals.vc2_currentGruppoFunzionale)
		drawChartFull();
	else
		drawChartNoGrop();
}

/**
 *
 * @properties={typeid:24,uuid:"95DC31D2-E464-4A54-B53D-5D4DAC02C5C3"}
 */
function drawChartFull() {
	/** @type {java.awt.chart} */
	var chart = elements.chart;
	chart.show(true);

	var count;
	var result;
	var output;
	var value;
	var values = [];
	var valuesPunteggio = [];
	var labels = [];

	var stati;
	var punteggi;

	var queryTmp1 = "";
	var queryTmp2 = "";

	var sum;
	var max = 0;

	var queryPunteggioDemerito = "select distinct punteggio_demerito from k8_anomalie where punteggio_demerito is not null";
	//FS
	//dep	result = databaseManager.getDataSetByQuery(controller.getServerName(),queryPunteggioDemerito,null,-1);
	result = databaseManager.getDataSetByQuery(serverName, queryPunteggioDemerito, null, -1);

	output = result.getAsText('', '|', "", false);
	punteggi = output.split("|");
	punteggi.pop();
	punteggi.sort(globals.utils_sortNumbers);

	var queryStato = "select distinct stato_anomalia from k8_anomalie where stato_anomalia is not null";
	if (globals.vc2_currentTipoScheda == "SEGNALAZIONE") queryStato = queryStato.replace(/anomalia/g, "segnalazione");

	//dep	result = databaseManager.getDataSetByQuery(controller.getServerName(),queryStato,null,-1);
	result = databaseManager.getDataSetByQuery(serverName, queryStato, null, -1);

	output = result.getAsText('', '|', "", false);
	stati = output.split("|");
	stati.pop();
	stati.sort();

	setQuery();

	for (var i = 0; i < punteggi.length; i++) {
		queryTmp1 = realQuery + " AND punteggio_demerito = ?";
		labels.push("Punteggio demerito: " + punteggi[i]);

		args.push(punteggi[i]);

		sum = 0;
		for (var j = 0; j < stati.length; j++) {
			queryTmp2 = queryTmp1 + " AND stato_anomalia = ?";
			if (globals.vc2_currentTipoScheda == "SEGNALAZIONE") queryTmp2 = queryTmp2.replace("stato_anomalia", "stato_segnalazione");

			args.push(stati[j]);
			//FS
			//dep		count= databaseManager.getDataSetByQuery(controller.getServerName(),queryTmp2,args,-1);
			count = databaseManager.getDataSetByQuery(serverName, queryTmp2, args, -1);

			value = count.getValue(1, 1);
			sum += value;
			valuesPunteggio.push(value);
			args.pop();
		}

		valuesPunteggio.push(sum);

		values.push(valuesPunteggio);
		valuesPunteggio = [];

		args.pop();
	}

	stati.push("TOTALE");

	var totali = [];

	for (var i2 = 0; i2 < stati.length; i2++) {
		sum = 0;
		for (var k = 0; k < punteggi.length; k++) {
			sum += values[k][i2];
		}
		if (sum > max) max = sum;
		totali.push(sum);
	}

	max *= 1.1;
	chart.setRange(0, max);
	if (maxValue && maxValue > max) {
		chart.setRange(0, maxValue);
	}

	chart.setSeriesCount(punteggi.length);
	chart.setSampleCount(stati.length);
	chart.setBarType(1);
	//FS
	/** @type {Array<String>} */
	var sampleColors = [java.awt.Color.LIGHT_GRAY, java.awt.Color.CYAN, java.awt.Color.MAGENTA, java.awt.Color.YELLOW, java.awt.Color.red];
	//dep	chart.setSampleColors([java.awt.Color.LIGHT_GRAY,java.awt.Color.CYAN,java.awt.Color.MAGENTA,java.awt.Color.YELLOW,java.awt.Color.red]);
	chart.setSampleColors(sampleColors);
	/** @type {Array<Number>} */	
	var valuesi3 = values[i3];
	for (var i3 = 0; i3 < punteggi.length; i3++)
	//dep	chart.setSampleValues(i3, values[i3]);
	chart.setSampleValues(i3, valuesi3);

	chart.setSampleLabels(totali);
	chart.setSampleLabelsOn(true);
	chart.setBarLabels(stati);
	chart.setValueLabelsOn(true);
	chart.setValueLabelStyle(0);
	chart.setLegendLabels(labels);
	chart.setLegendReverseOn(true);
}

/**
 *
 * @properties={typeid:24,uuid:"5C8E7357-AAF2-4174-92E4-62DA957B7890"}
 */
function drawChartNoGrop() {

	var chart = elements.chart;
	chart.show(true);

	var count;
	var value;
	var valuesOpen = new Array();
	var valuesClosed = new Array();

	setQuery();

	var query;
	if (globals.vc2_currentTipoScheda == "ANOMALIA") {
		query = realQuery + " AND gruppo_funzionale = ? AND stato_anomalia = ?";
	} else {
		query = realQuery + " AND gruppo_funzionale = ? AND stato_segnalazione = ?";
	}
	var gruppi = getGruppi();
	var gruppiLabels = new Array();

	var maxOpen = 0;
	var maxClosed = 0;

	var sumOpen = 0;
	var sumClosed = 0;

	for (var i = 0; i < gruppi.length; i++) {
		args.push(gruppi[i]);

		args.push("APERTA");
		//FS
		//dep		count = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,-1);
		count = databaseManager.getDataSetByQuery(serverName, query, args, -1);

		value = count.getValue(1, 1);
		sumOpen += value;
		if (value > maxOpen) maxOpen = value;
		valuesOpen.push(value);
		args.pop();

		args.push("CHIUSA");
		//dep		count = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,-1);
		count = databaseManager.getDataSetByQuery(serverName, query, args, -1);

		value = count.getValue(1, 1);
		sumClosed += value;
		if (value > maxClosed) maxClosed = value;
		valuesClosed.push(value);
		args.pop();

		args.pop();

		gruppiLabels.push(getGruppo(gruppi[i]));
	}

	var max = (maxOpen + maxClosed) * 1.1;
	chart.setRange(0, max);
	if (maxValue && maxValue > max) {
		chart.setRange(0, maxValue);
	}

	chart.setSeriesCount(2);
	chart.setSampleCount(gruppi.length);
	chart.setBarType(0);
	/** @type {Array<String>} */
	var sampleColors = [java.awt.Color.RED, java.awt.Color.GREEN];
	//dep	chart.setSampleColors([java.awt.Color.RED,java.awt.Color.GREEN]
	chart.setSampleColors(sampleColors);
	chart.setSampleValues(0, valuesOpen);
	chart.setSampleValues(1, valuesClosed);
	chart.setSampleLabelsOn(false);
	chart.setBarLabels(gruppiLabels);
	chart.setValueLabelsOn(0, true);
	chart.setValueLabelsOn(1, true);
	chart.setValueLabelStyle(1);
	chart.setLegendLabels(["Aperte: " + sumOpen, "Chiuse: " + sumClosed]);
	chart.setLegendReverseOn(false);
}

/**
 *
 * @properties={typeid:24,uuid:"9BEE2698-08D0-4B6E-971C-95470D9F1619"}
 */
function getColor(e) {
	var bgColor = e;

	if (bgColor == "Bianco")
		return java.awt.Color.WHITE;
	if (bgColor == "Azzurro")
		return java.awt.Color.cyan;
	if (bgColor == "Rosa")
		return java.awt.Color.pink;
	if (bgColor == "Verde")
		return java.awt.Color.GREEN;
	if (bgColor == "Rosso")
		return java.awt.Color.RED;
	if (bgColor == "Giallo")
		return java.awt.Color.YELLOW;
	if (bgColor == "Blu")
		return java.awt.Color.BLUE;

	return null;
}

/**
 *
 * @properties={typeid:24,uuid:"3C953A54-0BEB-487A-A326-5FF6DB621D0D"}
 */
function getFont(r) {
	var fontField = r;

	if (fontField == "Arial")
		return "Arial";
	if (fontField == "Times New Roman")
		return "TimesRoman";

	return null;
}

/**
 *
 * @properties={typeid:24,uuid:"FEC290A8-A2E5-49A0-9401-5313AF1A02E6"}
 */
function getGruppi() {
	var query = "select distinct gruppo_funzionale from k8_anomalie where gruppo_funzionale is not null order by gruppo_funzionale";
	//FS
	//dep	var result = databaseManager.getDataSetByQuery(controller.getServerName(),query,null,-1);
	var result = databaseManager.getDataSetByQuery(serverName, query, null, -1);

	var output = result.getAsText('', '|', "", false);
	var values = output.split("|");
	values.pop();

	return values;
}

/**
 *
 * @properties={typeid:24,uuid:"EA8E1217-D9BF-45F0-8C56-7789FA9B1720"}
 */
function getGruppo(t) {
	var groupNumber = t;

	var query = "select VALORE_2 from K8_LISTE_VALORI where VALUELIST_NAME = ? and VALORE_1 = ?";
	var argss = ["Gruppi", groupNumber];

	//dep	var set = databaseManager.getDataSetByQuery(controller.getServerName(),query,argss,1);
	var set = databaseManager.getDataSetByQuery(serverName, query, argss, 1);

	var groupName = set.getValue(1, 1);

	if (groupName != null)
		groupName = groupName.replace(" ", "\n");

	return groupName;
}

/**
 *
 * @properties={typeid:24,uuid:"6A076F69-FC20-48BB-A09F-DDA47A46A773"}
 */
function getStyle(y) {
	var styleField = y;

	if (styleField == "Normal")
		return java.awt.Font.PLAIN;
	if (styleField == "Bold")
		return java.awt.Font.BOLD;
	if (styleField == "Italic")
		return java.awt.Font.ITALIC;

	return null;
}

/**
 *
 * @properties={typeid:24,uuid:"1FB7D004-1647-4407-985F-01A7F62A9BCF"}
 */
function nfx_getTitle() {
	return "Gestione anomalie - Istogramma";
}

/**
 *
 * @properties={typeid:24,uuid:"6ED3DEB0-60BC-4E7D-8A9C-1E1AA09FDB4F"}
 */
function nfx_isProgram() {
	return true;
}

/**
 *
 * @properties={typeid:24,uuid:"DE9F3AB1-CFDE-4907-9E95-D646BAA7A58C"}
 */
function nfx_onShow() {
	var hereMessage = "Resta sulla schemata";
	var awayMessage = "Vai a \"Report Anomalie\"";
	if (plugins.dialogs.showWarningDialog("Attenzione!", "Si avvisano gli utenti che i dati presenti in questa schermata tengono conto solamente di quelle Segnalazioni Anomalia\na cui non è collegata alcuna Scheda Anomalia, per avere una visione completa del dato si consiglia di utilizzare il modulo\nreportistica \"Istogrammi\" presente alla voce \"Report Anomalie\" dell'albero di navigazione.", hereMessage, awayMessage) != awayMessage) {
		onDataChange();
	} else {
		globals.nfx_checkAndAddGoto(forms.report_anomalie_container.controller.getName());
	}
}

/**
 *
 * @properties={typeid:24,uuid:"39769815-4DBE-45C6-9127-4C4CD16FA774"}
 */
function onDataChange() {
	// Aggiorno i nodi dell'allbero
	if (globals.vc2_currentTipoScheda) {
		elements.vc2_currentProgetto.enabled = true;
		elements.vc2_currentVettura.enabled = true;
		elements.vetturaManual1.enabled = true;
		elements.vc2_currentLeader.enabled = true;
		elements.vc2_currentGruppoFunzionale.enabled = true;
		elements.vc2_currentTipoAnomalia.enabled = true;

		drawChart();
	} else {
		onLoad();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"F9AB141B-607D-4AA2-BDD5-303777F30A70"}
 */
function onLoad() {
	elements.vc2_currentProgetto.enabled = false;
	elements.vc2_currentVettura.enabled = false;
	elements.vetturaManual1.enabled = false;
	elements.vc2_currentLeader.enabled = false;
	elements.vc2_currentGruppoFunzionale.enabled = false;
	elements.vc2_currentSottogruppoFunzionale.enabled = false;
	elements.vc2_currentTipoAnomalia.enabled = false;

	globals.vc2_currentTipoScheda = null;
	globals.vc2_currentProgetto = null;
	globals.vc2_currentVettura = null;
	globals.vc2_currentLeader = null;
	globals.vc2_currentGruppoFunzionale = null;
	globals.vc2_currentSottogruppoFunzionale = null;
	globals.vc2_currentTipoAnomalia = null;

	var chart = elements.chart;

	chart.visible = false;
	application.updateUI();

	chart.set3DModeOn(true);
	chart.set3DDepth(20);
	chart.setBarShape(1);
	chart.setBarOutlineOn(false);
	chart.setZoomOn(true);
	chart.setMultiColorOn(true);
	chart.setLegendOn(true);
	chart.setBarLabelsOn(true);
	chart.setLegendBoxSizeAsFont(true);

	var fnt;
	fnt = new java.awt.Font(getFont(titoloFont), getStyle(titoloStyle), titoloSize);
	chart.setFont("titleFont", fnt);
	fnt = new java.awt.Font(getFont(valueLabelFont), getStyle(valueLabelStyle), valueLabelSize);
	chart.setFont("valueLabelFont", fnt);
	fnt = new java.awt.Font(getFont(barLabelFont), getStyle(barLabelStyle), barLabelSize);
	chart.setFont("barLabelFont", fnt);
}

/**
 *
 * @properties={typeid:24,uuid:"357DCF87-D4D0-4ADE-9382-E360FA29B114"}
 */
function saveAsImage() {
	var image = elements.chart.getImage(elements.chart.getWidth(), elements.chart.getHeight());
	globals.utils_saveAsPng(image, "chart.png");
}

/**
 *
 * @properties={typeid:24,uuid:"25136CCA-FC48-489F-8ACB-9DC3668638AF"}
 */
function setColorDown() {

	//SAuc
	//elements.chart.setChartBackground2(getColor(bgColorDown));

	/** @type {String} */
	var str = getColor(bgColorDown);
	elements.chart.setChartBackground2(str);

	if (globals.vc2_currentTipoScheda)
		drawChart();
}

/**
 *
 * @properties={typeid:24,uuid:"29FCB00A-5DD9-44E9-B99A-33E76BF4E775"}
 */
function setColorUp() {

	//SAuc
	//elements.chart.setChartBackground(getColor(bgColorUp));

	/** @type {String} */
	var str = getColor(bgColorUp);
	elements.chart.setChartBackground(str);

	if (globals.vc2_currentTipoScheda)
		drawChart();
}

/**
 *
 * @properties={typeid:24,uuid:"ED0B8F89-FF81-49B5-8ECF-A532D6E616F3"}
 */
function setMaxValue() {

	//SAuc
	//var chart = elements.chart;

	if (maxValue) {
		if (globals.vc2_currentTipoScheda)
			drawChart();
	} else {
		maxValue = 0;

		if (globals.vc2_currentTipoScheda)
			drawChart();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"C99A4D6E-759C-4125-A4C4-8ADBECDE0BF3"}
 */
function setQuery() {
	/*
	 * Con questo if gigantesco definisco la query in base
	 * a quali dei menù a tendina sono impostati, e gli argomenti
	 * da passare alla query.
	 */

	realQuery = "select count(*) from k8_anomalie where gruppo_funzionale is not null AND tipo_scheda = ?";
	args = [globals.vc2_currentTipoScheda];

	if (globals.vc2_currentProgetto) {
		realQuery += " AND codice_modello = ?";
		args.push(globals.vc2_currentProgetto);
	}

	if (vetturaManual) {
		globals.vc2_currentVettura = null;
		realQuery += "AND codice_versione like ?";
		vetturaManual = vetturaManual.replace(/%/g, "");
		vetturaManual = vetturaManual.toUpperCase();
		args.push("%" + vetturaManual + "%");
	}

	if (globals.vc2_currentVettura) {
		realQuery += "AND codice_versione = ?";
		args.push(globals.vc2_currentVettura);
	}

	if (globals.vc2_currentGruppoFunzionale) {
		realQuery += " AND gruppo_funzionale = ?";
		args.push(globals.vc2_currentGruppoFunzionale);
		elements.vc2_currentSottogruppoFunzionale.enabled = true;
	} else {
		elements.vc2_currentSottogruppoFunzionale.enabled = false;
		globals.vc2_currentSottogruppoFunzionale = null;
	}

	if (globals.vc2_currentSottogruppoFunzionale) {
		realQuery += " AND sottogruppo_funzionale = ?";
		args.push(globals.vc2_currentSottogruppoFunzionale);
	}

	if (globals.vc2_currentLeader) {
		realQuery += " AND leader = ?";
		args.push(globals.vc2_currentLeader);
	}

	if (globals.vc2_currentTipoAnomalia) {
		realQuery += " AND tipo_anomalia = ?";
		args.push(globals.vc2_currentTipoAnomalia);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"5D050208-F190-4660-9FE0-BB228B79983A"}
 */
function setTitolo() {
	var chart = elements.chart;

	if (titolo != "") {
		chart.setTitleOn(true);
		chart.setTitle(titolo);
		if (titoloFont && titoloStyle && titoloSize && !isNaN(titoloSize) && titoloSize != 0) {
			var fnt = new java.awt.Font(getFont(titoloFont), getStyle(titoloStyle), titoloSize);
			chart.setFont("titleFont", fnt);
		}
	} else {
		chart.setTitleOn(false);
	}

	if (globals.vc2_currentTipoScheda)
		drawChart();
}

/**
 *
 * @properties={typeid:24,uuid:"A9164A4C-5D9F-4330-8501-8D7302F8A4C9"}
 */
function setupBarLabels() {
	var chart = elements.chart;

	if (barLabelFont && barLabelStyle && barLabelSize && !isNaN(barLabelSize) && barLabelSize != 0) {
		var fnt = new java.awt.Font(getFont(barLabelFont), getStyle(barLabelStyle), barLabelSize);
		chart.setFont("barLabelFont", fnt);
	}

	if (globals.vc2_currentTipoScheda)
		drawChart();
}

/**
 *
 * @properties={typeid:24,uuid:"C2A20DA1-D44C-4DC7-903D-979548969BA5"}
 */
function setupValueLabels() {
	var chart = elements.chart;

	if (valueLabelFont && valueLabelStyle && valueLabelSize && !isNaN(valueLabelSize) && valueLabelSize != 0) {
		var fnt = new java.awt.Font(getFont(valueLabelFont), getStyle(valueLabelStyle), valueLabelSize);
		chart.setFont("valueLabelFont", fnt);
	}

	if (globals.vc2_currentTipoScheda)
		drawChart();
}
