/**
 * @properties={typeid:35,uuid:"E2526D91-5D79-49D5-8183-7D57A2735033",variableType:-4}
 */
var args = new Array();

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"62FABE36-60FC-4131-877C-B451A208DCD6"}
 */
var barLabelFont = "Arial";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"BFC6558A-7C31-4578-A9D9-467B601A927E",variableType:4}
 */
var barLabelSize = 12;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"029941F8-D327-4BBC-9B5F-CD6E7CDBB89E"}
 */
var barLabelStyle = "Normal";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"92173A1D-713A-448D-B613-49E4AB7DBACB"}
 */
var bgColorDown = '';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"2C128584-DAB7-4C20-A05C-2A76784BDB67"}
 */
var bgColorUp = '';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"3935C085-F77B-4E8D-B863-A5DA23D1C920"}
 */
var currentProgetto = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"8FE01315-751A-419B-A53F-4649D8618EAF",variableType:8}
 */
var maxValue = 0;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"DEB4BF56-7DDC-4557-B162-9E99B943E0AC"}
 */
var realQuery = '';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D4190559-B77C-4F5A-8E11-4D459F8D9022"}
 */
var titolo = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"AD0B1FBA-6AF0-4297-9035-C981264B3468"}
 */
var titoloFont = "Arial";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"3A558CE0-B84B-446A-8453-D525D3639102",variableType:4}
 */
var titoloSize = 25;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1D15D2FD-1872-413B-A3DC-FD9181E8AA76"}
 */
var titoloStyle = "Normal";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"E611E24C-A4A8-4CE3-AD6C-F431A01924B0"}
 */
var valueLabelFont = "Arial";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"4DE3851A-A231-4411-A81F-0F49F01C1D1F",variableType:4}
 */
var valueLabelSize = 12;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A6248B21-CA23-4D58-BF0E-FE84F1299426"}
 */
var valueLabelStyle = "Normal";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"80F5EFCB-13DC-4F54-A982-7F6B2E3109C2"}
 */
var vetturaManual = null;

/**@type {String}
 * @properties={typeid:35,uuid:"46D83169-F9FF-4A3A-A776-E525B300766C"}
 */
var serverName = databaseManager.getDataSourceServerName(controller.getDataSource());

/**
 *
 * @properties={typeid:24,uuid:"9AFB93CA-662A-4364-B4E8-3F6C5B01779F"}
 */
function drawChart() {

	if (globals.vc2_currentGruppoFunzionale)
		drawChartFull();
	else
		drawChartNoGrop();

}

/**
 *
 * @properties={typeid:24,uuid:"4C2E39EA-5603-49E9-9EE4-8410E3AEDB6B"}
 */
function drawChartFull() {
	/** @type {java.awt.chart} */
	var chart = elements.chart;
	chart.show(true);
	var count;
	var result;
	var output;
	var value;
	/** @type {Array<Number>} */
	var values = [];
	var valuesPunteggio = [];
	var labels = [];

	var stati;
	var punteggi;

	var queryTmp1 = "";
	var queryTmp2 = "";

	var sum;
	var max = 0;

	var queryPunteggioDemerito = "select distinct punteggio_demerito from k8_anomalie where punteggio_demerito is not null";

	//	result = databaseManager.getDataSetByQuery(controller.getServerName(),queryPunteggioDemerito,null,-1);

	result = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), queryPunteggioDemerito, null, -1);

	output = result.getAsText('', '|', "", false);
	punteggi = output.split("|");
	punteggi.pop();
	punteggi.sort(sortNumbers);

	var queryStato = "select distinct stato_anomalia from k8_anomalie where stato_anomalia is not null";
	if (globals.vc2_currentTipoScheda == "SEGNALAZIONE") queryStato = queryStato.replace(/anomalia/g, "segnalazione");

	//dep	result = databaseManager.getDataSetByQuery(controller.getServerName(),queryStato,null,-1);
	result = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), queryStato, null, -1);

	output = result.getAsText('', '|', "", false);
	stati = output.split("|");
	stati.pop();
	stati.sort();

	setQuery();

	for (var i = 0; i < punteggi.length; i++) {
		queryTmp1 = realQuery + " AND punteggio_demerito = ?";
		labels.push("Punteggio demerito: " + punteggi[i]);

		args.push(punteggi[i]);

		sum = 0;
		for (var j = 0; j < stati.length; j++) {
			queryTmp2 = queryTmp1 + " AND stato_anomalia = ?";
			if (globals.vc2_currentTipoScheda == "SEGNALAZIONE") queryTmp2 = queryTmp2.replace("stato_anomalia", "stato_segnalazione");

			args.push(stati[j]);
			//	count= databaseManager.getDataSetByQuery(controller.getServerName(),queryTmp2,args,-1);
			count = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), queryTmp2, args, -1);

			value = count.getValue(1, 1);
			sum += value;
			valuesPunteggio.push(value);
			args.pop();
		}

		valuesPunteggio.push(sum);
		//FS
		/** @type {Number} */
		var valuesP = valuesPunteggio;
		//dep		values.push(valuesPunteggio);
		values.push(valuesP);
		valuesPunteggio = [];

		args.pop();
	}

	stati.push("TOTALE");

	var totali = [];
	//dep	for(var i=0;i<stati.length;i++)
	for (i = 0; i < stati.length; i++) {
		sum = 0;
		for (var k = 0; k < punteggi.length; k++) {
			sum += values[k][i];
		}
		if (sum > max) max = sum;
		totali.push(sum);
	}

	max *= 1.1;
	chart.setRange(0, max);
	if (maxValue && maxValue > max) {
		chart.setRange(0, maxValue);
	}

	chart.setSeriesCount(punteggi.length);
	chart.setSampleCount(stati.length);
	chart.setBarType(1);
	//FS
	/** @type {Array<String>} */
	var sampleColors = [java.awt.Color.LIGHT_GRAY, java.awt.Color.CYAN, java.awt.Color.MAGENTA, java.awt.Color.YELLOW, java.awt.Color.red];
	//dep		chart.setSampleColors([java.awt.Color.LIGHT_GRAY,java.awt.Color.CYAN,java.awt.Color.MAGENTA,java.awt.Color.YELLOW,java.awt.Color.red]);
	chart.setSampleColors(sampleColors);
	/** @type {Array<Number>} */
	var valuesElem = null;
	//dep
	for (i = 0; i < punteggi.length; i++) {
		valuesElem = values[i];
		//dep	chart.setSampleValues(i,values[i]);
		chart.setSampleValues(i, valuesElem);
	}
	chart.setSampleLabels(totali);
	chart.setSampleLabelsOn(true);
	chart.setBarLabels(stati);
	chart.setValueLabelsOn(true);
	chart.setValueLabelStyle(0);
	chart.setLegendLabels(labels);
	chart.setLegendReverseOn(true);

}

/**
 *
 * @properties={typeid:24,uuid:"C600FFCB-F8AA-4721-84C5-6C0FB64FE4EB"}
 */
function drawChartNoGrop() {
	/** @type {java.awt.chart} */
	var chart = elements.chart;
	chart.show(true);

	var count;
	var value;
	var valuesOpen = new Array();
	var valuesClosed = new Array();

	setQuery();

	var query;
	if (globals.vc2_currentTipoScheda == "ANOMALIA") {
		query = realQuery + " AND gruppo_funzionale = ? AND stato_anomalia = ?";
	} else {
		query = realQuery + " AND gruppo_funzionale = ? AND stato_segnalazione = ?";
	}
	var gruppi = getGruppi();
	var gruppiLabels = new Array();

	var maxOpen = 0;
	var maxClosed = 0;

	var sumOpen = 0;
	var sumClosed = 0;

	for (var i = 0; i < gruppi.length; i++) {
		args.push(gruppi[i]);

		args.push("APERTA");
		count = databaseManager.getDataSetByQuery(serverName, query, args, -1);
		value = count.getValue(1, 1);
		sumOpen += value;
		if (value > maxOpen) maxOpen = value;
		valuesOpen.push(value);
		args.pop();

		args.push("CHIUSA");
		count = databaseManager.getDataSetByQuery(serverName, query, args, -1);
		value = count.getValue(1, 1);
		sumClosed += value;
		if (value > maxClosed) maxClosed = value;
		valuesClosed.push(value);
		args.pop();

		args.pop();

		gruppiLabels.push(getGruppo(gruppi[i]));
	}

	var max = (maxOpen + maxClosed) * 1.1;
	chart.setRange(0, max);
	if (maxValue && maxValue > max) {
		chart.setRange(0, maxValue);
	}

	chart.setSeriesCount(2);
	chart.setSampleCount(gruppi.length);
	chart.setBarType(0);
	//FS
	//dep	chart.setSampleColors([java.awt.Color.RED,java.awt.Color.GREEN]);
	/** @type {Array<String>} */
	var sampleColors = [java.awt.Color.RED, java.awt.Color.GREEN];
	chart.setSampleColors(sampleColors);
	chart.setSampleValues(0, valuesOpen);
	chart.setSampleValues(1, valuesClosed);
	chart.setSampleLabelsOn(false);
	chart.setBarLabels(gruppiLabels);
	chart.setValueLabelsOn(0, true);
	chart.setValueLabelsOn(1, true);
	chart.setValueLabelStyle(1);
	chart.setLegendLabels(["Aperte: " + sumOpen, "Chiuse: " + sumClosed]);
	chart.setLegendReverseOn(false);

}

/**
 *@param {String} bgColor
 * @properties={typeid:24,uuid:"4D3BC75C-ED23-47C4-8AFA-489ABE4C3B28"}
 */
function getColor(bgColor) {

	//dep	var bgColor = arguments[0];
	//
	//	if (bgColor == "Bianco")
	//		return java.awt.Color.WHITE;
	//	if (bgColor == "Azzurro")
	//		return java.awt.Color.cyan;
	//	if (bgColor == "Rosa")
	//		return java.awt.Color.pink;
	//	if (bgColor == "Verde")
	//		return java.awt.Color.GREEN;
	//	if (bgColor == "Rosso")
	//		return java.awt.Color.RED;
	//	if (bgColor == "Giallo")
	//		return java.awt.Color.YELLOW;
	//	if (bgColor == "Blu")
	//		return java.awt.Color.BLUE;
	/** @type {java.awt.Color} */
	var color = null;

	if (bgColor == "Bianco")
		color = java.awt.Color.WHITE;
	if (bgColor == "Azzurro")
		color = java.awt.Color.cyan;
	if (bgColor == "Rosa")
		color = java.awt.Color.pink;
	if (bgColor == "Verde")
		color = java.awt.Color.GREEN;
	if (bgColor == "Rosso")
		color = java.awt.Color.RED;
	if (bgColor == "Giallo")
		color = java.awt.Color.YELLOW;
	if (bgColor == "Blu")
		color = java.awt.Color.BLUE;

	return color;

}

/**
 *@param {String} fontField
 * @properties={typeid:24,uuid:"0FAC14B3-20D5-4392-892A-73CE0AC84AAB"}
 */
function getFont(fontField) {

	//dep	var fontField = arguments[0];
	/** @type {String} */
	var font = null;
	if (fontField == "Arial")
		font = "Arial";
	if (fontField == "Times New Roman")
		font = "TimesRoman";
	return font;
}

/**
 *
 * @properties={typeid:24,uuid:"9F15E8FE-24BB-4798-8676-376029D2BB66"}
 */
function getGruppi() {

	var query = "select distinct gruppo_funzionale from k8_anomalie where gruppo_funzionale is not null order by gruppo_funzionale";

	var result = databaseManager.getDataSetByQuery(serverName, query, null, -1);
	var output = result.getAsText('', '|', "", false);
	var values = output.split("|");
	values.pop();

	return values;

}

/**
 *@param{String} groupNumber
 * @properties={typeid:24,uuid:"975F8B89-67C0-4BBB-9CF8-4BE90647F43A"}
 */
function getGruppo(groupNumber) {

	//	var groupNumber = arguments[0];

	var query = "select VALORE_2 from K8_LISTE_VALORI where VALUELIST_NAME = ? and VALORE_1 = ?";
	var _args = ["Gruppi", groupNumber];

	var set = databaseManager.getDataSetByQuery(serverName, query, _args, 1);

	var groupName = set.getValue(1, 1);
	if (groupName != null)
		groupName = groupName.replace(" ", "\n");

	return groupName;

}

/**
 *@param {String} styleField
 * @properties={typeid:24,uuid:"458B5CEF-ACA6-4D8C-B638-C9BA6A10CBB6"}
 */
function getStyle(styleField) {

	//dep	var styleField = arguments[0];
	var style = null;
	//	if (styleField == "Normal")
	//		return java.awt.Font.PLAIN;
	//	if (styleField == "Bold")
	//		return java.awt.Font.BOLD;
	//	if (styleField == "Italic")
	//		return java.awt.Font.ITALIC;
	if (styleField == "Normal")
		style = java.awt.Font.PLAIN;
	if (styleField == "Bold")
		style = java.awt.Font.BOLD;
	if (styleField == "Italic")
		style = java.awt.Font.ITALIC;

	return style;
}

/**
 *
 * @properties={typeid:24,uuid:"735FDBA7-4B05-44D7-B11B-4867965663B0"}
 */
function nfx_getTitle() {

	return "ITDES - Istogramma";

}

/**
 * @properties={typeid:24,uuid:"FFA37C64-EBAB-478E-84AF-EE8C835E32A9"}
 */
function nfx_isProgram() {
	return true;
}

/**
 * @properties={typeid:24,uuid:"3008A313-1779-4935-A78B-402428EBF3E2"}
 */
function nfx_defaultPermissions() {
	return "^";
}

/**
 *
 * @properties={typeid:24,uuid:"7BF5FF5F-7B75-4802-9FBE-F5720C54BB2E"}
 */
function nfx_onHide() {
	globals.vc2_currentProgetto = currentProgetto;
	currentProgetto = null;
}

/**
 *
 * @properties={typeid:24,uuid:"835BF22C-CEF6-4963-B81E-F8CFFFED1D82"}
 */
function nfx_onShow() {
	currentProgetto = globals.vc2_currentProgetto;
	globals.vc2_currentProgetto = "F151";
	onDataChange();

}

/**
 *
 * @properties={typeid:24,uuid:"CCA19D69-8043-4A76-8BB3-2D4790ABCD6D"}
 */
function onDataChange() {
	// Aggiorno i nodi dell'allbero
	if (globals.vc2_currentTipoScheda) {
		elements.vc2_currentVettura.enabled = true;
		elements.vetturaManual1.enabled = true;
		elements.vc2_currentLeader.enabled = true;
		elements.vc2_currentGruppoFunzionale.enabled = true;

		drawChart();
	} else {
		onLoad();
	}

}

/**
 *
 * @properties={typeid:24,uuid:"50DB7CD3-5757-468D-8225-61AD48CA9813"}
 */
function onLoad() {
	elements.vc2_currentVettura.enabled = false;
	elements.vetturaManual1.enabled = false;
	elements.vc2_currentLeader.enabled = false;
	elements.vc2_currentGruppoFunzionale.enabled = false;

	globals.vc2_currentTipoScheda = null;
	globals.vc2_currentVettura = null;
	globals.vc2_currentLeader = null;
	globals.vc2_currentGruppoFunzionale = null;

	var chart = elements.chart;

	chart.visible = false;
	application.updateUI();

	chart.set3DModeOn(true);
	chart.set3DDepth(20);
	chart.setBarShape(1);
	chart.setBarOutlineOn(false);
	chart.setZoomOn(true);
	chart.setMultiColorOn(true);
	chart.setLegendOn(true);
	chart.setBarLabelsOn(true);
	chart.setLegendBoxSizeAsFont(true);

	var fnt;
	fnt = new java.awt.Font(getFont(titoloFont), getStyle(titoloStyle), titoloSize);
	chart.setFont("titleFont", fnt);
	fnt = new java.awt.Font(getFont(valueLabelFont), getStyle(valueLabelStyle), valueLabelSize);
	chart.setFont("valueLabelFont", fnt);
	fnt = new java.awt.Font(getFont(barLabelFont), getStyle(barLabelStyle), barLabelSize);
	chart.setFont("barLabelFont", fnt);

}

/**
 *
 * @properties={typeid:24,uuid:"04884F1F-17FF-4613-A9C0-3654752529F0"}
 */
function saveAsImage() {

	var image = elements.chart.getImage(elements.chart.getWidth(), elements.chart.getHeight());
	globals.utils_saveAsPng(image, "chart.png");

}

/**
 *
 * @properties={typeid:24,uuid:"33314781-339C-4DAB-A3A9-05C370F73342"}
 */
function setColorDown() {
	//FS
	/** @type {java.awt.chart} */
	var chart = elements.chart;
	/** @type {String} */
	var backGround2Color = getColor(bgColorDown);
	//dep	chart.setChartBackground2(getColor(bgColorDown));
	chart.setChartBackground2(backGround2Color);

	if (globals.vc2_currentTipoScheda)
		drawChart();

}

/**
 *
 * @properties={typeid:24,uuid:"19CAC8AF-CF25-4EDC-A170-6FF554C3D923"}
 */
function setColorUp() {
	/** @type {String} */
	var backGroundColor = getColor(bgColorUp);
	//dep	elements.chart.setChartBackground(getColor(bgColorUp));
	elements.chart.setChartBackground(backGroundColor);

	if (globals.vc2_currentTipoScheda)
		drawChart();

}

/**
 *
 * @properties={typeid:24,uuid:"5C12280F-64FC-4184-9192-26B1B424FE17"}
 */
function setMaxValue() {
	/** @type {java.awt.chart} */
	//dep  var chart = elements.chart;

	if (maxValue) {
		if (globals.vc2_currentTipoScheda)
			drawChart();
	} else {
		maxValue = 0;

		if (globals.vc2_currentTipoScheda)
			drawChart();
	}

}

/**
 *
 * @properties={typeid:24,uuid:"64B09918-CF33-4A14-BA79-88DA795FDA04"}
 */
function setQuery() {

	/*
	 * Con questo if gigantesco definisco la query in base
	 * a quali dei menù a tendina sono impostati, e gli argomenti
	 * da passare alla query.
	 */

	realQuery = "select count(*) from k8_anomalie where gruppo_funzionale is not null AND tipo_scheda = ?";
	args = [globals.vc2_currentTipoScheda];

	realQuery += " AND codice_modello = ?";
	args.push("F151");

	if (vetturaManual) {
		globals.vc2_currentVettura = null;
		realQuery += "AND codice_versione like ?";
		vetturaManual = vetturaManual.replace(/%/g, "");
		vetturaManual = vetturaManual.toUpperCase();
		args.push("%" + vetturaManual + "%");
	}

	if (globals.vc2_currentVettura) {
		realQuery += "AND codice_versione = ?";
		args.push(globals.vc2_currentVettura);
	}

	if (globals.vc2_currentGruppoFunzionale) {
		realQuery += " AND gruppo_funzionale = ?";
		args.push(globals.vc2_currentGruppoFunzionale);
	}

	if (globals.vc2_currentLeader) {
		realQuery += " AND leader = ?";
		args.push(globals.vc2_currentLeader);
	}

}

/**
 *
 * @properties={typeid:24,uuid:"BAE25DEE-B7B3-420F-B3A4-4D44413E8FF7"}
 */
function setTitolo() {
	/** @type {java.awt.chart} */
	var chart = elements.chart;

	if (titolo != "") {
		chart.setTitleOn(true);
		chart.setTitle(titolo);
		if (titoloFont && titoloStyle && titoloSize && !isNaN(titoloSize) && titoloSize != 0) {
			var fnt = new java.awt.Font(getFont(titoloFont), getStyle(titoloStyle), titoloSize);
			chart.setFont("titleFont", fnt);
		}
	} else {
		chart.setTitleOn(false);
	}

	if (globals.vc2_currentTipoScheda)
		drawChart();

}

/**
 *
 * @properties={typeid:24,uuid:"F4D931EE-5801-4DA7-B857-BB057EF3D607"}
 */
function setupBarLabels() {

	var chart = elements.chart;

	if (barLabelFont && barLabelStyle && barLabelSize && !isNaN(barLabelSize) && barLabelSize != 0) {
		var fnt = new java.awt.Font(getFont(barLabelFont), getStyle(barLabelStyle), barLabelSize);
		chart.setFont("barLabelFont", fnt);
	}

	if (globals.vc2_currentTipoScheda)
		drawChart();

}

/**
 *
 * @properties={typeid:24,uuid:"A21B6BE4-3D5B-488C-B99C-4DA9D9467FD7"}
 */
function setupValueLabels() {

	var chart = elements.chart;

	if (valueLabelFont && valueLabelStyle && valueLabelSize && !isNaN(valueLabelSize) && valueLabelSize != 0) {
		var fnt = new java.awt.Font(getFont(valueLabelFont), getStyle(valueLabelStyle), valueLabelSize);
		chart.setFont("valueLabelFont", fnt);
	}

	if (globals.vc2_currentTipoScheda)
		drawChart();

}

/**
 *
 * @properties={typeid:24,uuid:"9449C4FA-50DD-453C-B869-214DA9C5BCFC"}
 */
function sortNumbers() {

	var a = arguments[0];
	var b = arguments[1];

	return a - b;

}
