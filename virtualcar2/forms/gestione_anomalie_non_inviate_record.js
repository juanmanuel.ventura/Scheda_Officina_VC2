/**
 * @properties={typeid:24,uuid:"6938143B-D3F2-4832-880B-809AA290E681"}
 */
function goToS(){
	if (tipo_scheda == "SEGNALAZIONE"){
		//globals.alternate=true;
		//globals.toGoTo = "segnalazioni_anomalie_record";
		globals.nfx_goToProgramAndRecord("segnalazioni_anomalie_record", foundset.k8_anomalie_id, "k8_anomalie_id");
		//globals.alternate=false;
	}else{
		//globals.alternate=true;
		//globals.toGoTo = "schede_anomalie_record";
		globals.nfx_goToProgramAndRecord("schede_anomalie_record", foundset.k8_anomalie_id, "k8_anomalie_id");
		//globals.alternate=false;
	}
}

/**
 * @properties={typeid:24,uuid:"8F576CEF-9F82-4518-A66B-1C6A8BA7FF21"}
 */
function inviaMail(){
}

/**
 *
 * @properties={typeid:24,uuid:"19AE0F0D-3B49-4140-B322-36C715F1B1EB"}
 */
function nfx_defineAccess(){
	return [false, false, true];
}

/**
 *
 * @properties={typeid:24,uuid:"A347E210-5232-484F-AD6C-C53B000124F5"}
 */
function nfx_getTitle(){
	return "Segnalazioni e Anomalie non inviate";
}

/**
 *
 * @properties={typeid:24,uuid:"F663C825-4F20-41B9-8346-8CCCD2FF22FD"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"FEA1AFDE-AB96-4B76-9AEA-B4479837C8B1"}
 */
function nfx_defaultPermissions(){
	return "^";
}

/**
 *
 * @properties={typeid:24,uuid:"8F723897-966A-409B-A791-22FE504D3007"}
 */
function nfx_preDelete(){
	if(plugins.dialogs.showInfoDialog("Eliminazione segnalazione/scheda",
		"ATTENZIONE: la segnalazione/scheda verra' eliminata insieme alle relative descrizioni e non sara' recuperabile! Continuare?",
				"Si","No") == "No"){
		
	}
	return -1;
}

/**
 *
 * @properties={typeid:24,uuid:"B18C0592-D997-4137-A97B-C86026FE104B"}
 */
function nfx_sks(){
	return [{icon: "ripristina.png", tooltip:"Ripristina", method:"ripristinaAnomalia"}, {tootlip: "Vai a segnalazione/scheda", icon:"to-s.png", method:"goToS"}];
}

/**
 *
 * @properties={typeid:24,uuid:"2B7F73B9-62B3-4BFC-94A2-1C38097AD0E5"}
 */
function nfx_validate(){
	return 0;
}

/**
 *
 * @properties={typeid:24,uuid:"74D42C93-9B7B-403C-8E11-B3C5CF1EE04E"}
 */
function ripristinaAnomalia(){
	globals.vc2_ripristinaNonInviate(foundset,controller.getName());
}
