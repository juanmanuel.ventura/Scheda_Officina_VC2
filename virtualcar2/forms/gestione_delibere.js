/**
 * @properties={typeid:24,uuid:"1C7D472D-B1AB-434A-95A1-612ED21E16B9"}
 */
function nfx_getTitle() {
	return "Gestione delibere";
}

/**
 * @properties={typeid:24,uuid:"D44EA140-DDD1-489B-B9B9-9294DDFEC7CB"}
 */
function nfx_isProgram() {
	return true;
}

/**
 * @properties={typeid:24,uuid:"61D79202-F7E9-44DF-98C3-57AAB535AFFC"}
 */
function nfx_defaultPermissions() {
	return "^";
}

/**
 * @properties={typeid:24,uuid:"D4859F8D-D8E9-4F40-98E7-A71C48F728D4"}
 */
function nfx_onShow() {
	globals.vc2_currentDisegno = (globals.vc2_currentDisegno == -1) ? null : globals.vc2_currentDisegno;
}

/**
 * @properties={typeid:24,uuid:"FADD7684-F64B-4756-9A48-192E312DCC0C"}
 */
function nfx_onHide() {
	globals.vc2_currentDisegno = (!globals.vc2_currentDisegno) ? -1 : globals.vc2_currentDisegno;
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"E3DBEBE6-E5BA-44BA-A52A-28783BAC6C44"}
 */
function onDisegnoChange(oldValue, newValue, event) {
	//SAuc
	/** @type {JSFoundSet} */

	var fs = k8_dummy_to_k8_stato_delibere && k8_dummy_to_k8_stato_delibere;

	if (fs.getSize() == 0) {
		forms.gestione_delibere_sd.createManagement();

	}
	return true;
}
