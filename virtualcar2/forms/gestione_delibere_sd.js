/**
 * @properties={typeid:24,uuid:"E7FA767F-CE36-440A-8F82-B633281A1E45"}
 */
function createManagement(){
	controller.newRecord();
}

/**
 * @properties={typeid:24,uuid:"1A37086D-BC56-4424-BD54-E1976E93A61A"}
 */
function deleteManagement(){
	controller.deleteRecord();
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"D0D9D83B-1DC1-4BFA-A5B7-F7FE4D4B7121"}
 */
function onRightClick(event) {
//FS
//dep	var item = plugins.popupmenu.createMenuItem("Add row for " + elements[event.getElementName()].text,addRow);
//		item.setMethodArguments([event]);
//		plugins.popupmenu.showPopupMenu(elements[event.getElementName()],[item]);
	
	var menu=plugins.window.createPopupMenu();
	var item = menu.addMenuItem("Add row for " + elements[event.getElementName()].text,addRow);
	item.methodArguments=[event];
	menu.show(elements[event.getElementName()]);
}

/**
 * @param {JSEvent} event the event that triggered the action
 * @properties={typeid:24,uuid:"0D9D581E-4200-421F-9049-50CFF2FDC06C"}
 */
function addRow(event){
	/** @type {JSFoundSet} */
	var r = k8_stato_delibere_to_k8_righe_stato_delibere;
	
	r.newRecord(false,true);
	r.fk_field_name = event.getElementName().replace("_label","");
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"784DC17A-E312-4DB4-85E0-297856E6F42C"}
 */
function onAction(event) {
	try{
		down();
	}catch(e){
//		application.output(e.message);
	}
}

/**
 * @properties={typeid:24,uuid:"C1652347-64C4-4611-9CF4-4FFAC8A039BD"}
 */
function down(){
	//var elem = "stile";
	//elements[elem + "_label"].setLocation(elements[elem + "_label"].getLocationX(),elements[elem + "_label"].getLocationY() + 30);
	//elements[elem].setLocation(elements[elem].getLocationX(),elements[elem].getLocationY() + 30);
	//elements[elem + "1"].setLocation(elements[elem + "1"].getLocationX(),elements[elem + "1"].getLocationY() + 30);
	elements.stile.setSize(20,1000);
}
