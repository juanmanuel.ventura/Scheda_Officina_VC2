/**
 *
 * @properties={typeid:24,uuid:"73AC14A3-92E6-4D2F-8167-D53DA37C22AD"}
 */
function changePwd(){
	if(!globals.isPowerUser(globals.nfx_user) && globals.nfx_user != user_servoy){
		plugins.dialogs.showErrorDialog("Errore","L'utente " + globals.nfx_user + " non ha il permesso di modificare la password di " + nome + " " + cognome,"Ok");
	}else{
		
		//application.showFormInDialog(forms.gestione_utenze_cambio_password_popup,null,null,null,null,"Cambio password",true,false,"password_popup",true);
		
		var formq = forms.gestione_utenze_cambio_password_popup;
		var window = application.createWindow("password_popup",JSWindow.MODAL_DIALOG);
		window.title="Cambio password";
		window.resizable=true;
		formq.controller.show(window);
		
	}
}

/**
 *
 * @properties={typeid:24,uuid:"BCD2D4CF-474E-4D20-B8B8-856C6E7450FA"}
 */
function nfx_defineAccess(){
	return [true,true,true];
}

/**
 *
 * @properties={typeid:24,uuid:"CDBB8789-F996-46BC-8052-C0DF06B568BC"}
 */
function nfx_preAdd()
{
	var user = plugins.dialogs.showInputDialog("Username","Inserire lo Username desiderato");
	if(!user)
	{
		plugins.dialogs.showErrorDialog("Errore","Username non riconosciuto, inserimento annullato.","Ok");
		return -1;
	}
//FS
//dep	var result = databaseManager.getDataSetByQuery(controller.getServerName(),"select user_id from k8_gestione_utenze where user_servoy = ?",[user],1);
	var result = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()),"select user_id from k8_gestione_utenze where user_servoy = ?",[user],1);

	if(result.getValue(1,1))
	{
		plugins.dialogs.showErrorDialog("Errore","Lo Username \"" + user + "\" è già presente con ID " + result.getValue(1,1) + ", inserimento annullato.","Ok");
		return -1;
	}
	user_servoy = user;
	var pass1 = plugins.dialogs.showInputDialog("Password","Inserire la Password desiderata (min. " + globals.nfx_minPasswordLength + " caratteri)");
	if(!pass1 || pass1.length < globals.nfx_minPasswordLength)
	{
		plugins.dialogs.showErrorDialog("Errore","Password non valida, inserimento annullato.","Ok");
		return -1;
	}
	pass1 = utils.stringMD5HashBase64(pass1);
	var pass2 = plugins.dialogs.showInputDialog("Verifica","Ripetere la Password");
	pass2 = utils.stringMD5HashBase64(pass2);
	if(pass1 != pass2)
	{
		plugins.dialogs.showErrorDialog("Errore","Le Password inserite non coincidono, inserimento annullato.","Ok");
		return -1;		
		
	}
	password_value = pass1;
	
}

/**
 *
 * @properties={typeid:24,uuid:"7D409103-D287-476B-B2B7-D799F73765CD"}
 */
function nfx_sks(){
	return [{icon: "key.png", tooltip:"Cambio password", method:"changePwd"}];
}

/**
 *
 * @properties={typeid:24,uuid:"9485AF05-70D6-4575-AA4D-30D3758B43B2"}
 */
function nfx_validate(){
	return globals.utils_defaultValidationMethod(controller.getName(),{"user_servoy" : "User Servoy",
																	   "cognome" : "Cognome",
																	   "nome" : "Nome",
																	   "email": "Email"});
}
