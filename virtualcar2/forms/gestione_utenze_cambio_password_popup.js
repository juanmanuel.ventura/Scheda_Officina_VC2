/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"2C3308C4-259D-4646-9D6F-788EB8FBD6FC"}
 */
var name = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"3809D208-F068-4787-A147-027F348C9594"}
 */
var pass1 = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B3229E90-3198-4412-B526-F42E406D2E9B"}
 */
var pass2 = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1EEF04DA-30BB-4E47-8223-A81CCD65D97D"}
 */
var surname = null;

/**
 *@param {JSEvent} event
 * @properties={typeid:24,uuid:"4C36F771-404E-4580-B2E1-CFFA8AB243F9"}
 */
function change(event){
	if(!name || name == "" || !surname || surname == ""){
		elements.message_shadow.fgcolor = "#FFFF00";
		writeMessage("Campi di verifica non compilati");
		return;
	}
	if(name.toUpperCase() != nome.toUpperCase() || surname.toUpperCase() != cognome.toUpperCase()){
		elements.message_shadow.fgcolor = "#FFFF00";
		writeMessage("I campi di verifica non coincidono");
		surname = null;
		name = null;
		return;
	}
	if(pass1 && pass1.length >= globals.nfx_minPasswordLength){
		var pwd1 = utils.stringMD5HashBase64(pass1);
		pass1 = null;
	}else{
		elements.message_shadow.fgcolor = "#FF0000";
		writeMessage("Lunghezza minima: " + globals.nfx_minPasswordLength + " caratteri");
		pass1 = null;
		pass2 = null;
		return;
	}
	if(pass2){
		var pwd2 = utils.stringMD5HashBase64(pass2);
		pass2 = null;
	}else{
		elements.message_shadow.fgcolor = "#FF0000";
		writeMessage("Le password inserite non coincidono");
		pass2 = null;
		return;
	}
	if(pwd1 != pwd2){
		elements.message_shadow.fgcolor = "#FF0000";
		writeMessage("Le password inserite non coincidono");
		return;
	}
	
	password_value = pwd1;
	elements.message_shadow.fgcolor = "#00FF00";
	writeMessage("Password modificata");
	
	application.updateUI(500);
//FS	
//dep	application.closeFormDialog();
application.getActiveWindow().destroy();
//	var w=controller.getWindow();
//	w.destroy();
}

/**
 *
 * @properties={typeid:24,uuid:"F1357DE5-B4C0-4128-8DF0-CC9B4CFD009D"}
 */
function onHide(){
	name = null;
	surname = null;
	pass1 = null;
	pass2 = null;
	
	writeMessage("");
}

/**
 *
 * @properties={typeid:24,uuid:"48B4DFAB-507F-4571-848E-470466DF799D"}
 */
function writeMessage(message){

	
	elements.message.text = elements.message_shadow.text = message;
	application.updateUI();
}
