dataSource:"db:/ferrari/k8_gestione_utenze",
extendsID:"6C1E1FA7-B1E0-43C0-B63D-39843EDCD9DB",
items:[
{
labelFor:"nome",
location:"240,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Nome",
typeid:7,
uuid:"00EB6003-C9CD-45CE-998C-C64F654DDEE2"
},
{
labelFor:"telefono",
location:"1590,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Telefono",
typeid:7,
uuid:"024D5BB0-40BE-42A3-BFAC-63352059592D"
},
{
anchors:11,
background:"#ccff66",
borderType:"EtchedBorder,0,null,null",
dataProviderID:"email",
location:"390,40",
name:"email",
size:"140,20",
styleClass:"table",
text:"Email",
typeid:4,
uuid:"0D79E01E-9374-42E8-AEDA-12B6B81AE19E"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"telefono",
location:"1590,40",
name:"telefono",
size:"140,20",
styleClass:"table",
text:"Telefono",
typeid:4,
uuid:"16D57787-7F69-49D0-9771-BA26B72E50DC"
},
{
anchors:11,
background:"#ccff66",
borderType:"EtchedBorder,0,null,null",
dataProviderID:"nome",
location:"240,40",
name:"nome",
size:"140,20",
styleClass:"table",
text:"Nome",
typeid:4,
uuid:"34DE624B-FE97-4254-AC29-BBAFA2CBCD9E"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"azienda",
location:"1290,40",
name:"azienda",
size:"140,20",
styleClass:"table",
text:"Azienda",
typeid:4,
uuid:"3ED3A7F4-20E8-4ABE-BE2C-D7C5CEEBF938"
},
{
labelFor:"ente_as400",
location:"840,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Ente As400",
typeid:7,
uuid:"40820303-DEB6-4809-93D4-20CF60BB9FA8"
},
{
height:480,
partType:5,
typeid:19,
uuid:"44AE7C00-C8A9-4098-9497-DFE5907B7B00"
},
{
labelFor:"email",
location:"390,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Email",
typeid:7,
uuid:"4B123AD1-4850-46D4-AE38-872C575963D1"
},
{
labelFor:"corso",
location:"1740,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Corso",
typeid:7,
uuid:"525FE412-1EEB-4D16-880A-39632D5F69DB"
},
{
labelFor:"stato",
location:"1890,10",
mediaOptions:14,
size:"50,20",
styleClass:"table",
tabSeq:-1,
text:"Stato",
typeid:7,
uuid:"60C609FF-6BCD-4F2A-9305-5100115A4D09"
},
{
labelFor:"direzione",
location:"1440,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Direzione",
typeid:7,
uuid:"6171F026-6840-4ABE-91F4-93E8E2B9F18F"
},
{
labelFor:"ente",
location:"990,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Ente",
typeid:7,
uuid:"62AFF9DB-1111-4111-9DB0-8851396C6070"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"ente_as400",
location:"840,40",
name:"ente_as400",
size:"140,20",
styleClass:"table",
text:"Ente As400",
typeid:4,
uuid:"7A662588-EB59-4B66-8BF6-1503B7D1E4D2"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"cant_customize_tree",
location:"2360,40",
name:"cant_customize_tree",
size:"230,20",
styleClass:"table",
text:"Cant Customize Tree",
typeid:4,
uuid:"8134F439-91F6-4253-B2D8-CF959FE031F5"
},
{
labelFor:"note",
location:"1950,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Note",
typeid:7,
uuid:"845B994C-064B-47DA-A635-E6964AFBA17B"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"dipartimento",
location:"1140,40",
name:"dipartimento",
size:"140,20",
styleClass:"table",
text:"Dipartimento",
typeid:4,
uuid:"877807F4-3E6A-43B3-BA2D-8B54E26EE897"
},
{
labelFor:"dipartimento",
location:"1140,10",
mediaOptions:14,
size:"120,20",
styleClass:"table",
tabSeq:-1,
text:"Dipartimento",
typeid:7,
uuid:"93EEC747-DB89-44DE-B1B3-BB836221937F"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"ente",
location:"990,40",
name:"ente",
size:"140,20",
styleClass:"table",
text:"Ente",
typeid:4,
uuid:"9A26FE3A-4C30-4D4F-8898-82ECF15AF62F"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"note",
location:"1950,40",
name:"note",
size:"400,20",
styleClass:"table",
text:"Note",
typeid:4,
uuid:"B45FB42F-A9BF-488E-8985-3CC051E4FA13"
},
{
labelFor:"azienda",
location:"1290,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Azienda",
typeid:7,
uuid:"B6FA6571-C96F-4A86-897A-0E8DF0F9BB81"
},
{
labelFor:"user_as400",
location:"690,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"User As400",
typeid:7,
uuid:"B7750F80-D02A-49A4-8F55-1C76FAEA93DC"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"direzione",
location:"1440,40",
name:"direzione",
size:"140,20",
styleClass:"table",
text:"Direzione",
typeid:4,
uuid:"B9514083-1627-4864-85E7-6CE3B959A6E5"
},
{
anchors:11,
background:"#ccff66",
borderType:"EtchedBorder,0,null,null",
dataProviderID:"cognome",
location:"90,40",
name:"cognome",
size:"140,20",
styleClass:"table",
text:"Cognome",
typeid:4,
uuid:"BA2BA3F8-BDBA-4BD2-8574-DEECC4A8EBE1"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"stato",
location:"1890,40",
name:"stato",
size:"50,20",
styleClass:"table",
text:"Stato",
typeid:4,
uuid:"BF2F7B8B-92FE-4EA1-A9C1-0D2DCD4204B7"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"corso",
location:"1740,40",
name:"corso",
size:"140,20",
styleClass:"table",
text:"Corso",
typeid:4,
uuid:"C9C3BE1E-180F-40E8-8A26-2E482A4A2031"
},
{
labelFor:"cant_customize_tree",
location:"2360,10",
mediaOptions:14,
size:"230,20",
styleClass:"table",
tabSeq:-1,
text:"Disabilita personalizzazione albero",
typeid:7,
uuid:"E526907A-ED65-4845-B208-4B24B882E6F6"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"user_as400",
location:"690,40",
name:"user_as400",
size:"140,20",
styleClass:"table",
text:"User As400",
typeid:4,
uuid:"F24D20A7-4511-47FF-97AC-A704016E0513"
},
{
labelFor:"user_servoy",
location:"540,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"User Servoy",
typeid:7,
uuid:"F70687F9-FB63-4353-9774-443FC56F69F5"
},
{
anchors:11,
background:"#ccff66",
borderType:"EtchedBorder,0,null,null",
dataProviderID:"user_servoy",
editable:false,
location:"540,40",
name:"user_servoy",
size:"140,20",
styleClass:"table",
text:"User Servoy",
typeid:4,
uuid:"FA800E45-0CA9-42B3-AA97-D0F25DFBD503"
},
{
labelFor:"cognome",
location:"90,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Cognome",
typeid:7,
uuid:"FBA9EAC5-0FEB-4C43-8B34-A293BF8A0EF2"
}
],
name:"gestione_utenze_table",
paperPrintScale:100,
showInMenu:true,
size:"2600,480",
styleName:"keeneight",
typeid:3,
uuid:"E35CA121-3B07-4EEA-8307-A5BFADEA9294",
view:3