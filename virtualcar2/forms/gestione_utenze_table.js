/**
 * @properties={typeid:24,uuid:"BBA9EBB1-BF69-4F19-B275-F65DB932A017"}
 */
function nfx_getTitle(){
	return "Gestione Utenze";
}

/**
 * @properties={typeid:24,uuid:"C922158E-839A-4CED-85E0-27F0A0DA7C32"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"64EC3ABB-2B4B-41B2-8301-4F8CA15A06CD"}
 */
function nfx_defaultPermissions(){
	return "^";
}
