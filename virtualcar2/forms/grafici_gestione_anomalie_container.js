/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"FBB888EF-6028-4CB8-8F79-4DC7E6EA3C3D"}
 */
var checkDate = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D4606B50-7742-42A1-9A8A-B292AB7E2643"}
 */
var projects = null;

/**
 * @properties={typeid:35,uuid:"4F3348E2-6648-4B43-9AE0-256736582D8F",variableType:-4}
 */
var selected = null;

/**@type {String}
 * @properties={typeid:35,uuid:"1044458F-73EC-4F20-815B-600CCD2D22EF"}
 */
var serverName = databaseManager.getDataSourceServerName(controller.getDataSource());

/**
 * @properties={typeid:24,uuid:"7559D8BA-3BA3-4104-ABC4-BFAD92DFAEF7"}
 */
function nfx_getTitle() {
	return "Indicatori";
}

/**
 * @properties={typeid:24,uuid:"8CAA6A9A-9EAA-4D1E-9031-44C6BBEE193A"}
 */
function nfx_isProgram() {
	return true;
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"81D55D9F-8FE1-407D-AE16-05DDA998B27E"}
 */
function onDataChange(oldValue, newValue, event) {
	drawChart();
	return true;
}

/**
 * @properties={typeid:24,uuid:"DA00CF4A-E3B1-4AAB-B9B6-DFECA6DDF36D"}
 */
function getCheckDate(p) {
	//FS

	var query = "select (extract(year from DATA_RIFERIMENTO) * 12) + extract(month from DATA_RIFERIMENTO) from K8_DATE_RIFERIMENTO where PROGETTO = ? and TIPO = ?";
	var args = [p, checkDate];
	//dep	var date = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,1).getValue(1,1);
	var date = databaseManager.getDataSetByQuery(serverName, query, args, 1).getValue(1, 1);

	return date;
}

/**
 * @properties={typeid:24,uuid:"2B825645-184A-4DD6-8849-C6DAC1EB4467"}
 */
function getData(p) {
	if (!p) throw "Missing parameter";
	if (!forms[selected].query || !forms[selected].args) throw "Content query and/or args not detected";
	if (forms[selected].cumulative !== true && forms[selected].cumulative !== false) throw "Chart type not detected or not supported";
	var date = getCheckDate(p) || ( (new Date()).getFullYear() * 12) + 1;
	var query = forms[selected].query;
	var args = [date].concat(forms[selected].args).concat([p]);
	//FS
	//dep	var ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,-1);
	var ds = databaseManager.getDataSetByQuery(serverName, query, args, -1);

	var offsets = ds.getColumnAsArray(1);
	var counts = ds.getColumnAsArray(2);
	var added = new Array();
	for (var i = -44; i <= 26; i++) {
		if (offsets.indexOf(i) == -1) {
			offsets.push(i);
			added.push(i);
		}
	}
	offsets.sort(globals.utils_sortNumbers);
	for (var i1 = 0; i1 < added.length; i1++) {
		var index = offsets.indexOf(added[i1]);
		counts.splice(index, 0, 0);
	}
	var max = 0;
	if (forms[selected].cumulative) {
		for (var i2 = 1; i2 < counts.length; i2++) {
			counts[i2] += counts[i2 - 1];
		}
		max = counts[counts.length - 1];
	} else {
		for (var i3 = 1; i3 < counts.length; i3++) {
			if (counts[i3] > max) max = counts[i3];
		}
	}
	return (ds.getMaxRowIndex() > 0) ? { offsets: offsets, counts: counts, max: max } : null;
}

/**
 * @properties={typeid:24,uuid:"DCC0E471-E0F1-4976-9352-C6512D653D5E"}
 */
function setupChart() {

	//SAuc

	/** @type {java.awt.chart} */
	var chart = forms[selected].elements.chart;
	if (!chart) throw "Selected form must have a chart element called \"chart\"";
	chart.setZoomOn(true);

	/** @type {String} */
	var str = java.awt.Color.LIGHT_GRAY;
	chart.setChartBackground(str);

	/** @type {String} */
	var str2 = java.awt.Color.WHITE;
	chart.setChartBackground2(str2);

	chart.setAutoLabelSpacingOn(true);
	chart.setSampleLabelsOn(true);

	chart.setValueLinesOn(true);

	chart.setLegendOn(true);
	chart.setLegendPosition(2);
	chart.setLegendBoxSizeAsFont(true);

	chart.setSampleAxisRange(-44, 26);
	chart.setGridLines([0]);
	//FS
	/** @type {Array<String>} */
	var colorForGridLine = [java.awt.Color.BLACK];
	//dep	chart.setGridLineColors([java.awt.Color.BLACK]);
	chart.setGridLineColors(colorForGridLine);

	if (forms[selected].title) {
		chart.setTitleOn(true);
		chart.setTitle(forms[selected].title);
	}
}

/**
 * @properties={typeid:24,uuid:"87148AE0-22D9-4F1E-BD17-9527F2D3548F"}
 */
function drawChart() {
	/** @type {java.awt.chart} */
	var chart = forms[selected].elements.chart;
	if (!chart) throw "Selected form must have a chart element called \"chart\"";
	if (projects) {
		var p = projects.split("\n");
		var data = new Array();
		var legend = new Array();
		for (var i = 0; i < p.length; i++) {
			var d = getData(p[i]);
			if (d) {
				legend.push(p[i]);
				data.push(d);
			}
		}
		if (data.length > 0) {
			var max = 0;
			var index = null;
			chart.setSeriesCount(data.length);
			for (var i2 = 0; i2 < data[0].offsets.length; i2++) {
				if (data[0].offsets[i2] == 0) {
					index = i2;
					data[0].offsets[i2] = (checkDate) ? checkDate : data[0].offsets[i2];
				}
			}
			chart.setSampleLabels(data[0].offsets);
			chart.setSampleCount(data[0].offsets.length);
			for (var i3 = 0; i3 < data.length; i3++) {
				if (max < data[i3].max) {
					max = data[i3].max;
				}
				chart.setSampleValues(i3, data[i3].counts);
			}
			var n = Math.pow(10, Math.floor(Math.log(max) / Math.log(10)));
			var m = max - (max % n) + n;
			chart.setRange(0, m);
			for (var i4 = 0; i4 < data.length; i4++) {
				var t = data[i4].counts[index] / m;
				var x = ( (i4 % 2 == 0) ? 0.55 : 0.7) + (i4 / 100);
				var y = Math.abs(0.9 - t);
				chart.setLabel("label_" + i4, legend[i4] + ": " + data[i4].counts[index], /*x*/x, /*y*/y, i4, index);
			}
			chart.setLegendLabels(legend);
			chart.visible = true;
		} else {
			chart.visible = false;
		}
	} else {
		chart.visible = false;
	}
}
