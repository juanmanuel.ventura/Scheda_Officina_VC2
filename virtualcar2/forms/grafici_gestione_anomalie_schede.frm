dataSource:"db:/ferrari/k8_dummy",
items:[
{
anchors:15,
beanClassName:"com.objectplanet.chart.LineChart",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_18\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"com.objectplanet.chart.LineChart\"> \
  <void property=\"background\"> \
   <object class=\"java.awt.Color\"> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>255<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>150<\/int> \
    <int>150<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"chartData\"> \
   <void id=\"StringArray0\" property=\"seriesLabels\"/> \
   <void property=\"seriesLabels\"> \
    <object idref=\"StringArray0\"/> \
   <\/void> \
  <\/void> \
  <void id=\"StringArray1\" property=\"legendLabels\"/> \
  <void property=\"legendLabels\"> \
   <object idref=\"StringArray1\"/> \
  <\/void> \
  <void property=\"name\"> \
   <string>chart<\/string> \
  <\/void> \
  <void id=\"StringArray2\" property=\"seriesLabels\"/> \
  <void property=\"seriesLabels\"> \
   <object idref=\"StringArray2\"/> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"0,0",
name:"chart",
size:"150,150",
typeid:12,
uuid:"BF7F36EE-A54D-4844-9CB2-A37D67B12385"
},
{
height:150,
partType:5,
typeid:19,
uuid:"FCD972E9-524F-4CB3-924F-B2C12977824B"
}
],
name:"grafici_gestione_anomalie_schede",
navigatorID:"-1",
onLoadMethodID:"B83552FB-2BD3-462F-882C-BA5FA165607B",
onShowMethodID:"3B2A0623-F167-4E72-B935-02AAFD0FA2C2",
paperPrintScale:100,
showInMenu:true,
size:"150,150",
styleName:"keeneight",
typeid:3,
uuid:"1612D735-88EA-4872-9542-D84C85B0A536"