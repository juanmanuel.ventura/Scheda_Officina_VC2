/**
 * @properties={typeid:35,uuid:"359F7FD1-6642-4A02-9246-E0BD45519673",variableType:-4}
 */
var args = null;

/**
 * @properties={typeid:35,uuid:"7161383D-C062-41AB-91D8-55F13B5A53DF",variableType:-4}
 */
var container = null;

/**
 * @properties={typeid:35,uuid:"C32429BA-E757-4898-801F-1B2DC93BA9FC",variableType:-4}
 */
var cumulative = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C54CADAA-6EC5-42C9-847E-FA2D23394558"}
 */
var query = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A4E3B057-1F06-49E8-9C7E-1CE000A4B95F"}
 */
var title = null;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"B83552FB-2BD3-462F-882C-BA5FA165607B"}
 */
function onLoad(event) {
	container = forms.grafici_gestione_anomalie_container;
	query = "select offset,count(*) from (select ((extract(year from DATA_RILEVAZIONE) * 12) + extract(month from DATA_RILEVAZIONE)) - ? as offset from K8_ANOMALIE where TIPO_SCHEDA = ? and CODICE_MODELLO = ? and DATA_RILEVAZIONE is not null) where offset >= -44 and offset <= 26 group by offset order by offset";
	//Setto i parametri per la query TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	args = ["ANOMALIA"];
	title = "Schede";
	cumulative = true;
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"3B2A0623-F167-4E72-B935-02AAFD0FA2C2"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.drawChart();
}
