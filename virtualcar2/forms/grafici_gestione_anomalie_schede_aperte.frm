dataSource:"db:/ferrari/k8_dummy",
items:[
{
height:150,
partType:5,
typeid:19,
uuid:"14503918-6F77-40AF-8926-F2E7F6F33522"
},
{
anchors:15,
beanClassName:"com.objectplanet.chart.LineChart",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_18\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"com.objectplanet.chart.LineChart\"> \
  <void property=\"background\"> \
   <object class=\"java.awt.Color\"> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>255<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>150<\/int> \
    <int>150<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"chartData\"> \
   <void id=\"StringArray0\" property=\"seriesLabels\"/> \
   <void property=\"seriesLabels\"> \
    <object idref=\"StringArray0\"/> \
   <\/void> \
  <\/void> \
  <void id=\"StringArray1\" property=\"legendLabels\"/> \
  <void property=\"legendLabels\"> \
   <object idref=\"StringArray1\"/> \
  <\/void> \
  <void property=\"name\"> \
   <string>chart<\/string> \
  <\/void> \
  <void id=\"StringArray2\" property=\"seriesLabels\"/> \
  <void property=\"seriesLabels\"> \
   <object idref=\"StringArray2\"/> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"0,0",
name:"chart",
size:"150,150",
typeid:12,
uuid:"55AC3C67-7EB4-4B75-A9AA-5E437A5D46A1"
}
],
name:"grafici_gestione_anomalie_schede_aperte",
navigatorID:"-1",
onLoadMethodID:"658CF7D4-438A-49DD-B067-AF31B6C5C73C",
onShowMethodID:"8A897C03-4B8C-45F0-A77C-AE9CF5FE7E1B",
paperPrintScale:100,
showInMenu:true,
size:"150,150",
styleName:"keeneight",
typeid:3,
uuid:"BD11D008-63A1-4B8B-B187-1AB3AAD3E425"