/**
 * @properties={typeid:35,uuid:"AEA9CD50-00C7-4042-8C40-067CC5EE3560",variableType:-4}
 */
var args = null;

/**
 * @properties={typeid:35,uuid:"60ABE602-A637-44F8-A69F-B1337872530C",variableType:-4}
 */
var container = null;

/**
 * @properties={typeid:35,uuid:"2B1C4D7D-E45A-4430-96A7-82B05A3BDE22",variableType:-4}
 */
var cumulative = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F390BC75-28B1-41E0-A927-25E5914B35A1"}
 */
var query = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"556D66CF-8640-41D4-BEA3-D34263D509C7"}
 */
var title = null;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"658CF7D4-438A-49DD-B067-AF31B6C5C73C"}
 */
function onLoad(event) {
	container = forms.grafici_gestione_anomalie_container;
	query = "select offset,count(*) from (select ((extract(year from DATA_RILEVAZIONE) * 12) + extract(month from DATA_RILEVAZIONE)) - ? as offset from K8_ANOMALIE where TIPO_SCHEDA = ? and STATO_ANOMALIA = ? and CODICE_MODELLO = ? and DATA_RILEVAZIONE is not null) where offset >= -44 and offset <= 26 group by offset order by offset";
	args = ["ANOMALIA","APERTA"];
	title = "Schede - Aperte";
	cumulative = false;
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"8A897C03-4B8C-45F0-A77C-AE9CF5FE7E1B"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.drawChart();
}
