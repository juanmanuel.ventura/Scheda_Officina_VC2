dataSource:"db:/ferrari/k8_dummy",
items:[
{
anchors:15,
beanClassName:"com.objectplanet.chart.LineChart",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_18\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"com.objectplanet.chart.LineChart\"> \
  <void property=\"background\"> \
   <object class=\"java.awt.Color\"> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>255<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>150<\/int> \
    <int>150<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"chartData\"> \
   <void id=\"StringArray0\" property=\"seriesLabels\"/> \
   <void property=\"seriesLabels\"> \
    <object idref=\"StringArray0\"/> \
   <\/void> \
  <\/void> \
  <void id=\"StringArray1\" property=\"legendLabels\"/> \
  <void property=\"legendLabels\"> \
   <object idref=\"StringArray1\"/> \
  <\/void> \
  <void property=\"name\"> \
   <string>chart<\/string> \
  <\/void> \
  <void id=\"StringArray2\" property=\"seriesLabels\"/> \
  <void property=\"seriesLabels\"> \
   <object idref=\"StringArray2\"/> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"0,0",
name:"chart",
size:"150,150",
typeid:12,
uuid:"670F19CB-6232-4A54-B8D5-9DD30D6AB2C0"
},
{
height:150,
partType:5,
typeid:19,
uuid:"7688279F-0983-4856-9EFE-F543B50EF958"
}
],
name:"grafici_gestione_anomalie_schede_chiuse",
navigatorID:"-1",
onLoadMethodID:"6696EF92-1535-4003-883E-46757F6B06F5",
onShowMethodID:"8D288F48-366A-45DC-8EE5-13FD15E5F94B",
paperPrintScale:100,
showInMenu:true,
size:"150,150",
styleName:"keeneight",
typeid:3,
uuid:"6FC76404-CC55-4E61-8088-50980B2D6462"