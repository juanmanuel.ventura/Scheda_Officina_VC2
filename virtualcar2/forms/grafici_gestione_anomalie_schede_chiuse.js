/**
 * @properties={typeid:35,uuid:"7E740D89-B80D-4C52-A411-FEF009DF6AF4",variableType:-4}
 */
var args = null;

/**
 * @properties={typeid:35,uuid:"472E2D68-BC2C-4E20-BD7B-5AB91EE9F682",variableType:-4}
 */
var container = null;

/**
 * @properties={typeid:35,uuid:"64358E9D-AC7D-4EDF-A312-187573BEBA9A",variableType:-4}
 */
var cumulative = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"20A97BAB-A0F0-4818-B9A2-0A525CBBB95F"}
 */
var query = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"2655A898-1282-4C79-A76F-F4BFB7E48588"}
 */
var title = null;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"6696EF92-1535-4003-883E-46757F6B06F5"}
 */
function onLoad(event) {
	container = forms.grafici_gestione_anomalie_container;
	query = "select offset,count(*) from (select ((extract(year from DATA_RILEVAZIONE) * 12) + extract(month from DATA_RILEVAZIONE)) - ? as offset from K8_ANOMALIE where TIPO_SCHEDA = ? and STATO_ANOMALIA = ? and CODICE_MODELLO = ? and DATA_RILEVAZIONE is not null) where offset >= -44 and offset <= 26 group by offset order by offset";
	args = ["ANOMALIA","CHIUSA"];
	title = "Schede - Chiuse";
	cumulative = true;
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"8D288F48-366A-45DC-8EE5-13FD15E5F94B"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.drawChart();
}
