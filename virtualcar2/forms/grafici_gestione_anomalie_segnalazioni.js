/**
 * @properties={typeid:35,uuid:"F8FA6669-12F4-46C5-89E0-F91268433F71",variableType:-4}
 */
var args = null;

/**
 * @properties={typeid:35,uuid:"FC95D28C-D8E3-4F45-AF2B-82F59C0D1F8F",variableType:-4}
 */
var container = null;

/**
 * @properties={typeid:35,uuid:"80B7B54A-45D4-487F-9C76-F01441E0A947",variableType:-4}
 */
var cumulative = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"183F0842-92E9-4841-9C0C-990584EBFE44"}
 */
var query = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B94D5A8D-6075-42D8-A51A-2508304A8C8F"}
 */
var title = null;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"F45399E2-ED33-41BD-8A7E-E12F885DC916"}
 */
function onLoad(event) {
	container = forms.grafici_gestione_anomalie_container;
	query = "select offset,count(*) from (select ((extract(year from DATA_RILEVAZIONE) * 12) + extract(month from DATA_RILEVAZIONE)) - ? as offset from K8_ANOMALIE where TIPO_SCHEDA = ? and CODICE_MODELLO = ? and DATA_RILEVAZIONE is not null) where offset >= -44 and offset <= 26 group by offset order by offset";
	//Setto i parametri per la query TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	args = ["SEGNALAZIONE"];
	title = "Segnalazioni";
	cumulative = true;
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"ECE0B80C-EAAE-46AD-A401-28E32E7DB917"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.drawChart();
}
