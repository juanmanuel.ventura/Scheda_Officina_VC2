dataSource:"db:/ferrari/k8_dummy",
items:[
{
anchors:15,
beanClassName:"com.objectplanet.chart.LineChart",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_18\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"com.objectplanet.chart.LineChart\"> \
  <void property=\"background\"> \
   <object class=\"java.awt.Color\"> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>255<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>150<\/int> \
    <int>150<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"chartData\"> \
   <void id=\"StringArray0\" property=\"seriesLabels\"/> \
   <void property=\"seriesLabels\"> \
    <object idref=\"StringArray0\"/> \
   <\/void> \
  <\/void> \
  <void id=\"StringArray1\" property=\"legendLabels\"/> \
  <void property=\"legendLabels\"> \
   <object idref=\"StringArray1\"/> \
  <\/void> \
  <void property=\"name\"> \
   <string>chart<\/string> \
  <\/void> \
  <void id=\"StringArray2\" property=\"seriesLabels\"/> \
  <void property=\"seriesLabels\"> \
   <object idref=\"StringArray2\"/> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"0,0",
name:"chart",
size:"150,150",
typeid:12,
uuid:"59AA3068-145B-4BCD-AA39-D703CBD49667"
},
{
height:150,
partType:5,
typeid:19,
uuid:"72CE7417-3EBF-42A3-8AB3-AE86950AE096"
}
],
name:"grafici_gestione_anomalie_segnalazioni_aperte",
navigatorID:"-1",
onLoadMethodID:"CF596C10-857A-4B9A-97D4-454B32CBA0C5",
onShowMethodID:"002B8F3B-FF1A-4721-8799-FFB84C377D20",
paperPrintScale:100,
showInMenu:true,
size:"150,150",
styleName:"keeneight",
typeid:3,
uuid:"83BA21D2-25C9-4512-84DE-37145FEA78A7"