/**
 * @properties={typeid:35,uuid:"3344CF35-9887-4C5A-9EF9-E00D5CA79FF1",variableType:-4}
 */
var args = null;

/**
 * @properties={typeid:35,uuid:"769C6F54-D786-4752-8BEF-5C182015B63F",variableType:-4}
 */
var container = null;

/**
 * @properties={typeid:35,uuid:"26EE65F7-C5B9-4295-A72E-39CE021DA46A",variableType:-4}
 */
var cumulative = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A5CE1C74-7F1A-42CE-975E-354B01ED7610"}
 */
var query = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"79907081-CDF6-44CE-BFCD-F87206F69A44"}
 */
var title = null;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"CF596C10-857A-4B9A-97D4-454B32CBA0C5"}
 */
function onLoad(event) {
	container = forms.grafici_gestione_anomalie_container;
	query = "select offset,count(*) from (select ((extract(year from DATA_RILEVAZIONE) * 12) + extract(month from DATA_RILEVAZIONE)) - ? as offset from K8_ANOMALIE where TIPO_SCHEDA = ? and STATO_SEGNALAZIONE = ? and CODICE_MODELLO = ? and DATA_RILEVAZIONE is not null) where offset >= -44 and offset <= 26 group by offset order by offset";
	//Setto i parametri per la query TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	args = ["SEGNALAZIONE","APERTA"];
	title = "Segnalazioni - Aperte";
	cumulative = false;
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"002B8F3B-FF1A-4721-8799-FFB84C377D20"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.drawChart();
}
