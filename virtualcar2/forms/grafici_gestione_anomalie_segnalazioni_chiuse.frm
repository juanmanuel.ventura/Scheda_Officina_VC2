dataSource:"db:/ferrari/k8_dummy",
items:[
{
height:150,
partType:5,
typeid:19,
uuid:"1879394D-4C53-473D-BB5A-7399B7D0165F"
},
{
anchors:15,
beanClassName:"com.objectplanet.chart.LineChart",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_18\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"com.objectplanet.chart.LineChart\"> \
  <void property=\"background\"> \
   <object class=\"java.awt.Color\"> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>255<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>150<\/int> \
    <int>150<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"chartData\"> \
   <void id=\"StringArray0\" property=\"seriesLabels\"/> \
   <void property=\"seriesLabels\"> \
    <object idref=\"StringArray0\"/> \
   <\/void> \
  <\/void> \
  <void id=\"StringArray1\" property=\"legendLabels\"/> \
  <void property=\"legendLabels\"> \
   <object idref=\"StringArray1\"/> \
  <\/void> \
  <void property=\"name\"> \
   <string>chart<\/string> \
  <\/void> \
  <void id=\"StringArray2\" property=\"seriesLabels\"/> \
  <void property=\"seriesLabels\"> \
   <object idref=\"StringArray2\"/> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"0,0",
name:"chart",
size:"150,150",
typeid:12,
uuid:"365D3035-C47F-49F3-8405-57D44774C710"
}
],
name:"grafici_gestione_anomalie_segnalazioni_chiuse",
navigatorID:"-1",
onLoadMethodID:"24C5B183-2789-454A-8A8E-255D432D769B",
onShowMethodID:"071FA53A-BE50-4F47-B59A-EA9B5A409AE7",
paperPrintScale:100,
showInMenu:true,
size:"150,150",
styleName:"keeneight",
typeid:3,
uuid:"13D183A6-8988-49CB-92C2-35C127921237"