/**
 * @properties={typeid:35,uuid:"9FC6AD7F-8A66-4DA1-BB25-57EC4BD861B3",variableType:-4}
 */
var args = null;

/**
 * @properties={typeid:35,uuid:"95D5E962-7915-41C2-9D98-1D8806B3AF39",variableType:-4}
 */
var container = null;

/**
 * @properties={typeid:35,uuid:"4ACEA16E-7FD8-42A0-AAD5-379B11D2967F",variableType:-4}
 */
var cumulative = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"6254EDD1-193F-4683-A3FC-2D824954522A"}
 */
var query = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"49FA6944-8D43-4EFF-9D61-27276174952A"}
 */
var title = null;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"24C5B183-2789-454A-8A8E-255D432D769B"}
 */
function onLoad(event) {
	container = forms.grafici_gestione_anomalie_container;
	query = "select offset,count(*) from (select ((extract(year from DATA_RILEVAZIONE) * 12) + extract(month from DATA_RILEVAZIONE)) - ? as offset from K8_ANOMALIE where TIPO_SCHEDA = ? and STATO_SEGNALAZIONE = ? and CODICE_MODELLO = ? and DATA_RILEVAZIONE is not null) where offset >= -44 and offset <= 26 group by offset order by offset";
	//Setto i parametri per la query TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	args = ["SEGNALAZIONE","CHIUSA"];
	title = "Segnalazioni - Chiuse";
	cumulative = true;
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"071FA53A-BE50-4F47-B59A-EA9B5A409AE7"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.drawChart();
}
