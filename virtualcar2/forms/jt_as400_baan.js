/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"69C6F695-D675-439C-8BCA-77AA33A640BA"}
 */
var nfx_orderBy = "";

/**
 *
 * @properties={typeid:24,uuid:"E286EA83-F7D6-4249-AE5C-B44ACEBC3E4B"}
 */
function nfx_defineAccess()
{
	return [true, true, true];
}
