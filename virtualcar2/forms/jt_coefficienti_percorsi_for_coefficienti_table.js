/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1760DA4C-3A60-4E12-B5DD-5095F0E6CA20"}
 */
var nfx_orderBy = "percorso asc";

/**
 *
 * @properties={typeid:24,uuid:"B5E65FCA-08BA-43D2-9CA6-0554407488D2"}
 */
function nfx_getTitle()
{

	return "Percorsi - Tabella";
}
