/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"0A0E0417-594F-4732-871A-1ADFD911FC28"}
 */
var nfx_orderBy = "coefficiente asc";

/**
 *
 * @properties={typeid:24,uuid:"DABD1C45-1985-4769-946F-DA70505B06A5"}
 */
function nfx_getTitle()
{

	return "Coefficienti - Tabella";
}
