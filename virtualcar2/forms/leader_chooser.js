/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"8AE68D6A-2567-4C2F-A819-1C106C2490E3"}
 */
var callback = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"61049666-DF40-4B38-A052-F0BD75A69A00"}
 */
var field = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"744E3C81-8DC4-4453-A676-830BA670526C"}
 */
var form = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"34F8CE24-ACE1-405E-9D4E-30F82964DC89"}
 */
var model = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A5D99A24-AE25-4B35-AB4A-B82833294347"}
 */
var smart = "";

/**
 * @param {String} m
 * @param {String} fo
 * @param {String} fi
 * @param {String} [cb]
 * @properties={typeid:24,uuid:"4F384E87-D56C-44F4-BC4D-DC809B933BDD"}
 */
function openPopup(m,fo,fi,cb){
	setData(m,fo,fi,cb);
	controller.show("leaders");
}

/**
 * @param {String} m
 * @param {String} fo
 * @param {String} fi
 * @param {String} cb
 * @properties={typeid:24,uuid:"9A310102-21BA-478A-A344-0F5F9CE987B3"}
 */
function setData(m,fo,fi,cb){
	if(model != m || form != fo || field != fi){
		model = m;
		form = fo;
		field = fi;
		callback = cb;
		smart = "";
		filterData();
	}
	controller.readOnly = true;
	elements.smart.readOnly = false;
	
	var mode = forms.nfx_interfaccia_pannello_buttons.mode;
	elements.choose.enabled = (mode == "add" || mode == "edit") ? true : false;
}

/**
 * @properties={typeid:24,uuid:"455EAAC3-DE54-40D1-8B06-920F0E6A3EA5"}
 */
function filterData(){
	if(model){
		filterDataWrite();
	}else{
		filterDataRead();
	}
}

/**
 * @properties={typeid:24,uuid:"37D7367E-A529-430F-B33A-A6B0ECE63276"}
 * @AllowToRunInFind
 */
function filterDataWrite(){
	var filters = {progetto	:	model};

	if(foundset.find()){
		for(var f in filters){
			if(filters[f]){
				foundset[f] = filters[f];
			}
		}

		if(smart){
			foundset.leader = "#%" + smart + "%";
		}
		foundset.search();
	}
	elements.choose.enabled = (foundset.getSize()) ? true : false;
}

/**
 * @properties={typeid:24,uuid:"8FCE54A2-9CF9-497A-978C-7225628221C6"}
 * @AllowToRunInFind
 */
function filterDataRead(){
	if(foundset.find()){
		foundset.progetto = "#" + smart;
		if(!foundset.search() && foundset.find()){
			foundset.leader = "#%" + smart + "%";
			foundset.search();
		}
	}
}

/**@param {JSEvent} event
 * @properties={typeid:24,uuid:"3461A594-27D5-4150-895C-D318B869B02B"}
 */
function saveAndClose(event){
	forms[form][field] = leader;
	if(forms[form][callback] && typeof forms[form][callback] == "function") forms[form][callback]();
	//FS
//dep	if(application.isFormInDialog(controller.getName())){
	//application.output("Tipo finestra : "+	application.getActiveWindow().getType());
		if(application.getActiveWindow().getType() == JSWindow.DIALOG){
		//application.closeFormDialog("leaders");
		
		var w=controller.getWindow();
		w.destroy();
	}
}
