/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"780DE1FE-52C3-4FF1-BA01-45CCD8F79551"}
 */
var nfx_orderBy = "valuelist_name asc";

/**
 *
 * @properties={typeid:24,uuid:"F240CD57-AB7A-4F1F-8CD4-3348170F2455"}
 */
function nfx_defineAccess()
{
	return [true,true,true];
}
