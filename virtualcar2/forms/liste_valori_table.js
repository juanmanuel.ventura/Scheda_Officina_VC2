/**
 *
 * @properties={typeid:24,uuid:"B649FBDD-9F26-408F-BA67-1B235A632C3D"}
 */
function nfx_getTitle()
{
	return "Liste Valori - Tabella";
}

/**
 *
 * @properties={typeid:24,uuid:"A1A23CCE-A881-4058-A064-E76C5A1336D8"}
 */
function nfx_isProgram()
{
	return true;
}
