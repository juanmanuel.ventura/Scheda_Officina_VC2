/**
 * @properties={typeid:24,uuid:"CBC2E3FD-3069-4777-A855-4DBC40F55098"}
 */
function nfx_getTitle()
{
	return "MV (BETA)"; 
}

/**
 * @properties={typeid:24,uuid:"C074ED55-122E-4BAD-B9E8-278863FE9B22"}
 */
function nfx_isProgram()
{
	return true;
}

/**
 * @properties={typeid:24,uuid:"A3A83988-2CE5-42EF-BF8D-7554FE7E5880"}
 */
function writeMessage(message)
{

	
	elements.message.text = message;
	elements.message_shadow.text = message;
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"0C65EC95-7169-4E24-B2BD-B4A811E47EAB"}
 */
function onProgettoChange(oldValue, newValue, event) {
	globals.vc2_currentVersione = null;
	onVersioneChange(oldValue, newValue, event);
	globals.vc2_currentVettura = null;
	onVetturaChange(oldValue, newValue, event);
	return true;
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"D3271AF7-6094-4811-BAA5-E5CB438078FD"}
 */
function onVersioneChange(oldValue, newValue, event) {
	forms.montaggio_vetture_progetto_s5.setRoot(k8_dummy_to_k8_distinte_progetto_core$root.descrizione,
												k8_dummy_to_k8_distinte_progetto_core$root.path_id,
												k8_dummy_to_k8_distinte_progetto_core$root.numero_disegno);
	globals.vc2_currentVettura = null;
	onVetturaChange(oldValue, newValue, event);
	return true;
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"0B16F99D-3C62-45F5-803D-23F366AA9A2E"}
 */
function onVetturaChange(oldValue, newValue, event) {
	forms.montaggio_vetture_vettura_s5.setRoot(k8_dummy_to_k8_distinte_vetture$root.descrizione,
											   k8_dummy_to_k8_distinte_vetture$root.path_id,
											   k8_dummy_to_k8_distinte_vetture$root.numero_disegno);
	return true;
}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"8F299D7B-5831-4F52-9D1F-A1BD04D1409A"}
 */
function onLoad(event) {
	globals.utils_setSplitPane({splitpane : elements.mv_split,
								leftComponent : elements.progetto,
								rightComponent : elements.vettura,
								orientation : 1,
//FS
//dep							dividerLocation: (application.getWindowWidth() - 260) / 2});
								dividerLocation: (application.getWindow().getWidth() - 260) / 2});
}
