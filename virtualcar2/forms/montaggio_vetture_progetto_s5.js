/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"37B23147-0A90-465A-B35A-3D185042F7AB"}
 */
var filter = "";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"2A76FA57-2FB5-402D-9DFA-3343A556A5A9",variableType:4}
 */
var rootNumber = 0;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C5C63ED9-3CEB-4651-BDBF-0B6DCF4C56F4"}
 */
var rootPathID = "";

/**
 * @properties={typeid:24,uuid:"2F7E81D9-CE74-44E7-A94E-78129323C829"}
 */
function workWrap() {
	time(work);
}

/**
 * @properties={typeid:24,uuid:"2E09C670-B2A3-42B4-98C0-E24D7453BE2E"}
 */
function work() {
	if (status != "O") {
		if (status == "+") {
			expand();
			status = "-";
		} else {
			collapse();
			status = "?";
		}
	}
}

/**
 * @properties={typeid:24,uuid:"D83AF9F9-2561-4DC1-BC81-DF411784D9DB"}
 * @AllowToRunInFind
 */
function expand() {
	var index = controller.getSelectedIndex();
	var cv = codice_vettura;
	var nd = numero_disegno;
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		ufs.codice_vettura = cv;
		ufs.codice_padre = nd;
		ufs.search(false, false);
	}
	controller.loadRecords(ufs);
	controller.setSelectedIndex(index);
}

/**
 * @properties={typeid:24,uuid:"7953C7B0-A02B-43C7-BF76-BB6F57C5A88B"}
 * @AllowToRunInFind
 */
function collapse() {
	var index = controller.getSelectedIndex();
	var pid = path_id;
	var ufs = foundset.unrelate();
	for (var i = 1; i <= ufs.getSize(); i++) {
		var record = ufs.getRecord(i);
		if (record.path_id.search(pid) != -1) record.status = "?";
	}
	if (ufs.find()) {
		ufs.path_id = "!" + pid + "/%";
		ufs.search(false, true);
	}
	controller.loadRecords(ufs);
	controller.setSelectedIndex(index);
}

/**
 * @properties={typeid:24,uuid:"C42CA870-B804-4215-9AC2-68AE925A26C3"}
 */
function isolateWrap() {
	if (globals.vc2_currentVersione) {
		time(isolate);
	}
}

/**
 * @properties={typeid:24,uuid:"F06C014A-8DE9-4AF0-990E-07AD22B0327E"}
 * @AllowToRunInFind
 */
function isolate() {
	var pid = path_id;
	var d = descrizione;
	var cv = codice_vettura;
	var nd = numero_disegno;
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		ufs.codice_vettura = cv;
		ufs.codice_padre = nd;
		ufs.search();
	}
	controller.loadRecords(ufs);
	setRoot(d, pid, nd);
}

/**
 * @properties={typeid:24,uuid:"09B30AB9-5D33-452D-855C-70E2EBFDF1CE"}
 */
function releaseWrap() {
	if (globals.vc2_currentVersione) {
		time(release);
	}
}

/**
 * @properties={typeid:24,uuid:"E2E29891-022A-48B2-9430-BFAD9DB1AE6A"}
 */
function release() {
	/** @type {JSFoundSet} */
	var fs = k8_dummy_to_k8_distinte_progetto_core;
	for (var i = 1; i <= fs.getSize(); i++) {
		var record = fs.getRecord(i);
		record.status = "?";
	}
	databaseManager.recalculate(fs);
	controller.loadRecords(fs);
	setRoot(k8_dummy_to_k8_distinte_progetto_core$root.descrizione,
		k8_dummy_to_k8_distinte_progetto_core$root.path_id,
		k8_dummy_to_k8_distinte_progetto_core$root.numero_disegno);
	controller.sort("path_id asc");
}

/**
 * @properties={typeid:24,uuid:"B12EA782-7DBF-4047-A656-31AA5DF9BD1B"}
 */
function filterTreeWrap() {
	if (globals.vc2_currentVersione) {
		time(filterTree);
	}
}

/**
 * @properties={typeid:24,uuid:"97F25A84-CE7F-4CA3-BB3E-FD661B8D9B52"}
 * @AllowToRunInFind
 */
function filterTree() {
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		if (filter) {
			ufs.path_id = rootPathID + "/%";
			try {
				ufs.numero_disegno = java.lang.Double.parseDouble(filter);
			} catch (e) {
				ufs.descrizione = "#" + filter;
			}
		} else {
			ufs.codice_vettura = globals.vc2_currentVersione;
			ufs.codice_padre = rootNumber;
		}
		ufs.search();
	}
	controller.loadRecords(ufs);
}

/**
 * @properties={typeid:24,uuid:"C634FD49-AF30-4A9F-BD7E-E73F368BCD09"}
 */
function setRoot(label, pid, number) {
	elements.root.text = label;
	rootPathID = pid;
	rootNumber = number
}

/**
 * @properties={typeid:24,uuid:"D25A67AC-65EA-4BA3-A0AD-96D200F70CDA"}
 */
function time(func) {
	//tempo di esecuzione di una funzione senza paramenti
	if (func && typeof func == "function") {
		forms.montaggio_vetture_container_s5.writeMessage("...operazione in corso");
		application.updateUI();
		var start = new Date();
		var result = func();
		var end = new Date();
		forms.montaggio_vetture_container_s5.writeMessage( ( (end - start) / 1000).toFixed(2) + "s");

	}
	return result;
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"88D053F4-1AFE-4551-843C-1E125311BE00"}
 */
function onShowForm(firstShow, event) {
	foundset.multiSelect = true;
}

/**
 * Handle start of a drag, it can set the data that should be transfered and should return a constant which dragndrop mode/modes is/are supported.
 *
 * Should return a DRAGNDROP constant or a combination of 2 constants:
 * DRAGNDROP.MOVE if only a move can happen,
 * DRAGNDROP.COPY if only a copy can happen,
 * DRAGNDROP.MOVE|DRAGNDROP.COPY if a move or copy can happen,
 * DRAGNDROP.NONE if nothing is supported (drag should start).
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Number} DRAGNDROP.MOVE one of the DRAGNDROP constant (or a combination)
 *
 * @properties={typeid:24,uuid:"924F68BC-37BC-40B1-986E-3846113A33EF"}
 */
function onDrag(event) {
	var src = event.getSource();
	if (src) {
		event.data = path_id;
		return DRAGNDROP.MOVE;
	}
	return DRAGNDROP.NONE
}

/**
 * Called before the form component is rendered.
 *
 * @param {JSRenderEvent} event the render event
 *
 * @private
 *
 * @properties={typeid:24,uuid:"6DCC3D68-B9DC-4708-B6AD-5E83E826B5C9"}
 */
function onRender(event) {
	if(event.isRecordSelected() && event.getRenderable().getName()!='vc2_currentVersione' && event.getRenderable().getName()!='root' && event.getRenderable().getName()!='filter'&& event.getRenderable().getName()!='TreeIcon')
	{
		event.getRenderable().fgcolor = '#339eff';		
		event.getRenderable().font='Verdana,1,11';
	}
	else{
		event.getRenderable().fgcolor = '#000000';
	}
}
