/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"306EE0DE-5026-445D-930A-9A94808590BC"}
 */
var filter = "";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"5507A8B0-42F7-43CA-B87F-8C8AE6E2821D",variableType:4}
 */
var rootNumber = 0;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1A001443-F0B4-4FD6-8A06-8E2C68D009BB"}
 */
var rootPathID = "";

/**@type {String}
 * @properties={typeid:35,uuid:"E8CF567E-2F86-4B5D-AAB5-473BF18B0583"}
 */
var serverName = databaseManager.getDataSourceServerName(controller.getDataSource());

/**@type {String}
 * @properties={typeid:35,uuid:"15625557-E3A3-44EB-A906-AB54D82195ED"}
 */
var table = databaseManager.getDataSourceTableName(controller.getDataSource());

/**
 * @properties={typeid:24,uuid:"73E3BE9B-96F3-4CFB-9187-33AAFFEBCF7D"}
 */
function workWrap() {
	time(work);
}

/**
 * @properties={typeid:24,uuid:"85F63555-F225-486A-9560-335F7827A573"}
 */
function work() {
	if (status != "O") {
		if (status == "+") {
			expand();
			status = "-";
		} else {
			collapse();
			status = "?";
		}
	}
}

/**
 * @properties={typeid:24,uuid:"E52F3BFF-D24B-4BB1-B7BA-43EC42C7D9F0"}
 * @AllowToRunInFind
 */
function expand() {
	var index = controller.getSelectedIndex();
	var cv = versione;
	var nd = numero_disegno;
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		ufs.versione = cv;
		ufs.codice_padre = nd;
		ufs.search(false, false);
	}
	controller.loadRecords(ufs);
	controller.setSelectedIndex(index);
}

/**
 * @properties={typeid:24,uuid:"5384AF9B-FF59-4FA1-84D1-A23EE88CCF03"}
 * @AllowToRunInFind
 */
function collapse() {
	var index = controller.getSelectedIndex();
	var pid = path_id;
	var ufs = foundset.unrelate();
	for (var i = 1; i <= ufs.getSize(); i++) {
		var record = ufs.getRecord(i);
		if (record.path_id.search(pid) != -1) record.status = "?";
	}
	if (ufs.find()) {
		ufs.path_id = "!" + pid + "/%";
		ufs.search(false, true);
	}
	controller.loadRecords(ufs);
	controller.setSelectedIndex(index);
}

/**
 * @properties={typeid:24,uuid:"C435CD08-218E-4094-9D94-62BE62656630"}
 */
function isolateWrap() {
	if (globals.vc2_currentVettura) {
		time(isolate);
	}
}

/**
 * @properties={typeid:24,uuid:"761B4F7F-867A-4965-BAFE-4E40E2EE2B78"}
 * @AllowToRunInFind
 */
function isolate() {
	var pid = path_id;
	var d = descrizione;
	var cv = versione;
	var nd = numero_disegno;
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		ufs.versione = cv;
		ufs.codice_padre = nd;
		ufs.search();
	}
	controller.loadRecords(ufs);
	setRoot(d, pid, nd);
}

/**
 * @properties={typeid:24,uuid:"77EA1479-CF3D-4152-B870-7B0711F772D6"}
 */
function releaseWrap() {
	if (globals.vc2_currentVettura) {
		time(release);
	}
}

/**
 * @properties={typeid:24,uuid:"6F2CCE80-C163-4E18-91E2-D606E9F8DE96"}
 */
function release() {
	/** @type {JSFoundSet} */
	var fs = k8_dummy_to_k8_distinte_vetture;
	for (var i = 1; i <= fs.getSize(); i++) {
		var record = fs.getRecord(i);
		record.status = "?";
	}
	databaseManager.recalculate(fs);
	controller.loadRecords(fs);
	setRoot(k8_dummy_to_k8_distinte_vetture$root.descrizione,
		k8_dummy_to_k8_distinte_vetture$root.path_id,
		k8_dummy_to_k8_distinte_vetture$root.numero_disegno);
	controller.sort("path_id asc");
}

/**
 * @properties={typeid:24,uuid:"11622613-C1C2-45F4-AA6D-546FBAF5DF86"}
 */
function filterTreeWrap() {
	if (globals.vc2_currentVettura) {
		time(filterTree);
	}
}

/**
 * @properties={typeid:24,uuid:"6B4A8DC6-2216-49D6-B018-87E5E017C132"}
 * @AllowToRunInFind
 */
function filterTree() {
	var ufs = foundset.unrelate();
	if (ufs.find()) {
		if (filter) {
			ufs.path_id = rootPathID + "/%";
			try {
				ufs.numero_disegno = java.lang.Double.parseDouble(filter);
			} catch (e) {
				ufs.descrizione = "#" + filter;
			}
		} else {
			ufs.versione = globals.vc2_currentVersione;
			ufs.codice_padre = rootNumber;
		}
		ufs.search();
	}
	controller.loadRecords(ufs);
}

/**
 * @properties={typeid:24,uuid:"BF2D1425-9AD6-43F9-9962-9F06C4123E58"}
 */
function setRoot(label, pid, number) {
	elements.root.text = label;
	rootPathID = pid;
	rootNumber = number
}

/**
 * @properties={typeid:24,uuid:"13B2A0D4-7737-4C62-AE34-81DEC946DE0C"}
 */
function time(func) {
	//tempo di esecuzione di una funzione senza paramenti
	if (func && typeof func == "function") {
		forms.montaggio_vetture_container_s5.writeMessage("...operazione in corso");
		application.updateUI();
		var start = new Date();
		var result = func();
		var end = new Date();
		forms.montaggio_vetture_container_s5.writeMessage( ( (end - start) / 1000).toFixed(2) + "s");
	}
	return result;
}

/**
 * Handle a drag over.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} true if a drop can happen on the element
 *
 * @properties={typeid:24,uuid:"6101CEAF-BAD4-4C8B-8F10-4ECE6176904E"}
 */
function onDragOver(event) {
	var source = event.getSource();
	if (globals.vc2_currentVettura && source && event.data) {
		return true;
	}
	return false;
}

/**
 * Handle a drop.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 *
 * @properties={typeid:24,uuid:"0C123D73-6C47-4FF4-910C-6C59F9DCC011"}
 */
function onDrop(event) {
	forms.montaggio_vetture_container_s5.writeMessage("...operazione in corso");
	application.updateUI();
	var start = new Date();

	if (k8_dummy_to_k8_distinte_vetture$root.getSize() == 0) {
		createVettura();
	}
	checkRoots(event.data);
	var query = "select K8_DISTINTE_PROGETTO_ID,PROGETTO,CODICE_VETTURA,NUMERO_DISEGNO,ESPONENTE,DESCRIZIONE,MATRICOLA,RESPONSABILE,SIGNIFICATIVITA,AFFIDABILITA,LIVELLO,CODICE_PADRE,SEQUENZA,TIPO_ALLESTIMENTO from K8_DISTINTE_PROGETTO where PATH_ID like ? order by PATH_ID asc";
	var args = [event.data + "%"];
	//FS
	//dep	var ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,-1);
	var ds = databaseManager.getDataSetByQuery(serverName, query, args, -1);
	var size = ds.getMaxRowIndex();
	var data = { ds: ds, max: size };
	insertNodes(data);
	databaseManager.refreshRecordFromDatabase(k8_dummy_to_k8_distinte_vetture, -1);
	controller.loadRecords(k8_dummy_to_k8_distinte_vetture);
	var end = new Date();
	forms.montaggio_vetture_container_s5.writeMessage( ( (end - start) / 1000).toFixed(2) + "s");
}

/**
 * @properties={typeid:24,uuid:"5DBB2B52-E57F-43A7-BEAC-35097D0A8E05"}
 */
function createVettura() {
	var query = null;
	var args = null;

	query = "select K8_DISTINTE_PROGETTO_ID,PROGETTO,CODICE_VETTURA,NUMERO_DISEGNO,ESPONENTE,DESCRIZIONE,MATRICOLA,RESPONSABILE,SIGNIFICATIVITA,AFFIDABILITA,LIVELLO,SEQUENZA,TIPO_ALLESTIMENTO from K8_DISTINTE_PROGETTO where NUMERO_DISEGNO = ?";
	args = [globals.vc2_currentVersione];
	//FS
	//dep	var ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,1);
	var ds = databaseManager.getDataSetByQuery(serverName, query, args, 1);
	var p = ds.getValue(1, 2);
	var nd = ds.getValue(1, 4);
	var cp = null;
	var s = ds.getValue(1, 12);
	var ta = ds.getValue(1, 13);
	query = "insert into K8_DISTINTE_VETTURE (FK_DISTINTE_PROGETTO,PROGETTO,VERSIONE,NUMERO_DISEGNO,ESPONENTE,DESCRIZIONE,MATRICOLA,RESPONSABILE,SIGNIFICATIVITA,AFFIDABILITA,LIVELLO,SEQUENZA,TIPO_ALLESTIMENTO,VETTURA,DATA_MONTAGGIO,CONTEGGIO_FIGLI,TIMESTAMP_CREAZIONE,UTENTE_CREAZIONE) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,sysdate,0,sysdate,?)";
	args = ds.getRowAsArray(1).concat([globals.vc2_currentVettura, globals.nfx_user]);
	//FS
	//dep	var result = plugins.rawSQL.executeSQL(controller.getServerName(),
	//		   controller.getTableName(),
	//		   query,args);
	var result = plugins.rawSQL.executeSQL(serverName,
		table,
		query, args);
	if (result) {
		query = "update K8_DISTINTE_VETTURE set PATH_ID = '/'||K8_DISTINTE_VETTURE_ID where NUMERO_DISEGNO = ?";
		args = [globals.vc2_currentVersione];
		//dep		plugins.rawSQL.executeSQL(controller.getServerName(),
		//			  controller.getTableName(),
		//			  query,args);

		plugins.rawSQL.executeSQL(serverName,
			table,
			query, args);
		copyCoefficienti(p, nd, cp, s, ta);
		databaseManager.refreshRecordFromDatabase(k8_dummy_to_k8_distinte_vetture$root, -1);
		setRoot(k8_dummy_to_k8_distinte_vetture$root.descrizione,
			k8_dummy_to_k8_distinte_vetture$root.path_id,
			k8_dummy_to_k8_distinte_vetture$root.numero_disegno);
		controller.loadRecords(k8_dummy_to_k8_distinte_vetture);
	} else {
		application.output(plugins.rawSQL.getException().getMessage());
	}
}

/**
 * @properties={typeid:24,uuid:"08D1D07E-5828-4F67-BA6B-CD5F6B67354E"}
 */
function insertNodes(data) {
	//FS
	/** @type {JSDataSet} */
	var ds = data.ds;
	var max = data.max;
	var line = null;
	globals.nfx_progressBarShow(max);
	for (var i = 1; i <= max; i++) {
		line = ds.getRowAsArray(i);
		insertNode(line);
		globals.nfx_progressBarStep();
	}
}

/**
 * @properties={typeid:24,uuid:"1DD3BA19-B8B0-4FEF-AA1C-8E2274A6CED8"}
 */
function insertNode(line) {
	var query = null;
	var args = null;

	query = "select count(*) from K8_DISTINTE_VETTURE where VERSIONE = ? and VETTURA = ? and  NUMERO_DISEGNO = ? and CODICE_PADRE = ? and SEQUENZA = ? and TIPO_ALLESTIMENTO = ?";
	args = [line[2], globals.vc2_currentVettura, line[3], line[11], line[12], line[13]];
	//FS
	//dep	var ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,1);
	var ds = databaseManager.getDataSetByQuery(serverName, query, args, 1);
	if (ds.getValue(1, 1) == 0) {
		query = "insert into K8_DISTINTE_VETTURE (FK_DISTINTE_PROGETTO,PROGETTO,VERSIONE,NUMERO_DISEGNO,ESPONENTE,DESCRIZIONE,MATRICOLA,RESPONSABILE,SIGNIFICATIVITA,AFFIDABILITA,LIVELLO,CODICE_PADRE,SEQUENZA,TIPO_ALLESTIMENTO,VETTURA,DATA_MONTAGGIO,CONTEGGIO_FIGLI,TIMESTAMP_CREAZIONE,UTENTE_CREAZIONE) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,sysdate,0,sysdate,?)";
		args = line.concat([globals.vc2_currentVettura, globals.nfx_user]);
		//dep		var result = plugins.rawSQL.executeSQL(controller.getServerName(),
		//											   controller.getTableName(),
		//											   query,args);

		var result = plugins.rawSQL.executeSQL(serverName,
			table,
			query, args);
		if (result) {
			query = "update K8_DISTINTE_VETTURE set CONTEGGIO_FIGLI = CONTEGGIO_FIGLI + 1 where VERSIONE = ? and VETTURA = ? and NUMERO_DISEGNO = ?";
			args = [globals.vc2_currentVersione, globals.vc2_currentVettura, line[11]];
			//dep			plugins.rawSQL.executeSQL(controller.getServerName(),
			//									  controller.getTableName(),
			//									  query,args);

			plugins.rawSQL.executeSQL(serverName,
				table,
				query, args);
			query = "update K8_DISTINTE_VETTURE set PATH_ID = concat((select PATH_ID from K8_DISTINTE_VETTURE where VERSIONE = ? and VETTURA = ? and NUMERO_DISEGNO = ?),'/'||K8_DISTINTE_VETTURE_ID) where VERSIONE = ? and VETTURA = ? and NUMERO_DISEGNO = ? and CODICE_PADRE = ? and SEQUENZA = ? and TIPO_ALLESTIMENTO = ?";
			args = [globals.vc2_currentVersione, globals.vc2_currentVettura, line[11], globals.vc2_currentVersione, globals.vc2_currentVettura, line[3], line[11], line[12], line[13]];
			//dep			plugins.rawSQL.executeSQL(controller.getServerName(),
			//									  controller.getTableName(),
			//									  query,args);

			plugins.rawSQL.executeSQL(serverName,
				table,
				query, args);
			copyCoefficienti(line[1], line[3], line[11], line[12], line[13]);
		} else {
			application.output(plugins.rawSQL.getException().getMessage());
		}
	}
}

/**
 * @properties={typeid:24,uuid:"03FD134F-DA53-411C-BD9F-D384467495BC"}
 */
function checkRoots(idsStr) {
	var query = null;
	var args = null;
	//FS
	/** @type {JSDataSet} */
	var ds = null;

	/** @type {String} */

	var ids = idsStr.split("/");
	for (var i = 2; i < ids.length - 1; i++) {
		query = "select NUMERO_DISEGNO,CODICE_PADRE,SEQUENZA,TIPO_ALLESTIMENTO from K8_DISTINTE_PROGETTO where K8_DISTINTE_PROGETTO_ID = ?";
		args = [ids[i]];
		//dep		ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,1);
		ds = databaseManager.getDataSetByQuery(serverName, query, args, 1);

		query = "select count(*) from K8_DISTINTE_VETTURE where VERSIONE = ? and VETTURA = ? and NUMERO_DISEGNO = ? and CODICE_PADRE = ? and SEQUENZA = ? and TIPO_ALLESTIMENTO = ?";
		args = [globals.vc2_currentVersione, globals.vc2_currentVettura, ds.getValue(1, 1), ds.getValue(1, 2), ds.getValue(1, 3), ds.getValue(1, 4)];
		//dep		ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,1);
		ds = databaseManager.getDataSetByQuery(serverName, query, args, 1);

		if (ds.getValue(1, 1) === 0) {
			query = "select K8_DISTINTE_PROGETTO_ID,PROGETTO,CODICE_VETTURA,NUMERO_DISEGNO,ESPONENTE,DESCRIZIONE,MATRICOLA,RESPONSABILE,SIGNIFICATIVITA,AFFIDABILITA,LIVELLO,CODICE_PADRE,SEQUENZA,TIPO_ALLESTIMENTO from K8_DISTINTE_PROGETTO where K8_DISTINTE_PROGETTO_ID = ?";
			args = [ids[i]];
			//dep			ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,1);
			ds = databaseManager.getDataSetByQuery(serverName, query, args, 1);

			insertNode(ds.getRowAsArray(1));
		}
	}
}

/**
 * @properties={typeid:24,uuid:"FEAC5A33-9C11-4F86-B2A3-21A9BC2D4012"}
 */
function copyCoefficienti(p, nd, cp, s, ta) {
	var query = null;
	var args = null;

	query = "select COEFFICIENTE,PROGETTO,NUMERO_DISEGNO,NOTE from K8_DISTINTE_PROGETTO_COEFF where PROGETTO = ? and NUMERO_DISEGNO = ?";
	args = [p, nd];
	//FS

	//dep	var ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,-1);
	var ds = databaseManager.getDataSetByQuery(serverName, query, args, -1);
	var max = ds.getMaxRowIndex();
	if (max > 0) {
		query = "select K8_DISTINTE_VETTURE_ID from K8_DISTINTE_VETTURE where VERSIONE = ? and VETTURA = ? and NUMERO_DISEGNO = ? and CODICE_PADRE = ? and SEQUENZA = ? and TIPO_ALLESTIMENTO = ?";
		args = [globals.vc2_currentVersione, globals.vc2_currentVettura, nd, cp, s, ta];
		//dep		var id = databaseManager.getDataSetByQuery(controller.getServerName(),query,args,1).getValue(1,1);
		var id = databaseManager.getDataSetByQuery(serverName, query, args, 1).getValue(1, 1);

		query = "insert into K8_DISTINTE_VETTURE_COEFF (COEFFICIENTE,PROGETTO,NUMERO_DISEGNO,NOTE,FK_DISTINTE_VETTURE,UTENTE_CREAZIONE,TIMESTAMP_CREAZIONE,VETTURA) values (?,?,?,?,?,?,sysdate,?)";
		for (var i = 1; i <= max; i++) {
			args = ds.getRowAsArray(i).concat([id, globals.nfx_user, globals.vc2_currentVettura]);
			//dep			var result = plugins.rawSQL.executeSQL(controller.getServerName(),
			//												   forms.coefficienti_distinte_vetture.controller.getTableName(),
			//												   query,args);

			var result = plugins.rawSQL.executeSQL(serverName,
				forms.coefficienti_distinte_vetture[databaseManager.getDataSourceTableName(controller.getDataSource())],
				query, args);
			if (!result) application.output(plugins.rawSQL.getException().getMessage());
		}
	}
}

/**
 * Called before the form component is rendered.
 *
 * @param {JSRenderEvent} event the render event
 *
 * @private
 *
 * @properties={typeid:24,uuid:"2CBAC812-0FCA-4086-9753-6B9E086E808E"}
 */
function onRender(event) {
	if(event.isRecordSelected() && event.getRenderable().getName()!='vc2_currentVettura' && event.getRenderable().getName()!='root' && event.getRenderable().getName()!='filter'&& event.getRenderable().getName()!='TreeIcon')
	{
		event.getRenderable().fgcolor = '#339eff';		
		event.getRenderable().font='Verdana,1,11';
	}
	else{
		event.getRenderable().fgcolor = '#000000';
	}
}
