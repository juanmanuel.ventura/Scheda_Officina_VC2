/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"24372F4A-EE19-451F-BD97-DD486ADC3C66"}
 */
var nfx_orderBy = "descrizione asc";

/**
 *
 * @properties={typeid:24,uuid:"046B4A3A-F02D-480B-952E-E42420B3C010"}
 */
function nfx_defineAccess()
{
	return [true,true,true];
}
