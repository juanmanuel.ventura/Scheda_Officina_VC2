/**
 *
 * @properties={typeid:24,uuid:"1EB5EF99-B9D3-498D-9DEC-93919C94F287"}
 */
function nfx_getTitle()
{
	return "Parametrica Date - Tabella";
}

/**
 *
 * @properties={typeid:24,uuid:"14EE1CBB-577C-43E7-A318-D21D90840CBE"}
 */
function nfx_isProgram()
{
	return true;
}
