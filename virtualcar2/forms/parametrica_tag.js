/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"E6C2DD0E-D2F1-41B3-AAF6-2B2F9D6D3E77"}
 */
var nfx_orderBy = "tag asc";

/**
 *
 * @properties={typeid:24,uuid:"E8FAB953-A498-43C7-A836-562D2DA70BE9"}
 */
function nfx_defineAccess()
{
	return [true,true,true];
}
