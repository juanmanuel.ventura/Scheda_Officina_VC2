dataSource:"db:/ferrari/k8_parametrica_tag",
extendsID:"7AAB8627-1F6C-4098-B9D9-AF38ABBFADD3",
items:[
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"tag",
location:"310,30",
name:"tag",
size:"290,20",
styleClass:"table",
text:"Tag",
typeid:4,
uuid:"02D0DF6D-23FC-457B-A95C-A0AC0E7B1CD9"
},
{
labelFor:"valore3",
location:"910,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Valore 3",
typeid:7,
uuid:"151C9528-E901-4FF5-A338-12D1C8509803"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"valore2",
location:"760,30",
name:"valore2",
size:"140,20",
styleClass:"table",
text:"Valore2",
typeid:4,
uuid:"1623CFCC-13ED-4CE2-9534-D20712CEEE14"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"valore1",
location:"610,30",
name:"valore1",
size:"140,20",
styleClass:"table",
text:"Valore1",
typeid:4,
uuid:"27B32CF6-9C03-4F63-B04F-B6DBEE45314E"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"valore3",
location:"910,30",
name:"valore3",
size:"150,20",
styleClass:"table",
text:"Valore3",
typeid:4,
uuid:"52535E98-EB4D-4222-8034-E898EDBC8D1C"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"tabella",
location:"10,30",
name:"tabella",
size:"290,20",
styleClass:"table",
text:"Tabella",
typeid:4,
uuid:"66B8CE71-51DD-4828-BF37-E2F1996CC02A"
},
{
labelFor:"valore1",
location:"610,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Valore 1",
typeid:7,
uuid:"6A9591BA-47E4-49B2-B498-3C8B21A873AA"
},
{
labelFor:"tabella",
location:"10,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Tabella",
typeid:7,
uuid:"A3C9D601-84EB-42E6-B9EF-825FAF9B5A6C"
},
{
height:480,
partType:5,
typeid:19,
uuid:"C237E6D5-CCCF-4927-B82B-0D33396F4044"
},
{
labelFor:"valore2",
location:"760,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Valore 2",
typeid:7,
uuid:"EB9D7256-5A8C-4A48-85A0-54DFABDA3089"
},
{
labelFor:"tag",
location:"310,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Tag",
typeid:7,
uuid:"F220B4EE-F709-42A5-8D13-4CE2C0E3DFC0"
}
],
name:"parametrica_tag_table",
paperPrintScale:100,
showInMenu:true,
size:"1070,480",
styleName:"keeneight",
typeid:3,
uuid:"0336DEFE-F7FB-49B8-B834-7D5A3F746D0D",
view:3