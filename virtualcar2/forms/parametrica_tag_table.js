/**
 *
 * @properties={typeid:24,uuid:"977F488E-84B7-48E5-8320-3562FC6091F0"}
 */
function nfx_getTitle()
{
	return "Parametrica Tag - Tabella";
}

/**
 *
 * @properties={typeid:24,uuid:"F00CD09A-8C20-4708-8A2F-E5C99F99C4CE"}
 */
function nfx_isProgram()
{
	return true;
}
