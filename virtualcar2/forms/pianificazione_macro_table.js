/**
 *
 * @properties={typeid:24,uuid:"3CF531C3-1FF3-4E88-A1AA-8F17B0B4EA33"}
 */
function nfx_getTitle()
{
	return "Pianificazione macro";
}

/**
 *
 * @properties={typeid:24,uuid:"DD28F598-093F-47C1-BA75-AAABA9D9CAE2"}
 */
function nfx_isProgram()
{
	return false;
}
