/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"AD0ED424-F602-490A-9D73-D5750227F43C"}
 */
var nfx_orderBy = "data_pianificazione asc";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"7930AC4D-FB08-4CD1-A887-95B953D338F5"}
 */
var nfx_related = null;

/**
 *
 * @properties={typeid:24,uuid:"4C9CE09A-2EC0-4D07-A3D4-62FE3906898F"}
 */
function nfx_defineAccess()
{
	return [true,true,true];

}
