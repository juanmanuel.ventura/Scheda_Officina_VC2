/**
 *
 * @properties={typeid:24,uuid:"51A5A1B7-4765-427A-93BC-57FE1A12987E"}
 */
function disableCheck()
{
	var e = elements.allnames;
	var max = elements.allnames.length;
	for(var i=0;i<max;i++){
		if(e[i] != "crea_scheda") elements[e[i]].enabled = true;
	}
	controller.readOnly = true;
}

/**
 *
 * @properties={typeid:24,uuid:"60315816-FEE9-4ADF-BF86-39ED1D671E8F"}
 */
function enableCheck()
{
	var e = elements.allnames;
	var max = elements.allnames.length;
	for(var i=0;i<max;i++){
		if(e[i] != "crea_scheda") elements[e[i]].enabled = false;
	}
	controller.readOnly = false;
}

/**
 *
 * @properties={typeid:24,uuid:"59BA8CE4-794E-468A-9609-2680E3F475B9"}
 */
function nfx_customAdd()
{
	databaseManager.setAutoSave(false);
	
	//application.showFormInDialog(forms.pianificazione_micro_wizard);
	
	var formq = forms.pianificazione_micro_wizard;
	var window = application.createWindow("",JSWindow.MODAL_DIALOG);
	formq.controller.show(window);
	
	
	databaseManager.setAutoSave(true);
}

/**
 *
 * @properties={typeid:24,uuid:"5A206FA6-2E7B-4B8E-876C-3B0FC1702280"}
 */
function nfx_getTitle()
{
	return "Pianificazione micro";
}

/**
 *
 * @properties={typeid:24,uuid:"604AC27D-1E06-4783-9802-1EB811C46F61"}
 */
function nfx_isProgram()
{
	return false;
}

/**
 *
 * @properties={typeid:24,uuid:"527EC8B0-F2E5-434F-B5DD-4C9DDE820F7E"}
 */
function nfx_postEdit()
{
	elements.crea_scheda.enabled = true;
}

/**
 *
 * @properties={typeid:24,uuid:"CF1F6D2C-EC9C-4717-B51B-283D2A2DFE8F"}
 */
function nfx_postEditOnCancel()
{
	elements.crea_scheda.enabled = true;
}

/**
 *
 * @properties={typeid:24,uuid:"EE559F2B-ADFE-452F-AEEA-6ABD1480F81F"}
 */
function nfx_preEdit()
{
	elements.crea_scheda.enabled = false;
}

/**
 * @return {JSFoundSet}
 * @properties={typeid:24,uuid:"8BAA777B-E549-4A88-8344-087D5E20E87E"}
 */
function getFoundset() {
	return foundset;
}
