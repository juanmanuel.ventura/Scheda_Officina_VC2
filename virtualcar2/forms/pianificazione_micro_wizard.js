/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"DF9FCECB-D9D5-409E-92CD-85814B2DB60B",variableType:4}
 */
var currentStep = 1;

/**
 * @properties={typeid:35,uuid:"ADEAA903-7B00-44BD-8DC3-987697A2CA65",variableType:-4}
 */
var wiz_currentForm = null;

/**
 * @properties={typeid:35,uuid:"8240E438-852E-43DC-8091-997C88EABD91",variableType:-4}
 */
var wiz_forms = [];

/**
 *
 * @properties={typeid:24,uuid:"A6B369C9-7C5E-4352-B260-4FD6FA1E99FE"}
 */
function onCancelButton() {
	if (wiz_currentForm.wiz_onCancel) wiz_currentForm.wiz_onCancel();
	if (plugins.dialogs.showInfoDialog("Uscire?", "Terminare la pianificazione?", "Si", "No") == "Si") {

		//application.closeFormDialog();

		var w = controller.getWindow();
		w.destroy();

	}
}

/**
 *
 * @properties={typeid:24,uuid:"4BE6B2C7-B507-42F3-937B-21FD27DDB505"}
 */
function onChangeStep() {
	switch (currentStep) {
	case 2:
		elements.stepsPanel.removeAllTabs();
		elements.stepsPanel.addTab(forms.pianificazione_micro_wizard_step2)
		wiz_currentForm = forms.pianificazione_micro_wizard_step2;
		break;
	case 1:
		elements.stepsPanel.removeAllTabs();
		elements.stepsPanel.addTab(forms.pianificazione_micro_wizard_step1)
		wiz_currentForm = forms.pianificazione_micro_wizard_step1;
		break;
	default:
		elements.saveButton.enabled = elements.nextButton.enabled = elements.prevButton.enabled = false;
		break;
	}
	wiz_currentForm.controller.readOnly = false;
	wiz_currentForm["wiz_onSelect"]();
}

/**
 *
 * @properties={typeid:24,uuid:"F56920C7-84AF-48FE-A36D-71079FE8FF11"}
 */
function onNextButton() {
	if (wiz_currentForm.wiz_onNext() != -1) {
		currentStep++;
		onChangeStep();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"AF264D00-CE9C-42EC-904B-AE1E88057D7C"}
 */
function onPrevButton() {
	if (wiz_currentForm.wiz_onPrev() != -1) {
		currentStep--;
		onChangeStep();
	}
}

/**
 *@param {JSEvent} event
 * @properties={typeid:24,uuid:"D15D2A38-7272-469D-AD10-9800AC88F87A"}
 */
function onSaveButton(event) {
	var validationResult = wiz_currentForm.wiz_onSave();
	//application.output("Validazione : " + validationResult);
	if (validationResult == 0) {
		var window=controller.getWindow();
		window.destroy();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"0F0F04B0-88A7-4082-BC3E-5330239D03E6"}
 */
function onShow() {
	wiz_forms = [forms.pianificazione_micro_wizard_step1, forms.pianificazione_micro_wizard_step2];
	for (var i = 0; i < wiz_forms.length; i++) {
		wiz_forms[i]["wiz_form"] = forms[controller.getName()];
	}
	currentStep = 1;
	onChangeStep();
}
