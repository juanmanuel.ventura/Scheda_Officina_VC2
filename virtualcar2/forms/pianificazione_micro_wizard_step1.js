/**
 * @properties={typeid:35,uuid:"4B401AA5-33BA-4EFD-8282-5A547B179088",variableType:-4}
 */
var clear = true;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"EA904A46-0C45-495C-BDE4-87461578C49B"}
 */
var wiz_collaudatore = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"CD6515AB-49BB-4C2F-A4A6-D135E35E4926"}
 */
var wiz_commessa = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"8C302F2A-ADB1-4EA0-B617-6611B9CE65B1",variableType:93}
 */
var wiz_data = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"663D191D-1018-4F65-9F77-9D7A17ECBD31"}
 */
var wiz_ente = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"E9D70D80-2C07-4C3A-8DED-4EEFA096B7FC",variableType:93}
 */
var wiz_fine = null;

/**
 * @properties={typeid:35,uuid:"B97488E9-BA02-4BE2-AA5E-ECC6037D84BB",variableType:-4}
 */
var wiz_form = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"A6190049-252C-476C-BB40-3290731D76A7",variableType:93}
 */
var wiz_inizio = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"97297B51-4CFE-4943-B610-E41491A13C6B"}
 */
var wiz_km = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"3461F0DE-EAB3-4953-9C7B-53329FC09CF0"}
 */
var wiz_missione = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"EA477D4D-D4B5-4F7E-B3D1-0EA2D8EF71BB"}
 */
var wiz_percorso = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"BFB753FE-4DCF-46AA-A20A-17B85A8F7CA0"}
 */
var wiz_progetto = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C221798E-62A3-4E20-AB04-E77062F3695E"}
 */
var wiz_richiedente = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B71FDA4B-ED07-4C11-A41F-C4C07A7C19A0"}
 */
var wiz_template = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"35255CC5-7DD8-486D-850F-816A612F433D"}
 */
var wiz_tipo = "Singola";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"84185F25-FAFB-4259-B99F-8DA27B38C7B6"}
 */
var wiz_vettura = null;

/**
 *@param {Date} [testStartDay]
 *@param {Date} [testStartHour]
 *@param {Date} [testEndHour]
 * @properties={typeid:24,uuid:"A863EA4F-200B-4485-A4E6-BA32ABD44A5D"}
 */
function create(testStartDay, testStartHour, testEndHour) {
	controller.newRecord();
	progetto = wiz_progetto;
	vettura = wiz_vettura;
	fk_schede_collaudo_template = wiz_template;

	ente = wiz_ente;
	richiedente = wiz_richiedente;
	collaudatore = wiz_collaudatore;

	percorso = wiz_percorso;
	km = wiz_km;

	missione = wiz_missione;
	commessa = wiz_commessa;
	//
	//	if (data_pianificazione1 == null){data_pianificazione1 = wiz_data;}
	//	if (ora_inizio1 == null){ora_inizio1 = wiz_inizio;}
	//	if (ora_fine1 == null){ora_fine1 = wiz_fine;}
	data_pianificazione = arguments[0] || wiz_data;

		ora_inizio = arguments[1] || wiz_inizio;
		ora_fine = arguments[2] || wiz_fine;
	databaseManager.saveData();
}

/**
 *
 * @properties={typeid:24,uuid:"4BD0520E-CC52-4370-AC24-A995938AC905"}
 */
function enteChange() {
	if (wiz_ente == "") {
		wiz_ente = null;
		application.updateUI();
	}

	globals.vc2_currentEnte = wiz_ente;
}

/**
 *
 * @properties={typeid:24,uuid:"A243AD38-DAA4-4E3A-BC7C-D48EBA367629"}
 * @AllowToRunInFind
 */
function templateChange() {
	if (wiz_template == "") {
		wiz_template = null;
		application.updateUI();
	}

	if (wiz_template) {
		var templates = forms.schede_collaudo_template.foundset.duplicateFoundSet();
		if (templates.find()) {
			templates.k8_schede_collaudo_template_id = wiz_template;
			templates.search();
		}
		wiz_ente = templates.ente;
		enteChange();
		elements.wiz_ente1.enabled = false;
		wiz_richiedente = templates.richiedente;
		elements.wiz_richiedente1.enabled = false;
		wiz_percorso = templates.percorso;
		elements.wiz_percorso1.enabled = false;
		wiz_km = templates.km;
		elements.wiz_km1.enabled = false;
		wiz_missione = templates.oggetto_prova;
		elements.wiz_missione1.enabled = false;
		wiz_commessa = templates.commessa;
		elements.wiz_commessa1.enabled = false;
	} else {
		elements.wiz_ente1.enabled = true;
		elements.wiz_richiedente1.enabled = true;
		elements.wiz_percorso1.enabled = true;
		elements.wiz_km1.enabled = true;
		elements.wiz_missione1.enabled = true;
		elements.wiz_commessa1.enabled = true;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"C4FF3CA3-FD64-47F7-833A-1A8C0A4814B4"}
 */
function typeChange() {
	if (wiz_tipo == "Multipla") {
		wiz_data = null;
		elements.wiz_data1.enabled = false;
		wiz_inizio = null;
		elements.wiz_inizio1.enabled = false;
		wiz_fine = null;
		elements.wiz_fine1.enabled = false;

		wiz_form.elements.nextButton.enabled = true;
		wiz_form.elements.saveButton.enabled = false;
	} else {
		elements.wiz_data1.enabled = true;
		elements.wiz_inizio1.enabled = true;
		elements.wiz_fine1.enabled = true;

		wiz_form.elements.nextButton.enabled = false;
		wiz_form.elements.saveButton.enabled = true;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"01B03455-A0BB-49DD-92C8-C45BC9DE14DF"}
 */
function wiz_onCancel() {
	clear = true;
	forms.pianificazione_micro_wizard_step2.clear = true;
}

/**
 *
 * @properties={typeid:24,uuid:"60CECA40-1247-484B-9240-10F711D57758"}
 */
function wiz_onNext() {
	return wizValidator();
}

/**
 *
 * @properties={typeid:24,uuid:"4B95879E-690B-4284-AE01-BE179145A44C"}
 */
function wiz_onPrev() {
	return -1;
}

/**
 *
 * @properties={typeid:24,uuid:"4642AA32-CE11-4E69-A789-C060227C9551"}
 */
function wiz_onSave() {
	if (wizValidator() == 0) {
		//create(null,null,null);
		create();
		//imposta la pulizia dei form
		wiz_onCancel();
	} else {
		return -1;
	}
	return 0;
}

/**
 *
 * @properties={typeid:24,uuid:"394867F3-2AFA-4715-9B61-79D5E7A95E7E"}
 */
function wiz_onSelect() {
	wiz_form.elements.saveButton.enabled = true;
	wiz_form.elements.nextButton.enabled = false;
	wiz_form.elements.prevButton.enabled = false;

	wiz_progetto = forms.anagrafica_vetture.progetto;
	globals.vc2_currentProgetto = forms.anagrafica_vetture.progetto;
	wiz_vettura = forms.anagrafica_vetture.codice_vettura;

	if (clear) {
		wiz_template = null;
		templateChange();

		wiz_ente = null;
		elements.wiz_ente1.bgcolor = "#FFFFFF";
		enteChange();
		wiz_richiedente = null;
		elements.wiz_richiedente1.bgcolor = "#FFFFFF";
		wiz_collaudatore = null;
		wiz_percorso = null;
		elements.wiz_percorso1.bgcolor = "#FFFFFF";
		wiz_km = null;
		elements.wiz_km1.bgcolor = "#FFFFFF";
		wiz_missione = null;
		elements.wiz_missione1.bgcolor = "#FFFFFF";
		wiz_commessa = null;
		wiz_data = null;
		elements.wiz_data1.bgcolor = "#FFFFFF";
		wiz_inizio = null;
		elements.wiz_inizio1.bgcolor = "#FFFFFF";
		wiz_fine = null;

		forms.pianificazione_micro_wizard_step2.clear = true;
	}
	clear = null;
	wiz_tipo = "Singola";
	typeChange();
}

/**
 *@return {Number}
 * @properties={typeid:24,uuid:"45CA2F95-B701-4141-85D4-B945B0606F8A"}
 */
function wizValidator() {
	//FS
//	application.output("START pianificazione_micro_wizard_step1.wizValidator() " + LOGGINGLEVEL.INFO);

	var def = ["wiz_ente", "wiz_richiedente", "wiz_percorso", "wiz_km", "wiz_missione"];
	var toCheck = (wiz_tipo == "Singola") ? def.concat(["wiz_data", "wiz_inizio"]) : def;
	/** @type {Array<String>} */
	var formVariables = solutionModel.getForm(controller.getName()).getVariables().map(function(jsVariable) {
		return jsVariable.name
	});
	for (var i = 0; i < toCheck.length; i++) {
		//dep	var value = eval(allvariables[allvariables.indexOf(toCheck[i])]);
		var value = eval(formVariables[formVariables.indexOf(toCheck[i])]);
		//FIXME FS: non setta lo sfondo dell'elemento
		if (value) {
			elements[toCheck[i] + "1"].bgcolor = "#FFFFFF";
		} else {
			elements[toCheck[i] + "1"].bgcolor = "#FF0000";
			return -1;
		}
//		application.output("element " + elements[toCheck[i] + "1"].getName() + "tipo elem " + elements[toCheck[i] + "1"].getElementType() + " background  " + elements[toCheck[i] + "1"].bgcolor);
//		application.output("STOP pianificazione_micro_wizard_step1.wizValidator() " + LOGGINGLEVEL.INFO);
	}
	return 0;
}
