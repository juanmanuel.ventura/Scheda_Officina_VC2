/**
 * @properties={typeid:35,uuid:"FEA9F791-77B5-43FB-AE1F-7F67B2AA9111",variableType:-4}
 */
var clear = true;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"DE15F81C-4D5B-417C-9FEE-9A885FB79A02",variableType:93}
 */
var data_fine_pianificazione = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"A758D00D-FF61-42E8-BEA2-57EC8E122EE7",variableType:93}
 */
var data_inizio_pianificazione = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"28B543C1-F576-4224-8952-31B2DF090932",variableType:93}
 */
var fine_primo_d = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"893785ED-ECED-4A77-BD24-17D9132353F6",variableType:93}
 */
var fine_primo_l_v = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"EA2D0F79-07C8-4099-9BF1-10824C9A5A6B",variableType:93}
 */
var fine_primo_s = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"D556A501-B849-4910-BC45-63F50099D1B7",variableType:93}
 */
var fine_secondo_d = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"8A163409-E09F-4105-90D0-B60956C1982D",variableType:93}
 */
var fine_secondo_l_v = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"BB374F62-9F90-4A69-A2DD-BD774F4BBD87",variableType:93}
 */
var fine_secondo_s = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"5D16BE23-8DF2-4272-B93F-109F23F3D1F9",variableType:93}
 */
var fine_terzo_d = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"3C0A4D6A-584D-48E5-8485-33E8ECF7ABF8",variableType:93}
 */
var fine_terzo_l_v = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"4606370A-012B-4CFA-8BF6-3A2F21D65E4C",variableType:93}
 */
var fine_terzo_s = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"074C1B99-1473-419E-837E-8D63D530334B",variableType:93}
 */
var fine_zero_d = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"0F6F31A2-970C-44FA-9F7B-77A231D5A7D7",variableType:93}
 */
var fine_zero_l_v = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"AA3CAB18-2255-4319-A187-31A4E1098176",variableType:93}
 */
var fine_zero_s = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"6C6CA38E-95FC-4F92-B42D-7C729A21E37B",variableType:93}
 */
var inizio_primo_d = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"8AECFBDA-78EC-4364-B85D-ACCFEEF4127F",variableType:93}
 */
var inizio_primo_l_v = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"48687A3A-3C4E-4A4C-A89E-941F35FD38B5",variableType:93}
 */
var inizio_primo_s = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"72BC5473-C9E4-4A8A-BF65-CFFC3B68FC21",variableType:93}
 */
var inizio_secondo_d = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"80EC1BA5-9AD4-41DA-80C3-4AAA1B39FE3A",variableType:93}
 */
var inizio_secondo_l_v = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"2DDCDC68-58EC-4EAF-BDAA-83C380A83DFF",variableType:93}
 */
var inizio_secondo_s = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"B729620C-E557-43BC-B65E-58D5287F8E03",variableType:93}
 */
var inizio_terzo_d = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"70F06430-0F5B-45C0-B256-09FF314AAACC",variableType:93}
 */
var inizio_terzo_l_v = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"34966296-8E9D-492A-A350-1EBD450107FD",variableType:93}
 */
var inizio_terzo_s = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"17BCF0F0-8471-4938-8123-C4867B71FF93",variableType:93}
 */
var inizio_zero_d = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"E0F962C8-E84C-403E-A88A-415B2FE08619",variableType:93}
 */
var inizio_zero_l_v = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"7234D959-13EE-4D10-A5F7-70B90AF3B921",variableType:93}
 */
var inizio_zero_s = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"0B29135F-ECF3-49A3-B1C9-8D0D2BED19DA",variableType:4}
 */
var numero_turni_dom = 0;

/**
 * @properties={typeid:35,uuid:"8AAB5F4E-5B05-49A6-8000-087DFB25505B",variableType:-4}
 */
var numero_turni_domTurns = ["inizio_primo_d","fine_primo_d","inizio_secondo_d","fine_secondo_d","inizio_terzo_d","fine_terzo_d","inizio_zero_d","fine_zero_d"];

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"BE6513A1-931E-4F6D-B102-D8A249C4A979",variableType:4}
 */
var numero_turni_lun_ven = 0;

/**
 * @properties={typeid:35,uuid:"DDDDEE04-9FED-4BD9-9900-66AA7EF62350",variableType:-4}
 */
var numero_turni_lun_venTurns = ["inizio_primo_l_v","fine_primo_l_v","inizio_secondo_l_v","fine_secondo_l_v","inizio_terzo_l_v","fine_terzo_l_v","inizio_zero_l_v","fine_zero_l_v"];

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"EEBC9552-6E26-433B-B5B6-340D62AC6F55",variableType:4}
 */
var numero_turni_sab = 0;

/**
 * @properties={typeid:35,uuid:"093F627C-ADFE-4845-B910-F40546031351",variableType:-4}
 */
var numero_turni_sabTurns = ["inizio_primo_s","fine_primo_s","inizio_secondo_s","fine_secondo_s","inizio_terzo_s","fine_terzo_s","inizio_zero_s","fine_zero_s"];

/**
 * @properties={typeid:35,uuid:"A7D42F82-8F90-4852-A1D9-46D4CD06C923",variableType:-4}
 */
var wiz_form = null;

/**
 *
 * @properties={typeid:24,uuid:"D6902679-EE32-47DF-B0E3-85BCBDC7C074"}
 */
function createBlock(num,turns,date)
{

	if(num > 0){
		for(var i=0;i<num;i++)
		{
			var index = i * 2;
			var start = forms[controller.getName()][turns[index]];
			var end = forms[controller.getName()][turns[index + 1]];
			forms.pianificazione_micro_wizard_step1.create(date,start,end);
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"40CC05AC-9DD8-4F11-B4CD-8F4C9C74E670"}
 */
function disableAll()
{
	for(var i=0;i<elements.allnames.length;i++){
		if(elements.allnames[i] != "data_inizio_pianificazione" && elements.allnames[i] != "data_fine_pianificazione"){
			elements[elements.allnames[i]].enabled = false;
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"76DB38CF-D4ED-4F17-BD74-09E2F02E4157"}
 */
function disableTurns(turns,min)
{

	 min = min * 2;
	for(var i=min;i<turns.length;i++)
	{
		
		forms[controller.getName()][turns[i]] = null;
		elements[turns[i]].enabled = false;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"CA9F4A26-2F99-4D9E-80F5-D143E9508FF9"}
 */
function enableTurns(turns,max)
{

	 max = max * 2;
	for(var i=0;i<max;i++)
	{
		elements[turns[i]].enabled = true;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"9F40694B-0DC3-4F80-92C2-09D1217690CC"}
 */
function onDateChange()
{
	if(data_inizio_pianificazione && data_fine_pianificazione)
	{
		var dates = globals.utils_getDateIntervalAsArray(data_inizio_pianificazione,data_fine_pianificazione);
		if(globals.utils_howManyWorkingDays(dates) > 0){
			elements.numero_turni_lun_ven.enabled = true;
		}else{
			numero_turni_lun_ven = 0;
			elements.numero_turni_lun_ven.enabled = false;
			disableTurns(numero_turni_lun_venTurns,numero_turni_lun_ven);
		}
		if(globals.utils_howManySaturdays(dates) > 0){
			elements.numero_turni_sab.enabled = true;
		}else{
			numero_turni_sab = 0;
			elements.numero_turni_sab.enabled = false;
			disableTurns(numero_turni_sabTurns,numero_turni_sab);
		}
		if(globals.utils_howManySundays(dates) > 0){
			elements.numero_turni_dom.enabled = true;
		}else{
			numero_turni_dom = 0;
			elements.numero_turni_dom.enabled = false;
			disableTurns(numero_turni_domTurns,numero_turni_dom);
		}
	}
	else
	{
		disableAll();
	}
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"DCA923CD-C299-41D4-A213-9987945156E1"}
 */
function onTurnNumberChangeWrap(event)
{
//dep	onTurnNumberChange(null);
	onTurnNumberChange(event.getElementName());

}

/**
 *
 * @properties={typeid:24,uuid:"C1EFB8C5-B774-4ED4-BCA6-AC38D4124371"}
 */
function onTurnNumberChange(element)
{

	var turns = forms[controller.getName()][element + "Turns"];
	var num = forms[controller.getName()][element];
	disableTurns(turns,num);
	enableTurns(turns,num);
}

/**
 *
 * @properties={typeid:24,uuid:"33AF087C-2936-4E99-872E-2F51D26EB00D"}
 */
function validateBlock(num,turns)
{

	if(num > 0){
		for(var i=0;i<num;i++)
		{
			var index = i * 2;
			var value = forms[controller.getName()][turns[index]];
			if(!value || value == ""){
				elements[turns[index]].bgcolor = "#FF0000";
				return -1;
			}else{
				elements[turns[index]].bgcolor = "#FFFFFF";
			}
		}
	}
	return 0;
}

/**
 *
 * @properties={typeid:24,uuid:"5924039B-8D32-4A60-9837-929A0AEAEC7C"}
 */
function wiz_onCancel()
{
	forms.pianificazione_micro_wizard_step1.clear = true;
	clear = true;
}

/**
 *
 * @properties={typeid:24,uuid:"3F1393F2-0C39-449D-9333-D141CFAF89E2"}
 */
function wiz_onNext()
{
	return -1;
}

/**
 *
 * @properties={typeid:24,uuid:"2498250E-A365-4281-AD90-E807762D3DB8"}
 */
function wiz_onPrev()
{
	forms.pianificazione_micro_wizard_step1.clear = null;
	return 1;
}

/**
 *
 * @properties={typeid:24,uuid:"6C143AD8-9737-4D00-8B53-999764221DE0"}
 */
function wiz_onSave()
{
	if(wizValidator() != -1){
		var dates = globals.utils_getDateIntervalAsArray(data_inizio_pianificazione,data_fine_pianificazione);
		for(var i=0;i<dates.length;i++)
		{
			if(dates[i].getDay() > 0 && dates[i].getDay() < 6){
				createBlock(numero_turni_lun_ven,numero_turni_lun_venTurns,dates[i]);
			}
			if(dates[i].getDay() == 6){
				createBlock(numero_turni_sab,numero_turni_sabTurns,dates[i]);
			}
			if(dates[i].getDay() == 0){
				createBlock(numero_turni_dom,numero_turni_domTurns,dates[i]);
			}
		}
		//imposta la pulizia dei form
		wiz_onCancel();
	}else{
		return -1;
	}
	return 0;
}

/**
 *
 * @properties={typeid:24,uuid:"C9F2D584-535B-49A0-915A-F6E603ECEC1E"}
 */
function wiz_onSelect()
{
	wiz_form.elements.saveButton.enabled = true;
	wiz_form.elements.nextButton.enabled = false;
	wiz_form.elements.prevButton.enabled = true;
	
	if(clear){
		data_inizio_pianificazione = data_fine_pianificazione = null;
		numero_turni_lun_ven = numero_turni_sab = numero_turni_dom = 0;
		application.updateUI();
		clear = null;
	}
	
	onDateChange();
	application.updateUI();
	disableTurns(numero_turni_lun_venTurns,numero_turni_lun_ven);
	enableTurns(numero_turni_lun_venTurns,numero_turni_lun_ven);
	disableTurns(numero_turni_sabTurns,numero_turni_sab);
	enableTurns(numero_turni_sabTurns,numero_turni_sab);
	disableTurns(numero_turni_domTurns,numero_turni_dom);
	enableTurns(numero_turni_domTurns,numero_turni_dom);
}

/**
 *
 * @properties={typeid:24,uuid:"2232621B-3957-48D1-85FE-3A5A13A38EE1"}
 */
function wizValidator()
{
	if(!data_inizio_pianificazione){
		elements.data_inizio_pianificazione.bgcolor = "#FF0000";
		return;
	}else{
		elements.data_inizio_pianificazione.bgcolor = "#FFFFFF";
	}
	
	if(!data_fine_pianificazione){
		elements.data_fine_pianificazione.bgcolor = "#FF0000";
		return;
	}else{
		elements.data_fine_pianificazione.bgcolor = "#FFFFFF";
	}
	
	if(validateBlock(numero_turni_lun_ven,numero_turni_lun_venTurns) + validateBlock(numero_turni_sab,numero_turni_sabTurns) + validateBlock(numero_turni_dom,numero_turni_domTurns) < 0){
		return;
	}
}
