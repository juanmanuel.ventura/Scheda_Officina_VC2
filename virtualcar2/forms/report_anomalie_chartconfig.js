/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"6AC051FC-09C4-485F-8C39-2FECE1E4FACB"}
 */
var bgc1 = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"BFC4DADD-93DA-4CDB-A289-2E972EDEE77D"}
 */
var bgc2 = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"9922D01F-5FD1-4F37-AF3E-762D6A4FC0A1"}
 */
var content = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"768CA2B0-23A0-4227-B5F8-767BC53F9382"}
 */
var fntx = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"4E5FA5D0-B67C-4EE5-9DEC-2B0FEB684921"}
 */
var fnty = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B3C45050-9C89-46FE-9616-C6CB0F57DA0D"}
 */
var grid = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"91F0D841-D2D1-4A8F-AFAE-BC22E727D18A"}
 */
var report = "";

/**@param {String} _report
 * @param {Boolean} [bool]
 * @properties={typeid:24,uuid:"EB4EC379-A787-458D-8C8A-39FD85215E38"}
 */
function openPopup(_report,bool){
	report = _report;
	content = controller.getName() + "_" + forms[report].config.attributes.type;
	
	setStylePanel();
	
	setColorElement("bgc1",forms[report].config.bgc1);
	setColorElement("bgc2",forms[report].config.bgc2);
	
	if(forms[report].config.grid){
		elements.grid.visible = true;
		elements.grid_label.visible = true;
		setColorElement("grid",forms[report].config.grid);
	}else{
		elements.grid.visible = false;
		elements.grid_label.visible = false;
	}
	
	setFontElement("fntx",forms[report].config.fntx);
	setFontElement("fnty",forms[report].config.fnty);
	
	forms[content].setup(forms[report].config.styles,forms[report][forms[report].config.attributes.id]());
	
	controller.show("opzioni_grafiche");
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"B7ED50FC-9DD1-4FB4-91AB-18A43D9490D8"}
 */
function setColor(event) {
	var _color = application.showColorChooser(forms[controller.getName()][event.getElementName()]);
	if(_color){
		setColorElement(event.getElementName(),_color)
	}
}

/**
 * @properties={typeid:24,uuid:"F8B34E0E-9CB8-40A8-9427-9B35CAA4202B"}
 */
function setColorElement(_element,_color){
	elements[_element].bgcolor = _color;
	elements[_element].fgcolor = _color;
	forms[controller.getName()][_element] = _color;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 * 
 * @properties={typeid:24,uuid:"D1029AF8-CEE3-4EC4-9060-14ECDB244F23"}
 */
function setFont(event){
	var _font = application.showFontChooser(forms[controller.getName()][event.getElementName()]);
	if(_font){
		setFontElement(event.getElementName(),_font);
	}
}

/**
 * @properties={typeid:24,uuid:"C256FF6E-A40C-4B89-851A-23A46617B624"}
 */
function setFontElement(_element,_font){
	elements[_element].setFont(_font)
	forms[controller.getName()][_element] = _font;
}

/**
 * @properties={typeid:24,uuid:"44A572E9-F21A-4F4D-A001-864EA49AFD47"}
 */
function setStylePanel(){
	elements.styles_panel.removeAllTabs();
	elements.styles_panel.addTab(forms[content]);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"338E6FC7-BC93-4A16-B0DB-7AB38CBB90D1"}
 */
function saveConfig(event) {
	forms[report].config.bgc1 = bgc1;
	forms[report].config.bgc2 = bgc2;
	
	forms[report].config.grid = (elements.grid.visible) ? grid : null;
	
	forms[report].config.fntx = fntx;
	forms[report].config.fnty = fnty;
	
	forms[report].config.styles = forms[content].styles;
	
	//application.closeFormDialog();
	
	var w=controller.getWindow();
	w.destroy();
}
