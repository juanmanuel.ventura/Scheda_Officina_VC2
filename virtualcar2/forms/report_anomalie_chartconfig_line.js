/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"4FD6E14A-E8ED-4D69-AB33-B058F28CBCFA"}
 */
var styleColor = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"18CC083C-B1A6-4244-A00B-7780ABC25E9C"}
 */
var styleID = "";

/**
 * @properties={typeid:35,uuid:"6D5DD2D6-2CB1-4C44-A304-BC0B391C5F30",variableType:-4}
 */
var styles = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"2ABD5C57-1E8B-428D-9C39-B1B0D3DFA75C",variableType:4}
 */
var styleStroke = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"5A7BF34C-5B25-41E8-9964-905970DBE2F5",variableType:4}
 */
var styleWidth = null;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"8B917438-0C09-4570-A39F-957FC1FCB283"}
 */
function onLoad(event) {
	var vp = elements.scroll.getViewport();
	//FS
	//aggiunto elList
	//	/** @type {java.awt.Component} */
	//	var elList=elements.list;
	//	vp.add(elList);
	//dep

	//SAuc
	//vp.add(elements.list);

	vp.add(java.awt.Component(elements.list));
	//FS
	//	elements.list.addListSelectionListener(setSelectedContent);
	/** @type {javax.swing.event.ListSelectionListener} */
	var selectedContent = setSelectedContent;
	elements.list.addListSelectionListener(selectedContent);

}

/**
 * @properties={typeid:24,uuid:"8114452A-5867-450F-856E-F5D7F60E241C"}
 */
function setup(_styles, _list) {
	setDefaultContent();

	styles = new Object();
	for (var s in _styles) {
		styles[s] = _styles[s];
	}
	setListContent(styles, null);

	application.setValueListItems("static$report_id", _list);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"8053B4F4-EB20-42D1-9202-3A6035D68088"}
 */
function setColor(event) {
	var _color = application.showColorChooser(forms[controller.getName()][event.getElementName()]);
	if (_color) {
		setColorElement(event.getElementName(), _color)
	}
}

/**
 * @properties={typeid:24,uuid:"037FF72A-0393-4C2F-A34C-6F6426F467A5"}
 */
function setColorElement(_element, _color) {
	elements[_element].bgcolor = _color;
	elements[_element].fgcolor = _color;
	forms[controller.getName()][_element] = _color;
}

/**
 * @properties={typeid:24,uuid:"F9E12DBD-2845-4034-86AF-829A89347710"}
 */
function setListContent(_styles, _selected) {
	var _content = [];
	for (var s in styles) {
		_content.push(s);
	}
	elements.list.setListData(_content.sort());
	elements.list.setSelectedValue(_selected, true);
}

/**@param {javax.swing.event.ListSelectionEvent} fe
 * @properties={typeid:24,uuid:"7E8666F9-4BAE-47AE-B513-587C58FA4350"}
 */
function setSelectedContent(fe) {
	if (!fe.getValueIsAdjusting()) {
		var _v = elements.list.getSelectedValue();
		if (_v) {
			setCustomContent(_v, styles[_v][0], styles[_v][1], styles[_v][2][0]);
		} else {
			setDefaultContent();
		}
	}
}

/**
 * @properties={typeid:24,uuid:"DCCEA637-3E94-4CAD-BF54-7510866233F6"}
 */
function setCustomContent(_id, _color, _width, _stroke) {
	styleID = _id;
	setColorElement("styleColor", _color);
	styleWidth = _width;
	styleStroke = _stroke;
}

/**
 * @properties={typeid:24,uuid:"C498F6FA-9905-4EBE-9042-42CF28F83EDF"}
 */
function setDefaultContent() {
	styleID = null;
	setColorElement("styleColor", "#000000");
	styleWidth = 2;
	styleStroke = 0;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"4E397653-C125-4AC1-B39A-A03C192B2CA8"}
 */
function addStyle(event) {
	if (styleID) {
		styles[styleID] = [styleColor, styleWidth, [styleStroke]];
		setListContent(styles, styleID);
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"F563BDE2-45EE-437D-A703-03CD180CA640"}
 */
function remStyle(event) {
	if (styleID) {
		delete styles[styleID];
		setListContent(styles, null);
	}
}
