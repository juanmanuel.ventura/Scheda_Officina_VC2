/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"0C8BAB66-9039-4782-991D-8211CAFC2DD5"}
 */
var styleColor = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F0F72688-F68B-4983-B582-C78B9B5491DD"}
 */
var styleID = "";

/**
 * @properties={typeid:35,uuid:"89A4A65A-E59A-4F61-B29F-28CD68870B2B",variableType:-4}
 */
var styles = null;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"16207E23-12E4-47EC-90D6-0E9447F60CA0"}
 */
function onLoad(event) {
	var vp = elements.scroll.getViewport();
	//FS 
	//aggiunto elList 
	/** @type {java.awt.Component} */
	var elList=elements.list;
	vp.add(elList);
	//dep  vp.add(elements.list);
	//FS
	/** @type {javax.swing.event.ListSelectionListener} */	
	var selectedContent = setSelectedContent;
	elements.list.addListSelectionListener(selectedContent);
	//dep elements.list.addListSelectionListener(setSelectedContent);
}

/**
 * @properties={typeid:24,uuid:"258BA6F8-892C-4E1D-9A79-C73F1246F8D3"}
 */
function setup(_styles,_list){
	setDefaultContent();
	
	styles = new Object();
	for(var s in _styles){
		styles[s] = _styles[s];
	}
	setListContent(styles,null);
	
	application.setValueListItems("static$report_id",_list);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"1944D74E-12C3-410F-A7D9-7689F71B1745"}
 */
function setColor(event) {
	var _color = application.showColorChooser(forms[controller.getName()][event.getElementName()]);
	if(_color){
		setColorElement(event.getElementName(),_color)
	}
}

/**
 * @properties={typeid:24,uuid:"F85C5156-70A8-408A-87C7-24EDDC63217B"}
 */
function setColorElement(_element,_color){
	elements[_element].bgcolor = _color;
	elements[_element].fgcolor = _color;
	forms[controller.getName()][_element] = _color;
}

/**
 * @properties={typeid:24,uuid:"FB83B244-8901-44AC-BF7E-1CEB83CBD46C"}
 */
function setListContent(_styles,_selected){
	var _content = [];
	for(var s in styles){
		_content.push(s);
	}
	elements.list.setListData(_content.sort());
	elements.list.setSelectedValue(_selected,true);
}

/**
 * @properties={typeid:24,uuid:"D76DABD8-3771-462E-ACDE-DADE69D1986D"}
 */
function setSelectedContent(s){
	if(!s.getValueIsAdjusting()){
		var _v = elements.list.getSelectedValue();
		if(_v){
			setCustomContent(_v,styles[_v][0],null,null);
		}else{
			setDefaultContent();
		}
	}
}

/**
 * @properties={typeid:24,uuid:"7135F84C-63A0-46E8-B195-7BD113F501BC"}
 */
function setCustomContent(_id,_color,_width,_stroke){
	styleID = _id;
	setColorElement("styleColor",_color);
}

/**
 * @properties={typeid:24,uuid:"6B0A99FC-930E-4E95-9188-87F90A282A5D"}
 */
function setDefaultContent(){
	styleID = null;
	setColorElement("styleColor","#000000");
}

/**
 * @properties={typeid:24,uuid:"8FAE65C3-456F-485B-8150-58B69492EAA3"}
 */
function addStyle(event) {
	if(styleID){
		styles[styleID] = [styleColor];
		setListContent(styles,styleID);
	}
}

/**
 * @properties={typeid:24,uuid:"6038C94A-E4A7-48E1-9111-E19E9DE48C82"}
 */
function remStyle(event) {
	if(styleID){
		delete styles[styleID];
		setListContent(styles,null);
	}
}
