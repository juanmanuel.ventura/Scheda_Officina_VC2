borderType:"EmptyBorder,0,0,0,0",
items:[
{
anchors:6,
imageMediaID:"FCEE7CF0-4903-4ACF-BB0D-73124FECD40F",
location:"680,320",
mediaOptions:1,
name:"_selectionOptions",
onActionMethodID:"0133D6E7-D48C-451C-B1F9-72C66815F584",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
size:"110,20",
text:"Selezione",
typeid:7,
uuid:"06936D0C-C71E-490F-B664-24D42BDC41BF"
},
{
anchors:3,
horizontalAlignment:0,
imageMediaID:"519C04F3-2F1C-4374-B807-64B55FED53E9",
location:"560,10",
mediaOptions:2,
name:"menu",
onActionMethodID:"3A63AF4D-BB0A-4DDE-9B40-47CFCE3EDEFC",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
size:"90,20",
text:"Tools",
typeid:7,
uuid:"3D65D9EC-2290-4E6F-8EC9-8CFA78CEB600"
},
{
anchors:15,
location:"10,40",
name:"reportContainer",
printable:false,
size:"660,300",
transparent:true,
typeid:16,
uuid:"4BD97804-EFA3-4243-9B3A-BC56366E8CE1"
},
{
anchors:3,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"loader",
displayType:8,
editable:false,
horizontalAlignment:0,
location:"650,10",
name:"loader",
scrollbars:36,
size:"20,20",
transparent:true,
typeid:4,
uuid:"6F8FF5BF-02AD-4B2F-8034-B31AC7A96447"
},
{
height:350,
partType:5,
typeid:19,
uuid:"71F0D1F3-FB5C-48AC-AE4C-567B7737B1AA"
},
{
anchors:3,
imageMediaID:"5B01DDDD-FE06-402B-A690-62B7D4944EC6",
location:"470,10",
mediaOptions:10,
onActionMethodID:"4CFE87F4-FB87-4595-895C-1D7633FED0B0",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
size:"90,20",
text:"Chart",
typeid:7,
uuid:"7820FFD7-7CB4-4A06-B8EF-FFA4DA21BFAB"
},
{
anchors:11,
dataProviderID:"reportSelected",
displayType:2,
editable:false,
location:"10,10",
name:"reportSelected",
onDataChangeMethodID:"ACB3AE28-8407-40CD-808D-EBDA458B5EBE",
size:"450,20",
text:"Reportselected",
typeid:4,
uuid:"83FB879A-B7AE-4916-A6A6-199A087AE360",
valuelistID:"C88EFD4D-1327-4B2A-98EE-3F269E72DEF3"
},
{
anchors:3,
labelFor:"projects",
location:"690,0",
mediaOptions:14,
size:"110,20",
styleClass:"form",
tabSeq:-1,
text:"Progetti",
transparent:true,
typeid:7,
uuid:"9715A75D-BDCD-4055-AC62-E77CF7867983"
},
{
anchors:7,
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"projects",
displayType:4,
location:"680,20",
name:"projects",
onDataChangeMethodID:"4A686308-A454-4E95-AB12-6F75548C64C0",
size:"120,290",
text:"Projects",
transparent:true,
typeid:4,
uuid:"AE8BA35B-E4FF-423D-9369-684EDC8CF82E",
valuelistID:"8DBD3071-35BE-4D55-B40F-F9B6FB9F8DD6"
}
],
name:"report_anomalie_container",
navigatorID:"-1",
onHideMethodID:"7621013B-45FC-454C-88BE-EF2DC5A5C0E0",
onLoadMethodID:"60138BEE-9DAA-4DC8-9C15-3FC9B67682D6",
onRecordSelectionMethodID:"2859D255-6F8C-4ACE-B502-1E941A5C31EF",
onSearchCmdMethodID:"A4E5EAFC-62DD-48C2-B314-BC9A9204E61B",
onShowMethodID:"BCCAF39F-5866-42AB-9E3E-7563D9D8EA46",
paperPrintScale:100,
showInMenu:true,
size:"800,350",
styleName:"keeneight",
typeid:3,
uuid:"F87C02C9-4049-4FE3-A49C-59A3EF1E395D"