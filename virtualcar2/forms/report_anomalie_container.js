/**
 * @properties={typeid:35,uuid:"5FA96960-AEB6-43A9-904F-C0368453B685",variableType:-4}
 */
var advancedVersionChooser = true;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"4B7E37B0-480E-46E5-85B0-42B9EAC6E05A"}
 */
var chartTitleDateFormat = "dd/MM/yyyy ('week' w)";

/**
 * @properties={typeid:35,uuid:"B2EE976C-0D57-48C7-B93D-6B9BB9C73342",variableType:-4}
 */
var colors = ["#351060", "#6DC764", "#2D131A", "#DFD982", "#B0944E", "#203786", "#9366F1", "#9F01AE", "#E8E18F", "#3F586B", "#395066", "#143A6D", "#042097", "#F8F0BD", "#BC7A64", "#B71DBC", "#4D2AE4", "#AC6FC0", "#347A7B", "#774557", "#3095CA", "#C3F73A", "#3C7B07", "#86568B", "#DD2202", "#4731B4", "#F880C8", "#8DF880", "#72AE1B", "#0E5233", "#C8F5FC", "#F34360", "#75301B", "#7CE9FD", "#F2FD45", "#30EF17", "#E379C6", "#BFB97F", "#E144CE", "#BE4142", "#8933DA", "#A199C2", "#DC153C", "#C50556", "#84015C", "#4907C6", "#03EE73", "#2B7A1E", "#1B7CB0", "#A8308C", "#67EC58", "#A2044C", "#AF71B2", "#9AB89B", "#C119B2", "#109722", "#D0B827", "#6859AD", "#D0102E", "#84BFFE", "#860952", "#F1841D", "#F1352F", "#48A217", "#EE0F1C", "#75E1E4", "#AF34AC", "#4B31AE", "#2B6551", "#AB241F", "#33C2AF", "#D5DA9F", "#12410B", "#080B4B", "#6B0F35", "#66876B", "#D8EADC", "#97D6F2", "#8DC577", "#3F87AA", "#FDACC5", "#7160D4", "#394639", "#95433E", "#335E0A", "#833D3F", "#0539C7", "#EEFA87", "#CB4B3F", "#04DEEF", "#88B2AC", "#EFC998", "#50F995", "#10949D", "#35BCCD", "#AB5763", "#51354B", "#8E8815", "#3262A1", "#590ED7", "#6B7CAA"];

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B46BA57F-4B45-4154-8B7A-4EB3D2A65025"}
 */
var loader = "<html><body><img src='media:///ajax-loader.gif'></body></html>";

/**
 * @properties={typeid:35,uuid:"131030BB-267B-44B1-ACD8-5EF953D92B35",variableType:-4}
 */
var popup = null;

/**
 * @properties={typeid:35,uuid:"3B516DD0-C864-46AC-970D-13E420886FFC",variableType:-4}
 */
var profiles = { };

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F15818A4-11F7-4B86-93C0-C71D44736DC5"}
 */
var projects = "";

/**@type {Array<String>}
 * @properties={typeid:35,uuid:"17D4E7D7-91E5-476C-8E75-818D74A31A7C",variableType:-4}
 */
var reportList = ['null', "report_cumulate", "report_kpi", "report_monitoraggio", "report_dettaglio_cumulate", "report_istogrammi", "report_intervalli_monitoraggio", "report_intervalli"];

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"E2C0F4D7-FF7A-4E8E-91A4-62B845C49209",variableType:4}
 */
var reportSelected = 0;

/**
 * @properties={typeid:35,uuid:"31E9B9FD-ECB0-47EE-805E-237F80D63B44",variableType:-4}
 */
var versions = { };

/**
 * @properties={typeid:24,uuid:"E82ED153-43BD-4FD0-8810-55319A6B90A0"}
 */
function nfx_getTitle() {
	return "Report anomalie";
}

/**
 * @properties={typeid:24,uuid:"4F6EE27E-B706-49A8-8257-9AF70A958861"}
 */
function nfx_isProgram() {
	return true;
}

/**
 * @properties={typeid:24,uuid:"1B533D55-E301-4F0E-B600-66FF862DB4D1"}
 */
function nfx_defaultPermissions() {
	return "%";
}

/**
 * @properties={typeid:24,uuid:"6535B3BD-2050-4D79-ACF2-0F3CDF22C442"}
 */
function nfx_onShow() {
	runLoader(false);
}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"60138BEE-9DAA-4DC8-9C15-3FC9B67682D6"}
 */
function onLoad(event) {
	popup = forms.report_anomalie_popup;
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"ACB3AE28-8407-40CD-808D-EBDA458B5EBE"}
 */
function selectReport(oldValue, newValue, event) {
	elements.reportContainer.removeAllTabs();
	if (newValue != 0) {
		elements.reportContainer.addTab(forms[reportList[newValue]]);

		var _showConfigWarningCheck = forms.nfx_interfaccia_gestione_salvataggi.load(controller.getName() + "_showConfigWarningCheck");

		//Procedura di primo utilizzo: inizializzazione dato
		if (!_showConfigWarningCheck) {
			_showConfigWarningCheck = "yes";
			forms.nfx_interfaccia_gestione_salvataggi.save(_showConfigWarningCheck, controller.getName() + "_showConfigWarningCheck");
		}

		//Controllo su _showEnteWarningCheck
		if (_showConfigWarningCheck == "yes") {
			forms.report_anomalie_warning_popup.openPopup(controller.getName() + "_showConfigWarningCheck");
			if (forms.report_anomalie_warning_popup.getResult()) {
				openConfig();
			}
		}
	}
	return true;
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"4CFE87F4-FB87-4595-895C-1D7633FED0B0"}
 */
function draw(event) {
	if (isChartLoaded()) {
		runLoader(true);
		try {
			forms[reportList[reportSelected]].drawChart();
		} catch (e) {
			plugins.dialogs.showErrorDialog("Errore interno", "Testo del messaggio: " + e.message, "Ok");
		}
		runLoader(false);
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"3A63AF4D-BB0A-4DDE-9B40-47CFCE3EDEFC"}
 */
function openMenu(event) {
	var items = [];
	var menu = plugins.window.createPopupMenu();
	if (isChartVisible()) {
		//FS
		//dep		var _export = plugins.popupmenu.createMenuItem("Export dati (.xls)",exportData);
		//dep		var _save = plugins.popupmenu.createMenuItem("Salva immagine (.png)",saveChart);

		var _export = menu.addMenuItem("Export dati (.xls)", exportData);
		var _save = menu.addMenuItem("Salva immagine (.png)", saveChart);
		items.push(_export, _save);
	}
	if (reportSelected) {
		//FS
		//dep		var _config = plugins.popupmenu.createMenuItem("Opzioni interfaccia", openConfig);
		var _config = menu.addMenuItem("Opzioni interfaccia", openConfig);
		items.push(_config);
	}
	if (!items.length) {
		//FS
		//dep		var _noChart = plugins.popupmenu.createMenuItem("Menù non disponibile...", null);
		var _noChart = menu.addMenuItem("Menù non disponibile...", null);
		items = [_noChart];
	}
	//dep	plugins.popupmenu.showPopupMenu(event.getSource(), items);
	menu.show(event.getSource());
}

/**
 * @properties={typeid:24,uuid:"C813A8CF-213F-466A-A1CC-168B532D079E"}
 */
function exportData() {
	if (isChartVisible()) {
		runLoader(true);
		var p = (forms[reportList[reportSelected]].getProjectsForExport && typeof forms[reportList[reportSelected]].getProjectsForExport == "function") ? forms[reportList[reportSelected]].getProjectsForExport() : projects.split("\n");
		//FS
		/** @type {Array<String>} */
		var v = null;
		/** @type {java.awt.chart} */
		var selChart = null;
		try {

			selChart = getSelectedChart();
			//dep	v = getSelectedChart().getBarLabels();
			v = selChart.getBarLabels();
		} catch (e) {
			selChart = getSelectedChart();
			//dep	v = getSelectedChart().getSampleLabels();
			v = selChart.getSampleLabels();
		}
		popup.openPopup(p, v);
//		try {
//			var data = popup.getSelected();
//			if (data.project && data.value > -1) {
//				forms[reportList[reportSelected]].exportData(data.project, data.value);
//			}
//		} catch (e) {
//			plugins.dialogs.showErrorDialog("Errore interno", "Testo del messaggio: " + e.message, "Ok");
//		}
//		runLoader(false);
	}
}

/**
 * @properties={typeid:24,uuid:"3D48036F-E821-4992-A787-BA6A770F23A4"}
 */
function saveChart() {
	
	/** @type {java.awt.chart} */
	var chart = getSelectedChart();
	var image = chart.getImage(chart.getWidth(), chart.getHeight());
	var name = forms[forms[reportList[reportSelected]].selected].title.toLowerCase().replace(/ /g, "_") + "-" + utils.dateFormat(new Date(), "yyyyMMdd(w)") + ".png";
	globals.utils_saveAsPng(image, name);
}

/**
 * @properties={typeid:24,uuid:"6F380715-028F-4346-B0F3-FD656256ACA9"}
 */
function isChartLoaded() {
	return (forms[reportList[reportSelected]] && forms[forms[reportList[reportSelected]].selected] && forms[forms[reportList[reportSelected]].selected].elements["chart"]) ? true : false;
}

/**
 * @properties={typeid:24,uuid:"B2E12BB3-F19C-4CC5-AAF3-6F8A2DCABAF0"}
 */
function isChartVisible() {
	return (forms[reportList[reportSelected]] && forms[forms[reportList[reportSelected]].selected] && forms[forms[reportList[reportSelected]].selected].elements["chart"] && forms[forms[reportList[reportSelected]].selected].elements["chart"].visible) ? true : false;
}

/**@return{RuntimeComponent}
 * @properties={typeid:24,uuid:"A230E1C3-343E-4E05-9F60-F51FD9B95FC4"}
 */
function getSelectedChart() {
	return forms[forms[reportList[reportSelected]].selected].elements["chart"];
}

/**
 * @properties={typeid:24,uuid:"A51A33A6-B67E-4BAF-97FF-6A785A5ACF96"}
 */
function runLoader(run) {
	elements.loader.visible = run;
	application.updateUI();
}

/**
 * @properties={typeid:24,uuid:"ABBB6D12-7FF2-454B-94B2-A29E8D34433E"}
 */
function indexOfCaseInsensitive(list, element) {
	for (var i = 0; i < list.length; i++) {
		if (element.toLowerCase() == list[i].toLowerCase()) {
			return i;
		}
	}
	return -1;
}

/**
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"F4E923ED-7A6B-4B50-A4F8-9152ADC9CC8B"}
 */
function loadConfig(firstShow, event) {

	var _form = event.getFormName();
	var _config = null
	if (_config == forms.nfx_interfaccia_gestione_salvataggi.load(forms[_form].controller.getName() + "_chartConfig")) {
		forms[_form].config = _config;
	} else {
		forms.nfx_interfaccia_gestione_salvataggi.save(forms[_form].config, forms[_form].controller.getName() + "_chartConfig");
	}
}

/**
 * @properties={typeid:24,uuid:"214781D2-9EFA-45F5-9372-349ED1F6DCD8"}
 */
function openConfig() {
	forms.report_anomalie_chartconfig.openPopup(reportList[reportSelected], isChartVisible());
	forms.nfx_interfaccia_gestione_salvataggi.save(forms[reportList[reportSelected]].config, forms[reportList[reportSelected]].controller.getName() + "_chartConfig");
	forms[reportList[reportSelected]].setupChart();
	forms[reportList[reportSelected]].drawChart();
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"4A686308-A454-4E95-AB12-6F75548C64C0"}
 */
function onProgettiChange(oldValue, newValue, event) {
	//FS
	//dep	var _o = (oldValue) ? oldValue.split("\n") : [];
	//dep 	var _n = (newValue) ? newValue.split("\n") : [];
	var _o = (oldValue) ? oldValue.toString().split("\n") : [];
	var _n = (newValue) ? newValue.toString().split("\n") : [];
	if (_n.length > _o.length) {
		for (var _i = 0; _i < _n.length; _i++) {
			if (_o.indexOf(_n[_i]) == -1) {
				if (!versions[_n[_i]]) versions[_n[_i]] = "off";
				if (advancedVersionChooser) forms.report_anomalie_versions.openPopup(controller.getName(), "versions", _n[_i]);
				break;
			}
		}
	} else {
		for (var _j = 0; _j < _o.length; _j++) {
			if (_n.indexOf(_o[_j]) == -1) {
				delete versions[_o[_j]];
				break;
			}
		}
	}

	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"0133D6E7-D48C-451C-B1F9-72C66815F584"}
 */
function openSelectionOptionsMenu(event) {
	var _items = [];
	//FS
	var menu = plugins.window.createPopupMenu();
	profiles = forms.nfx_interfaccia_gestione_salvataggi.load(controller.getName() + "_profiles");
if (!profiles || forms.nfx_json_serializer.stringify(profiles, null, null) == forms.nfx_json_serializer.stringify({ })) {

		profiles = { };
		//dep		_items.push(plugins.popupmenu.createMenuItem("<Nessun profilo>"));

		_items.push(menu.addMenuItem("<Nessun profilo>"));

	} else {
		for (var _p in profiles) {
			
			var subMenu = menu.addMenu(_p);
			//dep			var _l = plugins.popupmenu.createMenuItem("Carica", loadProfile);
			var _l = subMenu.addMenuItem("Carica", loadProfile);
			//			_l.setMethodArguments(_p);
			_l.methodArguments = [_p];
			//dep		var _d = plugins.popupmenu.createMenuItem("Cancella", deleteProfile);
			//dep		_d.setMethodArguments(_p);
			//dep		_items.push(plugins.popupmenu.createMenuItem(_p, [_l, _d]));
			var _d =subMenu.addMenuItem("Cancella", deleteProfile);
			_d.methodArguments=[_p];
//dep			_items.push(plugins.popupmenu.createMenuItem(_p, [_l, _d]));
		}
	}

//dep	_items.push(plugins.popupmenu.createMenuItem("-"));
	_items.push(menu.addSeparator());
	var _icon = (advancedVersionChooser) ? "media:///save16.png" : "media:///cancel16.png";
//dep	_items.push(plugins.popupmenu.createMenuItem("Versioni AS400", advancedVersionChooserUpdater, _icon));
//dep	_items.push(plugins.popupmenu.createMenuItem("Annulla selezione", uncheckAll));
//dep	_items.push(plugins.popupmenu.createMenuItem("Riepilogo...", projectsVersionsRecap));
//dep	_items.push(plugins.popupmenu.createMenuItem("Nuovo profilo...", newProfile));
	_items.push(menu.addMenuItem("Versioni AS400", advancedVersionChooserUpdater, _icon));
	_items.push(menu.addMenuItem("Annulla selezione", uncheckAll));
	_items.push(menu.addMenuItem("Riepilogo...", projectsVersionsRecap));
	_items.push(menu.addMenuItem("Nuovo profilo...", newProfile));
//dep	plugins.popupmenu.showPopupMenu(elements._selectionOptions, _items);
	menu.show(elements._selectionOptions);
}

/**@param {Number} index   
 * @param {Number} parentIndex
 * @param {Boolean} isSelected
 * @param {String} parentText
 * @param {String} menuText
 * @param {Object} _p
 * @properties={typeid:24,uuid:"10DBAA79-2ED3-4DB9-B559-8D935649B753"}
 */
function loadProfile(index,parentIndex,isSelected,parentText,menuText,_p) {
	var _v = profiles[_p];
	var _projects = [];
	for (var _pj in _v) {
		_projects.push(_pj);
	}
	projects = _projects.sort().join("\n");
	versions = _v;
}

/**@param {Number} index   
* @param {Number} parentIndex
* @param {Boolean} isSelected
* @param {String} parentText
* @param {String} menuText
* @param {Object} _p
 * @properties={typeid:24,uuid:"A104A234-3C49-415E-AC64-92E4631FDC4E"}
 */
function deleteProfile(index,parentIndex,isSelected,parentText,menuText,_p) {
	if (plugins.dialogs.showQuestionDialog("Eliminare?", "Eliminare il profilo \"" + _p + "\"?", "Sì", "No") == "Sì") {
		delete profiles[_p];
		forms.nfx_interfaccia_gestione_salvataggi.save(profiles, controller.getName() + "_profiles");
	}
}

/**
 * @properties={typeid:24,uuid:"92224C78-004D-45D2-B667-82045801B50E"}
 */
function advancedVersionChooserUpdater() {
	advancedVersionChooser = !advancedVersionChooser;
	//application.output(advancedVersionChooser);
}

/**
 * @properties={typeid:24,uuid:"F38EAA93-DAA6-4142-814D-C1D77C1AAA18"}
 */
function uncheckAll() {
	projects = "";
	versions = { };
}

/**
 * @properties={typeid:24,uuid:"28A47887-8C4E-41FF-BD18-93F1B180919F"}
 */
function projectsVersionsRecap() {
	var _message = "";
	if (projects) {
		_message = "Progetti/Versioni selezionate:\n";
		var _p = projects.split("\n");
		for (var _i = 0; _i < _p.length; _i++) {
			if (typeof versions[_p[_i]] == "string") {
				_message += _p[_i] + " - no AS400\n";
			} else {
				//FS
				/** @type {Array} */				
				var _v = versions[_p[_i]];
				for (var _j = 0; _j < _v.length; _j++) {
					_message += _p[_i] + " - " + databaseManager.getDataSetByQuery("ferrari", "select versione from k8_anagrafica_progetti_ver where codice_vettura = ?", [_v[_j]], 1).getValue(1, 1) + "\n";
				}
			}
		}
	} else {
		_message = "Nessun progetto selezionato!";
	}

	plugins.dialogs.showInfoDialog("Riepilogo progetti/versioni", _message, "Ok");
}

/**
 * @properties={typeid:24,uuid:"14DA680B-A432-46A0-BE38-F7142FADBC19"}
 */
function newProfile() {
	if (forms.nfx_json_serializer.stringify(versions, null, null) == forms.nfx_json_serializer.stringify({ }, null, null)){
		plugins.dialogs.showInfoDialog("Impossibile!","Seleziona almeno un progetto!", "Ok");
		return;
	}else{
	var _n = plugins.dialogs.showInputDialog("Nuovo profilo", "Inserisci un nome per il nuovo profilo");
	if (_n) {
		if (profiles[_n] && plugins.dialogs.showQuestionDialog("Sovrascrivere?", "Il profilo \"" + _n + "\" è già presente, sovrascrivere?", "Sì", "No") == "Sì") {
			profiles[_n] = versions;
			forms.nfx_interfaccia_gestione_salvataggi.save(profiles, controller.getName() + "_profiles");
		} else {
			profiles[_n] = versions;
			forms.nfx_interfaccia_gestione_salvataggi.save(profiles, controller.getName() + "_profiles");
			}
		}
	}
}
