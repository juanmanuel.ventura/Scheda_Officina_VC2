items:[
{
beanClassName:"javax.swing.JList",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_22\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JList\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>listProjectc<\/string> \
  <\/void> \
  <void property=\"selectionMode\"> \
   <int>0<\/int> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"150,95",
name:"listValue",
size:"80,80",
typeid:12,
uuid:"0D20C4DF-8372-40F9-BDBB-A92CEFB418C6"
},
{
anchors:6,
imageMediaID:"BB0D781F-0317-4161-B5E3-7A1E7B7AE36F",
location:"230,240",
mediaOptions:10,
name:"cancel0",
onActionMethodID:"8B683FE9-0E38-42E5-81CF-C7D6D0483652",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
size:"20,20",
typeid:7,
uuid:"21BC9120-3E65-41DC-AEF1-2EDC4F59F6A4"
},
{
beanClassName:"javax.swing.JList",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_22\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JList\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>listProject<\/string> \
  <\/void> \
  <void property=\"selectionMode\"> \
   <int>0<\/int> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"20,95",
name:"listProject",
size:"80,80",
typeid:12,
uuid:"6ADF13B0-1227-4B00-B68F-0EC94CBC37D7"
},
{
anchors:13,
beanClassName:"javax.swing.JScrollPane",
formIndex:-1,
location:"10,35",
name:"scrollProject",
size:"100,200",
typeid:12,
uuid:"70B0292E-920F-4795-9B58-C61BA03B88D8"
},
{
location:"10,10",
mediaOptions:14,
size:"100,20",
styleClass:"form",
tabSeq:-1,
text:"Progetto",
transparent:true,
typeid:7,
uuid:"ADE4C74C-EEAD-4991-B009-62E3B0BF77FC"
},
{
anchors:6,
imageMediaID:"B2359D26-06A5-4F80-A5AA-67C1EF72F90F",
location:"200,240",
mediaOptions:10,
name:"save1",
onActionMethodID:"8B683FE9-0E38-42E5-81CF-C7D6D0483652",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
size:"20,20",
typeid:7,
uuid:"E34C4503-1F3E-4A16-A354-14E1312EAF20"
},
{
anchors:15,
beanClassName:"javax.swing.JScrollPane",
formIndex:-2,
location:"120,35",
name:"scrollValue",
size:"140,200",
typeid:12,
uuid:"E562EBEA-9CC5-4458-966C-D58FA6532986"
},
{
height:270,
partType:5,
typeid:19,
uuid:"EF1BA83A-8ACF-456E-9AFC-AC443A5C4D53"
},
{
location:"120,10",
mediaOptions:14,
size:"140,20",
styleClass:"form",
tabSeq:-1,
text:"Valore",
transparent:true,
typeid:7,
uuid:"F5872F33-288D-46CE-A6DA-FDA36EB8A50D"
}
],
name:"report_anomalie_popup",
navigatorID:"-1",
onLoadMethodID:"34C7B512-7D66-43DC-B828-BB16458782CC",
paperPrintScale:100,
showInMenu:true,
size:"270,270",
styleName:"keeneight",
titleText:"Export",
typeid:3,
uuid:"E204E2D8-6662-4810-9186-68E5452D6FB3"