/**
 * @properties={typeid:35,uuid:"51CE2D25-BE89-4A02-9754-B4D86AF7829A",variableType:-4}
 */
var projectList = null;

/**
 * @properties={typeid:35,uuid:"D10FA908-7872-4537-8333-5A9820F58699",variableType:-4}
 */
var selected = { project: null, value: -1 };

/**
 * @properties={typeid:35,uuid:"A06B7651-F6F8-4270-BB9A-D6E467B41222",variableType:-4}
 */
var valueList = null;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"34C7B512-7D66-43DC-B828-BB16458782CC"}
 */
function onLoad(event) {

	//FS variabile non usata,cosa fare?
	//SAuc
	//var container = forms.report_anomalie_container;
	
	
	
	//dep	elements.scrollProject.getViewport().add(elements.listProject);
	//dep 	elements.scrollValue.getViewport().add(elements.listValue);
	/** @type {java.awt.Component} */
	var listProject = elements.listProject;
	/** @type {java.awt.Component} */
	var listValue = elements.listValue;
	elements.scrollProject.getViewport().add(listProject);
	elements.scrollValue.getViewport().add(listValue);
}

/**
 * @properties={typeid:24,uuid:"DB1688FD-DCF7-4DA2-9FFC-67D23A80FDBC"}
 */
function openPopup(projects, values) {
	projectList = projects;
	valueList = values;
	setSelected(valueList);
	setListData();
	controller.show("Export");
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"8B683FE9-0E38-42E5-81CF-C7D6D0483652"}
 */
function closePopup(event) {
	setSelected(utils.stringToNumber(event.getElementName()));

	try {
		var data = forms.report_anomalie_container.popup.getSelected();
		if (data.project && data.value > -1) {
			forms[forms.report_anomalie_container.reportList[forms.report_anomalie_container.reportSelected]].exportData(data.project, data.value);
		}
	} catch (e) {
		plugins.dialogs.showErrorDialog("Errore interno", "Testo del messaggio: " + e.message, "Ok");
	}
	forms.report_anomalie_container.runLoader(false);
	
	//application.closeFormDialog();

	var w = controller.getWindow();
	w.destroy();
}

/**
 * @properties={typeid:24,uuid:"D614FB77-97CB-4BE3-BC3A-7EE14547034F"}
 */
function getSelected() {
	return selected;
}

/**
 * @properties={typeid:24,uuid:"7238938D-A465-47E6-BD0F-1CB31B8EC68B"}
 */
function setSelected(save) {
	selected.project = (save) ? elements.listProject.getSelectedValue() : null;
	selected.value = (save) ? elements.listValue.getSelectedIndex() : -1;
}

/**
 * @properties={typeid:24,uuid:"B108AD92-F19A-45F6-8F10-843134EB55F8"}
 */
function setListData() {
	elements.listProject.setListData(projectList);
	elements.listValue.setListData(valueList);
}
