/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"ACE631AE-CA19-428B-B883-76BA6AFA3885"}
 */
var AV = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1977328F-F401-4559-B398-49C226DFEFCF"}
 */
var PS = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1A174C6B-6BA9-46AC-B7D8-9E24BCF8172B"}
 */
var SOP = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"41D1EA8D-0842-4E5B-8442-A539D6A625B2"}
 */
var SOS = "";

/**
 * @properties={typeid:24,uuid:"86100A4D-ADFB-49E0-AE2A-E1AF828903A1"}
 */
function openPopup(delayLables){
	for(var d in delayLables){
		forms[controller.getName()][delayLables[d]] = d;
	}
	controller.show("Delta");
}

/**
 * @properties={typeid:24,uuid:"90D8B8C3-0699-48CA-99C6-CA221C7F47D0"}
 */
function closePopup(event) {
	var d = ["SOS","SOP","PS","AV"];
	var f = [forms.report_cumulate.controller.getName(),forms.report_kpi.controller.getName()];
	if(utils.stringToNumber(event.getElementName())){
		var delayLables = new Object();
		for(var i=0;i<d.length;i++){
			delayLables[forms[controller.getName()][d[i]]] = d[i];
		}
		for(var i2=0;i2<f.length;i2++){
			forms[f[i2]].delayLables = delayLables;
		}
	}
	
	//application.closeFormDialog();
	
	var w=controller.getWindow();
	w.destroy();
}
