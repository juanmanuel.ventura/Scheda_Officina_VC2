items:[
{
horizontalAlignment:0,
labelFor:"end",
location:"200,10",
mediaOptions:14,
onActionMethodID:"A06FBFC1-8A5B-44B6-AF40-3A9FCC98CDB0",
showClick:false,
showFocus:false,
size:"20,20",
styleClass:"form",
text:"D",
toolTipText:"Carica classi di default",
transparent:true,
typeid:7,
uuid:"251664B2-0158-49EB-8A54-D7E1B1030EB4"
},
{
anchors:6,
imageMediaID:"B2359D26-06A5-4F80-A5AA-67C1EF72F90F",
location:"160,240",
mediaOptions:10,
name:"save1",
onActionMethodID:"DEC0A063-AC97-4AA0-81A2-875F1792D140",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
size:"20,20",
typeid:7,
uuid:"36DDA8A4-0350-49DC-B892-A5A8937040A6"
},
{
horizontalAlignment:0,
labelFor:"end",
location:"140,10",
mediaOptions:14,
onActionMethodID:"2C0A7AE2-CD29-45DE-BF5F-41621BEE9C2D",
showClick:false,
showFocus:false,
size:"20,20",
styleClass:"form",
text:"+",
toolTipText:"Aggiungi classe",
transparent:true,
typeid:7,
uuid:"47318507-4209-4537-A6C7-2DA0B1D8E2E1"
},
{
anchors:6,
imageMediaID:"BB0D781F-0317-4161-B5E3-7A1E7B7AE36F",
location:"190,240",
mediaOptions:10,
name:"cancel0",
onActionMethodID:"DEC0A063-AC97-4AA0-81A2-875F1792D140",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
size:"20,20",
typeid:7,
uuid:"51C521B2-2C06-4432-8179-CDBC6B8B6AC7"
},
{
horizontalAlignment:0,
labelFor:"end",
location:"70,10",
mediaOptions:14,
size:"20,20",
styleClass:"form",
tabSeq:-1,
text:"a",
transparent:true,
typeid:7,
uuid:"58C7597E-647E-4E87-AEB1-220184267B82"
},
{
height:270,
partType:5,
typeid:19,
uuid:"61366E72-2A1F-493C-9DA6-842BD5472AD9"
},
{
beanClassName:"javax.swing.JList",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_22\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JList\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"name\"> \
   <string>bean_545<\/string> \
  <\/void> \
  <void property=\"selectionMode\"> \
   <int>0<\/int> \
  <\/void> \
 <\/object> \
<\/java> \
",
formIndex:1,
location:"20,50",
name:"listClasses",
size:"80,80",
typeid:12,
uuid:"6594E120-FF35-4DF1-A227-DC81051B7614"
},
{
horizontalAlignment:0,
labelFor:"end",
location:"160,10",
mediaOptions:14,
onActionMethodID:"E3AE8E83-E64E-4B0A-A88D-B861858251B3",
showClick:false,
showFocus:false,
size:"20,20",
styleClass:"form",
text:"-",
toolTipText:"Cancella classe",
transparent:true,
typeid:7,
uuid:"A5F71076-5EF1-4129-BD17-65D6A7CD5C6B"
},
{
horizontalAlignment:0,
labelFor:"start",
location:"10,10",
mediaOptions:14,
size:"20,20",
styleClass:"form",
tabSeq:-1,
text:"Da",
transparent:true,
typeid:7,
uuid:"B460644D-9C30-4AFD-B66E-E5F851B0AACE"
},
{
dataProviderID:"start",
horizontalAlignment:0,
location:"30,10",
name:"start",
size:"40,20",
text:"Start",
typeid:4,
uuid:"B5838905-282F-419A-9462-AF6BB5492482"
},
{
dataProviderID:"end",
horizontalAlignment:0,
location:"90,10",
name:"end",
size:"40,20",
text:"End",
typeid:4,
uuid:"CCB828F5-BBF4-4FEA-BA2A-4FCD2C4D9BE7"
},
{
horizontalAlignment:0,
labelFor:"end",
location:"180,10",
mediaOptions:14,
onActionMethodID:"52CBB308-C023-411D-9B5A-5FD8557D464A",
showClick:false,
showFocus:false,
size:"20,20",
styleClass:"form",
text:"^",
toolTipText:"Aggiorna classe",
transparent:true,
typeid:7,
uuid:"CFAF8E70-EA2A-4819-8D30-D530B6C299B5"
},
{
anchors:15,
beanClassName:"javax.swing.JScrollPane",
formIndex:-2,
location:"10,35",
name:"scrollClasses",
size:"210,200",
typeid:12,
uuid:"FAFBD58C-FE39-461F-A1D2-618DFCEF569E"
}
],
name:"report_anomalie_popup_classes",
navigatorID:"-1",
onLoadMethodID:"494BE76F-FE34-4582-B560-7310A05413B1",
paperPrintScale:100,
showInMenu:true,
size:"230,270",
styleName:"keeneight",
titleText:"Classi",
typeid:3,
uuid:"1FE1487C-AE80-4F7A-ABCD-A44D04A4EB14"