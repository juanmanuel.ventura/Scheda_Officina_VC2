/** @type {Array} *
 * @properties={typeid:35,uuid:"9FB10F40-C1CC-49DE-B573-3500AE1A7CD9",variableType:-4}
 */
var classes = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"7477A3B9-1176-4D8D-9AB4-12283EB85068"}
 */
var end = "";

/**
 * @properties={typeid:35,uuid:"51FE7BF6-2979-45E7-AFBB-861CFE8B929A",variableType:-4}
 */
var form = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"FF007D54-2259-4B2E-8E83-CFA9CBFCF1DA"}
 */
var id = "";

/**
 * @properties={typeid:35,uuid:"DFEEEC8F-5FBD-4AA3-943F-117D2F17E8F8",variableType:-4}
 */
var nulls = { start: "<", end: ">" };

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"062EB8B0-FB58-4DE2-B28B-C4086FD969B2"}
 */
var start = "";

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"494BE76F-FE34-4582-B560-7310A05413B1"}
 */
function onLoad(event) {
	//dep	elements.scrollClasses.getViewport().add(elements.listClasses);
	//dep	elements.listClasses.addListSelectionListener(setRefs);
	/** @type {java.awt.Component} */
	var listClasses = elements.listClasses;
	elements.scrollClasses.getViewport().add(listClasses);
	/** @type {javax.swing.event.ListSelectionListener} */	
	var ref=setRefs;
	elements.listClasses.addListSelectionListener(ref);
}

/**
 * @properties={typeid:24,uuid:"0B4D3A49-C3D1-4992-9FA6-DEC191A5B3F4"}
 */
function openPopup(formName, formClasses) {
	form = formName;
	classes = formClasses;
	elements.listClasses.setListData(fillList());
	controller.show("Classes");
}

/**
 * @properties={typeid:24,uuid:"DEC0A063-AC97-4AA0-81A2-875F1792D140"}
 */
function closePopup(event) {
	if (utils.stringToNumber(event.getElementName())) {
		forms.nfx_interfaccia_gestione_salvataggi.save(classes, forms[form].controller.getName());
		forms[form].classes = classes;
	}

	//application.closeFormDialog();

	var w = controller.getWindow();
	w.destroy();
}

/**
 * @properties={typeid:24,uuid:"4A3E784E-509D-49EA-B2B4-367C3B9CDDE7"}
 */
function fillList() {
	var list = new Array();
	for (var i = 0; i < classes.length; i++) {
		var s = getStart(classes[i]);
		var e = getEnd(classes[i]);
		list.push("Classe " + classes[i].id + " - da " + s + " a " + e);
	}
	return list;
}

/**
 * @properties={typeid:24,uuid:"19355F36-DCD2-4086-99D4-91C98F25649B"}
 */
function setRefs() {
	var index = elements.listClasses.getSelectedIndex();
	if (index > -1) {
		start = getStart(classes[index]);
		end = getEnd(classes[index]);
	}
}

/**
 * @properties={typeid:24,uuid:"F773A939-7F5B-4CB4-AD6C-8A6877DBD623"}
 */
function setStart() {
	return (start != nulls.start) ? utils.stringToNumber(start) : null;
}

/**
 * @properties={typeid:24,uuid:"6F398C89-A3DE-433E-BFE1-759FDD90EFE2"}
 */
function getStart(item) {
	return (item.start !== null) ? utils.numberFormat(item.start, "##########") : nulls.start;
}

/**
 * @properties={typeid:24,uuid:"B7B0E76E-1850-4F7D-BF08-E4C43FD19256"}
 */
function setEnd() {
	return (end != nulls.end) ? utils.stringToNumber(end) : null;
}

/**
 * @properties={typeid:24,uuid:"28E96772-AEA5-442C-B12A-F554B337F00F"}
 */
function getEnd(item) {
	return (item.end !== null) ? utils.numberFormat(item.end, "##########") : nulls.end;
}

/**
 * @properties={typeid:24,uuid:"2C0A7AE2-CD29-45DE-BF5F-41621BEE9C2D"}
 */
function addItem() {
	classes.push({ id: null, start: setStart(), end: setEnd() });
	updateIdList();
	elements.listClasses.setListData(fillList());
}

/**
 * @properties={typeid:24,uuid:"E3AE8E83-E64E-4B0A-A88D-B861858251B3"}
 */
function deleteItem() {
	var index = elements.listClasses.getSelectedIndex();
	if (index > -1) {
		classes.splice(index, 1);
	}
	updateIdList();
	elements.listClasses.setListData(fillList());
	elements.listClasses.setSelectedIndex(index);
}

/**
 * @properties={typeid:24,uuid:"52CBB308-C023-411D-9B5A-5FD8557D464A"}
 */
function updateItem() {
	var index = elements.listClasses.getSelectedIndex();
	if (index > -1) {
		classes[index].start = setStart();
		classes[index].end = setEnd();
	}
	elements.listClasses.setListData(fillList());
}

/**
 * @properties={typeid:24,uuid:"48000A20-0960-4692-8808-D6A34958B6A0"}
 */
function updateIdList() {
	for (var i = 0; i < classes.length; i++) {
		classes[i].id = i + 1;
	}
}

/**
 * @properties={typeid:24,uuid:"A06FBFC1-8A5B-44B6-AF40-3A9FCC98CDB0"}
 */
function setDefaultClasses() {
	classes = forms[form].defaultClasses.slice();
	elements.listClasses.setListData(fillList());
}
