/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"5DF5FA70-784E-4FB1-A0F5-4257CC95718F"}
 */
var AV = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"679628C0-F949-4A08-B926-D14CCEE9A581"}
 */
var PS = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"E89796AA-F697-4F68-A119-7F5C72BBB87B"}
 */
var SOP = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D2825581-F7CB-4562-B2E1-C72DF045FB0E"}
 */
var SOS = "";

/**
 * @properties={typeid:24,uuid:"826E88B4-A69B-4F76-9644-EA56C11A8172"}
 */
function openPopup(delayLables){
	for(var d in delayLables){
		forms[controller.getName()][delayLables[d]] = d;
	}
	controller.show("Delta");
}

/**
 * @properties={typeid:24,uuid:"FD9F78DB-51C7-4214-AF8C-70B6DD25B24A"}
 */
function closePopup(event) {
	var d = ["SOS","SOP","PS","AV"];
	var f = [forms.report_cumulate.controller.getName(),forms.report_kpi.controller.getName()];
	if(utils.stringToNumber(event.getElementName())){
		var delayLables = new Object();
		for(var i=0;i<d.length;i++){
			delayLables[forms[controller.getName()][d[i]]] = d[i];
		}
		for(var i2=0;i2<f.length;i2++){
			forms[f[i2]].delayLables = delayLables;
		}
	}
	
	//application.closeFormDialog();
	
	var w=controller.getWindow();
	w.destroy();
}
