/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"BD4D6B73-1910-4527-A0D5-6655BB86C692"}
 */
var _field = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"277D68CD-9DBF-4D6C-BA79-75F8DF739C8C"}
 */
var _form = "";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"7F12F2CD-6A77-446C-9B48-B397693F0168",variableType:4}
 */
var _mode = 0;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"8EB6960C-B045-49E0-B3A5-7C3A4749127C"}
 */
var _project = "";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"6CBE3EE2-AF5E-421E-9562-E2989C82478C",variableType:4}
 */
var _uncategorized = 0;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A6EA1B54-FE8D-4D8F-8BD0-279C88494E35"}
 */
var _versions = "";

/**
 * @properties={typeid:24,uuid:"FE3B95BB-55ED-4CC9-A8C5-A8AF9F637E75"}
 */
function openPopup(_fo,_fi,_p){
	_form = _fo;
	_field = _fi;
	elements._project_label.text = "ricerca per tutte le versioni del progetto: " + _p;
	globals.vc2_currentProgetto = _p;
	_project = _p;
	
	_mode = 0;
	_uncategorized = 0;
	_versions = selectAll().join("\n");
	searchMode(false);
	
	controller.show("versions");
}

/**
 * @properties={typeid:24,uuid:"D2918236-C692-469E-AA2C-C59A92FB0078"}
 */
function searchMode(_forVersion){
	elements._versions.enabled = _forVersion;
	elements._versions_label.enabled = _forVersion;
	elements._uncategorized.enabled = _forVersion;
	
	elements._project_label.enabled = !_forVersion;
}

/**
 * @properties={typeid:24,uuid:"2651890E-CBFB-4281-891D-51DABC77C6AD"}
 */
function selectAll(){
	var _vl = application.getValueListItems("static$versioni_from_progetti$no_null");
	var _rv = _vl.getColumnAsArray(2);
	return _rv.sort();
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"E5CFA463-CA84-42F5-905A-96E468B0A426"}
 */
function onModeChange(oldValue, newValue, event) {
	var _c = application.getValueListItems("static$versioni_from_progetti$no_null").getMaxRowIndex();
	if(!_c){
		_mode = oldValue;
		return true;
	}
	
	if(newValue){
		searchMode(true);
	}else{
		searchMode(false);
	}
	return true
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"D8A0F1AE-6C59-4125-B1BD-E694AA56181B"}
 */
function save(event) {
	forms[_form][_field][_project] = (_mode != 0 && _uncategorized == 0) ? _versions.split("\n") : "off";
	
	//application.closeFormDialog("versions");
	
	var w=controller.getWindow();
	w.destroy();
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"1EF889FC-888D-40DF-99C5-5EF9B6968C87"}
 */
function onVersioneChange(oldValue, newValue, event) {
	var _c = application.getValueListItems("static$versioni_from_progetti$no_null").getMaxRowIndex();
	if(_c == _versions.split("\n").length){
		_uncategorized = 1;
		elements._uncategorized.visible = true;
	}else{
		_uncategorized = 0;
		elements._uncategorized.visible = false;
	}
	return true;
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"EB0BA4D5-AB1E-4735-AFAC-5FC63A8D677F"}
 */
function onUncategorizedChange(oldValue, newValue, event) {
	return true;
}
