/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B6C1F81A-906F-413A-A334-6B063222295E"}
 */
var _message = "";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"D6B0BE85-6002-4B75-A3C6-AC82C3454B9C",variableType:4}
 */
var _notShowNextTime = 0;

/**
 * @properties={typeid:35,uuid:"1B392A38-FEAB-49CD-BCAD-64965841AE3D",variableType:-4}
 */
var _result = null;

/**
 * @properties={typeid:35,uuid:"5DEA2B5C-A4F8-44C6-9A18-10893E40FA36",variableType:-4}
 */
var _savingValueForm = null;

/**
 * @properties={typeid:24,uuid:"C3F0F08C-BB3B-4AB7-9463-13054823D9AF"}
 */
function openPopup(savingValueForm){
	_message = buildDisplayMessage();
	_savingValueForm = savingValueForm;
	_result = false;
	controller.show("showConfigWarning");
}

/**
 * @properties={typeid:24,uuid:"4A89C800-270E-438F-98F1-4FED6F62A1EB"}
 */
function buildDisplayMessage(){
	var _m1 = "Si avvisano gli utenti del modulo \"<b>Report anomalie</b>\" che da oggi è presente un nuovo pannello di configurazione grafica differenziato per ogni modulo in esso contenuto.<br>Tale pannello è accessibile tramite la voce \"<b>Opzioni interfaccia</b>\" presente nel menù \"<b>Tools</b>\" una volta selezionato il report desiderato.<br>Le impostazioni configurate saranno salvate in un Vostro profilo così da ritrovarle qualunque sia la postazione di accesso al sistema.";
	var _m2 = "Aprire la configurazione per il modulo selezionato?";
	return "<html><body><div style='font-size:8px; text-align:justify;'>" + _m1 + "</div><div style='margin-top:20px; font-size:8px; text-align:justify;'>" + _m2 + "</div></body></html>";
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"09216015-76ED-4B6F-AEA9-8AFD06C8CE57"}
 */
function setShowNextTime(oldValue, newValue, event) {
	var _value = (newValue) ? "no" : "yes";
	forms.nfx_interfaccia_gestione_salvataggi.save(_value,_savingValueForm);
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"2780F0C3-A896-4124-9B18-8AF8ACC0873B"}
 */
function setResult(event) {
	_result = (event.getElementName() == "_yes") ? true : false;
	
	//application.closeFormDialog();
	
	var w=controller.getWindow();
	w.destroy();
}

/**
 * @properties={typeid:24,uuid:"B3A481A2-D4E8-48F0-A65E-774701AAE10C"}
 */
function getResult(){
	return _result;
}
