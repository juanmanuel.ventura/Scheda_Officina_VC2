/**
 * @properties={typeid:35,uuid:"E194C976-7F6C-4E51-8D0B-54A76F1DADA1",variableType:-4}
 */
var config = {
	attributes: { type: "line", id: "getVList" },
	bgc1: "#cccccc", bgc2: "#ffffff",
	grid: "#ff0000",
	fntx: "Verdana,0,10", fnty: "Verdana,0,10",
	styles: { }
};

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"F71746B9-A920-46A9-80CD-78C8883C098A",variableType:4}
 */
var cumulative = 1;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"E6D08DB0-D725-4466-91A6-88C13A84CF4B"}
 */
var def_reason = "";

/**
 * @properties={typeid:35,uuid:"8D101CC4-EC56-45F1-AC6E-C1A81C47D701",variableType:-4}
 */
var delayLables = {
	"0": "SOS",
	"-3": "SOP",
	"-6": "PS",
	"-11": "AV"
};

/**
 * @properties={typeid:35,uuid:"D5509D70-7C3F-4946-85BF-B2E9E6251A25",variableType:-4}
 */
var exportAlternatives = {
	"Totali": "%",
	"Aperte": "APERTA",
	"Chiuse": "CHIUSA"
};

/**
 * @properties={typeid:35,uuid:"D0286315-FB26-4BEC-9394-84EE1AC665FB",variableType:-4}
 */
var filterNames = ["type", "subtype", "group", "subgroup", "severity", "def_reason"];

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"43D6CC4F-5DE8-4D9F-8302-3860AC302E7B"}
 */
var group = "";

/**
 * @properties={typeid:35,uuid:"F2B74202-EA87-4F70-BFF5-CC4B56C37920",variableType:-4}
 */
var legendLabels = {
	"base": " - Totali",
	"open": " - Aperte",
	"closed": " - Chiuse"
};

/**
 * @properties={typeid:35,uuid:"373EEFFB-8DF5-4317-9404-49D788AE26CB",variableType:-4}
 */
var offset = { start: -44, end: +26 };

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1FE7C711-4E22-4372-9753-35C112DAEF20"}
 */
var quotes = "No";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"9A56483B-CEE1-4C3D-8733-D4C95CEAFCF3"}
 */
var selected = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"5E666FCD-0F46-4CD0-AEE1-E786E39ED80E"}
 */
var severity = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"42F2A01F-1CC5-42F1-A788-25D6E303A940"}
 */
var status = "base\nclosed";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"CA25E473-E56E-40D8-BF4F-0571D39702D2"}
 */
var subgroup = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"6C1EF6D4-D19E-41CC-9BFB-F21725496E6C"}
 */
var subtype = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"301443BA-07FD-44EF-ABFB-201FB36E5795"}
 */
var totals = "Si";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A80AB142-D5D5-4D64-8C4F-A0BB35DBC2B5"}
 */
var type = "";

/**
 * @properties={typeid:24,uuid:"97CEBE8F-CC50-4AB6-B37D-82D450CDC13D"}
 */
function getVList() {
	var _list = [];
	var _projects = application.getValueListArray("progetti");
	for (var i = 0; i < _projects.length; i++) {
		if (_projects[i]) {
			for (var j in legendLabels) {
				_list.push(_projects[i] + legendLabels[j]);
			}
		}
	}
	return _list;
}

/**
 * @properties={typeid:24,uuid:"295EFA64-91D5-4769-827F-340064B08C9B"}
 */
function nfx_isProgram() {
	return false;
}

/**
 * @properties={typeid:24,uuid:"4D961377-E824-47CE-993C-5CE83959357C"}
 */
function getCheckDate(p) {
	var query = "select (extract(year from DATA_RIFERIMENTO) * 12) + extract(month from DATA_RIFERIMENTO) from K8_DATE_RIFERIMENTO where PROGETTO = ? and TIPO = ?";
	var args = [p, "SOS"];
	var date = databaseManager.getDataSetByQuery('ferrari', query, args, 1).getValue(1, 1);
	return date || ( (new Date()).getFullYear() * 12) + 1;
}

/**
 * @properties={typeid:24,uuid:"E4D40068-D5D8-4171-874A-C42DAA5DE802"}
 */
function getData(p, v, step) {
	if (!forms[selected].query[step] || !forms[selected].args[step]) throw "Content query and/or args not detected";
	var date = getCheckDate(p);
	/** @type {String} */
	var query = forms[selected].query[step];
	var args = forms[selected].args[step];
	//Inizio parte di filtraggio
	var filters = {
		type: type,
		subtype: subtype,
		group: group,
		subgroup: subgroup,
		severity: severity,
		def_reason: def_reason
	};
	var adding = forms[selected].addingParts;
	var replaceFinal = forms[selected].replacePart;
	var argsFinal = new Array();
	for (var i = 0; i < filterNames.length; i++) {
		if (filters[filterNames[i]]) {
			replaceFinal += " " + adding[filterNames[i]];
			argsFinal.push(filters[filterNames[i]]);
		}
	}
	//Aggiunta parte relativa alla gestione delle versioni AS400
	if (v) replaceFinal += " " + forms[selected].versionSpecificPart.replace("**VERSIONS_HERE**", v);
	query = query.replace(forms[selected].replacePart, replaceFinal);
	args = [date].concat(args).concat([p]).concat(argsFinal);
	//Fine parte di filtraggio
	//application.output(query + "\n" + args);
	var ds = databaseManager.getDataSetByQuery('ferrari', query, args, -1);
	var offsets = ds.getColumnAsArray(1);
	var counts = ds.getColumnAsArray(2);
	//se c'è un null è all'ultimo posto, quindi se l'ultimo è un null lo tolgo e sommo al precedente
	if (offsets.length > 1 && offsets[offsets.length - 1] == null) {
		counts[counts.length - 2] += counts[counts.length - 1];
		offsets.length = offsets.length - 1;
		counts.length = counts.length - 1;
	}
	var maxNotnullOffset = offsets[offsets.length - 1];
	var added = new Array();
	var first = Math.min(offset.start, offsets[0]);
	var last = Math.max(offset.end, offsets[offsets.length - 1]);
	for (var i2 = first; i2 <= last; i2++) {
		if (offsets.indexOf(i2) == -1) {
			offsets.push(i2);
			added.push(i2);
		}
	}
	offsets.sort(globals.utils_sortNumbers);
	for (var i3 = 0; i3 < added.length; i3++) {
		var index = offsets.indexOf(added[i3]);
		counts.splice(index, 0, 0);
	}
	var max = 0;
	if (cumulative == 1) {
		for (var i4 = 1; i4 < counts.length; i4++) {
			counts[i4] += counts[i4 - 1];
		}
		max = counts[counts.length - 1];
	} else {
		for (var i5 = 1; i5 < counts.length; i5++) {
			if (counts[i5] > max) max = counts[i5];
		}
	}
	var display = { offset: maxNotnullOffset, count: counts[offsets.indexOf(maxNotnullOffset)] };
	//Riporto la dimensione della serie a quella standard
	counts = counts.slice(offsets.indexOf(offset.start), offsets.indexOf(offset.end) + 1);
	offsets = offsets.slice(offsets.indexOf(offset.start), offsets.indexOf(offset.end) + 1);
	return (ds.getMaxRowIndex() > 0) ? { offsets: offsets, counts: counts, max: max, display: display } : null;
}

/**
 *
 * @properties={typeid:24,uuid:"3B80415A-4D8B-4287-8EDF-B6F2D5DB48B2"}
 */
function setupChart() {
	//FS
	//Ho cambiato utils._getJColor
	config = {
		attributes: { type: "line", id: "getVList" },
		bgc1: "#cccccc", bgc2: "#ffffff",
		grid: "#ff0000",
		fntx: "Verdana,0,10", fnty: "Verdana,0,10",
		styles: { }
	};
	/** @type {java.awt.chart} */
	var chart = forms[selected].elements.chart;
	if (!chart) throw "Selected form must have a chart element called \"chart\"";
	chart.setZoomOn(true);

	//dep	chart.setChartBackground(globals.utils_getJColor(config.bgc1));
	//dep	chart.setChartBackground2(globals.utils_getJColor(config.bgc2));
	//	chart.background=globals.utils_getJColor(config.bgc1);
	//	chart.background2=globals.utils_getJColor(config.bgc2);
	chart.setAutoLabelSpacingOn(true);
	chart.setSampleLabelsOn(true);

	chart.setValueLinesOn(true);

	chart.setLabelAngle("valueLabelAngle", 45);
	chart.setFont("valueLabelFont", new java.awt.Font("Verdana", java.awt.Font.PLAIN, 9));

	chart.setLegendOn(true);
	chart.setLegendPosition(2);
	chart.setLegendBoxSizeAsFont(true);
	chart.setSampleAxisRange(offset.start, offset.end);
	if (forms[selected].title) {
		chart.setTitleOn(true);
		chart.setTitle(forms[selected].title + " - " + utils.dateFormat(new Date(), forms.report_anomalie_container.chartTitleDateFormat));
	}

	chart.setFont("sampleLabelFont", globals.utils_getJFont(config.fntx));
	chart.setFont("rangeLabelFont", globals.utils_getJFont(config.fnty));
}

/**
 * @properties={typeid:24,uuid:"4DD8F10D-D786-4C04-9A7D-58C08A8D51FC"}
 */
function drawChart() {
	//FS
	/** @type {java.awt.chart} */
	var chart = forms[selected].elements.chart;
	if (!chart) throw "Selected form must have a chart element called \"chart\"";
	var projects = forms.report_anomalie_container.projects;
	var versions = forms.report_anomalie_container.versions;
	if (projects) {
		var p = projects.split("\n");
		var data = new Array();
		var legend = new Array();
		for (var i = 0; i < p.length; i++) {
			var v = (typeof versions[p[i]] == "string") ? null : versions[p[i]].join("','");
			if (status) {
				var s = status.split("\n");
				for (var j = 0; j < s.length; j++) {
					var d = getData(p[i], v, s[j]);
					if (d) {
						legend.push(p[i] + legendLabels[s[j]]);
						data.push(d);
					}
				}
			}
		}
		if (data.length > 0) {
			var max = 0;
			chart.setSeriesCount(data.length);
			var offsetLabels = data[0].offsets.slice();
			for (var i2 = 0; i2 < offsetLabels.length; i2++) {
				offsetLabels[i2] = (delayLables[offsetLabels[i2]]) ? delayLables[offsetLabels[i2]] + "\n" + offsetLabels[i2] : offsetLabels[i2]
			}
			chart.setSampleLabels(offsetLabels);
			chart.setSampleCount(offsetLabels.length);
			if (quotes != "No") {
				chart.setValueLabelStyle(0);
				chart.setValueLabelsOn(true);
			} else {
				chart.setValueLabelStyle(3);
				chart.setValueLabelsOn(true);
			}
			for (var i3 = 0; i3 < data.length; i3++) {
				if (max < data[i3].max) {
					max = data[i3].max;
				}
				if (config.styles[legend[i3]]) {
					//FS
					//dep chart.setSampleColor(i, globals.utils_getJColor(config.styles[legend[i3]][0]));
					chart.setSampleColor(i, globals.utils_getJColor(config.styles[legend[i3]][0]).toString());

					chart.setLineWidth(i, config.styles[legend[i3]][1]);
					chart.setLineStroke(i, config.styles[legend[i3]][2]);
				} else {
					//dep	chart.setSampleColor(i3, globals.utils_getJColor(forms.report_anomalie_container.colors[i3]));
					chart.setSampleColor(i3, globals.utils_getJColor(forms.report_anomalie_container.colors[i3]));

					chart.setLineWidth(i3, 2);
					chart.setLineStroke(i3, [0]);
				}
				chart.setSampleValues(i3, data[i3].counts);
			}
			var n = Math.pow(10, Math.floor(Math.log(max) / Math.log(10)));
			var m = max - (max % n) + n;
			chart.setRange(0, m);

			for (var i4 = 0; i4 < data.length; i4++) {
				var display = data[i4].display;
				var t = display.count / m;
				var x = ( (i4 % 2 == 0) ? 0.55 : 0.7) + (i4 / 100);
				var y = Math.abs(0.9 - t);
				var o = Math.min(display.offset, offset.end);
				var l = (totals == "No") ? null : "Curva: " + legend[i] + "\nTotale: " + display.count + "\nUltimo indice: " + display.offset;
				chart.setLabel("label_" + i4, l, x, y, i4, data[i4].offsets.indexOf(o));
			}

			/* -- Soluzione alternativa a 'Fumetti', da implementare l'associazione col controllo grafico.
			 var l = new Array();
			 for(var i=0;i<data.length;i++){
			 var display = data[i].display;
			 var t = display.count / m;
			 var x = ((i % 2 == 0) ? 0.55 : 0.7) + (i/100);
			 var y = Math.abs(0.9 - t);
			 var o = Math.min(display.offset,offset.end);
			 //var l = (totals == "No") ? null : "Curva: " + legend[i] + "\nTotale: " + display.count + "\nUltimo indice: " + display.offset;
			 //chart.setLabel("label_" + i,l,x,y,i,data[i].offsets.indexOf(o));
			 l.push("Curva: " + legend[i] + "\nTotale: " + display.count + "\nUltimo indice: " + display.offset);
			 }
			 chart.setLabel("total",l.join("\n---------------\n"),50,60);
			 chart.setFont("total",new java.awt.Font("Arial",java.awt.Font.BOLD,12));*/

			chart.setLegendLabels(legend);
			var gridLines = new Array();

			for (var dl in delayLables) {
				gridLines.push(dl);
			}
			chart.setGridLines(gridLines);
			//FS
			//dep 	chart.setGridLinesColor(globals.utils_getJColor(config.grid));
			chart.setGridLinesColor(globals.utils_getJColor(config.grid));
			chart.visible = true;
		} else {
			chart.visible = false;
			plugins.dialogs.showWarningDialog("Nessun dato", "Non è presente alcun dato relativo ai criteri selezionati", "Ok");
		}
	} else {
		chart.visible = false;
	}
}

/**
 * @properties={typeid:24,uuid:"D43B0609-7DF6-49F4-B5AE-89DDA3CE00C0"}
 */
function exportData(project, value) {
	/** @type {String} */
	var query = forms[selected].query_ex.query.replace(/\*\*DATE_HERE\*\*/g, forms[selected].dates[parseLegendLable(project).s]);
	var args = forms[selected].query_ex.args;
	var form = forms[selected].query_ex.form;
	//Inizio parte di filtraggio
	var filters = {
		type: type,
		subtype: subtype,
		group: group,
		subgroup: subgroup,
		severity: severity,
		def_reason: def_reason
	};
	var adding = forms[selected].addingParts;
	var replaceFinal = forms[selected].replacePart;
	var argsFinal = new Array();
	for (var i = 0; i < filterNames.length; i++) {
		if (filters[filterNames[i]]) {
			replaceFinal += " " + adding[filterNames[i]];
			argsFinal.push(filters[filterNames[i]]);
		}
	}
	//Aggiunta parte relativa alla gestione delle versioni AS400
	var versions = forms.report_anomalie_container.versions;
	if (typeof versions[parseLegendLable(project).p] != "string") replaceFinal += " " + forms[selected].versionSpecificPart.replace("**VERSIONS_HERE**", versions[parseLegendLable(project).p].join("','"));
	query = query.replace(forms[selected].replacePart, replaceFinal);
	args = [getCheckDate(parseLegendLable(project).p)].concat(args).concat([parseLegendLable(project).p]).concat(argsFinal).concat([parseLegendLable(project).s, offset.start + value]);
	//Fine parte di filtraggio
	//application.output(query + "\n" + args);
	forms[form].controller.loadRecords(query, args);
	globals.nfx_launchExport(form);
}

/**
 * @properties={typeid:24,uuid:"CBA63E6A-11D7-4F09-97FB-34F4644AD2F8"}
 */
function getProjectsForExport() {
	return forms[selected].elements.chart.getLegendLabels();
}

/**
 * @properties={typeid:24,uuid:"C77C8A5D-CF74-494C-8A98-DEA7DFD34C93"}
 */
function parseLegendLable(label) {
	var l = label.split(" - ");
	return { p: l[0], s: exportAlternatives[l[1]] };
}

/**
 * @properties={typeid:24,uuid:"589078FB-4CB8-4F86-8083-ED8D33D8DFF4"}
 */
function openDeltaPopup() {
	forms.report_anomalie_popup_delta.openPopup(delayLables);
}
