/**
 * @properties={typeid:35,uuid:"A28FD98D-CBAD-41F3-A06D-9F63BC3A6534",variableType:-4}
 */
var addingParts = {type			: "AND S.TIPO_ANOMALIA = ?",
                   subtype		: "AND S.SOTTOTIPO_ANOMALIA = ?",
                   group		: "AND S.GRUPPO_FUNZIONALE = ?",
                   subgroup		: "AND S.SOTTOGRUPPO_FUNZIONALE = ?",
                   severity		: "AND S.PUNTEGGIO_DEMERITO = ?",
                   def_reason	: "AND S.ATTRIBUZIONE = ?"};

/**
 * @properties={typeid:35,uuid:"2DADEC3E-36F6-4F11-B75E-7F5338531702",variableType:-4}
 */
var args = {base: null, open: null, closed: null};

/**
 * @properties={typeid:35,uuid:"45EF6AEC-B660-4F86-9662-60B887349D33",variableType:-4}
 */
var container = null;

/**
 * @properties={typeid:35,uuid:"AB89EBC5-A28C-4206-A462-95A94969E7BD",variableType:-4}
 */
var dates = {"%"		:	"S.DATA_RILEVAZIONE",
             "APERTA"	:	"S.DATA_RILEVAZIONE",
             "CHIUSA"	:	"S.DATA_CHIUSURA_SEGNALAZIONE"};

/**
 * @properties={typeid:35,uuid:"92277533-2A21-4BA6-A582-E060C4BD1D6E",variableType:-4}
 */
var query = {base: null, open: null, closed: null};

/**
 * @properties={typeid:35,uuid:"328D874A-A88B-4AD6-B650-EA7D7F6D4755",variableType:-4}
 */
var query_ex = {query: null, args: null, form: null};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"AE254E49-4D1F-4DD8-842D-9A16FF79EA74"}
 */
var replacePart = "S.CODICE_MODELLO = ?";

/**
 * @properties={typeid:35,uuid:"F6C035C5-846B-4394-923F-9D1597655DC7",variableType:-4}
 */
var title = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"7D987394-43E1-48BC-8935-C30981A67B56"}
 */
var versionSpecificPart = "AND S.CODICE_VERSIONE IN ('**VERSIONS_HERE**')";

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"99AEFF09-140A-427E-91EF-E11985CD86DA"}
 */
function onLoad(event) {
	container = forms.report_cumulate;
	
	//Setto le query e i parametri TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	//Base
	query.base = "SELECT OFFSET,COUNT(*) FROM (SELECT (EXTRACT(YEAR FROM S.DATA_RILEVAZIONE) * 12) + EXTRACT(MONTH FROM S.DATA_RILEVAZIONE) - ? AS OFFSET FROM K8_SEGNALAZIONI_FULL S WHERE S.CODICE_MODELLO = ?) GROUP BY OFFSET ORDER BY OFFSET";
	args.base = [];
	//Open
	query.open = query.base.replace(replacePart,"S.STATO_SEGNALAZIONE = ? AND " + replacePart);
	args.open = ["APERTA"];
	//Closed
	query.closed = query.base.replace(replacePart,"S.STATO_SEGNALAZIONE = ? AND " + replacePart).replace(/S.DATA_RILEVAZIONE/g,"S.DATA_CHIUSURA_SEGNALAZIONE");
	args.closed = ["CHIUSA"];
	//Estrattore
	query_ex.query = "SELECT ID_ FROM (SELECT S.K8_ANOMALIE_ID ID_, (EXTRACT(YEAR FROM **DATE_HERE**) * 12) + EXTRACT(MONTH FROM **DATE_HERE**) - ? OFFSET FROM K8_SEGNALAZIONI_FULL S WHERE S.CODICE_MODELLO = ? AND S.STATO_SEGNALAZIONE LIKE ?) WHERE OFFSET <= ?";
	query_ex.args = [];
	query_ex.form = forms.segnalazioni_anomalie_record.controller.getName();
	title = "Segnalazioni anomalia";
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"AFA831BE-054F-468C-9B91-F23F00C7B49A"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.drawChart();
}
