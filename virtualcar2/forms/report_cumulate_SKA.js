/**
 * @properties={typeid:35,uuid:"13372810-CCEA-47E7-A858-F8905FA4AF9F",variableType:-4}
 */
var addingParts = {type			: "AND SK.TIPO_ANOMALIA = ?",
                   subtype		: "AND SK.SOTTOTIPO_ANOMALIA = ?",
                   group		: "AND SK.GRUPPO_FUNZIONALE = ?",
                   subgroup		: "AND SK.SOTTOGRUPPO_FUNZIONALE = ?",
                   severity		: "AND SK.PUNTEGGIO_DEMERITO = ?",
                   def_reason	: "AND SK.ATTRIBUZIONE = ?"};

/**
 * @properties={typeid:35,uuid:"32990E9D-6468-4F5F-8237-7B9C78FB0F57",variableType:-4}
 */
var args = {base: null, open: null, closed: null};

/**
 * @properties={typeid:35,uuid:"090D7F23-C907-448A-B782-ABD59E4B7BCB",variableType:-4}
 */
var container = null;

/**
 * @properties={typeid:35,uuid:"6A9044E6-BC30-4787-A0BB-672AE7AF79DB",variableType:-4}
 */
var dates = {"%"		:	"SK.DATA_REGISTRAZIONE_SCHEDA",
             "APERTA"	:	"SK.DATA_REGISTRAZIONE_SCHEDA",
             "CHIUSA"	:	"SK.DATA_CHIUSURA"};

/**
 * @properties={typeid:35,uuid:"9D72628F-3831-47E3-A520-1A4DAE9B7DEF",variableType:-4}
 */
var query = {base: null, open: null, closed: null};

/**
 * @properties={typeid:35,uuid:"3B30F735-D305-4625-8889-951D2F14736D",variableType:-4}
 */
var query_ex = {query: null, args: null, form: null};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"0937E7B1-E28D-48AF-9416-68C3781AD7B5"}
 */
var replacePart = "SK.CODICE_MODELLO = ?";

/**
 * @properties={typeid:35,uuid:"F8AC0918-5EEF-4B17-86FF-F0BAE8266114",variableType:-4}
 */
var title = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"FDBB05AE-3D8A-43BA-ABA3-D2BA6C89455A"}
 */
var versionSpecificPart = "AND SK.CODICE_VERSIONE IN ('**VERSIONS_HERE**')";

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"36B03E2C-0991-49D6-B1C9-A4A41138D352"}
 */
function onLoad(event) {
	container = forms.report_cumulate;
	//Setto le query e i parametri TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	//Base
	query.base = "SELECT OFFSET,COUNT(*) FROM (SELECT ((EXTRACT(YEAR FROM SK.DATA_REGISTRAZIONE_SCHEDA) * 12) + EXTRACT(MONTH FROM SK.DATA_REGISTRAZIONE_SCHEDA)) - ? AS OFFSET FROM K8_SCHEDE_ANOMALIE SK WHERE SK.CODICE_MODELLO = ?) GROUP BY OFFSET ORDER BY OFFSET";
	args.base = [];
	//Open
	query.open = query.base.replace(replacePart,"SK.STATO_ANOMALIA = ? AND " + replacePart);
	args.open = ["APERTA"];
	//Closed
	query.closed = query.base.replace(replacePart,"SK.STATO_ANOMALIA = ? AND " + replacePart).replace(/SK.DATA_REGISTRAZIONE_SCHEDA/g,"SK.DATA_CHIUSURA");
	args.closed = ["CHIUSA"];
	//Estrattore
	query_ex.query = "SELECT ID_ FROM (SELECT SK.K8_ANOMALIE_ID ID_, (EXTRACT(YEAR FROM **DATE_HERE**) * 12) + EXTRACT(MONTH FROM **DATE_HERE**) - ? OFFSET FROM K8_SCHEDE_ANOMALIE SK WHERE SK.CODICE_MODELLO = ? AND SK.STATO_ANOMALIA LIKE ?) WHERE OFFSET <= ?";
	query_ex.args = [];
	query_ex.form = forms.schede_anomalie_record.controller.getName();
	title = "Schede anomalia";
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"B9F12D4F-FF8D-4099-802A-183F021E6A82"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.drawChart();
}
