/**
 * @properties={typeid:35,uuid:"5DFFE4D6-8CF4-44DD-BCDD-217D6E009D85",variableType:-4}
 */
var addingParts = {type			: "AND SKA.TIPO_ANOMALIA = ?",
                   subtype		: "AND SKA.SOTTOTIPO_ANOMALIA = ?",
                   group		: "AND SKM.GRUPPO_FUNZIONALE = ?",
                   subgroup		: "AND SKM.GRUPPO_FUNZIONALE2 = ?",
                   severity		: "AND SKA.PUNTEGGIO_DEMERITO = ?",
                   def_reason	: "AND SKA.ATTRIBUZIONE = ?"};

/**
 * @properties={typeid:35,uuid:"681A4048-A7DD-44A5-BD67-3A26BB0E33F0",variableType:-4}
 */
var args = {base: null, open: null, closed: null};

/**
 * @properties={typeid:35,uuid:"85FFBB44-A573-4D61-A832-588D64B914C2",variableType:-4}
 */
var container = null;

/**
 * @properties={typeid:35,uuid:"9D803D9C-F0B4-4078-9E45-48A33A4E3184",variableType:-4}
 */
var dates = {"%"		:	"SKM.DATA_PROPOSTA_MODIFICA",
             "APERTA"	:	"SKM.DATA_PROPOSTA_MODIFICA",
             "CHIUSA"	:	"NVL(SKM.DATA_APPROV_SK_PROP_MOD,SKM.DATA_ANNUL_SK_PROP_MOD)"};

/**
 * @properties={typeid:35,uuid:"AEBC7970-8729-48D7-BAC6-E3D983EBB390",variableType:-4}
 */
var query = {base: null, open: null, closed: null};

/**
 * @properties={typeid:35,uuid:"41B6195F-16C3-4DC3-8AFD-B183187A1AB3",variableType:-4}
 */
var query_ex = {query: null, args: null, form: null};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"3A4CC970-231D-4835-AD7F-F7EEB997E30C"}
 */
var replacePart = "SKM.MODELLO = ?";

/**
 * @properties={typeid:35,uuid:"3CC288E6-AA68-4EB2-8343-E7FB891D9E1A",variableType:-4}
 */
var title = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B914A4F0-68E1-47B2-BC80-FF3F14673C99"}
 */
var versionSpecificPart = "AND SKM.VERSIONE IN ('**VERSIONS_HERE**')";

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"AC0D09E2-D150-4A3F-BF4F-0F8F12CCB22C"}
 */
function onLoad(event) {
	container = forms.report_cumulate;
	//Setto le query e i parametri TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	//Base
	query.base = "SELECT OFFSET,COUNT(*) FROM (SELECT ((EXTRACT(YEAR FROM SKM.DATA_PROPOSTA_MODIFICA) * 12) + EXTRACT(MONTH FROM SKM.DATA_PROPOSTA_MODIFICA)) - ? AS OFFSET FROM K8_SEGNALAZIONI_FULL SKA INNER JOIN K8_SCHEDE_MODIFICA SKM ON SKA.ENTE = SKM.ENTE_INSERIMENTO_ANOMALIA AND SKA.NUMERO_SCHEDA = SKM.NR_SCHEDA_ANOMALIA WHERE SKM.CAUSALE_MODIFICA = ? AND SKM.MODELLO = ?) GROUP BY OFFSET ORDER BY OFFSET";
	args.base = [34];
	//Open
	query.open = query.base.replace(replacePart,"SKM.STATO_SCHEDA = ? AND " + replacePart);
	args.open = [34,"APERTA"];
	//Closed
	query.closed = query.base.replace(replacePart,"SKM.STATO_SCHEDA = ? AND " + replacePart).replace(/SKM.DATA_PROPOSTA_MODIFICA/g,"NVL(SKM.DATA_APPROV_SK_PROP_MOD,SKM.DATA_ANNUL_SK_PROP_MOD)");
	args.closed = [34,"CHIUSA"];
	//Estrattore
	query_ex.query = "SELECT ID_ FROM (SELECT SKM.K8_SCHEDE_MODIFICA_ID ID_, (EXTRACT(YEAR FROM **DATE_HERE**) * 12) + EXTRACT(MONTH FROM **DATE_HERE**) - ? OFFSET FROM K8_SEGNALAZIONI_FULL SKA INNER JOIN K8_SCHEDE_MODIFICA SKM ON SKA.ENTE = SKM.ENTE_INSERIMENTO_ANOMALIA AND SKA.NUMERO_SCHEDA = SKM.NR_SCHEDA_ANOMALIA WHERE SKM.CAUSALE_MODIFICA = ? AND SKM.MODELLO = ? AND SKM.STATO_SCHEDA LIKE ?) WHERE OFFSET <= ?";
	query_ex.args = [34];
	query_ex.form = forms.schede_modifica_record.controller.getName();
	title = "Schede modifica (causale 34)";
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"D449F435-340A-49EA-9938-B50FCB2F69DD"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.drawChart();
}
