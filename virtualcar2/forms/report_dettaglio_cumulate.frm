items:[
{
fontType:"Verdana,1,10",
formIndex:1,
horizontalAlignment:0,
location:"610,64",
mediaOptions:14,
name:"end_reset",
onActionMethodID:"7152EA6D-2ED3-47A5-BDBB-C9C3015F62FE",
showClick:false,
size:"20,20",
text:"X",
toolTipText:"Pulisci intervallo",
transparent:true,
typeid:7,
uuid:"00FC29AA-AD85-4392-9193-C41F60195200"
},
{
dataProviderID:"end_year",
formIndex:94,
format:"0000",
horizontalAlignment:0,
location:"550,64",
name:"end_year",
size:"40,20",
text:"End Year",
typeid:4,
uuid:"014612ED-276E-48C7-A869-9123B76E7896"
},
{
formIndex:97,
horizontalAlignment:4,
location:"490,64",
mediaOptions:14,
size:"50,20",
styleClass:"form",
tabSeq:-1,
text:"fine:",
transparent:true,
typeid:7,
uuid:"067A50A7-6880-4A0B-A61E-D5EB26A7D77A"
},
{
horizontalAlignment:4,
labelFor:"severity",
location:"260,0",
mediaOptions:14,
size:"60,20",
styleClass:"form",
tabSeq:-1,
text:"Demerito:",
transparent:true,
typeid:7,
uuid:"08C8028A-391C-441E-97D2-85AD83F32945"
},
{
dataProviderID:"def_reason",
displayType:2,
editable:false,
formIndex:86,
location:"130,100",
name:"def_reason",
size:"140,20",
text:"Def Reason",
typeid:4,
uuid:"1655D8D5-3068-4747-BF70-1674482D5D85",
valuelistID:"44BD220C-5A3E-4F3B-AF7C-A031012AE4C7"
},
{
formIndex:97,
horizontalAlignment:4,
location:"340,64",
mediaOptions:14,
size:"50,20",
styleClass:"form",
tabSeq:-1,
text:"inizio:",
transparent:true,
typeid:7,
uuid:"19490466-A9D8-475F-9C8A-C5FC3D12A595"
},
{
horizontalAlignment:4,
labelFor:"type",
location:"390,25",
mediaOptions:14,
size:"50,20",
styleClass:"form",
tabSeq:-1,
text:"Tipo:",
transparent:true,
typeid:7,
uuid:"1FA7464D-994F-4B47-B45C-519D670AB26D"
},
{
dataProviderID:"severity",
displayType:2,
editable:false,
location:"330,0",
name:"severity",
size:"50,20",
text:"Severity",
typeid:4,
uuid:"358B993F-D0AA-403B-8583-0BBBC3830C1A",
valuelistID:"33C20ED3-B7D9-4B5C-89B7-66E97A6CC757"
},
{
dataProviderID:"subgroup",
location:"610,0",
name:"subgroup",
size:"40,20",
text:"Subgroup",
typeid:4,
uuid:"3E7FB132-AE8C-4971-BADE-26875C813708"
},
{
dataProviderID:"start_option",
formIndex:95,
format:"00",
horizontalAlignment:0,
location:"440,64",
name:"start_option",
size:"20,20",
text:"Start Option",
typeid:4,
uuid:"4F91C870-B8DB-4CAD-8D43-D63AA3120D73"
},
{
height:300,
partType:5,
typeid:19,
uuid:"5391723E-0821-4316-92CA-ED31B4BDE8F0"
},
{
horizontalAlignment:4,
labelFor:"status",
location:"20,0",
mediaOptions:14,
size:"40,20",
styleClass:"form",
tabSeq:-1,
text:"Stato:",
transparent:true,
typeid:7,
uuid:"5767F6E8-E778-48E1-855E-D22B5FCC32DC"
},
{
formIndex:87,
horizontalAlignment:4,
labelFor:"history_flag",
location:"560,100",
mediaOptions:14,
size:"50,20",
styleClass:"form",
tabSeq:-1,
text:"Storico:",
transparent:true,
typeid:7,
uuid:"5A18926D-746D-46C9-B755-DC877307FAED"
},
{
dataProviderID:"mod_reason",
displayType:2,
editable:false,
formIndex:90,
location:"390,100",
name:"mod_reason",
size:"160,20",
text:"Mod Reason",
typeid:4,
uuid:"5E84B2D3-6C9D-42F1-89F3-52E623DCEA92",
valuelistID:"42EB7CA8-CAFA-4853-BB83-0DEC061723BC"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"history_flag",
displayType:4,
formIndex:88,
horizontalAlignment:0,
location:"610,100",
name:"history_flag",
onDataChangeMethodID:"13E003D4-298F-4889-B502-377AD1B8E0F1",
scrollbars:4,
size:"40,20",
transparent:true,
typeid:4,
uuid:"6DE51DBE-BCE8-4F4F-B532-70F950FC68FB"
},
{
borderType:"TitledBorder,Intervallo,4,0,DialogInput.plain,1,12,#333333",
formIndex:-1,
lineSize:1,
location:"20,50",
size:"630,40",
transparent:true,
typeid:21,
uuid:"6F35BBA2-D09D-4B77-878E-F3A310A45E2A"
},
{
dataProviderID:"start_year",
formIndex:96,
format:"0000",
horizontalAlignment:0,
location:"400,64",
name:"start_year",
size:"40,20",
text:"Start Year",
typeid:4,
uuid:"77E89077-FC5E-4838-B826-66C683F72656"
},
{
horizontalAlignment:4,
labelFor:"cumulative",
location:"260,25",
mediaOptions:14,
size:"60,20",
styleClass:"form",
tabSeq:-1,
text:"Cumulate:",
transparent:true,
typeid:7,
uuid:"79C0EBEF-4EC0-4648-9325-EA0D9E5BF254"
},
{
formIndex:91,
horizontalAlignment:4,
labelFor:"date_format",
location:"90,64",
mediaOptions:14,
size:"50,20",
styleClass:"form",
tabSeq:-1,
text:"step:",
transparent:true,
typeid:7,
uuid:"8A126D7D-8A17-4A1B-A4FA-8D358C5992B8"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"status",
displayType:4,
location:"60,0",
name:"status",
scrollbars:4,
size:"190,20",
text:"Status",
transparent:true,
typeid:4,
uuid:"99179741-6995-4C2B-BD23-EFF267F31D32",
valuelistID:"77431257-2E7B-48FD-BB9E-9FF014917075"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"quotes",
displayType:3,
formIndex:84,
horizontalAlignment:0,
location:"60,25",
name:"quotes",
scrollbars:4,
size:"190,20",
text:"Quotes",
transparent:true,
typeid:4,
uuid:"9D068D8A-94DB-42A3-ADAA-B196D9F07CB8",
valuelistID:"1E2C0E21-C850-4BCF-8F48-41FDCB0B17D0"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"date_format",
displayType:3,
displaysTags:true,
formIndex:92,
horizontalAlignment:0,
location:"140,64",
name:"date_format",
scrollbars:4,
size:"190,20",
text:"Date Format",
transparent:true,
typeid:4,
uuid:"ACFC493C-E2F2-42E6-AF37-5FA7AC70C686",
valuelistID:"E9C0EB2A-9FC0-441D-8EB9-1A819028BA78"
},
{
dataProviderID:"end_option",
formIndex:93,
format:"00",
horizontalAlignment:0,
location:"590,64",
name:"end_option",
size:"20,20",
text:"End Option",
typeid:4,
uuid:"B0229EED-4E44-4496-879F-21447BA007B9"
},
{
formIndex:83,
horizontalAlignment:4,
labelFor:"quotes",
location:"20,25",
mediaOptions:14,
size:"40,20",
styleClass:"form",
tabSeq:-1,
text:"Quote:",
transparent:true,
typeid:7,
uuid:"B319166B-B861-4B7C-970F-C30EBD60939A"
},
{
formIndex:85,
horizontalAlignment:4,
labelFor:"def_reason",
location:"20,100",
mediaOptions:14,
size:"100,20",
styleClass:"form",
tabSeq:-1,
text:"Caus. anomalia:",
transparent:true,
typeid:7,
uuid:"B6A5E796-FAEE-4886-A2E2-84460D929740"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"cumulative",
displayType:4,
horizontalAlignment:0,
location:"340,25",
name:"cumulative",
size:"20,20",
transparent:true,
typeid:4,
uuid:"B8D89413-7A9D-47EB-A22C-D110E08173F3"
},
{
dataProviderID:"group",
displayType:2,
editable:false,
location:"450,0",
name:"group",
size:"160,20",
text:"Group",
typeid:4,
uuid:"C1010EC4-30D8-4FA0-8DFE-60E605BBA04C",
valuelistID:"50DC6D6A-1FD6-4A3B-8DC0-D12CE8C7CF99"
},
{
formIndex:89,
horizontalAlignment:4,
labelFor:"mod_reason",
location:"280,100",
mediaOptions:14,
size:"100,20",
styleClass:"form",
tabSeq:-1,
text:"Caus. modifica:",
transparent:true,
typeid:7,
uuid:"CD95D9B0-D859-4730-B46A-35B1B889CFE3"
},
{
horizontalAlignment:4,
labelFor:"group",
location:"390,0",
mediaOptions:14,
size:"50,20",
styleClass:"form",
tabSeq:-1,
text:"Gruppo:",
transparent:true,
typeid:7,
uuid:"D4727CAD-748E-4F1B-9AEA-8A5D127D2A7B"
},
{
anchors:15,
items:[
{
containsFormID:"F359B2D3-C4B0-4B88-903E-07AF91B754A2",
location:"150,220",
text:"Schede modifica",
typeid:15,
uuid:"2ADA74FF-BEF2-45E2-8898-9ED5983EAA82"
},
{
containsFormID:"084C0A67-972D-4201-A270-E0069118DC0F",
location:"30,180",
text:"Segnalazioni anomalia",
typeid:15,
uuid:"2C73CEBD-BD5B-4059-A89B-8ECBF831F5BB"
},
{
containsFormID:"D1600C74-BD94-4DC9-B97C-281A9A131C91",
location:"110,200",
text:"Schede anomalia",
typeid:15,
uuid:"45F7E7B1-4671-475F-909C-8F7611793D17"
}
],
location:"0,130",
name:"cumulateTab",
printable:false,
size:"660,170",
transparent:true,
typeid:16,
uuid:"E0060EE2-4AC9-4D27-BE75-1DE5B5CA1261"
},
{
dataProviderID:"subtype",
displayType:10,
location:"610,25",
name:"subtype",
size:"40,20",
text:"Subtype",
typeid:4,
uuid:"E2916BE1-1044-47D6-95D9-9A55B1CE3C2F"
},
{
dataProviderID:"type",
displayType:2,
editable:false,
location:"450,25",
name:"type",
size:"160,20",
text:"Type",
typeid:4,
uuid:"E352F739-E68B-4C19-9CE5-D413BA66A7F6",
valuelistID:"75733E85-07F2-402A-AFC4-763C4B8E8626"
},
{
fontType:"Verdana,1,10",
formIndex:1,
horizontalAlignment:0,
location:"460,64",
mediaOptions:14,
name:"start_reset",
onActionMethodID:"7152EA6D-2ED3-47A5-BDBB-C9C3015F62FE",
showClick:false,
size:"20,20",
text:"X",
toolTipText:"Pulisci intervallo",
transparent:true,
typeid:7,
uuid:"E8B76551-2682-47B1-AAD2-F4D65DE89696"
}
],
name:"report_dettaglio_cumulate",
navigatorID:"-1",
onLoadMethodID:"8F00E763-E05E-4EBF-848C-7438D456303F",
paperPrintScale:100,
showInMenu:true,
size:"660,300",
styleName:"keeneight",
typeid:3,
uuid:"B9CEEFAB-1FA1-4568-8C26-9BE973CA95CF"