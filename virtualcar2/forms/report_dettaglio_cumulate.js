/**
 * @properties={typeid:35,uuid:"5411B6D5-9A80-4405-BD27-5DE9E9E0DAFC",variableType:-4}
 */
var config = {
	attributes: { type: "line", id: "getVList" },
	bgc1: "#cccccc", bgc2: "#ffffff",
	grid: null,
	fntx: "Verdana,0,10", fnty: "Verdana,0,10",
	styles: { }
};

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"A61D9113-3DB9-46F7-A144-F3C703713688",variableType:4}
 */
var cumulative = 1;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"FE6B6B73-1E26-4DB2-90D2-4C0FAE7C4D36"}
 */
var date_format = "month";

/**
 * @properties={typeid:35,uuid:"CC5FF759-6B45-4FD3-AA5D-CDF7A924EB98",variableType:-4}
 */
var date_formats = {
	"week": { format: "YYYY_IW", app_format: "yyyy_ww", max: "53", human: "settimana" },
	"month": { format: "YYYY_MM", app_format: "yyyy_MM", max: "12", human: "mese" }
};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"E3AAFEDD-DE94-414A-BC90-CB0B7F91ECF2"}
 */
var def_reason = "";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"245B88B7-2B19-43B6-B483-2E06FBB0BE4E",variableType:4}
 */
var end_option = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"BBE7F350-DBC9-4F49-A9E5-3EF9F28D8E2F",variableType:4}
 */
var end_year = null;

/**
 * @properties={typeid:35,uuid:"EDD1A9F4-B8E4-450E-8413-6B2D3768AFA0",variableType:-4}
 */
var exportAlternatives = {
	"Totali": "%",
	"Aperte": "APERTA",
	"Chiuse": "CHIUSA"
};

/**
 * @properties={typeid:35,uuid:"AF820C5E-FEC5-4B25-A8E5-97328797499E",variableType:-4}
 */
var filterNames = ["type", "subtype", "group", "subgroup", "severity", "def_reason", "mod_reason"];

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"0630664F-E723-4CF1-BF01-A4416304C898"}
 */
var group = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"3743A7B9-B1BE-41D8-82AC-2661A12C0CA3"}
 */
var history_flag = "";

/**
 * @properties={typeid:35,uuid:"0F5CAB55-F950-4E48-ACFD-C8EFA8DAD979",variableType:-4}
 */
var legendLabels = {
	"base": " - Totali",
	"open": " - Aperte",
	"closed": " - Chiuse"
};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A5195A18-CF71-446E-BC08-4A613A11F437"}
 */
var mod_reason = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C5032778-49EE-4BC7-87FB-CA311261EA54"}
 */
var quotes = "No";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"CECA7D2C-89AE-4DF6-A246-309F243CF94A"}
 */
var selected = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"DEAF9311-8F78-48F1-8383-A21BCF28EB18"}
 */
var severity = "";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"1018D041-6B1A-4622-9EE1-173C2262F584",variableType:4}
 */
var start_option = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"0783FE88-BC23-40CB-A9C8-02F1F8889415",variableType:4}
 */
var start_year = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"AA64875B-4309-4C11-8A5C-1499CA2386D1"}
 */
var status = "base\nclosed";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"0ECC409A-AA73-4DF6-BBBC-D0A56CABDB0A"}
 */
var subgroup = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"DC71E143-C1D8-4B1D-B6E5-7708B92411C9"}
 */
var subtype = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"27F0AED6-EE58-4CB8-B59D-BA65CD54620A"}
 */
var type = "";

/**
 * @properties={typeid:24,uuid:"F68BBE20-BEFC-4B08-B111-4B33B5137383"}
 */
function getVList() {
	var _list = [];
	var _projects = application.getValueListArray("progetti");
	for (var i = 0; i < _projects.length; i++) {
		if (_projects[i]) {
			for (var j in legendLabels) {
				_list.push(_projects[i] + legendLabels[j]);
			}
		}
	}
	return _list;
}

/**
 * @properties={typeid:24,uuid:"F8364A30-30E9-4896-A340-C94A2E039EDE"}
 */
function nfx_isProgram() {
	return false;
}

/**
 * @properties={typeid:24,uuid:"D990DC35-B4B8-4556-B126-4613886B5442"}
 */
function formatCheckDate(position) {
	var now = new Date();
	var dates = {
		"start": { year: start_year, option: start_option },
		"start_def": utils.dateFormat(new Date(now.getFullYear() - 1, now.getMonth(), now.getDate()), date_formats[date_format].app_format),
		"end": { year: end_year, option: end_option },
		"end_def": utils.dateFormat(now, date_formats[date_format].app_format)
	};
	return (dates[position].year && dates[position].year > 0 && dates[position].option && dates[position].option > 0 && dates[position].option <= date_formats[date_format].max) ? utils.numberFormat(dates[position].year, "0000") + "_" + utils.numberFormat(dates[position].option, "00") : dates[position + "_def"];
}

/**
 * @properties={typeid:24,uuid:"E8A5220A-5D85-4813-9D9E-646D4975E580"}
 */
function fullCheckDateRange(start, end) {
	var s = start.split("_");
	var e = end.split("_");
	var range = new Array();
	for (var i = s[0]; i <= e[0]; i++) {
		for (var j = 1; j <= date_formats[date_format].max; j++) {
			var r = utils.numberFormat(i, "0000") + "_" + utils.numberFormat(j, "00");
			if (r >= start && r <= end) {
				range.push(r);
			}
		}
	}
	return range;
}

/**
 * @properties={typeid:24,uuid:"38D3774B-C810-4DF8-9290-47FA828F8C3B"}
 */
function getData(p, v, step) {
	if (!forms[selected].query[step] || !forms[selected].args[step]) throw "Content query and/or args not detected";
	/** @type {String} */
	var query = forms[selected].query[step].replace(/\*\*FORMAT_HERE\*\*/g, date_formats[date_format].format);
	//FS
	/** @type {Array} */
	var args = forms[selected].args[step];
	//Inizio parte di filtraggio
	var filters = {
		type: type,
		subtype: subtype,
		group: group,
		subgroup: subgroup,
		severity: severity,
		def_reason: def_reason,
		mod_reason: mod_reason
	};
	var adding = forms[selected].addingParts;
	var replaceFinal = forms[selected].replacePart;
	var argsFinal = new Array();
	for (var i = 0; i < filterNames.length; i++) {
		if (filters[filterNames[i]]) {
			replaceFinal += " " + adding[filterNames[i]];
			argsFinal.push(filters[filterNames[i]]);
		}
	}
	//Aggiunta parte relativa alla gestione delle versioni AS400
	if (v) replaceFinal += " " + forms[selected].versionSpecificPart.replace("**VERSIONS_HERE**", v);
	query = query.replace(forms[selected].replacePart, replaceFinal);
	args = args.concat([p]).concat(argsFinal);
	//Fine parte di filtraggio
	//application.output(query + "\n" + args);
	var ds = databaseManager.getDataSetByQuery('ferrari', query, args, -1);
	var o = ds.getColumnAsArray(1);
	var c = ds.getColumnAsArray(2);
	var start = formatCheckDate("start");
	var end = formatCheckDate("end");
	var s = (o[0] < start) ? o[0] : start;
	var e = (o[o.length - 1] > end) ? o[o.length - 1] : end;
	var ranges = fullCheckDateRange(s, e);
	var counts = new Array();
	for (var i2 = 0; i2 < ranges.length; i2++) {
		counts[i2] = (o.indexOf(ranges[i2]) != -1) ? c[o.indexOf(ranges[i2])] : 0;
	}
	var max = 0;
	if (cumulative == 1) {
		for (var i3 = 1; i3 < counts.length; i3++) {
			counts[i3] += counts[i3 - 1];
		}
		max = counts[counts.length - 1];
	} else {
		for (var i4 = 1; i4 < counts.length; i4++) {
			if (counts[i4] > max) max = counts[i4];
		}
	}
	counts = counts.slice(ranges.indexOf(start), ranges.indexOf(end) + 1);
	ranges = ranges.slice(ranges.indexOf(start), ranges.indexOf(end) + 1);
	return (ds.getMaxRowIndex() > 0) ? { offsets: ranges, counts: counts, max: max } : null;
}

/**
 * @properties={typeid:24,uuid:"AE6840FE-C0D1-46BC-91A1-0DC2F3CFAC83"}
 */
function setupChart() {
	//FS
	config = {
		attributes: { type: "line", id: "getVList" },
		bgc1: "#cccccc", bgc2: "#ffffff",
		grid: null,
		fntx: "Verdana,0,10", fnty: "Verdana,0,10",
		styles: { }
	};
	/** @type {java.awt.chart} */
	var chart = forms[selected].elements.chart;
	if (!chart) throw "Selected form must have a chart element called \"chart\"";
	chart.setZoomOn(true);
	chart.setChartBackground(globals.utils_getJColor(config.bgc1));
	chart.setChartBackground2(globals.utils_getJColor(config.bgc2));

	chart.setAutoLabelSpacingOn(true);
	chart.setSampleLabelsOn(true);

	chart.setValueLinesOn(true);

	chart.setLabelAngle("valueLabelAngle", 45);
	chart.setFont("valueLabelFont", new java.awt.Font("Verdana", java.awt.Font.PLAIN, 9));

	chart.setLegendOn(true);
	chart.setLegendPosition(2);
	chart.setLegendBoxSizeAsFont(true);

	chart.setLabelAngle("barLabelAngle", 270);

	if (forms[selected].title) {
		chart.setTitleOn(true);
		chart.setTitle(forms[selected].title + " - " + utils.dateFormat(new Date(), forms.report_anomalie_container.chartTitleDateFormat));
	}

	chart.setFont("sampleLabelFont", globals.utils_getJFont(config.fntx));
	chart.setFont("rangeLabelFont", globals.utils_getJFont(config.fnty));
}

/**
 * @properties={typeid:24,uuid:"522CAED9-B484-4ACC-9E0D-E24B15A02200"}
 */
function drawChart() {
	/** @type {java.awt.chart} */
	var chart = forms[selected].elements.chart;
	if (!chart) throw "Selected form must have a chart element called \"chart\"";
	var projects = forms.report_anomalie_container.projects;
	var versions = forms.report_anomalie_container.versions;
	if (projects) {
		var p = projects.split("\n");
		var data = new Array();
		var legend = new Array();
		for (var i = 0; i < p.length; i++) {
			var v = (typeof versions[p[i]] == "string") ? null : versions[p[i]].join("','");
			if (status) {
				var s = status.split("\n");
				for (var j = 0; j < s.length; j++) {
					var d = getData(p[i], v, s[j]);
					if (d) {
						legend.push(p[i] + legendLabels[s[j]]);
						data.push(d);
					}
				}
			}

		}
		if (data.length > 0) {
			var max = 0;
			chart.setSeriesCount(data.length);
			chart.setSampleLabels(data[0].offsets);
			chart.setSampleCount(data[0].offsets.length);
			if (quotes != "No") {
				chart.setValueLabelStyle(0);
				chart.setValueLabelsOn(true);
			} else {
				chart.setValueLabelStyle(3);
				chart.setValueLabelsOn(true);
			}
			for (var i2 = 0; i2 < data.length; i2++) {
				if (max < data[i2].max) {
					max = data[i2].max;
				}
				if (config.styles[legend[i2]]) {
					chart.setSampleColor(i2, globals.utils_getJColor(config.styles[legend[i2]][0]));
					chart.setLineWidth(i2, config.styles[legend[i2]][1]);
					chart.setLineStroke(i2, config.styles[legend[i2]][2]);
				} else {
					chart.setSampleColor(i2, globals.utils_getJColor(forms.report_anomalie_container.colors[i2]));
					chart.setLineWidth(i2, 2);
					chart.setLineStroke(i2, [0]);
				}
				chart.setSampleValues(i2, data[i2].counts);
			}
			var n = Math.pow(10, Math.floor(Math.log(max) / Math.log(10)));
			var m = max - (max % n) + n;
			chart.setRange(0, m);
			chart.setLegendLabels(legend);
			chart.visible = true;
		} else {
			chart.visible = false;
			plugins.dialogs.showWarningDialog("Nessun dato", "Non è presente alcun dato relativo ai criteri selezionati", "Ok");
		}
	} else {
		chart.visible = false;
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"7152EA6D-2ED3-47A5-BDBB-C9C3015F62FE"}
 */
function resetPosition(event) {
	var p = event.getElementName().split("_")[0];
	if (p == "start") {
		start_year = null;
		start_option = null;
	}
	if (p == "end") {
		end_year = null;
		end_option = null;
	}
}

/**
 * @properties={typeid:24,uuid:"EDB8460E-D49A-484C-A515-E409D9AF7409"}
 */
function setupFilters() {
	var f = (forms[selected].filters) ? forms[selected].filters : new Array();
	var filters = ["def_reason", "mod_reason", "history_flag"];
	for (var i = 0; i < filters.length; i++) {
		if (f.indexOf(filters[i]) != -1) {
			elements[filters[i]].visible = true;
		} else {
			elements[filters[i]].visible = false;
			forms[controller.getName()][filters[i]] = "";
		}
	}
	setupHistoryFilters();
}

/**
 * @properties={typeid:24,uuid:"13E003D4-298F-4889-B502-377AD1B8E0F1"}
 */
function setupHistoryFilters() {
	var filters = ["type", "subtype", "severity", "def_reason"];
	if (elements.history_flag.visible && !utils.stringToNumber(history_flag)) {
		for (var i = 0; i < filters.length; i++) {
			elements[filters[i]].enabled = false;
			forms[controller.getName()][filters[i]] = "";
		}
	} else {
		for (var i2 = 0; i2 < filters.length; i2++) {
			elements[filters[i2]].enabled = true;
		}
	}
	forms.report_dettaglio_cumulate_SKM.setupQuery(utils.stringToNumber(history_flag));
}

/**
 * @properties={typeid:24,uuid:"870708FF-322E-4757-B6D9-B0A1B98E92D3"}
 */
function exportData(project, value) {
	/** @type {String} */
	var query = forms[selected].query_ex.query.replace(/\*\*DATE_HERE\*\*/g, forms[selected].dates[parseLegendLable(project).s]).replace(/\*\*FORMAT_HERE\*\*/g, date_formats[date_format].format);
	/** @type {String} */
	var args = forms[selected].query_ex.args;
	var form = forms[selected].query_ex.form;
	//Inizio parte di filtraggio
	var filters = {
		type: type,
		subtype: subtype,
		group: group,
		subgroup: subgroup,
		severity: severity,
		def_reason: def_reason,
		mod_reason: mod_reason
	};
	var adding = forms[selected].addingParts;
	var replaceFinal = forms[selected].replacePart;
	var argsFinal = new Array();
	for (var i = 0; i < filterNames.length; i++) {
		if (filters[filterNames[i]]) {
			replaceFinal += " " + adding[filterNames[i]];
			argsFinal.push(filters[filterNames[i]]);
		}
	}
	//Aggiunta parte relativa alla gestione delle versioni AS400
	var versions = forms.report_anomalie_container.versions;
	if (typeof versions[parseLegendLable(project).p] != "string") replaceFinal += " " + forms[selected].versionSpecificPart.replace("**VERSIONS_HERE**", versions[parseLegendLable(project).p].join("','"));
	query = query.replace(forms[selected].replacePart, replaceFinal);
	//FS
	//dep	args = args.concat([parseLegendLable(project).p]).concat(argsFinal).concat([parseLegendLable(project).s,forms[selected].elements.chart.getSampleLabels()[value]]);
	args = args.concat([parseLegendLable(project).p].toString()).concat(argsFinal.toString()).concat([parseLegendLable(project).s, forms[selected].elements.chart.getSampleLabels()[value]].toString());

	//Fine parte di filtraggio
	//application.output(query + "\n" + args);
	forms[form].controller.loadRecords(query, args);
	globals.nfx_launchExport(form);
}

/**
 * @properties={typeid:24,uuid:"12993A2A-31B2-483A-B53C-660901019344"}
 */
function getProjectsForExport() {
	return forms[selected].elements.chart.getLegendLabels();
}

/**
 * @properties={typeid:24,uuid:"0E921547-059F-4646-8F8D-AA89C8C8764E"}
 */
function parseLegendLable(label) {
	var l = label.split(" - ");
	return { p: l[0], s: exportAlternatives[l[1]] };
}
