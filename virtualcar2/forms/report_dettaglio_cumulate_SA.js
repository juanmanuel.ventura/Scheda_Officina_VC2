/**
 * @properties={typeid:35,uuid:"B31BBB8E-3565-47C2-85DB-8015D5D664D9",variableType:-4}
 */
var addingParts = {type		: "AND S.TIPO_ANOMALIA = ?",
                   subtype	: "AND S.SOTTOTIPO_ANOMALIA = ?",
                   group	: "AND S.GRUPPO_FUNZIONALE = ?",
                   subgroup	: "AND S.SOTTOGRUPPO_FUNZIONALE = ?",
                   severity	: "AND S.PUNTEGGIO_DEMERITO = ?"};

/**
 * @properties={typeid:35,uuid:"F1DAC358-DFAA-4984-A9CB-CC4DE9E62319",variableType:-4}
 */
var args = {base: null, open: null, closed: null};

/**
 * @properties={typeid:35,uuid:"DF47D3F6-4F14-438F-AD4F-11320D1BD59A",variableType:-4}
 */
var container = null;

/**
 * @properties={typeid:35,uuid:"41707ECC-AA11-444C-86E0-3C59A1245EF7",variableType:-4}
 */
var dates = {"%"		:	"S.DATA_RILEVAZIONE",
             "APERTA"	:	"S.DATA_RILEVAZIONE",
             "CHIUSA"	:	"S.DATA_CHIUSURA_SEGNALAZIONE"};

/**
 * @properties={typeid:35,uuid:"6C33E715-30A4-49E8-AA2D-A93915D631C6",variableType:-4}
 */
var filters = new Array();

/**
 * @properties={typeid:35,uuid:"7BD5BA8C-DC35-4C74-8A79-E8132486F467",variableType:-4}
 */
var query = {base: null, open: null, closed: null};

/**
 * @properties={typeid:35,uuid:"DC0EF77E-BDEB-42EA-A007-79DCF3263664",variableType:-4}
 */
var query_ex = {query: null, args: null, form: null};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"516D7A8D-002A-44CB-98A6-8355931616AC"}
 */
var replacePart = "S.CODICE_MODELLO = ?";

/**
 * @properties={typeid:35,uuid:"B954D1C4-B538-408D-998F-35E827D706BA",variableType:-4}
 */
var title = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"66E961CF-9DB2-4134-85CD-C6BD32BEC010"}
 */
var versionSpecificPart = "AND S.CODICE_VERSIONE IN ('**VERSIONS_HERE**')";

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"CDF2B5E5-82CB-4E6D-B30E-6366018924F5"}
 */
function onLoad(event) {
	container = forms.report_dettaglio_cumulate;
	//Setto le query e i parametri TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	//Base
	query.base = "SELECT OFFSET,COUNT(*) FROM (SELECT TO_CHAR(S.DATA_RILEVAZIONE,'**FORMAT_HERE**') OFFSET FROM K8_SEGNALAZIONI_FULL S WHERE S.CODICE_MODELLO = ?) GROUP BY OFFSET ORDER BY OFFSET";
	args.base = [];
	//Open
	query.open = query.base.replace(replacePart,"S.STATO_SEGNALAZIONE = ? AND " + replacePart);
	args.open = ["APERTA"];
	//Closed
	query.closed = query.base.replace(/S.DATA_RILEVAZIONE/g,"S.DATA_CHIUSURA_SEGNALAZIONE").replace(replacePart,"S.STATO_SEGNALAZIONE = ? AND " + replacePart);
	args.closed = ["CHIUSA"];
	//Estrattore
	query_ex.query = "SELECT ID_ FROM (SELECT S.K8_ANOMALIE_ID ID_, TO_CHAR(**DATE_HERE**,'**FORMAT_HERE**') OFFSET FROM K8_SEGNALAZIONI_FULL S WHERE S.CODICE_MODELLO = ? AND S.STATO_SEGNALAZIONE LIKE ?) WHERE OFFSET <= ?";
	query_ex.args = [];
	query_ex.form = forms.segnalazioni_anomalie_record.controller.getName();
	title = "Segnalazioni anomalia";
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"8D7F6C46-B0CA-4A24-ADA1-9C304BE25400"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.setupFilters();
	container.drawChart();
}
