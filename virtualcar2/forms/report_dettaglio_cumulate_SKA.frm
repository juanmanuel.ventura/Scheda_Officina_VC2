items:[
{
anchors:15,
beanClassName:"com.objectplanet.chart.LineChart",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_21\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"com.objectplanet.chart.LineChart\"> \
  <void property=\"background\"> \
   <object class=\"java.awt.Color\"> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>255<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"chartData\"> \
   <void id=\"StringArray0\" property=\"seriesLabels\"/> \
   <void property=\"seriesLabels\"> \
    <object idref=\"StringArray0\"/> \
   <\/void> \
  <\/void> \
  <void id=\"StringArray1\" property=\"legendLabels\"/> \
  <void property=\"legendLabels\"> \
   <object idref=\"StringArray1\"/> \
  <\/void> \
  <void property=\"name\"> \
   <string>bean_858<\/string> \
  <\/void> \
  <void id=\"StringArray2\" property=\"seriesLabels\"/> \
  <void property=\"seriesLabels\"> \
   <object idref=\"StringArray2\"/> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"0,0",
name:"chart",
size:"660,250",
typeid:12,
uuid:"8E241655-D069-4A8D-B075-44B5B49142CB"
},
{
height:250,
partType:5,
typeid:19,
uuid:"936D8219-5A4E-48EB-882F-00599F44D155"
}
],
name:"report_dettaglio_cumulate_SKA",
navigatorID:"-1",
onLoadMethodID:"2F866351-014B-43ED-819B-69D9F7484943",
onShowMethodID:"E13FC2CB-2124-4D51-8A3D-0E94ECA007F2",
paperPrintScale:100,
showInMenu:true,
size:"660,250",
styleName:"keeneight",
typeid:3,
uuid:"D1600C74-BD94-4DC9-B97C-281A9A131C91"