/**
 * @properties={typeid:35,uuid:"9CCDD08C-999C-4A4F-993E-A28FF220E4C4",variableType:-4}
 */
var addingParts = {type			:	"AND SK.TIPO_ANOMALIA = ?",
                   subtype		:	"AND SK.SOTTOTIPO_ANOMALIA = ?",
                   group		:	"AND SK.GRUPPO_FUNZIONALE = ?",
                   subgroup		:	"AND SK.SOTTOGRUPPO_FUNZIONALE = ?",
                   severity		:	"AND SK.PUNTEGGIO_DEMERITO = ?",
                   def_reason	:	"AND SK.ATTRIBUZIONE = ?"};

/**
 * @properties={typeid:35,uuid:"BA78EFEE-B68E-496F-924E-87191AA6F277",variableType:-4}
 */
var args = {base: null, open: null, closed: null};

/**
 * @properties={typeid:35,uuid:"834DE665-046A-47DE-97EF-84A699861B3C",variableType:-4}
 */
var container = null;

/**
 * @properties={typeid:35,uuid:"DCDBACCB-C0BD-4219-8B0E-9517A6099994",variableType:-4}
 */
var dates = {"%"		:	"SK.DATA_REGISTRAZIONE_SCHEDA",
             "APERTA"	:	"SK.DATA_REGISTRAZIONE_SCHEDA",
             "CHIUSA"	:	"SK.DATA_CHIUSURA"};

/**
 * @properties={typeid:35,uuid:"50345289-6AF3-4F7D-BE8F-E90547970CB7",variableType:-4}
 */
var filters = ["def_reason"];

/**
 * @properties={typeid:35,uuid:"2A7B7BD9-99DB-4B36-AF3E-7AC12C691072",variableType:-4}
 */
var query = {base: null, open: null, closed: null};

/**
 * @properties={typeid:35,uuid:"68F4734D-DE9D-4A61-80E7-F68C2CE6AC06",variableType:-4}
 */
var query_ex = {query: null, args: null, form: null};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"0C722D23-B8F0-466B-BDBE-7D93A40961D1"}
 */
var replacePart = "SK.CODICE_MODELLO = ?";

/**
 * @properties={typeid:35,uuid:"C4A143F7-2AF5-4B25-8133-5BB7F5C36260",variableType:-4}
 */
var title = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"6F8FDA3E-628B-418C-91B4-48741D5F4C1F"}
 */
var versionSpecificPart = "AND SK.CODICE_VERSIONE IN ('**VERSIONS_HERE**')";

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"2F866351-014B-43ED-819B-69D9F7484943"}
 */
function onLoad(event) {
	container = forms.report_dettaglio_cumulate;
	//Setto le query e i parametri TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	//Base
	query.base = "SELECT OFFSET,COUNT(*) FROM (SELECT TO_CHAR(SK.DATA_REGISTRAZIONE_SCHEDA,'**FORMAT_HERE**') OFFSET FROM K8_SCHEDE_ANOMALIE SK WHERE SK.CODICE_MODELLO = ?) GROUP BY OFFSET ORDER BY OFFSET";
	args.base = [];
	//Open
	query.open = query.base.replace(replacePart,"SK.STATO_ANOMALIA = ? AND " + replacePart);
	args.open = ["APERTA"];
	//Closed 
	query.closed = query.base.replace(/SK.DATA_REGISTRAZIONE_SCHEDA/g,"SK.DATA_CHIUSURA").replace(replacePart,"SK.STATO_ANOMALIA = ? AND " + replacePart);
	args.closed = ["CHIUSA"];
	//Estrattore
	query_ex.query = "SELECT ID_ FROM (SELECT SK.K8_ANOMALIE_ID ID_, TO_CHAR(**DATE_HERE**,'**FORMAT_HERE**') OFFSET FROM K8_SCHEDE_ANOMALIE SK WHERE SK.CODICE_MODELLO = ? AND SK.STATO_ANOMALIA LIKE ?) WHERE OFFSET <= ?";
	query_ex.args = [];
	query_ex.form = forms.schede_anomalie_record.controller.getName();
	title = "Schede anomalia";
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"E13FC2CB-2124-4D51-8A3D-0E94ECA007F2"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.setupFilters();
	container.drawChart();
}
