items:[
{
anchors:15,
beanClassName:"com.objectplanet.chart.LineChart",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_21\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"com.objectplanet.chart.LineChart\"> \
  <void property=\"background\"> \
   <object class=\"java.awt.Color\"> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>255<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"chartData\"> \
   <void id=\"StringArray0\" property=\"seriesLabels\"/> \
   <void property=\"seriesLabels\"> \
    <object idref=\"StringArray0\"/> \
   <\/void> \
  <\/void> \
  <void id=\"StringArray1\" property=\"legendLabels\"/> \
  <void property=\"legendLabels\"> \
   <object idref=\"StringArray1\"/> \
  <\/void> \
  <void property=\"name\"> \
   <string>bean_858<\/string> \
  <\/void> \
  <void id=\"StringArray2\" property=\"seriesLabels\"/> \
  <void property=\"seriesLabels\"> \
   <object idref=\"StringArray2\"/> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"0,0",
name:"chart",
size:"660,250",
typeid:12,
uuid:"0A7DED5E-5F66-452A-88BA-7E7F0A1926DC"
},
{
height:250,
partType:5,
typeid:19,
uuid:"30A53DEB-60CF-4089-81CB-9FC38ED177A1"
}
],
name:"report_dettaglio_cumulate_SKM",
navigatorID:"-1",
onLoadMethodID:"4270D81B-F5D2-40F9-8940-3B5048562CD7",
onShowMethodID:"F92D8E04-4CC9-4AF4-887E-E6B602F3D659",
paperPrintScale:100,
showInMenu:true,
size:"660,250",
styleName:"keeneight",
typeid:3,
uuid:"F359B2D3-C4B0-4B88-903E-07AF91B754A2"