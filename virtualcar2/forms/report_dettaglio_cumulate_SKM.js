/**
 * @properties={typeid:35,uuid:"B066FC8C-9DB8-41CC-8ABE-8BD56A068807",variableType:-4}
 */
var addingParts = {type			:	"AND SKA.TIPO_ANOMALIA = ?",
                   subtype		:	"AND SKA.SOTTOTIPO_ANOMALIA = ?",
                   group		:	"AND SKM.GRUPPO_FUNZIONALE = ?",
                   subgroup		:	"AND SKM.GRUPPO_FUNZIONALE2 = ?",
                   severity		:	"AND SKA.PUNTEGGIO_DEMERITO = ?",
                   def_reason	:	"AND SKA.ATTRIBUZIONE = ?",
                   mod_reason	:	"AND SKM.CAUSALE_MODIFICA = ?"};

/**
 * @properties={typeid:35,uuid:"BCB6099C-085E-486D-B197-3EE268AA9218",variableType:-4}
 */
var args = {base: null, open: null, closed: null};

/**
 * @properties={typeid:35,uuid:"FBDF71A1-34B9-4A10-AF9C-599965B0A2CE",variableType:-4}
 */
var container = null;

/**
 * @properties={typeid:35,uuid:"0589A30E-8578-44AA-ACC3-63760186229F",variableType:-4}
 */
var dates = {"%"		:	"SKM.DATA_PROPOSTA_MODIFICA",
             "APERTA"	:	"SKM.DATA_PROPOSTA_MODIFICA",
             "CHIUSA"	:	"NVL(SKM.DATA_APPROV_SK_PROP_MOD,SKM.DATA_ANNUL_SK_PROP_MOD)"};

/**
 * @properties={typeid:35,uuid:"3848BCBB-3532-4A7A-AD4C-03D5EB19905B",variableType:-4}
 */
var filters = ["def_reason","mod_reason","history_flag"];

/**
 * @properties={typeid:35,uuid:"8CB5E2C9-1B46-47BF-830D-99EC354348B4",variableType:-4}
 */
var query = {base: null, open: null, closed: null};

/**
 * @properties={typeid:35,uuid:"D041F0E3-2648-4E8F-B8FF-2CD05C3A6A5A",variableType:-4}
 */
var query_ex = {query: null, args: null, form: null};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F71B7B02-C61B-4EF9-9ECE-BAF5E93D1208"}
 */
var replacePart = "SKM.MODELLO = ?";

/**
 * @properties={typeid:35,uuid:"CC9478CD-2D22-4765-A1B0-0B2501E7BB81",variableType:-4}
 */
var title = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A7D0B446-5536-42D3-BD61-56C406AFA5A4"}
 */
var versionSpecificPart = "AND SKM.VERSIONE IN ('**VERSIONS_HERE**')";

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"4270D81B-F5D2-40F9-8940-3B5048562CD7"}
 */
function onLoad(event) {
	container = forms.report_dettaglio_cumulate;
	setupQuery(false);
	title = "Schede modifica";
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"F92D8E04-4CC9-4AF4-887E-E6B602F3D659"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.setupFilters();
	container.drawChart();
}

/**
 * @properties={typeid:24,uuid:"F65C2DD0-D7B0-468C-9A2E-8A8283C0B5E8"}
 */
function setupQuery(_history){
	var q_noHistory = "SELECT OFFSET,COUNT(*) FROM (SELECT TO_CHAR(SKM.DATA_PROPOSTA_MODIFICA,'**FORMAT_HERE**') OFFSET FROM K8_SCHEDE_MODIFICA SKM WHERE SKM.MODELLO = ?) GROUP BY OFFSET ORDER BY OFFSET";
	var q_history = "SELECT OFFSET,COUNT(*) FROM (SELECT TO_CHAR(SKM.DATA_PROPOSTA_MODIFICA,'**FORMAT_HERE**') OFFSET FROM K8_SEGNALAZIONI_FULL SKA INNER JOIN K8_SCHEDE_MODIFICA SKM ON SKA.ENTE = SKM.ENTE_INSERIMENTO_ANOMALIA AND SKA.NUMERO_SCHEDA = SKM.NR_SCHEDA_ANOMALIA WHERE SKM.MODELLO = ?) GROUP BY OFFSET ORDER BY OFFSET";
	//Setto le query e i parametri TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	//Base
	query.base = (_history) ? q_history : q_noHistory;
	args.base = [];
	//Open
	query.open = query.base.replace(replacePart,"SKM.STATO_SCHEDA = ? AND " + replacePart);
	args.open = ["APERTA"];
	//Closed
	query.closed = query.base.replace(/SKM.DATA_PROPOSTA_MODIFICA/g,"NVL(SKM.DATA_APPROV_SK_PROP_MOD,SKM.DATA_ANNUL_SK_PROP_MOD)").replace(replacePart,"SKM.STATO_SCHEDA = ? AND " + replacePart);
	args.closed = ["CHIUSA"];
	
	var q_exNoHistory = "SELECT ID_ FROM (SELECT SKM.K8_SCHEDE_MODIFICA_ID ID_, TO_CHAR(**DATE_HERE**,'**FORMAT_HERE**') OFFSET FROM K8_SCHEDE_MODIFICA SKM WHERE SKM.MODELLO = ? AND SKM.STATO_SCHEDA LIKE ?) WHERE OFFSET <= ?";
	var q_exHistory = "SELECT ID_ FROM (SELECT SKM.K8_SCHEDE_MODIFICA_ID ID_, TO_CHAR(**DATE_HERE**,'**FORMAT_HERE**') OFFSET FROM K8_SEGNALAZIONI_FULL SKA INNER JOIN K8_SCHEDE_MODIFICA SKM ON SKA.ENTE = SKM.ENTE_INSERIMENTO_ANOMALIA AND SKA.NUMERO_SCHEDA = SKM.NR_SCHEDA_ANOMALIA WHERE SKM.MODELLO = ? AND SKM.STATO_SCHEDA LIKE ?) WHERE OFFSET <= ?";
	query_ex.query = (_history) ? q_exHistory : q_exNoHistory;
	query_ex.args = [];
	query_ex.form = forms.schede_modifica_record.controller.getName();
}
