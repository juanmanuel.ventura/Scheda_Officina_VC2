/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"166D9C28-90A9-42F4-BA1C-66F53BA7BD9C",variableType:4}
 */
var _t1 = 10;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"6BCA9FE7-1E90-4E3F-8EF6-1F84980D6F99",variableType:4}
 */
var _t2 = 20;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"81763478-6E07-4694-AEDE-FC5C19267607",variableType:4}
 */
var _t3 = 30;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"4992A9C3-87F5-4FC1-BA50-BB32C460847A",variableType:4}
 */
var _t4 = 40;

/**
 * @properties={typeid:35,uuid:"456A2782-7D13-4E96-84C9-F7D602066139",variableType:-4}
 */
var config = {
	attributes: { type: "line", id: "getVList" },
	bgc1: "#cccccc", bgc2: "#ffffff",
	grid: "#ff0000",
	fntx: "Verdana,0,10", fnty: "Verdana,0,10",
	styles: { }
};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"72BF3C8B-2F9D-4F04-B723-A0B9F4784E6E"}
 */
var def_reason = "";

/**
 * @properties={typeid:35,uuid:"E0A31720-FE79-4469-ACCE-D862A73B23F2",variableType:-4}
 */
var filterNames = ["type", "subtype", "group", "subgroup", "severity", "def_reason"];

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"5DE25401-910D-44F6-99D4-50E60B4FF240"}
 */
var group = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"6940D0F1-AB15-4E2A-A26A-975C24971972"}
 */
var interval_mode = "cumulates\nvalues";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"5B0F3C91-A5D6-42E1-AE38-06CEFBE78B19"}
 */
var project_tmp = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"DBB40DC4-6939-4E5E-A2F8-135BAEBD082B"}
 */
var quotes = "Si";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"35672BB7-1FE2-48C3-A1ED-340C1AEFA8DB"}
 */
var selected = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"61F96262-A795-4398-B1FF-76B662BFAC4F"}
 */
var severity = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"0F6C4198-1991-4782-9EA0-0985793287B2"}
 */
var subgroup = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1C5C74D5-8157-4C2D-8930-338FED0AC26F"}
 */
var subtype = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"9FE3B57E-2AC8-4B6B-8D44-910A2CD47C2A"}
 */
var type = "";

/**
 * @properties={typeid:24,uuid:"D72AC815-2A8A-4527-8584-62570BB32AF6"}
 */
function getVList() {
	var _vl = application.getValueListArray("progetti");
	var _m = application.getValueListArray("static$report_interval_mode");
	var _r = [];
	for (var _i = 0; _i < _vl.length; _i++) {
		if (_vl[_i]) {
			for (var _j in _m) {
				_r.push(_vl[_i] + " - " + application.getValueListDisplayValue("static$report_interval_mode", _m[_j]));
			}
		}
	}
	return _r;
}

/**
 * @properties={typeid:24,uuid:"2823A06E-6D0B-442C-A263-FDA0EDF9F089"}
 */
function nfx_isProgram() {
	return false;
}

/**
 * @properties={typeid:24,uuid:"DC103C82-1BCD-4061-BA39-262FF260CAEE"}
 */
function getData(p, v) {

	if (!forms[selected].query || !forms[selected].args) throw "Content query and/or args not detected";
	/** @type {String} */
	var query = forms[selected].query;
	//FS
	/** @type {Array<String>} */
	var args = forms[selected].args;
	//Inizio parte di filtraggio
	var filters = {
		type: type,
		subtype: subtype,
		group: group,
		subgroup: subgroup,
		severity: severity,
		def_reason: def_reason
	};
	var adding = forms[selected].addingParts;
	var replaceFinal = forms[selected].replacePart;
	var argsFinal = new Array();
	for (var i = 0; i < filterNames.length; i++) {
		if (filters[filterNames[i]]) {
			replaceFinal += " " + adding[filterNames[i]];
			argsFinal.push(filters[filterNames[i]]);
		}
	}
	//Aggiunta parte relativa alla gestione delle versioni AS400
	if (v) replaceFinal += " " + forms[selected].versionSpecificPart.replace("**VERSIONS_HERE**", v);
	query = query.replace(forms[selected].replacePart, replaceFinal);
	args = args.concat([p]).concat(argsFinal);
	//Fine parte di filtraggio
	//application.output(query + "\n" + args);
	/** @type {JSDataSet} */
	var ds = databaseManager.getDataSetByQuery('ferrari', query, args, 1);
	var values = [null, ds.getValue(1, 1), ds.getValue(1, 2), ds.getValue(1, 3), ds.getValue(1, 4), null];
	var cumulates = [];
	var sum = 0;
	for (var _i = 0; _i < values.length; _i++) {
		if (values[_i] === null) {
			values[_i] = java.lang.Double.NaN;
			cumulates[_i] = java.lang.Double.NaN;
		} else {
			sum += values[_i];
			cumulates[_i] = sum;
		}
	}
	return { values: values, max_values: ds.getValue(1, 5), cumulates: cumulates, max_cumulates: sum };
}

/**
 * @properties={typeid:24,uuid:"2C6C1851-A9CE-4569-8CF6-BDFF27FB6C33"}
 */
function setupChart() {
	var _v = forms.nfx_interfaccia_gestione_salvataggi.load(controller.getName() + "_defaultBudget");
	if (_v) {
		_t1 = _v[0];
		_t2 = _v[1];
		_t3 = _v[2];
		_t4 = _v[3];
	}
	//FS
	config = {
		attributes: { type: "line", id: "getVList" },
		bgc1: "#cccccc", bgc2: "#ffffff",
		grid: "#ff0000",
		fntx: "Verdana,0,10", fnty: "Verdana,0,10",
		styles: { }
	};
	/** @type {java.awt.chart} */
	var chart = forms[selected].elements.chart;
	if (!chart) throw "Selected form must have a chart element called \"chart\"";
	chart.setZoomOn(true);
	chart.setChartBackground(globals.utils_getJColor(config.bgc1));
	chart.setChartBackground2(globals.utils_getJColor(config.bgc2));

	chart.setAutoLabelSpacingOn(true);
	chart.setSampleLabelsOn(true);

	chart.setValueLinesOn(true);

	chart.setFont("valueLabelFont", new java.awt.Font("Verdana", java.awt.Font.PLAIN, 9));

	chart.setLegendOn(true);
	chart.setLegendPosition(2);
	chart.setLegendBoxSizeAsFont(true);

	chart.setFont("sampleLabelFont", globals.utils_getJFont(config.fntx));
	chart.setFont("rangeLabelFont", globals.utils_getJFont(config.fnty));
}

/**
 * @properties={typeid:24,uuid:"E2DCA705-98E9-4C40-B872-363FFF635A89"}
 */
function drawChart() {
	//FS
	/** @type {java.awt.chart} */
	var chart = forms[selected].elements.chart;
	if (!chart) throw "Selected form must have a chart element called \"chart\"";
	var projects = forms.report_anomalie_container.projects;
	var versions = forms.report_anomalie_container.versions;
	if (projects) {
		var p = projects.split("\n");
		var data = [{ values: [java.lang.Double.NaN, _t1, _t2, _t3, _t4, java.lang.Double.NaN], max: Math.max(_t1, _t2, _t3, _t4) }];
		var legend = ["Riferimento-Obiettivo"];
		for (var i = 0; i < p.length; i++) {
			var v = (typeof versions[p[i]] == "string") ? null : versions[p[i]].join("','");
			var d = getData(p[i], v);
			if (d) {
				legend = legend.concat(createLegend(p[i]));
				data = data.concat(createSeries(d));
			}
		}
		if (data.length > 0) {
			var max = 0;
			chart.setSeriesCount(data.length);
			chart.setSampleLabels([null, "T1", "T2", "T3", "T4", null]);
			chart.setSampleCount(6);

			chart.setValueLabelStyle(0);
			chart.setSampleHighlightStyle(1, 20);

			if (quotes != "No") {
				chart.setValueLabelStyle(0);
				chart.setSampleHighlightOn(true);
			} else {
				chart.setValueLabelStyle(3);
				chart.setSampleHighlightOn(false);
			}

			chart.setValueLabelsOn(true);

			for (var i2 = 0; i2 < data.length; i2++) {
				max = Math.max(max, data[i2].max);
				if (config.styles[legend[i2]]) {
					chart.setSampleColor(i2, globals.utils_getJColor(config.styles[legend[i2]][0]));
					chart.setLineWidth(i2, config.styles[legend[i2]][1]);
					chart.setLineStroke(i2, config.styles[legend[i2]][2]);
				} else {
					chart.setSampleColor(i2, globals.utils_getJColor(forms.report_anomalie_container.colors[i2]));
					chart.setLineWidth(i2, 2);
					chart.setLineStroke(i2, [0]);
				}
				chart.setSampleHighlightStyle(i2, 1, 30);
				chart.setSampleValues(i2, data[i2].values);
			}

			var m = Math.ceil(Math.floor(max / 10) + 1) * 10 + 10;
			chart.setRange(0, m);
			chart.setLegendLabels(legend);

			var gridLines = new Array();
			chart.setGridLines(gridLines);
			chart.setGridLinesColor(globals.utils_getJColor(config.grid));

			if (forms[selected].title) {
				chart.setTitleOn(true);
				chart.setTitle(forms[selected].title + " - " + utils.dateFormat(new Date(), forms.report_anomalie_container.chartTitleDateFormat));
			}

			chart.visible = true;
		} else {
			chart.visible = false;
			plugins.dialogs.showWarningDialog("Nessun dato", "Non è presente alcun dato relativo ai criteri selezionati", "Ok");
		}
	} else {
		chart.visible = false;
	}
}

/**
 * @properties={typeid:24,uuid:"7DC70472-6197-42B4-B008-CE4E88BE9E30"}
 */
function exportData(project, value) {
	/** @type {String} */
	var query = forms[selected].query_ex.query;
	//FS
	/** @type {Array<String>} */
	var args = forms[selected].query_ex.args;
	//Inizio parte di filtraggio
	var filters = {
		type: type,
		subtype: subtype,
		group: group,
		subgroup: subgroup,
		severity: severity,
		def_reason: def_reason
	};
	var adding = forms[selected].addingParts;
	var replaceFinal = forms[selected].replacePart;
	var argsFinal = new Array();
	for (var i = 0; i < filterNames.length; i++) {
		if (filters[filterNames[i]]) {
			replaceFinal += " " + adding[filterNames[i]];
			argsFinal.push(filters[filterNames[i]]);
		}
	}
	//Aggiunta parte relativa alla gestione delle versioni AS400
	var versions = forms.report_anomalie_container.versions;
	if (typeof versions[parseLegendLable(project).p] != "string") replaceFinal += " " + forms[selected].versionSpecificPart.replace("**VERSIONS_HERE**", versions[parseLegendLable(project).p].join("','"));
	query = query.replace(forms[selected].replacePart, replaceFinal);
	args = args.concat([parseLegendLable(project).p]).concat(argsFinal);
	//Fine parte di filtraggio
	//application.output(query + "\n" + args);
	forms[forms[selected].query_ex.form].controller.loadRecords(query, args);
	forms[forms[selected].query_ex.form].report();
}

/**
 * @properties={typeid:24,uuid:"67275066-2C52-4D85-A456-2B067A59961D"}
 */
function showHints() {
	var _msg = "T1: range di tempo tra apertura SA e SKA\nT2: range di tempo tra apertura e chiusura SKA\nT3: range di tempo tra apertura e approvazione SKM\nT4: range di tempo tra approvazione SKM e messa in produzione";
	plugins.dialogs.showInfoDialog("Significato intervalli", _msg, "Ok");
}

/**
 * @properties={typeid:24,uuid:"1FC2B949-096B-4294-A7C1-A5A1D37C0DEC"}
 */
function setDefaultBudget() {
	var _v = [_t1, _t2, _t3, _t4];
	forms.nfx_interfaccia_gestione_salvataggi.save(_v, controller.getName() + "_defaultBudget");
	plugins.dialogs.showInfoDialog("Salvataggio riuscito", "I valori attualmente impostati sono stati salvati come valori di default.", "Ok");
}

/**
 * @properties={typeid:24,uuid:"FE55BB42-BEB1-4046-85DB-CCFD320E1D5C"}
 */
function createLegend(_project) {
	var _r = [];
	var _v = interval_mode.split("\n");
	for (var _i = 0; _i < _v.length; _i++) {
		_r.push(_project + " - " + application.getValueListDisplayValue("static$report_interval_mode", _v[_i]));
	}
	return _r;
}

/**
 * @properties={typeid:24,uuid:"D61AE5B3-7194-47E9-89FE-45E6097A1974"}
 */
function createSeries(_data) {
	var _r = [];
	var _v = interval_mode.split("\n");
	for (var _i = 0; _i < _v.length; _i++) {
		_r.push({ values: _data[_v[_i]], max: _data["max_" + _v[_i]] });
	}
	return _r;
}

/**
 * @properties={typeid:24,uuid:"D81B0383-E5B9-4C75-8B2D-F719E3E593D7"}
 */
function getProjectsForExport() {
	return forms[selected].elements.chart.getLegendLabels().slice(1);
}

/**
 * @properties={typeid:24,uuid:"CF3513E2-ECD8-47E7-971E-7486E4D3A25F"}
 */
function parseLegendLable(label) {
	var l = label.split(" - ");
	return { p: l[0], s: l[1] };
}
