/**
 * @properties={typeid:35,uuid:"E2DD3F6C-877E-4FDB-BE23-903896591D9D",variableType:-4}
 */
var addingParts = {type			: "AND A.TIPO_ANOMALIA = ?",
                   subtype		: "AND A.SOTTOTIPO_ANOMALIA = ?",
                   group		: "AND M.GRUPPO_FUNZIONALE = ?",
                   subgroup		: "AND M.GRUPPO_FUNZIONALE2 = ?",
                   severity		: "AND A.PUNTEGGIO_DEMERITO = ?",
                   def_reason	: "AND A.ATTRIBUZIONE = ?"};

/**
 * @properties={typeid:35,uuid:"81F987E8-7121-424D-B2A9-7FB3B66BD102",variableType:-4}
 */
var args = null;

/**
 * @properties={typeid:35,uuid:"15BC2447-E85E-4CDD-9925-CA5B6F0A1C5D",variableType:-4}
 */
var container = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C021F3EA-FD91-410A-9420-1E62F9F7A526"}
 */
var query = "";

/**
 * @properties={typeid:35,uuid:"858A8B73-28B6-47DC-AC4C-F61DC69E57EE",variableType:-4}
 */
var query_ex = {query: null, args: null, form: null};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B237BED5-29C5-44B8-A1DA-EBAC7361D7DB"}
 */
var replacePart = "M.MODELLO = ?";

/**
 * @properties={typeid:35,uuid:"DD9C3751-B846-4997-BDE6-5F7BEA067A03",variableType:-4}
 */
var title = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"A687090D-564A-40A9-843D-58108D20CCBF"}
 */
var versionSpecificPart = "AND M.VERSIONE IN ('**VERSIONS_HERE**')";

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"6BF5C3E3-7C9D-4AE5-81CB-EA23DFDF1A82"}
 */
function onLoad(event) {
	container = forms.report_intervalli;
	//Setto le query e i parametri TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	query = "SELECT AVG_T1,AVG_T2,AVG_T3,AVG_T4,GREATEST(NVL(AVG_T1,0),NVL(AVG_T2,0),NVL(AVG_T3,0),NVL(AVG_T4,0)) MAX_ FROM (SELECT ROUND(AVG(EXTRACT(DAY FROM A.DATA_CHIUSURA_SEGNALAZIONE - A.DATA_RILEVAZIONE))) AVG_T1, ROUND(AVG(EXTRACT(DAY FROM M.DATA_PROPOSTA_MODIFICA - A.DATA_CHIUSURA_SEGNALAZIONE)))  AVG_T2, ROUND(AVG(M.DATA_APPROV_SK_PROP_MOD - M.DATA_PROPOSTA_MODIFICA))  AVG_T3, ROUND(AVG(L.DATA_INTRODUZIONE_MODIFICA_1 - M.DATA_APPROV_SK_PROP_MOD))  AVG_T4 FROM K8_SEGNALAZIONI_FULL A RIGHT OUTER JOIN K8_SCHEDE_MODIFICA M ON A.ENTE = M.ENTE_INSERIMENTO_ANOMALIA AND A.NUMERO_SCHEDA = M.NR_SCHEDA_ANOMALIA LEFT OUTER JOIN K8_LETTERE_MODIFICA L ON M.NR_PROPOSTA_MODIFICA = L.NUMERO_LETTERA WHERE M.MODELLO = ?)";
	args = [];
	//Estrattore
	query_ex.query = "SELECT M.K8_SCHEDE_MODIFICA_ID FROM K8_SEGNALAZIONI_FULL A RIGHT OUTER JOIN K8_SCHEDE_MODIFICA M ON A.ENTE = M.ENTE_INSERIMENTO_ANOMALIA AND A.NUMERO_SCHEDA = M.NR_SCHEDA_ANOMALIA LEFT OUTER JOIN K8_LETTERE_MODIFICA L ON M.NR_PROPOSTA_MODIFICA = L.NUMERO_LETTERA WHERE M.MODELLO = ? GROUP BY M.K8_SCHEDE_MODIFICA_ID";
	query_ex.args = [];
	query_ex.form = forms.schede_modifica_record.controller.getName();
	title = "Intervalli";
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"FB51C8CA-BC58-412F-B48B-4BA49B131DF4"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.drawChart();
}
