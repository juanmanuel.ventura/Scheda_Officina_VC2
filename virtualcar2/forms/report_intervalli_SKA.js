/**
 * @properties={typeid:35,uuid:"0185979F-25DC-4341-9DB6-D70E00556DF8",variableType:-4}
 */
var addingParts = {type			: "AND A.TIPO_ANOMALIA = ?",
                   subtype		: "AND A.SOTTOTIPO_ANOMALIA = ?",
                   group		: "AND A.GRUPPO_FUNZIONALE = ?",
                   subgroup		: "AND A.SOTTOGRUPPO_FUNZIONALE = ?",
                   severity		: "AND A.PUNTEGGIO_DEMERITO = ?",
                   def_reason	: "AND A.ATTRIBUZIONE = ?"};

/**
 * @properties={typeid:35,uuid:"ECF620E0-A151-4574-B1D8-EEB0812F1A6F",variableType:-4}
 */
var args = null;

/**
 * @properties={typeid:35,uuid:"23A7BE4C-E0E6-4764-AFFF-23E271EF2A2B",variableType:-4}
 */
var container = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"9721E734-5721-4A16-8E28-5C92A470DB50"}
 */
var query = "";

/**
 * @properties={typeid:35,uuid:"183C90D7-A224-4E3C-B2A5-E5789944E40B",variableType:-4}
 */
var query_ex = {query: null, args: null, form: null};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"2D1AC17B-F498-4538-9242-1869F680A329"}
 */
var replacePart = "A.CODICE_MODELLO = ?";

/**
 * @properties={typeid:35,uuid:"8BBA7F60-E3AC-4F21-A600-2B98F480521D",variableType:-4}
 */
var title = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"8F17C598-46F0-451F-A836-A77E48AB0C57"}
 */
var versionSpecificPart = "AND A.CODICE_VERSIONE IN ('**VERSIONS_HERE**')";

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"CE5CC356-EA55-4753-912F-D516CB05982F"}
 */
function onLoad(event) {
	container = forms.report_intervalli;
	//Setto le query e i parametri TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	query = "SELECT AVG_T1,AVG_T2,AVG_T3,AVG_T4,GREATEST(NVL(AVG_T1,0),NVL(AVG_T2,0),NVL(AVG_T3,0),NVL(AVG_T4,0)) MAX_ FROM (SELECT ROUND(AVG(EXTRACT(DAY FROM A.DATA_CHIUSURA_SEGNALAZIONE - A.DATA_RILEVAZIONE))) AVG_T1, ROUND(AVG(EXTRACT(DAY FROM M.DATA_PROPOSTA_MODIFICA - A.DATA_CHIUSURA_SEGNALAZIONE)))  AVG_T2, ROUND(AVG(M.DATA_APPROV_SK_PROP_MOD - M.DATA_PROPOSTA_MODIFICA))  AVG_T3, ROUND(AVG(L.DATA_INTRODUZIONE_MODIFICA_1 - M.DATA_APPROV_SK_PROP_MOD))  AVG_T4 FROM K8_SEGNALAZIONI_FULL A LEFT OUTER JOIN K8_SCHEDE_MODIFICA M ON A.ENTE = M.ENTE_INSERIMENTO_ANOMALIA AND A.NUMERO_SCHEDA = M.NR_SCHEDA_ANOMALIA LEFT OUTER JOIN K8_LETTERE_MODIFICA L ON M.NR_PROPOSTA_MODIFICA = L.NUMERO_LETTERA WHERE A.CODICE_MODELLO = ?)";
	args = [];
	//Estrattore
	query_ex.query = "SELECT A.K8_ANOMALIE_ID FROM K8_SEGNALAZIONI_FULL A LEFT OUTER JOIN K8_SCHEDE_MODIFICA M ON A.ENTE = M.ENTE_INSERIMENTO_ANOMALIA AND A.NUMERO_SCHEDA = M.NR_SCHEDA_ANOMALIA LEFT OUTER JOIN K8_LETTERE_MODIFICA L ON M.NR_PROPOSTA_MODIFICA = L.NUMERO_LETTERA WHERE A.CODICE_MODELLO = ? GROUP BY A.K8_ANOMALIE_ID";
	query_ex.args = [];
	query_ex.form = forms.segnalazioni_anomalie_record.controller.getName();
	title = "Intervalli";
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"CED70C54-3875-4668-AF35-705354ED168A"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.drawChart();
}
