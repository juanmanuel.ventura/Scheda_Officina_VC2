/**
 * @properties={typeid:35,uuid:"CDABCEEF-6755-4D03-94E1-5FF49A12E3CE",variableType:-4}
 */
var classes = null;

/**
 * @properties={typeid:35,uuid:"93A31400-0AE8-437A-A25F-EFD2117B3867",variableType:-4}
 */
var config = {
	attributes: { type: "stack", id: "getVList" },
	bgc1: "#cccccc", bgc2: "#ffffff",
	grid: null,
	fntx: "Verdana,0,10", fnty: "Verdana,0,10",
	styles: { }
};

/**@type {Object}
 * @properties={typeid:35,uuid:"15D5DFAB-35AF-4CC0-96DB-FB9D3501932C",variableType:-4}
 */
var defaultClasses = [{ id: 1, start: 0, end: 1 }, { id: 2, start: 2, end: 5 }, { id: 3, start: 6, end: 10 }, { id: 4, start: 11, end: 15 }, { id: 5, start: 16, end: 20 }, { id: 6, start: 21, end: 25 }, { id: 7, start: 26, end: 30 }, { id: 8, start: 31, end: 35 }, { id: 9, start: 36, end: 40 }, { id: 10, start: 41, end: null }];

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1D38DCBA-8DA1-4F85-B39D-77818BBC92CE"}
 */
var delta = "T1\nT2\nT3";

/**@type {Object}
 * @properties={typeid:35,uuid:"A0D79FD2-0426-4DDB-A749-F1BD67664FE4",variableType:-4}
 */
var graphicalActions = {
	"load": { human: "Visualizza", icon: "media:///detail16.png" },
	"export": { human: "Export", icon: "media:///excel16.png" },
	"report": { human: "Full report", icon: "media:///report16.png" }
};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"37325AF6-D03C-4B82-A685-265FDD7D9EC5"}
 */
var mix_filter = "gruppo_funzionale";

/**
 * @properties={typeid:35,uuid:"A8EC872F-F61B-4D06-A4C0-62CCCF70D7AB",variableType:-4}
 */
var mix_filters = {
	"punteggio_demerito": { human: "Punteggio demerito", type: "number", query: "SEVERITY_", vlist: null },
	"gruppo_funzionale": { human: "Gruppo funzionale", type: "string", query: "GROUP_", vlist: "Gruppi" },
	"tipo_anomalia": { human: "Tipo anomalia", type: "string", query: "TYPE_", vlist: "gestione_anomalie$tipo_anomalia" },
	"tipo_componente": { human: "Tipo componente", type: "string", query: "TCOM_", vlist: "gestione_anomalie$tipo_componente" },
	"attribuzione": { human: "Causale anomalia", type: "string", query: "DEFREASON_", vlist: "gestione_anomalie$causale_anomalia" }
};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"633D20EE-2C03-416D-80D6-459E6C7C62EB"}
 */
var selected = "";

/**
 * @properties={typeid:24,uuid:"E48B3E38-82B4-4117-9492-B3CD342EF3EC"}
 */
function getVList() {
	var _list = new Array();
	for (var _mf in mix_filters) {
		if (mix_filters[_mf]["vlist"]) {
			var _vlist = application.getValueListArray(mix_filters[_mf]["vlist"]);
			for (var _vl in _vlist) {
				if (_vl && application.getValueListDisplayValue(mix_filters[_mf]["vlist"], _vlist[_vl])) {
					var _dv = application.getValueListDisplayValue(mix_filters[_mf]["vlist"], _vlist[_vl]);
					_list.push(application.getValueListDisplayValue("static$report_mix_filters", _mf) + " - " + _dv.substr(0, 1).toUpperCase() + _dv.substr(1).toLowerCase());
				}
			}
		} else {
			_list.push(application.getValueListDisplayValue("static$report_mix_filters", _mf) + " - <inserire_valore>");
		}
	}
	return _list.sort();
}

/**
 * @properties={typeid:24,uuid:"A06FA3EA-0072-40AE-908C-A5C0509964EC"}
 */
function nfx_isProgram() {
	return false;
}

/**
 * @properties={typeid:24,uuid:"562366F7-1A5E-4354-A53D-D0A835802A55"}
 */
function getData(p, v) {
	if (!forms[selected].query || !forms[selected].args) throw "Content query and/or args not detected";
	/** @type {String} */
	var query = forms[selected].query;
	query = query.replace("**FILTER_HERE**", mix_filters[mix_filter].query);
	query = query.replace("**MODELS_HERE**", p);
	query = query.replace("**VERSIONS_HERE**", v);
	var args = forms[selected].args;
	//application.output(query + "\n" + args);
	var ds = databaseManager.getDataSetByQuery('ferrari', query, args, -1);
	var offsets = ds.getColumnAsArray(1);
	var filters = ds.getColumnAsArray(2);
	var counts = ds.getColumnAsArray(3);
	var data = new Object();
	for (var i = 0; i < classes.length; i++) {
		data[classes[i].id] = new Object();
		for (var j = 0; j < offsets.length; j++) {
			if ( (!classes[i].start || offsets[j] >= classes[i].start) && (!classes[i].end || offsets[j] <= classes[i].end)) {
				if (!data[classes[i].id][filters[j]]) {
					data[classes[i].id][filters[j]] = counts[j];
				} else {
					data[classes[i].id][filters[j]] += counts[j];
				}
			}
		}
	}
	return (ds.getMaxRowIndex() > 0) ? data : null;
}

/**
 * @properties={typeid:24,uuid:"63CB6C41-2F91-4CCC-8A0B-AE60513CA8F6"}
 */
function setupChart() {
	//FS
	//dep	classes = forms.nfx_interfaccia_gestione_salvataggi.load(controller.getName()) || defaultClasses.slice();
	config = {
		attributes: { type: "stack", id: "getVList" },
		bgc1: "#cccccc", bgc2: "#ffffff",
		grid: null,
		fntx: "Verdana,0,10", fnty: "Verdana,0,10",
		styles: { }
	};
	classes = forms.nfx_interfaccia_gestione_salvataggi.load(controller.getName()) || defaultClasses.slice(0);
	/** @type {java.awt.chart} */
	var chart = forms[selected].elements.chart;
	if (!chart) throw "Selected form must have a chart element called \"chart\"";
	chart.setZoomOn(true);
	chart.setChartBackground(globals.utils_getJColor(config.bgc1));
	chart.setChartBackground2(globals.utils_getJColor(config.bgc2));

	chart.setBarType(1);
	chart.setBarLabelsOn(true);

	chart.setValueLabelStyle(0);

	chart.setValueLinesOn(true);

	chart.setLegendOn(true);
	chart.setLegendPosition(3);
	chart.setLegendBoxSizeAsFont(true);

	chart.setFont("barLabelFont", globals.utils_getJFont(config.fntx));
	chart.setFont("rangeLabelFont", globals.utils_getJFont(config.fnty));
}

/**
 * @properties={typeid:24,uuid:"0FF3544E-5831-44FC-8BB2-58E705437F4C"}
 */
function drawChart() {
	//FS
	/** @type {java.awt.chart} */
	var chart = forms[selected].elements.chart;
	if (!chart) throw "Selected form must have a chart element called \"chart\"";
	var projects = forms.report_anomalie_container.projects;
	var versions = forms.report_anomalie_container.versions;
	if (projects) {
		var p = [];
		var v = [];
		for (var _p in versions) {
			if (typeof versions[_p] == "string") {
				p.push(_p);
			} else {
				v = v.concat(versions[_p]);
			}
		}
		var data = getData("('" + p.join("','") + "')", "('" + v.join("','") + "')");
		if (data) {
			chart.setSampleCount(classes.length);
			var filters = getFiltersFromTable(mix_filter, "('" + p.join("','") + "')", "('" + v.join("','") + "')");
			chart.setSeriesCount(filters.length);
			var legend = new Array();
			var sum = new Array();
			for (var i = 0; i < filters.length; i++) {
				var sampleValues = new Array();
				for (var j = 0; j < classes.length; j++) {
					sampleValues[j] = (data[j + 1] && data[j + 1][filters[i]]) ? data[j + 1][filters[i]] : 0;
					sum[j] = (!sum[j]) ? sampleValues[j] : sum[j] + sampleValues[j];
				}
				chart.setSampleValues(i, sampleValues);
				var _style = config.styles[application.getValueListDisplayValue("static$report_mix_filters", mix_filter) + " - " + getLegendLabel(filters[i])];
				if (_style) {
					chart.setSampleColor(i, globals.utils_getJColor(_style[0]));
				} else {
					chart.setSampleColor(i, globals.utils_getJColor(forms.report_anomalie_container.colors[i]));
				}
				legend.push(getLegendLabel(filters[i]));
			}
			chart.setValueLabelsOn(true);
			chart.setLegendLabels(legend);
			//TODO: togliere costrutto for e utilizzare callback
			var max = 0;
			for (var i2 = 0; i2 < sum.length; i2++) {
				max = Math.max(max, sum[i2]);
			}
			chart.setRange(0, max + 5);

			chart.setBarLabels(getBarLabels());

			if (forms[selected].title) {
				chart.setTitleOn(true);
				chart.setTitle(forms[selected].title + " - " + projects.replace(/\n/g, ",") + " " + mix_filters[mix_filter].human + " - " + utils.dateFormat(new Date(), forms.report_anomalie_container.chartTitleDateFormat));
			}

			chart.visible = true;
		} else {
			chart.visible = false;
			plugins.dialogs.showWarningDialog("Nessun dato", "Non è presente alcun dato relativo ai criteri selezionati", "Ok");
		}
	} else {
		chart.visible = false;
	}
}

/**
 * @properties={typeid:24,uuid:"7B9D989C-1561-45E0-9267-E2DB4D1912B1"}
 */
function getFiltersFromTable(filter, project, version) {
	var local_filter = (mix_filters[filter].type == "number") ? "to_number(A." + filter + ")" : "A." + filter;
	var query = forms[selected].query_table.query.replace(/FILTER_/g, local_filter).replace(/MODELS_/g, project).replace(/VERSIONS_/g, version);
	var args = forms[selected].query_table.args;
	var ds = databaseManager.getDataSetByQuery('ferrari', query, args, -1);
	return ds.getColumnAsArray(1);
}

/**
 * @properties={typeid:24,uuid:"3DE54E79-6540-4085-9B1F-1A72E3AB6445"}
 */
function getBarLabels() {
	var labels = new Array();
	for (var i = 0; i < classes.length; i++) {
		var s = (classes[i].start !== null) ? classes[i].start : " < ";
		var e = (classes[i].end !== null) ? classes[i].end : " > ";
		labels.push("[" + s + ":" + e + "]");
	}
	return labels;
}

/**
 * @properties={typeid:24,uuid:"42B7F434-8CB5-4FD8-ABFA-E073DE5CBC6C"}
 */
function getLegendLabel(value) {
	if (mix_filters[mix_filter].vlist) {
		var str = application.getValueListDisplayValue(mix_filters[mix_filter].vlist, value);
		return (str) ? str.substr(0, 1).toUpperCase() + str.substr(1).toLowerCase() : value;
	} else {
		return value;
	}
}

/**
 * @properties={typeid:24,uuid:"68F77DF7-DD15-4A89-A9BC-4F46BB3FE3AC"}
 */
function exportData(project, value) {
	var _object = forms.report_anomalie_container.versions;
	var p = null;
	var v = null;
	if (typeof _object[project] == "string") {
		p = "('" + project + "')";
		v = "('')";
	} else {
		p = "('')";
		v = "('" + _object[project].join("','") + "')"
	}
	var query = forms[selected].query_ex.query.replace(/\*\*FILTER_HERE\*\*/g, mix_filters[mix_filter].query).replace(/\*\*MODELS_HERE\*\*/g, p).replace(/\*\*VERSIONS_HERE\*\*/g, v);
	var s = classes[value].start || 0;
	var e = classes[value].end || 1000000;
	var args = forms[selected].query_ex.args.concat([s, e]);
	var form = forms[selected].query_ex.form;
	//application.output(query + "\n" + args);
	forms[form].controller.loadRecords(query, args);
	globals.nfx_launchExport(form);
}

/**@param {JSEvent} event
 * @properties={typeid:24,uuid:"EE9B265D-7335-45AD-B92F-AECB1B97A8E3"}
 */
function openClassesPopup(event) {
	forms.report_anomalie_popup_classes.openPopup(controller.getName(), classes);
}

/**@param {java.awt.event.MouseEvent} event
 * @properties={typeid:24,uuid:"3CF6F57E-8996-4CCD-A374-2EA436B60C21"}
 */
function rightClickHandler(event) {
	if (event.getID() == java.awt.event.MouseEvent.MOUSE_CLICKED && event.getButton() == java.awt.event.MouseEvent.BUTTON3) {
		//application.output("click tasto destro");

		//FS
		/** @type {java.awt.chart} */
		var chart = forms[selected].elements.chart;
		var serie = chart.getLastSelectedSeries();
		var sample = chart.getLastSelectedSample();
		/** @type {plugins.window.Popup} */
		var menu = plugins.window.createPopupMenu();
		//dep	var menuItems = new Array();
		if (sample != -1 && serie != -1) {
			//FS
			/** @type {Array<String>} */
			var actions = forms[selected].actions;
			/** @type {plugins.window.MenuItem} */
			var item = null;
			for (var i = 0; i < actions.length; i++) {
				//FS
				item = null;
				//dep		item = plugins.popupmenu.createMenuItem(graphicalActions[actions[i]].human,graphicalExportData,graphicalActions[actions[i]].icon);
				//dep		item.setMethodArguments([serie,sample,actions[i]]);
				item = menu.addMenuItem(graphicalActions[actions[i]].human, graphicalExportData, graphicalActions[actions[i]].icon);
				item.methodArguments = [serie, sample, actions[i].toString()];

			}

		} else {
			//dep		menuItems.push(plugins.popupmenu.createMenuItem("Nessun elemento selezionato...",null,"media:///cancel16.png"));
			menu.addMenuItem("Nessun elemento selezionato...", null, "media:///cancel16.png");

		}

		//TODO: bisogna inserire in NFX un metodo che restituisca la finestra dell'applicazione
		//application.output("menu" + menu);
		/** @type {java.awt.Component} */
		var labelAsComponent = forms.nfx_interfaccia_pannello_base.elements.keyLabel;
		//		var window = Packages.javax.swing.SwingUtilities.getWindowAncestor(forms.nfx_interfaccia_pannello_base.elements.keyLabel);
		var window = Packages.javax.swing.SwingUtilities.getWindowAncestor(labelAsComponent);
		var pointer = java.awt.MouseInfo.getPointerInfo().getLocation();
		//dep		plugins.popupmenu.showPopupMenu(pointer.getX() - window.getX(),pointer.getY() - window.getY(),menuItems);
		menu.show(pointer.getX() - window.getX(), pointer.getY() - window.getY());

	}
}

/**@param {Number} index
 * @param {Number} parentIndex
 * @param {Boolean} isSelected
 * @param {String} parentText
 * @param {String} menuText
 * @param {Number} serie
 * @param {Number} sample
 * @param {String} action
 * @properties={typeid:24,uuid:"5BB3260F-B6A4-4BCC-A518-A764857205EF"}
 */
function graphicalExportData(index, parentIndex, isSelected, parentText, menuText, serie, sample, action) {
	//application.output("Serie " + serie + " sample " + sample + "action " + action);
	var versions = forms.report_anomalie_container.versions;
	var p = [];
	var v = [];
	for (var _p in versions) {
		if (typeof versions[_p] == "string") {
			p.push(_p);
		} else {
			v = v.concat(versions[_p]);
		}
	}
	var query = forms[selected].query_exGraphical.query.replace(/\*\*FILTER_HERE\*\*/g, mix_filters[mix_filter].query).replace(/\*\*MODELS_HERE\*\*/g, "('" + p.join("','") + "')").replace(/\*\*VERSIONS_HERE\*\*/g, "('" + v.join("','") + "')");
	var s = classes[sample].start || 0;
	var e = classes[sample].end || 1000000;
	var f = getSelectedFilterRealValue(serie);
	var args = forms[selected].query_exGraphical.args.concat([f, s, e]);
	var form = forms[selected].query_exGraphical.form;
	//application.output(query + "\n" + args);
	forms[form].controller.loadRecords(query, args);
	if (action == "load") {
		globals.nfx_goTo(forms[form].controller.getName());
	}
	if (action == "export") {
		globals.nfx_launchExport(forms[form].controller.getName());
	}
	if (action == "report") {
		forms[form].report();
	}

}

/**
 * @properties={typeid:24,uuid:"21336AE0-8C14-49BF-BF54-F7422165289E"}
 */
function getSelectedFilterRealValue(serie) {
	//FS
	/** @type {java.awt.chart} */
	var chart = forms[selected].elements.chart;
	var list = mix_filters[mix_filter].vlist;
	var filterValue = null;
	if (list) {
		var ds = application.getValueListItems(list);
		var real = ds.getColumnAsArray(2);
		var diplay = ds.getColumnAsArray(1);
		var element = chart.getLegendLabels()[serie];
		var index = forms.report_anomalie_container.indexOfCaseInsensitive(diplay, element);
		filterValue = (index != -1) ? real[index] : element;
	} else {
		filterValue = chart.getLegendLabels()[serie];
	}
	return filterValue;
}
