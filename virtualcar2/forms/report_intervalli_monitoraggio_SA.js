/**
 * @properties={typeid:35,uuid:"51E63DB5-EDC4-4BA3-8B12-7149FA1B1788",variableType:-4}
 */
var actions = ["load","export","report"];

/**
 * @properties={typeid:35,uuid:"A21CDBA3-9AF4-4B5C-BC02-D0D4E153E8C0",variableType:-4}
 */
var args = null;

/**
 * @properties={typeid:35,uuid:"990AC0BB-5519-4CE2-BD62-2F31945804D0",variableType:-4}
 */
var container = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"3D34F804-B245-4754-9EAA-C90267CFBA65"}
 */
var query = "";

/**
 * @properties={typeid:35,uuid:"06940A6E-D35B-4ACF-A859-44EE9853692D",variableType:-4}
 */
var query_ex = {query: null, args: null, form: null};

/**
 * @properties={typeid:35,uuid:"EB8FBD6F-5EAD-44D6-A1FB-C19D0E3A7FDB",variableType:-4}
 */
var query_exGraphical = {query: null, args: null, form: null};

/**
 * @properties={typeid:35,uuid:"0FBBD3E9-ABD6-412B-AD07-58187C93DD2F",variableType:-4}
 */
var query_table = {query: null, args: null};

/**
 * @properties={typeid:35,uuid:"CEB15C65-8F73-49E1-A6C0-E2D867267366",variableType:-4}
 */
var title = null;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"5E055FCB-5061-4E05-B5CC-874073F621DD"}
 */
function onLoad(event) {
	container = forms.report_intervalli_monitoraggio;
	//Aggiungo il listener per il click
	
//	JStaffa
//	elements.chart.addMouseListener(container.rightClickHandler);	
//	elements.chart.addMouseListener(java.awt.event.MouseListener(container.rightClickHandler));
//FS
/** @type {java.awt.event.MouseListener} */
	var rightClickHandler=container.rightClickHandler;
	elements.chart.addMouseListener(rightClickHandler);

	//Setto le query e i parametri TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	query = "SELECT SELECTED_,FILTER_,COUNT(*) COUNT_ FROM (SELECT DELTA_ SELECTED_, **FILTER_HERE** FILTER_ FROM (SELECT A.PUNTEGGIO_DEMERITO SEVERITY_,A.TIPO_ANOMALIA TYPE_,A.TIPO_COMPONENTE TCOM_,A.ATTRIBUZIONE DEFREASON_,A.GRUPPO_FUNZIONALE GROUP_,FLOOR(SYSDATE - A.DATA_RILEVAZIONE) DELTA_ FROM K8_SEGNALAZIONI_FULL A WHERE A.STATO_SEGNALAZIONE = 'APERTA' AND (A.CODICE_MODELLO IN **MODELS_HERE** OR A.CODICE_VERSIONE IN **VERSIONS_HERE**))) WHERE FILTER_ IS NOT NULL GROUP BY SELECTED_, FILTER_ ORDER BY SELECTED_ ASC, FILTER_ ASC";
	args = [];
	//Estrattore menù
	query_ex.query = "SELECT ID_ FROM (SELECT A.K8_ANOMALIE_ID ID_, TO_NUMBER(A.PUNTEGGIO_DEMERITO) SEVERITY_, A.TIPO_ANOMALIA TYPE_, A.TIPO_COMPONENTE TCOM_,A.ATTRIBUZIONE DEFREASON_, A.GRUPPO_FUNZIONALE GROUP_, FLOOR(SYSDATE - A.DATA_RILEVAZIONE) DELTA_ FROM K8_SEGNALAZIONI_FULL A WHERE A.STATO_SEGNALAZIONE = 'APERTA' AND (A.CODICE_MODELLO IN **MODELS_HERE** OR A.CODICE_VERSIONE IN **VERSIONS_HERE**)) WHERE **FILTER_HERE** IS NOT NULL AND DELTA_ >= ? AND DELTA_ <= ?";
	query_ex.args = [];
	query_ex.form = forms.segnalazioni_anomalie_record.controller.getName();
	//Estrattore diretto dal chart
	query_exGraphical.query = "SELECT ID_ FROM (SELECT A.K8_ANOMALIE_ID ID_, TO_NUMBER(A.PUNTEGGIO_DEMERITO) SEVERITY_, A.TIPO_ANOMALIA TYPE_, A.TIPO_COMPONENTE TCOM_,A.ATTRIBUZIONE DEFREASON_, A.GRUPPO_FUNZIONALE GROUP_, FLOOR(SYSDATE - A.DATA_RILEVAZIONE) DELTA_ FROM K8_SEGNALAZIONI_FULL A WHERE A.STATO_SEGNALAZIONE = 'APERTA' AND (A.CODICE_MODELLO IN **MODELS_HERE** OR A.CODICE_VERSIONE IN **VERSIONS_HERE**)) WHERE **FILTER_HERE** = ? AND DELTA_ >= ? AND DELTA_ <= ?";
	query_exGraphical.args = [];
	query_exGraphical.form = forms.segnalazioni_anomalie_record.controller.getName();
	//Classificatore filtro
	query_table.query = "SELECT FILTER_ FROM K8_SEGNALAZIONI_FULL A WHERE A.STATO_SEGNALAZIONE = 'APERTA' AND (A.CODICE_MODELLO IN MODELS_ OR A.CODICE_VERSIONE IN VERSIONS_) AND FILTER_ IS NOT NULL GROUP BY FILTER_ ORDER BY FILTER_ ASC";
	query_table.args = [];
	title = "Monitoraggio SA Aperte";
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"7E2C0692-CD9F-4960-80AA-21AA784BC00D"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.drawChart();
}
