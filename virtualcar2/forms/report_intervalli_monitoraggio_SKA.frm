items:[
{
anchors:15,
beanClassName:"com.objectplanet.chart.BarChart",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_21\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"com.objectplanet.chart.BarChart\"> \
  <void property=\"background\"> \
   <object class=\"java.awt.Color\"> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>255<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"chartData\"> \
   <void id=\"StringArray0\" property=\"seriesLabels\"/> \
   <void property=\"seriesLabels\"> \
    <object idref=\"StringArray0\"/> \
   <\/void> \
  <\/void> \
  <void property=\"name\"> \
   <string>bean_793<\/string> \
  <\/void> \
  <void id=\"StringArray1\" property=\"seriesLabels\"/> \
  <void property=\"seriesLabels\"> \
   <object idref=\"StringArray1\"/> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"0,0",
name:"chart",
size:"660,250",
typeid:12,
uuid:"462A1EB9-8417-4D5E-8A7D-8EBC2EE7BF91"
},
{
height:250,
partType:5,
typeid:19,
uuid:"76249885-E1E4-4D4C-AD32-4D62D54A7E8C"
}
],
name:"report_intervalli_monitoraggio_SKA",
navigatorID:"-1",
onLoadMethodID:"32A1781F-D6EE-4BFA-BA21-BB2A5C7FF6EC",
onShowMethodID:"18436EFB-9CDE-4C9A-A53B-8DB1609F115A",
paperPrintScale:100,
showInMenu:true,
size:"660,250",
styleName:"keeneight",
typeid:3,
uuid:"C64AAF68-17D5-467D-85DC-47040A220E5F"