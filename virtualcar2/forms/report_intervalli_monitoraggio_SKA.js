/**
 * @properties={typeid:35,uuid:"213A9431-58C0-4724-B218-DA4FB78578CB",variableType:-4}
 */
var actions = ["load","export"];

/**
 * @properties={typeid:35,uuid:"B73DB738-8B2B-4D65-B62A-14A1EE6314F7",variableType:-4}
 */
var args = null;

/**
 * @properties={typeid:35,uuid:"92AE321A-7146-4027-A5ED-A3029C109F01",variableType:-4}
 */
var container = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"8684CBDB-D829-4DDC-ADEF-4553AD5AEAEA"}
 */
var query = "";

/**
 * @properties={typeid:35,uuid:"138CF2C6-5C99-42B3-9177-28B94F6D06D8",variableType:-4}
 */
var query_ex = {query: null, args: null, form: null};

/**
 * @properties={typeid:35,uuid:"31773C05-09B2-4964-BA71-2EDC8ED6434B",variableType:-4}
 */
var query_exGraphical = {query: null, args: null, form: null};

/**
 * @properties={typeid:35,uuid:"B1E1BB58-12B6-4CB4-B250-AD30F9EBEB07",variableType:-4}
 */
var query_table = {query: null, args: null};

/**
 * @properties={typeid:35,uuid:"EB3BFBA5-C2BA-44DB-BF2A-65085B47A481",variableType:-4}
 */
var title = null;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"32A1781F-D6EE-4BFA-BA21-BB2A5C7FF6EC"}
 */
function onLoad(event) {
	container = forms.report_intervalli_monitoraggio;
	//Aggiungo il listener per il click
	
//	JStaffa
//	elements.chart.addMouseListener(container.rightClickHandler);
//	elements.chart.addMouseListener(java.awt.event.MouseListener(container.rightClickHandler));
	//FS
	/** @type {java.awt.event.MouseListener} */
	var rightClickHandler=container.rightClickHandler;
	elements.chart.addMouseListener(rightClickHandler);
	
	//Setto le query e i parametri TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	query = "SELECT SELECTED_,FILTER_,COUNT(*) COUNT_ FROM (SELECT DELTA_ SELECTED_, **FILTER_HERE** FILTER_ FROM (SELECT A.PUNTEGGIO_DEMERITO SEVERITY_,A.TIPO_ANOMALIA TYPE_,A.TIPO_COMPONENTE TCOM_,A.ATTRIBUZIONE DEFREASON_,A.GRUPPO_FUNZIONALE GROUP_,FLOOR(SYSDATE - A.DATA_REGISTRAZIONE_SCHEDA) DELTA_ FROM K8_SCHEDE_ANOMALIE A WHERE A.STATO_ANOMALIA = 'APERTA' AND (A.CODICE_MODELLO IN **MODELS_HERE** OR A.CODICE_VERSIONE IN **VERSIONS_HERE**))) WHERE FILTER_ IS NOT NULL GROUP BY SELECTED_, FILTER_ ORDER BY SELECTED_ ASC, FILTER_ ASC";
	args = [];
	//Estrattore menù
	query_ex.query = "SELECT ID_ FROM (SELECT A.K8_ANOMALIE_ID ID_, TO_NUMBER(A.PUNTEGGIO_DEMERITO) SEVERITY_, A.TIPO_ANOMALIA TYPE_, A.TIPO_COMPONENTE TCOM_,A.ATTRIBUZIONE DEFREASON_, A.GRUPPO_FUNZIONALE GROUP_, FLOOR(SYSDATE - A.DATA_REGISTRAZIONE_SCHEDA) DELTA_ FROM K8_SCHEDE_ANOMALIE A WHERE A.STATO_ANOMALIA = 'APERTA' AND (A.CODICE_MODELLO IN **MODELS_HERE** OR A.CODICE_VERSIONE IN **VERSIONS_HERE**)) WHERE **FILTER_HERE** IS NOT NULL AND DELTA_ >= ? AND DELTA_ <= ?";
	query_ex.args = [];
	query_ex.form = forms.schede_anomalie_record.controller.getName();
	//Estrattore diretto dal chart
	query_exGraphical.query = "SELECT ID_ FROM (SELECT A.K8_ANOMALIE_ID ID_, TO_NUMBER(A.PUNTEGGIO_DEMERITO) SEVERITY_, A.TIPO_ANOMALIA TYPE_, A.TIPO_COMPONENTE TCOM_,A.ATTRIBUZIONE DEFREASON_, A.GRUPPO_FUNZIONALE GROUP_, FLOOR(SYSDATE - A.DATA_REGISTRAZIONE_SCHEDA) DELTA_ FROM K8_SCHEDE_ANOMALIE A WHERE A.STATO_ANOMALIA = 'APERTA' AND (A.CODICE_MODELLO IN **MODELS_HERE** OR A.CODICE_VERSIONE IN **VERSIONS_HERE**)) WHERE **FILTER_HERE** = ? AND DELTA_ >= ? AND DELTA_ <= ?";
	query_exGraphical.args = [];
	query_exGraphical.form = forms.schede_anomalie_record.controller.getName();
	//Classificatore filtro
	query_table.query = "SELECT FILTER_ FROM K8_SCHEDE_ANOMALIE A WHERE A.STATO_ANOMALIA = 'APERTA' AND (A.CODICE_MODELLO IN MODELS_ OR A.CODICE_VERSIONE IN VERSIONS_) AND FILTER_ IS NOT NULL GROUP BY FILTER_ ORDER BY FILTER_ ASC";
	query_table.args = [];
	title = "Monitoraggio SKA Aperte";
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"18436EFB-9CDE-4C9A-A53B-8DB1609F115A"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.drawChart();
}
