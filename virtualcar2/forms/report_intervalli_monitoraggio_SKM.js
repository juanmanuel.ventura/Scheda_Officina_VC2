/**
 * @properties={typeid:35,uuid:"B264757B-16CD-4E09-86F9-EEEC148C1B22",variableType:-4}
 */
var actions = ["load", "export", "report"];

/**
 * @properties={typeid:35,uuid:"C2D7E558-2A37-40AC-AE4C-6011468FC3EE",variableType:-4}
 */
var args = null;

/**
 * @properties={typeid:35,uuid:"284C01F8-EEB1-42EF-BFB2-D2262F58018B",variableType:-4}
 */
var container = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D7BE5910-2658-4DC6-9489-CCD54231D78A"}
 */
var query = "";

/**
 * @properties={typeid:35,uuid:"6C8A5244-BCEE-4998-80B0-2DB65CC87A7A",variableType:-4}
 */
var query_ex = { query: null, args: null, form: null };

/**
 * @properties={typeid:35,uuid:"C9D5016E-36AF-4F49-85B0-BAED85F67016",variableType:-4}
 */
var query_exGraphical = { query: null, args: null, form: null };

/**
 * @properties={typeid:35,uuid:"D2FF53EC-077D-4DA6-BD63-FAE5AA640E93",variableType:-4}
 */
var query_table = { query: null, args: null };

/**
 * @properties={typeid:35,uuid:"71C2BA5D-7851-41E0-BEE6-6C459E35B278",variableType:-4}
 */
var title = null;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"668B5297-AAFF-4038-90BF-3BFDAF26FCA0"}
 */
function onLoad(event) {
	container = forms.report_intervalli_monitoraggio;
	//Aggiungo il listener per il click

	//	JStaffa
	//	elements.chart.addMouseListener(container.rightClickHandler);
	//	elements.chart.addMouseListener(java.awt.event.MouseListener(container.rightClickHandler));
	//FS
	/** @type {java.awt.event.MouseListener} */
	var rightClickHandler = container.rightClickHandler;
	elements.chart.addMouseListener(rightClickHandler);

	//Setto le query e i parametri TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	query = "SELECT SELECTED_,FILTER_,COUNT(*) COUNT_ FROM (SELECT DELTA_ SELECTED_, **FILTER_HERE** FILTER_ FROM (SELECT A.PUNTEGGIO_DEMERITO SEVERITY_,A.TIPO_ANOMALIA TYPE_,A.TIPO_COMPONENTE TCOM_,A.ATTRIBUZIONE DEFREASON_,M.GRUPPO_FUNZIONALE GROUP_,FLOOR(SYSDATE - M.DATA_PROPOSTA_MODIFICA) DELTA_ FROM K8_SEGNALAZIONI_FULL A INNER JOIN K8_SCHEDE_MODIFICA M ON A.ENTE = M.ENTE_INSERIMENTO_ANOMALIA AND A.NUMERO_SCHEDA = M.NR_SCHEDA_ANOMALIA WHERE M.STATO_SCHEDA = 'APERTA' AND (M.MODELLO IN **MODELS_HERE** OR M.VERSIONE IN **VERSIONS_HERE**))) WHERE FILTER_ IS NOT NULL GROUP BY SELECTED_, FILTER_ ORDER BY SELECTED_ ASC, FILTER_ ASC";
	args = [];
	//Estrattore menù
	query_ex.query = "SELECT ID_ FROM (SELECT M.K8_SCHEDE_MODIFICA_ID ID_, TO_NUMBER(A.PUNTEGGIO_DEMERITO) SEVERITY_, A.TIPO_ANOMALIA TYPE_, A.TIPO_COMPONENTE TCOM_,A.ATTRIBUZIONE DEFREASON_, M.GRUPPO_FUNZIONALE GROUP_, FLOOR(SYSDATE - M.DATA_PROPOSTA_MODIFICA) DELTA_ FROM K8_SEGNALAZIONI_FULL A INNER JOIN K8_SCHEDE_MODIFICA M ON A.ENTE = M.ENTE_INSERIMENTO_ANOMALIA AND A.NUMERO_SCHEDA = M.NR_SCHEDA_ANOMALIA WHERE M.STATO_SCHEDA = 'APERTA' AND (M.MODELLO IN **MODELS_HERE** OR M.VERSIONE IN **VERSIONS_HERE**)) WHERE **FILTER_HERE** IS NOT NULL AND DELTA_ >= ? AND DELTA_ <= ?";
	query_ex.args = [];
	query_ex.form = forms.schede_modifica_record.controller.getName();
	//Estrattore diretto dal chart
	query_exGraphical.query = "SELECT ID_ FROM (SELECT M.K8_SCHEDE_MODIFICA_ID ID_, TO_NUMBER(A.PUNTEGGIO_DEMERITO) SEVERITY_, A.TIPO_ANOMALIA TYPE_, A.TIPO_COMPONENTE TCOM_,A.ATTRIBUZIONE DEFREASON_, M.GRUPPO_FUNZIONALE GROUP_, FLOOR(SYSDATE - M.DATA_PROPOSTA_MODIFICA) DELTA_ FROM K8_SEGNALAZIONI_FULL A INNER JOIN K8_SCHEDE_MODIFICA M ON A.ENTE = M.ENTE_INSERIMENTO_ANOMALIA AND A.NUMERO_SCHEDA = M.NR_SCHEDA_ANOMALIA WHERE M.STATO_SCHEDA = 'APERTA' AND (M.MODELLO IN **MODELS_HERE** OR M.VERSIONE IN **VERSIONS_HERE**)) WHERE **FILTER_HERE** = ? AND DELTA_ >= ? AND DELTA_ <= ?";
	query_exGraphical.args = [];
	query_exGraphical.form = forms.schede_modifica_record.controller.getName();
	//Classificatore filtro
	query_table.query = "SELECT FILTER_ FROM K8_SEGNALAZIONI_FULL A INNER JOIN K8_SCHEDE_MODIFICA M ON A.ENTE = M.ENTE_INSERIMENTO_ANOMALIA AND A.NUMERO_SCHEDA = M.NR_SCHEDA_ANOMALIA WHERE M.STATO_SCHEDA = 'APERTA' AND (M.MODELLO IN MODELS_ OR M.VERSIONE IN VERSIONS_) AND FILTER_ IS NOT NULL GROUP BY FILTER_ ORDER BY FILTER_ ASC";
	query_table.args = [];
	title = "Monitoraggio SKM Aperte";
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"B19C159E-999B-48FE-B5F9-2F3C29090FAF"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if (firstShow) {
		container.setupChart();
	}
	container.drawChart();
}
