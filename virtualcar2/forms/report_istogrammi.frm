items:[
{
dataProviderID:"x_filter",
displayType:2,
editable:false,
location:"95,0",
name:"x_filter",
onDataChangeMethodID:"89935764-872E-4605-B995-2D96E29060D6",
size:"130,20",
text:"X Filter",
typeid:4,
uuid:"3871A6B3-120D-4211-8A5A-57CDD385F730",
valuelistID:"35D60997-4860-4481-9B71-E4EF5076CD91"
},
{
horizontalAlignment:4,
labelFor:"y_filter",
location:"255,0",
mediaOptions:14,
size:"60,20",
styleClass:"form",
tabSeq:-1,
text:"Ordinata:",
transparent:true,
typeid:7,
uuid:"410718CB-D1BC-45FE-ADAB-BE811D2E6277"
},
{
dataProviderID:"y_filter",
displayType:2,
editable:false,
location:"325,0",
name:"y_filter",
size:"130,20",
text:"Y Filter",
typeid:4,
uuid:"43C76422-65EA-422B-B386-926CAA54C535",
valuelistID:"D4A478F2-BF86-43FC-B3A7-B68B5F9CE51D"
},
{
horizontalAlignment:4,
labelFor:"def_reason",
location:"20,65",
mediaOptions:14,
size:"80,20",
styleClass:"form",
tabSeq:-1,
text:"Caus. anom.:",
transparent:true,
typeid:7,
uuid:"50EB38EB-1EA1-4333-AAB0-3B8CE31007C5"
},
{
horizontalAlignment:4,
labelFor:"mod_reason",
location:"280,65",
mediaOptions:14,
size:"70,20",
styleClass:"form",
tabSeq:-1,
text:"Caus. mod.:",
transparent:true,
typeid:7,
uuid:"5D25855C-2369-4883-B7E1-2B2CBFE0D484"
},
{
dataProviderID:"sub_type",
location:"560,40",
name:"sub_type",
size:"40,20",
text:"Sub Type",
typeid:4,
uuid:"66D151AF-2EA3-4A7D-BC20-97B4933064F5"
},
{
dataProviderID:"view_type",
displayType:2,
editable:false,
location:"545,0",
name:"view_type",
size:"140,20",
text:"View Type",
typeid:4,
uuid:"6C5780E5-6F63-47EB-A141-435CBF72211A",
valuelistID:"DFBD25A9-26F5-41A9-8F80-5F1A541B24B5"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"history_flag",
displayType:4,
horizontalAlignment:0,
location:"660,40",
name:"history_flag",
onDataChangeMethodID:"2CFC1AD4-D552-4D46-87F0-1081D7585AF7",
size:"20,20",
transparent:true,
typeid:4,
uuid:"71C102C3-FEF4-4ACD-BC71-2C8FCF2E4966"
},
{
horizontalAlignment:4,
labelFor:"history_flag",
location:"610,40",
mediaOptions:14,
size:"50,20",
styleClass:"form",
tabSeq:-1,
text:"Storico:",
transparent:true,
typeid:7,
uuid:"78F3D66C-6AF5-40F0-912C-0A30AC5C691F"
},
{
horizontalAlignment:0,
imageMediaID:"6DD08055-0C00-458E-9AA2-00FC45133893",
location:"190,40",
mediaOptions:2,
name:"local_help",
onActionMethodID:"A34142EF-D527-4810-AC1D-622CCF25B264",
showClick:false,
size:"20,20",
transparent:true,
typeid:7,
uuid:"7D956B1D-BC0E-4BA0-B3A2-2E1C1A96E395"
},
{
horizontalAlignment:4,
labelFor:"type",
location:"280,40",
mediaOptions:14,
size:"70,20",
styleClass:"form",
tabSeq:-1,
text:"Tipo:",
transparent:true,
typeid:7,
uuid:"90F8C9CE-461C-42F6-8F50-9615D07E8B26"
},
{
dataProviderID:"type",
displayType:2,
editable:false,
location:"360,40",
name:"type",
size:"200,20",
text:"Type",
typeid:4,
uuid:"95A0A10F-6D40-4A53-A0ED-FB72675893DB",
valuelistID:"75733E85-07F2-402A-AFC4-763C4B8E8626"
},
{
height:300,
partType:5,
typeid:19,
uuid:"AC5B5400-DE7A-4E63-AE70-693ABF92D01B"
},
{
anchors:11,
fontType:"Verdana,1,9",
horizontalAlignment:0,
location:"0,100",
mediaOptions:14,
size:"700,20",
tabSeq:-1,
text:"ATTENZIONE: per accedere al menù contestuale, utilizzare il tasto destro del mouse direttamente sulla zona del grafico interessata",
transparent:true,
typeid:7,
uuid:"BBF3D9C3-615A-42FC-B55F-850429CA0B9A"
},
{
dataProviderID:"mod_reason",
displayType:2,
editable:false,
location:"360,65",
name:"mod_reason",
size:"320,20",
text:"Mod Reason",
typeid:4,
uuid:"BDE2BDD9-89E9-49B1-9763-A63FBCC2A320",
valuelistID:"42EB7CA8-CAFA-4853-BB83-0DEC061723BC"
},
{
horizontalAlignment:4,
imageMediaID:"519C04F3-2F1C-4374-B807-64B55FED53E9",
labelFor:"x_filter",
location:"15,0",
mediaOptions:2,
onActionMethodID:"D4181A7E-8B3B-4614-BB75-AF5797D74B2D",
showClick:false,
size:"70,20",
styleClass:"form",
text:"Ascissa:",
transparent:true,
typeid:7,
uuid:"C50A59AC-4010-4BED-A7F7-487922BFFD9E"
},
{
horizontalAlignment:4,
labelFor:"status",
location:"20,40",
mediaOptions:14,
size:"80,20",
styleClass:"form",
tabSeq:-1,
text:"Stato:",
transparent:true,
typeid:7,
uuid:"C909353C-37F9-4AFE-9A20-1BE33AD2CFE4"
},
{
anchors:15,
items:[
{
containsFormID:"9B9CDC80-8F3D-4872-A68D-A809835271F6",
location:"270,240",
text:"Lettere di Modifica",
typeid:15,
uuid:"4B3CF372-BDC2-406B-AA7D-5CDD81E94810"
},
{
containsFormID:"C60E7840-1C7F-41AC-8D59-8F375C5DBCFE",
location:"80,170",
text:"Segnalazioni anomalia",
typeid:15,
uuid:"66E54497-66AA-49A3-BE75-E4FB4EEB6FB8"
},
{
containsFormID:"6A457226-0DF7-4728-A7BD-2B2B4DA00C9D",
location:"140,190",
text:"Schede anomalia",
typeid:15,
uuid:"AD4C6CBA-B528-417E-A94D-D7799F4A3A1E"
},
{
containsFormID:"FE387A22-9A88-4808-9FE8-25D6FCC4B5DD",
location:"200,210",
text:"Schede modifica",
typeid:15,
uuid:"C246E5F4-BB8C-4496-A5B0-32AD4FA2655A"
}
],
location:"0,120",
name:"cumulateTab",
printable:false,
size:"700,180",
transparent:true,
typeid:16,
uuid:"DC5D5ADB-8D5C-458E-9275-28314C2CE80D"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"multipleFilter",
displayType:4,
horizontalAlignment:0,
location:"225,0",
name:"multipleFilter",
onDataChangeMethodID:"B8421158-DA45-4DEA-98DB-9A6DAF3972DA",
size:"20,20",
toolTipText:"Raggruppamento multiparametro",
transparent:true,
typeid:4,
uuid:"DE26EC23-4AC8-456C-9388-1A39FD1AFD39"
},
{
dataProviderID:"def_reason",
displayType:2,
editable:false,
location:"110,65",
name:"def_reason",
size:"160,20",
text:"Def Reason",
typeid:4,
uuid:"EE5312F1-09FA-4CB2-A011-F18A675DCDBC",
valuelistID:"44BD220C-5A3E-4F3B-AF7C-A031012AE4C7"
},
{
borderType:"TitledBorder,Filtri,4,0,DialogInput.plain,1,12,#333333",
formIndex:-1,
lineSize:1,
location:"10,20",
size:"680,80",
transparent:true,
typeid:21,
uuid:"F0B22B72-6B1A-41E1-986A-9659BBFA553C"
},
{
dataProviderID:"status",
displayType:2,
editable:false,
location:"110,40",
name:"status",
size:"80,20",
text:"Status",
transparent:true,
typeid:4,
uuid:"F4AEE0EB-93EA-47FC-85C4-E57EF74CDD11",
valuelistID:"F370A13A-9C87-4F72-8940-2F89EE6F2635"
},
{
horizontalAlignment:4,
labelFor:"view_type",
location:"465,0",
mediaOptions:14,
size:"70,20",
styleClass:"form",
tabSeq:-1,
text:"Visualizza:",
transparent:true,
typeid:7,
uuid:"F5FAD471-5572-4B4F-99BC-BE84B52D730F"
}
],
name:"report_istogrammi",
navigatorID:"-1",
onLoadMethodID:"8F00E763-E05E-4EBF-848C-7438D456303F",
paperPrintScale:100,
showInMenu:true,
size:"700,300",
styleName:"keeneight",
typeid:3,
uuid:"F012D07A-7DA6-49FE-9FB6-5029B42438BA"