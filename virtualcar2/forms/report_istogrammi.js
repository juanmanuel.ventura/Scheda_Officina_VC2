/**
 * @properties={typeid:35,uuid:"B903C18B-D4D3-48E1-B795-366136378164",variableType:-4}
 */
var config = {
	attributes: { type: "stack", id: "getVList" },
	bgc1: "#cccccc", bgc2: "#ffffff",
	grid: null,
	fntx: "Verdana,0,10", fnty: "Verdana,0,10",
	styles: { }
};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"04D1EDC4-3831-408E-9F14-A64AD441FC7C"}
 */
var def_reason = "";

/**
 * @properties={typeid:35,uuid:"4BF981DA-9D9C-44B7-AB35-A1EB7259A9A0",variableType:-4}
 */
var filterNames = ["status", "type", "sub_type", "mod_reason", "def_reason"];

/**
 * @properties={typeid:35,uuid:"409B7147-B0A4-451C-93D3-F247F1DDA820",variableType:-4}
 */
var graphicalActions = {
	"load": { human: "Visualizza", icon: "media:///detail16.png" },
	"export": { human: "Export", icon: "media:///excel16.png" },
	"report": { human: "Full report", icon: "media:///report16.png" }
};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"46FA43EB-0EA9-4ECC-9121-2E5AB000FB20"}
 */
var history_flag = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"66657833-D0CE-46C2-87CD-9448819B8285"}
 */
var localHelpMessage = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"20F6F709-B627-4C82-94BC-D4AAF4BC99CE"}
 */
var mod_reason = "";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"A105FFC8-8D91-41F8-9449-C22B1433B7D4",variableType:4}
 */
var multipleFilter = 0;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"30D2CDAA-417F-4F9D-A0AE-79D84F4E037F"}
 */
var noData = "!Non impostato!";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"6DC4E9B5-62B5-430B-91C5-1FED97DE8FC7"}
 */
var selected = "";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"7C319760-AA79-4023-8A5D-5BB85A693DB3",variableType:4}
 */
var selected_level = 0;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"7282619C-4543-49AA-930D-1D3A44F0063B"}
 */
var status = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"947D16D6-8A77-453F-802F-C0B75975B4F8"}
 */
var sub_type = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"52C2F914-73AF-4C70-A59B-9628767C5D8C"}
 */
var type = "";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"E3F81DA9-E2AF-4E2D-A98D-B3E2BECDCFE7",variableType:4}
 */
var view_type = 1;

/**
 * @properties={typeid:35,uuid:"2C0DF0F6-B9C9-444E-AC3A-DC4988D14A42",variableType:-4}
 */
var view_types = {
	0: { query: "sum(FACTOR_)" },
	1: { query: "count(*)" }
};

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"B996B614-D8D4-49EA-80D7-E57D05B26B43",variableType:4}
 */
var x_filter = 0;

/**
 * @properties={typeid:35,uuid:"7007D26F-7A34-4F8D-8492-876BE83135AB",variableType:-4}
 */
var x_filters = {
	0: { query: "STATUS_", angle: 360, vlist: null },
	1: { query: "GROUP_", angle: 345, vlist: "Gruppi" },
	2: { query: "FULLGROUP_", angle: 360, vlist: null },
	3: { query: "DEPARTMENT_", angle: 335, vlist: null },
	4: { query: "LEADER_", angle: 310, vlist: null },
	5: { query: "REPORTER_", angle: 300, vlist: null },
	6: { query: "MANAGEMENT_", angle: 345, vlist: null },
	7: { query: "RESPONSIBLE_", angle: 335, vlist: null },
	8: { query: "MOD_REASON_", angle: 335, vlist: "gestione_anomalie$causale_modifica" },
	9: { query: "HISTORY_", angle: 345, vlist: "static$report_storico" },
	10: { query: "APPROVAL_", angle: 360, vlist: null },
	11: { query: "DEF_REASON_", angle: 335, vlist: "gestione_anomalie$causale_anomalia" },
	12: { query: "TCOM_", angle: 360, vlist: "gestione_anomalie$tipo_componente" }
};

/**
 /** @type {Array}
 * @properties={typeid:35,uuid:"84204176-6CA0-496C-9EFE-BDF65D1911F7",variableType:-4}
 */
var x_filtersList = [0];

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"DC68CC7E-2ACA-44DC-AD6A-8E194FA9EF9D",variableType:4}
 */
var y_filter = 0;

/**
 * @properties={typeid:35,uuid:"D0BFCD08-07E9-4757-A3F1-02BB0FDA78A1",variableType:-4}
 */
var y_filters = {
	0: { query: "SEVERITY_", type: "number", vlist: null },
	1: { query: "TYPE_", type: "string", vlist: "gestione_anomalie$tipo_anomalia" },
	2: { query: "DEF_REASON_", type: "string", vlist: "gestione_anomalie$causale_anomalia" },
	3: { query: "TCOM_", type: "string", vlist: "gestione_anomalie$tipo_componente" }
};

/**
 * @properties={typeid:24,uuid:"B3F9E291-E915-4D57-A54C-A1289A5E810B"}
 */
function getVList() {
	var _list = new Array();
	for (var _yf in y_filters) {
		if (y_filters[_yf]["vlist"]) {
			var _vlist = application.getValueListArray(y_filters[_yf]["vlist"]);
			for (var _vl in _vlist) {
				if (_vl && application.getValueListDisplayValue(y_filters[_yf]["vlist"], _vlist[_vl])) {
					var _dv = application.getValueListDisplayValue(y_filters[_yf]["vlist"], _vlist[_vl]);
					_list.push(application.getValueListDisplayValue("static$report_y", _yf) + " - " + _dv.substr(0, 1).toUpperCase() + _dv.substr(1).toLowerCase());
				}
			}
		} else {
			_list.push(application.getValueListDisplayValue("static$report_y", _yf) + " - <inserire_valore>");
		}
	}
	return _list.sort();
}

/**
 * @properties={typeid:24,uuid:"615E3D60-734B-4256-A235-E6BBF56F5B61"}
 */
function nfx_isProgram() {
	return false;
}

/**
 * @properties={typeid:24,uuid:"1FD284DB-2E73-4D9F-82D2-CEE77EC92EDC"}
 */
function packXValue() {
	var query = new Array();
	for (var i = 0; i < x_filtersList.length; i++) {
		query.push(x_filters[x_filtersList[i]].query);
	}
	return query.join("||'€€€'||");
}

/**
 * @properties={typeid:24,uuid:"37EE46C5-CDF7-4D21-A43C-EDAB88B5033B"}
 */
function getData(p, v) {
	if (!forms[selected].query || !forms[selected].args) throw "Content query and/or args not detected";
	//FS
	/** @type {String} */
	var query = forms[selected].query;
	/** @type {Array} */
	var args = forms[selected].args;
	//Inizio parte di filtraggio
	var filters = {
		status: status,
		type: type,
		sub_type: sub_type,
		mod_reason: mod_reason,
		def_reason: def_reason
	};
	var adding = forms[selected].addingParts;
	var replaceFinal = forms[selected].replacePart;
	var argsFinal = new Array();
	for (var i = 0; i < filterNames.length; i++) {
		if (filters[filterNames[i]]) {
			replaceFinal += " " + adding[filterNames[i]];
			argsFinal.push(filters[filterNames[i]]);
		}
	}
	query = query.replace(forms[selected].replacePart, replaceFinal);
	//FS
	args = args.concat(argsFinal);
	//Fine parte di filtraggio
	query = (view_type !== null) ? query.replace("**VIEW_HERE**", view_types[view_type].query) : query;
	query = query.replace("**X_HERE**", packXValue());
	query = (y_filter !== null) ? query.replace("**Y_HERE**", y_filters[y_filter].query) : query;
	query = query.replace("**MODELS_HERE**", p);
	query = query.replace("**VERSIONS_HERE**", v);
	//application.output(query + "\n" + args);
	var ds = databaseManager.getDataSetByQuery('ferrari', query, args, -1);
	//FS
	var x = getUnique(ds.getColumnAsArray(1), null);
	var y = (y_filter !== null) ? getUnique(ds.getColumnAsArray(2), y_filters[y_filter].type) : getUnique(ds.getColumnAsArray(2), null);
	var offsets = ds.getColumnAsArray(1);
	filters = ds.getColumnAsArray(2);
	var counts = ds.getColumnAsArray(3);
	var data = new Object();
	var found = false;
	for (i = 0; i < y.length; i++) {
		data[i] = new Array();
		for (var j = 0; j < x.length; j++) {
			for (var z = 0; z < offsets.length; z++) {
				if (offsets[z] == x[j] && filters[z] == y[i]) {
					data[i].push(counts[z]);
					found = true;
				}
			}
			if (!found) {
				data[i].push(0);
			}
			found = false;
		}
	}
	//Calcolo del massimo
	var sum = new Object();
	for (var i2 = 0; i2 < offsets.length; i2++) {
		if (!sum[offsets[i2]]) {
			sum[offsets[i2]] = counts[i2];
		} else {
			sum[offsets[i2]] += counts[i2];
		}
	}
	var max = 0;
	var total = 0;
	var totals = new Array();
	for (var i3 in sum) {
		max = Math.max(max, sum[i3]);
		total += sum[i3];
		totals.push("Totale: " + sum[i3]);
	}
	return (ds.getMaxRowIndex() > 0) ? { data: data, x: x, y: y, max: max, sum: totals, total: total } : null;
}

/**
 * @properties={typeid:24,uuid:"9B81C765-0FAA-4720-9A00-198765D23BA7"}
 */
function setupChart() {
	//FS 
	config = {
		attributes: { type: "stack", id: "getVList" },
		bgc1: "#cccccc", bgc2: "#ffffff",
		grid: null,
		fntx: "Verdana,0,10", fnty: "Verdana,0,10",
		styles: { }
	};
	/** @type {java.awt.chart} */
	var chart = forms[selected].elements.chart;
	if (!chart) throw "Selected form must have a chart element called \"chart\"";
	chart.setChartBackground(globals.utils_getJColor(config.bgc1));
	chart.setChartBackground2(globals.utils_getJColor(config.bgc2));
	chart.setZoomOn(true);
	chart.setBarType(1);
	chart.setBarLabelsOn(true);
	chart.setValueLabelStyle(0);
	chart.setValueLinesOn(true);
	chart.setLegendOn(true);
	chart.setLegendPosition(3);
	chart.setLegendBoxSizeAsFont(true);
	chart.setFont("barLabelFont", globals.utils_getJFont(config.fntx));
	chart.setFont("rangeLabelFont", globals.utils_getJFont(config.fnty));
}

/**
 * @properties={typeid:24,uuid:"F2CED340-7987-4941-8182-ACA282F11CB5"}
 */
function drawChart() {
	//FS
	/** @type {java.awt.chart} */	
	var chart = forms[selected].elements.chart;
	if (!chart) throw "Selected form must have a chart element called \"chart\"";
	var projects = forms.report_anomalie_container.projects;
	var versions = forms.report_anomalie_container.versions;
	if (projects) {
		var p = [];
		var v = [];
		for (var _p in versions) {
			if (typeof versions[_p] == "string") {
				p.push(_p);
			} else {
				v = v.concat(versions[_p]);
			}
		}
		var data_full = getData("('" + p.join("','") + "')", "('" + v.join("','") + "')");
		if (data_full) {
			var data = data_full["data"];
			chart.setSampleCount(data_full["x"].length);
			chart.setBarLabels(getBarLabels(data_full["x"]));
			chart.setLabelAngle("barLabelAngle", getBarLabelAngle());
			var filters = data_full["y"];
			chart.setSeriesCount(filters.length);
			var legend = new Array();
			for (var i = 0; i < filters.length; i++) {
				chart.setSampleValues(i, data[i]);
				var _style = config.styles[application.getValueListDisplayValue("static$report_y", y_filter) + " - " + getLegendLabel(filters[i])];
				if (_style) {
					//FS
				//dep	chart.setSampleColor(i, globals.utils_getJColor(_style[0]));
					chart.setSampleColor(i, globals.utils_getJColor(_style[0]));

				} else {
				//dep	chart.setSampleColor(i, globals.utils_getJColor(forms.report_anomalie_container.colors[i]));
					chart.setSampleColor(i, globals.utils_getJColor(forms.report_anomalie_container.colors[i]));

				}
				legend.push(getLegendLabel(filters[i]));
			}
			chart.setValueLabelsOn(true);
			chart.setLegendLabels(legend);
			chart.setRange(0, data_full["max"] * 1.1);

			if (forms[selected].title) {
				chart.setTitleOn(true);
				chart.setTitle(forms[selected].title + " " + projects.replace(/\n/g, ",") + " - " + utils.dateFormat(new Date(), forms.report_anomalie_container.chartTitleDateFormat) + " - Totale visualizzato: " + data_full["total"]);
			}

			chart.visible = true;
		} else {
			chart.visible = false;
			plugins.dialogs.showWarningDialog("Nessun dato", "Non è presente alcun dato relativo ai criteri selezionati", "Ok");
		}
	} else {
		chart.visible = false;
	}
}

/**@param {Array} array
 * @param {String} types
 * @return {Array}
 * @properties={typeid:24,uuid:"412019A5-FDDF-4CFC-8EFA-4016F8845C2B"}
 */
function getUnique(array, types) {
	var unique = new Array();
	for (var i = 0; i < array.length; i++) {
		if (unique.indexOf(array[i]) == -1) {
			unique.push(array[i]);
		}
	}
	return (types == "number") ? unique.sort(globals.utils_sortNumbers) : unique.sort();
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 *
 * @properties={typeid:24,uuid:"89935764-872E-4605-B995-2D96E29060D6"}
 */
function checkFilter(oldValue, newValue, event) {
	//FS
	/** @type {Number} */
	var n = (newValue) ? newValue : x_filter;
	var o = (oldValue) ? oldValue : 0;
	var e = (event) ? event.getElementName() : "x_filter";
	//FS
	/** @type {String} */
	var af = forms[selected].accepted_filters;
	if (af.indexOf(n) == -1) {
		forms[controller.getName()][e] = o;
	} else if (x_filtersList.indexOf(n) != -1) {
		plugins.dialogs.showInfoDialog("Info", "Il filtro \"" + application.getValueListDisplayValue("static$report_x", n) + "\" è già impostato", "Ok");
	} else if (multipleFilter) {
		x_filtersList.push(n);
	} else {
		x_filtersList = [n];
	}

}

/**
 * @properties={typeid:24,uuid:"B9FA296D-5BF7-49FF-88EE-7C561C8B8B1D"}
 */
function checkFiltersOnShow() {
	//FS
	/** @type {String} */
	var af = forms[selected].accepted_filters;
	var localAcceptedFilters = new Array();
	for (var i = 0; i < x_filtersList.length; i++) {
		if (af.indexOf(x_filtersList[i]) != -1) {
			localAcceptedFilters.push(x_filtersList[i]);
		}
	}
	x_filtersList = (localAcceptedFilters.length) ? localAcceptedFilters : [0];
	x_filter = x_filtersList[0];
}

/**
 * @properties={typeid:24,uuid:"E6530C3E-5417-496E-8E65-7C97869F3C97"}
 */
function setupFilters() {
	var f = (forms[selected].filters) ? forms[selected].filters : new Array();
	var filters = ["mod_reason", "history_flag"];
	for (var i = 0; i < filters.length; i++) {
		if (f.indexOf(filters[i]) != -1) {
			elements[filters[i]].visible = true;
		} else {
			elements[filters[i]].visible = false;
			forms[controller.getName()][filters[i]] = "";
		}
	}
	setupHistoryFilters();
}

/**
 * @properties={typeid:24,uuid:"2CFC1AD4-D552-4D46-87F0-1081D7585AF7"}
 */
function setupHistoryFilters() {
	var filters = ["y_filter", "view_type", "type", "sub_type", "def_reason"];
	var defaults = [0, 1, "", "", ""];
	if (elements.history_flag.visible && !utils.stringToNumber(history_flag)) {
		for (var i = 0; i < filters.length; i++) {
			elements[filters[i]].enabled = false;
			forms[controller.getName()][filters[i]] = null;
		}
	} else {
		for (var i2 = 0; i2 < filters.length; i2++) {
			elements[filters[i2]].enabled = true;
			forms[controller.getName()][filters[i2]] = defaults[i2];
		}
	}
	var setupInfo = forms[selected].setupInfo;
	//TODO  FS:verificare che setupInfo esista in qualche form!!!
	if (setupInfo && typeof setupInfo == "function") setupInfo(utils.stringToNumber(history_flag));
	//forms.report_istogrammi_SKM.setupInfo(utils.stringToNumber(history_flag));
	checkFiltersOnShow();
}

/**
 * @properties={typeid:24,uuid:"76CF8FF9-FE67-41D2-B9AF-E425C3185F6F"}
 */
function getLegendLabel(value) {
	if (!value) {
		return noData;
	}
	if (y_filter && y_filters[y_filter].vlist) {
		var str = application.getValueListDisplayValue(y_filters[y_filter].vlist, value);
		return (str) ? str.substr(0, 1).toUpperCase() + str.substr(1).toLowerCase() : value;
	} else {
		return value;
	}
}

/**
 * @properties={typeid:24,uuid:"D01EAA2B-9CBE-439E-834C-EEE8A43E363B"}
 */
function getBarLabels(values) {
	var str = null;
	var value = null;
	var returnValues = new Array();
	for (var i = 0; i < values.length; i++) {
		try {
			value = values[i].split("€€€");
		} catch (e) {
			value = [""];
		}
		var returnValue = new Array();
		for (var j = 0; j < x_filtersList.length; j++) {
			if (!value[j]) {
				str = noData;
			} else {
				if (x_filtersList[j] && x_filters[x_filtersList[j]].vlist) {
					str = application.getValueListDisplayValue(x_filters[x_filtersList[j]].vlist, value[j]);
					str = (str) ? str.substr(0, 1).toUpperCase() + str.substr(1).toLowerCase() : value[j];
				} else {
					str = value[j];
				}
			}
			returnValue.push(str);
		}
		returnValues.push(returnValue.join(' | '));
	}
	return returnValues;
}

/**
 * @properties={typeid:24,uuid:"A76D62CC-8011-4A89-8AA3-A4051AE59608"}
 */
function getBarLabelAngle() {
	var angle = 360;
	for (var i = 0; i < x_filtersList.length; i++) {
		angle = Math.min(angle, x_filters[x_filtersList[i]].angle);
	}
	return angle;
}

/**
 * @properties={typeid:24,uuid:"2E72833B-D741-4AA0-B94F-5B70C0BB9A86"}
 */
function getValueFromList(value) {
	var splittedValues = value.split(' | ');
	var returnValues = new Array();
	for (var i = 0; i < x_filtersList.length; i++) {
		if (splittedValues[i] == noData) {
			returnValues.push(null);
		} else {
			if (x_filters[x_filtersList[i]].vlist) {
				var vl = application.getValueListItems(x_filters[x_filtersList[i]].vlist);
				var values = vl.getColumnAsArray(1);
				var reals = vl.getColumnAsArray(2);
				var returnValue = (reals[values.indexOf(splittedValues[i])]) ? reals[values.indexOf(splittedValues[i])] : splittedValues[i];
				returnValues.push(returnValue);
			} else {
				returnValues.push(splittedValues[i]);
			}
		}
	}
	return (returnValues.join("€€€")) ? returnValues.join("€€€") : null;
}

/**
 * @properties={typeid:24,uuid:"7A68BA39-BFEE-4A29-9901-C06AE27DDFBB"}
 */
function exportData(project, value) {
	/** @type {String} */
	var query = forms[selected].query_ex.query;
	//FS
	/** @type {Array<String>} */
	var args = forms[selected].query_ex.args;
	var form = forms[selected].query_ex.form;
	//Inizio parte di filtraggio
	var filters = {
		status: status,
		type: type,
		sub_type: sub_type,
		mod_reason: mod_reason,
		def_reason: def_reason
	};
	var adding = forms[selected].addingParts;
	var replaceFinal = forms[selected].replacePart;
	var argsFinal = new Array();
	for (var i = 0; i < filterNames.length; i++) {
		if (filters[filterNames[i]]) {
			replaceFinal += " " + adding[filterNames[i]];
			argsFinal.push(filters[filterNames[i]]);
		}
	}
	query = query.replace(forms[selected].replacePart, replaceFinal);
	args = args.concat(argsFinal);
	//Fine parte di filtraggio
	query = query.replace(/\*\*X_HERE\*\*/g, packXValue());
	query = (y_filter !== null) ? query.replace(/\*\*Y_HERE\*\*/g, y_filters[y_filter].query) : query;
	var _object = forms.report_anomalie_container.versions;
	var p = null;
	var v = null;
	if (typeof _object[project] == "string") {
		p = "('" + project + "')";
		v = "('')";
	} else {
		p = "('')";
		v = "('" + _object[project].join("','") + "')"
	}
	query = query.replace(/\*\*MODELS_HERE\*\*/g, p).replace(/\*\*VERSIONS_HERE\*\*/g, v);
	args = args.concat([getValueFromList(forms[selected].elements.chart.getBarLabels()[value])]);
	//application.output(query + "\n" + args);
	forms[form].controller.loadRecords(query, args);
	globals.nfx_launchExport(form);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"D4181A7E-8B3B-4614-BB75-AF5797D74B2D"}
 */
function showList(event) {
	var items = new Array();
	for (var i = 0; i < x_filtersList.length; i++) {
		//FS
		var menu = plugins.window.createPopupMenu();
		//dep		var item = plugins.popupmenu.createMenuItem(application.getValueListDisplayValue("static$report_x",x_filtersList[i]),deleteFromList);
		//dep		item.setMethodArguments([i]);
		var item = menu.addMenuItem(application.getValueListDisplayValue("static$report_x", x_filtersList[i]), deleteFromList);
		item.methodArguments = [i];
		items.push(item);
	}
	//dep	plugins.popupmenu.showPopupMenu(elements.x_filter,items);
	menu.show(elements.x_filter);
}

/**
 * @properties={typeid:24,uuid:"5BC2401A-9180-49B2-A0E0-A6F33F9BE4B1"}
 */
function deleteFromList(index) {
	if (x_filtersList.length > 1) {
		x_filtersList.splice(index, 1);
	}
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 *
 * @properties={typeid:24,uuid:"B8421158-DA45-4DEA-98DB-9A6DAF3972DA"}
 */
function setMultiFilter(oldValue, newValue, event) {
	if (!newValue) {
		x_filtersList = [x_filter];
	}
}

/**@param {java.awt.event.MouseEvent} event
 *@properties={typeid:24,uuid:"15E70B69-7CAB-4F24-BE7D-6EF187E8D3D0"}
 */
function rightClickHandler(event) {
	if (event.getID() == java.awt.event.MouseEvent.MOUSE_CLICKED && event.getButton() == java.awt.event.MouseEvent.BUTTON3) {
		/** @type {java.awt.chart} */
		var chart = forms[selected].elements.chart;
		var serie = chart.getLastSelectedSeries();
		var sample = chart.getLastSelectedSample();
		/** @type {plugins.window.Popup} */		
		var menu = plugins.window.createPopupMenu();
		//var menuItems = new Array();
		if (sample != -1 && serie != -1) {
			//FS
			/** @type {Array<String>} */
			var actions = forms[selected].actions;
			/** @type {plugins.window.MenuItem} */			
			var item = null;
			for (var i = 0; i < actions.length; i++) {
				//FS
				//dep	item = plugins.popupmenu.createMenuItem(graphicalActions[actions[i]].human, graphicalExportData, graphicalActions[actions[i]].icon);
				//dep	item.setMethodArguments([serie, sample, actions[i]]);
				item = menu.addMenuItem(graphicalActions[actions[i]].human, graphicalExportData, graphicalActions[actions[i]].icon);
				item.methodArguments = [serie, sample, actions[i]];
				//dep menuItems.push(item);
			}
		} else {
			//dep	menuItems.push(plugins.popupmenu.createMenuItem("Nessun elemento selezionato...", null, "media:///cancel16.png"));
			menu.addMenuItem("Nessun elemento selezionato...", null, "media:///cancel16.png");
		}

		//TODO: bisogna inserire in NFX un metodo che restituisca la finestra dell'applicazione
		//FS
		var labelASComponent = java.awt.Component(forms.nfx_interfaccia_pannello_base.elements.keyLabel);
		//var window = Packages.javax.swing.SwingUtilities.getWindowAncestor(forms.nfx_interfaccia_pannello_base.elements.keyLabel);
		var window = Packages.javax.swing.SwingUtilities.getWindowAncestor(labelASComponent);
		var pointer = java.awt.MouseInfo.getPointerInfo().getLocation();

		//dep		plugins.popupmenu.showPopupMenu(pointer.getX() - window.getX(), pointer.getY() - window.getY(), menuItems);
		menu.show(pointer.getX() - window.getX(), pointer.getY() - window.getY());
	}
}

/**@param {Number} index
 * @param {Number} parentIndex
 * @param {Boolean} isSelected
 * @param {String} parentText
 * @param {String} menuText
 * @param {Number} serie
 * @param {Number} sample
 * @param {String} action
 * @properties={typeid:24,uuid:"14818C87-AC4F-4CF7-9A98-1FE159109ECF"}
 */
function graphicalExportData(index,parentIndex,isSelected,parentText,menuText,serie, sample, action) {

	//FS
	/** @type {String} */
	var query = forms[selected].query_exGraphical.query;
	/** @type {Array} */
	var args = forms[selected].query_exGraphical.args;
	var form = forms[selected].query_exGraphical.form;
	//Inizio parte di filtraggio
	var filters = {
		status: status,
		type: type,
		sub_type: sub_type,
		mod_reason: mod_reason,
		def_reason: def_reason
	};
	var adding = forms[selected].addingParts;
	var replaceFinal = forms[selected].replacePart;
	var argsFinal = new Array();
	for (var i = 0; i < filterNames.length; i++) {
		if (filters[filterNames[i]]) {
			replaceFinal += " " + adding[filterNames[i]];
			argsFinal.push(filters[filterNames[i]]);
		}
	}
	query = query.replace(forms[selected].replacePart, replaceFinal);
	args = args.concat(argsFinal);
	//Fine parte di filtraggio
	var _v = forms.report_anomalie_container.versions;
	var p = [];
	var v = [];
	for (var _p in _v) {
		if (typeof _v[_p] == "string") {
			p.push(_p);
		} else {
			v = v.concat(_v[_p]);
		}
	}
	query = query.replace(/\*\*MODELS_HERE\*\*/g, "('" + p.join("','") + "')");
	query = query.replace(/\*\*VERSIONS_HERE\*\*/g, "('" + v.join("','") + "')");
	query = query.replace(/\*\*X_HERE\*\*/g, packXValue());
	args = args.concat([getValueFromList(forms[selected].elements.chart.getBarLabels()[sample])]);
	if (y_filter !== null) {
		query = query.replace(/\*\*Y_HERE\*\*/g, y_filters[y_filter].query);
		args = args.concat([getSelectedFilterRealValue(serie)]);
	}
	//application.output(query + "\n" + args);
	forms[form].controller.loadRecords(query, args);
	if (action == "load") {
		//globals.alternate=true;
		//globals.toGoTo=forms[form].controller.getName();
		globals.nfx_goTo(forms[form].controller.getName());
		//globals.alternate=false;
	}
	if (action == "export") {
		globals.nfx_launchExport(forms[form].controller.getName());
	}
	if (action == "report") {
		forms[form].report();
	}
}

/**
 * @properties={typeid:24,uuid:"771104E3-C96C-4B69-A4CC-8DF574408E62"}
 */
function getSelectedFilterRealValue(serie) {
	//FS
	/** @type {java.awt.chart} */
	var chart = forms[selected].elements.chart;
	if (chart.getLegendLabels()[serie] == noData) {
		return null;
	}
	var list = y_filters[y_filter].vlist;
	var filterValue = null;
	if (list) {
		var ds = application.getValueListItems(list);
		var real = ds.getColumnAsArray(2);
		var diplay = ds.getColumnAsArray(1);
		var element = chart.getLegendLabels()[serie];
		var index = forms.report_anomalie_container.indexOfCaseInsensitive(diplay, element);
		filterValue = (index != -1) ? real[index] : element;
	} else {
		filterValue = chart.getLegendLabels()[serie];
	}
	return filterValue;
}

/**
 * @properties={typeid:24,uuid:"A34142EF-D527-4810-AC1D-622CCF25B264"}
 */
function showLocalHelpMessage() {
	if (localHelpMessage) {
		plugins.dialogs.showInfoDialog("Help", localHelpMessage, "Ok");
	}
}

/**
 * @properties={typeid:24,uuid:"C97C9F67-B956-4595-9F2E-497D5852A337"}
 */
function setLocalHelpMessage(_m) {
	if (_m) {
		localHelpMessage = _m;
		elements.local_help.visible = true;
	} else {
		elements.local_help.visible = false;
	}
}
