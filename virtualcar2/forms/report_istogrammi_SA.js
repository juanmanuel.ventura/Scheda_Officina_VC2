/**
 * @properties={typeid:35,uuid:"4CB4AF0A-A768-498A-B3C3-E37AC02DA627",variableType:-4}
 */
var accepted_filters = [0,1,2,3,4,5,11,12];

/**
 * @properties={typeid:35,uuid:"60C0FEE2-45C0-4CE6-BA89-2B10CFAF87FC",variableType:-4}
 */
var actions = ["load","export","report"];

/**
 * @properties={typeid:35,uuid:"E9B462B4-12B7-4A79-A651-6591E68FBCC0",variableType:-4}
 */
var addingParts = {status		: "AND S.STATO_SEGNALAZIONE = ?",
                   type			: "AND S.TIPO_ANOMALIA = ?",
                   subtype		: "AND S.SOTTOTIPO_ANOMALIA = ?",
                   def_reason	: "AND S.ATTRIBUZIONE = ?"};

/**
 * @properties={typeid:35,uuid:"839B0334-84E8-40EB-941E-842F509C3CA0",variableType:-4}
 */
var args = null;

/**
 * @properties={typeid:35,uuid:"BED63D97-29D2-452F-A2E7-C180D9E3AD98",variableType:-4}
 */
var container = null;

/**
 * @properties={typeid:35,uuid:"9974F349-9036-4308-B6C3-211B448D076B",variableType:-4}
 */
var filters = new Array();

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"52E7322D-5987-4A17-8596-AD4632BE04B8"}
 */
var query = "";

/**
 * @properties={typeid:35,uuid:"285211C4-CC9E-4916-9BC2-F2C0B3F89A2E",variableType:-4}
 */
var query_ex = {query: null, args: null, form: null};

/**
 * @properties={typeid:35,uuid:"CECF742E-C3ED-4CDF-9500-809E1147480D",variableType:-4}
 */
var query_exGraphical = {query: null, args: null, form: null};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"6BADCB07-1ABA-480A-B341-A2262D8B128C"}
 */
var replacePart = "(S.CODICE_MODELLO IN **MODELS_HERE** OR S.CODICE_VERSIONE IN **VERSIONS_HERE**)";

/**
 * @properties={typeid:35,uuid:"0E71A525-F368-4B3E-AE81-443BEAAB32C5",variableType:-4}
 */
var title = null;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"5D636D6E-CF4B-4988-AA37-F05F2E17BDAC"}
 */
function onLoad(event) {
	container = forms.report_istogrammi;
	//Aggiungo il listener per il click
	
//	JStaffa
//	elements.chart.addMouseListener(container.rightClickHandler);
//	elements.chart.addMouseListener(java.awt.event.MouseListener(container.rightClickHandler));
	/** @type {java.awt.event.MouseListener} */
	var rightClickHandler=container.rightClickHandler;
	elements.chart.addMouseListener(rightClickHandler);
	//Setto le query e i parametri TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	query = "SELECT FILTERX_, FILTERY_, **VIEW_HERE** COUNT_ FROM (SELECT **X_HERE** FILTERX_, **Y_HERE** FILTERY_, FACTOR_ FROM (SELECT S.STATO_SEGNALAZIONE STATUS_, S.GRUPPO_FUNZIONALE GROUP_, S.GRUPPO_FUNZIONALE || TRIM(S.SOTTOGRUPPO_FUNZIONALE) FULLGROUP_, S.ENTE_SEGNALAZIONE_ANOMALIA DEPARTMENT_, TRIM(S.LEADER) LEADER_, TRIM(S.SEGNALATO_DA) REPORTER_, TO_NUMBER(S.PUNTEGGIO_DEMERITO) SEVERITY_, S.TIPO_ANOMALIA TYPE_, S.TIPO_COMPONENTE TCOM_, S.ATTRIBUZIONE DEF_REASON_, NVL(TO_NUMBER(S.PUNTEGGIO_DEMERITO),0) FACTOR_ FROM K8_SEGNALAZIONI_FULL S WHERE (S.CODICE_MODELLO IN **MODELS_HERE** OR S.CODICE_VERSIONE IN **VERSIONS_HERE**))) GROUP BY FILTERX_, FILTERY_ ORDER BY FILTERX_, FILTERY_";
	args = [];
	//Estrattore
	query_ex.query = "SELECT ID_ FROM (SELECT S.K8_ANOMALIE_ID ID_, S.STATO_SEGNALAZIONE STATUS_, S.GRUPPO_FUNZIONALE GROUP_, S.GRUPPO_FUNZIONALE || TRIM(S.SOTTOGRUPPO_FUNZIONALE) FULLGROUP_, S.ENTE_SEGNALAZIONE_ANOMALIA DEPARTMENT_, TRIM(S.LEADER) LEADER_, TRIM(S.SEGNALATO_DA) REPORTER_, TO_NUMBER(S.PUNTEGGIO_DEMERITO) SEVERITY_, S.TIPO_ANOMALIA TYPE_, S.TIPO_COMPONENTE TCOM_, S.ATTRIBUZIONE DEF_REASON_, NVL(TO_NUMBER(S.PUNTEGGIO_DEMERITO),0) FACTOR_ FROM K8_SEGNALAZIONI_FULL S WHERE (S.CODICE_MODELLO IN **MODELS_HERE** OR S.CODICE_VERSIONE IN **VERSIONS_HERE**)) WHERE **X_HERE** = ?";
	query_ex.args = [];
	query_ex.form = forms.segnalazioni_anomalie_record.controller.getName();
	//Estrattore diretto dal chart
	query_exGraphical.query = "SELECT ID_ FROM (SELECT S.K8_ANOMALIE_ID ID_, S.STATO_SEGNALAZIONE STATUS_, S.GRUPPO_FUNZIONALE GROUP_, S.GRUPPO_FUNZIONALE || TRIM(S.SOTTOGRUPPO_FUNZIONALE) FULLGROUP_, S.ENTE_SEGNALAZIONE_ANOMALIA DEPARTMENT_, TRIM(S.LEADER) LEADER_, TRIM(S.SEGNALATO_DA) REPORTER_, TO_NUMBER(S.PUNTEGGIO_DEMERITO) SEVERITY_, S.TIPO_ANOMALIA TYPE_, S.TIPO_COMPONENTE TCOM_, S.ATTRIBUZIONE DEF_REASON_, NVL(TO_NUMBER(S.PUNTEGGIO_DEMERITO),0) FACTOR_ FROM K8_SEGNALAZIONI_FULL S WHERE (S.CODICE_MODELLO IN **MODELS_HERE** OR S.CODICE_VERSIONE IN **VERSIONS_HERE**)) WHERE **X_HERE** = ? AND **Y_HERE** = ?";
	query_exGraphical.args = [];
	query_exGraphical.form = forms.segnalazioni_anomalie_record.controller.getName();
	title = "Segnalazioni anomalia";
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"A704E833-4BD6-41A8-BA44-734A5D24E5F9"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.setLocalHelpMessage(null);
	container.checkFiltersOnShow();
	container.setupFilters();
	container.drawChart();
}
