items:[
{
height:250,
partType:5,
typeid:19,
uuid:"71F47009-0770-4415-834A-4130974DA614"
},
{
anchors:15,
beanClassName:"com.objectplanet.chart.BarChart",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_21\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"com.objectplanet.chart.BarChart\"> \
  <void property=\"background\"> \
   <object class=\"java.awt.Color\"> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>255<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"chartData\"> \
   <void id=\"StringArray0\" property=\"seriesLabels\"/> \
   <void property=\"seriesLabels\"> \
    <object idref=\"StringArray0\"/> \
   <\/void> \
  <\/void> \
  <void property=\"name\"> \
   <string>bean_793<\/string> \
  <\/void> \
  <void id=\"StringArray1\" property=\"seriesLabels\"/> \
  <void property=\"seriesLabels\"> \
   <object idref=\"StringArray1\"/> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"0,0",
name:"chart",
size:"660,250",
typeid:12,
uuid:"A02BD845-85BE-4E1B-895A-6C506CF041A2"
}
],
name:"report_istogrammi_SKA",
navigatorID:"-1",
onLoadMethodID:"A4E90CA2-7BB6-4032-8B72-23CE07707F61",
onShowMethodID:"96A679AE-FD1C-4187-9299-F39A263E69D3",
paperPrintScale:100,
showInMenu:true,
size:"660,250",
styleName:"keeneight",
typeid:3,
uuid:"6A457226-0DF7-4728-A7BD-2B2B4DA00C9D"