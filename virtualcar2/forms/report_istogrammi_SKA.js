/**
 * @properties={typeid:35,uuid:"24C19FEB-19AD-4956-A524-86679E4F16E9",variableType:-4}
 */
var accepted_filters = [0,1,2,3,4,5,6,7,11,12];

/**
 * @properties={typeid:35,uuid:"7021A79B-11C4-488D-A1FB-C7192683F304",variableType:-4}
 */
var actions = ["load","export"];

/**
 * @properties={typeid:35,uuid:"A7D93345-5834-434A-8F61-C39B36B08105",variableType:-4}
 */
var addingParts = {status		: "AND SK.STATO_ANOMALIA = ?",
                   type			: "AND SK.TIPO_ANOMALIA = ?",
                   subtype		: "AND SK.SOTTOTIPO_ANOMALIA = ?",
                   def_reason	: "AND SK.ATTRIBUZIONE = ?"};

/**
 * @properties={typeid:35,uuid:"2921E9E5-7548-47D4-97A8-D1F8F6747E9A",variableType:-4}
 */
var args = null;

/**
 * @properties={typeid:35,uuid:"8475880B-6ED6-4FDA-BF7D-0201A5511455",variableType:-4}
 */
var container = null;

/**
 * @properties={typeid:35,uuid:"892E0E16-368E-4C53-811D-8D2CA4C46547",variableType:-4}
 */
var filters = new Array();

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"E66B1DEF-996E-4AA5-82D9-F5BA7EBFACE6"}
 */
var query = "";

/**
 * @properties={typeid:35,uuid:"91214D56-241E-4B35-9D04-27D575F6EDD8",variableType:-4}
 */
var query_ex = {query: null, args: null, form: null};

/**
 * @properties={typeid:35,uuid:"B398993B-CC46-446F-8075-11BBAF5DB152",variableType:-4}
 */
var query_exGraphical = {query: null, args: null, form: null};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D2F53918-3DAD-4614-BDED-7C7865FD6F5B"}
 */
var replacePart = "(SK.CODICE_MODELLO IN **MODELS_HERE** OR SK.CODICE_VERSIONE IN **VERSIONS_HERE**)";

/**
 * @properties={typeid:35,uuid:"ED438E07-DAD3-414C-A394-8194B2A0E424",variableType:-4}
 */
var title = null;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"A4E90CA2-7BB6-4032-8B72-23CE07707F61"}
 */
function onLoad(event) {
	container = forms.report_istogrammi;
	//Aggiungo il listener per il click
	
//	JStaffa
//	elements.chart.addMouseListener(container.rightClickHandler);
//	elements.chart.addMouseListener(java.awt.event.MouseListener(container.rightClickHandler));
//FS
	/** @type {java.awt.event.MouseListener} */
	var rightClickHandler=container.rightClickHandler;
	elements.chart.addMouseListener(rightClickHandler);	
	//Setto le query e i parametri TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	query = "SELECT FILTERX_, FILTERY_, **VIEW_HERE** COUNT_ FROM (SELECT **X_HERE** FILTERX_, **Y_HERE** FILTERY_, FACTOR_ FROM (SELECT SK.STATO_ANOMALIA STATUS_, SK.GRUPPO_FUNZIONALE GROUP_, SK.GRUPPO_FUNZIONALE || TRIM(SK.SOTTOGRUPPO_FUNZIONALE) FULLGROUP_,SK.ENTE DEPARTMENT_, TRIM(SK.LEADER) LEADER_, TRIM(SK.SEGNALATO_DA) REPORTER_, SK.GESTIONE_ANOMALIA MANAGEMENT_, TRIM(SK.RESPONSABILE) RESPONSIBLE_, TO_NUMBER(SK.PUNTEGGIO_DEMERITO) SEVERITY_, SK.TIPO_ANOMALIA TYPE_, SK.TIPO_COMPONENTE TCOM_, SK.ATTRIBUZIONE DEF_REASON_, NVL(TO_NUMBER(SK.PUNTEGGIO_DEMERITO),0) FACTOR_ FROM K8_SCHEDE_ANOMALIE SK WHERE (SK.CODICE_MODELLO IN **MODELS_HERE** OR SK.CODICE_VERSIONE IN **VERSIONS_HERE**))) GROUP BY FILTERX_, FILTERY_ ORDER BY FILTERX_, FILTERY_";
	args = [];
	//Estrattore
	query_ex.query = "SELECT ID_ FROM (SELECT SK.K8_ANOMALIE_ID ID_, SK.STATO_ANOMALIA STATUS_, SK.GRUPPO_FUNZIONALE GROUP_, SK.GRUPPO_FUNZIONALE || TRIM(SK.SOTTOGRUPPO_FUNZIONALE) FULLGROUP_,SK.ENTE DEPARTMENT_, TRIM(SK.LEADER) LEADER_, TRIM(SK.SEGNALATO_DA) REPORTER_, SK.GESTIONE_ANOMALIA MANAGEMENT_, TRIM(SK.RESPONSABILE) RESPONSIBLE_, TO_NUMBER(SK.PUNTEGGIO_DEMERITO) SEVERITY_, SK.TIPO_ANOMALIA TYPE_, SK.TIPO_COMPONENTE TCOM_, SK.ATTRIBUZIONE DEF_REASON_, NVL(TO_NUMBER(SK.PUNTEGGIO_DEMERITO),0) FACTOR_ FROM K8_SCHEDE_ANOMALIE SK WHERE (SK.CODICE_MODELLO IN **MODELS_HERE** OR SK.CODICE_VERSIONE IN **VERSIONS_HERE**)) WHERE **X_HERE** = ?";
	query_ex.args = [];
	query_ex.form = forms.schede_anomalie_record.controller.getName();
	//Estrattore diretto dal chart
	query_exGraphical.query = "SELECT ID_ FROM (SELECT SK.K8_ANOMALIE_ID ID_, SK.STATO_ANOMALIA STATUS_, SK.GRUPPO_FUNZIONALE GROUP_, SK.GRUPPO_FUNZIONALE || TRIM(SK.SOTTOGRUPPO_FUNZIONALE) FULLGROUP_,SK.ENTE DEPARTMENT_, TRIM(SK.LEADER) LEADER_, TRIM(SK.SEGNALATO_DA) REPORTER_, SK.GESTIONE_ANOMALIA MANAGEMENT_, TRIM(SK.RESPONSABILE) RESPONSIBLE_, TO_NUMBER(SK.PUNTEGGIO_DEMERITO) SEVERITY_, SK.TIPO_ANOMALIA TYPE_, SK.TIPO_COMPONENTE TCOM_, SK.ATTRIBUZIONE DEF_REASON_, NVL(TO_NUMBER(SK.PUNTEGGIO_DEMERITO),0) FACTOR_ FROM K8_SCHEDE_ANOMALIE SK WHERE (SK.CODICE_MODELLO IN **MODELS_HERE** OR SK.CODICE_VERSIONE IN **VERSIONS_HERE**)) WHERE **X_HERE** = ? AND **Y_HERE** = ?";
	query_exGraphical.args = [];
	query_exGraphical.form = forms.schede_anomalie_record.controller.getName();
	title = "Schede anomalia";
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"96A679AE-FD1C-4187-9299-F39A263E69D3"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.setLocalHelpMessage(null);
	container.checkFiltersOnShow();
	container.setupFilters();
	container.drawChart();
}
