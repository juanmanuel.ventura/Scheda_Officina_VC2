items:[
{
anchors:15,
beanClassName:"com.objectplanet.chart.BarChart",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_21\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"com.objectplanet.chart.BarChart\"> \
  <void property=\"background\"> \
   <object class=\"java.awt.Color\"> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>255<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"chartData\"> \
   <void id=\"StringArray0\" property=\"seriesLabels\"/> \
   <void property=\"seriesLabels\"> \
    <object idref=\"StringArray0\"/> \
   <\/void> \
  <\/void> \
  <void property=\"name\"> \
   <string>bean_793<\/string> \
  <\/void> \
  <void id=\"StringArray1\" property=\"seriesLabels\"/> \
  <void property=\"seriesLabels\"> \
   <object idref=\"StringArray1\"/> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"0,0",
name:"chart",
size:"660,250",
typeid:12,
uuid:"54EFF38A-B018-4EB9-B8AE-592090294F82"
},
{
height:250,
partType:5,
typeid:19,
uuid:"739D50E8-FEDC-4429-BBF0-C06C1148DBED"
}
],
name:"report_istogrammi_SKM",
navigatorID:"-1",
onLoadMethodID:"5A4A8852-ED0A-4FD0-819B-0DDE1C306FE0",
onShowMethodID:"E979C752-E07F-4FD4-BC69-1C984B90C56C",
paperPrintScale:100,
showInMenu:true,
size:"660,250",
styleName:"keeneight",
typeid:3,
uuid:"FE387A22-9A88-4808-9FE8-25D6FCC4B5DD"