/**
 * @properties={typeid:35,uuid:"4A79DA33-D5A4-45CC-95C4-23358A42161C",variableType:-4}
 */
var config = {
	attributes: { type: "line", id: "getVList" },
	bgc1: "#cccccc", bgc2: "#ffffff",
	grid: "#ff0000",
	fntx: "Verdana,0,10", fnty: "Verdana,0,10",
	styles: { }
};

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"1BA8DE27-601B-4673-9EA2-77A22FF483E1",variableType:4}
 */
var cumulative = 0;

/**
 * @properties={typeid:35,uuid:"1D868E80-EACD-413F-9FB9-A48CE05216CE",variableType:-4}
 */
var dates = new Object();

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F78437D2-C6B0-4C84-9749-3C9E966B258F"}
 */
var def_reason = "";

/**
 * @properties={typeid:35,uuid:"A990344A-F209-430B-816F-2C9140AE4B50",variableType:-4}
 */
var delayLables = {
	"0": "SOS",
	"-3": "SOP",
	"-6": "PS",
	"-11": "AV"
};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F78E356E-A588-462B-9A55-7FBDB738409F"}
 */
var delta = "T1\nT2\nT3";

/**
 * @properties={typeid:35,uuid:"948770D7-2D55-4900-A423-52C39E33C903",variableType:-4}
 */
var filterNames = ["type", "subtype", "group", "subgroup", "severity", "start_date", "def_reason"];

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"713F0CED-76F2-4345-8C3D-0F81F34061FD"}
 */
var group = "";

/**
 * @properties={typeid:35,uuid:"DCF2DEAC-8D68-494A-807F-F2B4CE1D2B55",variableType:-4}
 */
var offset = { start: -44, end: +26 };

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"15D5BDA0-31E3-45D3-84CD-09055E6D5845"}
 */
var operation = "avg";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C21E1D01-B38E-4F58-9701-15ED42394E94"}
 */
var project_tmp = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C60D50D0-D52C-45BF-8844-1AB42BDF57A9"}
 */
var quotes = "No";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"507A09F6-BE3A-42A0-8330-AC3285DDDDF4"}
 */
var selected = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"FEE65CA9-DA5B-4338-8866-E6721BB15B7E"}
 */
var severity = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"3A3C1240-B56E-4108-A76B-047B6B0FB83C"}
 */
var subgroup = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"43156DF4-012E-4149-A6E0-ECE440BE546B"}
 */
var subtype = "";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"EF6B007A-654A-452E-B2CA-49DB56030D65",variableType:4}
 */
var target = 0;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"C92A8B6B-BF3C-4766-B056-6AE3BD8F454E",variableType:4}
 */
var target_bkp = 0;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"9102AC78-1A76-459F-8132-6CE8677412D2"}
 */
var type = "";

/**
 * @properties={typeid:24,uuid:"8960A838-234A-42EE-989D-2503B8F144E6"}
 */
function getVList() {
	return application.getValueListArray("progetti");
}

/**
 * @properties={typeid:24,uuid:"BCBFC426-EB9B-4838-B3D9-5245FCB46001"}
 */
function nfx_isProgram() {
	return false;
}

/**
 * @properties={typeid:24,uuid:"C0727F79-08F7-48CE-BF66-E9F4FC44AC4B"}
 */
function getCheckDate(p) {
	var query = "select (extract(year from DATA_RIFERIMENTO) * 12) + extract(month from DATA_RIFERIMENTO) from K8_DATE_RIFERIMENTO where PROGETTO = ? and TIPO = ?";
	var args = [p, "SOS"];
	var date = databaseManager.getDataSetByQuery('ferrari', query, args, 1).getValue(1, 1);
	return date || ( (new Date()).getFullYear() * 12) + 1;
}

/**
 * @properties={typeid:24,uuid:"56155B49-9080-4444-9D76-E7B6DA34E4E0"}
 */
function buildArgument() {
	var a = "round(" + operation + "(" + delta.replace(/\n/g, "+") + "))";
	return a;
}

/**
 * @properties={typeid:24,uuid:"C42E2D7B-0231-4753-9836-AC0FDE5BCB1F"}
 */
function getData(p, v) {
	if (!forms[selected].query || !forms[selected].args) throw "Content query and/or args not detected";
	var date = getCheckDate(p);
	//FS
	/** @type {String} */
	var query = forms[selected].query.replace("REPLACE_HERE", delta.replace(/\n/g, "+"));
	var args = forms[selected].args;
	//Inizio parte di filtraggio
	var filters = {
		type: type,
		subtype: subtype,
		group: group,
		subgroup: subgroup,
		severity: severity,
		start_date: dates[p],
		def_reason: def_reason
	};
	var adding = forms[selected].addingParts;
	var replaceFinal = forms[selected].replacePart;
	var argsFinal = new Array();
	for (var i = 0; i < filterNames.length; i++) {
		if (filters[filterNames[i]]) {
			replaceFinal += " " + adding[filterNames[i]];
			argsFinal.push(filters[filterNames[i]]);
		}
	}
	//Aggiunta parte relativa alla gestione delle versioni AS400
	if (v) replaceFinal += " " + forms[selected].versionSpecificPart.replace("**VERSIONS_HERE**", v);
	query = query.replace(forms[selected].replacePart, replaceFinal);
	args = [date].concat(args).concat([p]).concat(argsFinal);
	//Fine parte di filtraggio
	//application.output(query + "\n" + args);
	var ds = databaseManager.getDataSetByQuery('ferrari', query, args, -1);
	var offsets = ds.getColumnAsArray(1);
	var counts = ds.getColumnAsArray(2);
	var values = { offsets: new Array(), counts: new Array(), max: 0 };
	var first = Math.min(offset.start, offsets[0]);
	var last = Math.max(offset.end, offsets[offsets.length - 1]);
	for (var i2 = first; i2 <= last; i2++) {
		values.offsets.push(i2);
		var n = calculate(offsets, counts, i2);
		values.counts.push(n);
		values.max = Math.max(values.max, n);
	}
	if (cumulative == 1) {
		for (var i3 = 1; i3 < values.counts.length; i3++) {
			values.counts[i3] += values.counts[i3 - 1];
			values.max = values.counts[values.counts.length - 1];
		}
	}
	values.counts = values.counts.slice(values.offsets.indexOf(offset.start), values.offsets.indexOf(offset.end) + 1);
	values.offsets = values.offsets.slice(values.offsets.indexOf(offset.start), values.offsets.indexOf(offset.end) + 1);
	return (ds.getMaxRowIndex() > 0) ? values : null;
}

/**
 * @properties={typeid:24,uuid:"DBC94D0B-2695-4266-AECC-E80F6C42BCC1"}
 */
function setupChart() {
	//FS
	config = {
		attributes: { type: "line", id: "getVList" },
		bgc1: "#cccccc", bgc2: "#ffffff",
		grid: "#ff0000",
		fntx: "Verdana,0,10", fnty: "Verdana,0,10",
		styles: { }
	};
	/** @type {java.awt.chart} */

	var chart = forms[selected].elements.chart;
	if (!chart) throw "Selected form must have a chart element called \"chart\"";
	chart.setZoomOn(true);
	chart.setChartBackground(globals.utils_getJColor(config.bgc1));
	chart.setChartBackground2(globals.utils_getJColor(config.bgc2));

	chart.setAutoLabelSpacingOn(true);
	chart.setSampleLabelsOn(true);

	chart.setValueLinesOn(true);

	chart.setLabelAngle("valueLabelAngle", 45);
	chart.setFont("valueLabelFont", new java.awt.Font("Verdana", java.awt.Font.PLAIN, 9));

	chart.setLegendOn(true);
	chart.setLegendPosition(2);
	chart.setLegendBoxSizeAsFont(true);

	chart.setSampleAxisRange(offset.start, offset.end);

	chart.setFont("sampleLabelFont", globals.utils_getJFont(config.fntx));
	chart.setFont("rangeLabelFont", globals.utils_getJFont(config.fnty));
}

/**
 * @properties={typeid:24,uuid:"C2F1E00C-707E-439C-8EC7-A5A9264BAAD4"}
 */
function drawChart() {
	/** @type {java.awt.chart} */
	var chart = forms[selected].elements.chart;
	if (!chart) throw "Selected form must have a chart element called \"chart\"";
	var projects = forms.report_anomalie_container.projects;
	var versions = forms.report_anomalie_container.versions;
	if (projects) {
		var p = projects.split("\n");
		var data = new Array();
		var legend = new Array();
		for (var i = 0; i < p.length; i++) {
			var v = (typeof versions[p[i]] == "string") ? null : versions[p[i]].join("','");
			var d = getData(p[i], v);
			if (d) {
				legend.push(p[i]);
				data.push(d);
			}
		}
		if (data.length > 0) {
			var max = 0;
			var index = null;
			chart.setSeriesCount(data.length);
			for (var i2 = 0; i2 < data[0].offsets.length; i2++) {
				if (data[0].offsets[i2] == 0) {
					index = i2;
				}
				data[0].offsets[i2] = (delayLables[data[0].offsets[i2]]) ? delayLables[data[0].offsets[i2]] + "\n" + data[0].offsets[i2] : data[0].offsets[i2]
			}
			//			JStaffa aggiunto output per variabile non utilizzata
			//application.output('virtualcar2 report_kpi.drawChart() index: ' + index, LOGGINGLEVEL.INFO);
			chart.setSampleLabels(data[0].offsets);
			chart.setSampleCount(data[0].offsets.length);
			if (quotes != "No") {
				chart.setValueLabelStyle(0);
				chart.setValueLabelsOn(true);
			} else {
				chart.setValueLabelStyle(3);
				chart.setValueLabelsOn(true);
			}
			for (var i3 = 0; i3 < data.length; i3++) {
				if (max < data[i3].max) {
					max = data[i3].max;
				}
				if (config.styles[legend[i3]]) {
					chart.setSampleColor(i3, globals.utils_getJColor(config.styles[legend[i3]][0]));
					chart.setLineWidth(i3, config.styles[legend[i3]][1]);
					chart.setLineStroke(i3, config.styles[legend[i3]][2]);
				} else {
					chart.setSampleColor(i3, globals.utils_getJColor(forms.report_anomalie_container.colors[i3]));
					chart.setLineWidth(i3, 2);
					chart.setLineStroke(i3, [0]);
				}
				chart.setSampleValues(i3, data[i3].counts);
				chart.setSampleDecimalCount(i3, 1);
			}
			var n = Math.pow(10, Math.floor(Math.log(max) / Math.log(10)));
			var m = max - (max % n) + n;
			chart.setRange(0, m);
			//Rimuove la precedente target line
			chart.setTargetValueLine("Obiettivo\n" + target_bkp + " giorni", target_bkp, null, 2);
			//Disegna la target line
			if (target && target < m) {
				target_bkp = target;
				chart.setTargetValueLine("Obiettivo\n" + target + " giorni", target, java.awt.Color.RED, 2);
			}
			chart.setLegendLabels(legend);

			var gridLines = new Array();
			//SAuc
			//for (dl in delayLables) {
			for (var dl in delayLables) {
				gridLines.push(dl);
			}
			chart.setGridLines(gridLines);
			chart.setGridLinesColor(globals.utils_getJColor(config.grid));

			if (forms[selected].title) {
				chart.setTitleOn(true);
				chart.setTitle(forms[selected].title + " " + delta.replace(/\n/g, "+") + " - " + utils.dateFormat(new Date(), forms.report_anomalie_container.chartTitleDateFormat));
			}

			chart.visible = true;
		} else {
			chart.visible = false;
			plugins.dialogs.showWarningDialog("Nessun dato", "Non è presente alcun dato relativo ai criteri selezionati", "Ok");
		}
	} else {
		chart.visible = false;
	}
}

/**
 * @properties={typeid:24,uuid:"25C07DE3-5D32-46EA-BC84-01573ABA6160"}
 */
function calculate(offsets, counts, i) {
	if (operation == "avg") {
		return avg(offsets, counts, i);
	}
	if (operation == "median") {
		return median(offsets, counts, i);
	}
	return 1;
}

/**
 * @properties={typeid:24,uuid:"2E55881C-E5B2-4F5A-8F62-F3A2692ED419"}
 */
function avg(offsets, counts, i) {
	var sum = 0;
	var n = 0;
	for (var j = 0; j <= offsets.length; j++) {
		if (offsets[j] <= i) {
			sum += counts[j];
			n++;
		}
	}
	return (sum) ? sum / n : 0;
}

/**
 * @properties={typeid:24,uuid:"34293628-E534-4E70-B33D-5DA9312AF499"}
 */
function median(offsets, counts, i) {
	var numbers = new Array();
	for (var j = 0; j <= offsets.length; j++) {
		if (offsets[j] <= i) {
			numbers.push(counts[j]);
		}
	}
	if (numbers.length) {
		numbers.sort();
		var position = (numbers.length + 1) / 2;
		return (position % 2 == 0) ? counts[position - 1] : (counts[Math.floor(position) - 1] + counts[Math.ceil(position) - 1]) / 2;
	} else {
		return 0;
	}
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"076AC19F-80C9-4CF0-A329-6D181E399375"}
 */
function setStartDate(oldValue, newValue, event) {
	var now = new Date();
	var d = application.showCalendar(new Date(now.getFullYear(), now.getMonth(), 1), "dd/MM/yyyy");
	if (d) {
		dates[newValue] = utils.timestampToDate(d);
	}
	project_tmp = null;
	elements.delta.requestFocus();
	return true;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"B885F1F1-1304-4DBA-9FF7-A0C07A2CAC89"}
 */
function showStartDates(event) {
	var items = new Array();
	var item = null;
	for (var i in dates) {
		if (dates[i]) {
			//FS
			//dep			item = plugins.popupmenu.createMenuItem(i + ": " + utils.dateFormat(dates[i], "dd/MM/yyyy"), deleteStartDate);
			//dep			item.setMethodArguments([i]);
			var menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem(i + ": " + utils.dateFormat(dates[i], "dd/MM/yyyy"), deleteStartDate);
			item.methodArguments = [i];
			items.push(item);
		}
	}
	if (!items.length) {

		//dep		item = plugins.popupmenu.createMenuItem("Nessun filtro-data impostato", null);
		item = menu.addMenuItem("Nessun filtro-data impostato", null);
		items.push(item);
	}
	//dep	plugins.popupmenu.showPopupMenu(elements.items_list, items);
	menu.show(elements.items_list);
}

/**
 * @properties={typeid:24,uuid:"E8CF3E8D-AECE-4CA2-9A82-A18A6E2D6378"}
 */
function deleteStartDate(item) {
	dates[item] = null;
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"01602FF9-20FD-44A2-82AA-386FB3500A40"}
 */
function deleteAllStartDate(event) {
	dates = new Object();
}

/**
 * @properties={typeid:24,uuid:"93258777-7073-47C0-A382-3E0C050B2991"}
 */
function exportData(project, value) {
	//FS
	/** @type {String} */
	var query = forms[selected].query_ex.query;
	var args = forms[selected].query_ex.args;
	//Inizio parte di filtraggio
	var filters = {
		type: type,
		subtype: subtype,
		group: group,
		subgroup: subgroup,
		severity: severity,
		start_date: dates[project],
		def_reason: def_reason
	};
	var adding = forms[selected].addingParts;
	var replaceFinal = forms[selected].replacePart;
	var argsFinal = new Array();
	for (var i = 0; i < filterNames.length; i++) {
		if (filters[filterNames[i]]) {
			replaceFinal += " " + adding[filterNames[i]];
			argsFinal.push(filters[filterNames[i]]);
		}
	}
	//Aggiunta parte relativa alla gestione delle versioni AS400
	var versions = forms.report_anomalie_container.versions;
	if (typeof versions[project] != "string") replaceFinal += " " + forms[selected].versionSpecificPart.replace("**VERSIONS_HERE**", versions[project].join("','"));
	query = query.replace(forms[selected].replacePart, replaceFinal);
	args = [getCheckDate(project)].concat(args).concat([project]).concat(argsFinal).concat([offset.start + value]);
	//Fine parte di filtraggio
	//application.output(query + "\n" + args);
	forms.schede_modifica.controller.loadRecords(query, args);
	forms.schede_modifica.report();
}

/**
 * @properties={typeid:24,uuid:"25918916-F6C9-486D-8C43-A470C40A0C53"}
 */
function openDeltaPopup() {
	forms.report_anomalie_popup_delta.openPopup(delayLables);
}
