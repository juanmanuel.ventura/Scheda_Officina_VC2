items:[
{
anchors:15,
beanClassName:"com.objectplanet.chart.LineChart",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_21\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"com.objectplanet.chart.LineChart\"> \
  <void property=\"background\"> \
   <object class=\"java.awt.Color\"> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>250<\/int> \
    <int>255<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>80<\/int> \
    <int>80<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"chartData\"> \
   <void id=\"StringArray0\" property=\"seriesLabels\"/> \
   <void property=\"seriesLabels\"> \
    <object idref=\"StringArray0\"/> \
   <\/void> \
  <\/void> \
  <void id=\"StringArray1\" property=\"legendLabels\"/> \
  <void property=\"legendLabels\"> \
   <object idref=\"StringArray1\"/> \
  <\/void> \
  <void property=\"name\"> \
   <string>bean_858<\/string> \
  <\/void> \
  <void id=\"StringArray2\" property=\"seriesLabels\"/> \
  <void property=\"seriesLabels\"> \
   <object idref=\"StringArray2\"/> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"0,0",
name:"chart",
size:"660,250",
typeid:12,
uuid:"406BC07D-BE30-4FDE-9366-B5DE65A178F4"
},
{
height:250,
partType:5,
typeid:19,
uuid:"FABC95CC-8854-4D65-9CE4-47035F947194"
}
],
name:"report_kpi_SKM",
navigatorID:"-1",
onLoadMethodID:"B12047F9-3C5A-42D4-91FA-F529523BCC51",
onShowMethodID:"6D5DCEEE-8DD0-40FA-A1B7-33A4E4E6E376",
paperPrintScale:100,
showInMenu:true,
size:"660,250",
styleName:"keeneight",
typeid:3,
uuid:"CC67AC4D-5C20-4567-9620-842E0194748B"