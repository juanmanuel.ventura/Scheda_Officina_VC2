/**
 * @properties={typeid:35,uuid:"27111B47-DBA9-4116-9CE7-8B097A8342E4",variableType:-4}
 */
var addingParts = {type			: "AND A.TIPO_ANOMALIA = ?",
                   subtype		: "AND A.SOTTOTIPO_ANOMALIA = ?",
                   group		: "AND M.GRUPPO_FUNZIONALE = ?",
                   subgroup		: "AND M.GRUPPO_FUNZIONALE2 = ?",
                   severity		: "AND A.PUNTEGGIO_DEMERITO = ?",
                   start_date	: "AND A.DATA_RILEVAZIONE >= ?",
                   def_reason	: "AND A.ATTRIBUZIONE = ?"};

/**
 * @properties={typeid:35,uuid:"CC0B7E69-F2F3-44D6-99F6-8B054FFC2AD7",variableType:-4}
 */
var args = null;

/**
 * @properties={typeid:35,uuid:"E8F8DBF7-88BB-491E-B374-89B6717BFE59",variableType:-4}
 */
var container = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"6A833AFB-DD55-4B0C-9CF4-09A4704732C6"}
 */
var query = "";

/**
 * @properties={typeid:35,uuid:"B6444C37-E898-43B7-B7AB-A8301B3F0147",variableType:-4}
 */
var query_ex = {query: null, args: null};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C82E5BC0-D23E-46E5-AA49-1B6B8A924866"}
 */
var replacePart = "M.MODELLO = ?";

/**
 * @properties={typeid:35,uuid:"45E018C2-7779-4FB4-ADDE-04C5B3993134",variableType:-4}
 */
var title = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"94A552CF-9BDF-44F4-80FB-DA9908672467"}
 */
var versionSpecificPart = "AND M.VERSIONE IN ('**VERSIONS_HERE**')";

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"B12047F9-3C5A-42D4-91FA-F529523BCC51"}
 */
function onLoad(event) {
	container = forms.report_kpi;
	//Setto le query e i parametri TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	query = "SELECT OFFSET,REPLACE_HERE FROM (SELECT (EXTRACT(YEAR FROM M.DATA_APPROV_SK_PROP_MOD) * 12) + EXTRACT(MONTH FROM M.DATA_APPROV_SK_PROP_MOD) - ? OFFSET,EXTRACT(DAY FROM A.DATA_CHIUSURA_SEGNALAZIONE - A.DATA_RILEVAZIONE) T1,EXTRACT(DAY FROM M.DATA_PROPOSTA_MODIFICA - A.DATA_CHIUSURA_SEGNALAZIONE) T2,ROUND(M.DATA_APPROV_SK_PROP_MOD - M.DATA_PROPOSTA_MODIFICA) T3 FROM K8_SEGNALAZIONI_FULL A INNER JOIN K8_SCHEDE_MODIFICA M ON A.ENTE = M.ENTE_INSERIMENTO_ANOMALIA AND A.NUMERO_SCHEDA = M.NR_SCHEDA_ANOMALIA WHERE M.DATA_APPROV_SK_PROP_MOD IS NOT NULL AND M.CAUSALE_MODIFICA = 34 AND M.MODELLO = ?) ORDER BY OFFSET";
	args = [];
	//Estrattore
	query_ex.query = "SELECT ID_ FROM (SELECT M.K8_SCHEDE_MODIFICA_ID ID_, (EXTRACT(YEAR FROM M.DATA_PROPOSTA_MODIFICA) * 12) + EXTRACT(MONTH FROM M.DATA_PROPOSTA_MODIFICA) - ? OFFSET FROM K8_SEGNALAZIONI_FULL A INNER JOIN K8_SCHEDE_MODIFICA M ON A.ENTE = M.ENTE_INSERIMENTO_ANOMALIA AND A.NUMERO_SCHEDA = M.NR_SCHEDA_ANOMALIA where M.CAUSALE_MODIFICA = ? and M.MODELLO = ?) where OFFSET <= ?";
	query_ex.args = [34];
	title = "Tempi SKM chiuse Media mensile";
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"6D5DCEEE-8DD0-40FA-A1B7-33A4E4E6E376"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.drawChart();
}
