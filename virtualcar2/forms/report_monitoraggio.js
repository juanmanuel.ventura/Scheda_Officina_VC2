/**
 * @properties={typeid:35,uuid:"ACCB2AE2-F539-4D4C-B99C-935E63069CE7",variableType:-4}
 */
var classes = null;

/**@type {Object}
 * @properties={typeid:35,uuid:"513509A5-8A74-4186-B081-CAF7A994FF39",variableType:-4}
 */
var config = {
	attributes: { type: "stack", id: "getVList" },
	bgc1: "#cccccc",
	bgc2: "#ffffff",
	grid: null,
	fntx: "Verdana,0,10",
	fnty: "Verdana,0,10",
	styles: { }
};

/**
 * @properties={typeid:35,uuid:"21C18078-7C82-4677-B528-484EE6C0E470",variableType:-4}
 */
var defaultClasses = [{ id: 1, start: 0, end: 1 }, { id: 2, start: 2, end: 5 }, { id: 3, start: 6, end: 10 }, { id: 4, start: 11, end: 15 }, { id: 5, start: 16, end: 20 }, { id: 6, start: 21, end: 25 }, { id: 7, start: 26, end: 30 }, { id: 8, start: 31, end: 35 }, { id: 9, start: 36, end: 40 }, { id: 10, start: 41, end: null }];

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"32335547-D691-43FA-A908-744BE07A60C6"}
 */
var delta = "T1\nT2\nT3";

/**
 * @properties={typeid:35,uuid:"04CE7AB6-C183-468A-AAE6-CB746F1C5EAF",variableType:-4}
 */
var graphicalActions = {
	"load": { human: "Visualizza", icon: "media:///detail16.png" },
	"export": { human: "Export", icon: "media:///excel16.png" },
	"report": { human: "Full report", icon: "media:///report16.png" }
};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"231BCB60-B677-4A1C-841B-737365C1F78B"}
 */
var mix_filter = "gruppo_funzionale";

/**
 * @properties={typeid:35,uuid:"2C5136AC-0002-464D-84E8-68FBED783E2F",variableType:-4}
 */
var mix_filters = {
	"punteggio_demerito": { human: "Punteggio demerito", type: "number", query: "SEVERITY_", vlist: null },
	"gruppo_funzionale": { human: "Gruppo funzionale", type: "string", query: "GROUP_", vlist: "Gruppi" },
	"tipo_anomalia": { human: "Tipo anomalia", type: "string", query: "TYPE_", vlist: "gestione_anomalie$tipo_anomalia" },
	"tipo_componente": { human: "Tipo componente", type: "string", query: "TCOM_", vlist: "gestione_anomalie$tipo_componente" },
	"attribuzione": { human: "Causale anomalia", type: "string", query: "DEFREASON_", vlist: "gestione_anomalie$causale_anomalia" }
};

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"0B6617BC-310B-4A7D-81A2-9BDAB2810FA9"}
 */
var selected = "";

/**
 * @properties={typeid:24,uuid:"5E0DAB72-F676-4246-BA58-AB5B43BD623B"}
 */
function getVList() {
	var _list = new Array();
	for (var _mf in mix_filters) {
		if (mix_filters[_mf]["vlist"]) {
			var _vlist = application.getValueListArray(mix_filters[_mf]["vlist"]);
			for (var _vl in _vlist) {
				if (_vl && application.getValueListDisplayValue(mix_filters[_mf]["vlist"], _vlist[_vl])) {
					var _dv = application.getValueListDisplayValue(mix_filters[_mf]["vlist"], _vlist[_vl]);
					_list.push(application.getValueListDisplayValue("static$report_mix_filters", _mf) + " - " + _dv.substr(0, 1).toUpperCase() + _dv.substr(1).toLowerCase());
				}
			}
		} else {
			_list.push(application.getValueListDisplayValue("static$report_mix_filters", _mf) + " - <inserire_valore>");
		}
	}
	return _list.sort();
}

/**
 * @properties={typeid:24,uuid:"8FD770EC-4C77-43F3-A411-01834C7AE95E"}
 */
function nfx_isProgram() {
	return false;
}

/**
 * @properties={typeid:24,uuid:"C064BBCE-834B-4F20-9E61-6A5C5C6C90FD"}
 */
function getData(p, v) {
	if (!forms[selected].query || !forms[selected].args) throw "Content query and/or args not detected";
	//	FS
	/** @type {String} */
	var query = forms[selected].query;
	query = query.replace("**FILTER_HERE**", mix_filters[mix_filter].query);
	query = query.replace("**MODELS_HERE**", p);
	query = query.replace("**VERSIONS_HERE**", v);
	var args = forms[selected].args;
	//application.output(query + "\n" + args);
	var ds = databaseManager.getDataSetByQuery('ferrari', query, args, -1);
	var offsets = ds.getColumnAsArray(1);
	var filters = ds.getColumnAsArray(2);
	var counts = ds.getColumnAsArray(3);
	var data = new Object();
	for (var i = 0; i < classes.length; i++) {
		data[classes[i].id] = new Object();
		for (var j = 0; j < offsets.length; j++) {
			if ( (!classes[i].start || offsets[j] >= classes[i].start) && (!classes[i].end || offsets[j] <= classes[i].end)) {
				if (!data[classes[i].id][filters[j]]) {
					data[classes[i].id][filters[j]] = counts[j];
				} else {
					data[classes[i].id][filters[j]] += counts[j];
				}
			}
		}
	}
	return (ds.getMaxRowIndex() > 0) ? data : null;
}

/**
 * @properties={typeid:24,uuid:"155E50DA-0666-4010-85FA-CCCD01C0EED5"}
 */
function setupChart() {
	//FS
	//dep	classes = forms.nfx_interfaccia_gestione_salvataggi.load(controller.getName()) || defaultClasses.slice();
	classes = forms.nfx_interfaccia_gestione_salvataggi.load(controller.getName()) || defaultClasses.slice(0);
	config = {
		attributes: { type: "stack", id: "getVList" },
		bgc1: "#cccccc",
		bgc2: "#ffffff",
		grid: null,
		fntx: "Verdana,0,10",
		fnty: "Verdana,0,10",
		styles: { }
	};

	//FS
	/** @type {java.awt.chart} */
	var chart = forms[selected].elements.chart;
	if (!chart) throw "Selected form must have a chart element called \"chart\"";
	chart.setZoomOn(true);
	//FS
	/** @type {String} */
	var bgc1AsString = globals.utils_getJColor(config.bgc1);
	/** @type {String} */
	var bgc2AsString = globals.utils_getJColor(config.bgc2);
	chart.setChartBackground(bgc1AsString);
	chart.setChartBackground2(bgc2AsString);

	chart.setBarType(1);
	chart.setBarLabelsOn(true);

	chart.setValueLabelStyle(0);

	chart.setValueLinesOn(true);

	chart.setLegendOn(true);
	chart.setLegendPosition(3);
	chart.setLegendBoxSizeAsFont(true);

	chart.setFont("barLabelFont", globals.utils_getJFont(config.fntx));
	chart.setFont("rangeLabelFont", globals.utils_getJFont(config.fnty));
}

/**
 * @properties={typeid:24,uuid:"1EA710C1-E17F-449E-8886-7027468633D8"}
 */
function drawChart() {

	//FS
	/** @type {java.awt.chart} */
	var chart = forms[selected].elements.chart;
	if (!chart) throw "Selected form must have a chart element called \"chart\"";
	var _object = forms.report_anomalie_container.versions;
	var projects = [];
	var versions = [];
	for (var _o in _object) {
		if (typeof _object[_o] == "string") {
			projects.push(_o);
		} else {
			versions = versions.concat(_object[_o])
		}
	}
	if (projects.length != 0 || versions.length != 0) {
		var p = null;
		try {
			p = "('" + projects.join("','") + "')";
		} catch (e) {
			p = "('')";
		}
		var v = null;
		try {
			v = "('" + versions.join("','") + "')";
		} catch (e) {
			v = "('')";
		}
		var data = getData(p, v);
		if (data) {
			chart.setSampleCount(classes.length);
			var filters = getFiltersFromTable(mix_filter);
			chart.setSeriesCount(filters.length);
			var legend = new Array();
			var sum = new Array();
			for (var i = 0; i < filters.length; i++) {
				var sampleValues = new Array();
				for (var j = 0; j < classes.length; j++) {
					sampleValues[j] = (data[j + 1] && data[j + 1][filters[i]]) ? data[j + 1][filters[i]] : 0;
					sum[j] = (!sum[j]) ? sampleValues[j] : sum[j] + sampleValues[j];
				}
				chart.setSampleValues(i, sampleValues);
				var _style = config.styles[application.getValueListDisplayValue("static$report_mix_filters", mix_filter) + " - " + getLegendLabel(filters[i])];
				if (_style) {
					chart.setSampleColor(i, globals.utils_getJColor(_style[0]));
				} else {
					chart.setSampleColor(i, globals.utils_getJColor(forms.report_anomalie_container.colors[i]));
				}
				legend.push(getLegendLabel(filters[i]));
			}
			chart.setValueLabelsOn(true);
			chart.setLegendLabels(legend);
			//TODO: togliere costrutto for e utilizzare callback
			var max = 0;
			for (var i2 = 0; i2 < sum.length; i2++) {
				max = Math.max(max, sum[i2]);
			}
			chart.setRange(0, max + 5);

			chart.setBarLabels(getBarLabels());

			if (forms[selected].title) {
				chart.setTitleOn(true);
				chart.setTitle(forms[selected].title + " - " + forms.report_anomalie_container.projects.replace(/\n/g, ",") + " " + mix_filters[mix_filter].human + " - " + utils.dateFormat(new Date(), forms.report_anomalie_container.chartTitleDateFormat));
			}

			chart.visible = true;
		} else {
			chart.visible = false;
			plugins.dialogs.showWarningDialog("Nessun dato", "Non è presente alcun dato relativo ai criteri selezionati", "Ok");
		}
	} else {
		chart.visible = false;
	}
}

/**
 * @properties={typeid:24,uuid:"09BE689C-F012-4D3D-BC1E-844ED70E06E9"}
 */
function getFiltersFromTable(filter) {
	var local_filter = (mix_filters[filter].type == "number") ? "to_number(A." + filter + ")" : "A." + filter;
	var query = "select FILTER_ from K8_SEGNALAZIONI_FULL A inner join K8_SCHEDE_MODIFICA M on A.ENTE = M.ENTE_INSERIMENTO_ANOMALIA and A.NUMERO_SCHEDA = M.NR_SCHEDA_ANOMALIA where M.CAUSALE_MODIFICA = 34 and M.STATO_SCHEDA = 'APERTA' and FILTER_ is not null group by FILTER_ order by FILTER_ asc".replace(/FILTER_/g, local_filter);
	var args = [];
	var ds = databaseManager.getDataSetByQuery('ferrari', query, args, -1);
	return ds.getColumnAsArray(1);
}

/**
 * @properties={typeid:24,uuid:"BFA78F29-C511-4DCF-81EF-06B823C1A924"}
 */
function getBarLabels() {
	var labels = new Array();
	for (var i = 0; i < classes.length; i++) {
		var s = (classes[i].start !== null) ? classes[i].start : " < ";
		var e = (classes[i].end !== null) ? classes[i].end : " > ";
		labels.push("[" + s + ":" + e + "]");
	}
	return labels;
}

/**
 * @properties={typeid:24,uuid:"C92A6F5B-9B33-47E7-93F0-CF70D6BE54A1"}
 */
function getLegendLabel(value) {
	if (mix_filters[mix_filter].vlist) {
		var str = application.getValueListDisplayValue(mix_filters[mix_filter].vlist, value);
		return (str) ? str.substr(0, 1).toUpperCase() + str.substr(1).toLowerCase() : value;
	} else {
		return value;
	}
}

/**
 * @properties={typeid:24,uuid:"3D789DF8-A8E3-4270-9879-2D2A5FD78F0B"}
 */
function exportData(project, value) {
	var _object = forms.report_anomalie_container.versions;
	var p = "('')";
	var v = "('')";
	if (typeof _object[project] == "string") {
		p = "('" + project + "')";
	} else {
		v = "('" + _object[project].join("','") + "')";
	}
	var query = forms[selected].query_ex.query.replace(/\*\*FILTER_HERE\*\*/g, mix_filters[mix_filter].query).replace(/\*\*MODELS_HERE\*\*/g, p).replace(/\*\*VERSIONS_HERE\*\*/g, v);
	var s = classes[value].start || 0;
	var e = classes[value].end || 1000000;
	var args = forms[selected].query_ex.args.concat([s, e]);
	//application.output(query + "\n" + args);
	forms.schede_modifica.controller.loadRecords(query, args);
	forms.schede_modifica.report();
}

/**
 * @param {JSEvent} event
 * @properties={typeid:24,uuid:"4F90E909-B105-4BF4-90B7-525589ED72E0"}
 */
function openClassesPopup(event) {
	forms.report_anomalie_popup_classes.openPopup(controller.getName(), classes);
}

/**@param {java.awt.event.MouseEvent} event
 * @properties={typeid:24,uuid:"0C0F4D3C-30A9-4307-A705-29270E6D344D"}
 */
function rightClickHandler(event) {
	if (event.getID() == java.awt.event.MouseEvent.MOUSE_CLICKED && event.getButton() == java.awt.event.MouseEvent.BUTTON3) {
		//FS
		/** @type {java.awt.chart} */
		var chart = forms[selected].elements.chart;
		var serie = chart.getLastSelectedSeries();
		var sample = chart.getLastSelectedSample();

		//dep var menuItems = new Array();
		/** @type {plugins.window.Popup} */
		var menu = plugins.window.createPopupMenu();
		if (sample != -1 && serie != -1) {
			//FS
			/** @type {Array<String>} */
			var actions = forms[selected].actions;
			/** @type {plugins.window.MenuItem} */
			var item = null;
			for (var i = 0; i < actions.length; i++) {
				//dep	item = plugins.popupmenu.createMenuItem(graphicalActions[actions[i]].human, graphicalExportData, graphicalActions[actions[i]].icon);
				//dep	item.setMethodArguments([serie, sample, actions[i]]);
				item = menu.addMenuItem(graphicalActions[actions[i]].human, graphicalExportData, graphicalActions[actions[i]].icon);
				item.methodArguments = [serie, sample, actions[i]];
				//dep menuItems.push(item);

			}
		} else {
			//dep			menuItems.push(plugins.popupmenu.createMenuItem("Nessun elemento selezionato...", null, "media:///cancel16.png"));
			menu.addMenuItem("Nessun elemento selezionato...", null, "media:///cancel16.png");

		}

		//TODO: bisogna inserire in NFX un metodo che restituisca la finestra dell'applicazione
		//FS
		//dep 	var window = Packages.javax.swing.SwingUtilities.getWindowAncestor(forms.nfx_interfaccia_pannello_base.elements.keyLabel);
		var pointer = java.awt.MouseInfo.getPointerInfo().getLocation();
		var labelAsComponent = java.awt.Component(forms.nfx_interfaccia_pannello_base.elements.keyLabel);
		var window = Packages.javax.swing.SwingUtilities.getWindowAncestor(labelAsComponent);
		//dep	plugins.popupmenu.showPopupMenu(pointer.getX() - window.getX(), pointer.getY() - window.getY(), menuItems);
		menu.show(pointer.getX() - window.getX(), pointer.getY() - window.getY());
	}
}

/**@param {Number} index
 * @param {Number} parentIndex
 * @param {Boolean} isSelected
 * @param {String} parentText
 * @param {String} menuText
 * @param {Number} serie
 * @param {Number} sample
 * @param {String} action
 * @properties={typeid:24,uuid:"AE430D66-5C31-46ED-BEF0-BCD3F8A897D3"}
 */
function graphicalExportData(index, parentIndex, isSelected, parentText, menuText, serie, sample, action) {
	var _object = forms.report_anomalie_container.versions;
	var projects = [];
	var versions = [];
	for (var _o in _object) {
		if (typeof _object[_o] == "string") {
			projects.push(_o);
		} else {
			versions = versions.concat(_object[_o])
		}
	}
	var p = null;
	try {
		p = "('" + projects.join("','") + "')";
	} catch (e) {
		p = "('')";
	}
	var v = null;
	try {
		v = "('" + versions.join("','") + "')";
	} catch (e) {
		v = "('')";
	}
	var query = forms[selected].query_exGraphical.query.replace(/\*\*FILTER_HERE\*\*/g, mix_filters[mix_filter].query).replace(/\*\*MODELS_HERE\*\*/g, p).replace(/\*\*VERSIONS_HERE\*\*/g, v);
	var s = classes[sample].start || 0;
	var e = classes[sample].end || 1000000;
	var f = getSelectedFilterRealValue(serie);
	var args = forms[selected].query_exGraphical.args.concat([f, s, e]);
	//application.output(query + "\n" + args);
	forms.schede_modifica.controller.loadRecords(query, args);
	if (action == "load") {
		globals.nfx_goTo(forms.schede_modifica_record.controller.getName());
	}
	if (action == "export") {
		globals.nfx_launchExport(forms.schede_modifica_record.controller.getName());
	}
	if (action == "report") {
		forms.schede_modifica_record.report();
	}
}

/**
 * @properties={typeid:24,uuid:"5594BE37-5717-4B8E-B98C-9E7C1A7DF55C"}
 */
function getSelectedFilterRealValue(serie) {
	/** @type {java.awt.chart} */
	var chart = forms[selected].elements.chart;
	var list = mix_filters[mix_filter].vlist;
	var filterValue = null;
	if (list) {
		var ds = application.getValueListItems(list);
		var real = ds.getColumnAsArray(2);
		var diplay = ds.getColumnAsArray(1);
		var element = chart.getLegendLabels()[serie];
		var index = forms.report_anomalie_container.indexOfCaseInsensitive(diplay, element);
		filterValue = (index != -1) ? real[index] : element;
	} else {
		filterValue = chart.getLegendLabels()[serie];
	}
	return filterValue;
}
