/**
 * @properties={typeid:35,uuid:"A01AF8EE-C47D-4DDE-AE40-2D06CBC1D628",variableType:-4}
 */
var actions = ["load","export","report"];

/**
 * @properties={typeid:35,uuid:"605977DD-8123-41F1-8B09-BE6D1FB0C062",variableType:-4}
 */
var args = null;

/**
 * @properties={typeid:35,uuid:"2A43104B-2991-4B76-9189-E27CC744861D",variableType:-4}
 */
var container = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D3D1F0B8-841C-4A4F-AF8D-86B24381E9CC"}
 */
var query = "";

/**
 * @properties={typeid:35,uuid:"26C62683-71AC-4D9A-BE02-F7FC03EF1412",variableType:-4}
 */
var query_ex = {query: null, args: null};

/**
 * @properties={typeid:35,uuid:"16331DC1-60B6-4454-8245-44ACC33DDDC8",variableType:-4}
 */
var query_exGraphical = {query: null, args: null};

/**
 * @properties={typeid:35,uuid:"44AC55DB-FC4E-45A8-BBB1-DC78530F64EB",variableType:-4}
 */
var title = null;

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"49207F75-1F80-4369-A943-E7B4E3A13A05"}
 */
function onLoad(event) {
	container = forms.report_monitoraggio;
	//Aggiungo il listener per il click
	
//	JStaffa
//	elements.chart.addMouseListener(container.rightClickHandler);
//	elements.chart.addMouseListener(java.awt.event.MouseListener(container.rightClickHandler));
//FS
/** @type {java.awt.event.MouseListener} */
var rightClickHandler=container.rightClickHandler;
	elements.chart.addMouseListener(rightClickHandler);

	//Setto le query e i parametri TRANNE il primo l'ultimo, che verranno generati dal metodo di generazione del chart
	query = "SELECT SELECTED_,FILTER_,COUNT(*) COUNT_ FROM (SELECT DELTA_ SELECTED_, **FILTER_HERE** FILTER_ FROM (SELECT A.PUNTEGGIO_DEMERITO SEVERITY_,A.TIPO_ANOMALIA TYPE_,A.TIPO_COMPONENTE TCOM_,A.ATTRIBUZIONE DEFREASON_,M.GRUPPO_FUNZIONALE GROUP_,FLOOR(SYSDATE - A.DATA_RILEVAZIONE) DELTA_ FROM K8_SEGNALAZIONI_FULL A INNER JOIN K8_SCHEDE_MODIFICA M ON A.ENTE = M.ENTE_INSERIMENTO_ANOMALIA AND A.NUMERO_SCHEDA = M.NR_SCHEDA_ANOMALIA WHERE M.CAUSALE_MODIFICA = 34 AND M.STATO_SCHEDA = 'APERTA' AND (M.MODELLO IN **MODELS_HERE** OR M.VERSIONE IN **VERSIONS_HERE**))) WHERE FILTER_ IS NOT NULL GROUP BY SELECTED_, FILTER_ ORDER BY SELECTED_ ASC, FILTER_ ASC";
	args = [];
	//Estrattore menù
	query_ex.query = "SELECT ID_ FROM (SELECT M.K8_SCHEDE_MODIFICA_ID ID_, TO_NUMBER(A.PUNTEGGIO_DEMERITO) SEVERITY_, A.TIPO_ANOMALIA TYPE_, A.TIPO_COMPONENTE TCOM_,A.ATTRIBUZIONE DEFREASON_, M.GRUPPO_FUNZIONALE GROUP_, FLOOR(SYSDATE - A.DATA_RILEVAZIONE) DELTA_ FROM K8_SEGNALAZIONI_FULL A INNER JOIN K8_SCHEDE_MODIFICA M ON A.ENTE = M.ENTE_INSERIMENTO_ANOMALIA AND A.NUMERO_SCHEDA = M.NR_SCHEDA_ANOMALIA WHERE M.CAUSALE_MODIFICA = 34 AND M.STATO_SCHEDA = 'APERTA' AND (M.MODELLO IN **MODELS_HERE** OR M.VERSIONE IN **VERSIONS_HERE**)) WHERE **FILTER_HERE** IS NOT NULL AND DELTA_ >= ? AND DELTA_ <= ?";
	query_ex.args = [];
	//Estrattore diretto dal chart
	query_exGraphical.query = "SELECT ID_ FROM (SELECT M.K8_SCHEDE_MODIFICA_ID ID_, TO_NUMBER(A.PUNTEGGIO_DEMERITO) SEVERITY_, A.TIPO_ANOMALIA TYPE_, A.TIPO_COMPONENTE TCOM_,A.ATTRIBUZIONE DEFREASON_, M.GRUPPO_FUNZIONALE GROUP_, FLOOR(SYSDATE - A.DATA_RILEVAZIONE) DELTA_ FROM K8_SEGNALAZIONI_FULL A INNER JOIN K8_SCHEDE_MODIFICA M ON A.ENTE = M.ENTE_INSERIMENTO_ANOMALIA AND A.NUMERO_SCHEDA = M.NR_SCHEDA_ANOMALIA WHERE M.CAUSALE_MODIFICA = 34 AND M.STATO_SCHEDA = 'APERTA' AND (M.MODELLO IN **MODELS_HERE** OR M.VERSIONE IN **VERSIONS_HERE**)) WHERE **FILTER_HERE** = ? AND DELTA_ >= ? AND DELTA_ <= ?";
	query_exGraphical.args = [];
	title = "Monitoraggio SKM Aperte";
}

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"FD634766-ABA9-4AE9-B21A-B8935B80833B"}
 */
function onShowForm(firstShow, event) {
	container.selected = controller.getName();
	if(firstShow){
		container.setupChart();
	}
	container.drawChart();
}
