/**
 *
 * @properties={typeid:24,uuid:"37002EC0-3487-4BDD-A560-DEA971AB879A"}
 */
function nfx_defineAccess()
{
	return [true,true,true];
}

/**
 *
 * @properties={typeid:24,uuid:"14B6FF5E-ED6F-449B-81AD-C7EE3FDCD723"}
 */
function nfx_isProgram()
{
	return false;
}
