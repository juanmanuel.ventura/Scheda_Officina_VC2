/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"8632B578-1CD6-4948-A89A-E3F4ACCBECE5"}
 */
var nfx_orderBy = "k8_righe_azionam_manovre_id asc";

/**
 *
 * @properties={typeid:24,uuid:"B31D201E-2772-45E5-8D63-3F45AE800040"}
 */
function nfx_getTitle(){
	return "Azionamenti/Manovre";
}
