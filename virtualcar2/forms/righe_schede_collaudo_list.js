/**
 *
 * @properties={typeid:24,uuid:"7BF2D152-0D00-469E-A5D1-1121AC90BF59"}
 */
function nfx_getTitle() {
	return "Collaudo";
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"1527B603-6A82-4ABE-B17A-AE230DF2325A"}
 */
function onAction(event) {
	var tree = forms.nfx_interfaccia_pannello_tree.elements.tree;
	var title = forms.segnalazioni_anomalie_record.nfx_getTitle();
	var isReachable = globals.nfx_getNavigatorTreeNode(tree, title, null);
	if (isReachable) {
		/** @type {JSFoundSet} */
		var segn = k8_righe_schede_collaudo_to_k8_segnalazioni_anomalie;
		if (segn.getSize() == 0) {
			segn.newRecord(false, true);
			segn.tipo_scheda = "SEGNALAZIONE";
			segn.codice_modello = k8_righe_schede_collaudo_to_k8_schede_collaudo.progetto;
			segn.codice_versione = k8_righe_schede_collaudo_to_k8_schede_collaudo.vettura;
			segn.segnalato_da = k8_righe_schede_collaudo_to_k8_schede_collaudo.richiedente;
			segn.gruppo_funzionale = gruppo;
			segn.sottogruppo_funzionale = sottogruppo;
			segn.codice_disegno = numero_disegno;
			segn.esponente = esponente;
			segn.punteggio_demerito = demerito;
			segn.codice_difetto = anomalia;
			segn.km_percorsi = km_anomalia;
			databaseManager.saveData();
			forms.segnalazioni_anomalie_record.fromCollaudo = true;
			//FS
			//dep			var id = databaseManager.getDataSetByQuery(controller.getServerName(), "select max(K8_ANOMALIE_ID) from K8_SEGNALAZIONI_ANOMALIE", new Array(), 1).getValue(1, 1);
			var id = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), "select max(K8_ANOMALIE_ID) from K8_SEGNALAZIONI_ANOMALIE", new Array(), 1).getValue(1, 1);

			globals.nfx_goToProgramAndRecord(forms.segnalazioni_anomalie_record.controller.getName(), id, "k8_anomalie_id");
		} else {
			plugins.dialogs.showErrorDialog("Errore", "Per questo collaudo è già presente una scheda anomalia.", "Ok");
		}
	} else {
		plugins.dialogs.showInfoDialog("Form irraggiungibile", "Il form \"" + title + "\" non è presente nell'albero di navigazione.\nPer utilizzare questa funzionalità è necessario aggiungerlo", "Ok");
	}
}
