/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"2E03B15C-4E80-4022-B192-D14D9FE3B0D5"}
 */
var nfx_orderBy = "nriga asc";

/**
 *
 * @properties={typeid:24,uuid:"82EABFD1-63AE-4805-A2C3-52F2DDC6609E"}
 */
function nfx_defineAccess()
{
	return [false,false,false];
}

/**
 *
 * @properties={typeid:24,uuid:"1705A37B-5FF9-4929-9322-DA6B0AF57AC1"}
 */
function nfx_isProgram()
{
	return false;
}
