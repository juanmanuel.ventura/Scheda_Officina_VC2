/**
 * @properties={typeid:24,uuid:"38A892D6-285B-44F5-95BA-60D2FD739A3B"}
 */
function nfx_getTitle() {
	return "Esecuzione";
}

/**
 * @properties={typeid:24,uuid:"C6EA5651-3185-458A-94BE-DD25314DEA14"}
 */
function nfx_isProgram() {
	return null;
}

/**
 * @properties={typeid:24,uuid:"6DC204B7-11BC-4CE8-B3EB-CC2A798531BC"}
 */
function nfx_defineAccess() {
	return [true, true, true];
}

/**
 * @properties={typeid:24,uuid:"B9627AB8-1E14-4D5E-9B27-9070A5F6803C"}
 */
function nfx_postAdd() {
	post();
}

/**
 * @properties={typeid:24,uuid:"C536E349-518C-456E-9B02-64D6DA653DE0"}
 */
function nfx_postEdit() {
	post();
}

/**
 * @properties={typeid:24,uuid:"C79A76AF-262C-4BBA-9CA9-C99C8B836E0D"}
 */
function post() {
	//SAuc
	/** @type {JSFoundSet} */
	var fs = k8_righe_schede_officina_to_k8_righe_schede_officina_op;
	if(fs)
	fs.setSelectedIndex(1);
	application.updateUI();
}

/**
 * @properties={typeid:24,uuid:"F9174769-C83D-4058-A57D-EDDF1B9D408B"}
 */
function addLine() {
	if (!controller.readOnly) {
		var rel = k8_righe_schede_officina_to_k8_righe_schede_officina_op;
		rel.newRecord(false, true);
		databaseManager.saveData();
		databaseManager.refreshRecordFromDatabase(rel, -1);
	}
}

/**
 * @properties={typeid:24,uuid:"19A31FC8-98BE-48A4-8F42-4BEEB646B882"}
 */
function removeLine() {
	if (!controller.readOnly) {
		var rel = k8_righe_schede_officina_to_k8_righe_schede_officina_op;
		rel.deleteRecord();
		databaseManager.saveData();
		databaseManager.refreshRecordFromDatabase(rel, -1);
	}
}
