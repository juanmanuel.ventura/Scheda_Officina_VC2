/**
 * @properties={typeid:24,uuid:"A3C5FE74-C35B-4DC1-9F1C-F660059C789E"}
 */
function nfx_getTitle() {
	return "Richiesta";
}

/**
 * @properties={typeid:24,uuid:"182F9515-11F9-4E82-8733-7DDAF2D595E7"}
 */
function nfx_isProgram() {
	return null;
}

/**
 * @properties={typeid:24,uuid:"C35409BE-C749-4F75-9833-FA92E0052F21"}
 */
function nfx_defineAccess() {
	return [true, true, true];
}
