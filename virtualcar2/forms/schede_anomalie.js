/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"116B0F7B-B9FA-4A6F-8345-75E5CCB2A25D",variableType:4}
 */
var activateWebServices = 0;

/**
 * @properties={typeid:35,uuid:"50105750-CA82-416E-924F-BF852F2A5BBE",variableType:-4}
 */
var nfx_related = ["anomalie_vetture_table","schede_anomalie_descrizione_record","schede_anomalie_analisi_record","schede_anomalie_dettaglio_record", "schede_anomalie_provvedimenti_record", "documenti_nas_sa_record", "documenti_nas_ska_record", "schede_modifica_list", "time_line_table", "tag_table", "anomalie_disegni_table", "anomalie_responsabili_table", "anomalie_requisiti_table"];

/**
 * @properties={typeid:24,uuid:"B660A578-8978-402F-BF6F-EB0C9A8AFE12"}
 */
function nfx_preAdd(){
	globals.vc2_setRightDepartments(true);
	if(typeof forms[controller.getName()].local_preAdd == "function"){
		forms[controller.getName()].local_preAdd();
	}
}

/**
 * @properties={typeid:24,uuid:"B6FAABCD-7A51-411B-88B6-7B5493280B44"}
 */
function nfx_postAdd(){
	globals.vc2_setRightDepartments(false);
	if(typeof forms[controller.getName()].local_postAdd == "function"){
		forms[controller.getName()].local_postAdd();
	}
}

/**
 * @properties={typeid:24,uuid:"126FE6AD-250F-40C5-BE24-DFDAD4C629D1"}
 */
function nfx_postAddOnCancel(){
	globals.vc2_setRightDepartments(false);
	if(typeof forms[controller.getName()].local_postAddOnCancel == "function"){
		forms[controller.getName()].local_postAddOnCancel();
	}
}

/**
 * @properties={typeid:24,uuid:"40E14A73-B5A1-4E7D-B272-CBF8490F6F1F"}
 */
function nfx_preEdit(){
	globals.vc2_setRightDepartments(true);
	if(typeof forms[controller.getName()].local_preEdit == "function"){
		forms[controller.getName()].local_preEdit();
	}
}

/**
 * @properties={typeid:24,uuid:"4A0DE421-648D-4E2B-A89A-3FFDA56DD0B9"}
 */
function nfx_postEdit(){
	modification_user_ska = globals.nfx_user;
	modification_date_ska = new Date();
	
	globals.vc2_setRightDepartments(false);
	if(typeof forms[controller.getName()].local_postEdit == "function"){
		forms[controller.getName()].local_postEdit();
	}
}

/**
 * @properties={typeid:24,uuid:"7D5FE97A-AE1E-4B3B-929B-E7F4BAA4B320"}
 */
function nfx_postEditOnCancel(){
	globals.vc2_setRightDepartments(false);
	if(typeof forms[controller.getName()].local_postEditOnCancel == "function"){
		forms[controller.getName()].local_postEditOnCancel();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"CDD246DE-1755-4691-A4AA-4B736377B992"}
 */
function chiusuraScheda()
{
	if(stato_anomalia != "CHIUSA"){
		if(attribuzione == "PROCESSO" || attribuzione == "FORNITURA"){
			forms.schede_anomalie_chiusura_pf.openPopup();
			var _r = forms.schede_anomalie_chiusura_pf._canClose;
			if(_r > 0){
				if (nfx_validate(null) == -1){
					return;
				}
				utente_chiusura_scheda = modification_user_ska = globals.nfx_user;
				data_chiusura = modification_date_ska = new Date();
				stato_anomalia = "CHIUSA";
				forms.nfx_interfaccia_pannello_buttons.editSave(controller.getName());
				
			}
			return;
		}
		
		if (plugins.dialogs.showInfoDialog("Cambio stato della scheda","Questa scheda verra' chiusa e non sara' possibile modificarla, continuare?","Si","No") == "No"){
			return;
		}else{
			if (nfx_validate(null) == -1){
				return;
			}
			utente_chiusura_scheda = modification_user_ska = globals.nfx_user;
			data_chiusura = modification_date_ska = new Date();
			stato_anomalia = "CHIUSA";
			forms.nfx_interfaccia_pannello_buttons.editSave(controller.getName());
		}
	}else{
		plugins.dialogs.showErrorDialog("Errore","Impossibile completate l'operazione, la scheda è già chiusa","Ok");
	}
}

/**
 *
 * @properties={typeid:24,uuid:"8B428572-6243-4CC0-BD7A-C844E2AD0D31"}
 */
function nfx_defineAccess() {
	if(globals.nfx_user == "fsalvarani" || globals.isDeveloper(globals.nfx_user)){
		return [false, false, true];
	}
	if (stato_anomalia == "CHIUSA") {
		if(globals.nfx_selectedTab() == forms.documenti_nas_ska_record.controller.getName()){
			return [false, false, false];
		}else{
			return [false, false, false, true];
		}
	} else {
		return [false, false, true];
	}
}

/**
 *
 * @properties={typeid:24,uuid:"6322AB6F-D2F7-4BD7-9708-2DAB1C2B58F2"}
 */
function nfx_excelExport(){
	var toReturn = new Array();
	toReturn.push({label:	"Descrizione",		field:	"k8_descrizione",	format:	null});
	toReturn.push({label:	"Analisi",			field:	"k8_analisi",		format:	null});
	toReturn.push({label:	"Provvedimenti",	field:	"k8_provvedimento",	format:	null});
	return toReturn;
}

/**
 *
 * @properties={typeid:24,uuid:"01B54E34-1BFE-4B02-813D-20C00A05E2D1"}
 */
function nfx_postValidate()
{
	if (sendToService() < 0 && !globals.vc2_persistUnacceptedRecord(foundset,
			"mancato_invio_anomalia",
			"numero_scheda")){
		return -1;		
	}
}

/**
 *
 * @properties={typeid:24,uuid:"14FF244B-4A04-46BC-BD9F-7E2A6697E706"}
 */
function nfx_sks()
{
	return [{icon: "chiudi_segnalazione.png", 	tooltip:"Chiusura Scheda", 				method:"chiusuraScheda"},
			{icon: "findplus.png",				tooltip:"Ricerca per descrizione...",	method:"openDescriptionSearch"}]; 
}

/**
 *
 * @properties={typeid:24,uuid:"0878ED72-DF4A-44B6-AE4C-766205D7C78B"}
 */
function nfx_validate(fields){
	var allFields;
	if (fields){
		allFields = fields;
	}else{
		allFields = {"ente"						:	"Ente (scheda anomalia)",
		             "data_rilevazione_scheda"	:	"Data rilevazione",
		             "codice_modello"			:	"Cod. modello",
		             "codice_versione"			:	"Versione",
		             "gruppo_funzionale"		:	"Gruppo funzionale",
		             "tipo_anomalia"			:	"Tipo anomalia",
		             "segnalato_da"				:	"Segnalato da",
		             "leader"					:	"Leader",
		             "responsabile"				:	"Responsabile",
		             "codice_disegno"			:	"Cod. disegno\n(Nel caso sia sconosciuto, inserire nel campo Cod.disegno il Gruppo 79 corrispondente)",
		             "tipo_componente"			:	"Tipo componente",
		             "codice_difetto"			:	"Difetto",
		             "punteggio_demerito"		:	"Demerito",
		             "attribuzione"				:	"Causale anomalia"};
	}
	if (!stato_anomalia){
		stato_anomalia = "APERTA";
	}

	if (!data_registrazione_scheda){
		data_registrazione_scheda = new Date();
	}
	
	if(!tipo_scheda){
		tipo_scheda = "ANOMALIA";
	}
	
	if(gruppo_funzionale && globals.vc2_isSottogruppoFunzionaleRequired(gruppo_funzionale) && !sottogruppo_funzionale){
			allFields["sottogruppo_funzionale"] = "Sottogruppo Funzionale";
	}
		
	if(tipo_componente == "V"){
		allFields["esponente"] = "Esponente";
		allFields["revisione"] = "Revisione";
	}
	
	var missing = [];
	for (var f in allFields){
		if (foundset[f] === null){
			missing.push(f);
		}
	}

	//INIZIO CHECK RESPONSABILE
	if(responsabile && !databaseManager.getDataSetByQuery("ferrari","SELECT * FROM XCH_SRUTE00F_IN WHERE TRIM(MCDUTE) = ?",[responsabile],1).getMaxRowIndex()){
		missing.push("Responsabile");
	}
	//FINE CHECK RESPONSABILE

	if (missing.length > 0){
		var message = "Compilare i campi obbligatori:\n\n";
		var i = 1;
		for each(var mf in missing){
			message += (allFields[mf]) ? i++ + ". Inserire " + allFields[mf] + "\n" : mf + ": valore non corretto\n";
		}
		plugins.dialogs.showErrorDialog("Errori nella compilazione",message,"Ok");
		return -1;
	}else{
		//I campi obbligatori sono tutti compilati qui imposto criteri più complessi (servirebbe una callback)
		if(data_registrazione_scheda < data_rilevazione){
			plugins.dialogs.showErrorDialog("Errore inserimento","La data di rilevazione deve necessariamente essere minore o uguale alla data di registrazione.","Ok");
			return -1
		}else{
			return 0;
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"E553C553-49D8-4329-AE3D-5E1F32F5CC5E"}
 */
function packData(found)
{
	var f = found || foundset;
		var data = {
		  'SCCTIP' : 'T', //OBBLIGATORIO La Testata
		  'SCVEEX' : globals.vc2_webServiceMode, //OBBLIGATORIO Sempre esecuzione
          'SCOP'   : "I", //<INSERIMENTO/MODIFICA/CANCELLAZIONE> OBBLIGATORIO
          "SCAPPL" : "VIRTCAR2",
          'SCCENT' : f.ente, //<Cd Ente> OBBIGATORIO
          'SCNREG' : f.numero_scheda, //<N Registrazione evento> OBBLIGATORIO IN MODIFICA.
          'SCDREG' : globals.utils_dateFormatAS400(f.data_registrazione_scheda), //<Dt Reg.Evento> OBBLIGATORIO. YYYY MM DD
          'SCDRIL' : globals.utils_dateFormatAS400(f.data_rilevazione_scheda), //<Dt Rilevazione> OBBLIGATORIO
          'SCMODE' : f.codice_modello, //<Modello> OBBLIGATORIO.
		  'SCVERS' : f.codice_versione, //<Versione> OBBLIGARIO
		  'SCPROV' : globals.utils_stateFormatAS400(f.stato_anomalia), //<Stato Provvedim.> OBBLIGATORIO. Chiarire Funzione. Un carattere. 
		  'SCENTE' : f.ente_segnalazione_anomalia,
          'SCNSCH' : f.numero_segnalazione,
          'SCDAVA' : "", //Data ultimo av...anzamento?
          'SCCOMP' : f.componente_vettura, //<Cod. Componente> OBBLIGATORIO NUMERICO
          'SCCDIS' : f.codice_disegno, //<Cd. Disegno> OBBLIGATORIO NUMERICO
          'SCLEA2' : f.leader, //<Leader> OBBLIGATORIO TESTO.
          'SCDESP' : null,//globals.utils_dateFormatAS400(data_ultimo_esp), //data ultimo esponente.
          'SCMATR' : f.matricola, //<Matricola AOMAT> NON OBBLIGATORIO NUMERICO dal 12 giugno (sviluppo previsto)
		  'SCFUNZ' : f.gruppo_funzionale, //<Gruppo Funzionale> OBBLIGATORIO
		  'SCSUGR' : f.sottogruppo_funzionale, //<Sottogruppo funzionale> OBBLIGATORIO
		  'SCCOMO' : f.omologazione_sicurezza, //Codice Omologazione
          'SCCDIF' : f.codice_difetto, //<Cd Difetto> OBBLIGATORIO
          'SCFLGC' : f.flag_componente_specifico, //flag componente specifico.
          'SCPERC' : "", //percentuale difetti
          'SCKPER' : f.km_percorsi, //<Km Percorsi> NON OBBLIGATORIO (Viene settato a 0 dal servizio).
          'SCDEMI' : globals.utils_dateFormatAS400(f.data_prevista_chiusura_sc_anom), //data prevista chiusura scheda
          'SCDLEA' : globals.utils_dateFormatAS400(f.data_assegnazione_leader), //data assegnazione leader
          'SCLEAD' : f.segnalato_da, //<Segnalato da> OBBLIGATORIO (Servoy dovrebbe metterlo in automatico in base all'utenza).
          'SCRESP' : f.responsabile, //Responsabile Team
          'SCCFOR' : "", //Codice fornitore
          'SCDRIC' : globals.utils_dateFormatAS400(f.data_prev_cons_pz_mod), //Data prevista componente modificato
          'SCDICS' : f.tipo_anomalia, //<Tipo anomalia> OBBLIGATORIO.
          'SCDICP' : f.punteggio_demerito, //<Demerito ICP> OBBLIGATORIO.
          'SCCAUS' : f.causale_modifica, // Causale
          'SCTIPA' : f.tipo_approvazione, //Tipo approvazione
          'SCDATI' : globals.utils_dateFormatAS400(f.timestamp_creazione || new Date()), //<Data Inser.> NON OBBLIGATORIO, MA CONSIGLIATO, ARRIVA DA SERVOY
          'SCDATV' : globals.utils_dateFormatAS400(f.timestamp_modifica || new Date()), //<Data Variaz.> NON OBBLIGATORIO
          'SCTCOM' : f.tipo_componente, //<Tipo componente> OBBLIGATORIO, assunme i valori F (Fisico) e V (Virtuale).
          'SCESPO' : f.esponente, //<Esponente> OBBLIGATORIO se 'Tipo componente' è 'V'
          'SCREVI' : f.revisione, //<Revisione> OBBLIGATORIO se 'Tipo componente' è 'V'
          'SCCANO' : f.attribuzione, //<Causale Anomalia> o <Classe Anomalia> OBBLIGATORIO.
          'SCDINT' : globals.utils_dateFormatAS400(f.data_introduzione), //Data introduzione OBBLIGATORIO in caso di chiusura SKA di tipo PROCESSO/FORNITURA
          'SCNASS' : f.numero_assembly //Numero assembli
	}
	return data;
}

/**
 *
 * @properties={typeid:24,uuid:"836CA809-B8B9-4D82-A9F6-9A10D4038E11"}
 */
function packDataAndUser(pack)
{
	var data = packData(pack);
	data["SACHIA"] = 'SERVOY';
	var userInfo = globals.vc2_getUserInfo(null);
	if (userInfo && userInfo.user_as400){
		if(data["SCNREG"]){
			utente_modifica = userInfo.user_as400;
		}else{
			utente_creazione = userInfo.user_as400
		}
		data["SCNOMI"] = userInfo.user_as400;
	}
	return data;
}

/**
 *
 * @properties={typeid:24,uuid:"D7397A5F-2A9A-4D13-B953-C0B5C833F1B7"}
 */
function sendToService()
{
	if (activateWebServices == 0) {
		var regNum = transmitData(foundset)
		if (regNum && regNum > 0){
			numero_scheda = regNum;
			return 1;
		}else{
			return -1;
		}
	}else{
		return 1;
	}
}

/**
 * TODO generated, please specify type and doc for the params
 * @param f
 *
 * @properties={typeid:24,uuid:"BE965DBF-C2A5-4C6F-90E0-05D87C532D92"}
 */
function transmitData(f) {
	var data = packDataAndUser(f);
	//FS
	//dep	var p = plugins.http.getPoster(globals.vc2_serviceLocationSk, "scheda_anomalia");
	var client = plugins.http.createNewHttpClient();
	var postReq = client.createPostRequest(globals.vc2_serviceLocationSk);
	//dep	p.setCharset("ISO-8859-1");
	postReq.setCharset("ISO-8859-1");
//dep	for (var s in data) {
		for (var _s in data) {
		//dep	p.addParameter(s, data[s]);
		postReq.addHeader(_s, data[_s]);
	}
	//dep	var code = p.doPost();
	var res = postReq.executeRequest();
	/** @type {String} */
	var response = null;
	
	//dep	if (code == 200) {
	if (res!=null && res.getStatusCode() == 200) {
		//dep	response = globals.vc2_parseResponse(foundset, p.getPageData(), "SCNREG_OUT", ["SCCENT", "SCMODE", "SCVERS", "SCENTE", "SCLEA2", "SCLEAD", "SCRESP", "SCNOMI", "SCNOMV"]);
		response = globals.vc2_parseResponse(foundset, res.getResponseBody(), "SCNREG_OUT", ["SCCENT", "SCMODE", "SCVERS", "SCENTE", "SCLEA2", "SCLEAD", "SCRESP", "SCNOMI", "SCNOMV"]);

	} else {
		// Codice Errore WSNET1: errore nella connessione di rete il servizio non ha risposto 200.
		plugins.dialogs.showErrorDialog("Dati segnalazione non trasmessi", "Il servizio AS400 non è al momento disponibile. (WSNET1)", "Ok");
		globals.vc2_lastErrorLog = "Codice: " + res + " - Il servizio AS400 non è al momento disponibile. (WSNET1)";
		response = -1;
	}
	//Inizio LOG
	var table = databaseManager.getDataSourceTableName(controller.getDataSource());
	var table_key = k8_anomalie_id;
	//FS
	var logic_key = (response != -1) ? ente + " " + utils.stringToNumber(response) : ente;
	globals.vc2_logTransactionToAS400(table, table_key, logic_key, data, globals.vc2_lastErrorLog, response, globals.vc2_getLogOperation());
	//Fine LOG
	return response;
}

/**
 * @properties={typeid:24,uuid:"DD5484EC-AE22-4D9C-A9B5-ABD3D8700792"}
 */
function openDescriptionSearch(event) {
	var _f = controller.getName();
	var _cf = {p: "codice_modello",
	           v: "codice_versione",
	           s: "stato_anomalia",
	           d: "k8_schede_anomalie_to_k8_distinte_progetto_core.descrizione"};
	
	forms._vc2_description_searchEngine.openPopup(_f,_cf);
}
