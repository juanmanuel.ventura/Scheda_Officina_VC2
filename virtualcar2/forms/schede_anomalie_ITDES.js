/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"BC3D0F26-E690-4EC7-8B24-EE0AEB66A773",variableType:4}
 */
var activateWebServices = 0;

/**
 * @properties={typeid:35,uuid:"A996452D-B092-4226-BACA-38996F95A203",variableType:-4}
 */
var nfx_related = ["anomalie_vetture_table","schede_anomalie_descrizione_record","schede_anomalie_analisi_record","schede_anomalie_ITDES_dettaglio_record", "schede_anomalie_provvedimenti_record", "documenti_record", "schede_modifica_list", "time_line_table", "tag_table", "anomalie_disegni_table", "anomalie_responsabili_table", "anomalie_requisiti_table"];

/**
 *
 * @properties={typeid:24,uuid:"06BC6CDD-A9F9-40B5-840F-F792030532EC"}
 */
function chiusuraScheda()
{
	if (plugins.dialogs.showInfoDialog("Cambio stato della scheda",
			"Questa scheda verra' chiusa e non sara' possibile modificarla, continuare?",
			"Si","No") == "No"){
		return;
	}else{
		if (nfx_validate() == -1){
			return;
		}
		data_chiusura_scheda = new Date();
		stato_anomalia = "CHIUSA";
		forms.nfx_interfaccia_pannello_buttons.editSave(controller.getName());
	}
}

/**
 *
 * @properties={typeid:24,uuid:"7A65B087-2F97-42B5-9587-36C3D1825421"}
 */
function nfx_customEnableButtons()
{
   forms.nfx_interfaccia_pannello_buttons.elements.xexport.enabled = false;
}

/**
 *
 * @properties={typeid:24,uuid:"0845D023-C287-4468-A3DC-4625EB74E4A0"}
 */
function nfx_defineAccess()
{
	return [false,false,false, true];
}

/**
 *
 * @properties={typeid:24,uuid:"6BC7C557-7798-4EA9-96EB-D0F9B8380D68"}
 */
function nfx_excelExport(){
	var toReturn = new Array();
	toReturn.push({label:	"Descrizione",		field:	"k8_descrizione",	format:	null});
	toReturn.push({label:	"Analisi",			field:	"k8_analisi",		format:	null});
	toReturn.push({label:	"Provvedimenti",	field:	"k8_provvedimento",	format:	null});
	return toReturn;
}

/**
 *
 * @properties={typeid:24,uuid:"F8F2CF84-52F2-4948-B6F1-C39E49392BDD"}
 */
function nfx_postValidate()
{
	if (sendToService() < 0 && !globals.vc2_persistUnacceptedRecord(foundset,
			"mancato_invio_anomalia",
			"numero_scheda")){
		return -1;		
	}
}

/**
 *
 * @properties={typeid:24,uuid:"357FAC18-C1FB-4CAE-B17F-187C1A82C0A9"}
 */
function nfx_validate()
{
	if (!stato_anomalia){
		stato_anomalia = "APERTA";
	}
	
	if (!data_registrazione_scheda){
		data_registrazione_scheda = new Date();
	}
		
	//var tableName = controller.getTableName();
	var tableName = databaseManager.getDataSourceTableName(controller.getDataSource());
	var missing = [];
	
	var allFields = {"data_rilevazione" : "Data rilevazione",
				"codice_versione" : "Codice versione",
				"codice_modello" : "Codice Modello",
				"gruppo_funzionale": "Gruppo funzionale",
				"punteggio_demerito": "Demerito",
				"codice_difetto": "Codice difetto",
				"ente": "Ente",
				"responsabile": "Responsabile",
				"segnalato_da": "Segnalatore",
				"codice_disegno" : "Codice disegno",
				"leader": "Leader"
				};
	
	for (var f in allFields){
		if (!foundset[f]){
			missing.push(f);
		}
	}
	
	if (missing.length > 0){
		var message = "Compilare i campi obbligatori:\n\n";
		var i = 1;
		for each(var mf in missing){
			message += i++ + ". Inserire " + allFields[mf] + "\n";
		}
		plugins.dialogs.showErrorDialog("Errori nella compilazione",
			message,"Ok");
		return -1;
	}else{
		return 0;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"3170625D-A97B-472D-859B-BC059FEA82DB"}
 */
function onChangeCriticita()
{
	try{
	if (indice_criticita == "Rosso"){
		elements.indice_criticita.bgcolor = elements.indice_criticita.fgcolor = "#FF0000";
	}else if (indice_criticita == "Verde"){
		elements.indice_criticita.bgcolor = elements.indice_criticita.fgcolor = "#00FF00";
	}else if (indice_criticita == "Giallo"){
		elements.indice_criticita.bgcolor = elements.indice_criticita.fgcolor = "#FFFF00";
	}else{
		indice_criticita = "";
		elements.indice_criticita.bgcolor = elements.indice_criticita.fgcolor = "#FFFFFF";
	}
	}catch(e){
		return;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"C13AC48E-980E-4AE4-BDC2-3D158517677E"}
 */
function onClickCriticita()
{
	if ((forms.nfx_interfaccia_pannello_buttons.mode == 'edit' || 
			forms.nfx_interfaccia_pannello_buttons.mode == 'add') && 
			forms.nfx_interfaccia_pannello_buttons.workingForm == controller.getName()){
	
//		var items = [plugins.popupmenu.createMenuItem("Rosso",toRed),
//		             plugins.popupmenu.createMenuItem("Giallo",toYellow),
//		             plugins.popupmenu.createMenuItem("Verde",toGreen)]
//		plugins.popupmenu.showPopupMenu(elements.indice_criticita_l, items);
		
		var menu = plugins.window.createPopupMenu();
		menu.addMenuItem('Rosso', toRed);
		menu.addMenuItem('Giallo', toYellow);
		menu.addMenuItem('Verder', toGreen);
		menu.show(elements[indice_criticita]);
		
	}
}

/**
 *
 * @properties={typeid:24,uuid:"C221E672-7A8B-4446-AA5E-631F4E1A83B4"}
 */
function packData(found)
{
	var f = found || foundset;
		var data = {
		  'SCCTIP' : 'T', //OBBLIGATORIO La Testata
		  'SCVEEX' : globals.vc2_webServiceMode, //OBBLIGATORIO Sempre esecuzione
          'SCOP'   : "I", //<INSERIMENTO/MODIFICA/CANCELLAZIONE> OBBLIGATORIO
          "SCAPPL" : "VIRTCAR2",
          'SCCENT' : f.ente, //<Cd Ente> OBBIGATORIO
          'SCNREG' : f.numero_scheda, //<N Registrazione evento> OBBLIGATORIO IN MODIFICA.
          'SCDREG' : globals.utils_dateFormatAS400(f.data_registrazione_scheda), //<Dt Reg.Evento> OBBLIGATORIO. YYYY MM DD
          'SCDRIL' : globals.utils_dateFormatAS400(f.data_rilevazione), //<Dt Rilevazione> OBBLIGATORIO
          'SCMODE' : f.codice_modello, //<Modello> OBBLIGATORIO.
		  'SCVERS' : f.codice_versione, //<Versione> OBBLIGARIO
		  'SCPROV' : globals.utils_stateFormatAS400(f.stato_anomalia), //<Stato Provvedim.> OBBLIGATORIO. Chiarire Funzione. Un carattere. 
		  'SCENTE' : f.ente_segnalazione_anomalia,
          'SCNSCH' : f.numero_segnalazione,
          'SCDAVA' : "", //Data ultimo av...anzamento?
          'SCCOMP' : f.componente_vettura, //<Cod. Componente> OBBLIGATORIO NUMERICO
          'SCCDIS' : f.codice_disegno, //<Cd. Disegno> OBBLIGATORIO NUMERICO
          'SCLEA2' : f.leader, //<Leader> OBBLIGATORIO TESTO.
          'SCDESP' : null,//globals.utils_dateFormatAS400(data_ultimo_esp), //data ultimo esponente.
          'SCMATR' : f.matricola, //<Matricola AOMAT> NON OBBLIGATORIO NUMERICO dal 12 giugno (sviluppo previsto)
		  'SCFUNZ' : f.gruppo_funzionale, //<Gruppo Funzionale> OBBLIGATORIO
		  'SCSUGR' : f.sottogruppo_funzionale, //<Sottogruppo funzionale> OBBLIGATORIO
		  'SCCOMO' : f.omologazione_sicurezza, //Codice Omologazione
          'SCCDIF' : f.codice_difetto, //<Cd Difetto> OBBLIGATORIO
          'SCFLGC' : f.flag_componente_specifico, //flag componente specifico.
          'SCPERC' : "", //percentuale difetti
          'SCKPER' : f.km_percorsi, //<Km Percorsi> NON OBBLIGATORIO (Viene settato a 0 dal servizio).
          'SCDEMI' : globals.utils_dateFormatAS400(f.data_prop_em_sk_mod), //data proposta emisssione scheda modifica
          'SCDLEA' : globals.utils_dateFormatAS400(f.data_assegnazione_leader), //data assegnazione leader
          'SCLEAD' : f.segnalato_da, //<Segnalato da> OBBLIGATORIO (Servoy dovrebbe metterlo in automatico in base all'utenza).
          'SCRESP' : f.responsabile, //Responsabile Team
          'SCCFOR' : "", //Codice fornitore
          'SCDRIC' : globals.utils_dateFormatAS400(f.data_prev_cons_pz_mod), //Data prevista componente modificato
          'SCDICS' : "", //<Demerito ICS> NOTA: definire di cosa si tratta. OBBLIGATORIO.
          'SCDICP' : f.punteggio_demerito, //<Demerito ICP> OBBLIGATORIO.
          'SCCAUS' : f.causale_modifica, // Causale
          'SCTIPA' : f.tipo_approvazione, //Tipo approvazione
          'SCNOMI' : "Servoy", //<Utente Inser.>  NON OBBLIGATORIO, MA CONSIGLIATO in fase di test sara' SERVOY, poi ci sara' la mappatura.
          'SCDATI' : globals.utils_dateFormatAS400(f.timestamp_creazione || new Date()), //<Data Inser.> NON OBBLIGATORIO, MA CONSIGLIATO, ARRIVA DA SERVOY
          'SCNOMV' : "Servoy",//<Utente Variaz.> NON OBBLIGATORIO, MA CONSIGLIATO in fase di test sara' SERVOY, poi ci sara' la mappatura.
          'SCDATV' : globals.utils_dateFormatAS400(f.timestamp_modifica || new Date()) //<Data Variaz.> NON OBBLIGATORIO
	}
	return data;
}

/**
 *
 * @properties={typeid:24,uuid:"8738FA96-D133-4573-A23E-7FD540755AF0"}
 */
function sendToService()
{
	if (activateWebServices == 0) {
		var regNum = transmitData(foundset)
		if (regNum && regNum > 0){
			numero_scheda = regNum;
		}else{
			return -1;
		}
	}else{
		return 1;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"EAC61840-7FF8-4C65-844E-4CB3DCB7BFF3"}
 */
function toGreen()
{
	indice_criticita = "Verde";
	onChangeCriticita();
}

/**
 *
 * @properties={typeid:24,uuid:"B8C78A24-2C17-48D0-8B04-D028CE399121"}
 */
function toRed()
{
	indice_criticita = "Rosso"
	onChangeCriticita();
}

/**
 *
 * @properties={typeid:24,uuid:"7F697E84-D5C5-4368-8216-BE84C26C132C"}
 */
function toYellow()
{
	indice_criticita = "Giallo";
	onChangeCriticita();
}

/**
 *@param {JSFoundSet} f 
 * @properties={typeid:24,uuid:"909B29EF-3555-48E8-B185-CD83705E5291"}
 */
function transmitData(f) {
	var data = packData(null);
	//dep	var p = plugins.http.getPoster(globals.vc2_serviceLocationSk,"scheda_anomalia");
	application.output("globals.vc2_serviceLocationSk è  "+globals.vc2_serviceLocationSk);
	var client = plugins.http.createNewHttpClient();
	var postReq = client.createPostRequest(globals.vc2_serviceLocationSk);
	//dep	var p = plugins.http.getPoster("http://f400.unix.ferlan.it:8081/Attorney/schede","schede_testata");
	//dep  	p.setCharset("ISO-8859-1");
	postReq.setCharset("ISO-8859-1");
	for (var _s in data) {
		//dep	p.addParameter(s, data[s]);
	postReq.addParameter(_s, data[_s]);
	}
	//dep	var code = p.doPost();
	var res = postReq.executeRequest();
	application.output("Risposta del server : "+res);
	//dep if(code ==200){
	if (res!=null && res.getStatusCode() == 200) {
		//dep  return globals.vc2_parseResponse(foundset, p.getPageData(), "SCNREG_OUT",null);
		return globals.vc2_parseResponse(foundset, res.getResponseBody(), "SCNREG_OUT", null);
	} else {
		// Codice Errore WSNET1: errore nella connessione di rete il servizio non ha risposto 200.
		plugins.dialogs.showErrorDialog("Dati segnalazione non trasmessi", "Il servizio AS400 non e' al momento disponibile. (WSNET1)", "Ok");
		return -1;
	}
}
