/**
 *
 * @properties={typeid:24,uuid:"B45C1E2A-56B1-4DF2-9FDA-46C1437A12AF"}
 */
function nfx_getTitle(){
	return "ITDES - Schede Anomalie";
}

/**
 * @properties={typeid:24,uuid:"4F2C9C35-B524-4D66-AB64-955AF5D269A1"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"1ADE065E-58AB-486C-8EA6-6EEA1C55E171"}
 */
function nfx_defaultPermissions(){
	return "^";
}
