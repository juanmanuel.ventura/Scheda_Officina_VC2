/**
 * @properties={typeid:35,uuid:"8F8A4AE3-1FDE-4E7A-B3B2-E29511891D7B",variableType:-4}
 */
var nfx_related = ["schede_anomalie_descrizione_admin_record","schede_anomalie_analisi_admin_record","schede_anomalie_dettaglio_record", "schede_anomalie_provvedimenti_admin_record", "documenti_record", "schede_modifica_list", "time_line_table", "tag_table", "anomalie_disegni_table", "anomalie_responsabili_table", "anomalie_requisiti_table"];

/**
 *
 * @properties={typeid:24,uuid:"527B139C-4AE6-40D8-AA88-C8AE006B74BE"}
 */
function nfx_defineAccess(){
	return [true,true,true];
}

/**
 *
 * @properties={typeid:24,uuid:"06FC07E7-F9C2-4F10-AB44-CF4BB14299B8"}
 */
function nfx_getTitle(){
	return "Schede Anomalie - Tabella => ADMIN";
}

/**
 *
 * @properties={typeid:24,uuid:"10A82656-5BC5-4EEE-8139-0410EC0613CA"}
 */
function nfx_isProgram(){
		return true;
}

/**
 * @properties={typeid:24,uuid:"4AEEC456-0ACC-44B9-9303-2CD8918D148A"}
 */
function nfx_defaultPermissions(){
	return "^";
}
