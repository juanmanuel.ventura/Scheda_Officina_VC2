/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"AC1D88B4-F528-4067-BEC9-6B44D674EF87",variableType:4}
 */
var activateWebServices = 0;

/**
 *
 * @properties={typeid:24,uuid:"5F1B5EB3-4994-457C-98E8-35EBCFD01309"}
 */
function nfx_defineAccess(){
	return [true,false,false];
}

/**
 * @properties={typeid:24,uuid:"42CB852A-F594-4500-BAD7-CD555A871296"}
 */
function nfx_postAdd(){
	return sendToAS400();
}

/**
 * @properties={typeid:24,uuid:"2FD4EDF5-049E-4DAE-A8E9-364F4FAB18D2"}
 */
function nfx_postEdit(){
	return sendToAS400();
}

/**
 * @properties={typeid:24,uuid:"3A26C49D-937F-4A63-94C2-AB62A858C955"}
 */
function sendToAS400(){
	if (sendToService() < 0 && !globals.vc2_persistUnacceptedRecord(foundset,null,null)){
		return -1;
	}
	return 1;
}

/**
 *
 * @properties={typeid:24,uuid:"901812AD-A02C-4060-9469-8A7269E7A5AF"}
 */
function packData(){
	var n_SKA = numero_scheda || globals.lastSKAfromSA;
	var dateAndDesc = globals.utils_dateTextFormatAS400(controller.getName());
	var datad = {
	    "SCCTIP" : "P", 
		"SCVEEX" : globals.vc2_webServiceMode, 
		"SCOP" : "I", 
		"SCAPPL" : "VIRTCAR2",
		"SCCENT": ente, 
		"SCNREG": n_SKA,
		"SCDAT3" : dateAndDesc.date,
        "SCDES3" : dateAndDesc.text
	}
	return datad;
}

/**
 *
 * @properties={typeid:24,uuid:"699B32D6-1FA2-4071-B8A2-7ACC2E2630D3"}
 */
function packDataAndUser(){
	var data = packData();
	data["SACHIA"] = 'SERVOY';
	var userInfo = globals.vc2_getUserInfo(null);
	if (userInfo && userInfo.user_as400){
		utente_modifica = data["SCNOMI"] = userInfo.user_as400;
		//utente_creazione = data["SANOMI"] = k8_anomalie_anal_full_to_k8_schede_anomalie.utente_creazione;
	}
	return data;
}

/**
 *
 * @properties={typeid:24,uuid:"5C50914E-864E-41B5-A580-A232C19B16BC"}
 */
function sendToService(){
	if (activateWebServices == 0) {
		var regNum = transmitData(foundset)
		if (!regNum || regNum < 0){
			return -1;
		}
	}else{
		return 1;
	}
	return 1;
}

/**
 *@param {JSFoundSet} f //aggiunto parametro formale
 * @properties={typeid:24,uuid:"E599EAD0-4F22-4B04-90A9-B0BFAF9F214D"}
 */
function transmitData(f){
	//TODO Testare in PRO/PRE la chiamata al web service
	var data = packDataAndUser();	
	//dep var p = plugins.http.getPoster(globals.vc2_serviceLocationSk,"analisi_scheda_anomalia");
	var client = plugins.http.createNewHttpClient();
	var postReq=client.createPostRequest(globals.vc2_serviceLocationSk);
	
	//dep p.setCharset("ISO-8859-1");
	postReq.setCharset("ISO-8859-1");

	for (var s in data){
		//dep p.addParameter(s, data[s]);
		postReq.addHeader(s,data[s])
	}
	//dep var code = p.doPost();
	var res = postReq.executeRequest();
	/** @type {String} */	
	var response = null;
	//dep	if (code == 200){
	if (res!=null && res.getStatusCode() == 200){
		//dep response = globals.vc2_parseResponse(foundset, p.getPageData(), "SCNREG_OUT", ["SCDES3"]);
		response=globals.vc2_parseResponse(foundset, res.getResponseBody(),"SCNREG_OUT",["SCDES3"]);
	}else{
		// Codice Errore WSNET1: errore nella connessione di rete il servizio non ha risposto 200.
		plugins.dialogs.showErrorDialog("Dati segnalazione non trasmessi","Il servizio AS400 non e' al momento disponibile. (WSNET1)","Ok");
		globals.vc2_lastErrorLog = "Codice: " + res + " - Il servizio AS400 non e' al momento disponibile. (WSNET1)";
		response = -1;
	}
	//Inizio LOG
	var table = databaseManager.getDataSourceTableName(controller.getDataSource());
	var table_key = k8_anomalie_anal_full_id;
	var logic_key = (response != -1) ? ente + " " + utils.stringToNumber(response) : ente;
	globals.vc2_logTransactionToAS400(table,table_key,logic_key,data,globals.vc2_lastErrorLog,response,globals.vc2_getLogOperation());
	//Fine LOG
	return response;
}
