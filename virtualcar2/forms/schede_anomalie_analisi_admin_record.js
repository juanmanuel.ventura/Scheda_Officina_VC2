/**
 *
 * @properties={typeid:24,uuid:"111D1647-E45E-4EF8-8E7B-602B19116A05"}
 */
function nfx_defineAccess(){
	return [true,true,true];
}

/**
 *
 * @properties={typeid:24,uuid:"2D908539-3461-4B5C-A0C0-622ACA501C1A"}
 */
function nfx_getTitle(){
	return "Analisi Schede (Admin)"
}

/**
 *
 * @properties={typeid:24,uuid:"F111DF06-1FB5-44EF-B54D-2DCF1617DF61"}
 */
function nfx_isProgram(){
	return false;
}
