/**
 *
 * @properties={typeid:24,uuid:"7A43946A-C8D9-4B8C-A07D-CE11323FFCB2"}
 */
function nfx_getTitle(){
	return "Schede analisi non inviate";
}

/**
 *
 * @properties={typeid:24,uuid:"957AA6FC-28B0-4FBF-950B-F0317EBDE500"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"47A96E2D-D796-4EF0-A0ED-EBB79D6D2757"}
 */
function nfx_defaultPermissions(){
	return "^";
}

/**
 *
 * @properties={typeid:24,uuid:"4E422E68-C624-4E3D-9D76-9FDEC4A9D633"}
 */
function nfx_sks(){
	return [{icon: "ripristina.png", tooltip:"Ripristina", method:"ripristina"}];
}

/**
 *
 * @properties={typeid:24,uuid:"CE45175C-2AD7-4C32-B6A6-44B4262445DF"}
 */
function ripristina(){
	globals.vc2_ripristinaNonInviate(foundset,controller.getName());
}
