/**
 * @properties={typeid:35,uuid:"741F7912-94EA-4584-ABF3-7E848B808026",variableType:-4}
 */
var _canClose = null;

/**
 * @type {String}
 * @properties={typeid:35,uuid:"0DB3E7D1-539E-40C7-8DF7-20BA4CC36E69"}
 */
var _message = null;

/**
 * @properties={typeid:24,uuid:"1C5BFD0C-2C26-49C0-98A7-5940FCD3BB27"}
 */
function openPopup(){
	_canClose = -1;
	_message = "";
	controller.show("showClosingData");
}

/**
 * @properties={typeid:24,uuid:"976FC29D-84BD-406D-A43B-7809D3C6D6E3"}
 */
function close(event){
	if(data_introduzione){
		_canClose = 1;
		_message = "";
		
		//application.closeFormDialog();
		
		var w=controller.getWindow();
		w.destroy();
	}else{
		_canClose = -1;
		_message = "Per poter procedere è\nnecessario compilare il\ncampo \"Data introduzione\".";
	}
}
