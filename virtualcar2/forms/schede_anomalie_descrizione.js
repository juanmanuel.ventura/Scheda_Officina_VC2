/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"2A1D4D45-B050-427C-B072-9783078A82C0",variableType:4}
 */
var activateWebServices = 0;

/**
 *
 * @properties={typeid:24,uuid:"6DCA4C43-D594-4BE8-9282-9F5DEE64BB7B"}
 */
function nfx_defineAccess(){
	return [true,false,false];
}

/**
 * @properties={typeid:24,uuid:"8271CB89-6E56-4E10-B6F2-D62F99219B94"}
 */
function nfx_postAdd(){
	return sendToAS400();
}

/**
 * @properties={typeid:24,uuid:"8F60DC92-676F-4C77-8586-6355BE0D5827"}
 */
function nfx_postEdit(){
	return sendToAS400();
}

/**
 * @properties={typeid:24,uuid:"8CB08603-48D2-4239-9070-50C4384B3E7A"}
 */
function sendToAS400(){
	if (sendToService() < 0 && !globals.vc2_persistUnacceptedRecord(foundset,null,null)){
		return -1;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"F1A15F4B-CB2C-4A67-84DB-1D8086EA8B72"}
 */
function packData(){
	var n_SKA = numero_scheda || globals.lastSKAfromSA;
	return {
	    "SCCTIP" : "A", //OBBLIGATORIO La Testata
		"SCVEEX" : globals.vc2_webServiceMode, //OBBLIGATORIO Sempre esecuzione
		"SCOP" : "I", //<INSERIMENTO/MODIFICA/CANCELLAZIONE> OBBLIGATORIO
		"SCAPPL" : "VIRTCAR2",
		"SCCENT" : ente, //<Cd Ente> OBBIGATORIO
		"SCNREG" : n_SKA,
        "SCDES2" : globals.utils_mergeText(controller.getName(),"testo",50)
	}
}

/**
 *
 * @properties={typeid:24,uuid:"C6424C0E-64DF-41C4-A1B2-4EC7063BA479"}
 */
function packDataAndUser()
{
	var data = packData();
	data["SACHIA"] = 'SERVOY';
	var userInfo = globals.vc2_getUserInfo(null);
	if (userInfo && userInfo.user_as400){
		utente_modifica = data["SCNOMI"] = userInfo.user_as400;
		//utente_creazione = data["SANOMI"] = k8_anomalie_desc_full_to_k8_schede_anomalie.utente_creazione;
	}
	return data;
}

/**
 *
 * @properties={typeid:24,uuid:"F8B1B627-F1E5-4E04-8322-F1E33DA7D4EC"}
 */
function sendToService()
{
	if (activateWebServices == 0) {
		var regNum = transmitData(foundset)
		if (!regNum || regNum < 0){
			return -1;
		}
	}else{
		return 1;
	}
}

/**
 *@param {JSFoundSet} f
 * @properties={typeid:24,uuid:"24F50BEF-5996-4A30-957B-F05F3675E776"}
 */
function transmitData(f){
//dep	var data = packDataAndUser();
	var _data = packDataAndUser();

	//dep var p = plugins.http.getPoster(globals.vc2_serviceLocationSk,"descrizione_scheda_anomalia");
	var client = plugins.http.createNewHttpClient();
	var postReq=client.createPostRequest(globals.vc2_serviceLocationSk);
	//dep p.setCharset("ISO-8859-1");
	postReq.setCharset("ISO-8859-1");
	for (var s in _data){
		//dep p.addParameter(s, data[s]);
		postReq.addHeader(s,_data[s])
	}
	//dep var code = p.doPost();
	var res = postReq.executeRequest();
/** @type {String} */
	var response = null;
	if (res!=null && res.getStatusCode() == 200){
		//dep response = globals.vc2_parseResponse(foundset, p.getPageData(), "SCNREG_OUT", ["SCDES2"]);
		response = globals.vc2_parseResponse(foundset, res.getResponseBody(), "SCNREG_OUT", ["SCDES2"]);

	}else{
		// Codice Errore WSNET1: errore nella connessione di rete il servizio non ha risposto 200.
		plugins.dialogs.showErrorDialog("Dati segnalazione non trasmessi","Il servizio AS400 non e' al momento disponibile. (WSNET1)","Ok");
		globals.vc2_lastErrorLog = "Codice: " + res + " - Il servizio AS400 non e' al momento disponibile. (WSNET1)";
		response = -1;
	}
	//Inizio LOG
	var table = databaseManager.getDataSourceTableName(controller.getDataSource());
	var table_key = k8_anomalie_desc_full_id;
	var logic_key = (response != -1) ? ente + " " + utils.stringToNumber(response) : ente;
	globals.vc2_logTransactionToAS400(table,table_key,logic_key,_data,globals.vc2_lastErrorLog,response,globals.vc2_getLogOperation());
	//Fine LOG
	return response;
}
