/**
 *
 * @properties={typeid:24,uuid:"AC060977-486E-4FF0-A1F8-1DF7563782C1"}
 */
function nfx_defineAccess(){
	return [true,true,true];
}

/**
 *
 * @properties={typeid:24,uuid:"1AD74F15-E898-4EC9-9840-A155801CA468"}
 */
function nfx_getTitle(){
	return "Descrizione Schede (Admin)";
}

/**
 *
 * @properties={typeid:24,uuid:"C899BD86-3D66-48D6-83AA-C611C83E9ADD"}
 */
function nfx_isProgram(){
	return false;
}
