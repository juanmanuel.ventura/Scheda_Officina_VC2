/**
 *
 * @properties={typeid:24,uuid:"81CFF229-A2C3-486C-9B09-6FCCD9EEA744"}
 */
function nfx_getTitle(){
	return "Schede descrizioni non inviate";
}

/**
 *
 * @properties={typeid:24,uuid:"88F19FB7-72AA-4CAC-A10B-3AFC35DC95A1"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"8AEC0466-E179-4813-A41A-EA0164CCCF92"}
 */
function nfx_defaultPermissions(){
	return "^";
}

/**
 *
 * @properties={typeid:24,uuid:"120A9248-FA1D-485F-A756-63B0B0EEB821"}
 */
function nfx_sks(){
	return [{icon: "ripristina.png", tooltip:"Ripristina", method:"ripristina"}];
}

/**
 *
 * @properties={typeid:24,uuid:"924088FB-EB74-4204-93CB-BB4640AB6DD9"}
 */
function ripristina(){
	globals.vc2_ripristinaNonInviate(foundset,controller.getName());
}
