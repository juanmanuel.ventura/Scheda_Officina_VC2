/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"29576203-C60C-4FAF-B6FF-53DFDD56F655",variableType:4}
 */
var activateWebServices = 0;

/**
 *
 * @properties={typeid:24,uuid:"092F21FA-3236-4E9E-8363-3382687731A1"}
 */
function nfx_defineAccess(){
	return [true,false,false];
}

/**
 * @properties={typeid:24,uuid:"5A7B1882-67E6-482C-9BE1-2D17446E5DFC"}
 */
function nfx_postAdd(){
	return sendToAS400();
}

/**
 * @properties={typeid:24,uuid:"AD3D03A5-A7C4-4DE0-B79A-D699A7A6C895"}
 */
function nfx_postEdit(){
	return sendToAS400();
}

/**
 * @properties={typeid:24,uuid:"BB76C0EA-BED1-492D-92A4-3BE948FE4AAA"}
 */
function sendToAS400(){
	if (sendToService() < 0 && !globals.vc2_persistUnacceptedRecord(foundset,null,null)){
		return -1;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"2C6C0653-E9DE-4D23-8FD2-A610CD4AD422"}
 */
function packData(){
	var n_SKA = numero_scheda || globals.lastSKAfromSA;
	var dateAndDesc = globals.utils_dateTextFormatAS400(controller.getName());
	var data = {
	    "SCCTIP" : "S", 
		"SCVEEX" : globals.vc2_webServiceMode, 
		"SCOP" : "I", 
		"SCAPPL" : "VIRTCAR2",
		"SCCENT": ente, 
		"SCNREG": n_SKA,
		"SCDAT4" : dateAndDesc.date,
        "SCDES4" : dateAndDesc.text
	}
	return data;

}

/**
 *
 * @properties={typeid:24,uuid:"BC5F34FE-D729-4861-B464-4621EB0D0F9C"}
 */
function packDataAndUser()
{
	var data = packData();
	data["SACHIA"] = 'SERVOY';
	var userInfo = globals.vc2_getUserInfo(null);
	if (userInfo && userInfo.user_as400){
		utente_modifica = data["SCNOMI"] = userInfo.user_as400;
		//utente_creazione = data["SANOMI"] = k8_anomalie_provv_full_to_k8_schede_anomalie.utente_creazione;
	}
	return data;
}

/**
 *
 * @properties={typeid:24,uuid:"24FC8CB1-8971-4FCE-8820-E42BAABFEC74"}
 */
function sendToService()
{
	if (activateWebServices == 0) {
		var regNum = transmitData(foundset)
		if (!regNum || regNum < 0){
			return -1;
		}
	}else{
		return 1;
	}

}

/**
 *@param {JSFoundSet} f
 * @properties={typeid:24,uuid:"8BAFD3AB-8F23-4A8E-B444-EE78567012BA"}
 */
function transmitData(f) {
	var datas = packDataAndUser();
	//FS
	//dep	var p = plugins.http.getPoster(globals.vc2_serviceLocationSk,"provvdimenti_scheda_anomalia");
	var client = plugins.http.createNewHttpClient();
	var postReq = client.createPostRequest(globals.vc2_serviceLocationSk);
	//dep	p.setCharset("ISO-8859-1");
	postReq.setCharset("ISO-8859-1");

	for (var s in datas) {
		//dep		p.addParameter(s, datas[s]);
		postReq.addHeader(s, data[s])
	}
	//dep	var code = p.doPost();
	var res = postReq.executeRequest();
	/** @type {String} */
	var response = null;
	//dep	if (code == 200){
	if (res != null){
		if( res.getStatusCode() == 200) {

		//dep	response = globals.vc2_parseResponse(foundset, p.getPageData(), "SCNREG_OUT", ["SCDES4"]);
		response = globals.vc2_parseResponse(foundset, res.getResponseBody(), "SCNREG_OUT", ["SCDES4"]);

	} }else {
		// Codice Errore WSNET1: errore nella connessione di rete il servizio non ha risposto 200.
		plugins.dialogs.showErrorDialog("Dati segnalazione non trasmessi", "Il servizio AS400 non e' al momento disponibile. (WSNET1)", "Ok");
		globals.vc2_lastErrorLog = "Codice: " + res + " - Il servizio AS400 non e' al momento disponibile. (WSNET1)";
		response = -1;
	}

	//Inizio LOG
	var table = databaseManager.getDataSourceTableName(controller.getDataSource());
	var table_key = k8_anomalie_provv_full_id;
	var logic_key = (response != -1) ? ente + " " + utils.stringToNumber(response) : ente;
	globals.vc2_logTransactionToAS400(table, table_key, logic_key, data, globals.vc2_lastErrorLog, response, globals.vc2_getLogOperation());
	//Fine LOG
	return response;
}
