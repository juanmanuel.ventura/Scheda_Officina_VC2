/**
 *
 * @properties={typeid:24,uuid:"4599B627-7045-4AF2-BD0B-95933923AE66"}
 */
function nfx_defineAccess(){
	return [true,true,true];
}

/**
 *
 * @properties={typeid:24,uuid:"9384AC57-7E55-49A2-BDF9-5D02AF3E9B58"}
 */
function nfx_getTitle(){
	return "Provvedimenti (Admin)"
}

/**
 *
 * @properties={typeid:24,uuid:"074054A6-92A0-4C5D-A963-01D2C197C118"}
 */
function nfx_isProgram(){
	return false;
}
