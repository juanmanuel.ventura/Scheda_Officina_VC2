/**
 *
 * @properties={typeid:24,uuid:"BBA2024D-32F6-4E71-8FF6-8DAA18675676"}
 */
function nfx_getTitle(){
	return "Schede provvedimenti non inviati";
}

/**
 *
 * @properties={typeid:24,uuid:"FCF5276A-96B9-485E-98DC-7436EC4E1D39"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"6711B5A4-3E09-4324-83F7-AC3C4618C80B"}
 */
function nfx_defaultPermissions(){
	return "^";
}

/**
 *
 * @properties={typeid:24,uuid:"CF5827D5-72C2-416A-A291-E2544B132368"}
 */
function nfx_sks(){
	return [{icon: "ripristina.png", tooltip:"Ripristina", method:"ripristina"}];
}

/**
 *
 * @properties={typeid:24,uuid:"F72B10D5-3ED6-49E4-8FFE-2292B2D611F0"}
 */
function ripristina(){
	globals.vc2_ripristinaNonInviate(foundset,controller.getName());
}
