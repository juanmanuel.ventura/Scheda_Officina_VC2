/**
 * @properties={typeid:35,uuid:"48A97984-322B-4210-B4CC-AA0CAAC26F27",variableType:-4}
 */
var blockedFields = ["ente_segnalazione_anomalia","numero_segnalazione","stato_segnalazione","numero_scheda","stato_anomalia","ente_proposta_modifica","nr_proposta_modifica","stato_scheda","causale_modifica","tipo_approvazione","numero_cid","data_approv_sk_prop_mod","data_emissione","tipo","data_prevista_attuazione","richiesta_assembly","data_introduzione_modifica_1","numero_assembly_1","data_registrazione_scheda","data_assegnazione_leader","complessivo","utente_chiusura_scheda","data_chiusura"];

/**
 * @properties={typeid:35,uuid:"9D7617D9-1937-47D0-B197-5EDACCEB6AD2",variableType:-4}
 */
var disabledFields = ["ente_segnalazione_anomalia","ente_proposta_modifica"];

/**
 * @properties={typeid:35,uuid:"92351F4D-8085-452F-BE50-04B82A0B3879",variableType:-4}
 */
var requiredFields = ["ente","data_rilevazione_scheda","codice_modello","codice_versione","gruppo_funzionale","tipo_anomalia","segnalato_da","leader","responsabile","codice_disegno",/*"componente_vettura",*/"tipo_componente","codice_difetto","punteggio_demerito","attribuzione"];

/**
 * @properties={typeid:24,uuid:"8ADED8DC-7806-4B5B-8F42-42828B38B9EA"}
 */
function nfx_getTitle(){
	return "Schede Anomalie";
}

/**
 * @properties={typeid:24,uuid:"6F0F3D3F-4368-4214-9DD4-6D52603426CF"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"B8181347-3D88-4274-97A5-B0ED82B46DFF"}
 */
function local_preAdd(){
	colorize();
}

/**
 * @properties={typeid:24,uuid:"3577CFD0-73AD-420E-9907-D3D2163AC69B"}
 */
function local_postAdd(){
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"968B6C1A-9F45-4005-9A4E-C500F420EC56"}
 */
function local_postAddOnCancel(){
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"1BBE1EC0-A600-42DC-A778-0128009D4B99"}
 */
function local_preEdit(){
	colorize();
}

/**
 * @properties={typeid:24,uuid:"8A9A29C8-3893-4554-9912-A3CB815DF426"}
 */
function local_postEdit(){
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"A58D9109-A7CD-4C4C-88A6-EBACB620992F"}
 */
function local_postEditOnCancel(){
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"DD2524CD-1B44-4366-895D-17E05D3310E6"}
 */
function colorize(){
	decolorize();
	setRequiredFields();
	globals.utils_colorize(controller.getName(),requiredFields,"#FFFFC4");
	globals.utils_colorize(controller.getName(),blockedFields,"#E0E0E0");
	globals.utils_enable(controller.getName(),disabledFields,false);
}

/**
 * @properties={typeid:24,uuid:"36CEED8F-0E1F-4912-82D2-3D44B66A10CC"}
 */
function decolorize(){
	globals.utils_colorize(controller.getName(),requiredFields.concat(blockedFields),"#FFFFFF");
	globals.utils_enable(controller.getName(),disabledFields,true);
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"D70D5F8B-0CAD-4F83-979C-D87BBABDB6BB"}
 * @AllowToRunInFind
 */
function onSegnalatoreChange(oldValue, newValue, event) {
	var f = forms.gestione_utenze.foundset.duplicateFoundSet();
	if(f.find()){
		f.user_as400 = newValue;
		if(f.search()){
			ente = f.ente_as400;
		}else{
			ente = null;
		}
	}
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"3D5D6911-242B-42F2-BAC2-1545116CECB7"}
 */
function onLeaderChange(oldValue, newValue, event) {
	data_assegnazione_leader = new Date();
}

/**
 * @properties={typeid:24,uuid:"90819C06-52A4-4D53-A766-6596B3377E5D"}
 */
function goToForm(event) {
	var elem = event.getElementName();
	if(elem == "sa" && controller.getName() != "segnalazioni_anomalie_record" && ente_segnalazione_anomalia && numero_segnalazione){
		globals.nfx_goToProgramAndRecord("segnalazioni_anomalie_record",k8_anomalie_id,"k8_anomalie_id");
		return;
	}else if(elem == "ska" && controller.getName() != "schede_anomalie_record" && ente && numero_scheda){
		globals.nfx_goToProgramAndRecord("schede_anomalie_record",k8_anomalie_id,"k8_anomalie_id");
		return;
	}else if(elem == "skm" && k8_schede_anomalie_to_k8_schede_modifica && k8_schede_anomalie_to_k8_schede_modifica.ente_proposta_modifica && k8_schede_anomalie_to_k8_schede_modifica.nr_proposta_modifica){
		globals.nfx_goToProgramAndRecord("schede_modifica_record",k8_schede_anomalie_to_k8_schede_modifica.k8_schede_modifica_id,"k8_schede_modifica_id");
		return;
	}
}

/**
 * @properties={typeid:24,uuid:"5A66C15A-DADE-41DF-9BEC-428C4EFF86D2"}
 */
function copyToClipboard(txt){
	application.setClipboardContent(txt);
}

/**
 * @properties={typeid:24,uuid:"ACAE98B6-2835-4BD4-B919-4F99D151E050"}
 */
function showDescriptionAS400(event) {
	var rel = k8_schede_anomalie_to_k8_anagrafica_disegni;
	var item = null;
	if(rel){
		var txt = rel.descrizione;
		if(txt){
//dep		item = plugins.popupmenu.createMenuItem(txt,copyToClipboard);
//dep		item.setMethodArguments([txt]);
			var menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem(txt,copyToClipboard);
			item.methodArguments = [txt];

		}else{
			//dep item = plugins.popupmenu.createMenuItem("Non presente in anagrafica...",null);
			menu=plugins.window.createPopupMenu();
			item=menu.addMenuItem("Non presente in anagrafica...",null);
		}
		//dep plugins.popupmenu.showPopupMenu(elements.codice_disegno,[item]);
		menu.show(elements.codice_disegno);

	}
}

/**
 * @properties={typeid:24,uuid:"451753B1-9C6A-43B8-91E5-F08F2223FF23"}
 */
function showDescription(event) {
	var rel = k8_schede_anomalie_to_k8_distinte_progetto_core;
	if(rel){
		var txt = rel.descrizione_cpl;
		var item = null;
		if(txt){
//	dep		item = plugins.popupmenu.createMenuItem(txt,copyToClipboard);
//	dep		item.setMethodArguments([txt]);
			var menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem(txt,copyToClipboard);
			item.methodArguments = [txt];
		}else{
			//item = plugins.popupmenu.createMenuItem("Non presente in distinta...",null);
			menu = plugins.window.createPopupMenu();
			item=menu.addMenuItem("Non presente in distinta...",null);
		}
		//dep plugins.popupmenu.showPopupMenu(elements.complessivo,[item]);
		menu.show(elements.complessivo);
	}
}

/**
 * @properties={typeid:24,uuid:"7ADC6CDC-042D-4BCD-B953-827A9856633E"}
 */
function showComponentDescription(event) {
	var rel = k8_schede_anomalie_to_k8_componenti;
	if(rel){
		var txt = rel.descrizione;
		var item = null;
		if(txt){
			
//dep		item = plugins.popupmenu.createMenuItem(txt,copyToClipboard);
//dep		item.setMethodArguments([txt]);
			var menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem(txt,copyToClipboard);
			item.methodArguments = [txt];
		}else{
		 	//dep item = plugins.popupmenu.createMenuItem("Non presente in lista componenti...",null);
			menu = plugins.window.createPopupMenu();
			item=menu.addMenuItem("Non presente in lista componenti...",null);

		}
		//dep plugins.popupmenu.showPopupMenu(elements.componente_vettura,[item]);
		menu.show(elements.componente_vettura);

	}
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"43F408EC-041C-436A-BC5D-E1A0D0987E92"}
 */
function onTipoComponenteChange(oldValue, newValue, event) {
	var mode = forms.nfx_interfaccia_pannello_buttons.mode;
	if(mode == "add" || mode == "edit"){
		colorize();
	}
}

/**
 * @properties={typeid:24,uuid:"57E01899-1F87-4413-8659-6E5CD0F1124F"}
 */
function setRequiredFields(){
	//Impostazione condizionale dei campi obbligatori
	
	//Tipo Componente
	var fields = ["esponente","revisione"];
	if(tipo_componente == "V"){
		for(var i=0;i<fields.length;i++){
			if(requiredFields.indexOf(fields[i]) == -1){
				requiredFields.push(fields[i]);
			}
		}
	}else{
		for(var i=0;i<fields.length;i++){
			if(requiredFields.indexOf(fields[i]) != -1){
				requiredFields.splice(requiredFields.indexOf(fields[i]),1);
			}
		}
	}
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"87DDB799-37B6-4383-BE28-6D873F646FD1"}
 */
function onGruppoFunzionaleChange(oldValue, newValue, event) {
	sottogruppo_funzionale = null;
}

/**
 * @properties={typeid:24,uuid:"75CAF1D9-4BCC-4056-9C58-AB4CC65B3B3F"}
 */
function nextLM(event){
	if(k8_schede_anomalie_to_k8_schede_modifica && k8_schede_anomalie_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica){
		var _l = k8_schede_anomalie_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica;
		_l.setSelectedIndex(_l.getSelectedIndex() + 1);
		databaseManager.recalculate(foundset.getSelectedRecord());
	}
}

/**
 * @properties={typeid:24,uuid:"212FE41B-F76B-43AE-B020-B459BDCD6662"}
 */
function previousLM(event){
	if(k8_schede_anomalie_to_k8_schede_modifica && k8_schede_anomalie_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica){
		var _l = k8_schede_anomalie_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica;
		_l.setSelectedIndex(_l.getSelectedIndex() - 1);
		databaseManager.recalculate(foundset.getSelectedRecord());
	}
}
