/**
 * @properties={typeid:35,uuid:"25BF870B-75D6-4198-8178-D609A7F34836",variableType:-4}
 */
var blockedFields = ["ente_segnalazione_anomalia","numero_segnalazione","stato_segnalazione","numero_scheda","stato_anomalia","ente_proposta_modifica","nr_proposta_modifica","stato_scheda","causale_modifica","tipo_approvazione","numero_cid","data_approv_sk_prop_mod","data_emissione","tipo","data_prevista_attuazione","richiesta_assembly","data_introduzione_modifica_1","numero_assembly_1","data_registrazione_scheda","data_assegnazione_leader","complessivo","utente_chiusura_scheda","data_chiusura"];

/**
 * @properties={typeid:35,uuid:"214061C5-63C7-41E3-993B-5895EDAD2F91",variableType:-4}
 */
var requiredFields = ["ente","data_rilevazione_scheda","codice_modello","codice_versione","gruppo_funzionale","tipo_anomalia","segnalato_da","leader","responsabile","codice_disegno","codice_difetto","punteggio_demerito"];

/**
 * @properties={typeid:24,uuid:"B7C7C427-2CCA-411D-8AC8-B6BEDBD41598"}
 */
function nfx_getTitle(){
	return "Schede Anomalie";
}

/**
 * @properties={typeid:24,uuid:"690E4A08-53A6-444E-B18D-E0E0730FCBFB"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"D4B93996-01EC-40AF-8409-B35B93AE1039"}
 */
function local_preAdd(){
	colorize();
}

/**
 * @properties={typeid:24,uuid:"0CC3DAA3-C8CE-42F4-8003-32F0B57AFA34"}
 */
function local_postAdd(){
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"485914D8-F3D0-4926-9480-C8C210444A35"}
 */
function local_postAddOnCancel(){
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"2399E6DB-897A-4903-BA46-11E4D25003B6"}
 */
function local_preEdit(){
	colorize();
}

/**
 * @properties={typeid:24,uuid:"EF394F14-FC26-4989-903E-A1450E56BF96"}
 */
function local_postEdit(){
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"788F082D-8F10-4604-AB9D-E3203837C768"}
 */
function local_postEditOnCancel(){
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"07D64AF1-1ACD-42E9-962F-62F0193B5493"}
 */
function colorize(){
	globals.utils_colorize(controller.getName(),requiredFields,"#FFFFC4");
	globals.utils_colorize(controller.getName(),blockedFields,"#E0E0E0");
}

/**
 * @properties={typeid:24,uuid:"9EF089B3-1726-4637-964D-B71D4F8FA41B"}
 */
function decolorize(){
	globals.utils_colorize(controller.getName(),requiredFields.concat(blockedFields),"#FFFFFF");
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"1EFA4A27-9502-4A70-969E-DB3769CEBBBA"}
 */
function onLeaderChange(oldValue, newValue, event) {
	data_assegnazione_leader = new Date();
}

/**
 * @properties={typeid:24,uuid:"D3548BBC-CCAC-4BE2-A281-A8EF50B0BF20"}
 */
function goToForm(event) {
	var elem = event.getElementName();
	if(elem == "sa" && controller.getName() != "segnalazioni_anomalie_record" && ente_segnalazione_anomalia && numero_segnalazione){
		globals.nfx_goToProgramAndRecord("segnalazioni_anomalie_record",k8_anomalie_id,"k8_anomalie_id");
		return;
	}else if(elem == "ska" && controller.getName() != "schede_anomalie_record" && ente && numero_scheda){
		globals.nfx_goToProgramAndRecord("schede_anomalie_record",k8_anomalie_id,"k8_anomalie_id");
		return;
	}else if(elem == "skm" && k8_schede_anomalie_to_k8_schede_modifica && k8_schede_anomalie_to_k8_schede_modifica.ente_proposta_modifica && k8_schede_anomalie_to_k8_schede_modifica.nr_proposta_modifica){
		globals.nfx_goToProgramAndRecord("schede_modifica_record",k8_schede_anomalie_to_k8_schede_modifica.k8_schede_modifica_id,"k8_schede_modifica_id");
		return;
	}
}

/**
 * @properties={typeid:24,uuid:"CE225F17-20EE-4BF6-9A78-37C73C63D922"}
 */
function copyToClipboard(txt){
	application.setClipboardContent(txt);
}

/**
 * @properties={typeid:24,uuid:"B05E3DF6-F11F-4195-9CB9-2BC1808FE56D"}
 */
function showDescriptionAS400(event) {
	var rel = k8_schede_anomalie_to_k8_anagrafica_disegni;
	if(rel){
		var txt = rel.descrizione;
		var item = null;
		if(txt){
//FS
//dep		item = plugins.popupmenu.createMenuItem(txt,copyToClipboard);
//dep		item.setMethodArguments([txt]);
			var menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem(txt,copyToClipboard);
			item.methodArguments = [txt];

		}else{
		//dep 	 item = plugins.popupmenu.createMenuItem("Non presente in anagrafica...",null);
		item = menu.addMenuItem("Non presente in anagrafica...",null);
		}
		//dep plugins.popupmenu.showPopupMenu(elements.codice_disegno,[item]);
		menu.show(elements.codice_disegno);
	}
}

/**
 * @properties={typeid:24,uuid:"607B1FC1-FF4F-4943-BACD-225BB3BE2FE9"}
 */
function showDescription(event) {
	var rel = k8_schede_anomalie_to_k8_distinte_progetto_core;
	if(rel){
		var txt = rel.descrizione_cpl;
		var item = null;
		if(txt){
//dep		item = plugins.popupmenu.createMenuItem(txt,copyToClipboard);
//dep 		item.setMethodArguments([txt]);
			var menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem(txt,copyToClipboard);
			item.methodArguments = [txt];
		}else{
//dep		item = plugins.popupmenu.createMenuItem("Non presente in distinta...",null);
			menu = plugins.window.createPopupMenu();
			item.enabled=menu.addMenuItem("Non presente in distinta...",null);
		}
//dep 	plugins.popupmenu.showPopupMenu(elements.complessivo,[item]);
		menu.show(elements.complessivo);		
	}
}
