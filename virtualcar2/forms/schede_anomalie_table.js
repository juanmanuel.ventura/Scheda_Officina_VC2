/**
 * @properties={typeid:35,uuid:"42C8BB50-50A0-4145-91F3-9ABA40A5D4AE",variableType:-4}
 */
var _choices = {"Ente (SA)"							: {dataprovider: "ente_segnalazione_anomalia",																								name: "ente_segnalazione_anomalia",			size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.COMBOBOX,			editable: false,	valuelist: "gestione_anomalie$enti",								format: null,					method: null,							button: null},
                "N° (SA)"							: {dataprovider: "numero_segnalazione",																										name: "numero_segnalazione",				size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null},
                "Stato (SA)"						: {dataprovider: "stato_segnalazione",																										name: "stato_segnalazione",					size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: "gestione_anomalie_stato",								format: null,					method: null,							button: null},
                "Ente (SKA)"						: {dataprovider: "ente",																													name: "ente",								size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.COMBOBOX,			editable: false,	valuelist: "gestione_anomalie$enti",								format: null,					method: null,							button: null},
                "N° (SKA)"							: {dataprovider: "numero_scheda",																											name: "numero_scheda",						size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null},
                "Stato (SKA)"						: {dataprovider: "stato_anomalia",																											name: "stato_anomalia",						size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: "gestione_anomaile_stato",								format: null,					method: null,							button: null},
                "Registrazione (SKA)"				: {dataprovider: "data_registrazione_scheda",																								name: "data_registrazione_scheda",			size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Data rilevazione (SKA)"			: {dataprovider: "data_rilevazione_scheda",																									name: "data_rilevazione_scheda",			size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.CALENDAR,			editable: true,		valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Gestione"							: {dataprovider: "gestione_anomalia",																										name: "gestione_anomalia",					size: 130,	align: SM_ALIGNMENT.CENTER,	type: JSField.COMBOBOX,			editable: false,	valuelist: "gestione_anomalie$gestione",							format: null,					method: null,							button: null},
                "Cod. modello"						: {dataprovider: "codice_modello",																											name: "codice_modello",						size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: true,		valuelist: "progetti",												format: null,					method: null,							button: null},
                "Versione"							: {dataprovider: "codice_versione",																											name: "codice_versione",					size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: true,		valuelist: "schede_anomalie$versione",								format: null,					method: null,							button: null},
                "Gruppo funzionale"					: {dataprovider: "gruppo_funzionale",																										name: "gruppo_funzionale",					size: 150,	align: SM_ALIGNMENT.LEFT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "Gruppi",												format: null,					method: "onGruppoFunzionaleChange",		button: null},
                "Sottogruppo"						: {dataprovider: "sottogruppo_funzionale",																									name: "sottogruppo_funzionale",				size: 20,	align: SM_ALIGNMENT.CENTER,	type: JSField.TYPE_AHEAD,		editable: true,		valuelist: "schede_anomalie$sottogruppi_funzionali",				format: null,					method: null,							button: null},
                "Tipo anomalia"						: {dataprovider: "tipo_anomalia",																											name: "tipo_anomalia",						size: 150,	align: SM_ALIGNMENT.LEFT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "gestione_anomalie$tipo_anomalia",						format: null,					method: null,							button: null},
                "Sottotipo"							: {dataprovider: "sottotipo_anomalia",																										name: "sottotipo_anomalia",					size: 20,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: true,		valuelist: null,													format: null,					method: null,							button: null},
                "Segnalato da"						: {dataprovider: "segnalato_da",																											name: "segnalato_da",						size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: true,		valuelist: null,													format: null,					method: null,							button: null},
                "Leader"							: {dataprovider: "leader",																													name: "leader",								size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: true,		valuelist: "segnalazioni_anomalie$leader",							format: null,					method: "onLeaderChange",				button: null},
                "Data Assegnazione"					: {dataprovider: "data_assegnazione_leader",																								name: "data_assegnazione_leader",			size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Nuova?"							: {dataprovider: "flag_nuova",																												name: "flag_nuova",							size: 20,	align: SM_ALIGNMENT.CENTER,	type: JSField.CHECKS,			editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Responsabile"						: {dataprovider: "responsabile",																											name: "responsabile",						size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: true,		valuelist: "static$utenti_as400",									format: null,					method: "globals.nfx_trim",				button: null},
                "Gruppo 79"							: {dataprovider: "k8_schede_anomalie_to_k8_distinte_progetto_core.complessivo",																name: "complessivo",						size: 250,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Cod. disegno"						: {dataprovider: "codice_disegno",																											name: "codice_disegno",						size: 250,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: true,		valuelist: "schede_anomalie$componenti_from_distinta",				format: null,					method: null,							button: null},
                "Componente vettura"				: {dataprovider: "componente_vettura",																										name: "componente_vettura",					size: 250,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: true,		valuelist: "schede_anomalie$componenti_codifica_sat",				format: null,					method: null,							button: null},
                "Esponente"							: {dataprovider: "esponente",																												name: "esponente",							size: 60,	align: SM_ALIGNMENT.RIGHT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "schede_anomalie$esponente",								format: null,					method: null,							button: null},
                "Revisione"							: {dataprovider: "revisione",																												name: "revisione",							size: 60,	align: SM_ALIGNMENT.RIGHT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "schede_anomalie$revisione",								format: null,					method: null,							button: null},
                "Tipo Componente"					: {dataprovider: "tipo_componente",																											name: "tipo_componente",					size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "gestione_anomalie$tipo_componente",						format: null,					method: "onTipoComponenteChange",		button: null},
                "Difetto"							: {dataprovider: "codice_difetto",																											name: "codice_difetto",						size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "gestione_anomalie$codici_difetto",						format: null,					method: null,							button: null},
                "Demerito"							: {dataprovider: "punteggio_demerito",																										name: "punteggio_demerito",					size: 130,	align: SM_ALIGNMENT.RIGHT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "schede_anomalie$demerito",								format: null,					method: null,							button: null},
                "Indice criticità"					: {dataprovider: "indice_criticita",																										name: "indice_criticita",					size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "indice_criticita",										format: null,					method: null,							button: null},
                "Icona criticità"					: {dataprovider: "icon_criticita",																											name: "icon_criticita",						size: 20,	align: SM_ALIGNMENT.CENTER,	type: JSField.IMAGE_MEDIA,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Causale anomalia"					: {dataprovider: "attribuzione",																											name: "attribuzione",						size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "schede_anomalie$causale_anomalia",						format: null,					method: null,							button: null},
                "Chiusura richiesta"				: {dataprovider: "data_chiusura_richiesta",																									name: "data_chiusura_richiesta",			size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.CALENDAR,			editable: true,		valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Chiusura prevista"					: {dataprovider: "data_prevista_chiusura_sc_anom",																							name: "data_prevista_chiusura_sc_anom",		size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.CALENDAR,			editable: true,		valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Utente Chius. (SKA)"				: {dataprovider: "utente_chiusura_scheda",																									name: "utente_chiusura_scheda",				size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Data Chius. (SKA)"					: {dataprovider: "data_chiusura",																											name: "data_chiusura",						size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},             
                "Ente (SKM)"						: {dataprovider: "k8_schede_anomalie_to_k8_schede_modifica.ente_proposta_modifica",															name: "ente_proposta_modifica",				size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.COMBOBOX,			editable: false,	valuelist: "gestione_anomalie$enti",								format: null,					method: null,							button: null},
                "N° (SKM)"							: {dataprovider: "k8_schede_anomalie_to_k8_schede_modifica.nr_proposta_modifica",															name: "nr_proposta_modifica",				size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null},
                "Stato (SKM)"						: {dataprovider: "k8_schede_anomalie_to_k8_schede_modifica.stato_scheda",																	name: "stato_scheda",						size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: "gestione_anomaile_stato",								format: null,					method: null,							button: null},
                "Causale modifica"					: {dataprovider: "k8_schede_anomalie_to_k8_schede_modifica.causale_modifica",																name: "causale_modifica",					size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Approvazione"						: {dataprovider: "k8_schede_anomalie_to_k8_schede_modifica.tipo_approvazione",																name: "tipo_approvazione",					size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "CID"								: {dataprovider: "k8_schede_anomalie_to_k8_schede_modifica.numero_cid",																		name: "numero_cid",							size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null},
                "Annulata?"							: {dataprovider: "k8_schede_anomalie_to_k8_schede_modifica.annullata",																		name: "annullata",							size: 20,	align: SM_ALIGNMENT.CENTER,	type: JSField.CHECKS,			editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Data approvazione (SKM)"			: {dataprovider: "k8_schede_anomalie_to_k8_schede_modifica.data_approv_sk_prop_mod",														name: "data_approv_sk_prop_mod",			size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Data emissione (LM)"				: {dataprovider: "k8_schede_anomalie_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica.data_emissione",						name: "data_emissione",						size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Tipo (LM)"							: {dataprovider: "k8_schede_anomalie_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica.tipo",									name: "tipo",								size: 130,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Prevista attuazione (LM)"			: {dataprovider: "k8_schede_anomalie_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica.data_prevista_attuazione",				name: "data_prevista_attuazione",			size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Richiesta assembly (LM)"			: {dataprovider: "k8_schede_anomalie_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica.richiesta_assembly",					name: "richiesta_assembly",					size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Introduzione modifica (LM)"		: {dataprovider: "data_introduzione_calc",																									name: "data_introduzione_modifica_1",		size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Numero assembly (LM)"				: {dataprovider: "numero_assembly_calc",																									name: "numero_assembly_1",					size: 130,	align: SM_ALIGNMENT.RIGHT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null}};

/**
 * @properties={typeid:35,uuid:"318F2B17-F9DA-4B80-9FF7-81B9A52BBBA8",variableType:-4}
 */
var _fields = ["Ente (SA)", "N° (SA)", "Stato (SA)","Ente (SKA)","N° (SKA)","Stato (SKA)", "Registrazione (SKA)", "Data rilevazione (SKA)", "Gestione", "Cod. modello", "Versione", "Gruppo funzionale", "Sottogruppo", "Tipo anomalia", "Sottotipo", "Segnalato da", "Leader", "Data Assegnazione", "Nuova?", "Responsabile", "Gruppo 79", "Cod. disegno", "Componente vettura", "Esponente", "Revisione", "Tipo Componente", "Difetto", "Demerito", "Indice criticità", "Icona criticità", "Causale anomalia", "Chiusura richiesta", "Chiusura prevista", "Utente Chius. (SKA)", "Data Chius. (SKA)","Ente (SKM)", "N° (SKM)", "Stato (SKM)", "Causale modifica", "Approvazione", "CID", "Annulata?", "Data approvazione (SKM)", "Data emissione (LM)", "Tipo (LM)", "Prevista attuazione (LM)", "Richiesta assembly (LM)", "Introduzione modifica (LM)", "Numero assembly (LM)"];

/**
 * @properties={typeid:35,uuid:"4F3E2AE6-BBAD-4D5D-9CA7-5BF6E6096939",variableType:-4}
 */
var _savedFS = null;

/**
 *
 * @properties={typeid:24,uuid:"D5E55DAC-8566-423C-A40D-709BBAA99D4D"}
 */
function nfx_getTitle(){
	return "Schede Anomalie - Tabella";
}

/**
 * @properties={typeid:24,uuid:"70B20CEC-DBE0-47A6-BF80-313CD92F73E1"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"E9E17AB8-C59E-4844-A090-3D057E9312CD"}
 */
function nfx_sks(){
	return [{icon:	"chiudi_segnalazione.png",	tooltip:"Chiusura Scheda",				method:"chiusuraScheda"},
	        {icon:	"findplus.png",				tooltip:"Ricerca per descrizione...",	method:"openDescriptionSearch"},
	        {icon:	"table.png",				tooltip:"Scelta colonne",				method:"localFieldChooserWrap"},
	        {icon:	"working_lock.png",			tooltip:"Modifica multipla",			method:"lockInterface"}]; 
}

/**
 * @properties={typeid:24,uuid:"76598D55-746C-479D-AF52-F84B81A59E28"}
 */
function localFieldChooserWrap(){
	forms._vc2_colums_customizer.opnePopup(_choices,_fields,controller.getName(),"_fields");
	globals.vc2_drawFieldsInForm(controller.getName(),_choices,_fields);
}

/**
 * @properties={typeid:24,uuid:"02006EFE-E887-404B-ACA7-DE85E2083123"}
 * @AllowToRunInFind
 */
function lockInterface(){
	var _c = forms[globals.nfx_selectedProgram()].controller;
	if(_c.readOnly){
		forms.nfx_interfaccia_pannello_buttons.mode = "edit";
		_savedFS = forms[globals.nfx_selectedProgram()].foundset.duplicateFoundSet();
		if(foundset.find()){
			foundset.stato_anomalia = "APERTA";
			foundset.search(false,true);
		}
		forms.nfx_interfaccia_pannello_buttons.disableNavigationTree();
		forms.nfx_interfaccia_pannello_buttons.disableButtons(["sk3"]);
		//forms.nfx_interfaccia_pannello_buttons.disableButtons(["sk2"]);
		databaseManager.setAutoSave(false);
		_c.readOnly = false;
	}else{
		_c.readOnly = true;
		var _e = databaseManager.getEditedRecords();
		for(var _i=0;_i<_e.length;_i++){
			if(localRecordValidation(_e[_i]) > -1 && localTransmitData(_e[_i]) > -1){
				databaseManager.saveData(_e[_i]);
			}
		}
		//databaseManager.rollbackEditedRecords();
		databaseManager.revertEditedRecords();
		databaseManager.setAutoSave(true);
		forms.nfx_interfaccia_pannello_buttons.enableCorrectButtons();
		forms.nfx_interfaccia_pannello_buttons.enableNavigationTree();
		_savedFS = forms[globals.nfx_selectedProgram()].foundset.loadRecords(_savedFS);
		forms.nfx_interfaccia_pannello_buttons.mode = null;
	}
}

/**
 * @properties={typeid:24,uuid:"B7134481-69C2-4694-B3D4-CB6AEE3F622E"}
 */
function localRecordValidation(_rec){
	var allFields = {"ente"						:	"Ente (scheda anomalia)",
		             "data_rilevazione_scheda"	:	"Data rilevazione",
		             "codice_modello"			:	"Cod. modello",
		             "codice_versione"			:	"Versione",
		             "gruppo_funzionale"		:	"Gruppo funzionale",
		             "tipo_anomalia"			:	"Tipo anomalia",
		             "segnalato_da"				:	"Segnalato da",
		             "leader"					:	"Leader",
		             "responsabile"				:	"Responsabile",
		             "codice_disegno"			:	"Cod. disegno\n(Nel caso sia sconosciuto, inserire nel campo Cod.disegno il Gruppo 79 corrispondente)",
		             "tipo_componente"			:	"Tipo componente",
		             "codice_difetto"			:	"Difetto",
		             "punteggio_demerito"		:	"Demerito",
		             "attribuzione"				:	"Causale anomalia"};
	
	if(_rec.gruppo_funzionale && globals.vc2_isSottogruppoFunzionaleRequired(_rec.gruppo_funzionale) && !_rec.sottogruppo_funzionale){
			allFields["sottogruppo_funzionale"] = "Sottogruppo Funzionale";
	}
		
	if(_rec.tipo_componente == "V"){
		allFields["esponente"] = "Esponente";
		allFields["revisione"] = "Revisione";
	}
	
	var missing = [];
	for (var f in allFields){
		if (_rec[f] === null || _rec[f] === ""){
			missing.push(f);
		}
	}

	//INIZIO CHECK RESPONSABILE
	if(_rec.responsabile && !databaseManager.getDataSetByQuery("ferrari","SELECT * FROM XCH_SRUTE00F_IN WHERE TRIM(MCDUTE) = ?",[_rec.responsabile],1).getMaxRowIndex()){
		missing.push("Responsabile");
	}
	//FINE CHECK RESPONSABILE

	if (missing.length > 0){
		var message = "Rilevati errori nella compilazione:\n\n";
		var i = 1;
		for each(var mf in missing){
			message += (allFields[mf]) ? i++ + ". Inserire " + allFields[mf] + "\n" : mf + ": valore non corretto\n";
		}
		message += "\nIl record non verrà inviato ad AS400 e sarà riportato allo stato precedente alle modifiche.";
		plugins.dialogs.showErrorDialog("Errori nella compilazione",message,"Ok");
		return -1;
	}else{
		//I campi obbligatori sono tutti compilati qui imposto criteri più complessi (servirebbe una callback)
		if(_rec.data_registrazione_scheda < _rec.data_rilevazione){
			var message = "Rilevati errori nella compilazione:\n\n";
			message += "1. La data di rilevazione deve necessariamente essere minore o uguale alla data di registrazione\n";
			message += "\nIl record non verrà inviato ad AS400 e sarà riportato allo stato precedente alle modifiche.";
			plugins.dialogs.showErrorDialog("Errori nella compilazione",message,"Ok");
			return -1
		}else{
			return 0;
		}
	}
}

/**
 * @properties={typeid:24,uuid:"B0956E68-2912-411E-A61B-3A6891C627A4"}
 */
function localTransmitData(_rec){
	var data = localPackDataAndUser(_rec);
	//FS
//dep	var p = plugins.http.getPoster(globals.vc2_serviceLocationSk,"scheda_anomalia");
	var client = plugins.http.createNewHttpClient();
	var postReq=client.createPostRequest(globals.vc2_serviceLocationSk);
//dep	p.setCharset("ISO-8859-1");
	postReq.setCharset("ISO-8859-1");
	//dep	 for (var s in data){
		for (var _s in data){
//dep		p.addParameter(s, data[s]);
		postReq.addHeader(_s,data[_s])
	}
//dep	var code = p.doPost();
	var res = postReq.executeRequest()
	var response = null;
//dep	if (code == 200){
//commento di FS al posto di 200 si può scrivere plugins.http.HTTP_STATUS.SC_OK (https://wiki.servoy.com/display/DOCS/HTTP_STATUS)
	if (res!=null && res.getStatusCode() == 200){
//dep		response = globals.vc2_parseResponse(_rec, p.getPageData(), "SCNREG_OUT", ["SCCENT","SCMODE","SCVERS","SCENTE","SCLEA2","SCLEAD","SCRESP","SCNOMI","SCNOMV"]);
		response = globals.vc2_parseResponse(_rec, res.getResponseBody(), "SCNREG_OUT", ["SCCENT","SCMODE","SCVERS","SCENTE","SCLEA2","SCLEAD","SCRESP","SCNOMI","SCNOMV"]);
	}else{
		// Codice Errore WSNET1: errore nella connessione di rete il servizio non ha risposto 200.
		plugins.dialogs.showErrorDialog("Dati segnalazione non trasmessi","Il servizio AS400 non è al momento disponibile. (WSNET1)","Ok");
		globals.vc2_lastErrorLog = "Codice: " + res + " - Il servizio AS400 non è al momento disponibile. (WSNET1)";
		response = -1;
	}
	//Inizio LOG
	var table = databaseManager.getDataSourceTableName(controller.getDataSource());
	var table_key = _rec.k8_anomalie_id;
//	JStaffa
//	var logic_key = (response != -1) ? _rec.ente + " " + utils.stringToNumber(response) : _rec.ente;
	var logic_key = (response != -1) ? _rec.ente + " " + utils.stringToNumber(response.toString()) : _rec.ente;
	globals.vc2_logTransactionToAS400(table,table_key,logic_key,data,globals.vc2_lastErrorLog,response,globals.vc2_getLogOperation());
	//Fine LOG
	return response;
}

/**
 * @properties={typeid:24,uuid:"8855BC44-7286-4ED5-962B-B1CD04F1F556"}
 */
function localPackDataAndUser(_rec){
	var data = localPackData(_rec);
	data["SACHIA"] = 'SERVOY';
	var userInfo = globals.vc2_getUserInfo(null);
	if (userInfo && userInfo.user_as400){
		if(data["SCNREG"]){
			_rec.utente_modifica = userInfo.user_as400;
		}else{
			_rec.utente_creazione = userInfo.user_as400;
		}
		data["SCNOMI"] = userInfo.user_as400;
	}
	return data;
}

/**
 * @properties={typeid:24,uuid:"31116F13-CD42-43BF-8643-E42CE96882B5"}
 */
function localPackData(_rec){
	var data = {'SCCTIP' : 'T', //OBBLIGATORIO La Testata
				'SCVEEX' : globals.vc2_webServiceMode, //OBBLIGATORIO Sempre esecuzione
				'SCOP'   : "I", //<INSERIMENTO/MODIFICA/CANCELLAZIONE> OBBLIGATORIO
				'SCAPPL' : "VIRTCAR2",
				'SCCENT' : _rec.ente, //<Cd Ente> OBBIGATORIO
				'SCNREG' : _rec.numero_scheda, //<N Registrazione evento> OBBLIGATORIO IN MODIFICA.
				'SCDREG' : globals.utils_dateFormatAS400(_rec.data_registrazione_scheda), //<Dt Reg.Evento> OBBLIGATORIO. YYYY MM DD
				'SCDRIL' : globals.utils_dateFormatAS400(_rec.data_rilevazione_scheda), //<Dt Rilevazione> OBBLIGATORIO
				'SCMODE' : _rec.codice_modello, //<Modello> OBBLIGATORIO.
				'SCVERS' : _rec.codice_versione, //<Versione> OBBLIGARIO
				'SCPROV' : globals.utils_stateFormatAS400(_rec.stato_anomalia), //<Stato Provvedim.> OBBLIGATORIO. Chiarire Funzione. Un carattere. 
				'SCENTE' : _rec.ente_segnalazione_anomalia,
				'SCNSCH' : _rec.numero_segnalazione,
				'SCDAVA' : "", //Data ultimo av...anzamento?
				'SCCOMP' : _rec.componente_vettura, //<Cod. Componente> OBBLIGATORIO NUMERICO
				'SCCDIS' : _rec.codice_disegno, //<Cd. Disegno> OBBLIGATORIO NUMERICO
				'SCLEA2' : _rec.leader, //<Leader> OBBLIGATORIO TESTO.
				'SCDESP' : null,//globals.utils_dateFormatAS400(data_ultimo_esp), //data ultimo esponente.
				'SCMATR' : _rec.matricola, //<Matricola AOMAT> NON OBBLIGATORIO NUMERICO dal 12 giugno (sviluppo previsto)
				'SCFUNZ' : _rec.gruppo_funzionale, //<Gruppo Funzionale> OBBLIGATORIO
				'SCSUGR' : _rec.sottogruppo_funzionale, //<Sottogruppo funzionale> OBBLIGATORIO
				'SCCOMO' : _rec.omologazione_sicurezza, //Codice Omologazione
				'SCCDIF' : _rec.codice_difetto, //<Cd Difetto> OBBLIGATORIO
				'SCFLGC' : _rec.flag_componente_specifico, //flag componente specifico.
				'SCPERC' : "", //percentuale difetti
				'SCKPER' : _rec.km_percorsi, //<Km Percorsi> NON OBBLIGATORIO (Viene settato a 0 dal servizio).
				'SCDEMI' : globals.utils_dateFormatAS400(_rec.data_prevista_chiusura_sc_anom), //data prevista chiusura scheda
				'SCDLEA' : globals.utils_dateFormatAS400(_rec.data_assegnazione_leader), //data assegnazione leader
				'SCLEAD' : _rec.segnalato_da, //<Segnalato da> OBBLIGATORIO (Servoy dovrebbe metterlo in automatico in base all'utenza).
				'SCRESP' : _rec.responsabile, //Responsabile Team
				'SCCFOR' : "", //Codice fornitore
				'SCDRIC' : globals.utils_dateFormatAS400(_rec.data_prev_cons_pz_mod), //Data prevista componente modificato
				'SCDICS' : _rec.tipo_anomalia, //<Tipo anomalia> OBBLIGATORIO.
				'SCDICP' : _rec.punteggio_demerito, //<Demerito ICP> OBBLIGATORIO.
				'SCCAUS' : _rec.causale_modifica, // Causale
				'SCTIPA' : _rec.tipo_approvazione, //Tipo approvazione
				'SCDATI' : globals.utils_dateFormatAS400(_rec.timestamp_creazione || new Date()), //<Data Inser.> NON OBBLIGATORIO, MA CONSIGLIATO, ARRIVA DA SERVOY
				'SCDATV' : globals.utils_dateFormatAS400(_rec.timestamp_modifica || new Date()), //<Data Variaz.> NON OBBLIGATORIO
				'SCTCOM' : _rec.tipo_componente, //<Tipo componente> OBBLIGATORIO, assunme i valori F (Fisico) e V (Virtuale).
				'SCESPO' : _rec.esponente, //<Esponente> OBBLIGATORIO se 'Tipo componente' è 'V'
				'SCREVI' : _rec.revisione, //<Revisione> OBBLIGATORIO se 'Tipo componente' è 'V'
				'SCCANO' : _rec.attribuzione, //<Causale Anomalia> o <Classe Anomalia> OBBLIGATORIO.
          		'SCDINT' : globals.utils_dateFormatAS400(_rec.data_introduzione), //Data introduzione OBBLIGATORIO in caso di chiusura SKA di tipo PROCESSO/FORNITURA
          		'SCNASS' : _rec.numero_assembly //Numero assembli
				};
	return data;
}
