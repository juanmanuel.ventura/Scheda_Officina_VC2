/**
 * @properties={typeid:35,uuid:"6A5D4A89-B238-4E21-B1FB-2E64F1830646",variableType:-4}
 */
var nfx_related = ["righe_azionamenti_e_manovre_list", "variabili_ambientali_table", "documenti_record"];

/**
 * @properties={typeid:24,uuid:"CBA0273A-20B6-4C25-8B92-0BE343027BE6"}
 */
function nfx_defineAccess() {
	return [true, true, true];
}

/**
 * @properties={typeid:24,uuid:"A68DA751-EBCE-4E47-8B1F-67CA1DE081BE"}
 */
function nfx_preAdd() {
	numero_scheda = calcolaNumeroScheda();
	revisione = 0;
}

/**
 * @properties={typeid:24,uuid:"B956F7B7-A0AF-4412-B640-F44136FA5B14"}
 */
function nfx_onRecordSelection() {
	refrshFormDB();
}

/**
 * @properties={typeid:24,uuid:"E28329B9-FF7B-4AFC-B911-6188D1CAFA5E"}
 */
function calcolaNumeroScheda() {
	var offset = 3;
	//FS
	//dep	var query = "select numero_scheda from " + controller.getTableName() + " where extract(year from data) = ? order by data desc";
	var query = "select numero_scheda from " + databaseManager.getDataSourceTableName(controller.getDataSource()) 	+ " where extract(year from data) = ? order by data desc";
	var yearFourDigit = utils.dateFormat(new Date(), "yyyy");
	var yearTwoDigit = utils.dateFormat(new Date(), "yy");

	//dep	var ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,[yearFourDigit],1);
	var ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, [yearFourDigit], 1);

	var ns = ds.getValue(1, 1);
	var number = (ns) ? ns.substring(0, ns.length - offset) : 0;
	number++;
	return number + "/" + yearTwoDigit;
}

/**
 * @properties={typeid:24,uuid:"3AAD3250-B68C-4C7E-A214-5FA36AE6E222"}
 */
function nfx_sks() {
	return [{ icon: "fuel.png", tooltip: "Calcolo consumi", method: "createMenu" }, { icon: "copy.png", tooltip: "Duplica scheda", method: "duplicaScheda" }, { icon: "refresh2.png", tooltip: "Sincronizza dati", method: "refrshFormDB" }];
}

/**
 * @properties={typeid:24,uuid:"1B6C8E1F-71E8-464B-BF6A-64BCEED1A572"}
 */
function createMenu() {
	//dep var current = plugins.popupmenu.createMenuItem("Scheda corrente",calcolaKm);
	var menu = plugins.window.createPopupMenu();
	menu.addMenuItem("Scheda corrente", calcolaKm);
	//dep	var total = plugins.popupmenu.createMenuItem("Totale schede",calcolaKmGlobale);
	menu.addMenuItem("Totale schede", calcolaKmGlobale);
	//dep plugins.popupmenu.showPopupMenu(forms.nfx_interfaccia_pannello_buttons.elements["sk0"],[current,total]);
	menu.show(forms.nfx_interfaccia_pannello_buttons.elements["sk0"]);

}

/**
 * @properties={typeid:24,uuid:"6FE03ACC-4021-4BAD-9F3E-595709606F4B"}
 */
function duplicaScheda() {
	var old_pk = k8_schede_collaudo_id;
	//FS
	//dep	var query = "select max(REVISIONE) from " + controller.getTableName() + " where NUMERO_SCHEDA = ?";
	var query = "select max(REVISIONE) from " + databaseManager.getDataSourceTableName(controller.getDataSource()) + " where NUMERO_SCHEDA = ?";
//dep 	var ds = databaseManager.getDataSetByQuery(controller.getServerName(), query, [numero_scheda], 1);
	var ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, [numero_scheda], 1);

	var fs = foundset.duplicateFoundSet();
	fs.duplicateRecord(fs.getSelectedIndex(), true, true);
	fs.revisione = ds.getValue(1, 1) + 1;
	databaseManager.saveData();
	databaseManager.refreshRecordFromDatabase(fs, -1);
	controller.loadRecords(fs);
	controller.setSelectedIndex(1);
	var new_pk = k8_schede_collaudo_id;
	duplicaAM(old_pk, new_pk);
	forms.nfx_interfaccia_pannello_buttons.updatePosition();
}

/**
 * @properties={typeid:24,uuid:"845F2044-AAA6-4085-ABE1-9D26884A19CF"}
 * @AllowToRunInFind
 */
function duplicaAM(old_pk, new_pk) {
	var fs = foundset.duplicateFoundSet();
	if (fs.find()) {
		fs.k8_schede_collaudo_id = old_pk;
		if (fs.search()) {
			var rel = fs.k8_schede_collaudo_to_k8_righe_azionam_manovre.duplicateFoundSet();
			var max = rel.getSize();
			for (var i = 1; i <= max; i++) {
				rel.duplicateRecord(i, false, true);
				rel.fk_schede_collaudo = new_pk;
				rel.ripetizioni_eseguite = null;
				rel.note_collaudatore = null;
			}
			databaseManager.saveData();
			databaseManager.refreshRecordFromDatabase(rel, -1);
			globals.nfx_syncTabs();
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"2A05CE09-BC39-4E4D-B6AD-9C858DD2DE87"}
 * @AllowToRunInFind
 */
function calcolaKm() {
	var kmStart = odometro_inizio;
	var related = k8_schede_collaudo_to_k8_variabili_ambientali.duplicateFoundSet();
	if (related.find()) {
		related.variabile = 'Carburante Immesso';
		related.odometro = '> 0';
		related.valore = '> 0';
		related.search();
	}
	if (utils.hasRecords(related)) {
		related.sort("odometro desc");
		km_percorsi_totali_calc_carbur = related.odometro - kmStart;
		carburante_immesso_tot_scheda = 0;
		for (var i = 1; i <= related.getSize(); i++) {
			related.setSelectedIndex(i);
			carburante_immesso_tot_scheda += related.valore;
		}
		if (km_percorsi_totali_calc_carbur && carburante_immesso_tot_scheda) {
			consumo_carburante = km_percorsi_totali_calc_carbur / carburante_immesso_tot_scheda
		}
		databaseManager.saveData();
	} else {
		plugins.dialogs.showInfoDialog("Attenzione", "Rifornimento carburante non effettuato", "Ok");
	}
}

/**
 *
 * @properties={typeid:24,uuid:"38CA1633-C741-471D-86C7-19EF3EE91A57"}
 */
function calcolaKmGlobale() {
	media_kilometrica_totale = null;

	//application.showFormInDialog(forms.schede_collaudo_consumi_dialog,null,null,1024);

	var formq = forms.schede_collaudo_consumi_dialog;
	var window = application.createWindow("", JSWindow.MODAL_DIALOG);
	//window.setSize(1024,null);
	formq.controller.show(window);

}

/**
 * @properties={typeid:24,uuid:"C1579192-9C82-4DF2-9E86-71565B0E4576"}
 */
function refrshFormDB() {
	databaseManager.refreshRecordFromDatabase(foundset, controller.getSelectedIndex());
	databaseManager.refreshRecordFromDatabase(k8_schede_collaudo_to_k8_righe_schede_collaudo, -1);
	databaseManager.refreshRecordFromDatabase(k8_schede_collaudo_to_k8_righe_azionam_manovre, -1);
	databaseManager.refreshRecordFromDatabase(k8_schede_collaudo_to_k8_variabili_ambientali, -1);
}

/**
 * @properties={typeid:24,uuid:"C9729029-6F7C-4A6E-A6B7-55AB28D7D2E9"}
 */
function createScheda(lol) {
	/*
	 TODO: metodo da rifare!!!
	 */
}
