/**
 *
 * @properties={typeid:24,uuid:"5CD6D4A0-71CA-4618-ACAB-A287E908D5F1"}
 */
function close(){
	
	//application.closeFormDialog();
	
	var w=controller.getWindow();
	w.destroy();
}

/**
 *
 * @properties={typeid:24,uuid:"4EE9E90A-A17A-4807-BF45-8638F23A11AF"}
 */
function compute(){
	forms.schede_collaudo_consumi_table.compute();
	//application.closeFormDialog();
}

/**
 *
 * @properties={typeid:24,uuid:"AFAE9276-6136-44D6-9994-F36DB9541C49"}
 */
function searchSchede(){
	forms.schede_collaudo_consumi_table.findSearch();
}
