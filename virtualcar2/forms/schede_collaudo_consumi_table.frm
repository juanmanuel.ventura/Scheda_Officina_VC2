dataSource:"db:/ferrari/k8_schede_collaudo",
extendsID:"9C713D8B-20C7-40B5-B33B-66CF65C39B89",
items:[
{
anchors:11,
labelFor:"data",
location:"10,60",
mediaOptions:14,
size:"100,20",
styleClass:"table",
tabSeq:-1,
text:"Data",
typeid:7,
uuid:"27C34E59-AE3A-4DE3-BAEC-3A7C661754FF"
},
{
height:251,
partType:5,
typeid:19,
uuid:"38664DB7-CD4E-447C-A8ED-A5ED6A745464"
},
{
anchors:11,
labelFor:"km_percorsi",
location:"260,10",
mediaOptions:14,
size:"140,20",
styleClass:"table",
tabSeq:-1,
text:"Km Percorsi",
typeid:7,
uuid:"3EF1893E-AFC5-47E5-AF28-566195B30CA0"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"oggetto_prova",
displayType:10,
editable:false,
location:"270,80",
name:"oggetto_prova",
size:"290,20",
styleClass:"tablerow",
text:"Oggetto Prova",
typeid:4,
uuid:"42DED6C0-09D9-4FF4-A909-5DD77777BCBF"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"progetto",
displayType:10,
editable:false,
location:"10,30",
name:"progetto",
size:"100,20",
styleClass:"tablerow",
text:"Progetto",
typeid:4,
uuid:"45D91968-3A80-4256-9A52-0B589D7A46B6",
valuelistID:"8DBD3071-35BE-4D55-B40F-F9B6FB9F8DD6"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"data",
displayType:5,
editable:false,
location:"10,80",
name:"data",
size:"100,20",
styleClass:"tablerow",
text:"Data",
typeid:4,
uuid:"548B65E1-B16E-4E87-8CAA-D72733684A36"
},
{
anchors:11,
labelFor:"report_consumi",
location:"600,110",
mediaOptions:14,
size:"150,20",
styleClass:"table",
tabSeq:-1,
text:"Report Consumi",
typeid:7,
uuid:"56CCE0F7-BD3A-4231-9E76-228DF2898845"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"percorso",
editable:false,
location:"120,80",
name:"percorso",
size:"140,20",
styleClass:"tablerow",
text:"Percorso",
typeid:4,
uuid:"59EBF98B-CBA8-4DFC-8AC5-1C5AAAFE76E0",
valuelistID:"2AB6EA09-F79F-4074-B086-5D102C54506D"
},
{
anchors:11,
labelFor:"consumo_carburante",
location:"410,10",
mediaOptions:14,
size:"140,20",
styleClass:"table",
tabSeq:-1,
text:"Consumo Carburante",
typeid:7,
uuid:"5CEC1195-7BBB-4D0B-BF0B-46D80C91B65E"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"vettura",
editable:false,
location:"120,30",
name:"vettura",
size:"100,20",
styleClass:"tablerow",
text:"Vettura",
typeid:4,
uuid:"60E9F393-581E-437C-8165-68870BD6130F",
valuelistID:"EA754802-DAA2-41C1-B159-3800888EEC2E"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"carburante_immesso_tot_scheda",
editable:false,
location:"10,130",
name:"carburante_immesso_tot_scheda",
size:"210,20",
styleClass:"tablerow",
typeid:4,
uuid:"79519C67-E83E-4432-A357-3ABE67421620"
},
{
anchors:11,
labelFor:"progetto",
location:"10,10",
mediaOptions:14,
size:"100,20",
styleClass:"table",
tabSeq:-1,
text:"Progetto",
typeid:7,
uuid:"94790E7D-CCEE-4BD2-9094-F95E9FC8571D"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"consumo_carburante",
editable:false,
format:"#####.00",
location:"410,30",
name:"consumo_carburante",
size:"140,20",
styleClass:"tablerow",
typeid:4,
uuid:"AA6E16CA-E833-4857-8EC9-5FF4C52ECC2A"
},
{
anchors:11,
labelFor:"oggetto_prova",
location:"270,60",
mediaOptions:14,
size:"140,20",
styleClass:"table",
tabSeq:-1,
text:"Oggetto prova",
typeid:7,
uuid:"B8E62034-87B2-4B24-81D1-6DF856E311AF"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"report_consumi",
displayType:3,
location:"600,140",
name:"report_consumi",
scrollbars:36,
size:"160,40",
styleClass:"tablerow",
typeid:4,
uuid:"B9FBFAA1-490A-45E0-9C7A-0E96DEC66FD4",
valuelistID:"1E2C0E21-C850-4BCF-8F48-41FDCB0B17D0"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"km_percorsi",
editable:false,
location:"260,30",
name:"km_percorsi",
size:"140,20",
styleClass:"tablerow",
typeid:4,
uuid:"BA7F66B7-096F-4A96-9382-91E2EB5507A0"
},
{
anchors:11,
labelFor:"carburante_immesso_tot_scheda",
location:"10,110",
mediaOptions:14,
size:"210,20",
styleClass:"table",
tabSeq:-1,
text:"Carburante Immesso Tot Scheda",
typeid:7,
uuid:"DC714623-2850-441F-BE79-E5E30FF74BD2"
},
{
anchors:11,
labelFor:"vettura",
location:"120,10",
mediaOptions:14,
size:"100,20",
styleClass:"table",
tabSeq:-1,
text:"Vettura",
typeid:7,
uuid:"E5690E08-F689-4B5B-A7EE-34854A9799CF"
},
{
anchors:11,
labelFor:"percorso",
location:"120,60",
mediaOptions:14,
size:"140,20",
styleClass:"table",
tabSeq:-1,
text:"Percorso",
typeid:7,
uuid:"F992A954-1898-47D5-8355-5E27565DE260"
}
],
name:"schede_collaudo_consumi_table",
namedFoundSet:"separate",
navigatorID:"-1",
onHideMethodID:"-1",
onRecordSelectionMethodID:"-1",
onSearchCmdMethodID:"F7CB0F17-ED69-49B3-AA31-DC523956138C",
onShowMethodID:"3EF6A7E9-2555-4788-884F-2704710DE787",
paperPrintScale:100,
scrollbars:4,
showInMenu:true,
size:"794,251",
styleName:"keeneight",
typeid:3,
uuid:"7B4EF4DB-8661-4476-9861-05F4076EA0BA",
view:3