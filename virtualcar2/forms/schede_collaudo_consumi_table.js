/**
 * @properties={typeid:35,uuid:"4033C893-E000-4CBA-AE14-C5893C5E025C",variableType:-4}
 */
var startup = true;

/**
 *
 * @properties={typeid:24,uuid:"08334471-F454-4FF5-BA40-B68EC2F3B825"}
 * @AllowToRunInFind
 */
function compute(){
	try{
	
	var fs = foundset.duplicateFoundSet();
	if (fs.find()){
		fs.report_consumi = 'Si'
		fs.search(false,true);
	}
	var totale = 0;
	for (var i=1; i<=fs.getSize(); i++){
		fs.setSelectedIndex(i);
		calcolaKm();
		totale += fs.consumo_carburante;
	}
	globals.vc2_mediaKmScCollaudo = (totale / foundset.getSize()).toFixed(2).replace(".",",");
	}catch(exc){}
}

/**
 *
 * @properties={typeid:24,uuid:"D2FAEEF9-E70E-47E5-BE53-6A104A8F6F72"}
 */
function findSearch(){
	if (startup){
		onSearch();
	}else{
		onShow();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"F7CB0F17-ED69-49B3-AA31-DC523956138C"}
 * @AllowToRunInFind
 */
function onSearch(){
	controller.search();
	if (startup){
		for(var i=1;i<=foundset.getSize();i++){
			foundset.setSelectedIndex(i);
			report_consumi = 'Si';
		}
	databaseManager.saveData();
	startup = false;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"3EF6A7E9-2555-4788-884F-2704710DE787"}
 */
function onShow(){
	if(controller.find()){
		vettura_partita_con_pieno = 'Sì';
	}
	globals.vc2_mediaKmScCollaudo = "Non calcolato";
	startup = true;
}
