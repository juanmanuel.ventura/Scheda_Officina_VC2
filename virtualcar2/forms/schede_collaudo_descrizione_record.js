/**
 *
 * @properties={typeid:24,uuid:"346E11A8-7BE4-4C67-A507-CA471A028488"}
 */
function nfx_defineAccess()
{
	return [false,false,true];
}

/**
 *
 * @properties={typeid:24,uuid:"30346F99-9053-43B5-827E-F0B59BFA4A3E"}
 */
function nfx_getTitle()
{
	return "Descrizione Scheda";
}

/**
 *
 * @properties={typeid:24,uuid:"A17F6862-B731-46D5-92AB-4CB4E8BB13CD"}
 */
function nfx_isProgram()
{
	return false;
}

/**
 *
 * @properties={typeid:24,uuid:"55CC7E02-421E-4D83-9FB9-8DEA7D750F5C"}
 */
function nfx_postEdit()
{
	if(odometro_inizio && odometro_fine){
		forms.distinte_vetture.refreshVettura(vettura,progetto);
	}
}
