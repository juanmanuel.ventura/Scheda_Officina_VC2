/**
 *
 * @properties={typeid:24,uuid:"85726079-E74C-4912-8F21-A140BF56BF9A"}
 */
function nfx_defineAccess()
{
	return [false,false,true];
}

/**
 *
 * @properties={typeid:24,uuid:"750CEF6F-543A-42D9-BF84-EDFE0CBA04F5"}
 */
function nfx_getTitle()
{
	return "Note";
}

/**
 *
 * @properties={typeid:24,uuid:"6C2349AB-4551-4CBC-9F8F-61021E1F2D1C"}
 */
function nfx_isProgram()
{
	return false;
}
