/**
 *
 * @properties={typeid:24,uuid:"BA176341-1DAA-4CED-B803-6AE4E52165DA"}
 */
function nfx_getTitle(){
	return "Schede Collaudo";
}

/**
 *
 * @properties={typeid:24,uuid:"53AACDC0-8EE0-4D0F-88E8-AC081E099261"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"78C8D9E9-8D53-4FDE-923B-9EF7B8F0689C"}
 */
function onVetturaChange(oldValue, newValue, event) {
	numero_telaio = k8_schede_collaudo_to_k8_anagrafica_vetture$unoauno.numero_telaio;
	return true;
}
