/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"5EC449D6-8384-494C-8549-E59271A638A0"}
 */
var nfx_orderBy = "data desc";

/**
 *
 * @properties={typeid:24,uuid:"4F10EF4B-F881-49C7-9995-2BB09083C733"}
 */
function nfx_getTitle()
{
	return "Schede Collaudo - Tabella";
}

/**
 *
 * @properties={typeid:24,uuid:"951CD76D-0F47-4219-8F3A-E9CE18B5AC75"}
 */
function nfx_isProgram()
{
	return false;
}
