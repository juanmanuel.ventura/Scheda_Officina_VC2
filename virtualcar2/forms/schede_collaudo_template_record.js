/**
 * @properties={typeid:35,uuid:"40384D80-1954-4B80-AE7E-11D29B5DEC90",variableType:-4}
 */
var nfx_related = ["righe_azionamenti_e_manovre_list"];

/**
 *
 * @properties={typeid:24,uuid:"34C6CBB3-22CC-4108-8570-65F0E14AD9B8"}
 */
function nfx_defineAccess()
{
	return [true,true,true];
}

/**
 *
 * @properties={typeid:24,uuid:"0DC5CE7E-3903-4113-AE4C-EC3C18233226"}
 */
function nfx_getTitle()
{
	return "Schede Collaudo Template";
}

/**
 *
 * @properties={typeid:24,uuid:"95AF2E93-769B-4CF9-AE8C-70D02E63DF63"}
 */
function nfx_isProgram()
{
	return true;
}
