/**
 * @properties={typeid:35,uuid:"E0C3456B-F44D-4046-8860-7B980A620D8D",variableType:-4}
 */
var nfx_related = ["righe_schede_modifica_table","schede_modifica_dettaglio_record","skmod_aggiornamenti_soppressioni_table","skmod_anagrafica_disegni_table"];

/**
 *
 * @properties={typeid:24,uuid:"4DEEA158-F2FD-4788-86F8-6F880864614C"}
 */
function nfx_defineAccess(){
	return [true,true,true];
}

/**
 * @properties={typeid:24,uuid:"96F2BCED-D3CB-4736-8AD8-21756CAB1526"}
 */
function nfx_preAdd(){
	globals.vc2_setRightDepartments(true);
	if(typeof forms[controller.getName()].local_preAdd == "function"){
		forms[controller.getName()].local_preAdd();
	}
}

/**
 * @properties={typeid:24,uuid:"C50E3AF6-AD18-4AA0-82D4-08A5A1F68348"}
 */
function nfx_postAdd(){
	globals.vc2_setRightDepartments(false);
	if(typeof forms[controller.getName()].local_postAdd == "function"){
		forms[controller.getName()].local_postAdd();
	}
}

/**
 * @properties={typeid:24,uuid:"25941731-CE19-4C05-A995-FB63D8232E01"}
 */
function nfx_postAddOnCancel(){
	globals.vc2_setRightDepartments(false);
	if(typeof forms[controller.getName()].local_postAddOnCancel == "function"){
		forms[controller.getName()].local_postAddOnCancel();
	}
}

/**
 * @properties={typeid:24,uuid:"0DCA3558-50F9-4AC5-8150-C61822BFD606"}
 */
function nfx_preEdit(){
	globals.vc2_setRightDepartments(true);
	if(typeof forms[controller.getName()].local_preEdit == "function"){
		forms[controller.getName()].local_preEdit();
	}
}

/**
 * @properties={typeid:24,uuid:"F5E6053E-AE5C-4572-8AE1-3822DA48DEE3"}
 */
function nfx_postEdit(){
	globals.vc2_setRightDepartments(false);
	if(typeof forms[controller.getName()].local_postEdit == "function"){
		forms[controller.getName()].local_postEdit();
	}
}

/**
 * @properties={typeid:24,uuid:"35660329-3E0A-429F-A5C8-6B135119FE56"}
 */
function nfx_postEditOnCancel(){
	globals.vc2_setRightDepartments(false);
	if(typeof forms[controller.getName()].local_postEditOnCancel == "function"){
		forms[controller.getName()].local_postEditOnCancel();
	}
}

/**
 * @properties={typeid:24,uuid:"406CE1D6-7759-4252-B42B-100987A791C3"}
 */
function nfx_sks(){
	return [{icon: "report.png", tooltip:"Full report (BETA)", method:"report"}];
}

/**
 * @properties={typeid:24,uuid:"7FCF838A-9ADB-47C6-B640-08D33D75E12D"}
 */
function report(){
	var fields = [{title: "N. prop. mod.", field: "nr_proposta_modifica"},
	              {title: "Ente prop. mod.", field: "ente_proposta_modifica"},
	              {title: "Data prop. mod.", field: "data_proposta_modifica"},
	              {title: "Leader prop. mod.", field: "leader"},
	              {title: "Data inizio val.", field: "data_inizio_valutaz_economica"},
	              {title: "Data fine val.", field: "data_fine_valutaz_economica"},
	              {title: "N. cid", field: "numero_cid"},
	              {title: "Gruppo funzionale", field: "gruppo_funzionale"},
	              {title: "Sottogruppo funzionale", field: "gruppo_funzionale2"},
	              {title: "Gruppo 79", field: "complessivo"},
	              {title: "N. CPL Interessati", field: "complessivi_interessati"},
	              {title: "Tipo approvazione", field: "tipo_approvazione"},
	              {title: "Data approvazione", field: "data_approv_sk_prop_mod"},
	              {title: "Data annullamento", field: "data_annul_sk_prop_mod"},
	              {title: "Approvato da", field: "approvato_da"},
	              {title: "Causale", field: "causale_modifica"},
	              {title: "Descrizione causale", field: "k8_schede_modifica_to_xch_srcau00f_in.mcdcau"},
	              {title: "Descrizione modifica", field: "k8_schede_modifica_to_k8_schede_modifica_righe.descrizione_proposta_modifica"},
	              {title: "Progetto", field: "modello"},
	              {title: "Versione", field: "versione"},
	              {title: "Codice vettura", field: "codice_vettura"},
	              {title: "Tipo introduzione", field: "flag_modifica_da_gestire"},
	              {title: "Rif. reponsabilità economica", field: "rif_responsabilita_economica"},
	              {title: "Stato scheda", field: "stato_scheda"},
	              {title: "N. scheda anomalia", field: "nr_scheda_anomalia"},
	              {title: "Ente anomalia", field: "ente_inserimento_anomalia"},
	              {title: "N. segnalazione", field: "k8_schede_modifica_to_k8_schede_anomalie.numero_segnalazione"},
	              {title: "Ente segnalazione", field: "k8_schede_modifica_to_k8_schede_anomalie.ente_segnalazione_anomalia"},
	              {title: "Demerito", field: "k8_schede_modifica_to_k8_schede_anomalie.punteggio_demerito"},
	              {title: "Segnalato da", field: "k8_schede_modifica_to_k8_schede_anomalie.segnalato_da"},
	              {title: "Registrazione segnalazione", field: "k8_schede_modifica_to_k8_schede_anomalie.data_registrazione_segnalaz"},
	              {title: "Data rilevazione", field: "k8_schede_modifica_to_k8_schede_anomalie.data_rilevazione"},
	              {title: "Chiusura segnalazione", field: "k8_schede_modifica_to_k8_schede_anomalie.data_chiusura_segnalazione"},
	              {title: "Tipo anomalia", field: "k8_schede_modifica_to_k8_schede_anomalie.tipo_anomalia"},
	              {title: "Sottotipo anomalia", field: "k8_schede_modifica_to_k8_schede_anomalie.sottotipo_anomalia"},
	              {title: "Responsabile", field: "k8_schede_modifica_to_k8_schede_anomalie.responsabile"},
	              {title: "Data assegnazione leader", field: "k8_schede_modifica_to_k8_schede_anomalie.data_assegnazione_leader"},
	              {title: "Chiusura anomalia", field: "k8_schede_modifica_to_k8_schede_anomalie.data_chiusura"},
	              {title: "Apertura scheda modifica", field: "data_creazione_as400"},
	              {title: "Data prevista attuazione", field: "k8_schede_modifica_to_k8_lettere_modifica.data_prevista_attuazione"},
	              {title: "Data introduzione modifica", field: "k8_schede_modifica_to_k8_lettere_modifica.data_introduzione_modifica_1"},
	              {title: "Descrizione anomalia", field: "k8_schede_modifica_to_k8_schede_anomalie.k8_descrizione"},
	              {title: "Analisi anomalia", field: "k8_schede_modifica_to_k8_schede_anomalie.k8_analisi"},
	              {title: "Provvedimento anomalia", field: "k8_schede_modifica_to_k8_schede_anomalie.k8_provvedimento"}];
	globals.vc2_fullReport(controller.getName(),fields,"skm");
}
