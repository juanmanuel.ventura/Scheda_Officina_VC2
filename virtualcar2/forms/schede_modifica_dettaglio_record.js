/**
 *
 * @properties={typeid:24,uuid:"A7333143-89F3-4BA5-8101-79D3BFA65440"}
 */
function nfx_defineAccess()
{
	return [false,false,false];
}

/**
 *
 * @properties={typeid:24,uuid:"F546D5AB-2912-46B8-8414-35DF3AB4C265"}
 */
function nfx_getTitle()
{
	return "Dettaglio";
}

/**
 *
 * @properties={typeid:24,uuid:"FB1279B6-AF9E-4312-A1E2-119F6D4B9F92"}
 */
function nfx_isProgram()
{
	return false;
}
