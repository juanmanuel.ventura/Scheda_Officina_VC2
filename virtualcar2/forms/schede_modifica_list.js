/**
 *
 * @properties={typeid:24,uuid:"108F5137-D231-4D02-8BE5-B4B77044EBCD"}
 */
function nfx_defineAccess()
{
	return [false,false,false];
}

/**
 *
 * @properties={typeid:24,uuid:"E7CA8A45-161B-4262-9DE8-3C6E92DC3759"}
 */
function nfx_getTitle()
{
	return "Sc. Modifica";
}

/**
 *
 * @properties={typeid:24,uuid:"98877E6B-7FA2-4779-A776-27A5B5E34E00"}
 */
function nfx_isProgram()
{
	return false;
}
