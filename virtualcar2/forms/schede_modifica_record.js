/**
 * @properties={typeid:35,uuid:"148A36B9-4EDF-4BA6-999E-69E4E5A09357",variableType:-4}
 */
var blockedFields = [];

/**
 * @properties={typeid:35,uuid:"C3ED69F1-7E62-4A86-8E8E-13C684ED4999",variableType:-4}
 */
var disabledFields = [];

/**
 * @properties={typeid:35,uuid:"80EB509D-F364-49B8-8480-3A876C387C48",variableType:-4}
 */
var requiredFields = [];

/**
 * @properties={typeid:24,uuid:"28CE2DDE-3D6C-4D53-93DF-AF075B78906D"}
 */
function nfx_defineAccess() {
	return [false, false, false];
}

/**
 * @properties={typeid:24,uuid:"745F4352-137F-4301-B909-44F72D62FC37"}
 */
function nfx_getTitle() {
	return "Schede Modifica";
}

/**
 * @properties={typeid:24,uuid:"7C62DE78-AE61-4C68-882A-87669E688872"}
 */
function nfx_isProgram() {
	return true;
}

/**
 * @properties={typeid:24,uuid:"DC9C79E3-3687-491A-8D2C-1E853C51318B"}
 */
function local_preAdd() {
	colorize();
}

/**
 * @properties={typeid:24,uuid:"3A75D9AF-9942-405A-94AE-EF5524066753"}
 */
function local_postAdd() {
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"669C29A0-29AE-4E14-B5D2-56FEDA4B9064"}
 */
function local_postAddOnCancel() {
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"DFA8825E-975F-4785-BD70-6A2BE76346A2"}
 */
function local_preEdit() {
	colorize();
}

/**
 * @properties={typeid:24,uuid:"891C09F8-952D-4039-8A05-5874EDA42010"}
 */
function local_postEdit() {
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"95F86D04-0938-40C1-ABAE-8E6216A12295"}
 */
function local_postEditOnCancel() {
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"98C2BA3F-E394-4C69-B288-DDC3E5EC0538"}
 */
function colorize() {
	globals.utils_colorize(controller.getName(), requiredFields, "#FFFFC4");
	globals.utils_colorize(controller.getName(), blockedFields, "#E0E0E0");
	globals.utils_enable(controller.getName(), disabledFields, false);
}

/**
 * @properties={typeid:24,uuid:"A28A31CA-B317-4B3C-B36A-72A7B9390AD0"}
 */
function decolorize() {
	globals.utils_colorize(controller.getName(), requiredFields.concat(blockedFields), "#FFFFFF");
	globals.utils_enable(controller.getName(), disabledFields, true);
}

/**
 * @properties={typeid:24,uuid:"E2C632B9-6434-4FEA-9F00-2A4CE250ECE0"}
 */
function nextLM(event) {
	if (k8_schede_modifica_to_k8_lettere_modifica) {
		var _l = k8_schede_modifica_to_k8_lettere_modifica;
		_l.setSelectedIndex(_l.getSelectedIndex() + 1);
		databaseManager.recalculate(foundset.getSelectedRecord());
	}
}

/**
 * @properties={typeid:24,uuid:"1898B47C-D35E-4B29-BAD3-C5A4E59A6B2C"}
 */
function previousLM(event) {
	if (k8_schede_modifica_to_k8_lettere_modifica) {
		var _l = k8_schede_modifica_to_k8_lettere_modifica;
		_l.setSelectedIndex(_l.getSelectedIndex() - 1);
		databaseManager.recalculate(foundset.getSelectedRecord());
	}
}

/**
 * @properties={typeid:24,uuid:"F27357EB-B9AC-469B-9A37-B2D3D8D7CEA5"}
 */
function goToForm(event) {
	var elem = event.getElementName();
	if (elem == "sa" && k8_schede_modifica_to_k8_schede_anomalie.ente_segnalazione_anomalia && k8_schede_modifica_to_k8_schede_anomalie.numero_segnalazione) {
		globals.nfx_goToProgramAndRecord("segnalazioni_anomalie_record", k8_schede_modifica_to_k8_schede_anomalie.k8_anomalie_id, "k8_anomalie_id");
		return;
	} else if (elem == "ska" && k8_schede_modifica_to_k8_schede_anomalie.ente && k8_schede_modifica_to_k8_schede_anomalie.numero_scheda) {
		globals.nfx_goToProgramAndRecord("schede_anomalie_record", k8_schede_modifica_to_k8_schede_anomalie.k8_anomalie_id, "k8_anomalie_id");
		return;
	} else if (elem == "skm") {
		return;
	}
}

/**
 * @properties={typeid:24,uuid:"CD1677F7-5D64-47DF-B4A7-94A63A48D8ED"}
 */
function showDescriptionAS400(event) {
	var rel = k8_schede_modifica_to_k8_anagrafica_disegni;
	if (rel) {
		var txt = rel.descrizione;
		//FS
		var menu = null;
		var item = null;
		if (txt) {
			//FS
			//dep 	item = plugins.popupmenu.createMenuItem(txt,copyToClipboard);
			//dep   item.setMethodArguments([txt]);
			menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem(txt, copyToClipboard);
			item.methodArguments = [txt];

		} else {
			//dep	item = plugins.popupmenu.createMenuItem("Non presente in anagrafica...", null);
			//menu = plugins.window.createPopupMenu();
			menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem("Non presente in anagrafica...", null);

		}
		//FS
		//dep		plugins.popupmenu.showPopupMenu(elements.codice_disegno, [item]);
		menu.show(elements.codice_disegno);
	}
}

/**
 * @properties={typeid:24,uuid:"2ED6F20F-5787-48DD-92A7-A3ABFEE81909"}
 */
function showDescription(event) {
	var rel = k8_schede_modifica_to_k8_distinte_progetto_core;
	if (rel) {
		var menu = null;
		var txt = rel.descrizione_cpl;
		var item = null;
		if (txt) {
			//FS
			//dep			item = plugins.popupmenu.createMenuItem(txt, copyToClipboard);
			menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem(txt, copyToClipboard);
			item.methodArguments = [txt];
			//dep			item.setMethodArguments([txt]);
		} else {
			//dep			item = plugins.popupmenu.createMenuItem("Non presente in distinta...", null);
			menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem("Non presente in distinta...", null);
		}
		//FS
		//dep		plugins.popupmenu.showPopupMenu(elements.complessivo, [item]);
		menu.show(elements.complessivo);
	}
}

/**
 * @properties={typeid:24,uuid:"8E62C731-9063-48CD-94FA-E8D606C2FFD5"}
 */
function copyToClipboard(txt) {
	application.setClipboardContent(txt);
}
