/**
 * @properties={typeid:35,uuid:"F52F3F30-4B1C-41B8-B57A-CA311D64020D",variableType:-4}
 */
var blockedFields = [];

/**
 * @properties={typeid:35,uuid:"949D9490-112A-48EB-A113-E2E4C3A77D9C",variableType:-4}
 */
var disabledFields = [];

/**
 * @properties={typeid:35,uuid:"3FE51E8C-F2D7-438D-B54B-C30F56F9B367",variableType:-4}
 */
var requiredFields = [];

/**
 * @properties={typeid:24,uuid:"AD3B9032-1C4B-484C-ADE9-2A7943A9864C"}
 */
function nfx_defineAccess(){
	return [false,false,false];
}

/**
 * @properties={typeid:24,uuid:"01873B70-449E-4AC3-BE46-AB37A90CAA99"}
 */
function nfx_getTitle(){
	return "Schede Modifica";
}

/**
 * @properties={typeid:24,uuid:"DCBF38DB-88AD-4E06-86D8-066D84DB953C"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"91B1E615-0CD7-4BE7-8523-6BDA84E1AEA8"}
 */
function local_preAdd(){
	colorize();
}

/**
 * @properties={typeid:24,uuid:"D8998328-DEB7-4C93-AD0A-0FD19AA4EA27"}
 */
function local_postAdd(){
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"228E734F-8B64-46A0-93AB-7AE522D3FE18"}
 */
function local_postAddOnCancel(){
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"6602F552-9132-4A96-86B6-182A8909DC0F"}
 */
function local_preEdit(){
	colorize();
}

/**
 * @properties={typeid:24,uuid:"82BB6EAB-90ED-43F6-A483-6D157ED7B140"}
 */
function local_postEdit(){
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"E5B595BE-A7DC-48F8-949C-C901122C9B5D"}
 */
function local_postEditOnCancel(){
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"13E65382-1E9E-4D71-9CCB-8A42F42ABF05"}
 */
function colorize(){
	globals.utils_colorize(controller.getName(),requiredFields,"#FFFFC4");
	globals.utils_colorize(controller.getName(),blockedFields,"#E0E0E0");
	globals.utils_enable(controller.getName(),disabledFields,false);
}

/**
 * @properties={typeid:24,uuid:"C5D94B23-52D2-46B0-9484-C978474AE0BD"}
 */
function decolorize(){
	globals.utils_colorize(controller.getName(),requiredFields.concat(blockedFields),"#FFFFFF");
	globals.utils_enable(controller.getName(),disabledFields,true);
}
