/**@type {Array<Object>}
 * @properties={typeid:35,uuid:"7A79C036-4679-4186-84D8-8F6DDECAA307",variableType:-4}
 */
var _choices = {"Ente (SKM)"						: {dataprovider: "ente_proposta_modifica",															name: "ente_proposta_modifica",				size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.COMBOBOX,			editable: false,	valuelist: "gestione_anomalie$enti",								format: null,					method: null,							button: null},
                "N° (SKM)"							: {dataprovider: "nr_proposta_modifica",															name: "nr_proposta_modifica",				size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null},
                "Stato (SKM)"						: {dataprovider: "stato_scheda",																	name: "stato_scheda",						size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: "gestione_anomaile_stato",								format: null,					method: null,							button: null},
                "Causale modifica"					: {dataprovider: "causale_modifica",																name: "causale_modifica",					size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Approvazione"						: {dataprovider: "tipo_approvazione",																name: "tipo_approvazione",					size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "CID"								: {dataprovider: "numero_cid",																		name: "numero_cid",							size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null},
                "Annulata?"							: {dataprovider: "annullata",																		name: "annullata",							size: 20,	align: SM_ALIGNMENT.CENTER,	type: JSField.CHECKS,			editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Inserimento"						: {dataprovider: "data_inserimento",																name: "data_inserimento",					size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Proposta modifica"					: {dataprovider: "data_proposta_modifica",															name: "data_proposta_modifica",				size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Leader"							: {dataprovider: "leader",																			name: "leader",								size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Cod. modello"						: {dataprovider: "modello",																			name: "modello",							size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: false,	valuelist: "progetti",												format: null,					method: null,							button: null},
                "Versione"							: {dataprovider: "versione",																		name: "versione",							size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: false,	valuelist: "schede_modifica$versione",								format: null,					method: null,							button: null},
                "Gruppo funzionale"					: {dataprovider: "gruppo_funzionale",																name: "gruppo_funzionale",					size: 150,	align: SM_ALIGNMENT.LEFT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "Gruppi",												format: null,					method: null,							button: null},
                "Sottogruppo"						: {dataprovider: "gruppo_funzionale2",																name: "sottogruppo_funzionale",				size: 20,	align: SM_ALIGNMENT.CENTER,	type: JSField.TYPE_AHEAD,		editable: false,	valuelist: "schede_modifica$sottogruppi_funzionali",				format: null,					method: null,							button: null},
                "N° CPL"							: {dataprovider: "complessivi_interessati",															name: "complessivi_interessati",			size: 130,	align: SM_ALIGNMENT.RIGHT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null},
                "N° matricola"						: {dataprovider: "nr_matricola",																	name: "nr_matricola",						size: 130,	align: SM_ALIGNMENT.RIGHT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null},
                "N° matricola motore"				: {dataprovider: "nr_matricola_motore",																name: "nr_matricola_motore",				size: 130,	align: SM_ALIGNMENT.RIGHT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null},
                "Tipo cambio"						: {dataprovider: "tipo_cambio",																		name: "tipo_cambio",						size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "N° matricola cambio"				: {dataprovider: "nr_matricola_cambio",																name: "nr_matricola_cambio",				size: 130,	align: SM_ALIGNMENT.RIGHT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null},
                "Gruppo 79"							: {dataprovider: "k8_schede_modifica_to_k8_distinte_progetto_core.complessivo",						name: "complessivo",						size: 250,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Cod. disegno"						: {dataprovider: "numero_disegno_int",																name: "codice_disegno",						size: 250,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: true,		valuelist: "schede_modifica$componenti_from_distinta",				format: null,					method: null,							button: null},
                "Inizio valutazione"				: {dataprovider: "data_inizio_valutaz_economica",													name: "data_inizio_valutaz_economica",		size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Fine valutazione"					: {dataprovider: "data_fine_valutaz_economica",														name: "data_fine_valutaz_economica",		size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Resp. economica"					: {dataprovider: "rif_responsabilita_economica",													name: "rif_responsabilita_economica",		size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Tipo componente"					: {dataprovider: "tipo_componente",																	name: "tipo_componente",					size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Approvazione team"					: {dataprovider: "approvazione_team",																name: "approvazione_team",					size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Approvato da"						: {dataprovider: "approvato_da",																	name: "approvato_da",						size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Delibera funzionale"				: {dataprovider: "data_delibera_funzionale",														name: "data_delibera_funzionale",			size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Delibera attrezzam."				: {dataprovider: "data_delibera_attrezzamento",														name: "data_delibera_attrezzamento",		size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "N° Esperimento 1"					: {dataprovider: "nr_esperimento1",																	name: "nr_esperimento1",					size: 130,	align: SM_ALIGNMENT.RIGHT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null},
                "N° Esperimento 2"					: {dataprovider: "nr_esperimento2",																	name: "nr_esperimento2",					size: 130,	align: SM_ALIGNMENT.RIGHT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null},
                "Fine esperimenti"					: {dataprovider: "data_fine_esperimenti",															name: "data_fine_esperimenti",				size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Data installazione"				: {dataprovider: "data_eff_instal_modifica",														name: "data_eff_instal_modifica",			size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Ente (SA)"							: {dataprovider: "k8_schede_modifica_to_k8_schede_anomalie.ente_segnalazione_anomalia",				name: "ente_segnalazione_anomalia",			size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.COMBOBOX,			editable: false,	valuelist: "gestione_anomalie$enti",								format: null,					method: null,							button: null},
                "N° (SA)"							: {dataprovider: "k8_schede_modifica_to_k8_schede_anomalie.numero_segnalazione",					name: "numero_segnalazione",				size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null},
                "Stato (SA)"						: {dataprovider: "k8_schede_modifica_to_k8_schede_anomalie.stato_segnalazione",						name: "stato_segnalazione",					size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: "gestione_anomalie_stato",								format: null,					method: null,							button: null},
                "Ente (SKA)"						: {dataprovider: "k8_schede_modifica_to_k8_schede_anomalie.ente",									name: "ente",								size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.COMBOBOX,			editable: false,	valuelist: "gestione_anomalie$enti",								format: null,					method: null,							button: null},
                "N° (SKA)"							: {dataprovider: "k8_schede_modifica_to_k8_schede_anomalie.numero_scheda",							name: "numero_scheda",						size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null},
                "Stato (SKA)"						: {dataprovider: "k8_schede_modifica_to_k8_schede_anomalie.stato_anomalia",							name: "stato_anomalia",						size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: "gestione_anomaile_stato",								format: null,					method: null,							button: null},
                "Data approvazione (SKM)"			: {dataprovider: "data_approv_sk_prop_mod",															name: "data_approv_sk_prop_mod",			size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Data emissione (LM)"				: {dataprovider: "k8_schede_modifica_to_k8_lettere_modifica.data_emissione",						name: "data_emissione",						size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Tipo (LM)"							: {dataprovider: "k8_schede_modifica_to_k8_lettere_modifica.tipo",									name: "tipo",								size: 130,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Prevista attuazione (LM)"			: {dataprovider: "k8_schede_modifica_to_k8_lettere_modifica.data_prevista_attuazione",				name: "data_prevista_attuazione",			size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Richiesta assembly (LM)"			: {dataprovider: "k8_schede_modifica_to_k8_lettere_modifica.richiesta_assembly",					name: "richiesta_assembly",					size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Introduzione modifica (LM)"		: {dataprovider: "k8_schede_modifica_to_k8_lettere_modifica.data_introduzione_modifica_1",			name: "data_introduzione_modifica_1",		size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Numero assembly (LM)"				: {dataprovider: "k8_schede_modifica_to_k8_lettere_modifica.numero_assembly_1",						name: "numero_assembly_1",					size: 130,	align: SM_ALIGNMENT.RIGHT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null}};

/**
 * @properties={typeid:35,uuid:"11CCCFBF-8B4F-4673-BA3F-329A4FA890E4",variableType:-4}
 */
var _fields = ["Ente (SKM)","N° (SKM)","Stato (SKM)","Causale modifica","Approvazione","CID","Annulata?","Inserimento","Proposta modifica","Leader","Cod. modello","Versione","Gruppo funzionale","Sottogruppo","N° CPL","N° matricola","N° matricola motore","Tipo cambio","N° matricola cambio","Gruppo 79","Cod. disegno","Inizio valutazione","Fine valutazione","Resp. economica","Tipo componente","Approvazione team","Approvato da","Delibera funzionale","Delibera attrezzam.","N° Esperimento 1","N° Esperimento 2","Fine esperimenti","Data installazione","Ente (SA)","N° (SA)","Stato (SA)","Ente (SKA)","N° (SKA)","Stato (SKA)","Data approvazione (SKM)","Data emissione (LM)","Tipo (LM)","Prevista attuazione (LM)","Richiesta assembly (LM)","Introduzione modifica (LM)","Numero assembly (LM)"];

/**
 * @properties={typeid:24,uuid:"D8EDAA7B-38D9-4DE6-884B-76EDB16756B2"}
 */
function nfx_defineAccess(){
	return [false,false,false];
}

/**
 * @properties={typeid:24,uuid:"92EB4BE4-E428-4E31-8639-8C460D5348C0"}
 */
function nfx_getTitle(){
	return "Schede Modifica - Tabella";
}

/**
 * @properties={typeid:24,uuid:"5A52D1FC-145B-47B1-9B7A-E05A6B9A8052"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"075A8057-8AA8-4B89-A230-214B353120AC"}
 */
function nfx_sks(){
	return [{icon: "report.png",	tooltip:"Full report (BETA)",	method:"report"},
	        {icon: "table.png",		tooltip:"Scelta colonne",		method:"localFieldChooserWrap"}];
}

/**
 * @properties={typeid:24,uuid:"04FA4F1B-45D4-4CEA-BB35-111A3B7AE9AD"}
 */
function localFieldChooserWrap(){
	forms._vc2_colums_customizer.opnePopup(_choices,_fields,controller.getName(),"_fields");
	//FS
	globals.vc2_drawFieldsInForm(controller.getName(),_choices,_fields);
}
