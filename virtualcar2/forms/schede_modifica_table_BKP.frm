dataSource:"db:/ferrari/k8_schede_modifica",
extendsID:"D8B33347-CA21-433E-A6FA-F6731F577BD0",
items:[
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"data_inserimento",
displayType:5,
editable:false,
location:"1900,30",
name:"data_inserimento",
size:"120,20",
styleClass:"table",
text:"Data Inserimento",
typeid:4,
uuid:"00234CA2-DDC0-49AD-8506-EB350BAE9365"
},
{
labelFor:"gruppo_funzionale2",
location:"1440,10",
mediaOptions:14,
size:"130,20",
styleClass:"table",
tabSeq:-1,
text:"Gruppo funzionale 2",
typeid:7,
uuid:"067AB004-168C-4B11-8233-A54B502CE3EF"
},
{
labelFor:"nr_scheda_anomalia",
location:"160,10",
mediaOptions:14,
size:"130,20",
styleClass:"table",
tabSeq:-1,
text:"N° scheda anomalia",
typeid:7,
uuid:"0CC28F25-98A1-462F-A4F4-5D9EF496A3AB"
},
{
labelFor:"modello",
location:"430,10",
mediaOptions:14,
size:"90,20",
styleClass:"table",
tabSeq:-1,
text:"Modello",
typeid:7,
uuid:"12471AA6-9A48-4A69-8953-E77CD789A8C7"
},
{
labelFor:"stato_scheda",
location:"940,10",
mediaOptions:14,
size:"90,20",
styleClass:"table",
tabSeq:-1,
text:"Stato scheda",
typeid:7,
uuid:"12BD399F-32D8-46FA-9CF2-31E4CA5D9F14"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"leader",
editable:false,
location:"2810,30",
name:"leader",
size:"360,20",
styleClass:"table",
text:"Leader",
typeid:4,
uuid:"13A0F61A-62DB-409A-8841-31192F11C60C"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"gruppo_funzionale2",
editable:false,
location:"1440,30",
name:"gruppo_funzionale2",
size:"130,20",
styleClass:"table",
text:"Gruppo Funzionale2",
typeid:4,
uuid:"19077794-F3CF-42DF-9A4F-59E53AD73F4F"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"rif_responsabilita_economica",
editable:false,
location:"1740,30",
name:"rif_responsabilita_economica",
size:"150,20",
styleClass:"table",
text:"Rif Responsabilita Economica",
typeid:4,
uuid:"1D9566A0-0DA7-4A71-8061-4148DD4F0C61"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"ente_proposta_modifica",
editable:false,
location:"2650,30",
name:"ente_proposta_modifica",
size:"150,20",
styleClass:"table",
text:"Ente Proposta Modifica",
typeid:4,
uuid:"1D9667A2-0118-45A1-80C4-DF006DD8007E"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"nr_proposta_modifica",
editable:false,
location:"10,30",
name:"nr_proposta_modifica",
size:"140,20",
styleClass:"table",
text:"Nr Proposta Modifica",
typeid:4,
uuid:"20E9539F-1ABD-4DA1-9E32-D6768EA5C59B"
},
{
labelFor:"data_inserimento",
location:"1900,10",
mediaOptions:14,
size:"120,20",
styleClass:"table",
tabSeq:-1,
text:"Data inserimento",
typeid:7,
uuid:"3F3B4E82-B99B-41D7-96D1-430511BFD885"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"gruppo_funzionale",
editable:false,
location:"1310,30",
name:"gruppo_funzionale",
size:"120,20",
styleClass:"table",
text:"Gruppo Funzionale",
typeid:4,
uuid:"4880D646-44C7-4C59-AA38-6E176B74C49B"
},
{
labelFor:"tipo_componente",
location:"1040,10",
mediaOptions:14,
size:"120,20",
styleClass:"table",
tabSeq:-1,
text:"Tipo componente",
typeid:7,
uuid:"4F05C759-65CE-4A8C-9D39-D9BA193FF9BA"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"modello",
editable:false,
location:"430,30",
name:"modello",
size:"90,20",
styleClass:"table",
text:"Modello",
typeid:4,
uuid:"52743944-2303-4E54-9F02-333B2C1F316C",
valuelistID:"8DBD3071-35BE-4D55-B40F-F9B6FB9F8DD6"
},
{
labelFor:"testo",
location:"3180,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Testo",
typeid:7,
uuid:"53831664-BABD-40C2-A6DF-45A349492B23"
},
{
labelFor:"rif_responsabilita_economica",
location:"1740,10",
mediaOptions:14,
size:"150,20",
styleClass:"table",
tabSeq:-1,
text:"Rif resp economica",
typeid:7,
uuid:"6292483D-D238-493B-9DA1-A7661F885083"
},
{
labelFor:"nr_matricola_cambio",
location:"1580,10",
mediaOptions:14,
size:"150,20",
styleClass:"table",
tabSeq:-1,
text:"N° matricola cambio",
typeid:7,
uuid:"63AA6981-1E0C-4128-AC32-8ED172EDCBB3"
},
{
labelFor:"gruppo_funzionale",
location:"1310,10",
mediaOptions:14,
size:"120,20",
styleClass:"table",
tabSeq:-1,
text:"Gruppo funzionale",
typeid:7,
uuid:"674F1A08-4E2F-48C0-93F6-2E15C82B65A6"
},
{
labelFor:"codice_disegno",
location:"300,10",
mediaOptions:14,
size:"120,20",
styleClass:"table",
tabSeq:-1,
text:"Cod. disegno",
typeid:7,
uuid:"71DAF89C-235B-41C7-825D-9248DDF3CDF4"
},
{
labelFor:"nr_proposta_modifica",
location:"10,10",
mediaOptions:14,
size:"140,20",
styleClass:"table",
tabSeq:-1,
text:"N° proposta modifica",
typeid:7,
uuid:"72618508-CC20-4E64-8699-F5718122AE54"
},
{
labelFor:"versione",
location:"530,10",
mediaOptions:14,
size:"150,20",
styleClass:"table",
tabSeq:-1,
text:"Versione",
typeid:7,
uuid:"74D1DA43-0E44-4CBC-8A20-7B87DB11B51F"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"nr_matricola_motore",
editable:false,
location:"1170,30",
name:"nr_matricola_motore",
size:"130,20",
styleClass:"table",
text:"Nr Matricola Motore",
typeid:4,
uuid:"7672E2F6-84CA-48E2-BF67-C2CAD0C07F5F"
},
{
labelFor:"nr_matricola_motore",
location:"1170,10",
mediaOptions:14,
size:"130,20",
styleClass:"table",
tabSeq:-1,
text:"N° matricola motore",
typeid:7,
uuid:"77EC343D-019A-409E-8822-B1AE599D89D6"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"nr_variante_prop_modifica",
editable:false,
location:"2360,30",
name:"nr_variante_prop_modifica",
size:"171,20",
styleClass:"table",
text:"Nr Variante Prop Modifica",
typeid:4,
uuid:"7B548E34-4CCE-45A5-B92F-3DF5F93DE3ED"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"stato_scheda",
displayType:2,
editable:false,
location:"940,30",
name:"stato_scheda",
size:"90,20",
styleClass:"yellow",
text:"Stato Scheda",
typeid:4,
uuid:"81760BBB-4600-4D2A-8D35-433CCC0BB1E7",
valuelistID:"924512BA-159E-491C-800A-0F20E9E87F3A"
},
{
labelFor:"nr_matricola",
location:"690,10",
mediaOptions:14,
size:"140,20",
styleClass:"table",
tabSeq:-1,
text:"N° matricola",
typeid:7,
uuid:"821FD482-1C0F-4EBF-AF78-8964ABCAC4C8"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"k8_schede_modifica_to_k8_righe_schede_modifica.testo",
editable:false,
location:"3180,30",
name:"testo",
size:"560,20",
styleClass:"table",
text:"Testo",
typeid:4,
uuid:"879BA69A-3CFE-43BA-88D5-8E2FBFA9675B"
},
{
labelFor:"nr_variante_prop_modifica",
location:"2360,10",
mediaOptions:14,
size:"171,20",
styleClass:"table",
tabSeq:-1,
text:"N° variante prop modifica",
typeid:7,
uuid:"8D76AB58-72F6-47E7-B037-0F5C444BA1A2"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"data_prev_approvazione",
displayType:5,
editable:false,
location:"2540,30",
name:"data_prev_approvazione",
size:"100,20",
styleClass:"table",
text:"Data Prev Approvazione",
typeid:4,
uuid:"A0F333AD-5148-492E-B610-5735FED47DF0"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"tipo_componente",
editable:false,
location:"1040,30",
name:"tipo_componente",
size:"120,20",
styleClass:"table",
text:"Tipo Componente",
typeid:4,
uuid:"A51BE2DC-3294-4E4B-B3E1-587EAFE09DBB"
},
{
labelFor:"tipo_approvazione",
location:"2210,10",
mediaOptions:14,
size:"140,20",
styleClass:"table",
tabSeq:-1,
text:"Tipo approvazione",
typeid:7,
uuid:"A6FFB251-DAA8-453F-BCB4-0DCBABC65BA8"
},
{
labelFor:"leader",
location:"2810,10",
mediaOptions:14,
size:"80,20",
styleClass:"table",
tabSeq:-1,
text:"Leader",
typeid:7,
uuid:"A90A1FF1-6845-4CF8-80C5-619E4ECA4D0C"
},
{
height:480,
partType:5,
typeid:19,
uuid:"B6CEE704-1CF5-45AF-AE3A-6D27ED0F0CDF"
},
{
labelFor:"ente_inserimento_anomalia",
location:"2030,10",
mediaOptions:14,
size:"170,20",
styleClass:"table",
tabSeq:-1,
text:"Ente inserimento anomalia",
typeid:7,
uuid:"BDD6D4DB-398C-4789-9FF8-B64965339E08"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"codice_disegno",
editable:false,
location:"300,30",
name:"codice_disegno",
size:"120,20",
styleClass:"table",
text:"Codice Disegno",
typeid:4,
uuid:"C60B013D-5490-4855-9552-034F51FF1F87"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"tipo_cambio",
editable:false,
location:"840,30",
name:"tipo_cambio",
size:"90,20",
styleClass:"table",
text:"Tipo Cambio",
typeid:4,
uuid:"C7753F9F-6A20-432A-8370-B6D000D43CAE"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"tipo_approvazione",
editable:false,
location:"2210,30",
name:"tipo_approvazione",
size:"140,20",
styleClass:"table",
text:"Tipo Approvazione",
typeid:4,
uuid:"CBE576F8-582E-4DC3-B8FE-B73FD8ACCFB6"
},
{
labelFor:"ente_proposta_modifica",
location:"2650,10",
mediaOptions:14,
size:"150,20",
styleClass:"table",
tabSeq:-1,
text:"Ente proposta modifica",
typeid:7,
uuid:"CDCD078D-B86D-421D-A4D2-D0BDFC1E9653"
},
{
labelFor:"data_prev_approvazione",
location:"2540,10",
mediaOptions:14,
size:"100,20",
styleClass:"table",
tabSeq:-1,
text:"Data prev app.",
typeid:7,
uuid:"E39AFFCF-C11D-4476-B1CC-0C70D0FC0758"
},
{
labelFor:"tipo_cambio",
location:"840,10",
mediaOptions:14,
size:"90,20",
styleClass:"table",
tabSeq:-1,
text:"Tipo cambio",
typeid:7,
uuid:"E455DCBE-FE5E-4C8F-8660-41D4F51C75F3"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"versione",
editable:false,
location:"530,30",
name:"versione",
size:"150,20",
styleClass:"table",
text:"Versione",
typeid:4,
uuid:"ED227D3F-992D-4C13-A9A3-A005D0DC8BD4"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"nr_matricola_cambio",
editable:false,
location:"1580,30",
name:"nr_matricola_cambio",
size:"150,20",
styleClass:"table",
text:"Nr Matricola Cambio",
typeid:4,
uuid:"EF055033-7A8B-4D6F-87D2-A07DAFB2C616"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"nr_matricola",
editable:false,
location:"690,30",
name:"nr_matricola",
size:"140,20",
styleClass:"table",
text:"Nr Matricola",
typeid:4,
uuid:"F529E85F-98C4-4695-95CE-18E692666EF5"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"nr_scheda_anomalia",
editable:false,
location:"160,30",
name:"nr_scheda_anomalia",
size:"130,20",
styleClass:"table",
text:"Nr Scheda Anomalia",
typeid:4,
uuid:"F6B00DA0-1C60-4CFC-9EA2-7A35FBEDEAEF"
},
{
anchors:11,
borderType:"EtchedBorder,0,null,null",
dataProviderID:"ente_inserimento_anomalia",
editable:false,
location:"2030,30",
name:"ente_inserimento_anomalia",
size:"170,20",
styleClass:"table",
text:"Ente Inserimento Anomalia",
typeid:4,
uuid:"F78C4C1D-EC26-44BA-95D1-89DD5FD6F0F5"
}
],
name:"schede_modifica_table_BKP",
paperPrintScale:100,
showInMenu:true,
size:"3750,480",
styleName:"keeneight",
typeid:3,
uuid:"D2AC1659-1A3A-4372-A63B-B7DB1C753770",
view:3