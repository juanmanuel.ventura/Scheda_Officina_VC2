/**
 * @properties={typeid:24,uuid:"F1DBF9D1-BC19-4F0A-9A0B-EF31E2CF2EB1"}
 */
function nfx_defineAccess(){
	return [false,false,false];
}

/**
 * @properties={typeid:24,uuid:"A975697C-0D09-400C-ADAB-BA86A17AE4B2"}
 */
function nfx_getTitle(){
	return "Schede Modifica - Tabella";
}

/**
 * @properties={typeid:24,uuid:"B1D4021C-FCCA-4370-B755-9B7D7A22DBB5"}
 */
function nfx_isProgram(){
	return true;
}
