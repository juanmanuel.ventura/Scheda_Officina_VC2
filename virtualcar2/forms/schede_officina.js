/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"DE9261A1-A7AD-466C-9942-C3B0360BB045"}
 */
var nfx_orderBy = null;

/**
 * @properties={typeid:35,uuid:"25823778-40A2-4621-BE73-488912D8933B",variableType:-4}
 */
var nfx_related = ["righe_schede_officina_richiesta_record",
                   "righe_schede_officina_esecuzione_record",
                   "documenti_nas_record",
                   "software_table",
                   "schede_officina_note_record"];

/**
 * @properties={typeid:24,uuid:"9BD1C992-4D65-4DC7-AC38-97E8EC78F4CE"}
 */
function nfx_defineAccess() {
	return [true, true, true];
}

/**
 * @properties={typeid:24,uuid:"4D11344B-2268-4347-938E-7C9BA64FAC49"}
 */
function nfx_preAdd(){
	numero_scheda = calcolaNumeroScheda();
}

/**
 * @properties={typeid:24,uuid:"1E02A615-DCB7-49BD-80CA-46659E0D5EBC"}
 */
function calcolaNumeroScheda(){
	var offset = 3;
	//FS
//dep	var query = "select numero_scheda from " + controller.getTableName() + " where extract(year from data_richiesta_scheda) = ? order by data_richiesta_scheda desc";
	var query = "select numero_scheda from " +databaseManager.getDataSourceTableName(controller.getDataSource()) + " where extract(year from data_richiesta_scheda) = ? order by data_richiesta_scheda desc";

	var yearFourDigit = utils.dateFormat(new Date(),"yyyy");
	var yearTwoDigit = utils.dateFormat(new Date(),"yy");

//dep	var ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,[yearFourDigit],1);
var ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()),query,[yearFourDigit],1);

	var ns = ds.getValue(1,1);
	var number = (ns) ? ns.substring(0,ns.length-offset) : 0;
	number++;
	return number + "/" + yearTwoDigit;
}
