/**
 * @properties={typeid:24,uuid:"83142612-1509-44DC-8CB5-EED19581809C"}
 */
function nfx_defineAccess() {
	return [false, false, true];
}

/**
 * @properties={typeid:24,uuid:"AC308EED-6E52-463B-A014-32A9AFF9A57E"}
 */
function nfx_getTitle(){
	return "Note";
}

/**
 * @properties={typeid:24,uuid:"8B9E184C-DBCF-40A6-99E9-F578825BE1D3"}
 */
function nfx_isProgram(){
	return false;
}
