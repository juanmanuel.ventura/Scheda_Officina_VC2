/**
 * @properties={typeid:24,uuid:"3128741A-A4B5-4F12-8E82-2862ED0E4996"}
 */
function nfx_getTitle() {
	return "Schede Officina";
}

/**
 * @properties={typeid:24,uuid:"E0D57F50-8BDA-4CEB-A521-CBEAF2833173"}
 */
function nfx_isProgram() {
	return true;
}

/**
 * @properties={typeid:24,uuid:"7933F9C7-12C3-45F6-B45F-CEEAB5FA1F35"}
 */
function nfx_onRecordSelection() {
	setColorAttivitaFuoriCiclo();
	setColorMaterialiInfiammabili();
}

/**
 * @properties={typeid:24,uuid:"068F0DC7-F5D3-4B8E-840C-1B7D7EF71584"}
 */
function setColorAttivitaFuoriCiclo() {
	if(attivita_fuori_ciclo == "Si"){
		elements.attivita_fuori_ciclo.bgcolor = "#FF0000";
	}else if(attivita_fuori_ciclo == "No"){
		elements.attivita_fuori_ciclo.bgcolor = null;
	}
}

/**
 * @properties={typeid:24,uuid:"507935B8-0F38-49CE-9FF0-8FB167ED2A42"}
 */
function setColorMaterialiInfiammabili() {
	if(materiali_infiammabili == "Si"){
		elements.materiali_infiammabili.bgcolor = "#FF0000";
	}else if(materiali_infiammabili == "No"){
		elements.materiali_infiammabili.bgcolor = null;
	}
}
