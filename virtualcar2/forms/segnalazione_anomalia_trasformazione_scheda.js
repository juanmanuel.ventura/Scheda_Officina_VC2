/**
 * @properties={typeid:35,uuid:"9E7D3F85-23C9-44AB-91DD-83C9596A686E",variableType:-4}
 */
var responsabili = [];

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"7CEC2C89-CA07-4AD9-AA5A-E500DD6F95BE",variableType:4}
 */
var saved = -1;

/**
 * Callback method for when form is shown.
 *
 * @param {Boolean} firstShow form is shown first time after load
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"DB7C018A-5155-48D6-A4A1-1415F2151541"}
 */
function onShowForm(firstShow, event) {
	saved = -1;
}

/**
 * @properties={typeid:24,uuid:"15198B22-5273-4169-9436-6D296AE6F320"}
 */
function close(){
	saved = 1;
	//application.closeFormDialog();
	
	var w=controller.getWindow();
	w.destroy();
}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"F5A16D27-7BC4-42A9-BAC4-DF829F7A931E"}
 */
function onLoad(event) {
	// TODO Auto-generated method stub
}
