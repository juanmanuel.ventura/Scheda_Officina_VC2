/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"946BB68D-76F4-49F5-95BF-D041F1A78BEB",variableType:4}
 */
var activateWebServices = 0;

/**
 * @properties={typeid:35,uuid:"FA8E7B69-122F-46AE-81DF-40567ADB8141",variableType:-4}
 */
var data_registrazione_segnalaz_BPK = null;

/**
 * @properties={typeid:35,uuid:"C046DFE1-0511-4262-9ADF-D5D9AA38DEDE",variableType:-4}
 */
var nfx_related = ["anomalie_vetture_table","segnalazioni_anomalie_descrizione_record","segnalazioni_anomalie_analisi_record","segnalazioni_anomalie_dettaglio_record","documenti_nas_record","time_line_table","tag_table"];

/**
 * @properties={typeid:35,uuid:"ADC9740E-F998-4F07-855A-4B61F0C51182",variableType:-4}
 */
var onlyClose = null;

/**
 * @properties={typeid:35,uuid:"F3D8FB39-EFA4-4DDD-B2F7-1E7504A441DF",variableType:-4}
 */
var openSK = null;

/**
 * @properties={typeid:35,uuid:"52983450-95E3-4BA9-83D6-C56204E76125",variableType:-4}
 */
var stato_segnalazione_BPK = null;

/**
 * @properties={typeid:24,uuid:"4D4D1905-71C7-4734-BF97-FE3CB44B5751"}
 */
function nfx_preAdd(){
	var user = globals.vc2_getUserInfo(null);
	ente_segnalazione_anomalia = user.ente_as400;
	segnalato_da = user.user_as400;
	
	globals.vc2_setRightDepartments(true);
	if(typeof forms[controller.getName()].local_preAdd == "function"){
		forms[controller.getName()].local_preAdd();
	}
}

/**
 * @properties={typeid:24,uuid:"B7CAB72A-E8EB-4D64-B20F-089AF54B4B85"}
 */
function nfx_postAdd(){
	
	//application.output("opensk 1 : "+openSK);
	
	if(!data_registrazione_segnalaz && data_registrazione_segnalaz_BPK){
		data_registrazione_segnalaz = data_registrazione_segnalaz_BPK;
	}
	if(!stato_segnalazione && stato_segnalazione_BPK){
		stato_segnalazione = stato_segnalazione_BPK;
	}
	if(databaseManager.saveData()){
		data_registrazione_segnalaz_BPK = null;
		stato_segnalazione_BPK = null;
	}
	
	globals.vc2_setRightDepartments(false);
	if(typeof forms[controller.getName()].local_postAdd == "function"){
		forms[controller.getName()].local_postAdd();
	}
}

/**
 * @properties={typeid:24,uuid:"E86F4790-5BFF-42C4-9CC2-76C90D0C2F00"}
 */
function nfx_postAddOnCancel(){
	globals.vc2_setRightDepartments(false);
	if(typeof forms[controller.getName()].local_postAddOnCancel == "function"){
		forms[controller.getName()].local_postAddOnCancel();
	}
}

/**
 * @properties={typeid:24,uuid:"9C60CA55-98AC-4F51-99B8-BC7A56770211"}
 */
function nfx_preEdit(){
	globals.vc2_setRightDepartments(true);
	if(typeof forms[controller.getName()].local_preEdit == "function"){
		forms[controller.getName()].local_preEdit();
	}
}

/**
 * @properties={typeid:24,uuid:"9B626194-0133-4947-894B-135AC4FD3F98"}
 */
function nfx_postEdit(){
	modification_user_sa = globals.nfx_user;
	modification_date_sa = new Date();
	
	globals.vc2_setRightDepartments(false);
	if(typeof forms[controller.getName()].local_postEdit == "function"){
		forms[controller.getName()].local_postEdit();
	}
}

/**
 * @properties={typeid:24,uuid:"7A839BF4-E977-48D7-85D0-11A8D0F573FE"}
 */
function nfx_postEditOnCancel(){
	globals.vc2_setRightDepartments(false);
	if(typeof forms[controller.getName()].local_postEditOnCancel == "function"){
		forms[controller.getName()].local_postEditOnCancel();
	}
}

/**
 * @properties={typeid:24,uuid:"91DA1242-CA35-483D-9F15-A61D78F9F9AA"}
 */
function chiusuraSegnalazione(){
	if(stato_segnalazione != "CHIUSA"){
		if (plugins.dialogs.showInfoDialog("Cambio stato della segnalazione","Questa segnalazione verra' chiusa e non sara' possibile modificarla. Questa procedura NON genera una scheda anomalia, continuare?","Si","No") == "No"){
			return;
		}else{
			if (nfx_validate() == -1){
				return;
			}
			utente_chiusura_segnalazione = modification_user_sa = globals.nfx_user;
			data_chiusura_segnalazione = modification_date_sa = new Date();
			stato_segnalazione = "CHIUSA";
			forms.nfx_interfaccia_pannello_buttons.silentEditSave(controller.getName());
		}
	}else{
		plugins.dialogs.showErrorDialog("Errore","Impossibile completate l'operazione, la segnalazione è già chiusa","Ok");
	}

}

/**
 * @properties={typeid:24,uuid:"9227C29C-2984-43CD-AEF1-3B535BE95EC2"}
 */
function creazioneScheda(){
	if(stato_segnalazione != "CHIUSA"){
		if (plugins.dialogs.showInfoDialog("Cambio stato della scheda","Questa segnalazione verra' chiusa e trasformata in una scheda anomalia, continuare?","Si","No") == "No"){
			return;
		}else{
			
			//CONTROLLO CAMPO LEADER
			
			openSK = null;
			
			if(leader == null){
				plugins.dialogs.showErrorDialog("Errore","Inserisci Leader", "Ok");
				return;
			}
			
			if (nfx_validate() == -1){
				return;
			}
			/*salvo dei valori che andranno ripristinati
			  nel caso di mancata creazione scheda*/
			var cric = data_chiusura_richiesta;
			var cprev = data_prevista_chiusura_sc_anom;
			var canom = attribuzione;
			data_chiusura_richiesta = null;
			data_prevista_chiusura_sc_anom = null;
			attribuzione = null;
			//application.showFormInDialog(forms.segnalazione_anomalia_trasformazione_scheda,null,null,null,null,"Dati Scheda Anomalia",false,false,"sk_popup",true);

			var formq = forms.segnalazione_anomalia_trasformazione_scheda;
			var window = application.createWindow("sk_popup", JSWindow.MODAL_DIALOG);
			window.resizable = false;
			window.title = "Dati Scheda Anomalia";
			formq.controller.show(window);
			
			if(forms.segnalazione_anomalia_trasformazione_scheda.saved != -1){
				data_rilevazione_scheda = data_rilevazione;
				utente_chiusura_segnalazione = globals.nfx_user;
				data_chiusura_segnalazione = data_registrazione_scheda = new Date();
				stato_anomalia = "APERTA";
				stato_segnalazione = "CHIUSA";
				//Se non presente indico il tipo di approvazione di default
				var ta = tipo_approvazione;
				if(!tipo_approvazione){
					tipo_approvazione = "B";
				}
				//Questo è giusto stia qui, perché silentEditSave() fa la chiamata al servizio e deve sapere se aprire la SKA o mandare solo la chiusura della SA
				openSK = true;
				var success = forms.nfx_interfaccia_pannello_buttons.silentEditSave(controller.getName());
			}else{
				plugins.dialogs.showInfoDialog("Operazione annullata","L'operazione di trasformazione è stata annullata","Ok");
				var success = forms.segnalazione_anomalia_trasformazione_scheda.saved;
			}
			if (success == -1){
				//Apertura scheda non riuscita, ripristino i campi modificati:
				data_rilevazione_scheda = null;
				utente_chiusura_segnalazione = null;
				data_chiusura_segnalazione = null; 
				data_registrazione_scheda = null;
				stato_anomalia = null;
				stato_segnalazione = "APERTA";
				tipo_approvazione = ta;
				//campi settati tramite il popup di trasformazione
				data_chiusura_richiesta = cric;
				data_prevista_chiusura_sc_anom = cprev;
				attribuzione = canom;
				ente = null;
				responsabile = null;
			}else{
				modification_user_sa = globals.nfx_user;
				modification_date_sa = new Date();
				databaseManager.saveData();
				
				//Copia delle descrizioni + invio ad AS400
				if(k8_segnalazioni_full_to_k8_segn_desc_full && k8_segnalazioni_full_to_k8_segn_desc_full.getSize()){
					var n_desc = k8_segnalazioni_full_to_k8_segn_desc_full.getSize();
					for(var i=1;i<=n_desc;i++){
						try{
							application.output(application.output("Ente : "+foundset.ente));
							application.output(application.output("Numero_scheda : "+foundset.numero_scheda));
							
						var record = k8_segnalazioni_full_to_k8_segn_desc_full.getRecord(i);
						k8_segnalazioni_full_to_k8_anomalie_desc_full.newRecord();
						k8_segnalazioni_full_to_k8_anomalie_desc_full.testo = record.testo;
						
						}catch(Exc){
							application.output("Descrizione excpetion! k8_segnalazioni_full_to_k8_anomalie_desc_full");
						}
					}
					databaseManager.saveData();
					forms.schede_anomalie_descrizione.foundset.loadRecords(k8_segnalazioni_full_to_k8_anomalie_desc_full);
					forms.schede_anomalie_descrizione.sendToAS400();
				}
				
				//Copia delle analisi + invio ad AS400
				
				if(k8_segnalazioni_full_to_k8_segn_anal_full && k8_segnalazioni_full_to_k8_segn_anal_full.getSize()){
					var n_anal = k8_segnalazioni_full_to_k8_segn_anal_full.getSize();
					for(var i=1;i<=n_anal;i++){
						try{
							application.output("Numero_segnalazione : "+foundset.numero_segnalazione);
							application.output("vc2_null : "+scopes.globals.vc2_null);
							application.output("Ente segnalazione anomalia : "+ente_segnalazione_anomalia);
						
						var record = k8_segnalazioni_full_to_k8_segn_anal_full.getRecord(i);
						k8_segnalazioni_full_to_k8_anomalie_anal_full.newRecord();
						k8_segnalazioni_full_to_k8_anomalie_anal_full.testo = record.testo;
						}
						catch(exc){
							application.output("Analisi Exception! k8_segnalazioni_full_to_k8_segn_anal_full");
						}
						
					}
					databaseManager.saveData();
					forms.schede_anomalie_analisi.foundset.loadRecords(k8_segnalazioni_full_to_k8_anomalie_anal_full);
					forms.schede_anomalie_analisi.sendToAS400();
				}
				tipo_scheda = "ANOMALIA";
				databaseManager.saveData();
				globals.nfx_goToProgramAndRecord("schede_anomalie_record",k8_anomalie_id,"k8_anomalie_id");
				openSK=null;
				application.output("Settato openSK null!");
			}
		}
	}else{
		plugins.dialogs.showErrorDialog("Errore","Impossibile completate l'operazione, la segnalazione è già chiusa","Ok");
	}
	//application.output('OpensSK fine: '+openSK);
}

/**
 *
 * @properties={typeid:24,uuid:"BC4A6B2E-D965-4F44-B803-2F37F702E19C"}
 */
function dataValidation(data){
	
	
	//application.output("opensk 2 : "+openSK);	
	
	
	var allFields;
	if (data){
		allFields = data;
	}else{
		allFields = {"ente_segnalazione_anomalia"	:	"Ente (segnalazione)",
		             "data_rilevazione"				:	"Data rilevazione",
		             "codice_modello"				:	"Cod. modello",
		             "codice_versione"				:	"Versione",
		             "gruppo_funzionale"			:	"Gruppo funzionale",
		             "tipo_anomalia"				:	"Tipo anomalia",
		             "segnalato_da"					:	"Segnalato da",
		          //   "leader"						:	"Leader",
		             "codice_disegno"				:	"Cod. disegno\n(Nel caso sia sconosciuto, inserire nel campo Cod.disegno il Gruppo 79 corrispondente)",
		             "tipo_componente"				:	"Tipo componente",
		             "codice_difetto"				:	"Difetto",
		             "punteggio_demerito"			:	"Demerito",
		             "attribuzione"					:	"Causale anomalia"};
	}
	
	if (!stato_segnalazione){
		stato_segnalazione = "APERTA";
	}else{
		stato_segnalazione_BPK = stato_segnalazione;
	}
	
	if (!data_registrazione_segnalaz){
		data_registrazione_segnalaz = new Date();
	}else{
		data_registrazione_segnalaz_BPK = data_registrazione_segnalaz;
	}
	
	if(!tipo_scheda){
		tipo_scheda = "SEGNALAZIONE";
	}
	
	if(gruppo_funzionale && globals.vc2_isSottogruppoFunzionaleRequired(gruppo_funzionale) && !sottogruppo_funzionale){
			allFields["sottogruppo_funzionale"] = "Sottogruppo Funzionale";
	}
	
	if(tipo_componente == "V"){
		allFields["esponente"] = "Esponente";
		allFields["revisione"] = "Revisione";
	}
	
	//application.output("opensk 3 : "+openSK);	 
	if(openSK){
		allFields["responsabile"] = "Responsabile";
		allFields["ente"] = "Ente (scheda anomalia)";
	}
	
	var missing = [];
	for (var f in allFields){
		if (foundset[f] === null){
			missing.push(f);
		}
	}

	//INIZIO CHECK RESPONSABILE
	if(openSK && responsabile && !databaseManager.getDataSetByQuery("ferrari","SELECT * FROM XCH_SRUTE00F_IN WHERE TRIM(MCDUTE) = ?",[responsabile],1).getMaxRowIndex()){
		missing.push("Responsabile");
	}
	//FINE CHECK RESPONSABILE

	if (missing.length > 0){
		var message = "Compilare i campi obbligatori:\n\n";
		var i = 1;
		for each(var mf in missing){
			message += (allFields[mf]) ? i++ + ". Inserire " + allFields[mf] + "\n" : mf + ": valore non corretto\n";
		}
		plugins.dialogs.showErrorDialog("Errori nella compilazione",message,"Ok");
		return -1;
	}else{
		//I campi obbligatori sono tutti compilati qui imposto criteri più complessi (servirebbe una callback)
		if(data_registrazione_segnalaz < data_rilevazione){
			plugins.dialogs.showErrorDialog("Errore inserimento","La data di rilevazione deve necessariamente essere minore o uguale alla data di registrazione.","Ok");
			return -1
		}else{
			return 0;
		}
	}

}

/**
 *
 * @properties={typeid:24,uuid:"EA6E7D65-0488-4668-9627-FC87D830968C"}
 */
function nfx_defineAccess() {
	if(globals.nfx_user == "fsalvarani"){
		return [true, false, true];
	}
	if (stato_segnalazione == "CHIUSA") {
		return [true, false, false, true];
	} else {
		return [true, false, true];
	}
}

/**
 *
 * @properties={typeid:24,uuid:"4EFA528C-F632-42F1-A7CC-51D58F5E1F0B"}
 */
function nfx_excelExport(){
	var toReturn = new Array();
	toReturn.push({label:	"Descrizione",	field:	"k8_descrizione",	format:	null});
	toReturn.push({label:	"Analisi",		field:	"k8_analisi",		format:	null});
	return toReturn;
}

/**
 *
 * @properties={typeid:24,uuid:"8DFB4789-A740-42F2-9C39-3C08A0CED03E"}
 */
function nfx_postValidate(){
	if (sendToService() < 0){
		//gestisco gli errori
		if (globals.vc2_persistUnacceptedRecord(foundset,"mancato_invio_segnalazione","numero_segnalazione")){
			errori_riportati = globals.vc2_lastErrorLog;
		}else{
			return -1;
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"44916F4A-1033-4D58-98D0-3D6E6967A204"}
 */
function nfx_sks(){
	return [{icon: "chiudi_segnalazione.png",	tooltip:"Chiudi Segnalazione",			method:"chiusuraSegnalazione"},
			{icon: "apri_scheda.png",			tooltip:"Crea Scheda", 					method:"creazioneScheda"},
			{icon: "report.png", 				tooltip:"Full report (BETA)",			method:"report"},
			{icon: "findplus.png",				tooltip:"Ricerca per descrizione...",	method:"openDescriptionSearch"}];
}

/**
 *
 * @properties={typeid:24,uuid:"33406180-DB0C-4B74-B11C-41A7F88D233F"}
 */
function nfx_validate(){
	return dataValidation();
}

/**
 *
 * @properties={typeid:24,uuid:"D13D6C1F-34CB-42C8-A6CF-01B4E85FAD86"}
 */
function packData()
{
var data = {
		  'SACTIP' : 'T', //OBBLIGATORIO La Testata
		  'SAVEEX' : globals.vc2_webServiceMode, //OBBLIGATORIO Sempre esecuzione?
          'SAOP'   :"I", //<INSERIMENTO/MODIFICA/CANCELLAZIONE> OBBLIGATORIO
          "SAAPPL" : "VIRTCAR2",
          'SACENT' : ente_segnalazione_anomalia, //<Cd Ente> OBBIGATORIO
          'SANREG' : numero_segnalazione, //<N Registrazione evento> OBBLIGATORIO IN MODIFICA.
          'SADREG' : globals.utils_dateFormatAS400(data_registrazione_segnalaz), //<Dt Reg.Evento> OBBLIGATORIO. YYYY MM DD
          'SADRIL' : globals.utils_dateFormatAS400(data_rilevazione), //<Dt Rilevazione> OBBLIGATORIO
          'SAFUNZ' : gruppo_funzionale, //<Gruppo Funzionale> OBBLIGATORIO
          'SASUGR' : sottogruppo_funzionale, //<Sottogruppo funzionale> OBBLIGATORIO
          'SADICS' : tipo_anomalia, //<Demerito ICS> Tipo Anomalia NOTA: definire di cosa si tratta. OBBLIGATORIO.
          'SADICP' : punteggio_demerito, //<Demerito ICP> OBBLIGATORIO.
          'SAMODE' : codice_modello, //<Modello> OBBLIGATORIO.
          'SAVERS' : codice_versione, //<Versione> OBBLIGARIO
          'SAKPER' : km_percorsi, //<Km Percorsi> NON OBBLIGATORIO (Viene settato a 0 dal servizio).
          'SACOMP' : componente_vettura, //<Cod. Componente> OBBLIGATORIO NUMERICO
          'SALEAD' : segnalato_da, //<Segnalato da> OBBLIGATORIO (Servoy dovrebbe metterlo in automatico in base all'utenza).
          'SACDIS' : codice_disegno, //<Cd. Disegno> OBBLIGATORIO NUMERICO
          'SAPROV' : null, //<Stato Provvedim.> OBBLIGATORIO. Chiarire Funzione. Un carattere. 
          'SAMATR' : matricola_vettura, //<Matricola AOMAT> NON OBBLIGATORIO NUMERICO dal 12 giugno (sviluppo previsto)
          'SACCLI' : "0", //<Importatore> NON OBBLIGATORIO RIGUARDA UN IMPORTATORE O UN DEALER
          'SAFREQ' : frequenza, //<Frequenza> NON OBBLIGATORIO FLOAT separato da punto.
          'SACREC' : costo_unitario_recupero, //<Costo Un.Recupero> NON OBBLIGATORIO FLOAT separato da punto.
          'SAPRIO' : priorita, //<Priorita> NON OBBLIGATORIO
          'SACDIF' : codice_difetto, //<Cd Difetto> OBBLIGATORIO
          'SASTAT' : globals.utils_stateFormatAS400(stato_segnalazione), //<Stato> NON OBBLIGATORIO AS400 mette nulla per le aperte C chiuse e poi P e R
          'SALIB3' : " ", //<Libero> NON OBBLIGATORIO.
          'SADATV' : globals.utils_dateFormatAS400(timestamp_modifica || new Date()), //<Data Variaz.> NON OBBLIGATORIO
          'SALEA2' : leader, //<Leader> OBBLIGATORIO TESTO.
          'SANOMI' : utente_creazione, //<Utente Inser.>  NON OBBLIGATORIO, MA CONSIGLIATO in fase di test sara' SERVOY, poi ci sara' la mappatura.
          'SADATI' : globals.utils_dateFormatAS400(timestamp_creazione || new Date()), //<Data Inser.> NON OBBLIGATORIO, MA CONSIGLIATO, ARRIVA DA SERVOY
          'SANOMV' : utente_modifica, //<Utente Variaz.> NON OBBLIGATORIO, MA CONSIGLIATO in fase di test sara' SERVOY, poi ci sara' la mappatura.
          'SATCOM' : tipo_componente, //<Tipo componente> OBBLIGATORIO, assunme i valori F (Fisico) e V (Virtuale).
          'SAESPO' : esponente, //<Esponente> OBBLIGATORIO se 'Tipo componente' è 'V'
          'SAREVI' : revisione, //<Revisione> OBBLIGATORIO se 'Tipo componente' è 'V'
          'SACANO' : attribuzione //<Causale Anomalia> o <Classe Anomalia> OBBLIGATORIO.
	}
	return data;
}

/**
 *
 * @properties={typeid:24,uuid:"ED30FB8F-7896-4B82-B545-F7AE42E9602B"}
 */
function packDataAndUser()
{
	var data = packData();
	data["SACHIA"] = 'SERVOY';
	var userInfo = globals.vc2_getUserInfo(null);
	if (userInfo && userInfo.user_as400){
		if(data["SANREG"]){
			utente_modifica = userInfo.user_as400;
		}else{
			utente_creazione = userInfo.user_as400;
		}
		data["SANOMI"] = userInfo.user_as400;
	}
	return data;
}

/**
 * @properties={typeid:24,uuid:"F634FED6-2820-4523-9E66-1651E1BD7AF7"}
 */
function sendToService(){
	if(activateWebServices == 0){
		//Invio i dati relativi alla segnalazione
		var regNum = transmitData(foundset,null,null);
		if(regNum && regNum > 0){
			numero_segnalazione = regNum;
			/*
			 * Una volta salvata la chiusura della segnalazione
			 * se è necessario provvedo all'apertura della scheda
			 * associata
			 */
			if(openSK){
				var regSK = transmitDataChiusura(foundset);
				if(regSK && regSK > 0){
					//Salvo il valore della SKA appena creata
					globals.lastSKAfromSA = regSK;
					numero_scheda = regSK;
					openSK = null;
					return 1;
				}else{
					return -1;
				}
			}else{
				return 1;
			}
		}else{
			return -1;
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"A3DD03A9-94EC-4A66-8BD6-0DD9A682598B"}
 */
function transmitData(foundSet, param2, param3) {
	var data = packDataAndUser();
	//FS
	//dep		var p = plugins.http.getPoster(globals.vc2_serviceLocationSe,"segnalazione_anomalia");
	var client = plugins.http.createNewHttpClient();
	var postReq = client.createPostRequest(globals.vc2_serviceLocationSe);
	if (postReq) {
		application.output("globals.vc2_serviceLocationSe " + globals.vc2_serviceLocationSe);
		//dep	p.setCharset("ISO-8859-1");
		postReq.setCharset("ISO-8859-1");
		//dep	for (var s in data) {
		for (var _s in data) {
			//dep	p.addParameter(s, data[s]);
			postReq.addParameter(_s, data[_s]);
		}
	}

	//dep	var code = p.doPost();
	//application.output("Codice post req "+code);

	/** @type {plugins.http.Response} */
	var res = postReq.executeRequest();
	application.output("Risposta del server " + res);
	/** @type {String} */
	var response = null;
	
	//application.output("risposta del server "+code);
	//dep	if (code == 200) {
	if (res != null){
			if(res.getStatusCode() == 200) {
		//dep	response = globals.vc2_parseResponse(foundset, p.getPageData(), "SANREG_OUT", ["SACENT", "SAMODE", "SAVERS", "SALEAD", "SALEA2", "SANOMI", "SANOMV"]);
		response = globals.vc2_parseResponse(foundset, res.getResponseBody(), "SANREG_OUT", ["SACENT", "SAMODE", "SAVERS", "SALEAD", "SALEA2", "SANOMI", "SANOMV"]);

	} 
	}else {
		// Codice Errore WSNET1: errore nella connessione di rete il servizio non ha risposto 200.
		plugins.dialogs.showErrorDialog("Dati segnalazione non trasmessi", "Il servizio AS400 non è al momento disponibile. (WSNET1)", "Ok");
		//dep 	globals.vc2_lastErrorLog = "Codice: " + code + " - Il servizio AS400 non e' al momento disponibile. (WSNET1)";
		globals.vc2_lastErrorLog = "Codice: " + res + " - Il servizio AS400 non è al momento disponibile. (WSNET1)";
		response = -1;
	}
	
	//Inizio LOG
	var table = databaseManager.getDataSourceTableName(controller.getDataSource());
	var table_key = k8_anomalie_id;
	//FS
	var logic_key = (response != -1) ? ente_segnalazione_anomalia + " " + utils.stringToNumber(response) : ente_segnalazione_anomalia;
	globals.vc2_logTransactionToAS400(table, table_key, logic_key, data, globals.vc2_lastErrorLog, response, globals.vc2_getLogOperation());
	//Fine LOG
	return response;
}

/**
 *@param {JSFoundSet} f
 * @properties={typeid:24,uuid:"8ACC2451-2EDF-42B2-9436-1FA000A9C2DE"}
 */
function transmitDataChiusura(f) {
	var data = forms.schede_anomalie.packDataAndUser(foundset);
	//FS
	//dep	var p = plugins.http.getPoster(globals.vc2_serviceLocationSk,"scheda_anomalia");
	var client = plugins.http.createNewHttpClient();
	var postReq = client.createPostRequest(globals.vc2_serviceLocationSk);
	//dep	p.setCharset("ISO-8859-1");
	postReq.setCharset("ISO-8859-1");
	for (var _s in data) {
		//dep		p.addParameter(s, data[s]);
		postReq.addParameter(_s, data[_s]);
	}
	//dep	var code = p.doPost();
	var res = postReq.executeRequest();

	/** @type {String} */
	var response = null;
	//dep	if (code == 200){
	if (res != null){
		if( res.getStatusCode() == 200) {
		//dep		response = globals.vc2_parseResponse(foundset, p.getPageData(), "SCNREG_OUT", ["SCCENT","SCMODE","SCVERS","SCENTE","SCLEA2","SCLEAD","SCRESP","SCNOMI","SCNOMV"]);
		response = globals.vc2_parseResponse(foundset, res.getResponseBody(), "SCNREG_OUT", ["SCCENT", "SCMODE", "SCVERS", "SCENTE", "SCLEA2", "SCLEAD", "SCRESP", "SCNOMI", "SCNOMV"]);
	} }else {
		// Codice Errore WSNET1: errore nella connessione di rete il servizio non ha risposto 200.
		plugins.dialogs.showErrorDialog("Dati segnalazione non trasmessi", "Il servizio AS400 non è al momento disponibile. (WSNET1)", "Ok");
		globals.vc2_lastErrorLog = "Codice: " + res + " - Il servizio AS400 non è al momento disponibile. (WSNET1)";
		response = -1;
	}
	//Inizio LOG
	var table = databaseManager.getDataSourceTableName(forms.schede_anomalie.controller.getDataSource());
	var table_key = k8_anomalie_id;
	var logic_key = (response != -1) ? ente + " " + utils.stringToNumber(response) : ente;
	globals.vc2_logTransactionToAS400(table, table_key, logic_key, data, globals.vc2_lastErrorLog, response, globals.vc2_getLogOperation());
	//Fine LOG
	return response;
}

/**
 * @properties={typeid:24,uuid:"2C3EC995-4C04-47F7-9BCA-96F6D8F84E71"}
 */
function report(){
	var fields = [{title: "Indice criticità",					field: "indice_criticita"},
	              {title: "Codice modello",						field: "codice_modello"},
	              {title: "Versione",							field: "codice_versione"},
	              {title: "Causale anomalia",					field: "attribuzione"},
	              {title: "Gestione",							field: "gestione_anomalia"},
	              {title: "Gruppo funzionale",					field: "gruppo_funzionale"},
	              {title: "Sottogruppo funzionale",				field: "sottogruppo_funzionale"},
	              {title: "Tipo componente",					field: "tipo_componente"},
	              {title: "Gruppo 79",							field: "k8_segnalazioni_full_to_k8_distinte_progetto_core.complessivo"},
	              {title: "Codice disegno",						field: "codice_disegno"},
	              {title: "Eponente",							field: "esponente"},
	              {title: "Revisione",							field: "revisione"},
	              {title: "Descrizione componente anagrafica",	field: "k8_segnalazioni_full_to_k8_anagrafica_disegni.descrizione"},
	              {title: "Demerito",							field: "punteggio_demerito"},
	              {title: "Tipo anomalia",						field: "tipo_anomalia"},
	              {title: "N. (SA)",							field: "numero_segnalazione"},
	              {title: "Ente (SA)",							field: "ente_segnalazione_anomalia"},
	              {title: "Data rilevazione (SA)",				field: "data_rilevazione"},
	              {title: "Segnalato da",						field: "segnalato_da"},
	              {title: "Leader (SA)",						field: "leader"},
	              {title: "Stato (SA)",							field: "stato_segnalazione"},
	              {title: "Data chiusura (SA)",					field: "data_chiusura_segnalazione"},
	              {title: "Utente chiusura (SA)",				field: "utente_chiusura_segnalazione"},
	              {title: "N. (SKA)",							field: "k8_segnalazioni_full_to_k8_schede_anomalie.numero_scheda"},
	              {title: "Ente (SKA)",							field: "k8_segnalazioni_full_to_k8_schede_anomalie.ente"},
	              {title: "Leader (SKA)",						field: "k8_segnalazioni_full_to_k8_schede_anomalie.leader"},
	              {title: "Data assegnazione leader (SKA)",		field: "k8_segnalazioni_full_to_k8_schede_anomalie.data_assegnazione_leader"},
	              {title: "Responsabile (SKA)",					field: "k8_segnalazioni_full_to_k8_schede_anomalie.responsabile"},
	              {title: "Stato (SKA)",						field: "k8_segnalazioni_full_to_k8_schede_anomalie.stato_anomalia"},
	              {title: "Data chiusura (SKA)",				field: "k8_segnalazioni_full_to_k8_schede_anomalie.data_chiusura"},
	              {title: "Utente chiusura (SKA)",				field: "k8_segnalazioni_full_to_k8_schede_anomalie.utente_chiusura_scheda"},
	              {title: "Descrizione",						field: descrizione},
	              {title: "Analisi",							field: analisi},
	              {title: "Provvedimento",						field: "k8_segnalazioni_full_to_k8_schede_anomalie.k8_provvedimento"},
	              {title: "N. (SKM)",							field: "k8_segnalazioni_full_to_k8_schede_modifica.nr_proposta_modifica"},
	              {title: "Ente (SKM)",							field: "k8_segnalazioni_full_to_k8_schede_modifica.ente_proposta_modifica"},
	              {title: "Leader (SKM)",						field: "k8_segnalazioni_full_to_k8_schede_modifica.leader"},
	              {title: "Tipo approvazione (SKM)",			field: "k8_segnalazioni_full_to_k8_schede_modifica.tipo_approvazione"},
	              {title: "Causale (SKM)",						field: "k8_segnalazioni_full_to_k8_schede_modifica.causale_modifica"},
	              {title: "Descrizione causale (SKM)",			field: "k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_to_xch_srcau00f_in.mcdcau"},
	              {title: "Descrizione modifica (SKM)",			field: "k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_to_k8_schede_modifica_righe.descrizione_proposta_modifica"},
	              {title: "Data proposta modifica (SKM)",		field: "k8_segnalazioni_full_to_k8_schede_modifica.data_proposta_modifica"},
	              {title: "Stato (SKM)",						field: "k8_segnalazioni_full_to_k8_schede_modifica.stato_scheda"},
	              {title: "Data approvazione (SKM)",			field: "k8_segnalazioni_full_to_k8_schede_modifica.data_approv_sk_prop_mod"},
	              {title: "Data annullamento (SKM)",			field: "k8_segnalazioni_full_to_k8_schede_modifica.data_annul_sk_prop_mod"},
	              {title: "N.CID",								field: "k8_segnalazioni_full_to_k8_schede_modifica.numero_cid"},
	              {title: "Data prevista attuazione",			field: "k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica.data_prevista_attuazione"},
	              {title: "Data introduzione modifica",			field: "data_introduzione_calc"},];
	globals.vc2_fullReport(controller.getName(),fields,"sa");
}

/**
 * @properties={typeid:24,uuid:"1F4C1E9F-0A2E-4C42-A194-56FFBECB23E3"}
 */
function descrizione(record){
	return (record.tipo_scheda == "SEGNALAZIONE") ? "k8_descrizione" : "k8_segnalazioni_full_to_k8_schede_anomalie.k8_descrizione";
}

/**
 * @properties={typeid:24,uuid:"864BCC25-42B1-4FA2-BAB5-EE4ED4FB1694"}
 */
function analisi(record){
	return (record.tipo_scheda == "SEGNALAZIONE") ? "k8_analisi" : "k8_segnalazioni_full_to_k8_schede_anomalie.k8_analisi";
}

/**
 * @properties={typeid:24,uuid:"41B51E1A-4702-4A9F-B1A9-2FC55770DB18"}
 */
function sendEmailNotifications(){
	var addresses	= getMailAddrersses();
	var object		= getMailObject();
	var message		= getMailMessage();
	if(addresses.length){
		var success = plugins.mail.sendMail(addresses,"virtualcar2-noreply@ferrari.com",object,message);
		if(success){
			plugins.dialogs.showInfoDialog("Conferma invio notifica","La notifica per la segnalazione appena creata è stata iviata correttamente a " + addresses.split(",").length + " destinatari","Ok");
		}else{
			var error = plugins.mail.getLastSendMailExceptionMsg();
			plugins.dialogs.showErrorDialog("Errore invio notifica","Non è stato possibile inviare la notifica via email.\nErrore riscontrato: " + error,"Ok");
		}
	}else{
		plugins.dialogs.showInfoDialog("Mancato invio notifica","Nessun utente iscritto alla mailing-list per questa segnalazione","Ok");
	}
}

/**
 * @properties={typeid:24,uuid:"DF8BB05C-30ED-4F1A-A85E-70CDEBF74B43"}
 */
function getMailAddrersses(){
	var query	= "SELECT DISTINCT LOWER(MLI.INDIRIZZO) FROM K8_MAILING_LIST_INDIRIZZI MLI WHERE MLI.ENTE IN (SELECT ML.ENTE FROM K8_MAILING_LIST ML WHERE (ML.PROGETTO IS NULL OR ML.PROGETTO = ?) AND (ML.GRUPPO_00 IS NULL OR ML.GRUPPO_00 = ? OR ML.GRUPPO_01 = ? OR ML.GRUPPO_02 = ? OR ML.GRUPPO_03 = ? OR ML.GRUPPO_04 = ? OR ML.GRUPPO_05 = ? OR ML.GRUPPO_06 = ? OR ML.GRUPPO_07 = ? OR ML.GRUPPO_08 = ? OR ML.GRUPPO_09 = ?) AND (ML.DEMERITO_00 IS NULL OR ML.DEMERITO_00 = ? OR ML.DEMERITO_01 = ? OR ML.DEMERITO_02 = ? OR ML.DEMERITO_03 = ? OR ML.DEMERITO_04 = ? OR ML.DEMERITO_05 = ? OR ML.DEMERITO_06 = ? OR ML.DEMERITO_07 = ? OR ML.DEMERITO_08 = ? OR ML.DEMERITO_09 = ?) AND (ML.ENTE_SEGNALAZIONE IS NULL OR ML.ENTE_SEGNALAZIONE = ?)) AND (MLI.VALIDITA_INIZIO <= ? AND MLI.VALIDITA_FINE >= ?) AND (MLI.PROGETTO IS NULL OR MLI.PROGETTO = ?) ORDER BY LOWER(MLI.INDIRIZZO) ASC";
	var args	= [codice_modello,gruppo_funzionale,gruppo_funzionale,gruppo_funzionale,gruppo_funzionale,gruppo_funzionale,gruppo_funzionale,gruppo_funzionale,gruppo_funzionale,gruppo_funzionale,gruppo_funzionale,punteggio_demerito,punteggio_demerito,punteggio_demerito,punteggio_demerito,punteggio_demerito,punteggio_demerito,punteggio_demerito,punteggio_demerito,punteggio_demerito,punteggio_demerito,ente_segnalazione_anomalia,data_registrazione_segnalaz,data_registrazione_segnalaz,codice_modello];
	var ds		= databaseManager.getDataSetByQuery('ferrari',query,args,-1);
	var addresses = (ds) ? ds.getColumnAsArray(1).join(",") : "";
	return addresses;
}

/**
 * @properties={typeid:24,uuid:"8B1D7D03-FE53-4947-9D25-97FF2F42AF39"}
 */
function getMailObject(){
	return "Nuova segnalazione anomalia [Ente: " + ente_segnalazione_anomalia + ", Numero: " + numero_segnalazione + ", Progetto: " + codice_modello + ", Versione: " + codice_versione + ", Disegno: " + codice_disegno + ", Descrizione: " + getDescrizione() + "]";
}

/**
 * @properties={typeid:24,uuid:"4A0A192A-7ACA-4878-BDC3-F66D8AC4619B"}
 */
function getMailMessage(){
	var user = globals.vc2_getUserInfo(null);
	return	"<html><div>L'utente " + globals.nfx_user + " (<b>" + user.cognome + " " + user.nome + "</b>) ha inserito una segnalazione anomalia con i seguenti riferimenti:</div><br/><hr>" +
			"<div><table cellspacing=5 cellpadding=5 border=2><tr><td>Ente: <b>" + ente_segnalazione_anomalia + "</b></td><td>N°: <b>" + numero_segnalazione + "</b></td><td colspan=\"2\">Demerito: <b>" + punteggio_demerito + "</b></td></tr>" +
		    "<tr><td>Cod. modello: <b>" + codice_modello + "</b></td><td>Versione: <b>" + codice_versione + "</b></td><td colspan=\"2\">Gruppo funzionale: <b>" + gruppo_funzionale + sottogruppo_funzionale + "</b></td></tr>" + 
		    "<tr><td>Disegno: <b>" + utils.numberFormat(codice_disegno,"############") + "</b></td><td colspan=\"3\">Descrizione: <b>" + getDescrizione() + "</b></td></tr>" + 
		    "<tr><td>Tipo anomalia: <b>" + getTipoAnomalia() + "</b></td><td>Difetto: <b>" + getDifetto() + "</b></td><td>Causale anomalia: <b>" + attribuzione + "</b></td><td>Frequenza: <b>" + getFrequenza() + "</b></td></tr>" + 
		    "<tr><td colspan=\"4\">Leader: <b>" + leader + "</b></td></table></div><br>" +
		    "<div><b><u>Descrizione:</u></b><br>" + k8_segnalazioni_full_to_k8_segn_desc_full.testo.replace(/\n/g,"<br>") + "</div><br>" + 
		    "<div><b><u>Accesso diretto:</u></b><br>Per accedere direttamente alla segnalazione cliccare <a href=\"" + getMailDirectLink() + "\"><b>qui</b></a>.<br><hr>" +
		    "<div>Inserita usando <b><u>Virtalcar2</u></b><br>Scarica Virtalcar2 da: <a href=\"http://virtualcar2.unix.ferlan.it/\">virtualcar2.unix.ferlan.it</a></div></html>";
}

/**
 * @properties={typeid:24,uuid:"884920A5-C590-46DA-BAE8-05A15698C0EB"}
 */
function getFrequenza(){
	return utils.numberFormat(k8_segnalazioni_full_to_k8_anomalie_vetture.getSize() + 1,"############");
}

/**
 * @properties={typeid:24,uuid:"B7DA8596-F815-4A23-AC71-4B89493F2234"}
 * @AllowToRunInFind
 */
function getDescrizione(){
	var _n = codice_disegno;
	var _fs = null;
	
	var _s = ["k8_distinte_progetto","k8_distinte_progetto_storico","k8_anagrafica_disegni"];
	
	for(var _i=0;_i<_s.length;_i++){
		_fs = databaseManager.getFoundSet("ferrari",_s[_i]);
		if(_fs.find()){
			_fs.numero_disegno = _n;
			if(_fs.search()){
				return _fs.descrizione;
			}
		}
	}
	
	return "Non disponibile...";
}

/**
 * @properties={typeid:24,uuid:"E23A0784-172B-4E4E-A837-F13881692B1A"}
 */
function getTipoAnomalia(){
	return application.getValueListDisplayValue("gestione_anomalie$tipo_anomalia",tipo_anomalia);
}

/**
 * @properties={typeid:24,uuid:"61FE9C87-6CBF-4A49-96FA-8A3834C53973"}
 */
function getDifetto(){
	return application.getValueListDisplayValue("gestione_anomalie$codici_difetto",codice_difetto);
}

/**
 * @properties={typeid:24,uuid:"82FB552C-B1B9-4B8C-BC40-2E62C1787DE9"}
 */
function getMailDirectLink(){
	return "http://virtualcar2.unix.ferlan.it/servoy-client/servoy_client.jnlp?m=vc2_showOnLoad&a=segnalazioni_anomalie_record|k8_anomalie_id|" + k8_anomalie_id;
}

/**
 * @properties={typeid:24,uuid:"43203805-F9D0-4960-A314-55473B29A216"}
 */
function printTestMailMessage(){
	application.output(getMailObject());
	application.output(getMailMessage());
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"C82F7935-2EDD-464D-9ECB-E54E31F124A5"}
 */
function openDescriptionSearch(event) {
	var _f = controller.getName();
	var _cf = {p: "codice_modello",
	           v: "codice_versione",
	           s: "stato_segnalazione",
	           d: "k8_segnalazioni_full_to_k8_distinte_progetto_core.descrizione"};
	
	forms._vc2_description_searchEngine.openPopup(_f,_cf);
}
