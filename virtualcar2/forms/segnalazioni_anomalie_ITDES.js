/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"970BB6EE-5781-43A1-A6E4-D58DD9A9A59C",variableType:4}
 */
var activateWebServices = 0;

/**
 * @properties={typeid:35,uuid:"77399159-6832-4F41-B9DC-1B79274E740C",variableType:-4}
 */
var nfx_related = ["anomalie_vetture_table","segnalazioni_anomalie_descrizione_record","segnalazioni_anomalie_analisi_record","segnalazioni_anomalie_ITDES_dettaglio_record","documenti_record","time_line_table","tag_table"];

/**
 *
 * @properties={typeid:24,uuid:"2CF08F5B-A140-4A57-A367-624B9EFEFEA9"}
 */
function chiusuraSegnalazione()
{
	if (plugins.dialogs.showInfoDialog("Cambio stato della segnalazione",
			"Questa segnalazione verra' chiusa e non sara' possibile modificarla. Questa procedura NON genera una scheda anomalia, continuare?",
			"Si","No") == "No"){
		return ;
	}else{
		if (nfx_validate() == -1){
			return;
		}
		data_chiusura_segnalazione = new Date();
		stato_segnalazione = "CHIUSA";
		forms.nfx_interfaccia_pannello_buttons.editSave(controller.getName());
	}

}

/**
 *
 * @properties={typeid:24,uuid:"832C0DCC-898F-47A6-8CD5-F736A7F834FE"}
 */
function creazioneScheda()
{
	if (plugins.dialogs.showInfoDialog("Cambio stato della scheda",
			"Questa segnalazione verra' chiusa e trasformata in una scheda anomalia, continuare?",
			"Si","No") == "No"){
		return;
	}else{
		if (nfx_validate() == -1){
			return;
		}
//		application.showFormInDialog(forms.segnalazione_anomalia_trasformazione_scheda);
		
		var formq = forms.segnalazione_anomalia_trasformazione_scheda;
		var window = application.createWindow("", JSWindow.MODAL_DIALOG);
		formq.controller.show(window);
		
		tipo_scheda = "ANOMALIA";
		data_chiusura_segnalazione = data_registrazione_scheda = new Date();
		stato_scheda = "APERTA";
		stato_segnalazione = "CHIUSA";
		//TODO:Copia di descrizioni varie
		forms.nfx_interfaccia_pannello_buttons.editSave(controller.getName());
	}
}

/**
 *
 * @properties={typeid:24,uuid:"68587CB8-2C42-40C4-8A34-0DBE9E162AB9"}
 */
function dataValidation()
{
	if (!stato_segnalazione){
		stato_segnalazione = "APERTA";
	}
	
	if (tipo_scheda != "ANOMALIA"){
		tipo_scheda = "SEGNALAZIONE";
	}
	
	if (!data_registrazione_segnalaz){
		data_registrazione_segnalaz = new Date();
	}
	
	//var tableName = controller.getTableName();
	var tableName = databaseManager.getDataSourceTableName(controller.getDataSource());
	
	var missing = [];
	var allFields = {"data_rilevazione" : "Data rilevazione",
				"codice_versione" : "Codice versione",
				"codice_modello" : "Codice Modello",
				"gruppo_funzionale": "Gruppo Funzionale",
				"punteggio_demerito": "Demerito",
				"codice_difetto": "Codice difetto",
				"ente_segnalazione_anomalia": "Ente",
				"segnalato_da": "Segnalatore",
				"codice_disegno" : "Codice disegno",
				"leader": "Leader"};
	
	for (var f in allFields){
		if (!foundset[f]){
			missing.push(f);
		}
	}
	
	if (missing.length > 0){
		var message = "Compilare i campi obbligatori:\n\n";
		var i = 1;
		for each(var mf in missing){
			message += i++ + ". Inserire " + allFields[mf] + "\n";
		}
		plugins.dialogs.showErrorDialog("Errori nella compilazione",
			message,"Ok");
		return -1;
	}else{
		return 0;
	}

}

/**
 *
 * @properties={typeid:24,uuid:"50123DD9-FF91-4906-979A-4A3F695699B5"}
 */
function nfx_customEnableButtons()
{
   forms.nfx_interfaccia_pannello_buttons.elements.xexport.enabled = false;
}

/**
 *
 * @properties={typeid:24,uuid:"87BCAA51-E9ED-4FA7-84C2-417F54B0AF74"}
 */
function nfx_defineAccess()
{
	return [false,false,false, true];
}

/**
 *
 * @properties={typeid:24,uuid:"E1358866-7A15-417D-B325-66C2678600DC"}
 */
function nfx_excelExport(){
	var toReturn = new Array();
	toReturn.push({label:	"Descrizione",	field:	"k8_descrizione",	format:	null});
	toReturn.push({label:	"Analisi",		field:	"k8_analisi",		format:	null});
	return toReturn;
}

/**
 *
 * @properties={typeid:24,uuid:"3F6BDE49-6F45-4CE4-A4B5-0B4B0DC7F794"}
 */
function nfx_isProgram()
{
	return false;
}

/**
 *
 * @properties={typeid:24,uuid:"5D87C58A-8B45-42EB-BC72-7FAC2B62B02F"}
 */
function nfx_postValidate()
{
	if (sendToService() < 0){
		if (globals.vc2_persistUnacceptedRecord(foundset,
			"mancato_invio_segnalazione",
			"numero_segnalazione")){
			errori_riportati = globals.vc2_lastErrorLog;	
		}else{
			return -1;
		}
		
	}

}

/**
 *
 * @properties={typeid:24,uuid:"1A1B92BF-F42A-4805-810C-A76B6CC42BE0"}
 */
function nfx_validate()
{

	return dataValidation();

}

/**
 *
 * @properties={typeid:24,uuid:"F5C011BD-327A-4F1A-82B8-E2D524BFABA1"}
 */
function packData()
{
var data = {
	'SACTIP' : 'T', //OBBLIGATORIO La Testata
		  'SAVEEX' : globals.vc2_webServiceMode, //OBBLIGATORIO Sempre esecuzione
          'SAOP' :"I", //<INSERIMENTO/MODIFICA/CANCELLAZIONE> OBBLIGATORIO
          "SAAPPL" : "VIRTCAR2",
          'SACENT' : ente_segnalazione_anomalia, //<Cd Ente> OBBIGATORIO
          'SANREG' : numero_segnalazione, //<N Registrazione evento> OBBLIGATORIO IN MODIFICA.
          'SADREG' : globals.utils_dateFormatAS400(data_registrazione_segnalaz), //<Dt Reg.Evento> OBBLIGATORIO. YYYY MM DD
          'SADRIL' : globals.utils_dateFormatAS400(data_rilevazione), //<Dt Rilevazione> OBBLIGATORIO
          'SAFUNZ' : gruppo_funzionale, //<Gruppo Funzionale> OBBLIGATORIO
          'SASUGR' : sottogruppo_funzionale, //<Sottogruppo funzionale> OBBLIGATORIO
          'SADICS' : tipo_segnalazione, //<Demerito ICS> Tipo Anomalia NOTA: definire di cosa si tratta. OBBLIGATORIO.
          'SADICP' : punteggio_demerito, //<Demerito ICP> OBBLIGATORIO.
          'SAMODE' : codice_modello, //<Modello> OBBLIGATORIO.
          'SAVERS' : codice_versione, //<Versione> OBBLIGARIO
          'SAKPER' : km_percorsi, //<Km Percorsi> NON OBBLIGATORIO (Viene settato a 0 dal servizio).
          'SACOMP' : componente_vettura, //<Cod. Componente> OBBLIGATORIO NUMERICO
          'SALEAD' : segnalato_da, //<Segnalato da> OBBLIGATORIO (Servoy dovrebbe metterlo in automatico in base all'utenza).
          'SACDIS' : codice_disegno, //<Cd. Disegno> OBBLIGATORIO NUMERICO
          'SAPROV' : null, //<Stato Provvedim.> OBBLIGATORIO. Chiarire Funzione. Un carattere. 
          'SAMATR' : matricola_vettura, //<Matricola AOMAT> NON OBBLIGATORIO NUMERICO dal 12 giugno (sviluppo previsto)
          'SACCLI' : "0", //<Importatore> NON OBBLIGATORIO RIGUARDA UN IMPORTATORE O UN DEALER
          'SAFREQ' : frequenza, //<Frequenza> NON OBBLIGATORIO FLOAT separato da punto.
          'SACREC' : costo_unitario_recupero, //<Costo Un.Recupero> NON OBBLIGATORIO FLOAT separato da punto.
          'SAPRIO' : priorita, //<Priorita> NON OBBLIGATORIO
          'SACDIF' : codice_difetto, //<Cd Difetto> OBBLIGATORIO
          'SASTAT' : globals.utils_stateFormatAS400(stato_segnalazione), //<Stato> NON OBBLIGATORIO AS400 mette nulla per le aperte C chiuse e poi P e R
          'SALIB3' : " ", //<Libero> NON OBBLIGATORIO.
          'SADATV' : globals.utils_dateFormatAS400(timestamp_creazione || new Date()), //<Data Variaz.> NON OBBLIGATORIO
          'SALEA2' : leader, //<Leader> OBBLIGATORIO TESTO.
          'SANOMI' : "Servoy", //<Utente Inser.>  NON OBBLIGATORIO, MA CONSIGLIATO in fase di test sara' SERVOY, poi ci sara' la mappatura.
          'SADATI' : globals.utils_dateFormatAS400(timestamp_modifica || new Date()), //<Data Inser.> NON OBBLIGATORIO, MA CONSIGLIATO, ARRIVA DA SERVOY
          'SANOMV' : "Servoy" //<Utente Variaz.> NON OBBLIGATORIO, MA CONSIGLIATO in fase di test sara' SERVOY, poi ci sara' la mappatura.
	}
	return data;

}

/**
 *
 * @properties={typeid:24,uuid:"B31535E3-0839-482F-BA28-62FCF0BD8A13"}
 */
function sendToService()
{

	if (activateWebServices == 0) {
		var regNum = transmitData(foundset)
		if (regNum && regNum > 0){
			numero_segnalazione = regNum;
		}else{
			return -1;
		}
		if (tipo_scheda == "ANOMALIA"){
			regNum = transmitDataChiusura(foundset)
			if (regNum && regNum > 0){
				numero_scheda = regNum;
			}else{
				return -1;
			}
		}
	}else{
		return 1;
	}

}

/**
*@param {JSFoundSet} f
* @properties={typeid:24,uuid:"7FA4A731-33E7-49E1-B761-084B2E463093"}
*/
function transmitData(f) {
	var data = packData();
	//dep	var p = plugins.http.getPoster(globals.vc2_serviceLocationSe,"segnalazione_anomalia");
	var client = plugins.http.createNewHttpClient();
	var post = client.createPostRequest(globals.vc2_serviceLocationSe);
	//dep	p.setCharset("ISO-8859-1");
	post.setCharset("ISO-8859-1");
	//dep	for (var s in data) {
	for (var _s in data) {
		//dep		p.addParameter(s, data[s]);
		post.addHeader(_s, data[_s])
	}
	//dep	var code = p.doPost();
	var res = post.executeRequest();
	//dep	if (code == 200){
	if (res != null ){
		
	if(res.getStatusCode() == 200) {
		//dep		return globals.vc2_parseResponse(foundset, p.getPageData(), "SANREG_OUT",null);
		return globals.vc2_parseResponse(foundset, res.getResponseBody(), "SANREG_OUT", null)
	} }else {
		// Codice Errore WSNET1: errore nella connessione di rete il servizio non ha risposto 200.
		plugins.dialogs.showErrorDialog("Dati segnalazione non trasmessi", "Il servizio AS400 non è al momento disponibile. (WSNET1)", "Ok");
		return -1;
	}
	
}

/**
*@param {JSFoundSet} f
* @properties={typeid:24,uuid:"883577B1-D4F8-40F1-9A4D-5E4320B9264D"}
*/
function transmitDataChiusura(f) {
	var data = forms.schede_anomalie.packData(foundset);
	//dep	var p = plugins.http.getPoster(globals.vc2_serviceLocationSk,"scheda_anomalia");
	var client = plugins.http.createNewHttpClient();
	var postReq = client.createPostRequest(globals.vc2_serviceLocationSk);
	//var p = plugins.http.getPoster("http://f400.unix.ferlan.it:8081/Attorney/schede","schede_testata");
	//dep	p.setCharset("ISO-8859-1");
	postReq.setCharset("ISO-8859-1");
	//dep	for (var s in data) {
	for (var _s in data) {
		//dep		p.addParameter(s, data[s]);
		postReq.addHeader(_s, data[_s]);
	}
	//dep	var code = p.doPost();
	var res = postReq.executeRequest();
	if (res != null){
			if(res.getStatusCode() == 200) {
		//dep	if (code == 200){
		//dep		return globals.vc2_parseResponse(foundset, p.getPageData(), "SCNREG_OUT",null);
		return globals.vc2_parseResponse(foundset, res.getResponseBody(), "SCNREG_OUT", null);
	} }else {
		// Codice Errore WSNET1: errore nella connessione di rete il servizio non ha risposto 200.
		plugins.dialogs.showErrorDialog("Dati segnalazione non trasmessi", "Il servizio AS400 non è al momento disponibile. (WSNET1)", "Ok");
		return -1;
	
	}
}
