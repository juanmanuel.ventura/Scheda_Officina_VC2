/**
 *
 * @properties={typeid:24,uuid:"8D26324C-3207-4688-A375-4459CFF585C2"}
 */
function nfx_getTitle()
{
	return "ITDES - Segnalazioni Anomalie";
}

/**
 * @properties={typeid:24,uuid:"E4DD2550-890B-4E59-A60D-FA21EB234C53"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"55103DD4-8066-46AA-AD6C-C5FA32773D60"}
 */
function nfx_defaultPermissions(){
	return "^";
}
