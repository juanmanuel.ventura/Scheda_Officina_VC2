/**
 * @properties={typeid:35,uuid:"E03528A1-BD84-4953-8002-FD88427F524B",variableType:-4}
 */
var nfx_related = ["segnalazioni_anomalie_descrizione_admin_record","segnalazioni_anomalie_analisi_admin_record"];

/**
 *
 * @properties={typeid:24,uuid:"C176EA81-06FE-4935-A712-9DB33B0D8AC2"}
 */
function nfx_defineAccess(){
	return [true,true,true];
}

/**
 *
 * @properties={typeid:24,uuid:"A3F13445-7110-44A9-B0F3-D7E333C2EB6B"}
 */
function nfx_getTitle(){
	return "Segnalazioni Anomalie - Tabella => ADMIN";
}

/**
 *
 * @properties={typeid:24,uuid:"6D685ADE-48DA-4893-83DB-815465E64BCE"}
 */
function nfx_isProgram(){
		return true;
}

/**
 * @properties={typeid:24,uuid:"EF95E924-D5F0-4F7C-9961-167708E44FFA"}
 */
function nfx_defaultPermissions(){
	return "^";
}

/**
 *
 * @properties={typeid:24,uuid:"718E90A0-49C3-4230-AE7C-99ECFE925FBC"}
 */
function nfx_sks(){
	return [{icon: "radioactive.png", tooltip:"Reinvia Mail Segnalazione", method:"resendMail"}];
}

/**
 *
 * @properties={typeid:24,uuid:"B42B7559-C661-4D4D-957A-40DA826BF974"}
 */
function resendMail(){
}
