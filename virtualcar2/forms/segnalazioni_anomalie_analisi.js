/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"FC86197B-D66D-4810-85C3-EEB41EB00E96",variableType:4}
 */
var activateWebServices = 0;

/**
 *
 * @properties={typeid:24,uuid:"F380E59B-94E6-4A82-9EB7-E981EA130050"}
 */
function nfx_defineAccess(){
	return [true, false, false];
}

/**
 *
 * @properties={typeid:24,uuid:"A714A1A2-CCAA-4B60-9C85-C427C474823D"}
 */
function nfx_postAdd(){
	return sendToAS400();
}

/**
 * @properties={typeid:24,uuid:"D6E9F15F-8CF3-4DFD-A0CF-1DA89227C2DB"}
 */
function nfx_postEdit(){
	return sendToAS400();
}

/**
 * @properties={typeid:24,uuid:"DCA97DD5-5BE2-465C-A18C-5F510FC5EB23"}
 */
function sendToAS400(){
	if (sendToService() < 0 && !globals.vc2_persistUnacceptedRecord(foundset,null,null)){
		return -1;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"B8DEE415-E133-4F14-87C9-3BD8857CA5E7"}
 */
function packData()
{
	var dateAndDesc = globals.utils_dateTextFormatAS400(controller.getName());
	return {
	    "SACTIP" : "P", //OBBLIGATORIO La Testata
		"SAVEEX" : globals.vc2_webServiceMode, //OBBLIGATORIO Sempre esecuzione
		"SAOP" : "I", //<INSERIMENTO/MODIFICA/CANCELLAZIONE> OBBLIGATORIO
		"SAAPPL" : "VIRTCAR2",
		"SACENT" : ente, //<Cd Ente> OBBIGATORIO
		"SANREG" : numero_scheda,
        "SADES3" : dateAndDesc.text,
        "SADAT3" : dateAndDesc.date
	}

}

/**
 *
 * @properties={typeid:24,uuid:"354C5E0F-0DA4-40A7-98F6-98FD9B62C6BC"}
 */
function packDataAndUser()
{
	var data = packData();
	data["SACHIA"] = 'SERVOY';
	var userInfo = globals.vc2_getUserInfo(null);
	if (userInfo && userInfo.user_as400){
		utente_modifica = data["SANOMI"] = userInfo.user_as400;
		//utente_creazione = data["SANOMI"] = k8_segn_anal_full_to_k8_segnalazioni_anomalie.utente_creazione;
	}
	return data;
}

/**
 *
 * @properties={typeid:24,uuid:"C0469F7D-E264-4290-B510-60E7AFEA101D"}
 */
function sendToService()
{
	if (activateWebServices == 0) {
		var regNum = transmitData(foundset)
		if (!regNum || regNum < 0){
			return -1;
		}
	}else{
		return 1;
	}

}

/**
 *@param {JSFoundSet} f //aggiunto parametro formal
 * @properties={typeid:24,uuid:"52F7D6B9-0AD9-47F3-A942-A84C14C52C71"}
 */
function transmitData(f){
//FS
//dep	var data = packDataAndUser();
	var _data = packDataAndUser();
	//dep var p = plugins.http.getPoster(globals.vc2_serviceLocationSe,"analisi_scheda_anomalia");
	var client = plugins.http.createNewHttpClient();
	var postReq=client.createPostRequest(globals.vc2_serviceLocationSe);
	//dep p.setCharset("ISO-8859-1");
	postReq.setCharset("ISO-8859-1");
	for (var s in _data){
		//dep p.addParameter(s, data[s]);
		postReq.addHeader(s,_data[s]);
	}
	//dep var code = p.doPost();
	var res = postReq.executeRequest();
	/** @type {String} */	
	var response = null;
//dep	if (code == 200){
	if (res!=null && res.getStatusCode() == 200){	
		//dep response = globals.vc2_parseResponse(foundset, p.getPageData(), "SANREG_OUT", ["SADES3"]);
		response = globals.vc2_parseResponse(foundset,res.getResponseBody(),"SANREG_OUT", ["SADES3"]);

	}else{
		// Codice Errore WSNET1: errore nella connessione di rete il servizio non ha risposto 200.
		plugins.dialogs.showErrorDialog("Dati segnalazione non trasmessi","Il servizio AS400 non è al momento disponibile. (WSNET1)","Ok");
		globals.vc2_lastErrorLog = "Codice: " + res + " - Il servizio AS400 non è al momento disponibile. (WSNET1)";
		response = -1;
	}
	//Inizio LOG
	var table = databaseManager.getDataSourceTableName(controller.getDataSource());
	var table_key = k8_segn_anal_full_id;
	//dep var logic_key = (response != -1) ? ente + " " + utils.stringToNumber(response) : ente;
	var logic_key = (response != -1) ? ente + " " + utils.stringToNumber(response) : ente;
	globals.vc2_logTransactionToAS400(table,table_key,logic_key,_data,globals.vc2_lastErrorLog,response,globals.vc2_getLogOperation());
	//Fine LOG
	return response;
}
