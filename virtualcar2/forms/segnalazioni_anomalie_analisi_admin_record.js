/**
 *
 * @properties={typeid:24,uuid:"8CDAD0B8-72CA-46F4-996C-06D2DB49F490"}
 */
function nfx_defineAccess(){
	return [true,true,true];
}

/**
 *
 * @properties={typeid:24,uuid:"A0B73992-568C-4B82-A53C-7DF1E222DDBF"}
 */
function nfx_getTitle(){
	return 'Analisi (Admin)';
}

/**
 *
 * @properties={typeid:24,uuid:"B22E4D40-76B4-47B1-841E-4C6747D29B0F"}
 */
function nfx_isProgram(){
	return false;
}
