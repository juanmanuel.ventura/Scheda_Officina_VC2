/**
 * @properties={typeid:24,uuid:"23DFFDBA-73BD-4938-B03D-D9813BB342D6"}
 */
function nfx_getTitle(){
	return "Segnalazioni analisi non inviate";
}

/**
 *
 * @properties={typeid:24,uuid:"C2B4DC55-CA71-4D2F-A2B8-3353C1239BF4"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"9836325B-CA69-4A90-93EB-F320D32DA197"}
 */
function nfx_defaultPermissions(){
	return "^";
}

/**
 *
 * @properties={typeid:24,uuid:"7845B4AE-0E83-4019-8E1B-0D4FC826E496"}
 */
function nfx_sks(){
	return [{icon: "ripristina.png", tooltip:"Ripristina", method:"ripristina"}];
}

/**
 *
 * @properties={typeid:24,uuid:"E6218932-F3E9-4EC1-A72A-3835F8A91951"}
 */
function ripristina(){
	globals.vc2_ripristinaNonInviate(foundset,controller.getName());
}
