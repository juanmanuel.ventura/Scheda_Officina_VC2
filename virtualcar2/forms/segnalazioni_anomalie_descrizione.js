/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"87322C8A-A7A7-4459-940F-938C006D6AEE",variableType:4}
 */
var activateWebServices = 0;

/**
 *
 * @properties={typeid:24,uuid:"F985428A-554F-45F9-80C5-9D8EB30BAC7F"}
 */
function nfx_defineAccess(){
	return [true,false,false];
}

/**
 * @properties={typeid:24,uuid:"C1D676EC-51B6-4486-BAE6-97194BD0456A"}
 */
function nfx_postAdd(){
	var send = sendToAS400();
	if(k8_segn_desc_full_to_k8_segnalazioni_full){
	if(!k8_segn_desc_full_to_k8_segnalazioni_full.k8_mail_confirm){
		if(globals.vc2_serverPROD.indexOf(application.getServerURL()) != -1){
			forms.segnalazioni_anomalie.sendEmailNotifications();
		}else{
			plugins.dialogs.showInfoDialog("Conferma invio notifica","!!!AMBIENTE DI TEST!!!\nNessuna notifica è stata inviata!","Ok");
		}
		k8_segn_desc_full_to_k8_segnalazioni_full.k8_mail_confirm = 1;
	}
	}
	return send;
}

/**
 * @properties={typeid:24,uuid:"0201EEC2-4B1B-44DA-8AF3-31E632FDF130"}
 */
function nfx_postEdit(){
	return sendToAS400();
}

/**
 * @properties={typeid:24,uuid:"7D0D23A7-0606-4470-8169-B26FF30792ED"}
 */
function sendToAS400(){
	if (sendToService() < 0 && !globals.vc2_persistUnacceptedRecord(foundset,null,null)){
		return -1;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"25D5491E-E133-4901-BBAD-D4EABF252B47"}
 */
function packData(){
	return {
	    "SACTIP" : "A", //OBBLIGATORIO La Testata
		"SAVEEX" : globals.vc2_webServiceMode, //OBBLIGATORIO Sempre esecuzione
		'SAOP' :"I", //<INSERIMENTO/MODIFICA/CANCELLAZIONE> OBBLIGATORIO
        "SAAPPL" : "VIRTCAR2",
        'SACENT' : ente, //<Cd Ente> OBBIGATORIO
        'SANREG' : numero_scheda,
        'SADES2' : globals.utils_mergeText(controller.getName(),"testo",50)
	}
}

/**
 *
 * @properties={typeid:24,uuid:"F936BD9D-37E0-476D-900E-65CF4055C706"}
 */
function packDataAndUser(){
	var data = packData();
	data["SACHIA"] = 'SERVOY';
	var userInfo = globals.vc2_getUserInfo(null);
	if (userInfo && userInfo.user_as400){
		utente_modifica = data["SANOMI"] = userInfo.user_as400;
		//utente_creazione = data["SANOMI"] = k8_segn_desc_full_to_k8_segnalazioni_anomalie.utente_creazione;
	}
	return data;
}

/**
 *
 * @properties={typeid:24,uuid:"0FDBFDE5-91E4-4D0D-9198-4411A3EF8DA2"}
 */
function sendToService(){
	if (activateWebServices == 0){
		var regNum = transmitData(foundset)
		if (!regNum || regNum < 0){
			return -1;
		}
	}else{
		return 1;
	}
}

/**
 *@param{JSFoundSet} f
 * @properties={typeid:24,uuid:"CE4BD5DE-5CD5-4B36-A27C-9562F56375EB"}
 */
function transmitData(f) {
	var datad = packDataAndUser();
	//FS
	//dep	var p = plugins.http.getPoster(globals.vc2_serviceLocationSe,"descrizione_segnalazione_anomalia");
	var client = plugins.http.createNewHttpClient();
	var postReq = client.createPostRequest(globals.vc2_serviceLocationSe);
	//dep	p.setCharset("ISO-8859-1");
	postReq.setCharset("ISO-8859-1");
	for (var s in datad) {
		//dep		p.addParameter(s, datad[s]);
		postReq.addHeader(s, datad[s]);
	}
	//dep	var code = p.doPost();
	var res = postReq.executeRequest();
	/** @type {String} */
	var response = null;
	//dep	if (code == 200){
	if (res!=null && res.getStatusCode() == 200) {
		//dep		response = globals.vc2_parseResponse(foundset, p.getPageData(), "SANREG_OUT", ["SADES2"]);
		response = globals.vc2_parseResponse(foundset, res.getResponseBody(), "SANREG_OUT", ["SADES2"]);
	} else {
		// Codice Errore WSNET1: errore nella connessione di rete il servizio non ha risposto 200.
		plugins.dialogs.showErrorDialog("Dati segnalazione non trasmessi", "Il servizio AS400 non è al momento disponibile. (WSNET1)", "Ok");
		globals.vc2_lastErrorLog = "Codice: " + res + " - Il servizio AS400 non è al momento disponibile. (WSNET1)";
		response = -1;
	}
	//Inizio LOG
	var table = databaseManager.getDataSourceTableName(controller.getDataSource());
	var table_key = k8_segn_desc_full_id;
	var logic_key = (response != -1) ? ente + " " + utils.stringToNumber(response) : ente;
	globals.vc2_logTransactionToAS400(table, table_key, logic_key, data, globals.vc2_lastErrorLog, response, globals.vc2_getLogOperation());
	//Fine LOG
	return response;
}
