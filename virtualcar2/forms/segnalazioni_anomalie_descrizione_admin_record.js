/**
 *
 * @properties={typeid:24,uuid:"06AB2719-03BE-4918-8068-93B44BC4DEAA"}
 */
function nfx_defineAccess(){
	return [true,true,true];
}

/**
 *
 * @properties={typeid:24,uuid:"DEAE1AB7-750C-4B0B-BDF4-724C241F6B1E"}
 */
function nfx_getTitle(){
	return 'Descrizione (Admin)';
}

/**
 *
 * @properties={typeid:24,uuid:"9AC271FF-CF83-4B1E-A811-0891C485CD69"}
 */
function nfx_isProgram(){
	return false;
}
