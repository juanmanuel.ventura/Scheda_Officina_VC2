/**
 *
 * @properties={typeid:24,uuid:"CDB0E037-7004-4336-B8E1-EE36EE494BDA"}
 */
function nfx_getTitle(){
	return "Segnalazioni descrizioni non inviate";
}

/**
 *
 * @properties={typeid:24,uuid:"E5BDF3AF-F551-4D07-9F41-1A5DF8567E4C"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"495AC8F0-CA68-4960-934F-E9C11F13F1D1"}
 */
function nfx_defaultPermissions(){
	return "^";
}

/**
 *
 * @properties={typeid:24,uuid:"77A2537A-183E-4404-8314-99C2EC264907"}
 */
function nfx_sks(){
	return [{icon: "ripristina.png", tooltip:"Ripristina", method:"ripristina"}];
}

/**
 *
 * @properties={typeid:24,uuid:"55EC16CA-FBF2-4AEA-B739-DB237F33D724"}
 */
function ripristina(){
	globals.vc2_ripristinaNonInviate(foundset,controller.getName());
}
