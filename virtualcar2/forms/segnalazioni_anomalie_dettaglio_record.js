/**
 *
 * @properties={typeid:24,uuid:"041893F7-C9C9-4F59-B073-BA28AEF3746A"}
 */
function nfx_defineAccess()
{
	if (stato_segnalazione == "CHIUSA"){
		return [false,false,false];
	}else{
		return [false,false,true];
	}
}

/**
 *
 * @properties={typeid:24,uuid:"1FFCD199-4D09-4FFA-A2A9-F2A7EFF6700A"}
 */
function nfx_getTitle()
{
	return "Dettaglio";

}

/**
 *
 * @properties={typeid:24,uuid:"7BDBA448-1BC1-4B04-900A-0B6671F3D235"}
 */
function nfx_isProgram()
{
	return false;
}
