/**
 * @type {String}
 * @properties={typeid:35,uuid:"7DA20F70-21CA-40F5-807A-0F63FB769BAE"}
 */
var modello = null;

/**
 * @properties={typeid:24,uuid:"E37319B6-3982-4D4C-9FE8-F64D2367AC75"}
 */
function openPopup(){
	modello = null;
	controller.show("segn_modello");
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"914368EF-A810-4722-A697-F06B07A39A53"}
 */
function onDataChange(oldValue, newValue, event) {
	if(newValue){
		forms.segnalazioni_anomalie.codice_modello = newValue;
		
		
		//application.closeFormDialog("segn_modello");
		
		var w=controller.getWindow();
		w.destroy();
	}
	return true;
}
