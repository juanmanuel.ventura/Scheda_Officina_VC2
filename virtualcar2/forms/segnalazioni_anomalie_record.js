/**
 * @properties={typeid:35,uuid:"D7194403-A0F9-426F-9788-6F83BD18193E",variableType:-4}
 */
var _enteWarningShowed = false;

/**
 * @properties={typeid:35,uuid:"AB5ED893-BB6D-40D4-81B8-FD3A33A11CBE",variableType:-4}
 */
var blockedFields = ["numero_segnalazione","stato_segnalazione","ente","numero_scheda","stato_anomalia","ente_proposta_modifica","nr_proposta_modifica","stato_scheda","causale_modifica","tipo_approvazione","numero_cid","data_approv_sk_prop_mod","data_emissione","tipo","data_prevista_attuazione","richiesta_assembly","data_introduzione_modifica_1","numero_assembly_1","data_registrazione_segnalaz","data_assegnazione_leader","responsabile","complessivo","utente_chiusura_segnalazione","data_chiusura_segnalazione"];

/**
 * @properties={typeid:35,uuid:"1102AD62-10F0-4FED-9593-10B6717EEF87",variableType:-4}
 */
var disabledFields = ["ente","ente_proposta_modifica"];

/**
 * @properties={typeid:35,uuid:"DF962701-D1E4-4C78-91FE-DD3B8A14BE47",variableType:-4}
 */
var fromCollaudo = null;

/**
 * @properties={typeid:35,uuid:"71A1176C-ABB8-40AB-BD03-235230F70D74",variableType:-4}
 */
var requiredFields = ["ente_segnalazione_anomalia","data_rilevazione","codice_modello","codice_versione","gruppo_funzionale","tipo_anomalia","segnalato_da","leader","codice_disegno",/*"componente_vettura",*/"tipo_componente","codice_difetto","punteggio_demerito","attribuzione"];

/**
 *
 * @properties={typeid:24,uuid:"1CEF5045-5405-48B2-811A-3E2B18CEBC90"}
 */
function nfx_getTitle(){
	return "Segnalazioni Anomalie";
}

/**
 * @properties={typeid:24,uuid:"6ABEAF54-F09B-40DB-A2A1-13B198E9FB75"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"784B5B37-E84C-4BB5-9A70-72C443192E94"}
 */
function nfx_onShow(){
	if(fromCollaudo){
		plugins.dialogs.showInfoDialog("Creazione eseguita","La segnalazione è stata creata con successo, è ora necessario inserire le informanzioni obbligatorie al fine di inviarla ad AS400.","Ok");
		forms.nfx_interfaccia_pannello_buttons.edit(controller.getName());
	}else{
		var _showEnteWarningCheck = forms.nfx_interfaccia_gestione_salvataggi.load(controller.getName() + "_showEnteWarningCheck");
		
		//Procedura di primo utilizzo: inizializzazione dato
		if(!_showEnteWarningCheck){
			_showEnteWarningCheck = "yes";
			forms.nfx_interfaccia_gestione_salvataggi.save(_showEnteWarningCheck,controller.getName() + "_showEnteWarningCheck");
		}
		
		//Controllo su _showEnteWarningCheck
		if(!_enteWarningShowed && _showEnteWarningCheck == "yes"){
			forms.segnalazioni_anomalie_warning_popup.openPopup(controller.getName() + "_showEnteWarningCheck");
			_enteWarningShowed = true;
			if(forms.segnalazioni_anomalie_warning_popup.getResult()){
				globals.nfx_checkAndAddGoto(forms.gestione_account_record.controller.getName());
			}
		}
	}
}

/**
 * @properties={typeid:24,uuid:"746C43CB-2B3A-4096-8099-52BC5AC0D1C9"}
 */
function local_preAdd(){
	colorize();
	forms.segnalazioni_anomalie_popup_modello.openPopup();
}

/**
 * @properties={typeid:24,uuid:"1BED950F-DFD5-4202-BF6D-AC10060DDB33"}
 */
function local_postAdd(){
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"0E2EA3E3-DF62-4FCA-9A46-9595620AC890"}
 */
function local_postAddOnCancel(){
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"E4DE47DA-CB74-40CB-AFA7-63226768D4B6"}
 */
function local_preEdit(){
	colorize();
}

/**
 * @properties={typeid:24,uuid:"31B6BE7C-CD71-47D9-8BD9-32FFCBCBEB78"}
 */
function local_postEdit(){
	if(fromCollaudo){
		if(numero_segnalazione && k8_segnalazioni_full_to_k8_righe_schede_collaudo.osservazioni){
			k8_segnalazioni_full_to_k8_segn_desc_full.newRecord(false,true);
			k8_segnalazioni_full_to_k8_segn_desc_full.testo = k8_segnalazioni_full_to_k8_righe_schede_collaudo.osservazioni;
			databaseManager.saveData();
			var result = forms.segnalazioni_anomalie_descrizione.sendToAS400();
			if(result){
				var email = (globals.vc2_getAs400UserMail(foundset.leader, "user_as400")) ? globals.vc2_getAs400UserMail(foundset.leader, "user_as400") : plugins.dialogs.showInputDialog("Nessun indirizzo email per il Leader","Il leader selezionato non ha un indirizzo email associato.");
				plugins.mail.sendMail(email,"servoy@ferrari.com","Nuova Segnalazione Anomalia",createMessage());
			}
		}
		fromCollaudo = null;
	}
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"1A31741D-72DB-4F3B-B853-69B060E1ABD3"}
 */
function local_postEditOnCancel(){
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"4BCD8029-CE92-4326-B4BA-B8323832E4ED"}
 */
function colorize(){
	decolorize();
	setRequiredFields();
	globals.utils_colorize(controller.getName(),requiredFields,"#FFFFC4");
	globals.utils_colorize(controller.getName(),blockedFields,"#E0E0E0");
	globals.utils_enable(controller.getName(),disabledFields,false);
}

/**
 * @properties={typeid:24,uuid:"001420C9-318B-4818-A71F-253FA8682733"}
 */
function decolorize(){
	globals.utils_colorize(controller.getName(),requiredFields.concat(blockedFields),"#FFFFFF");
	globals.utils_enable(controller.getName(),disabledFields,true);
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"6E330B3C-3F6B-42DC-80CA-84E85ED24521"}
 */
function onGruppoFunzionaleChange(oldValue, newValue, event) {
	sottogruppo_funzionale = null;
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"A11B778D-39E6-4298-BE61-F01E8046A95A"}
 * @AllowToRunInFind
 */
function onSegnalatoreChange(oldValue, newValue, event) {
	var f = forms.gestione_utenze.foundset.duplicateFoundSet();
	if(f.find()){
		f.user_as400 = newValue;
		if(f.search()){
			ente_segnalazione_anomalia = f.ente_as400;
		}else{
			ente_segnalazione_anomalia = null;
		}
	}
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"A07F6D99-092C-48AE-83A3-1E8294C6A0BD"}
 */
function onLeaderChange(oldValue, newValue, event) {
	data_assegnazione_leader = new Date();
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"D1B9DEA6-0BCA-4C4C-B637-7B443ABDA407"}
 */
function onTipoComponenteChange(oldValue, newValue, event) {
	var mode = forms.nfx_interfaccia_pannello_buttons.mode;
	if(mode == "add" || mode == "edit"){
		colorize();
	}
}

/**
 * @properties={typeid:24,uuid:"0331A3E2-02C8-4FEB-AA29-E6EF6A838BAE"}
 */
function setRequiredFields(){
	//Impostazione condizionale dei campi obbligatori
	
	//Tipo Componente
	var fields = ["esponente","revisione"];
	if(tipo_componente == "V"){
		for(var i=0;i<fields.length;i++){
			if(requiredFields.indexOf(fields[i]) == -1){
				requiredFields.push(fields[i]);
			}
		}
	}else{
		for(var i=0;i<fields.length;i++){
			if(requiredFields.indexOf(fields[i]) != -1){
				requiredFields.splice(requiredFields.indexOf(fields[i]),1);
			}
		}
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"8E7A1603-EC1E-456D-BBD5-C639D2182F72"}
 */
function showDescriptionAS400(event) {
	var rel = k8_segnalazioni_full_to_k8_anagrafica_disegni;
	if (rel) {
		var txt = rel.descrizione;
		var item ;
		var menu ;
		if (txt) {
			//FS
			//dep	item = plugins.popupmenu.createMenuItem(txt,copyToClipboard);
			menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem(txt, copyToClipboard);
			//dep	item.setMethodArguments([txt]);
			item.methodArguments = [txt];
		} else {
			menu = plugins.window.createPopupMenu();
			//dep			item = plugins.popupmenu.createMenuItem("Non presente in anagrafica...", null);
			item = menu.addMenuItem("Non presente in anagrafica...", null);
		}
		//FS
		//dep		plugins.popupmenu.showPopupMenu(elements.codice_disegno, [item]);
		menu.show(elements.codice_disegno);
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"19523F34-54E4-4BF1-96D3-EFFC2647DE13"}
 */
function showDescription(event) {
	var rel = k8_segnalazioni_full_to_k8_distinte_progetto_core;
	if (rel) {
		var txt = rel.descrizione_cpl;
		var item ;
		var menu ;
		if (txt) {
			//FS
			//dep			item = plugins.popupmenu.createMenuItem(txt, copyToClipboard);
			menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem(txt, copyToClipboard);
			//dep			item.setMethodArguments([txt]);
			item.methodArguments = [txt];
		} else {
			//dep			item = plugins.popupmenu.createMenuItem("Non presente in distinta...", null);
			menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem("Non presente in distinta...", null);

		}
		//FS
		//dep		plugins.popupmenu.showPopupMenu(elements.complessivo, [item]);
		menu.show(elements.complessivo);
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"8E169A2F-AD46-4211-865A-76247C98B964"}
 */
function showComponentDescription(event) {
	var rel = k8_segnalazioni_full_to_k8_componenti;
	if (rel) {
		var txt = rel.descrizione;
		var item ;
		var menu ;
		if (txt) {
			//FS
			//dep		item = plugins.popupmenu.createMenuItem(txt, copyToClipboard);
			menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem(txt, copyToClipboard);
			//dep	item.setMethodArguments([txt]);
			item.methodArguments = [txt];
		} else {
			//dep	item = plugins.popupmenu.createMenuItem("Non presente in lista componenti...", null);
			menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem("Non presente in lista componenti...", null);
		}
		//FS
		//dep		plugins.popupmenu.showPopupMenu(elements.componente_vettura, [item]);
		menu.show(elements.componente_vettura);
	}
}

/**
 * @properties={typeid:24,uuid:"C0752D0C-406C-4205-9109-F3396618ED4F"}
 */
function copyToClipboard(txt){
	application.setClipboardContent(txt);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"30AE0F3A-FBDC-4BBF-AEF5-A189DC6EE394"}
 */
function openEntiList(event){
	forms.ente_chooser.openPopup(controller.getName(),"ente_segnalazione_anomalia");
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"A721CCD7-4E62-4D6E-99F0-0CFFFF58EDE6"}
 */
function openLeaderList(event) {
	var mode = forms.nfx_interfaccia_pannello_buttons.mode;
	if(mode == "add" || mode == "edit"){
		if(!codice_modello){
			plugins.dialogs.showWarningDialog("Cod. modello","Prima di poter procedere è necessario compilare il campo \"Cod. modello\"","Ok");
			return;
		}
		forms.leader_chooser.openPopup(codice_modello,controller.getName(),"leader","onLeaderChange");
	}else{
		forms.leader_chooser.openPopup(null,controller.getName(),"leader","onLeaderChange");
	}
}

/**
 * @properties={typeid:24,uuid:"2B6F5128-EB4B-4220-B7B8-2230D7685913"}
 */
function openDifettiList(event){
	forms.defect_chooser.openPopup(controller.getName(),"codice_difetto");
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"7623A8A2-9A57-4E51-8CA3-4ACFC5794D27"}
 */
function openDB(event) {
	var mode = forms.nfx_interfaccia_pannello_buttons.mode;
	if(mode == "add" || mode == "edit"){
		if(!codice_modello){
			plugins.dialogs.showWarningDialog("Cod. modello","Prima di poter procedere è necessario compilare il campo \"Cod. modello\"","Ok");
			return;
		}
		forms.distinte_progetto_albero_picker.from = controller.getName();
		forms.distinte_progetto_albero_picker.field = "codice_disegno";
		globals.vc2_currentProgetto = codice_modello;
		//application.showFormInDialog(forms.distinte_progetto_albero_picker,null,null,null,null,"Seleziona numero disegno",true,false,"treePicker",true);
		var formq = forms.distinte_progetto_albero_picker;
		var window = application.createWindow("treePicker", JSWindow.MODAL_DIALOG);
		window.title="Seleziona numero disegno";
		window.resizable=true;
		formq.controller.show(window);
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"13B2142A-E4CA-4E29-99CB-2C66F0906297"}
 */
function openComponentsList(event) {
	var mode = forms.nfx_interfaccia_pannello_buttons.mode;
	if(mode == "add" || mode == "edit"){
		if(!codice_modello){
			plugins.dialogs.showWarningDialog("Cod. modello","Prima di poter procedere è necessario compilare il campo \"Cod. modello\"","Ok");
			return;
		}
		forms.componenti_chooser.openPopup(codice_modello,controller.getName(),"componente_vettura");
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"3564CD8D-C042-446A-8AC2-1D9CD4784E90"}
 */
function goToForm(event) {
	var elem = event.getElementName();
	if(elem == "sa" && controller.getName() != "segnalazioni_anomalie_record" && ente_segnalazione_anomalia && numero_segnalazione){
		globals.nfx_goToProgramAndRecord("segnalazioni_anomalie_record",k8_anomalie_id,"k8_anomalie_id");
		return;
	}else if(elem == "ska" && controller.getName() != "schede_anomalie_record" && ente && numero_scheda){
		globals.nfx_goToProgramAndRecord("schede_anomalie_record",k8_anomalie_id,"k8_anomalie_id");
		return;
	}else if(elem == "skm" && k8_segnalazioni_full_to_k8_schede_modifica && k8_segnalazioni_full_to_k8_schede_modifica.ente_proposta_modifica && k8_segnalazioni_full_to_k8_schede_modifica.nr_proposta_modifica){
		globals.nfx_goToProgramAndRecord("schede_modifica_record",k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_id,"k8_schede_modifica_id");
		return;
	}
}

/**
 * @properties={typeid:24,uuid:"4A4F528A-1B09-4EF1-9067-A5C383BFFE82"}
 */
function moreInfoVersion(event){
	plugins.dialogs.showInfoDialog("8.2.Apertura SA su AS400/VC2:","Per aprire una SA su AS400/VC2 è necessario compilare i campi seguenti con le modalità qui riportate:\n- a.       Modello: indicare il modello di vettura su cui si è riscontrata l’anomalia,\nobbligatorio (es.: F142, F142AD, ecc.).\n- b.      Versione: indicare la versione se l’anomalia si verifica su una singola versione (es.: l’anomalia può\nessere generica o limitata alla versione Taiwan) altrimenti indicare la versione europa guida sinistra\nperché anche tale distinta dovrà essere modificata.","Ok");
}

/**
 * @properties={typeid:24,uuid:"137B1840-E296-4F8C-8904-5967B1E8A99E"}
 */
function nextLM(event){
	if(k8_segnalazioni_full_to_k8_schede_modifica && k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica){
		var _l = k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica;
		_l.setSelectedIndex(_l.getSelectedIndex() + 1);
		databaseManager.recalculate(foundset.getSelectedRecord());
	}
}

/**
 * @properties={typeid:24,uuid:"0CF7069A-30F6-43D8-B818-31853522F53B"}
 */
function previousLM(event){
	if(k8_segnalazioni_full_to_k8_schede_modifica && k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica){
		var _l = k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica;
		_l.setSelectedIndex(_l.getSelectedIndex() - 1);
		databaseManager.recalculate(foundset.getSelectedRecord());
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"27D39979-C08E-4270-8B81-344706262966"}
 */
function openSceltaDisegno(event){
	if(codice_modello && codice_versione){
		forms._vc2_bomViewer_container.openPopup(controller.getName(),"codice_disegno",codice_modello,codice_versione);
	}else{
		plugins.dialogs.showErrorDialog("Errore","Per utilizzare questa funzionalità è necessario compilare i campi:\n - Cod. modello\n - Versione","Ok");
	}
	
}
