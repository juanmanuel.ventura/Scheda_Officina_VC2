/**
 * @properties={typeid:35,uuid:"6563FDE6-B814-4ACD-9E12-F9F84875BE2A",variableType:-4}
 */
var blockedFields = ["numero_segnalazione", "stato_segnalazione", "ente", "numero_scheda", "stato_anomalia", "ente_proposta_modifica", "nr_proposta_modifica", "stato_scheda", "causale_modifica", "tipo_approvazione", "numero_cid", "data_approv_sk_prop_mod", "data_emissione", "tipo", "data_prevista_attuazione", "richiesta_assembly", "data_introduzione_modifica_1", "numero_assembly_1", "data_registrazione_segnalaz", "data_assegnazione_leader", "responsabile", "complessivo", "utente_chiusura_segnalazione", "data_chiusura_segnalazione"];

/**
 * @properties={typeid:35,uuid:"3C40F3E9-2061-43A8-A39E-E1B6B2ADB9AC",variableType:-4}
 */
var fromCollaudo = null;

/**
 * @properties={typeid:35,uuid:"76C9901C-5163-4794-93C8-736CAD677F89",variableType:-4}
 */
var requiredFields = ["ente_segnalazione_anomalia", "data_rilevazione", "codice_modello", "codice_versione", "gruppo_funzionale", "tipo_anomalia", "segnalato_da", "leader", "codice_disegno", "codice_difetto", "punteggio_demerito"];

/**
 *
 * @properties={typeid:24,uuid:"A2AE3F66-0B85-4F45-AC44-6CFC86358624"}
 */
function nfx_getTitle() {
	return "Segnalazioni Anomalie";
}

/**
 * @properties={typeid:24,uuid:"CCF66DCE-115A-459F-ACD5-A13E335491A3"}
 */
function nfx_isProgram() {
	return true;
}

/**
 * @properties={typeid:24,uuid:"E49DFBF0-1C8D-4188-B8B6-0CE55FB8564D"}
 */
function nfx_onShow() {
	if (fromCollaudo) {
		plugins.dialogs.showInfoDialog("Creazione eseguita", "La segnalazione è stata creata con successo, è ora necessario inserire le informanzioni obbligatorie al fine di inviarla ad AS400.", "Ok");
		forms.nfx_interfaccia_pannello_buttons.edit(controller.getName());
	}
}

/**
 * @properties={typeid:24,uuid:"7E08575B-1CD6-41F1-A252-DDE275C9B293"}
 */
function local_preAdd() {
	colorize();
}

/**
 * @properties={typeid:24,uuid:"CC231714-CB5D-45D8-8BF3-EEA46075D693"}
 */
function local_postAdd() {
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"F734DE9B-2C2F-435E-8639-469F0417F2BF"}
 */
function local_postAddOnCancel() {
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"F04E99C5-C5FA-43A5-A2E9-0691C671AA8F"}
 */
function local_preEdit() {
	colorize();
}

/**
 * @properties={typeid:24,uuid:"57AAE6D9-B3D7-4674-92B7-44DD820A2E5F"}
 */
function local_postEdit() {
	if (fromCollaudo) {
		if (numero_segnalazione && k8_segnalazioni_full_to_k8_righe_schede_collaudo.osservazioni) {
			k8_segnalazioni_full_to_k8_segn_desc_full.newRecord(false, true);
			k8_segnalazioni_full_to_k8_segn_desc_full.testo = k8_segnalazioni_full_to_k8_righe_schede_collaudo.osservazioni;
			databaseManager.saveData();
			var result = forms.segnalazioni_anomalie_descrizione.sendToAS400();
			if (result) {
				var email = (globals.vc2_getAs400UserMail(foundset.leader, "user_as400")) ? globals.vc2_getAs400UserMail(foundset.leader, "user_as400") : plugins.dialogs.showInputDialog("Nessun indirizzo email per il Leader", "Il leader selezionato non ha un indirizzo email associato.");
				plugins.mail.sendMail(email, "servoy@ferrari.com", "Nuova Segnalazione Anomalia", createMessage());
			}
		}
		fromCollaudo = null;
	}
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"64918B76-5C70-43BD-93E4-40A4B987C111"}
 */
function local_postEditOnCancel() {
	decolorize();
}

/**
 * @properties={typeid:24,uuid:"C7237867-F6D2-4841-9DE2-A82D49B4126B"}
 */
function colorize() {
	globals.utils_colorize(controller.getName(), requiredFields, "#FFFFC4");
	globals.utils_colorize(controller.getName(), blockedFields, "#E0E0E0");
}

/**
 * @properties={typeid:24,uuid:"383A9A67-403F-4CAE-8F21-F39090301DB2"}
 */
function decolorize() {
	globals.utils_colorize(controller.getName(), requiredFields.concat(blockedFields), "#FFFFFF");
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"7227AEEE-E0D8-42CF-BC03-795342E4C5A0"}
 */
function onLeaderChange(oldValue, newValue, event) {
	data_assegnazione_leader = new Date();
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"CB22BFAC-4928-4553-A512-CC5F28BE6C49"}
 */
function showDescriptionAS400(event) {
	var rel = k8_segnalazioni_full_to_k8_anagrafica_disegni;
	if (rel) {
		var txt = rel.descrizione;
		var item = null;
		var menu = null;
		if (txt) {
			//FS
			//dep		item = plugins.popupmenu.createMenuItem(txt,copyToClipboard);
			menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem(txt, copyToClipboard);
			//dep		item.setMethodArguments([txt]);
			item.methodArguments = [txt];
		} else {
			//dep		item = plugins.popupmenu.createMenuItem("Non presente in anagrafica...",null);
			menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem("Non presente in anagrafica...", null);
		}
		//dep		plugins.popupmenu.showPopupMenu(elements.codice_disegno,[item]);
		menu.show(elements.codice_disegno);
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"1C30D335-71C7-4131-B702-8F130161BF52"}
 */
function showDescription(event) {
	var rel = k8_segnalazioni_full_to_k8_distinte_progetto_core;
	if (rel) {
		var txt = rel.descrizione_cpl;
		var item = null;
		var menu = null;
		if (txt) {
			//FS
			//dep			item = plugins.popupmenu.createMenuItem(txt, copyToClipboard);
			menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem(txt, copyToClipboard);
			//dep 			item.setMethodArguments([txt]);
			item.methodArguments = [txt];
		} else {
			//dep			item = plugins.popupmenu.createMenuItem("Non presente in distinta...", null);
			menu = plugins.window.createPopupMenu();
			item = menu.addMenuItem("Non presente in distinta...", null);
		}
		//dep		plugins.popupmenu.showPopupMenu(elements.complessivo, [item]);
		menu.show(elements.complessivo);
	}
}

/**
 * @properties={typeid:24,uuid:"5A950BCF-9D84-46DB-8A69-09D715F3C164"}
 */
function copyToClipboard(txt) {
	application.setClipboardContent(txt);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"52509C4F-C7AB-4A0D-84EA-2F90513D022B"}
 */
function openDB(event) {
	var mode = forms.nfx_interfaccia_pannello_buttons.mode;
	if (mode == "add" || mode == "edit") {
		forms.distinte_progetto_albero_picker.from = controller.getName();
		forms.distinte_progetto_albero_picker.field = "codice_disegno";
		globals.vc2_currentProgetto = codice_modello;

		//application.showFormInDialog(forms.distinte_progetto_albero_picker,null,null,null,null,"Seleziona numero disegno",true,false,"treePicker",true);

		var formq = forms.distinte_progetto_albero_picker;
		var window = application.createWindow("treePicker", JSWindow.MODAL_DIALOG);
		window.title = "Seleziona numero disegno";
		window.resizable = true;
		formq.controller.show(window);
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"A3EC6ECD-7E52-4333-B332-A11B28E08BEE"}
 */
function openComponentsList(event) {
	forms.componenti_chooser.openPopup("49", controller.getName(), "componente_vettura");
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"9AB339BB-7CF1-4780-B3B4-E5D9948D09F3"}
 */
function goToForm(event) {
	var elem = event.getElementName();
	if (elem == "sa" && controller.getName() != "segnalazioni_anomalie_record" && ente_segnalazione_anomalia && numero_segnalazione) {
		globals.nfx_goToProgramAndRecord("segnalazioni_anomalie_record", k8_anomalie_id, "k8_anomalie_id");
		return;
	} else if (elem == "ska" && controller.getName() != "schede_anomalie_record" && ente && numero_scheda) {
		globals.nfx_goToProgramAndRecord("schede_anomalie_record", k8_anomalie_id, "k8_anomalie_id");
		return;
	} else if (elem == "skm" && k8_segnalazioni_full_to_k8_schede_modifica && k8_segnalazioni_full_to_k8_schede_modifica.ente_proposta_modifica && k8_segnalazioni_full_to_k8_schede_modifica.nr_proposta_modifica) {
		globals.nfx_goToProgramAndRecord("schede_modifica_record", k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_id, "k8_schede_modifica_id");
		return;
	}
}
