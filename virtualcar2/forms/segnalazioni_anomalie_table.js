/**
 * @properties={typeid:35,uuid:"C48803E1-2EF1-4A97-A522-B4844F139E37",variableType:-4}
 */
var _choices = {"Ente (SA)"							: {dataprovider: "ente_segnalazione_anomalia",																								name: "ente_segnalazione_anomalia",			size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.COMBOBOX,			editable: false,	valuelist: "gestione_anomalie$enti",								format: null,					method: null,							button: {icon:"normal_down.png",action:"openEntiList",tooltip:"Apri anagrafica enti"}},
                "N° (SA)"							: {dataprovider: "numero_segnalazione",																										name: "numero_segnalazione",				size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null},
                "Stato (SA)"						: {dataprovider: "stato_segnalazione",																										name: "stato_segnalazione",					size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: "gestione_anomalie_stato",								format: null,					method: null,							button: null},
                "Registrazione (SA)"				: {dataprovider: "data_registrazione_segnalaz",																								name: "data_registrazione_segnalaz",		size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Data rilevazione (SA)"				: {dataprovider: "data_rilevazione",																										name: "data_rilevazione",					size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.CALENDAR,			editable: true,		valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Gestione"							: {dataprovider: "gestione_anomalia",																										name: "gestione_anomalia",					size: 130,	align: SM_ALIGNMENT.CENTER,	type: JSField.COMBOBOX,			editable: false,	valuelist: "gestione_anomalie$gestione",							format: null,					method: null,							button: null},
                "Cod. modello"						: {dataprovider: "codice_modello",																											name: "codice_modello",						size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: true,		valuelist: "progetti",												format: null,					method: null,							button: null},
                "Versione"							: {dataprovider: "codice_versione",																											name: "codice_versione",					size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: true,		valuelist: "segnalazioni_anomalie$versione",						format: null,					method: null,							button: {icon:"significativita_null.png",action:"moreInfoVersion",tooltip:"Maggiori informazioni sul campo 'Versione'"}},
                "Gruppo funzionale"					: {dataprovider: "gruppo_funzionale",																										name: "gruppo_funzionale",					size: 150,	align: SM_ALIGNMENT.LEFT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "Gruppi",												format: null,					method: "onGruppoFunzionaleChange",		button: null},
                "Sottogruppo"						: {dataprovider: "sottogruppo_funzionale",																									name: "sottogruppo_funzionale",				size: 20,	align: SM_ALIGNMENT.CENTER,	type: JSField.TYPE_AHEAD,		editable: true,		valuelist: "segnalazioni_anomalie$sottogruppi_funzionali",			format: null,					method: null,							button: null},
                "Tipo anomalia"						: {dataprovider: "tipo_anomalia",																											name: "tipo_anomalia",						size: 150,	align: SM_ALIGNMENT.LEFT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "gestione_anomalie$tipo_anomalia",						format: null,					method: null,							button: null},
                "Sottotipo"							: {dataprovider: "sottotipo_anomalia",																										name: "sottotipo_anomalia",					size: 20,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: true,		valuelist: null,													format: null,					method: null,							button: null},
                "Segnalato da"						: {dataprovider: "segnalato_da",																											name: "segnalato_da",						size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: true,		valuelist: null,													format: null,					method: "onSegnalatoreChange",			button: null},
                "Leader"							: {dataprovider: "leader",																													name: "leader",								size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: true,		valuelist: "segnalazioni_anomalie$leader",							format: null,					method: null,							button: {icon:"normal_down.png",action:"openLeaderList",tooltip:"Apri anagrafica leader"}},
                "Data Assegnazione"					: {dataprovider: "data_assegnazione_leader",																								name: "data_assegnazione_leader",			size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Nuova?"							: {dataprovider: "flag_nuova",																												name: "flag_nuova",							size: 20,	align: SM_ALIGNMENT.CENTER,	type: JSField.CHECKS,			editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Responsabile"						: {dataprovider: "responsabile",																											name: "responsabile",						size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: false,	valuelist: "static$utenti_as400",									format: null,					method: null,							button: null},
                "Gruppo 79"							: {dataprovider: "k8_segnalazioni_full_to_k8_distinte_progetto_core.complessivo",															name: "complessivo",						size: 250,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Cod. disegno"						: {dataprovider: "codice_disegno",																											name: "codice_disegno",						size: 250,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: true,		valuelist: "segnalazioni_anomalie$componenti_from_distinta",		format: null,					method: null,							button: {icon:"smartfolder.png",action:"openDB",tooltip:"Apri distinta di progetto"}},
                "Componente vettura"				: {dataprovider: "componente_vettura",																										name: "componente_vettura",					size: 250,	align: SM_ALIGNMENT.LEFT,	type: JSField.TYPE_AHEAD,		editable: true,		valuelist: "segnalazioni_anomalie$componenti_codifica_sat",			format: null,					method: null,							button: {icon:"smartfolder.png",action:"openComponentsList",tooltip:"Apri anagrafica componenti"}},
                "Esponente"							: {dataprovider: "esponente",																												name: "esponente",							size: 60,	align: SM_ALIGNMENT.RIGHT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "segnalazioni_anomalie$esponente",						format: null,					method: null,							button: null},
                "Revisione"							: {dataprovider: "revisione",																												name: "revisione",							size: 60,	align: SM_ALIGNMENT.RIGHT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "segnalazioni_anomalie$revisione",						format: null,					method: null,							button: null},
                "Tipo Componente"					: {dataprovider: "tipo_componente",																											name: "tipo_componente",					size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "gestione_anomalie$tipo_componente",						format: null,					method: "onTipoComponenteChange",		button: null},
                "Difetto"							: {dataprovider: "codice_difetto",																											name: "codice_difetto",						size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "gestione_anomalie$codici_difetto",						format: null,					method: null,							button: null},
                "Demerito"							: {dataprovider: "punteggio_demerito",																										name: "punteggio_demerito",					size: 130,	align: SM_ALIGNMENT.RIGHT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "segnalazioni_anomalie$demerito",						format: null,					method: null,							button: null},
                "Indice criticità"					: {dataprovider: "indice_criticita",																										name: "indice_criticita",					size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "indice_criticita",										format: null,					method: null,							button: null},
                "Icona criticità"					: {dataprovider: "icon_criticita",																											name: "icon_criticita",						size: 20,	align: SM_ALIGNMENT.CENTER,	type: JSField.IMAGE_MEDIA,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Causale anomalia"					: {dataprovider: "attribuzione",																											name: "attribuzione",						size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.COMBOBOX,			editable: false,	valuelist: "segnalazioni_anomalie$causale_anomalia",				format: null,					method: null,							button: null},
                "Chiusura richiesta"				: {dataprovider: "data_chiusura_richiesta",																									name: "data_chiusura_richiesta",			size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.CALENDAR,			editable: true,		valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Chiusura prevista"					: {dataprovider: "data_prevista_chiusura_sc_anom",																							name: "data_prevista_chiusura_sc_anom",		size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.CALENDAR,			editable: true,		valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Utente Chius. (SA)"				: {dataprovider: "utente_chiusura_segnalazione",																							name: "utente_chiusura_segnalazione",		size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Data Chius. (SA)"					: {dataprovider: "data_chiusura_segnalazione",																								name: "data_chiusura_segnalazione",			size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Ente (SKA)"						: {dataprovider: "ente",																													name: "ente",								size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.COMBOBOX,			editable: false,	valuelist: "gestione_anomalie$enti",								format: null,					method: null,							button: null},
                "N° (SKA)"							: {dataprovider: "numero_scheda",																											name: "numero_scheda",						size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null},
                "Stato (SKA)"						: {dataprovider: "stato_anomalia_fake",																										name: "stato_anomalia",						size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: "gestione_anomalie_stato",								format: null,					method: null,							button: null},
                "Ente (SKM)"						: {dataprovider: "k8_segnalazioni_full_to_k8_schede_modifica.ente_proposta_modifica",														name: "ente_proposta_modifica",				size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.COMBOBOX,			editable: false,	valuelist: "gestione_anomalie$enti",								format: null,					method: null,							button: null},
                "N° (SKM)"							: {dataprovider: "k8_segnalazioni_full_to_k8_schede_modifica.nr_proposta_modifica",															name: "nr_proposta_modifica",				size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null},
                "Stato (SKM)"						: {dataprovider: "k8_segnalazioni_full_to_k8_schede_modifica.stato_scheda",																	name: "stato_scheda",						size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: "gestione_anomalie_stato",								format: null,					method: null,							button: null},
                "Causale modifica"					: {dataprovider: "k8_segnalazioni_full_to_k8_schede_modifica.causale_modifica",																name: "causale_modifica",					size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Approvazione"						: {dataprovider: "k8_segnalazioni_full_to_k8_schede_modifica.tipo_approvazione",															name: "tipo_approvazione",					size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "CID"								: {dataprovider: "k8_segnalazioni_full_to_k8_schede_modifica.numero_cid",																	name: "numero_cid",							size: 100,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null},
                "Annulata?"							: {dataprovider: "k8_segnalazioni_full_to_k8_schede_modifica.annullata",																	name: "annullata",							size: 20,	align: SM_ALIGNMENT.CENTER,	type: JSField.CHECKS,			editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Data approvazione (SKM)"			: {dataprovider: "k8_segnalazioni_full_to_k8_schede_modifica.data_approv_sk_prop_mod",														name: "data_approv_sk_prop_mod",			size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Data emissione (LM)"				: {dataprovider: "k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica.data_emissione",						name: "data_emissione",						size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Tipo (LM)"							: {dataprovider: "k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica.tipo",								name: "tipo",								size: 130,	align: SM_ALIGNMENT.CENTER,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Prevista attuazione (LM)"			: {dataprovider: "k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica.data_prevista_attuazione",			name: "data_prevista_attuazione",			size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Richiesta assembly (LM)"			: {dataprovider: "k8_segnalazioni_full_to_k8_schede_modifica.k8_schede_modifica_to_k8_lettere_modifica.richiesta_assembly",					name: "richiesta_assembly",					size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: null,					method: null,							button: null},
                "Introduzione modifica (LM)"		: {dataprovider: "data_introduzione_calc",																									name: "data_introduzione_modifica_1",		size: 130,	align: SM_ALIGNMENT.LEFT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "dd/MM/yyyy",			method: null,							button: null},
                "Numero assembly (LM)"				: {dataprovider: "numero_assembly_calc",																									name: "numero_assembly_1",					size: 130,	align: SM_ALIGNMENT.RIGHT,	type: JSField.TEXT_FIELD,		editable: false,	valuelist: null,													format: "############",			method: null,							button: null}};

/**
 * @properties={typeid:35,uuid:"CD48A749-A062-4B1C-B112-6B600D226397",variableType:-4}
 */
var _fields = ["Ente (SA)", "N° (SA)", "Stato (SA)", "Registrazione (SA)", "Data rilevazione (SA)", "Gestione", "Cod. modello", "Versione", "Gruppo funzionale", "Sottogruppo", "Tipo anomalia", "Sottotipo", "Segnalato da", "Leader", "Data Assegnazione", "Nuova?", "Responsabile", "Gruppo 79", "Cod. disegno", "Componente vettura", "Esponente", "Revisione", "Tipo Componente", "Difetto", "Demerito", "Indice criticità", "Icona criticità", "Causale anomalia", "Chiusura richiesta", "Chiusura prevista", "Utente Chius. (SA)", "Data Chius. (SA)", "Ente (SKA)", "N° (SKA)", "Stato (SKA)", "Ente (SKM)", "N° (SKM)", "Stato (SKM)", "Causale modifica", "Approvazione", "CID", "Annulata?", "Data approvazione (SKM)", "Data emissione (LM)", "Tipo (LM)", "Prevista attuazione (LM)", "Richiesta assembly (LM)", "Introduzione modifica (LM)", "Numero assembly (LM)"];

/**
 * @properties={typeid:35,uuid:"6A76194C-E429-41ED-A481-EF655F327540",variableType:-4}
 */
var _savedFS = null;

/**
 * @properties={typeid:24,uuid:"7BE60A4C-2187-4F93-94B6-8F7264D95980"}
 */
function nfx_getTitle(){
	return "Segnalazioni Anomalie - Tabella";
}

/**
 * @properties={typeid:24,uuid:"926DBDA8-A610-411A-8346-55280D0CAC30"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"BFBE9395-D57E-4EAF-A461-DB993049E36D"}
 */
function nfx_sks(){
	return [{icon:	"chiudi_segnalazione.png",	tooltip:"Chiudi Segnalazione",			method:"chiusuraSegnalazione"},
			{icon:	"apri_scheda.png",			tooltip:"Crea Scheda", 					method:"creazioneScheda"},
			{icon:	"report.png", 				tooltip:"Full report (BETA)",			method:"report"},
			{icon:  "findplus.png",				tooltip:"Ricerca per descrizione...",	method:"openDescriptionSearch"},
			{icon:	"table.png",				tooltip:"Scelta colonne",				method:"localFieldChooserWrap"},
			{icon:	"working_lock.png",			tooltip:"Modifica multipla",			method:"lockInterface"}];
}

/**
 * @properties={typeid:24,uuid:"DCBF30F8-118C-4D7A-8F12-F039583D4FA4"}
 */
function localFieldChooserWrap(){
	forms._vc2_colums_customizer.opnePopup(_choices,_fields,controller.getName(),"_fields");
	globals.vc2_drawFieldsInForm(controller.getName(),_choices,_fields);
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"AC5BC4A8-886D-4415-B651-4A70CD1BA9CE"}
 */
function onGruppoFunzionaleChange(oldValue, newValue, event) {
	sottogruppo_funzionale = null;
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"F99EDE45-1CC0-4947-8B78-BD8D989EC8A3"}
 * @AllowToRunInFind
 */
function onSegnalatoreChange(oldValue, newValue, event) {
	var f = forms.gestione_utenze.foundset.duplicateFoundSet();
	if(f.find()){
		f.user_as400 = newValue;
		if(f.search()){
			ente_segnalazione_anomalia = f.ente_as400;
		}else{
			ente_segnalazione_anomalia = null;
		}
	}
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"F4A9F2E8-F209-4D70-8DED-BC79E39FF3A4"}
 */
function onTipoComponenteChange(oldValue, newValue, event) {
	var mode = forms.nfx_interfaccia_pannello_buttons.mode;
	if(mode == "add" || mode == "edit"){
		colorize();
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"14F7A300-AA75-4A37-AF65-C55452FFA30E"}
 */
function openEntiList(event){
	forms.ente_chooser.openPopup(controller.getName(),"ente_segnalazione_anomalia");
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"0E388BE4-FE5F-49DD-B520-74D704285487"}
 */
function openLeaderList(event) {
	var mode = forms.nfx_interfaccia_pannello_buttons.mode;
	if(mode == "add" || mode == "edit"){
		if(!codice_modello){
			plugins.dialogs.showWarningDialog("Cod. modello","Prima di poter procedere è necessario compilare il campo \"Cod. modello\"","Ok");
			return;
		}
		forms.leader_chooser.openPopup(codice_modello,controller.getName(),"leader");
	}else{
		forms.leader_chooser.openPopup(null,controller.getName(),"leader");
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"F8E48331-1820-4F06-B71D-08E90071455B"}
 */
function openDB(event) {
	var mode = forms.nfx_interfaccia_pannello_buttons.mode;
	if(mode == "add" || mode == "edit"){
		if(!codice_modello){
			plugins.dialogs.showWarningDialog("Cod. modello","Prima di poter procedere è necessario compilare il campo \"Cod. modello\"","Ok");
			return;
		}
		forms.distinte_progetto_albero_picker.from = controller.getName();
		forms.distinte_progetto_albero_picker.field = "codice_disegno";
		globals.vc2_currentProgetto = codice_modello;
		
		var formq = forms.distinte_progetto_albero_picker;
		var window = application.createWindow("treePicker", JSWindow.MODAL_DIALOG);
		window.title = "Seleziona numero disegno";
		window.resizable = true;
		formq.controller.show(window);
		
		//application.showFormInDialog(forms.distinte_progetto_albero_picker,null,null,null,null,"Seleziona numero disegno",true,false,"treePicker",true);
	}
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"10022FC8-1B04-4F83-B46B-EC014B7CDB55"}
 */
function openComponentsList(event) {
	var mode = forms.nfx_interfaccia_pannello_buttons.mode;
	if(mode == "add" || mode == "edit"){
		if(!codice_modello){
			plugins.dialogs.showWarningDialog("Cod. modello","Prima di poter procedere è necessario compilare il campo \"Cod. modello\"","Ok");
			return;
		}
		forms.componenti_chooser.openPopup(codice_modello,controller.getName(),"componente_vettura");
	}
}

/**
 * @properties={typeid:24,uuid:"D809AE49-AE95-489A-B4B2-884094BDF21D"}
 */
function moreInfoVersion(event){
	plugins.dialogs.showInfoDialog("8.2.Apertura SA su AS400/VC2:","Per aprire una SA su AS400/VC2 è necessario compilare i campi seguenti con le modalità qui riportate:\n- a.       Modello: indicare il modello di vettura su cui si è riscontrata l’anomalia,\nobbligatorio (es.: F142, F142AD, ecc.).\n- b.      Versione: indicare la versione se l’anomalia si verifica su una singola versione (es.: l’anomalia può\nessere generica o limitata alla versione Taiwan) altrimenti indicare la versione europa guida sinistra\nperché anche tale distinta dovrà essere modificata.","Ok");
}

/**
 * @properties={typeid:24,uuid:"848E7453-1220-471B-B7DD-FA8C530F9991"}
 * @AllowToRunInFind
 */
function lockInterface(){
	var _c = forms[globals.nfx_selectedProgram()].controller;
	if(_c.readOnly){
		forms.nfx_interfaccia_pannello_buttons.mode = "edit";
		_savedFS = forms[globals.nfx_selectedProgram()].foundset.duplicateFoundSet();
		if(foundset.find()){
			foundset.stato_segnalazione = "APERTA";
			foundset.search(false,true);
		}
		forms.nfx_interfaccia_pannello_buttons.disableNavigationTree();
		forms.nfx_interfaccia_pannello_buttons.disableButtons(["sk5"]);
		//forms.nfx_interfaccia_pannello_buttons.disableButtons(["sk4"]);
		databaseManager.setAutoSave(false);
		_c.readOnly = false;
	}else{
		_c.readOnly = true;
		var _e = databaseManager.getEditedRecords();
		for(var _i=0;_i<_e.length;_i++){
			if(localRecordValidation(_e[_i]) > -1 && localTransmitData(_e[_i]) > -1){
				databaseManager.saveData(_e[_i]);
			}
		}
//		databaseManager.rollbackEditedRecords();
		databaseManager.revertEditedRecords();
		databaseManager.setAutoSave(true);
		forms.nfx_interfaccia_pannello_buttons.enableCorrectButtons();
		forms.nfx_interfaccia_pannello_buttons.enableNavigationTree();
		_savedFS = forms[globals.nfx_selectedProgram()].foundset.loadRecords(_savedFS);
		forms.nfx_interfaccia_pannello_buttons.mode = null;
	}
}

/**
 * @properties={typeid:24,uuid:"E1FFA018-CBB4-4EAE-8AF1-EEFF71AAAED9"}
 */
function localRecordValidation(_rec){
	var allFields = {"ente_segnalazione_anomalia"	:	"Ente (segnalazione)",
		             "data_rilevazione"				:	"Data rilevazione",
		             "codice_modello"				:	"Cod. modello",
		             "codice_versione"				:	"Versione",
		             "gruppo_funzionale"			:	"Gruppo funzionale",
		             "tipo_anomalia"				:	"Tipo anomalia",
		             "segnalato_da"					:	"Segnalato da",
		             "leader"						:	"Leader",
		             "codice_disegno"				:	"Cod. disegno\n(Nel caso sia sconosciuto, inserire nel campo Cod.disegno il Gruppo 79 corrispondente)",
		             "tipo_componente"				:	"Tipo componente",
		             "codice_difetto"				:	"Difetto",
		             "punteggio_demerito"			:	"Demerito",
		             "attribuzione"					:	"Causale anomalia"};
	
	if(_rec.gruppo_funzionale && globals.vc2_isSottogruppoFunzionaleRequired(_rec.gruppo_funzionale) && !_rec.sottogruppo_funzionale){
			allFields["sottogruppo_funzionale"] = "Sottogruppo Funzionale";
	}
	
	if(_rec.tipo_componente == "V"){
		allFields["esponente"] = "Esponente";
		allFields["revisione"] = "Revisione";
	}
	
	var missing = [];
	for (var f in allFields){
		if (_rec[f] === null || _rec[f] === ""){
			missing.push(f);
		}
	}
	
	if (missing.length > 0){
		var message = "Rilevati errori nella compilazione:\n\n";
		var i = 1;
		for each(var mf in missing){
			message += (allFields[mf]) ? i++ + ". Inserire " + allFields[mf] + "\n" : mf + ": valore non corretto\n";
		}
		message += "\nIl record non verrà inviato ad AS400 e sarà riportato allo stato precedente alle modifiche.";
		plugins.dialogs.showErrorDialog("Errori nella compilazione",message,"Ok");
		return -1;
	}else{
		//I campi obbligatori sono tutti compilati qui imposto criteri più complessi (servirebbe una callback)
		if(_rec.data_registrazione_segnalaz < _rec.data_rilevazione){
			var message = "Rilevati errori nella compilazione:\n\n";
			message += "1. La data di rilevazione deve necessariamente essere minore o uguale alla data di registrazione\n";
			message += "\nIl record non verrà inviato ad AS400 e sarà riportato allo stato precedente alle modifiche.";
			plugins.dialogs.showErrorDialog("Errori nella compilazione",message,"Ok");
			return -1
		}else{
			return 0;
		}
	}
}

/**
 * @properties={typeid:24,uuid:"7A838D4C-BA40-412D-AA40-B8032BF534DB"}
 */
function localTransmitData(_rec) {
	//aggiunto parametro formale opzionale _rec di tipo JSRecord
	var data = packDataAndUser(_rec);
	//dep	var p = plugins.http.getPoster(globals.vc2_serviceLocationSe,"segnalazione_anomalia");
	var client = plugins.http.createNewHttpClient();
	var postReq = client.createPostRequest(globals.vc2_serviceLocationSe);
	//dep	p.setCharset("ISO-8859-1");
	postReq.setCharset("ISO-8859-1");
//dep	for (var s in data) {
		for (var _s in data) {
		//dep	p.addParameter(s, data[s]);
		postReq.addHeader(_s, data[_s])
	}
	//dep	code = p.doPost();
	var res = postReq.executeRequest();
	/** @type {String} */
	var response = null;
	//dep	if (code == 200){
	if (res!=null && res.getStatusCode() == 200) {
		//dep		response = globals.vc2_parseResponse(_rec, p.getPageData(), "SANREG_OUT", ["SACENT","SAMODE","SAVERS","SALEAD","SALEA2","SANOMI","SANOMV"]);
		response = globals.vc2_parseResponse(_rec, res.getResponseBody(), "SANREG_OUT", ["SACENT", "SAMODE", "SAVERS", "SALEAD", "SALEA2", "SANOMI", "SANOMV"]);
	} else {
		// Codice Errore WSNET1: errore nella connessione di rete il servizio non ha risposto 200.
		plugins.dialogs.showErrorDialog("Dati segnalazione non trasmessi", "Il servizio AS400 non è al momento disponibile. (WSNET1)", "Ok");
		globals.vc2_lastErrorLog = "Codice: " + res + " - Il servizio AS400 non è al momento disponibile. (WSNET1)";
		response = -1;
	}
	//Inizio LOG
	var table = databaseManager.getDataSourceTableName(controller.getDataSource());
	var table_key = _rec.k8_anomalie_id;
	var logic_key = (response != -1) ? _rec.ente_segnalazione_anomalia + " " + utils.stringToNumber(response) : _rec.ente_segnalazione_anomalia;
	globals.vc2_logTransactionToAS400(table, table_key, logic_key, data, globals.vc2_lastErrorLog, response, globals.vc2_getLogOperation());
	//Fine LOG
	return response;
}

/**
 * @properties={typeid:24,uuid:"BB3AEC03-6E34-4AF4-8EA7-B818962AF4C3"}
 */
function localPackDataAndUser(_rec){
	var data = localPackData(_rec)
	data["SACHIA"] = 'SERVOY';
	var userInfo = globals.vc2_getUserInfo(null);
	if (userInfo && userInfo.user_as400){
		if(data["SANREG"]){
			_rec.utente_modifica = userInfo.user_as400;
		}else{
			_rec.utente_creazione = userInfo.user_as400;
		}
		data["SANOMI"] = userInfo.user_as400;
	}
	return data;
}

/**
 * @properties={typeid:24,uuid:"4A504523-6E7F-4882-975B-9E9BC9D8EDB9"}
 */
function localPackData(_rec){
	var data = {'SACTIP' : 'T', //OBBLIGATORIO La Testata
				'SAVEEX' : globals.vc2_webServiceMode, //OBBLIGATORIO Sempre esecuzione?
				'SAOP'   :"I", //<INSERIMENTO/MODIFICA/CANCELLAZIONE> OBBLIGATORIO
				'SAAPPL' : "VIRTCAR2",
				'SACENT' : _rec.ente_segnalazione_anomalia, //<Cd Ente> OBBIGATORIO
				'SANREG' : _rec.numero_segnalazione, //<N Registrazione evento> OBBLIGATORIO IN MODIFICA.
				'SADREG' : globals.utils_dateFormatAS400(_rec.data_registrazione_segnalaz), //<Dt Reg.Evento> OBBLIGATORIO. YYYY MM DD
				'SADRIL' : globals.utils_dateFormatAS400(_rec.data_rilevazione), //<Dt Rilevazione> OBBLIGATORIO
				'SAFUNZ' : _rec.gruppo_funzionale, //<Gruppo Funzionale> OBBLIGATORIO
				'SASUGR' : _rec.sottogruppo_funzionale, //<Sottogruppo funzionale> OBBLIGATORIO
				'SADICS' : _rec.tipo_anomalia, //<Demerito ICS> Tipo Anomalia NOTA: definire di cosa si tratta. OBBLIGATORIO.
				'SADICP' : _rec.punteggio_demerito, //<Demerito ICP> OBBLIGATORIO.
				'SAMODE' : _rec.codice_modello, //<Modello> OBBLIGATORIO.
				'SAVERS' : _rec.codice_versione_ver, //<Versione> OBBLIGARIO
				'SAKPER' : _rec.km_percorsi, //<Km Percorsi> NON OBBLIGATORIO (Viene settato a 0 dal servizio).
				'SACOMP' : _rec.componente_vettura, //<Cod. Componente> OBBLIGATORIO NUMERICO
				'SALEAD' : _rec.segnalato_da, //<Segnalato da> OBBLIGATORIO (Servoy dovrebbe metterlo in automatico in base all'utenza).
				'SACDIS' : _rec.codice_disegno, //<Cd. Disegno> OBBLIGATORIO NUMERICO
				'SAPROV' : null, //<Stato Provvedim.> OBBLIGATORIO. Chiarire Funzione. Un carattere. 
				'SAMATR' : _rec.matricola_vettura, //<Matricola AOMAT> NON OBBLIGATORIO NUMERICO dal 12 giugno (sviluppo previsto)
				'SACCLI' : "0", //<Importatore> NON OBBLIGATORIO RIGUARDA UN IMPORTATORE O UN DEALER
				'SAFREQ' : _rec.frequenza, //<Frequenza> NON OBBLIGATORIO FLOAT separato da punto.
				'SACREC' : _rec.costo_unitario_recupero, //<Costo Un.Recupero> NON OBBLIGATORIO FLOAT separato da punto.
				'SAPRIO' : _rec.priorita, //<Priorita> NON OBBLIGATORIO
				'SACDIF' : _rec.codice_difetto, //<Cd Difetto> OBBLIGATORIO
				'SASTAT' : globals.utils_stateFormatAS400(_rec.stato_segnalazione), //<Stato> NON OBBLIGATORIO AS400 mette nulla per le aperte C chiuse e poi P e R
				'SALIB3' : " ", //<Libero> NON OBBLIGATORIO.
				'SADATV' : globals.utils_dateFormatAS400(_rec.timestamp_modifica || new Date()), //<Data Variaz.> NON OBBLIGATORIO
				'SALEA2' : _rec.leader, //<Leader> OBBLIGATORIO TESTO.
				'SANOMI' : _rec.utente_creazione, //<Utente Inser.>  NON OBBLIGATORIO, MA CONSIGLIATO in fase di test sara' SERVOY, poi ci sara' la mappatura.
				'SADATI' : globals.utils_dateFormatAS400(_rec.timestamp_creazione || new Date()), //<Data Inser.> NON OBBLIGATORIO, MA CONSIGLIATO, ARRIVA DA SERVOY
				'SANOMV' : _rec.utente_modifica, //<Utente Variaz.> NON OBBLIGATORIO, MA CONSIGLIATO in fase di test sara' SERVOY, poi ci sara' la mappatura.
				'SATCOM' : _rec.tipo_componente, //<Tipo componente> OBBLIGATORIO, assunme i valori F (Fisico) e V (Virtuale).
				'SAESPO' : _rec.esponente, //<Esponente> OBBLIGATORIO se 'Tipo componente' è 'V'
				'SAREVI' : _rec.revisione, //<Revisione> OBBLIGATORIO se 'Tipo componente' è 'V'
				'SACANO' : _rec.attribuzione //<Causale Anomalia> o <Classe Anomalia> OBBLIGATORIO.
				};
	return data;
}
