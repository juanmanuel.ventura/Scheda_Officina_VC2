/**
 * @properties={typeid:24,uuid:"96C73EEC-ED31-4350-9D1B-B82FDE1F3626"}
 */
function nfx_getTitle(){
	return "Segnalazioni Anomalie - Tabella";
}

/**
 * @properties={typeid:24,uuid:"847590E7-A4D3-4891-B191-CB4EC08404F5"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"92C29722-F156-483F-8F98-59F083626357"}
 */
function nfx_sks(){
	return [{icon:	"chiudi_segnalazione.png",	tooltip:"Chiudi Segnalazione",	method:"chiusuraSegnalazione"},
			{icon:	"apri_scheda.png",			tooltip:"Crea Scheda", 			method:"creazioneScheda"},
			{icon:	"report.png", 				tooltip:"Full report (BETA)",	method:"report"},
			{icon:	"warning.png",				tooltip:"Prova",				method:"wrap"}];
}

/**
 * @properties={typeid:24,uuid:"6130B46C-0008-4AA3-B4D4-8B99BA5DFF82"}
 */
function wrap(){
	globals.vc2_drawFieldsInForm(controller.getName(),null);
}
