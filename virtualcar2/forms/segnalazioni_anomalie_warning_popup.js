/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"74D9F553-644F-448C-952F-056B2E85B03B"}
 */
var _message = "";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"2B81B0A2-F338-4569-9EAA-D437C7D0BA31",variableType:4}
 */
var _notShowNextTime = 0;

/**
 * @properties={typeid:35,uuid:"17BBA1FE-5333-4A42-B6A4-599E030E0A56",variableType:-4}
 */
var _result = null;

/**
 * @properties={typeid:35,uuid:"ECDFC0EC-77D2-43BF-8F8F-FEF5DB9BEF62",variableType:-4}
 */
var _savingValueForm = null;

/**
 * @properties={typeid:24,uuid:"7CD00A20-82B8-4D7D-A0A6-CB65C7DF3072"}
 */
function openPopup(savingValueForm){
	_message = buildDisplayMessage();
	_savingValueForm = savingValueForm;
	_result = false;
	controller.show("showEnteWarning");
}

/**
 * @properties={typeid:24,uuid:"19F626C5-FABB-4D30-ABD2-AFBAC048F558"}
 */
function buildDisplayMessage(){
	var _e = globals.vc2_getUserInfo(null).ente_as400;
	var _d = getDescrizioneAssociata(_e);
	var _m1 = "Con l'ultimo aggiornamento di <b>VirtualCar2</b> è stata introdotta una nuova modalità di inserimento delle <b>segnalazioni anomalia</b> che lega ogni segnalazione al <b>centro di costo</b> appropriato. Tale centro di costo è stato preventivamente assegnato al Suo account, ma al fine di prevenire eventuali imprecisioni La preghiamo di verificarne la correttezza.";
	var _m2 = "Il centro di costo associato all'account <b style='color:red'>" + globals.nfx_user + "</b> è:<ul><li style='color:red;font-weight:bold;'>" + _e + " - " + _d + "</li></ul>È necessario modificare il centro di costo legato al Suo account?"
	return "<html><body><div style='font-size:8px; text-align:justify;'>" + _m1 + "</div><div style='font-size:8px; text-align:justify;'>" + _m2 + "</div></body></html>";
}

/**
 * @properties={typeid:24,uuid:"99EF49B2-3255-40AC-B25D-17230063A8CB"}
 * @AllowToRunInFind
 */
function getDescrizioneAssociata(ente){
	var _f = forms.ente_chooser.foundset.duplicateFoundSet();
	if(_f.find()){
		_f.ente = ente;
		_f.search();
		return _f.descrizione;
	}
	return "<i>Nessuna descrizione</i>";
}

/**
 * Handle changed data.
 *
 * @param {Object} oldValue old value
 * @param {Object} newValue new value
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean} valid value
 *
 * @properties={typeid:24,uuid:"7A2FC003-F812-4059-92CB-3EC8B7C484DC"}
 */
function setShowNextTime(oldValue, newValue, event) {
	var _value = (newValue) ? "no" : "yes";
	forms.nfx_interfaccia_gestione_salvataggi.save(_value,_savingValueForm);
}

/**
 * Perform the element default action.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"F7F4B340-1B62-4E52-8084-612F46EB3604"}
 */
function setResult(event) {
	_result = (event.getElementName() == "_yes") ? true : false;
//application.closeFormDialog();
	
	var w=controller.getWindow();
	w.destroy();
}

/**
 * @properties={typeid:24,uuid:"173EF2E1-8674-49FA-A824-78EE135CE762"}
 */
function getResult(){
	return _result;
}
