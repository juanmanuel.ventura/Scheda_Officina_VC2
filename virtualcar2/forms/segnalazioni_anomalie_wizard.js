/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"0025EE05-B007-4C44-A04A-DFF2FF57F28B",variableType:4}
 */
var currentStep = 1;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"91B23919-3949-4F93-BBB5-33F06BB84806"}
 */
var email = null;

/**
 * @properties={typeid:35,uuid:"2B558DAC-7806-4A37-BF75-F7079EAA3D72",variableType:-4}
 */
var saved = false;

/**
 * @properties={typeid:35,uuid:"F85C1B7F-0D14-4389-BA4E-A8875DE70D16",variableType:-4}
 */
var wiz_currentForm = null;

/**
 * @properties={typeid:35,uuid:"4289F609-CAE5-4C5D-B2BE-9370DBD4B6A5",variableType:-4}
 */
var wiz_forms = [];

/**
 *
 * @properties={typeid:24,uuid:"BEC62109-0E93-43BF-8D07-EDAE29A9C477"}
 */
function onCancelButton()
{
	//cancella record attuale;
	if (plugins.dialogs.showInfoDialog("Uscire?","Se si annulla alcuni dati e la mail al leader non verranno inviati","Si","No") == "Si"){
		databaseManager.revertEditedRecords();
		databaseManager.refreshRecordFromDatabase(foundset,-1);
		databaseManager.setAutoSave(true);
		
		
		//application.closeFormDialog();
		
		var w=controller.getWindow();
		w.destroy();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"14F49D8F-5329-4EB3-BB85-5A64DC6765FD"}
 */
function onChangeStep()
{
	switch(currentStep){
	case 3:
		databaseManager.revertEditedRecords();
		elements.stepsPanel.removeAllTabs();
		elements.stepsPanel.addTab(forms.segnalazioni_anomalie_wizard_step3)
		wiz_currentForm = forms.segnalazioni_anomalie_wizard_step3;
		break;
	case 2:
		elements.stepsPanel.removeAllTabs();
		elements.stepsPanel.addTab(forms.segnalazioni_anomalie_wizard_step2)
		wiz_currentForm = forms.segnalazioni_anomalie_wizard_step2;
		break;
	case 1:
		elements.stepsPanel.removeAllTabs();
		elements.stepsPanel.addTab(forms.segnalazioni_anomalie_wizard_step1)
		wiz_currentForm = forms.segnalazioni_anomalie_wizard_step1;
		break;
	default:
		elements.saveButton.enabled = elements.nextButton.enabled = elements.prevButton.enabled = false;
		break;
	}
	wiz_currentForm.controller.readOnly = false;
	wiz_currentForm["wiz_onSelect"]();
}

/**
 *
 * @properties={typeid:24,uuid:"76F927D4-5599-4819-8D1E-13CF85238528"}
 */
function onNextButton()
{
	if (wiz_currentForm.wiz_onNext() != -1){
		currentStep ++;
		onChangeStep();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"BF45B743-6498-459E-B4F1-881F42324766"}
 */
function onPrevButton()
{
	if (wiz_currentForm.wiz_onPrev() != -1){
		currentStep --;
		onChangeStep();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"41628477-1171-487E-A2D5-31E4140AE180"}
 */
function onSaveButton()
{
	//TODO cosa fare?
	if (wiz_currentForm.wiz_onSave() != -1){
		saved = true;
		databaseManager.setAutoSave(true);
		
		//application.closeFormDialog();
		
		var w=controller.getWindow();
		w.destroy();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"1DF29835-678A-46C7-AD0E-B532002912BA"}
 */
function onShow()
{
	wiz_forms = [forms.segnalazioni_anomalie_wizard_step3, forms.segnalazioni_anomalie_wizard_step2, forms.segnalazioni_anomalie_wizard_step1]
//FS aggiunto var 
	for (var i=0; i < wiz_forms.length; i++){
		wiz_forms[i]["wiz_form"] = forms[controller.getName()];
	}
	databaseManager.setAutoSave(false);
	currentStep = 1;
	saved = false;
	email = null;
	controller.newRecord(true);
	onChangeStep();
}
