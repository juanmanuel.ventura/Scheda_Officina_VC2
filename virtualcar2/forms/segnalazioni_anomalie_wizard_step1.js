/**@type {RuntimeForm}
 * @properties={typeid:35,uuid:"85B20F6A-D0AD-490D-B52C-C478EAE5EC7B",variableType:-4}
 */
var wiz_form = null;

/**
 *
 * @properties={typeid:24,uuid:"5116F9A2-5E32-43CD-96AB-83CCB86473FE"}
 */
function openTreePicker()
{

	if (codice_modello){
		globals.vc2_currentProgetto = codice_modello;
		//application.showFormInDialog(forms.distinte_progetto_albero_picker,null,null,null,null,"Seleziona numero disegno",true,false,"treePicker",true);
		
		var formq = forms.distinte_progetto_albero_picker;
		var window = application.createWindow("treePicker",JSWindow.MODAL_DIALOG);
		window.title="Seleziona numero disegno";
		window.resizable=true;
		formq.controller.show(window);
		
		
	}else{
		application.beep();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"06089B25-648C-401A-9D20-5750E615E5B0"}
 */
function wiz_onNext()
{
	var fields =  {//"data_rilevazione" : "Data rilevazione",
				"codice_versione" : "Codice versione",
				"codice_modello" : "Codice Modello",
				//"gruppo_funzionale": "Gruppo Funzionale",
				//"punteggio_demerito": "Demerito",
				//"codice_difetto": "Codice difetto",
				//"ente_segnalazione_anomalia": "Ente",
				//"segnalato_da": "Segnalatore",
				"codice_disegno" : "Codice disegno"}
				//"leader": "Leader"};
	//Da gestione_anomalie superclass
	return dataValidation(fields);
}

/**
 *
 * @properties={typeid:24,uuid:"DFA98880-0001-47D0-9498-C7A96ED371DB"}
 */
function wiz_onPrev()
{
	return -1;
}

/**
 *
 * @properties={typeid:24,uuid:"4141797A-5D1D-4130-B17F-E74B3431840B"}
 */
function wiz_onSelect()
{
	wiz_form.elements.saveButton.enabled = false;
	wiz_form.elements.nextButton.enabled = true;
	wiz_form.elements.prevButton.enabled = false;
	controller.loadRecords(wiz_form.foundset);
}
