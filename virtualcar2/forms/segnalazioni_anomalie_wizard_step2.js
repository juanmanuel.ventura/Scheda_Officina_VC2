/**@type {RuntimeForm}
 * @properties={typeid:35,uuid:"DB04241F-306F-4B39-A28B-1DB6E9515BE8",variableType:-4}
 */
var wiz_form = null;

/**
 * @properties={typeid:24,uuid:"4142F2F1-8E1C-4430-9FF9-7D3B29CE8AE4"}
 */
function wiz_onSelect()
{
	wiz_form.elements.saveButton.enabled = false;
	wiz_form.elements.nextButton.enabled = true;
	wiz_form.elements.prevButton.enabled = true;
	controller.loadRecords(wiz_form.foundset);
}

/**
 * @properties={typeid:24,uuid:"14555DEC-92A2-4BA5-B703-07F22E9C26FF"}
 */
function wiz_onNext()
{
	//salvataggio e invio su as400 della segnalazione
	if(nfx_validate() != -1){
		var email = globals.vc2_getAs400UserMail(foundset.leader, "user_as400");
		if (email){
			wiz_form.email = email;
		}else{
			wiz_form.email = plugins.dialogs.showInputDialog("Nessun indirizzo email per il Leader",
			"Il leader selezionato non ha un indirizzo email associato.");
		}
		if(nfx_postValidate() != -1){
			databaseManager.saveData();
			foundset.sort("k8_anomalie_id desc");
			forms.segnalazioni_anomalie_wizard_step3.riferimenti.ente = ente_segnalazione_anomalia;
			forms.segnalazioni_anomalie_wizard_step3.riferimenti.numero = numero_segnalazione;
		}else{
			return -1;
		}
	}else{
		return -1;
	}
	return 1;
}

/**
 * @properties={typeid:24,uuid:"B844CA4D-FA7B-4C20-88A3-7763CF2A8D5E"}
 */
function sendEmail()
{
}

/**
 * @properties={typeid:24,uuid:"5366A101-60BD-4011-AC7F-3C935A99471D"}
 */
function wiz_onPrev()
{
	return 0;
}
