/**
 * @properties={typeid:35,uuid:"5565B295-2662-4B3D-9E81-23BB56BB2436",variableType:-4}
 */
var riferimenti = { ente: null, numero: null };

/**
 * @properties={typeid:35,uuid:"0324B6A5-7BC9-4DD7-8118-7D1F17885E5C",variableType:-4}
 */
var wiz_form = null;

/**
 * @properties={typeid:24,uuid:"3452D4E4-E839-47F5-A16C-1DBB2EAACEFE"}
 */
function wiz_onSelect() {
	wiz_form.elements.saveButton.enabled = true;
	wiz_form.elements.nextButton.enabled = false;
	wiz_form.elements.prevButton.enabled = false;
	controller.newRecord();
	ente = riferimenti.ente;
	numero_scheda = riferimenti.numero;
}

/**
 * @properties={typeid:24,uuid:"817A1EDA-89A2-4A33-A3F9-06910CFFBC37"}
 */
function wiz_onNext() {
	return -1;
}

/**
 * @properties={typeid:24,uuid:"312B9228-F7DF-4439-AFBB-6CFFEA814423"}
 */
function wiz_onPrev() {
	return -1;
}

/**
 * @properties={typeid:24,uuid:"57695D94-FCE3-4763-B24B-F5084E01DBA3"}
 */
function wiz_onSave() {
	var success = sendToAS400();
	if (success != -1 && wiz_form.email) {
		plugins.mail.sendMail(wiz_form.email, "servoy@ferrari.com", "Nuova Segnalazione Anomalia", createMessage());
		databaseManager.saveData();
		foundset.sort("k8_segn_desc_full_id desc");
		riferimenti = { ente: null, numero: null };
		return 0;
	}
	return -1;
}

/**
 * @properties={typeid:24,uuid:"58D5916A-E866-4563-97D2-70820D5FEF80"}
 */
function createMessage() {
	var ente = wiz_form.ente_segnalazione_anomalia;
	var numero = wiz_form.numero_segnalazione;
	var modello = wiz_form.codice_modello;
	var versione = wiz_form.codice_versione;
	var demerito = wiz_form.punteggio_demerito;
	var descrizione = wiz_form.k8_segnalazioni_anomalie_to_k8_segn_desc_full.testo;
	//	JStaffa aggiunto output per variabile non utilizzata
//	application.output('virtualcar2 segnalazioni_anomalie_wizard_step3.createMessage() descrizione: ' + descrizione, LOGGINGLEVEL.INFO);
	return "L'utente '" + globals.nfx_user + "' ha inserito una segnalazione anomalia con i seguenti riferimenti: \n" + "----------------------------------------------\n" + "Ente e numero segn: " + ente + " " + numero + "\n" + "Vettura:            " + modello + " " + versione + "\n" + "Demerito:           " + demerito + "\n" + "Gruppo funzionale:  " + wiz_form.gruppo_funzionale + "\n" + "Descrizione:        " + testo + "\n" + "----------------------------------------------\n" + "Inserita usando Virtalcar2 (Scarica Virtualcar2 a http://f400.unix.ferlan.it:8080/)";
}
