/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D0F889E5-021F-4F82-855E-ABFFF17E7A66"}
 */
var nfx_orderBy_ = null;

/**
 * @properties={typeid:35,uuid:"CC99D087-FBE7-41BF-846E-57544F7BA072",variableType:-4}
 */
var nfx_related_ = ["anomalie_vetture_table","segnalazioni_anomalie_descrizione_record","segnalazioni_anomalie_analisi_record","segnalazioni_chiuse_con_sk_dettaglio_record","documenti_record","time_line_table","tag_table"];

/**
 *
 * @properties={typeid:24,uuid:"62228EB8-3F4A-47C0-9DBA-5837D88BED00"}
 */
function nfx_defineAccess(){
	return [false,false,false,true];
}
