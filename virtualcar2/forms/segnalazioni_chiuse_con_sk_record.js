/**
 *
 * @properties={typeid:24,uuid:"D946CC47-0B41-4E1E-B60E-82CF79D08F24"}
 */
function nfx_getTitle(){
	return "Segnalazioni (con Sk Anomalia)";
}

/**
 *
 * @properties={typeid:24,uuid:"BECBA105-79C7-46FD-96B1-6AF9EE7986E9"}
 */
function nfx_isProgram(){
	return false;
}

/**
 * @properties={typeid:24,uuid:"5122362F-4F06-44FC-AE42-7DE70A4EBA9B"}
 */
function nfx_onShow(){
	globals.nfx_goTo("segnalazioni_anomalie_record");
}
