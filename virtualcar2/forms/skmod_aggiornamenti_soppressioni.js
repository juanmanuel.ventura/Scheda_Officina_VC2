/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"DD6088DB-A631-4608-8A15-F7B3FC837BD6"}
 */
var nfx_orderBy = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"AD857594-8DB1-4B93-A5FA-E8C580EBF407"}
 */
var nfx_related = null;

/**
 * @properties={typeid:24,uuid:"19C70F45-A763-4122-A786-B65F0FFF3AC9"}
 */
function nfx_defineAccess(){
	return [false,false,false,true];
}
