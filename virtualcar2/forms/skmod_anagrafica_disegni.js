/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1AFCAE5D-8EE8-477C-BD7B-7E59D47714CF"}
 */
var nfx_orderBy = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"71AB178A-9817-45F1-9720-166255646210"}
 */
var nfx_related = null;

/**
 * @properties={typeid:24,uuid:"7DA97BA4-2C66-4C17-8D68-CA48D91EB5D4"}
 */
function nfx_defineAccess(){
	return [false,false,false,true];
}
