/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"76B15AA8-DB59-4476-BF4E-313F0860C4D1"}
 */
var nfx_orderBy = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1C2B3428-EF63-427D-B12D-B5F2E685B1E4"}
 */
var nfx_related = null;

/**
 * @properties={typeid:24,uuid:"F5A629FB-2CF9-44F9-A8C9-5C90B274D4E8"}
 */
function nfx_defineAccess(){
	return [true,true,true];
}
