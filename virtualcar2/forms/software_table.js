/**
 * @properties={typeid:24,uuid:"F8EF46AA-A4C6-4FCF-90F1-11F5130C3B33"}
 */
function nfx_getTitle(){
	return "Software";
}

/**
 * @properties={typeid:24,uuid:"20290BB2-E7A7-4C50-AACC-D492884F8F77"}
 */
function nfx_isProgram(){
	return false;
}
