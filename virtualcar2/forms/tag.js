/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"D581DEEC-57E8-4BEB-939C-354BF0B59039"}
 */
var nfx_orderBy = "k8_tag_id asc";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F52E6FB9-EE5A-4DA0-9475-06C0014BDF4E"}
 */
var valuesToCheck = null;

/**
 *
 * @properties={typeid:24,uuid:"4C874E87-07E0-4316-8C52-2411D61E5697"}
 */
function nfx_defineAccess()
{
	if((forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero.controller.getName()
			|| forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero_lite.controller.getName())
			&& globals.vc2_progettiCurrentNode == -1)
	{
		return [false,true,true];
	}
	else
	{
		return [true,true,true];
	}
}

/**
 *
 * @properties={typeid:24,uuid:"01E593FA-A4DC-47D5-9AB6-AFF640226B83"}
 */
function nfx_postAdd()
{
	if((forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero.controller.getName()
			|| forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero_lite.controller.getName())
			&& plugins.dialogs.showQuestionDialog("Propagazione","Propago la TAG a tutti gli elementi sottostanti?","Sì","No") == "Sì")
	{
		var form = forms.nfx_interfaccia_pannello_body.mainForm;
		//FS
		//dep var query = "select k8_distinte_progetto_id,numero_disegno from " + forms[form].controller.getTableName() + " where path_id like ?";
		 var query = "select k8_distinte_progetto_id,numero_disegno from " + forms[form].databaseManager.getDataSourceTableName(controller.getDataSource())+ " where path_id like ?";
		var selectedIndex = controller.getSelectedIndex();
		var selectedRecord = foundset.getRecord(selectedIndex);		
		//dep 	var ids = databaseManager.getDataSetByQuery(controller.getServerName(),query,[globals.vc2_progettiCurrentNodePath.replace("%","/%")],-1);
		var ids = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()),query,[globals.vc2_progettiCurrentNodePath.replace("%","/%")],-1);

		
		for(var i=1;i<=ids.getMaxRowIndex();i++)
		{
			var id = ids.getValue(i,1);
			
			databaseManager.setAutoSave(false);
			controller.newRecord();
			
			record_fk = id;
			table_name_fk = forms[form].tableName;
			
			tag = selectedRecord.tag;
			valore = selectedRecord.valore;
			note = selectedRecord.note;
			
			databaseManager.saveData();
			databaseManager.setAutoSave(true);
		}
		
		if(controller.getName() == globals.nfx_selectedTab())
		{
			globals.nfx_syncTabs();
		}
		
		plugins.dialogs.showInfoDialog("Operazione terminata","La propagazione di " + ids.getMaxRowIndex() + " TAG è terminata.","Ok");
	}
}

/**
 *
 * @properties={typeid:24,uuid:"6E194433-4290-4B75-811A-9FFE4CFD153E"}
 * @AllowToRunInFind
 */
function nfx_postRemove()
{
	if((forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero.controller.getName()
			|| forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero_lite.controller.getName())
			&& plugins.dialogs.showQuestionDialog("Propagazione","Cancello la TAG da tutti gli elementi sottostanti?","Sì","No") == "Sì")
	{
		//FS
		var form = forms.nfx_interfaccia_pannello_body.mainForm;
//dep		var query = "select k8_distinte_progetto_id,numero_disegno from " + forms[form].controller.getTableName() + " where path_id like ?";
		var query = "select k8_distinte_progetto_id,numero_disegno from " + forms[form].databaseManager.getDataSourceTableName(controller.getDataSource()) + " where path_id like ?";

//dep		var ids = databaseManager.getDataSetByQuery(controller.getServerName(),query,[globals.vc2_progettiCurrentNodePath.replace("%","/%")],-1);
		var ids = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()),query,[globals.vc2_progettiCurrentNodePath.replace("%","/%")],-1);

		var toDelete = 0;
		var deleted = 0;
		
		for(var i=1;i<=ids.getMaxRowIndex();i++)
		{
			controller.loadAllRecords();
			
			var id = ids.getValue(i,1);
			
			if(controller.find())
			{
				record_fk = id;
				table_name_fk = forms[form].tableName;
				
				tag = valuesToCheck.tag;
				valore = valuesToCheck.valore;
				note = valuesToCheck.note;
				
				controller.search();
			}
			
			toDelete += foundset.getSize();
			
			databaseManager.setAutoSave(false);
			for(var j=1;j<=foundset.getSize();j++)
			{
				foundset.setSelectedIndex(i);
				
				var success = databaseManager.acquireLock(foundset,foundset.getSelectedIndex());
				if(success)
				{
					controller.deleteRecord();
					deleted++;
					databaseManager.releaseAllLocks();
				}
			}
			databaseManager.setAutoSave(true);
		}
		
		if(controller.getName() == globals.nfx_selectedTab())
		{
			globals.nfx_syncTabs();
		}
		
		plugins.dialogs.showInfoDialog("Operazione terminata",deleted + " TAG sono state eliminate, " + (toDelete-deleted) + " hanno creato problemi.\nOperazione completata","Ok");
	}
}

/**
 *
 * @properties={typeid:24,uuid:"703ABA84-D92A-4EA8-AF13-91438AED5774"}
 */
function nfx_preRemove()
{
	if((forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero.controller.getName()
			|| forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero_lite.controller.getName())
			&& globals.vc2_progettiCurrentNode == -1)
	{
		plugins.dialogs.showWarningDialog("Attenzione","Selezionare un elemento dall'albero per rimuovere una TAG","Ok");
		return -1;
	}
	if((forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero.controller.getName()
			|| forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero_lite.controller.getName())
			&& globals.vc2_progettiCurrentNode != -1)
	{
		var selectedIndex = controller.getSelectedIndex();
		valuesToCheck = foundset.getRecord(selectedIndex);
	}
	return null;
}
