/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"263CA4A2-069E-441B-AA1F-5507F882B979"}
 */
var nfx_orderBy = "k8_time_line_id asc";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"4F53DE87-A579-4F91-B7F4-29D2E401F8BE"}
 */
var valuesToCheck = null;

/**
 *
 * @properties={typeid:24,uuid:"A8783886-763F-49F4-96F5-5863BA6B90CF"}
 */
function nfx_defineAccess() {
	if ( (forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero.controller.getName() || forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero_lite.controller.getName()) && globals.vc2_progettiCurrentNode == -1) {
		return [false, true, true];
	} else {

		return [true, true, true];
	}
}

/**
 *
 * @properties={typeid:24,uuid:"0177239C-EB7B-412E-8949-9B82783EDF35"}
 */
function nfx_postAdd() {
	if (numero_disegno) {
		codice_articolo_baan = k8_time_line_to_k8_jt_as400_baan$baan.codice_articolo_baan;
	} else if (codice_articolo_baan) {
		numero_disegno = k8_time_line_to_k8_jt_as400_baan$as400.numero_disegno;
	}

	if ( (forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero.controller.getName() || forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero_lite.controller.getName()) && plugins.dialogs.showQuestionDialog("Propagazione", "Propago la delibera a tutti gli elementi sottostanti?", "Sì", "No") == "Sì") {
		var form = forms.nfx_interfaccia_pannello_body.mainForm;
//FS		
//dep		var query = "select k8_distinte_progetto_id,numero_disegno from " + forms[form].controller.getTableName() + " where path_id like ?";
		var query = "select k8_distinte_progetto_id,numero_disegno from " + forms[form].databaseManager.getDataSourceTableName(controller.getDataSource()) + " where path_id like ?";

		var selectedIndex = controller.getSelectedIndex();
		var selectedRecord = foundset.getRecord(selectedIndex);

		//dep		var ids = databaseManager.getDataSetByQuery(controller.getServerName(),query,[globals.vc2_progettiCurrentNodePath.replace("%","/%")],-1);
		var ids = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, [globals.vc2_progettiCurrentNodePath.replace("%", "/%")], -1);
		for (var i = 1; i <= ids.getMaxRowIndex(); i++) {
			var id = ids.getValue(i, 1);
			var nd = ids.getValue(i, 2);
			var cb = null;

			if (nd) {
	//FS			
	//dep				var tmp = databaseManager.getDataSetByQuery(controller.getServerName(), "select codice_articolo_baan from k8_jt_as400_baan where numero_disegno = ?", [nd], 1);
				var tmp = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), "select codice_articolo_baan from k8_jt_as400_baan where numero_disegno = ?", [nd], 1);

				cb = tmp.getValue(1, 1);
			}

			databaseManager.setAutoSave(false);
			controller.newRecord();
			record_fk = id;
			table_name_fk = forms[form].tableName;
			numero_disegno = nd;
			codice_articolo_baan = cb;

			descrizione = selectedRecord.descrizione;
			inizio_previsto = selectedRecord.inizio_previsto;
			inizio_effettivo = selectedRecord.inizio_effettivo;
			fine_prevista = selectedRecord.fine_prevista;
			fine_effettiva = selectedRecord.fine_effettiva;
			note = selectedRecord.note;

			databaseManager.saveData();
			databaseManager.setAutoSave(true);
		}

		if (controller.getName() == globals.nfx_selectedTab()) {
			globals.nfx_syncTabs();
		}

		plugins.dialogs.showInfoDialog("Operazione terminata", "La propagazione di " + ids.getMaxRowIndex() + " delibere è terminata.", "Ok");
	}

}

/**
 *
 * @properties={typeid:24,uuid:"F8CE0F15-97AC-47A0-AAF6-9E2412C5CC4E"}
 * @AllowToRunInFind
 */
function nfx_postRemove() {
	if ( (forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero.controller.getName() || forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero_lite.controller.getName()) && plugins.dialogs.showQuestionDialog("Propagazione", "Cancello la delibera da tutti gli elementi sottostanti?", "Sì", "No") == "Sì") {
		var form = forms.nfx_interfaccia_pannello_body.mainForm;
//dep		var query = "select k8_distinte_progetto_id,numero_disegno from " + forms[form].controller.getTableName() + " where path_id like ?";
		var query = "select k8_distinte_progetto_id,numero_disegno from " + forms[form].databaseManager.getDataSourceTableName(controller.getDataSource()) + " where path_id like ?";

//dep		var ids = databaseManager.getDataSetByQuery(controller.getServerName(), query, [globals.vc2_progettiCurrentNodePath.replace("%", "/%")], -1);
		var ids = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, [globals.vc2_progettiCurrentNodePath.replace("%", "/%")], -1);

		var toDelete = 0;
		var deleted = 0;

		for (var i = 1; i <= ids.getMaxRowIndex(); i++) {
			controller.loadAllRecords();

			var id = ids.getValue(i, 1);
			var nd = ids.getValue(i, 2);

			if (controller.find()) {
				record_fk = id;
				table_name_fk = forms[form].tableName;
				numero_disegno = nd;

				descrizione = valuesToCheck.descrizione;
				inizio_previsto = valuesToCheck.inizio_previsto;
				inizio_effettivo = valuesToCheck.inizio_effettivo;
				fine_prevista = valuesToCheck.fine_prevista;
				fine_effettiva = valuesToCheck.fine_effettiva;
				note = valuesToCheck.note;

				controller.search();
			}

			toDelete += foundset.getSize();

			databaseManager.setAutoSave(false);
			for (var j = 1; j <= foundset.getSize(); j++) {
				foundset.setSelectedIndex(i);

				var success = databaseManager.acquireLock(foundset, foundset.getSelectedIndex());
				if (success) {
					controller.deleteRecord();
					deleted++;
					databaseManager.releaseAllLocks();
				}
			}
			databaseManager.setAutoSave(true);
		}

		if (controller.getName() == globals.nfx_selectedTab()) {
			globals.nfx_syncTabs();
		}

		plugins.dialogs.showInfoDialog("Operazione terminata", deleted + " delibere sono state eliminate, " + (toDelete - deleted) + " hanno creato problemi.\nOperazione completata", "Ok");
	}

}

/**
 *
 * @properties={typeid:24,uuid:"434D57C3-0B4B-4AFE-BC70-F61B509244F4"}
 */
function nfx_preRemove() {

	if ( (forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero.controller.getName() || forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero_lite.controller.getName()) && globals.vc2_progettiCurrentNode == -1) {
		plugins.dialogs.showWarningDialog("Attenzione", "Selezionare un elemento dall'albero per rimuovere una delibera", "Ok");
		return -1;
	}
	if ( (forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero.controller.getName() || forms.nfx_interfaccia_pannello_body.mainForm == forms.distinte_progetto_albero_lite.controller.getName()) && globals.vc2_progettiCurrentNode != -1) {
		var selectedIndex = controller.getSelectedIndex();
		valuesToCheck = foundset.getRecord(selectedIndex);
	}
	return null;
}

/**
 *
 * @properties={typeid:24,uuid:"6A733ED1-6ABE-41AC-96BD-81A2A4DC725F"}
 */
function nfx_validate() {
	//FS
	//dep	var tableName = controller.getTableName(); var tableName non usata la tolgo?
	var sourceTable = databaseManager.getDataSourceTableName(controller.getDataSource());
	//	JStaffa aggiunto output per variabile non utilizzata
//	application.output('virtualca2 time_line.nfx_validate() sourceTable: ' + sourceTable,LOGGINGLEVEL.INFO);
	var missing = [];

	var allFields = { "descrizione": "Descrizione", "fine_prevista": "Fine Prevista" };
	//aggiunto var davanti f
	for (var f in allFields) {
		if (!foundset[f]) {
			missing.push(f);
		}
	}

	if (missing.length > 0) {
		var message = "Compilare i campi obbligatori:\n\n";
		var i = 1;
		//aggiunto var davanti mf
		for each (var mf in missing) {
			message += i++ + ". Inserire " + allFields[mf] + "\n";
		}
		plugins.dialogs.showErrorDialog("Errori nella compilazione",
			message, "Ok");
		return -1;
	} else {
		return 0;
	}

}

/**@param {Boolean} isToDisable
 * @properties={typeid:24,uuid:"835415AB-7A51-404E-ABD3-E448D24B3725"}
 */
function disableDateFields(isToDisable) {

	//disabilito in modifica i campi delle date per la visualizzazione record o tabellare
	forms.time_line_record.elements.fine_effettiva.enabled=!isToDisable;
	forms.time_line_record.elements.fine_prevista.enabled=!isToDisable;
	forms.time_line_record.elements.inizio_previsto.enabled=!isToDisable;
	forms.time_line_record.elements.inizio_effettivo.enabled=!isToDisable;
	
	forms.time_line_table.elements.fine_effettiva.enabled=!isToDisable;
	forms.time_line_table.elements.fine_prevista.enabled=!isToDisable;
	forms.time_line_table.elements.inizio_previsto.enabled=!isToDisable;
	forms.time_line_table.elements.inizio_effettivo.enabled=!isToDisable;
	
}

/**
 * Callback method form when editing is started.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @returns {Boolean}
 *
 * @protected 
 *
 * @properties={typeid:24,uuid:"93EF7651-395A-4E51-BE1D-C152702E1E7E"}
 */
function onRecordEditStart(event) {
	disableDateFields(true);
	return true;
}

/**
 * @properties={typeid:24,uuid:"775C0309-AF46-4432-BBF0-392C9B5FF656"}
 */
function nfx_preAdd() {
	//application.output("START time_line.nfx_preAdd abilito l'edit delle date");
	disableDateFields(false);
	//application.output("STOP time_line.nfx_preAdd");

}
