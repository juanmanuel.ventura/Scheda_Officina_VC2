/**
 * @properties={typeid:24,uuid:"1D85ED19-F45E-4AA9-BEE2-EA411A685BDF"}
 */
function nfx_defineAccess(){
	return [false,false,false,true];
}

/**
 * @properties={typeid:24,uuid:"E449ABEA-5499-437F-87BE-16830730DB77"}
 */
function nfx_sks(){
	return [{icon: "disk_arrow.png",	tooltip:"Salva il log come file di testo",		method:"saveLog"},
	        {icon: "Rosso.png",			tooltip:"Visualizza solo trasmissioni fallite",	method:"onlyFails"}];
}

/**
 * @properties={typeid:24,uuid:"D962B186-F624-4C47-B763-EF13F1E949C8"}
 */
function saveLog(){
	globals.utils_saveAsTXT(as400_response,"log_" + utils.dateFormat(new Date(),"yyyyMMdd") + ".txt");
}

/**
 * @properties={typeid:24,uuid:"7F1DF7DD-5A12-4F93-90B1-2BF75CBBB9CD"}
 * @AllowToRunInFind
 */
function onlyFails(){
	if(foundset.find()){
		foundset.result = -1;
		foundset.search(false,true);
	}
}
