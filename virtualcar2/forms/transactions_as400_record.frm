dataSource:"db:/ferrari/k8_transaction_log",
extendsID:"0AC3C1FF-DE81-4D81-99B1-5ABDF87E2479",
items:[
{
dataProviderID:"logic_key",
editable:false,
location:"330,60",
name:"logic_key",
size:"260,20",
text:"Logic Key",
typeid:4,
uuid:"06F3D32E-8348-44FB-BD37-F3F863B446E7"
},
{
anchors:3,
location:"730,70",
mediaOptions:14,
name:"defect_chooser",
onActionMethodID:"A25ED0E1-1ECC-434B-B507-E9DFF1D51CB6",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
size:"80,16",
text:"Difetti",
typeid:7,
uuid:"3B0B0141-62A2-4DFA-920C-416FD88A2358"
},
{
dataProviderID:"logged_user",
editable:false,
location:"80,10",
name:"logged_user",
size:"140,20",
text:"Logged User",
typeid:4,
uuid:"4632F7E8-23CC-4E92-A907-1EE1F789692C"
},
{
borderType:"EmptyBorder,0,0,0,0",
dataProviderID:"result_icon",
displayType:9,
editable:false,
location:"520,90",
name:"result_icon",
scrollbars:36,
size:"20,20",
text:"Result Icon",
transparent:true,
typeid:4,
uuid:"4F0C2266-2F1E-4D54-B315-286D36DE07FC"
},
{
dataProviderID:"operation_timestamp",
editable:false,
format:"dd/MM/yyyy - HH:mm:ss",
location:"330,10",
name:"operation_timestamp",
size:"260,20",
text:"Operation Timestamp",
typeid:4,
uuid:"684DC89F-928A-4F1C-A9B0-3F6CC9DCE157"
},
{
anchors:3,
location:"730,30",
mediaOptions:14,
name:"ente_chooser",
onActionMethodID:"A25ED0E1-1ECC-434B-B507-E9DFF1D51CB6",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
size:"80,16",
text:"Enti",
typeid:7,
uuid:"6B0181E6-6F49-4216-970C-CE8B36DCC6F2"
},
{
horizontalAlignment:4,
labelFor:"table_key",
location:"230,35",
mediaOptions:14,
size:"90,20",
styleClass:"form",
tabSeq:-1,
text:"ID record",
transparent:true,
typeid:7,
uuid:"73A7B7D2-DDEF-40C3-9E72-97CD741BEBE1"
},
{
dataProviderID:"table_name",
editable:false,
location:"80,35",
name:"table_name",
size:"140,20",
text:"Table Name",
typeid:4,
uuid:"79513715-1BEC-4365-BC28-0C371197A898"
},
{
anchors:15,
dataProviderID:"as400_response",
displayType:1,
editable:false,
fontType:"Lucida Console,0,12",
location:"520,120",
name:"as400_response",
size:"300,150",
text:"As400 Response",
typeid:4,
uuid:"7960BE7E-8C78-4EC4-AA0F-13568659539E"
},
{
horizontalAlignment:4,
labelFor:"table_name",
location:"10,35",
mediaOptions:14,
size:"60,20",
styleClass:"form",
tabSeq:-1,
text:"Tabella",
transparent:true,
typeid:7,
uuid:"7E510410-30E5-40A7-9A02-3B62A365E212"
},
{
height:480,
partType:5,
typeid:19,
uuid:"80B2CEC2-E840-4E51-8520-633E01D02758"
},
{
labelFor:"message4human",
location:"10,90",
mediaOptions:14,
size:"80,20",
styleClass:"form",
tabSeq:-1,
text:"Dati inviati",
transparent:true,
typeid:7,
uuid:"8E075D20-A26A-4A13-8F9D-D34CE4A3327D"
},
{
horizontalAlignment:4,
labelFor:"operation_timestamp",
location:"230,10",
mediaOptions:14,
size:"90,20",
styleClass:"form",
tabSeq:-1,
text:"Data/Ora",
transparent:true,
typeid:7,
uuid:"8EAE2ADC-3C3E-4E5A-B737-16D252A18273"
},
{
anchors:3,
borderType:"TitledBorder,Liste,4,2,DialogInput.plain,1,12,#333333",
formIndex:-1,
lineSize:1,
location:"720,10",
size:"100,90",
transparent:true,
typeid:21,
uuid:"900BEF9E-85A7-4364-BB8D-5A71CBEEBC03"
},
{
anchors:13,
dataProviderID:"message4human",
displayType:1,
editable:false,
location:"10,120",
name:"message4human",
size:"500,150",
text:"Message4human",
typeid:4,
uuid:"92FF0784-19E5-4701-97AA-68A9EC13509B"
},
{
dataProviderID:"table_key",
editable:false,
format:"###############",
location:"330,35",
name:"table_key",
size:"260,20",
text:"Table Key",
typeid:4,
uuid:"9860FB1C-A68D-45A0-B9F8-C12FDBCB5A18"
},
{
dataProviderID:"operation",
location:"80,60",
name:"operation",
size:"140,20",
text:"Operation",
typeid:4,
uuid:"BE60ABCB-92B0-430A-B083-C286A07A49D9",
valuelistID:"6C0FFED0-1E6B-4F48-B196-AF98A7214E6F"
},
{
horizontalAlignment:4,
labelFor:"operation",
location:"10,60",
mediaOptions:14,
size:"60,20",
styleClass:"form",
tabSeq:-1,
text:"Tipo",
transparent:true,
typeid:7,
uuid:"C65EB811-1716-4B5A-9FC4-08B1C04CCD79"
},
{
anchors:3,
location:"730,50",
mediaOptions:14,
name:"leader_chooser",
onActionMethodID:"A25ED0E1-1ECC-434B-B507-E9DFF1D51CB6",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
size:"80,16",
text:"Leader",
typeid:7,
uuid:"C8128F3B-5310-45D8-A1A2-E265C01830F2"
},
{
horizontalAlignment:4,
labelFor:"logic_key",
location:"230,60",
mediaOptions:14,
size:"90,20",
styleClass:"form",
tabSeq:-1,
text:"Chiave logica",
transparent:true,
typeid:7,
uuid:"D1F7943C-07E5-45DA-B553-C479C5688D82"
},
{
labelFor:"as400_response",
location:"540,90",
mediaOptions:14,
size:"80,20",
styleClass:"form",
tabSeq:-1,
text:"Risposta",
transparent:true,
typeid:7,
uuid:"E1FE3E65-0FAD-4D3B-9CA9-F643F9AF1BE1"
},
{
horizontalAlignment:4,
labelFor:"logged_user",
location:"10,10",
mediaOptions:14,
size:"60,20",
styleClass:"form",
tabSeq:-1,
text:"Utente",
transparent:true,
typeid:7,
uuid:"EA44436E-D1E6-44BE-8F41-0E4AE60B2480"
}
],
name:"transactions_as400_record",
paperPrintScale:100,
showInMenu:true,
size:"830,480",
styleName:"keeneight",
typeid:3,
uuid:"A7DE9407-5A51-46D4-BE12-C6B945DB3911"