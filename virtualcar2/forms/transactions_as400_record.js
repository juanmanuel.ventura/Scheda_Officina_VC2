/**
 * @properties={typeid:24,uuid:"6C97AE37-2C01-40CA-8462-BC58D1138C93"}
 */
function nfx_getTitle(){
	return "Transazioni AS400";
}

/**
 * @properties={typeid:24,uuid:"72B3A87B-1D6B-4001-964E-FA1E9BF0BDC1"}
 */
function nfx_isProgram(){
	return true;
}

/**
 * @properties={typeid:24,uuid:"F4E636FA-7BF4-41B8-9390-016C37CADD34"}
 */
function nfx_defaultPermissions(){
	return "^";
}

/**
 * @properties={typeid:24,uuid:"A25ED0E1-1ECC-434B-B507-E9DFF1D51CB6"}
 */
function open(event){
	forms[event.getElementName()].openPopup();
}
