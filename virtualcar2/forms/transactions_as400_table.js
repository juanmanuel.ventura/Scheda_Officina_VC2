/**
 * @properties={typeid:24,uuid:"1BE89113-647A-46D6-B671-001CA75B3E20"}
 */
function nfx_getTitle(){
	return "Transazioni AS400 - Tabella";
}

/**
 * @properties={typeid:24,uuid:"2BB416CE-3955-4A01-87AF-82BBA8252CFE"}
 */
function nfx_isProgram(){
	return false;
}

/**
 * @properties={typeid:24,uuid:"8C43A100-FB94-4707-A935-7B7C111C9EB4"}
 */
function nfx_defaultPermissions(){
	return "^";
}
