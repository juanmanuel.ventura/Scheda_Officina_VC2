/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F8722F93-DF23-4BE2-987F-161C74E78181"}
 */
var nfx_orderBy = "k8_variabili_ambientali_id asc";

/**
 *
 * @properties={typeid:24,uuid:"2B6A76D3-40AB-4AEF-B28A-F4264FFC9AA3"}
 */
function nfx_defineAccess(){
	return [true,true,true];
}

/**
 *
 * @properties={typeid:24,uuid:"6D6277F6-5C42-4586-90B7-DC886B8A74BC"}
 */
function nfx_isProgram(){
	return false;
}
