/**
 * @properties={typeid:35,uuid:"6CE597E5-390C-42D5-B577-91F77CE6851F",variableType:-4}
 */
var lastDate;

/**
 *
 * @properties={typeid:24,uuid:"B654BBB5-3678-4655-8FB7-468F56A5E52A"}
 */
function copyDate(rec)
{

	rec.data_montaggio = lastDate;
}

/**
 *
 * @properties={typeid:24,uuid:"93CBB7CA-E35E-4973-935A-EA5E787AE6AF"}
 * @AllowToRunInFind
 */
function onShow()
{
	if(controller.find()){
		vettura = globals.vc2_currentVettura;
		controller.search();
	}
	controller.sort("data_montaggio desc");
}

/**
 *
 * @properties={typeid:24,uuid:"11A62A20-0DDE-42CC-BC28-EAA9283E4F0E"}
 */
function propagateDate()
{
	if(plugins.dialogs.showQuestionDialog("Propagare?",
		"Voi propagare la data di montaggio ai figli di questo componente?","Si", "No") == "Si"){
		propagateDateToChildren();
		databaseManager.recalculate(foundset);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"E0CED561-09C2-433E-8446-810D3A72D306"}
 */
function propagateDateToChildren()
{
	var rec = foundset.getRecord(controller.getSelectedIndex());
	lastDate = rec.data_montaggio;
	globals.utils_descendTreeNode(rec["k8_distinte_vetture$figli"], "k8_distinte_vetture$figli", copyDate);
}
