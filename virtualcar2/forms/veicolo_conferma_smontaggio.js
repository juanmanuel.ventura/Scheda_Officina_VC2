/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"426342D3-0B45-48ED-94A1-5D9B3BB33B3C"}
 */
var cause = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"220A009A-A478-4D82-8D03-8E18408C087E"}
 */
var isBroken = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"319D7371-ED90-4C4B-94D1-9CED3A9B619C"}
 */
var unmount = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"EE7EA098-D392-4494-A1FD-70C421EF9BCD",variableType:93}
 */
var unmountDate = new Date();

/**
 *
 * @properties={typeid:24,uuid:"740E1B9E-1289-4400-BAAB-EB6E97A2D038"}
 */
function onShow()
{
	isBroken = "No";
	unmountDate = new Date();
	cause = null;
	unmount = null;
}

/**
 *
 * @properties={typeid:24,uuid:"269C99F1-81A8-4E67-8997-05956B4B6D03"}
 */
function save()
{//FS cosa fare?
	var phrase = (isBroken == "Si") ? "Verrà smontato e segnalato come guasto il componente \"" + globals.vc2_vettureCurrentNodeFoundset.descrizione +  "\" e tutti i suoi figli, procedere?" : "Verrà smontato il componente \"" + globals.vc2_vettureCurrentNodeFoundset.descrizione +  "\" e tutti i suoi figli, procedere?" 
	if(plugins.dialogs.showQuestionDialog("Smontaggio",phrase,"Sì","No") == "Sì"){
		unmount = true;
	}
	
	//application.closeFormDialog();
	
	var w=controller.getWindow();
	w.destroy();
}
