/**
 *
 * @properties={typeid:24,uuid:"28B3B0FC-700C-4B66-8398-FCD71FE5CDCA"}
 * @AllowToRunInFind
 */
function createDistintaVettura(p) {

	if (controller.find()) {
		vettura = globals.vc2_currentVettura;
		numero_disegno = p.numero_disegno;
		//numero_disegno_padre = p.numero_disegno_padre;
		codice_padre=p.numero_disegno_padre;
		controller.search();
		if (foundset.getSize() > 0) {
			return -1;
		}
	} else {
		return -1;
	}
	controller.newRecord();
	numero_disegno = p.numero_disegno;
	//numero_disegno_padre = p.numero_disegno_padre;
	codice_padre=p.numero_disegno_padre;
	descrizione = p.descrizione;
	esponente = p.esponente;
	significativita = p.significativita;
	affidabilita = p.affidabilita;
	//k8_distinte_progetto_id = p.id;
	fk_distinte_progetto=p.id;
	vettura = globals.vc2_currentVettura;
	progetto = globals.vc2_currentProgetto;
	data_montaggio = new Date();

	databaseManager.saveData();
	return -1;
}

/**
 *
 * @properties={typeid:24,uuid:"16C39A17-8346-43C1-8BD0-DB70C2EF9E5B"}
 */
function deleteNode() {
	if (globals.vc2_vettureCurrentNode > 0) {
		globals.vc2_vettureCurrentNodeFoundset.deleteRecord(1);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"B240E270-BDAD-4149-8476-59607A811B6E"}
 */
function dragEnter() {
	//application.output("DRAG ENTER");
	return true;
}

/**
 *
 * @properties={typeid:24,uuid:"ED4F630E-E354-4112-9017-9876A683F676"}
 */
function dragOver() {
	//application.output("DRAG OVER");
	return true;
}

/**
 *
 * @properties={typeid:24,uuid:"CE03B49A-4BB3-4993-B546-ED38D06BD372"}
 */
function drop() {
	//application.output("DROP");
	forms.veicolo_montaggio_vetture_progetti.copyNodes();
	return true;
}

/**
 *
 * @properties={typeid:24,uuid:"3D42A373-D809-41B6-84BA-E8E6C025492B"}
 */
function hideTree() {
	elements.vettureTreeView.visible = false;
	elements.warning.visible = true;
}

/**
 *
 * @properties={typeid:24,uuid:"3D988618-719D-4E23-AA0F-7A436549190D"}
 * @AllowToRunInFind
 */
function onClick(value) {
	if (controller.find()) {
		k8_distinte_vetture_id = value;
		controller.search();
	}
	globals.vc2_vettureCurrentNode = k8_distinte_vetture_id;
	globals.vc2_vettureCurrentNodeFoundset = foundset;
}

/**
 *
 * @properties={typeid:24,uuid:"6E080C45-49D0-48AA-8CE0-1E8F85738852"}
 */
function onCurrentVetturaChange() {
	vettureTreeViewSetRoots();
}

/**
 *
 * @properties={typeid:24,uuid:"97174E3B-8BBB-445A-9D60-DB7B5322E878"}
 */
function onLoad() {
	//FS
	//dep	var vettureTreeViewBinding = elements.vettureTreeView.createBinding(controller.getServerName(),controller.getTableName());
	var vettureTreeViewBinding = elements.vettureTreeView.createBinding(databaseManager.getDataSourceServerName(controller.getDataSource()), databaseManager.getDataSourceTableName(controller.getDataSource()));
	vettureTreeViewBinding.setTextDataprovider("descrizione");
	vettureTreeViewBinding.setNRelationName("k8_distinte_vetture$figli");
	vettureTreeViewBinding.setMethodToCallOnClick(onClick, "k8_distinte_vetture_id");
	vettureTreeViewBinding.setImageURLDataprovider("treeIconAffi");
	//elements.vettureTreeView.setOnDragEnterMethod(dragEnter);
	//elements.vettureTreeView.setOnDragOverMethod(dragOver);
	//elements.vettureTreeView.setOnDropMethod(drop);
}

/**
 *
 * @properties={typeid:24,uuid:"4B63FE8C-6B43-4EFA-BB45-1BF64BDD5B53"}
 */
function showTree() {
	elements.vettureTreeView.visible = true;
	elements.warning.visible = false;
}

/**
 *
 * @properties={typeid:24,uuid:"6744136C-E079-4448-B96F-D82324DCFECD"}
 */
function unmount() {
	if (globals.vc2_vettureCurrentNode > 0) {
		var f = forms.veicolo_conferma_smontaggio;

		//application.showFormInDialog(f);

		var window = application.createWindow("", JSWindow.MODAL_DIALOG);
		f.controller.show(window);

		if (f.unmount) {
			globals.utils_descendTreeNodeChildrenFirst(globals.vc2_vettureCurrentNodeFoundset, "k8_distinte_vetture$figli", unmountNode);
			vettureTreeViewSetRoots();
		}
	} else {
		plugins.dialogs.showWarningDialog("Attenzione", "È necessario selezionare un componente dall'albero per procedere allo smontaggio", "Ok");
	}
}

/**
 *
 * @properties={typeid:24,uuid:"2B17673C-DF50-4154-B63C-81A4B4DBE160"}
 */
function unmountNode(rec) {
	var f = forms.veicolo_conferma_smontaggio;
	rec.stato = (f.isBroken == "Si") ? "Guasto" : rec.stato;
	rec.k8_distinte_vetture_to_k8_montaggi.newRecord();
	rec.k8_distinte_vetture_to_k8_montaggi.progetto = rec.progetto;
	rec.k8_distinte_vetture_to_k8_montaggi.vettura = rec.vettura;
	rec.k8_distinte_vetture_to_k8_montaggi.matricola = rec.matricola;
	rec.k8_distinte_vetture_to_k8_montaggi.numero_disegno = rec.numero_disegno;
	rec.k8_distinte_vetture_to_k8_montaggi.esponente = rec.esponente;
	rec.k8_distinte_vetture_to_k8_montaggi.descrizione = rec.descrizione;
	rec.k8_distinte_vetture_to_k8_montaggi.stato = rec.stato;
	rec.k8_distinte_vetture_to_k8_montaggi.note = rec.note;
	rec.k8_distinte_vetture_to_k8_montaggi.data_montaggio = rec.data_montaggio;
	rec.k8_distinte_vetture_to_k8_montaggi.data_smontaggio = f.unmountDate;
	rec.k8_distinte_vetture_to_k8_montaggi.causale = f.cause;
	rec.vettura = null;
	rec.data_montaggio = null;
}

/**
 *
 * @properties={typeid:24,uuid:"EBC843FA-297F-444E-9B93-C32085EB7B84"}
 * @AllowToRunInFind
 */
function vettureTreeViewSetRoots() {

	if (globals.vc2_currentProgetto && globals.vc2_currentVettura) {
		if (controller.find()) {
			//FS cosa fare?
			//numero_disegno_padre = "^";
			codice_padre="^";
			progetto = globals.vc2_currentProgetto;
			vettura = globals.vc2_currentVettura;
			controller.search();
		}
		elements.vettureTreeView.removeAllRoots();
		elements.vettureTreeView.addRoots(foundset);

		showTree();
	} else {
		hideTree();
	}

}
