/**
 * @properties={typeid:35,uuid:"127725EC-EDF8-4462-9B9A-61D9E4AAB059",variableType:-4}
 */
var dragRecognizer;

/**@type { java.awt.dnd.DragSource}
 * @properties={typeid:35,uuid:"4A2E4C78-2BE5-4C4D-B03C-35C8ECA9F563",variableType:-4}
 */
var dragSource;

/**
 *
 * @properties={typeid:35,uuid:"F932DE27-1C66-4778-BEF0-0CD9244ACAEB",variableType:-4}
 */
var dropTarget;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"07689727-1C28-4B83-B37E-E70AD499171E",variableType:4}
 */
var margin = 20;

/**
 *@param {java.awt.dnd.DropTargetDragEvent} dtde
 * @properties={typeid:24,uuid:"7479C3DD-AD38-4CF7-825D-AD6177024633"}
 */
function dragEnter(dtde) {
	dtde.acceptDrag(dtde.getDropAction());
}

/**
 *@param {java.awt.dnd.DropTargetEvent} dt
 * @properties={typeid:24,uuid:"2D55E099-2E0D-4E06-98EC-1D88D6E10591"}
 */
function dragExit(dt) {
	var dtde = dt;
	//	JStaffa aggiunto output per variabile non utilizzata
//	application.output('nfx nfx_interfaccia_popup_filltree.dragExit(dt) dtde: ' + dtde, LOGGINGLEVEL.INFO);
}

/**
 *@param {java.awt.dnd.DragGestureEvent} str
 * @properties={typeid:24,uuid:"22EAD8BA-342C-4723-9B65-E914D40BA99E"}
 */
function dragGestureRecognized(str) {

	dragSource.startDrag(str, java.awt.dnd.DragSource.DefaultMoveNoDrop, globals.nfx_tree_getVoidTransferableImpl(), dragRecognizer);
}

/**
 *@param {java.awt.dnd.DropTargetDragEvent} dtde
 * @properties={typeid:24,uuid:"22B639FB-EF92-4EB1-8CB0-A15B7EDFF7D1"}
 */
function dragOver(dtde) {
	dtde.acceptDrag(dtde.getDropAction());
	var point = dtde.getLocation();
	//cosa fare?

	//TODO  FS: i metodi usati su elements.target sono di un oggetto JTree!!inoltre outer e realrow dove sono?
//	var row = elements.target.getRowForLocation(point.x, point.y);
//	var bounds = elements.target.getBounds();
//	row = (p.y + outer.y <= margin ? row < 1 ? 0 : row - 1 : row < getRowCount() - 1 ? row + 1 : row);
//	elements.target.scrollRowToVisible(realrow);
	/** @type {javax.swing.JTree} */	
	var tabPanelAsJtree=elements.target;
	var row = tabPanelAsJtree.getRowForLocation(point.x, point.y);
	var bounds = tabPanelAsJtree.getBounds();
	row = (p.y + outer.y <= margin ? row < 1 ? 0 : row - 1 : row < getRowCount() - 1 ? row + 1 : row);
	tabPanelAsJtree.scrollRowToVisible(realrow);

	//	JStaffa aggiunto output per variabile non utilizzata
//	application.output('virtualcar2 veicolo_montaggio_vetture.dragOver(dtde) bounds: ' + bounds, LOGGINGLEVEL.INFO);
	/*
	 public void autoscroll(Point p) {
	 int realrow = getRowForLocation(p.x, p.y);
	 Rectangle outer = getBounds();
	 realrow = (p.y + outer.y <= margin ? realrow < 1 ? 0 : realrow - 1
	 : realrow < getRowCount() - 1 ? realrow + 1 : realrow);
	 scrollRowToVisible(realrow);
	 }

	 public Insets getAutoscrollInsets() {
	 Rectangle outer = getBounds();
	 Rectangle inner = getParent().getBounds();
	 return new Insets(inner.y - outer.y + margin, inner.x - outer.x + margin, outer.height - inner.height - inner.y + outer.y + margin, outer.width - inner.width - inner.x + outer.x + margin);
	 }
	 */
}

/**
 *@param {java.awt.dnd.DropTargetDropEvent} dtde
 * @properties={typeid:24,uuid:"D38BFDC7-62C2-4E2A-9AC6-22AC5A1C32A2"}
 */
function drop(dtde) {

	dtde.dropComplete(true);

}

/**
 *
 * @properties={typeid:24,uuid:"73B3C730-EBDD-406C-BA39-D7303B809323"}
 */
function dropEnd(s) {
	var dtde = s;
	//	JStaffa aggiunto output per variabile non utilizzata
//	application.output('nfx nfx_interfaccia_popup_filltree.dragExit(s) dtde: ' + dtde, LOGGINGLEVEL.INFO);
}

/**
 *
 * @properties={typeid:24,uuid:"E906E1D2-54D2-4A1E-9575-EB5354CB6147"}
 */
function nfx_getTitle() {
	return "Veicolo - Montaggio Vetture";
}

/**
 * @properties={typeid:24,uuid:"CFE25CAB-231E-4B45-803E-382A8B8577B8"}
 */
function nfx_isProgram() {
	return true;
}

/**
 * @properties={typeid:24,uuid:"23CFA8B1-CF93-4FA9-A640-088E893428E9"}
 */
function nfx_defaultPermissions() {
	return "^";
}

/**
 *
 * @properties={typeid:24,uuid:"58D82C77-7E05-4947-8849-06C2294D24FB"}
 */
function nfx_onShow() {
	//FS
	//dep	elements.split_pane.dividerLocation = (application.getWindowWidth() - 260) / 2;
	elements.split_pane.dividerLocation = (application.getWindow().getWidth() - 260) / 2;
}

/**
 *
 * @properties={typeid:24,uuid:"DA633A50-4233-4881-B6EA-B5D9655906BB"}
 */
function onbutton() {
	onLoad();
}

/**
 *
 * @properties={typeid:24,uuid:"9EB49892-D8C3-490C-847B-2AF01DEEC7A0"}
 */
function onLoad() {
	globals.utils_setSplitPane({
		splitpane: elements.split_pane,
		leftComponent: elements.progetti_tabs,
		rightComponent: elements.target,
		orientation: 1
	});
}
