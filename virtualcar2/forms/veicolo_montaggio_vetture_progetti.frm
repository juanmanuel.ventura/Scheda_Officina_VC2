dataSource:"db:/ferrari/k8_distinte_progetto",
items:[
{
anchors:15,
formIndex:-1,
horizontalAlignment:0,
location:"20,180",
mediaOptions:14,
name:"warning",
size:"260,50",
styleClass:"form",
tabSeq:-1,
text:"Selezionare progetto e versione",
transparent:true,
typeid:7,
uuid:"01F0D3EB-1803-425D-AE50-F9946B49DC34"
},
{
anchors:14,
beanClassName:"javax.swing.JProgressBar",
beanXML:"<?xml version=\"1.0\" encoding=\"UTF-8\"?> \
<java version=\"1.6.0_11\" class=\"java.beans.XMLDecoder\"> \
 <object class=\"javax.swing.JProgressBar\"> \
  <void property=\"size\"> \
   <object class=\"java.awt.Dimension\"> \
    <int>370<\/int> \
    <int>20<\/int> \
   <\/object> \
  <\/void> \
  <void property=\"doubleBuffered\"> \
   <boolean>true<\/boolean> \
  <\/void> \
  <void property=\"focusable\"> \
   <boolean>false<\/boolean> \
  <\/void> \
  <void property=\"name\"> \
   <string>bean_764<\/string> \
  <\/void> \
  <void property=\"string\"> \
   <string>Inizio Copia...<\/string> \
  <\/void> \
  <void property=\"stringPainted\"> \
   <boolean>true<\/boolean> \
  <\/void> \
 <\/object> \
<\/java> \
",
location:"0,310",
name:"copyLabel",
size:"200,20",
typeid:12,
uuid:"16DEF940-8792-4969-83E4-CE28514B62C3"
},
{
height:330,
partType:5,
typeid:19,
uuid:"1CCE8C35-FD7F-4C31-A236-38156020F56B"
},
{
labelFor:"filterTag1",
location:"30,80",
mediaOptions:14,
size:"30,20",
styleClass:"form",
tabSeq:-1,
text:"TAG:",
transparent:true,
typeid:7,
uuid:"251ED5D5-0D07-4F33-BD88-FF17189AFD5C"
},
{
labelFor:"vc2_currentProgetto",
location:"10,10",
mediaOptions:14,
size:"80,20",
styleClass:"form",
tabSeq:-1,
text:"Progetto",
transparent:true,
typeid:7,
uuid:"43E71A65-43F5-458A-9516-CF91A3678450"
},
{
anchors:6,
imageMediaID:"40905D44-A323-4D4C-9CCA-CC832C1E545D",
location:"200,310",
mediaOptions:14,
onActionMethodID:"5882A306-6217-44A4-98F3-1E7A43FC0564",
onDoubleClickMethodID:"-1",
onRightClickMethodID:"-1",
size:"100,20",
text:"Monta",
typeid:7,
uuid:"53905C7B-3DAE-4A24-A039-8E3DECC43DA1"
},
{
anchors:3,
dataProviderID:"filterAff",
displayType:10,
location:"170,80",
name:"filterAff1",
onActionMethodID:"7912EFC1-9C9A-44FC-B8FA-B4233F866D10",
onDataChangeMethodID:"-1",
size:"30,20",
text:"Filteraff",
typeid:4,
uuid:"65A49866-A03F-4E62-8F7E-3590B61A5560",
valuelistID:"5132A996-57B2-4EB4-9FE0-876176A35C3C"
},
{
anchors:3,
labelFor:"filterSig1",
location:"210,80",
mediaOptions:14,
size:"30,20",
styleClass:"form",
tabSeq:-1,
text:"Sign.",
transparent:true,
typeid:7,
uuid:"66D58680-B7D0-43FA-98EC-149B9163EB95"
},
{
anchors:11,
dataProviderID:"filterTag",
displayType:10,
location:"70,80",
name:"filterTag1",
onActionMethodID:"7912EFC1-9C9A-44FC-B8FA-B4233F866D10",
onDataChangeMethodID:"-1",
size:"60,20",
text:"Filtertag",
typeid:4,
uuid:"7F9FCD7E-07F9-4CEF-80A0-15A3ACFC981C",
valuelistID:"040D260D-DF02-41B5-8942-3D1591313A99"
},
{
anchors:15,
formIndex:-4,
lineSize:1,
location:"10,60",
roundedRadius:20,
shapeType:2,
size:"280,240",
typeid:21,
uuid:"8A5A2763-C458-421B-AB2F-0182535767CA"
},
{
anchors:3,
dataProviderID:"filterSig",
displayType:10,
location:"240,80",
name:"filterSig1",
onActionMethodID:"7912EFC1-9C9A-44FC-B8FA-B4233F866D10",
onDataChangeMethodID:"-1",
size:"30,20",
text:"Filtersig",
typeid:4,
uuid:"9766D269-185B-4C9B-8D95-2E4F6E2CC935",
valuelistID:"D9564AB7-654D-42F7-880D-864426F29A67"
},
{
anchors:11,
dataProviderID:"globals.vc2_currentVersione",
displayType:2,
editable:false,
location:"100,30",
name:"vc2_currentVersione",
onDataChangeMethodID:"247DA2A3-B34A-4455-AF66-8BA162D0B88B",
size:"190,20",
text:"Vc2 Currentversione",
transparent:true,
typeid:4,
uuid:"AECB41F6-3DFF-4322-A948-D42C2B7156B7",
valuelistID:"3CD28F78-CDC1-4709-AC32-02348CA4B79A"
},
{
anchors:11,
borderType:"TitledBorder,Filtri,4,0,DialogInput.plain,1,12,#333333",
foreground:"#808040",
formIndex:-3,
location:"20,60",
size:"260,50",
transparent:true,
typeid:21,
uuid:"CCAD12EE-1C04-4942-AF7D-464E8C98908B"
},
{
anchors:11,
dataProviderID:"globals.vc2_currentProgetto",
displayType:2,
editable:false,
location:"100,10",
name:"vc2_currentProgetto",
onDataChangeMethodID:"42B7E81F-6E56-45E3-A826-A8C29D5D8EDB",
size:"190,20",
text:"Vc2 Currentprogetto",
transparent:true,
typeid:4,
uuid:"D8215F21-4F90-47F6-B54E-9A1FA1E26FDB",
valuelistID:"8DBD3071-35BE-4D55-B40F-F9B6FB9F8DD6"
},
{
anchors:3,
labelFor:"filterAff1",
location:"140,80",
mediaOptions:14,
size:"30,20",
styleClass:"form",
tabSeq:-1,
text:"Aff.",
transparent:true,
typeid:7,
uuid:"E73DB900-C5B0-40C2-95F3-5B0CD0337132"
},
{
anchors:15,
beanClassName:"com.servoy.extensions.beans.dbtreeview.DBTreeView",
location:"20,110",
name:"progettiTreeView",
size:"260,180",
typeid:12,
uuid:"F17DAE83-3E0F-4469-A138-71172F43260F"
},
{
labelFor:"vc2_currentVersione",
location:"10,30",
mediaOptions:14,
size:"80,20",
styleClass:"form",
tabSeq:-1,
text:"Versione",
transparent:true,
typeid:7,
uuid:"F981AE65-114F-4056-B252-4BFE564EBEF2"
}
],
name:"veicolo_montaggio_vetture_progetti",
namedFoundSet:"separate",
navigatorID:"-1",
onHideMethodID:"332AC48B-FE17-4EE3-BFA9-AF7F43A5A048",
onLoadMethodID:"DD6B62AC-8308-481B-AE64-B1B5A60C11BC",
onRecordSelectionMethodID:"-1",
onSearchCmdMethodID:"-1",
onShowMethodID:"0BB3EED6-D6F9-4B2A-AC4C-B9F7B94536E0",
paperPrintScale:100,
showInMenu:true,
size:"300,330",
styleName:"keeneight",
typeid:3,
uuid:"85562550-0946-4FE6-AAF1-E198B01DBE02"