/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C431CF3B-6A26-4DEA-83BC-DAF7DC354210"}
 */
var filterAff = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"56322853-BB3E-4E29-89B4-1BE9C063767C"}
 */
var filterSig = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"E9A79A61-DC36-4B65-B222-AFB7A54164B4"}
 */
var filterTag = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"DC13E59C-5BA9-4B78-A796-421B4DCC2372",variableType:4}
 */
var firstLevel = 0;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"1BCDDB33-095E-41C7-8B28-369C2424979C",variableType:4}
 */
var nodesCopied;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"691DF240-ECE4-4118-A0BD-249F4AD4BA56",variableType:4}
 */
var nodesToCopy;

/**
 *
 * @properties={typeid:24,uuid:"2C747362-A474-4C29-8B74-E5D0D1F7D3C9"}
 */
function copyNodeChildren() {
	globals.utils_descendTreeNode(globals.vc2_progettiCurrentNodeFoundset["k8_distinte_progetto$figli"], "k8_distinte_progetto$figli", copyNodeToVetture);
}

/**
 *
 * @properties={typeid:24,uuid:"5882A306-6217-44A4-98F3-1E7A43FC0564"}
 * @AllowToRunInFind
 */
function copyNodes() {
	if (globals.vc2_currentVettura) {
		showLabel();
		forms.veicolo_montaggio_vetture.elements.progetti_tabs.enabled = forms.veicolo_montaggio_vetture.elements.target.enabled = false;
		var path = elements.progettiTreeView.selectionPath;
		var query = "select count(*) from k8_distinte_progetto where path_id LIKE ?";
		//FS
		//dep		var ds = databaseManager.getDataSetByQuery(controller.getServerName(),query,[path_id + '%'],1);
		var ds = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), query, [path_id + '%'], 1);

		nodesToCopy = ds.getValue(1, 1) + path.length - 1;
		nodesCopied = 0;
		for (var i = 0; i < path.length; i++) {
			if (controller.find()) {
				k8_distinte_progetto_id = path[i];
				controller.search();
			}
			copyNodeToVetture(foundset.getRecord(1));
		}
		copyNodeChildren();

		forms.veicolo_distinte_vetture_albero.vettureTreeViewSetRoots();

		//application.showFormInDialog(forms.veicolo_conferma_date_vetture);

		var formq = forms.veicolo_conferma_date_vetture;
		var window = application.createWindow("", JSWindow.MODAL_DIALOG);
		formq.controller.show(window);

		databaseManager.saveData();
		hideLabel();
		forms.veicolo_montaggio_vetture.elements.progetti_tabs.enabled = forms.veicolo_montaggio_vetture.elements.target.enabled = true;
	} else {
		plugins.dialogs.showErrorDialog("Errore", "Nessuna vettura selezionata per il montaggio", "Ok");
	}
}

/**
 *
 * @properties={typeid:24,uuid:"F2C50A7D-9993-43E9-B3FB-F7762A67196A"}
 */
function copyNodeToVetture(rec) {

	var result = forms.veicolo_distinte_vetture_albero.createDistintaVettura({
		numero_disegno: rec.numero_disegno,
		numero_disegno_padre: rec.codice_padre,
		descrizione: rec.descrizione,
		significativita: rec.significativita,
		affidabilita: rec.affidabilita,
		esponente: rec.esponente,
		id: rec.k8_distinte_progetto_id
	});

	if (result != -1) {
		//Copia dei coefficienti
		for (var i = 1; i <= rec.k8_distinte_progetto_to_k8_distinte_progetto_coeff.getSize(); i++) {
			rec.k8_distinte_progetto_to_k8_distinte_progetto_coeff.setSelectedIndex(i);
			rec.k8_distinte_progetto_to_k8_distinte_vetture_coeff.newRecord()
			rec.k8_distinte_progetto_to_k8_distinte_vetture_coeff.fk_distinte_vetture = rec.k8_distinte_progetto_to_k8_distinte_vetture$unoauno.k8_distinte_vetture_id;
			rec.k8_distinte_progetto_to_k8_distinte_vetture_coeff.coefficiente = rec.k8_distinte_progetto_to_k8_distinte_progetto_coeff.coefficiente;
			rec.k8_distinte_progetto_to_k8_distinte_vetture_coeff.note = rec.k8_distinte_progetto_to_k8_distinte_progetto_coeff.note;
		}
	}

	nodesCopied++;
	updateLabel();
	application.updateUI();
}

/**
 *
 * @properties={typeid:24,uuid:"8DF13837-F4B6-40C4-9D5D-8DF0E989E055"}
 */
function dragStart() {
	//application.output("DRAG START");
	return true;
}

/**
 *
 * @properties={typeid:24,uuid:"F3F5DB94-E786-4165-8A32-BE35B0149B8B"}
 */
function hideLabel() {
	elements.copyLabel.visible = false;
}

/**
 *
 * @properties={typeid:24,uuid:"FE585CCE-A452-4775-B988-2214298A4966"}
 */
function hideTree() {
	elements.progettiTreeView.visible = false;
	elements.warning.visible = true;
}

/**
 *
 * @properties={typeid:24,uuid:"B329F2EB-86C5-4FF9-88E3-AFDFB6F14E91"}
 * @AllowToRunInFind
 */
function onClick(value) {

	if (controller.find()) {
		k8_distinte_progetto_id = value;
		controller.search();
	}
	
	globals.vc2_progettiCurrentNode = k8_distinte_progetto_id;
	globals.vc2_progettiCurrentNodeFoundset = foundset;
	//FS da sistemare la globals
	globals.vc2_progettiCurrentNode_path = path_id + "%";
}

/**
 *
 * @properties={typeid:24,uuid:"42B7E81F-6E56-45E3-A826-A8C29D5D8EDB"}
 */
function onCurrentProgettoChange() {
	globals.vc2_currentVersione = null;
	globals.vc2_currentVettura = null;
	application.updateUI();
	progettiTreeViewSetRoots();
	forms.veicolo_distinte_vetture_albero.vettureTreeViewSetRoots();
}

/**
 *
 * @properties={typeid:24,uuid:"247DA2A3-B34A-4455-AF66-8BA162D0B88B"}
 */
function onCurrentVersioneChange() {
	progettiTreeViewSetRoots();
}

/**
 *
 * @properties={typeid:24,uuid:"332AC48B-FE17-4EE3-BFA9-AF7F43A5A048"}
 */
function onHide() {
	//FS
	//dep	databaseManager.removeTableFilterParam(controller.getServerName(), "FILTROMONTAGGIO");
	databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), "FILTROMONTAGGIO");

}

/**
 *
 * @properties={typeid:24,uuid:"DD6B62AC-8308-481B-AE64-B1B5A60C11BC"}
 */
function onLoad() {
	//FS
	//dep	var progettiTreeViewBinding = elements.progettiTreeView.createBinding(controller.getServerName(),controller.getTableName());
	var progettiTreeViewBinding = elements.progettiTreeView.createBinding(databaseManager.getDataSourceServerName(controller.getDataSource()),databaseManager.getDataSourceTableName(controller.getDataSource()));
	progettiTreeViewBinding.setTextDataprovider("descrizione");
	progettiTreeViewBinding.setNRelationName("k8_distinte_progetto$figli");
	progettiTreeViewBinding.setMethodToCallOnClick(onClick, "k8_distinte_progetto_id");
	progettiTreeViewBinding.setImageURLDataprovider("treeIconAffi");
	//elements.progettiTreeView.setOnDragStartMethod(dragStart,[]);

}

/**
 *
 * @properties={typeid:24,uuid:"0BB3EED6-D6F9-4B2A-AC4C-B9F7B94536E0"}
 */
function onShow() {
	progettiTreeViewSetRoots();
	hideLabel();
}

/**
 *
 * @properties={typeid:24,uuid:"9BE8B566-4DA0-4AC9-AB5E-F5AD581DCCA0"}
 * @AllowToRunInFind
 */
function progettiTreeViewSetRoots() {
	filterTag = null;
	filterAff = null;
	filterSig = null;
	//FS
//dep	databaseManager.removeTableFilterParam(controller.getServerName(), "FILTROMONTAGGIO");
	databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), "FILTROMONTAGGIO");
	controller.loadAllRecords();

	if (globals.vc2_currentProgetto && globals.vc2_currentVersione) {
		if (controller.find()) {
			livello = 0;
			progetto = globals.vc2_currentProgetto;
			//FS aggiunto il toString()
			codice_vettura = java.lang.Double.parseDouble(globals.vc2_currentVersione.toString());
			controller.search();
		}
		elements.progettiTreeView.removeAllRoots();
		elements.progettiTreeView.addRoots(foundset);

		showTree();
	} else {
		globals.vc2_progettiCurrentNode = -1;
		globals.vc2_progettiCurrentNodeFoundset = null;
		globals.vc2_progettiCurrentNodePath = null;

		hideTree();
	}
}

/**
 *
 * @properties={typeid:24,uuid:"D36280C4-9E7E-45BF-90C2-FABD10A65085"}
 */
function showLabel() {
	elements.copyLabel.visible = true;
}

/**
 *
 * @properties={typeid:24,uuid:"6CE242D6-97A6-4F1F-8154-24928D0A3888"}
 */
function showTree() {
	elements.progettiTreeView.visible = true;
	elements.warning.visible = false;
}

/**
 *
 * @properties={typeid:24,uuid:"7912EFC1-9C9A-44FC-B8FA-B4233F866D10"}
 * @AllowToRunInFind
 */
function upDateFilter() {
	if (filterTag == "") {
		filterTag = null;
		application.updateUI();
	}
	if (filterAff == "") {
		filterAff = null;
		application.updateUI();
	}
	if (filterSig == "") {
		filterSig = null;
		application.updateUI();
	}
	if (filterTag) {
//FS
//dep		databaseManager.removeTableFilterParam(controller.getServerName(), "FILTROMONTAGGIO");
		databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()),"FILTROMONTAGGIO");

		var idsToKeep = new Array();
		//aggiunto toString
		var currentVersione = java.lang.Double.parseDouble(globals.vc2_currentVersione.toString());
		var codiceVersione = currentVersione.toLocaleString();
		var queryPath = "select path_id from k8_distinte_progetto inner join k8_tag on k8_distinte_progetto_id = record_fk and 'k8_distinte_progetto' = table_name_fk where progetto = ? and codice_vettura = ?";
		var args = [globals.vc2_currentProgetto, codiceVersione];
		var words = filterTag.split(" ");
		for (var i = 0; i < words.length; i++) {
			queryPath += " and upper(tag) like ?";
			var tagLike = "%" + words[i].toUpperCase() + "%";
			args.push(tagLike);
		}
		if (filterAff) {
			queryPath += " and affidabilita = ?";
			args.push(filterAff);
			if (filterSig) {
				queryPath += " and significativita = ?";
				args.push(filterAff);
			} else {
				queryPath += " and significativita is null";
			}
		}
//FS
//dep		var dataSet = databaseManager.getDataSetByQuery(controller.getServerName(), queryPath, args, -1);
		var dataSet = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), queryPath, args, -1);
		var path = dataSet.getColumnAsArray(1);
		if (path.length != 0) {
			for (var i2 = 0; i2 < path.length; i2++) {
				if (path[i2]) {
					if (idsToKeep.length > 200) {
						break;
						//serve per limitare i tempi d'attesa
					}
					var idsTmp = path[i2].split("/");
					for (var j = 1; j < idsTmp.length; j++) {
						if (idsToKeep.length > 200) {
							break;
							//serve per limitare i tempi d'attesa
						}
						if (idsToKeep.indexOf(idsTmp[j]) == -1) {
							idsToKeep.push(idsTmp[j]);
						}
						if (j == idsTmp.length - 1) {
							var like = "%/" + idsTmp[j] + "/%";
							var queryLike = "select k8_distinte_progetto_id from k8_distinte_progetto_ft where progetto = ? and codice_vettura = ? and path_id like ?";
							args = [globals.vc2_currentProgetto, codiceVersione, like];
//FS
//dep						dataSet = databaseManager.getDataSetByQuery(controller.getServerName(), queryLike, args, -1);
							dataSet = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(controller.getDataSource()), queryLike, args, -1);
							for (var k = 1; k <= dataSet.getMaxRowIndex(); k++) {
								if (idsToKeep.indexOf(dataSet.getValue(k, 1)) == -1) {
									idsToKeep.push(dataSet.getValue(k, 1));
								}
								if (idsToKeep.length > 200) {
									break;
									//serve per limitare i tempi d'attesa
								}
							}
						}
					}
				}
			}
			//application.output(idsToKeep.length);
			if (idsToKeep.length <= 200) {
				// è il massino consentito dalla clausula "IN"
//FS
//dep				databaseManager.addTableFilterParam(controller.getServerName(), controller.getTableName(), "k8_distinte_progetto_id", "IN", idsToKeep, "FILTROMONTAGGIO");
				databaseManager.addTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), databaseManager.getDataSourceTableName(controller.getDataSource()) , "k8_distinte_progetto_id", "IN", idsToKeep, "FILTROMONTAGGIO");
				if (controller.find()) {
					livello = 0;
					progetto = globals.vc2_currentProgetto;
					//aggiunto toString
					codice_vettura = java.lang.Double.parseDouble(globals.vc2_currentVersione.toString());
					controller.search();
				}
				elements.progettiTreeView.removeAllRoots();
				elements.progettiTreeView.addRoots(foundset);
			} else {
				plugins.dialogs.showWarningDialog("Attenzione", "Selezionare criteri di ricerca pi\xf9 restrittivi", "Ok");
			}
		} else {
			plugins.dialogs.showWarningDialog("Attenzione", "Non sono presenti componenti che corrispondono ai criteri di ricerca", "Ok");
		}
	} else {
//FS
//dep		databaseManager.removeTableFilterParam(controller.getServerName(), "FILTROMONTAGGIO");
		databaseManager.removeTableFilterParam(databaseManager.getDataSourceServerName(controller.getDataSource()), "FILTROMONTAGGIO");
		controller.loadAllRecords();
		if (controller.find()) {
			livello = 0;
			progetto = globals.vc2_currentProgetto;
			//aggiunto toString
			codice_vettura = java.lang.Double.parseDouble(globals.vc2_currentVersione.toString());
			controller.search();
		}
		elements.progettiTreeView.removeAllRoots();
		elements.progettiTreeView.addRoots(foundset);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"7DD5B4F2-96F1-4776-BDA7-E7B4AE3033FA"}
 */
function updateLabel() {
	if (nodesToCopy < 1) return;
	var n = Math.min(nodesCopied, nodesToCopy);
	elements.copyLabel.string = "Controllo la copia di " + n + " di " + nodesToCopy + " elementi";
	elements.copyLabel.value = n / nodesToCopy * 100;
}
