/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B83A0D95-8828-45F1-BB80-F24E58D8958B"}
 */
var k8_distinte_progetto = "k8_distinte_progetto";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"01929FE8-A1E8-42E2-AF3D-EA72F4565C31"}
 */
var k8_distinte_vetture = "k8_distinte_vetture";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"10398E31-CF3C-4D8C-9376-A3E4BDA3FC00"}
 */
var k8_schede_anomalie = "k8_schede_anomalie";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"05644332-03ED-4307-98AA-485A1F1A7AE0"}
 */
var k8_schede_collaudo = "k8_schede_collaudo";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"60527AE7-388A-4B1D-9D99-8F864368FCE0"}
 */
var k8_schede_officina = "k8_schede_officina";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"429ACCCA-36CF-4958-96B7-8A37E6C4ABF1"}
 */
var k8_segnalazioni_anomalie = "k8_segnalazioni_anomalie";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"2E49172D-A36F-4071-828E-817D077A0E64",variableType:4}
 */
var lastSKAfromSA = 0;

/**
 * @properties={typeid:35,uuid:"D19EE112-1216-4F11-B57D-1991DB120D68",variableType:-4}
 */
var unacceptedInfo = { form: "", times: 0 };

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"5DC752F1-62F7-4E9B-BD97-A9556128B9D4"}
 */
var vc2_azionamentiManovreDescrizione = "Azionamenti_e_Manovre";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"EA4F5BC1-8C0A-471F-8E21-E05569178322"}
 */
var vc2_componentiCurrentGroup = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"9686FB21-7A41-4C08-BED8-F0BBED75A9AE"}
 */
var vc2_constraintSottogruppo = 'Sottogruppo_Funzionale';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"9386AA8B-D909-4C5A-AED8-900E487F1C1F"}
 */
var vc2_currentAlternativa = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"BF339A7E-7A72-4B03-A848-1C70E6C19F84",variableType:8}
 */
var vc2_currentDisegno = -1;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"B2D8C161-4644-4FB4-9BE0-F388DD96D500"}
 */
var vc2_currentEnte = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"90C7D536-6CB6-400B-83F7-8B91CF0A4C17"}
 */
var vc2_currentGruppoFunzionale = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"7B47BF24-24E3-4903-9762-09FBEFB8F882"}
 */
var vc2_currentLeader = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"37F85682-CA91-4882-87AC-D835E18F37B1"}
 */
var vc2_currentProgetto = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"FC9C702D-A092-43B8-B7F3-4852FC7DACD7"}
 */
var vc2_currentSottogruppoFunzionale = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"094E9578-C304-40B1-8E33-B7C89592B513"}
 */
var vc2_currentTipoAnomalia = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"985DFD24-AAD1-421B-AE1A-10116925EF75"}
 */
var vc2_currentTipoScheda = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"C8EFBF8C-87E7-4EBB-9638-F032D376DCCC",variableType:8}
 */
var vc2_currentVersione = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"DFBC18E5-C9FF-4342-ACA4-2E2B7E20CE06"}
 */
var vc2_currentVettura = null;

/**
 * @type {Date}
 *
 * @properties={typeid:35,uuid:"AB2BD8D3-86BA-4DCB-9922-C32B3C4C15DE",variableType:93}
 */
var vc2_dateNull = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"FF870C84-17F1-47B0-9928-09E4AE23E6D1"}
 */
var vc2_docPath = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"AA8B9FEE-5388-426B-81BD-D43569B4F03E"}
 */
var vc2_globale_constraint_ente = 'Utenti_Servoy';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1DEAD236-B089-47B4-BD1A-416C9F85BA38"}
 */
var vc2_lastErrorLog = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"039305A7-34F9-4477-9206-F799B4E66C98"}
 */
var vc2_lastHumanMessage = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"88AC77E8-7E4F-41A4-AA37-6354E8A69AAE"}
 */
var vc2_mediaKmScCollaudo = 'Non calcolato';

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F71D0F93-A3EB-44A9-BE7B-A30998ED86D5"}
 */
var vc2_null = null;

/**@type {Array<String>}
 * @properties={typeid:35,uuid:"E16B7B44-F042-42E2-8389-88164B59D522",variableType:-4}
 */
var vc2_powerUsers = ["rfedeli", "fmarra", "mbuso", "sboaretti", "astorti", "cvioli", "sauciello", "dfrau"];

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"2697B54E-2B71-45B8-B220-463A258A22D2",variableType:4}
 */
var vc2_progettiCurrentNode = -1;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"8090C572-DD62-4861-B2A9-67ACF0E5B785",variableType:4}
 */
var vc2_progettiCurrentNodeEsponente = -1;

/**@type {JSFoundSet}
 * @properties={typeid:35,uuid:"25D0ACF4-022B-4D8D-93D7-F2650D48D0C2",variableType:-4}
 */
var vc2_progettiCurrentNodeFoundset = null;

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"23DE4194-956F-47E1-8BF7-01E643864E09",variableType:4}
 */
var vc2_progettiCurrentNodeNumeroDisegno = -1;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"3A6C9103-68E9-48D6-AAE6-525BAC0B3FF6"}
 */
var vc2_progettiCurrentNodePath = null;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"34CCEF3C-DBCA-4CC6-B59F-803E175FDB6B"}
 */
var vc2_schedeCollaudoCollaudatore = "schede_collaudo_collaudatore";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"9E833095-9D20-45DB-BE9E-F4BDB9130694"}
 */
var vc2_schedeCollaudoRichiedente = "schede_collaudo_richiedente";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"F957C461-7212-4681-BE82-8281FEC2BE62"}
 */
var vc2_serverName = "f400anomalie.unix.ferlan.it/attorney/";

/**
 * @properties={typeid:35,uuid:"63B21BB7-5808-458F-94D4-622EBA65D52A",variableType:-4}
 */
var vc2_serverPROD = ["http://enzo.unix.ferlan.it:9191/", "http://enzopdm.unix.ferlan.it:9191/", "http://f599.unix.ferlan.it:9191/", "http://f599pdm.unix.ferlan.it:9191/"];

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"7A6AD283-AD9C-4C7A-A777-CD6B187615E8"}
 */
var vc2_serviceLocationCambioEnte = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"0C8156FA-7DF6-4E95-87FF-BB2A2DEFD563"}
 */
var vc2_serviceLocationSe = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"3A53EB39-1ECE-49E7-AC57-6D8AD0EB2CE0"}
 */
var vc2_serviceLocationSk = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"542F8601-D8EC-4B05-A088-3663BF748959"}
 */
var vc2_testMessage = "";

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"C8D57FE1-BB6E-4C86-930A-A6A682EED3C7"}
 */
var vc2_variabiliAmbientaliUdm = "Variabili_Ambientali";

/**
 * @type {Number}
 *
 * @properties={typeid:35,uuid:"CBCC7EF7-7866-4D67-984C-50FB79C4A453",variableType:4}
 */
var vc2_vettureCurrentNode = -1;

/**@type {JSFoundSet}
 * @properties={typeid:35,uuid:"8A563CB7-3E6B-4B95-AFA1-9900ED5BD86F",variableType:-4}
 */
var vc2_vettureCurrentNodeFoundset;

/**
 * @type {String}
 *
 * @properties={typeid:35,uuid:"1236C96C-892B-42EA-BAB6-67E0D5C2E48B"}
 */
var vc2_webServiceMode = 'E';

/**
 * JStaffa mancava la dichiarazione di una global che veniva solo settata senza farle fare nulla..
 * @properties={typeid:35,uuid:"A307AFE7-D740-4A05-9BD2-726C840D02D9",variableType:-4}
 */
var vc2_progettiCurrentNode_path = null;

/**
 * @properties={typeid:24,uuid:"BDD7EF33-ABB7-41AD-AA36-175E1A1AE7A6"}
 */
function vc2_setSystemPaths() {
	if (vc2_serverPROD.indexOf(application.getServerURL()) != -1) {
		//--> PRODUZIONE <--

		//Servizi WEB
		vc2_serviceLocationSe = "http://anomalie.unix.ferlan.it/attorney/Segnalazione_Anomalie.jsp";
		vc2_serviceLocationSk = "http://anomalie.unix.ferlan.it/attorney/Scheda_Anomalie.jsp";
		vc2_serviceLocationCambioEnte = "http://anomalie.unix.ferlan.it/attorney/Associazione_Ente.jsp";
		//Indirizzo documentale
		vc2_docPath = "//gt.ferlan.it/dt$/VirtualCar/VC2_NASP/";
	} else {
		//-->    TEST    <--

		//Servizi WEB
		vc2_serviceLocationSe = "http://f400anomalie.unix.ferlan.it/attorney/Segnalazione_Anomalie.jsp";
		vc2_serviceLocationSk = "http://f400anomalie.unix.ferlan.it/attorney/Scheda_Anomalie.jsp";
		vc2_serviceLocationCambioEnte = "http://f400anomalie.unix.ferlan.it/attorney/Associazione_Ente.jsp";

		//Indirizzo documentale
		vc2_docPath = "//gt.ferlan.it/dt$/VirtualCar/VC2_NAS/";

		//Per test documentale in locale
		//vc2_docPath = "C:/Documents and Settings/omar/Desktop/VC2_NAS_TMP/";

		vc2_testMessage = "{\\rtf1\\ansi{\\fonttbl\\f0\\fnil Monospaced;\\f1\\fnil Verdana;}{\\colortbl\\red0\\green0\\blue0;\\red255\\green0\\blue0;}\\li0\\ri0\\fi0\\qc\\f1\\fs96\\i0\\b\\ul0\\cf1 ATTENZIONE\\par\\qc AMBIENTE DI TEST!\\fs20\\cf0\\par\\qc\\fs56\\cf1 Per poter utilizzare l'applicativo\\par\\qc\\'e8 necessario riscaricarlo da:\\fs20\\cf0\\par\\qc\\fs96\\ul http://virtualcar2.unix.ferlan.it\\fs72\\par\\li0\\ri0\\fi0\\ul0  \\fs22\\b0\\ul0}";
	}
	//plugins.dialogs.showInfoDialog("TEST","SERVER: " + application.getServerURL() + "\nSA: " + vc2_serviceLocationSe + "\nSKA: " + vc2_serviceLocationSk + "\n DOCS: " + vc2_docPath,"Ok");
}

/**
 *
 * @properties={typeid:24,uuid:"F832E544-95DF-424D-B5EA-8DA891D1CACE"}
 */
function utils_dateFormatAS400(date) {
	if (date) {
		return date.getFullYear().toString() + globals.utils_zeroFill(date.getMonth() + 1) + globals.utils_zeroFill(date.getDate());
	} else {
		return "";
	}
}

/**
 *
 * @properties={typeid:24,uuid:"EA5C6923-E9CF-437D-8E16-3B43CAAD29E7"}
 */
function utils_dateTextFormatAS400(form) {
	/** @type {JSFoundSet} */
	var vFoundset = forms[form].foundset.duplicateFoundSet();
	vFoundset.sort(globals.utils_getIdDataProvider(form) + " asc");

	var retVal = { text: "", date: "" };

	for (var i = 1; i <= vFoundset.getSize(); i++) {
		vFoundset.setSelectedIndex(i);
		/*if (vFoundset.sorgente == 'AS400'){
		 break;
		 }*/
		var compStr = "";

		var str = utils_textFormatAS400(vFoundset.testo);
		for (var j = 0; j < str.length; j += 50) {
			var tmpStr = str.substring(j, j + 50);
			// Filla di spazi il tutto.
			while (tmpStr.length < 50) {
				tmpStr += " ";
			}
			compStr += tmpStr;
			retVal.date += utils_dateFormatAS400(vFoundset.timestamp_creazione || new Date());
		}
		retVal.text += compStr;
	}
	return retVal;
}

/**
 *
 * @properties={typeid:24,uuid:"EA70D2B8-5EC4-4DCB-99CE-5F057F6CBAA2"}
 */
function utils_descendTreeNode(f, relationName, mapFunction) {

	if (!f) return;

	for (var i = 1; i <= f.getSize(); i++) {
		f.setSelectedIndex(i);
		mapFunction(f.getRecord(i), f, i);
		if (utils.hasRecords(f[relationName])) {
			globals.utils_descendTreeNode(f[relationName], relationName, mapFunction);
		}
	}
}

/**
 *
 * @properties={typeid:24,uuid:"DE36DEE8-5C7B-4729-A1E8-E40A4A1B49FD"}
 */
function utils_descendTreeNodeChildrenFirst(f, relationName, mapFunction) {
	if (!f) return;
	for (var i = 1; i <= f.getSize(); i++) {
		f.setSelectedIndex(i);
		if (utils.hasRecords(f[relationName])) {
			globals.utils_descendTreeNode(f[relationName], relationName, mapFunction);
		}
		mapFunction(f.getRecord(i), f, i);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"3E43DF03-C08A-45CF-8524-84DEE6BFC0A3"}
 */
function utils_smartTruncate(s, l) {
	var str = s || "";
	var length = l || 50;
	return str.slice(0, length) + " ...";
}

/**
 *
 * @properties={typeid:24,uuid:"01E4E9EF-4618-4301-A902-FB914AF0BE02"}
 */
function utils_stateFormatAS400(stato) {

	if (stato == "APERTA") {
		return null;
	} else if (stato == "CHIUSA") {
		return 'C';
	} else if (stato == "ANNULLATA") {
		return 'A';
	} else {
		return stato;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"D1C6121D-7CEB-46C0-B632-A2AF61A324AA"}
 */
function utils_textFormatAS400(d) {
	var formatted = d || "";
	//formatted = formatted.replace(/[^a-z0-9,\.\s\(\)_\-\?]/gi, " ");
	formatted = formatted.replace(/€/g, "¤");
	formatted = formatted.replace(/\n+/g, " ");
	formatted = formatted.replace(/\r+/g, " ");
	formatted = formatted.replace(/\t+/g, " ");
	formatted = formatted.replace(/\s+/g, " ");
	return formatted;
}

/**
 *
 * @properties={typeid:24,uuid:"EE883379-E805-4C23-9C18-4D7EC0B16D81"}
 */
function utils_zeroFill(s) {
	var num = Number(s);
	if (num < 10 && num > 0) {
		return "0" + num;
	}
	return num.toString();
}

/**
 *
 * @properties={typeid:24,uuid:"4FCED155-1E67-4EAE-8369-9E02ADE60F17"}
 */
function vc2_as400ToHuman(n) {
	var names = {
		ESDICS: "Tipo Segnalazione/Anomalia",
		ESLEAD: "Nome Segnalatore",
		ESLEA2: "Nome Leader",
		ESFUNZ: "Gruppo Funzionale",
		ESCENT: "Ente",
		ESCOMP: "Componente Vettura",
		ESCDIF: "Codice Difetto",
		SASUGR: "Sottogruppo Funzionale",
		ESNREG: "Numero Segnalazione/Anomalia (oggetto non esistente su AS400?)",
		ESMODE: "Codice Modello",
		ESNOMI: "Nome utente",
		ESNOMV: "Nome utente modifica",
		ESSUGR: "Sottogruppo Funzionale",
		ESRESP: "Nome Responsabile",
		ESUSER: "Nome utente - AS400 (Utente inesistente su AS400?)",
		ESDICP: "Demerito"
	};

	if (names[n]) {
		return names[n];
	} else {
		return n;
	}
}

/**
 *@param {String} user
 *@param {String} userAS400
 * @properties={typeid:24,uuid:"7DDEE68F-0A9D-4FA7-AFF2-9515CA526854"}
 */
function vc2_getAs400UserMail(user, userAS400) {

	var ds = databaseManager.getDataSetByQuery('ferrari', "select nome,cognome from K8_GESTIONE_UTENZE where USER_AS400 = ? order by K8_GESTIONE_UTENZE_ID asc", [user], -1);
	var first_name = ds.getValue(1, 1);
	var last_name = ds.getValue(1, 2);
	if (first_name && last_name) {
		return first_name + "." + last_name + '@ferrari.com';
	} else {
		return null;
	}
}

/**
 * @properties={typeid:24,uuid:"D3A983C0-C6A7-4371-8747-ACC60BE811AE"}
 * @AllowToRunInFind
 */
function vc2_getUserInfo(d) {
	var u = d || globals.nfx_user;
	var f = forms.gestione_utenze.foundset.duplicateFoundSet();
	//	JStaffa
	//	if (f.find()) {
	//		f.user_servoy = u;
	//		f.search();
	//		return f.getRecord(1);
	//	} else {
	//		return null;
	//	}
	if (f.find()) {
		f.user_servoy = u;
		if (f.search() > 0) {
			return f.getRecord(1);
		} else {
			return null;
		}
	} else {
		return null;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"C1798536-DFD6-4C9F-8B36-063DDD3D6A67"}
 */
function vc2_icon_criticita(e) {
	if (e) {
		return plugins.http.getMediaData("media:///" + e + ".png");
	} else {
		return null;
	}
}

/**
 *@return {String|Number}
 * @properties={typeid:24,uuid:"80F3821F-481F-4C3E-9585-B04E3B3C9495"}
 */
function vc2_parseResponse(vFoundset, responseString, s, d) {
	var kind = s || "SANREG_OUT";
	var tagToNormalize = d || new Array();
	globals.vc2_lastErrorLog = "";
	globals.vc2_lastHumanMessage = "";

	try {
		/** @type {String} */

		var responseManipulated = (!tagToNormalize.length) ? responseString : vc2_normalizeIrregularXML(responseString, tagToNormalize);
		var feed = new XML(responseManipulated.replace(/^<\?xml\s+version\s*=\s*(["'])[^\1]+\1[^?]*\?>/, ""));
		globals.vc2_lastErrorLog = feed.toXMLString().replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">");
		vFoundset["errori_riportati"] = globals.vc2_lastErrorLog;
	} catch (e) {
		// Errore nel feed XML : WSXMLERR1 => Errore nel parsing SAX ritiene non Well Formed il feed di risposta.
		plugins.dialogs.showErrorDialog("Dati non trasmessi", "I dati non sono stati trasmessi a causa di un errore tecnico (WSXMLERR1)", "Ok");
		globals.vc2_lastErrorLog = "I dati non sono stati trasmessi a causa di un errore tecnico (WSXMLERR1)";
		return -1;
	}
	application.output("Text XML : "+feed.MessageBody.OUTPUT.DATA.ESSANO);
	if (feed.MessageBody.OUTPUT.DATA.ESSANO != null && feed.MessageBody.OUTPUT.DATA.ESSANO.toString() == "OK") {
		return feed.MessageBody.OUTPUT.DATA[kind].toString();
	} else if (feed.MessageBody.OUTPUT.DATA.ESSANO != null && feed.MessageBody.OUTPUT.DATA.ESSANO.toString() == "KO") {
		plugins.dialogs.showInfoDialog("Dati non trasmessi", "AS400 ha segnalato i seguenti campi come mancanti o errati:\n\n" + vc2_showWebServicesErrorDialog(feed.MessageBody.OUTPUT.DATA), "Ok");
		globals.vc2_lastHumanMessage = "AS400 ha segnalato i seguenti campi come mancanti o errati:\n\n" + vc2_showWebServicesErrorDialog(feed.MessageBody.OUTPUT.DATA);
		return -1;
	} else if (feed.MessageErrors.Exception.length() > 0) {
		// Errore nella risposta del servizio web: WSIP1 => il servizio AS400 non e' disponibile. L'ip non e' attivato.
		plugins.dialogs.showErrorDialog("Dati non trasmessi", "I dati non sono stati trasmessi perche' l'inserimento ha creato un eccezione nel servizio AS400", "Ok");
		return -1;
	} else {
		// Errore nella risposta del servizio web: WSIP1 => il servizio AS400 non e' disponibile. L'ip non e' attivato.
		plugins.dialogs.showErrorDialog("Dati non trasmessi", "I dati non sono stati trasmessi perche il servizio AS400 non è raggiungibile (WSIP1-XML)", "Ok");
		return -1;
	}
}

/**
 * @properties={typeid:24,uuid:"4FEEEF24-BBAF-45C9-824B-AF30FD093E28"}
 */
function vc2_normalizeIrregularXML(irregularString, textTags) {
	var textTag = null;
	for (var z = 0; z < textTags.length; z++) {
		textTag = textTags[z];
		var openTag = "<" + textTag + ">";
		var closeTag = "</" + textTag + ">";
		var start = irregularString.indexOf(openTag, 0);
		while (start != -1) {
			var stop = irregularString.indexOf(closeTag, start);
			var oriPart = irregularString.substring(start + openTag.length, stop);
			/** @type {String} */

			var repPart = oriPart;
			repPart = repPart.replace(/&/g, "&amp;");
			repPart = repPart.replace(/</g, "&lt;");
			repPart = repPart.replace(/>/g, "&gt;");

			if (oriPart != repPart) {
				while (irregularString.indexOf(oriPart) != -1) {
					irregularString = irregularString.replace(oriPart, repPart);
				}
			}

			start = irregularString.indexOf(openTag, stop + closeTag.length)
		}
	}

	return irregularString;
}

/**
 * @properties={typeid:24,uuid:"9EB2912A-46A1-4E26-BFB3-1D98C6DF5008"}
 */
function vc2_persistUnacceptedRecord(f, r, regNumField) {

	var fieldName = r || "mancato_invio";
	if (plugins.dialogs.showInfoDialog("Salvare i dati?", "I dati non sono stati trasmessi, tuttavia è possibile salvare i dati in VirtualCar2; un operatore provvederà al salvataggio manuale al piu presto.", "Salva in VirtualCar2", "Reinserisci i dati non accettati") == "Salva in VirtualCar2") {
		f[fieldName] = new Date();
		f["commenti"] = globals.vc2_lastHumanMessage;
		//Se e' anomalia o segnalazione setta anche id
		if (regNumField && !f[regNumField]) {
			f[regNumField] = f.k8_anomalie_id;
		}
		return true;
	} else {
		return false;
	}
}

/**
 *
 * @properties={typeid:24,uuid:"56868B52-937E-457F-B5DE-A51398FECCCA"}
 */
function vc2_progettiExportFull(s, n) {
	if (!s || !n) return null;

	if (globals.vc2_progettiCurrentNode == -1) {
		plugins.dialogs.showWarningDialog("Attenzione", "Selezionare un elemento dall'albero per effettuare l'export", "Ok");
		return null;
	} else {
		/** @type {Array} */
		var fields = s;
		/** @type {Array} */
		var labels = n;

		var form = globals.nfx_selectedProgram();

		var buffer = "";
		var initialLevel = 0;

		var fieldList = "livello,descrizione";
		for (var i = 0; i < fields.length; i++) {
			fieldList += "," + fields[i];
		}
		//FS
		//dep	var query = "select " + fieldList + " from " + forms[form].controller.getTableName() + " where path_id like ? order by path_id asc";
		var query = "select " + fieldList + " from " + databaseManager.getDataSourceTableName(forms[form].controller.getDataSource()) + " where path_id like ? order by path_id asc";

		var args = [globals.vc2_progettiCurrentNodePath];
		//dep	var d = databaseManager.getDataSetByQuery(forms[form].controller.getServerName(), query, args, -1);
		var d = databaseManager.getDataSetByQuery(databaseManager.getDataSourceServerName(forms[form].controller.getDataSource()), query, args, -1);

		var maxRow = d.getMaxRowIndex();
		var notExport = null;
		if (maxRow > 100) {
			notExport = (plugins.dialogs.showQuestionDialog("Export", "L'estrazione dei " + maxRow + " record richiesti potrebbe richiedere molto tempo, continuare?", "Sì", "No") == "No") ? true : null;
		}
		if (!notExport) {
			var dataProviders = new Array();
			var maxColumn = d.getMaxColumnIndex();
			for (var i4 = 1; i4 <= maxColumn; i4++) {
				dataProviders.push(d.getColumnAsArray(i4));
			}
			buffer = "\t";
			for (var i2 = 0; i2 < labels.length; i2++) {
				buffer += labels[i2] + "\t";
			}
			buffer += "\n";
			initialLevel = globals.vc2_progettiCurrentNodeFoundset.livello;
			globals.nfx_progressBarShow(maxRow);
			for (var i3 = 0; i3 < maxRow; i3++) {
				var indentation = "";
				for (var j = 0; j < dataProviders[0][i3] - initialLevel; j++) {
					indentation += "     ";
				}
				buffer += indentation + dataProviders[1][i3] + "\t";
				for (var j2 = 0; j2 < fields.length; j2++) {
					buffer += dataProviders[j2 + 2][i3] + "\t";
				}
				buffer += "\n";
				globals.nfx_progressBarStep();
				application.updateUI();
			}
			globals.utils_saveAsTXT(buffer, "exportAlberoProgetti-" + utils.dateFormat(new Date(), "yyyyMMdd") + ".xls");
		}
	}
	return null;
}

/**
 *
 * @properties={typeid:24,uuid:"2B78A8A0-558E-435B-AB1E-A9E7CF106AB6"}
 */
function vc2_realServices() {
	return ["http://f400anomalie.unix.ferlan.it/attorney/Scheda_Anomalie.jsp", "http://f400anomalie.unix.ferlan.it/attorney/Segnalazione_Anomalie.jsp"];
}

/**
 *
 * @properties={typeid:24,uuid:"B99C2D70-AD13-4FE0-BB88-2AC41CF49F56"}
 */
function vc2_ripristinaNonInviate(f, formName) {

	if (plugins.dialogs.showInfoDialog("Riabilitazione anomalia",
		"La segnalazione/scheda sarà ripristinata e non sarà più visibile in questa schermata, continuare?",
		"Si", "No") == "No") {
		return;
	} else {
		f.mancato_invio_anomalia = null;
		f.mancato_invio_segnalazione = null;
		f.mancato_invio = null;
		forms.nfx_interfaccia_pannello_buttons.editSave(formName);
		databaseManager.refreshRecordFromDatabase(f, -1);
	}
}

/**
 *
 * @properties={typeid:24,uuid:"3E7E48F4-35BA-40B7-AB0D-785E864C1127"}
 */
function vc2_showWebServicesErrorDialog(input)
{
	var results = new XML(input);
	application.output("XML Trasmitted : "+results);
	var errorString = "";
	var errors = [];
	var i = 1;
	for each (var result in results.*) {
    	if (result.toString() == "KO" && result.name().localName != "ESSANO"){
    		errors.push(result);
    		errorString += i++ + ". " + globals.vc2_as400ToHuman(result.name().localName) + "\n";
    	}
 	}
	errorString = !errorString ? "Nessun campo accettato" : errorString;
	return errorString;
}

/**
 * @properties={typeid:24,uuid:"B7A0BCFF-F371-4DB3-BB35-EF869A6E0FBF"}
 */
function vc2_createExportString(relation) {
	if (relation && relation.getSize() > 0) {
		/** @type {JSFoundSet} */
		var rel = relation.duplicateFoundSet();
		var ret = "";
		for (var i = 1; i <= rel.getSize(); i++) {
			rel.setSelectedIndex(i);
			var _text = (!rel.testo || typeof rel.testo != "string") ? rel.testo : rel.testo.replace(/\n/g, " ").replace(/\r/g, " ").replace(/\s+/g, " ");
			ret += _text + " (creazione: " + rel.utente_creazione + " - " + utils.dateFormat(rel.timestamp_creazione, "dd/MM/yyyy HH:mm") + ", modifica: " + rel.utente_modifica + " - " + utils.dateFormat(rel.timestamp_modifica, "dd/MM/yyyy HH:mm") + ")";
			if (i != rel.getSize()) ret += " | ";
		}
		return ret;
	} else {
		return null;
	}
}

/**
 * @properties={typeid:24,uuid:"251A28A7-02E5-4D2C-B035-1952BEBF8DB3"}
 */
function utils_mergeText(form, field, block) {
	var f = forms[form].foundset.duplicateFoundSet();
	f.sort(utils_getIdDataProvider(form) + " asc");
	var text = "";
	for (var i = 1; i <= f.getSize(); i++) {
		var r = f.getRecord(i);
		var t = globals.utils_textFormatAS400(r[field]);
		var fill = block - (t.length % block);
		for (var j = 0; j < fill; j++) {
			t += " ";
		}
		text += t;
	}
	return text;
}

/**
 * @properties={typeid:24,uuid:"F68970D0-8658-4523-B536-90A584EA8C60"}
 */
function vc2_logTransactionToAS400(table_name, table_key, logic_key, message, response, result, operation) {
	
	
//	var log = databaseManager.getFoundSet('ferrari','k8_transaction_log');
//	var index = log.newRecord();
//	var record = null;
//	record = log.getRecord(index);
//	
//	record.logged_user = globals.nfx_user;
//	record.operation_timestamp = application.getServerTimeStamp();
//	record.table_name = table_name;
//	record.table_key = table_key;
//	record.logic_key = logic_key;
//	record.vc2_message = forms.nfx_json_serializer.stringify(message);
//	record.as400_response = response;
//	record.result = (result == -1) ? -1 : 0;
//	record.operation = operation;
//	
//	databaseManager.saveData(record);
//	return record.k8_transaction_log_id;
	
	var log = databaseManager.getFoundSet('ferrari', 'k8_transaction_log');
	var query = 'select max(K8_TRANSACTION_LOG_ID) from  K8_TRANSACTION_LOG';
	var pk = databaseManager.getDataSetByQuery('ferrari', query, [], 1).getValue(1, 1)+1;
	
	log.newRecord();
	
	log.k8_transaction_log_id = pk;
	log.logged_user = globals.nfx_user;
	log.operation_timestamp = application.getServerTimeStamp();
	log.table_name = table_name;
	log.table_key = table_key;
	log.logic_key = logic_key;
	log.vc2_message = forms.nfx_json_serializer.stringify(message, null, null);
	log.as400_response = response;
	log.result = (result == -1) ? -1 : 0;
	log.operation = operation;

	databaseManager.saveData(log);
	return pk;
}

/**
 * @properties={typeid:24,uuid:"2E8BE646-156F-44A3-AD6C-DB58C1096FBE"}
 */
function vc2_getLogOperation() {
	var m = forms.nfx_interfaccia_pannello_buttons.mode;
	if (m == "add") {
		return "I";
	}
	if (m == "edit") {
		return "U";
	}
	return "B";
}

/**
 * @properties={typeid:24,uuid:"2A9D7E67-B1EB-42D1-B6D7-E18CE4D9B7F0"}
 */
function utils_colorize(_form, _elements, _color) {
	for (var i = 0; i < _elements.length; i++) {
		if (_elements[i] != "leader")
			forms[_form].elements[_elements[i]].bgcolor = _color;
	}
}

/**
 * @properties={typeid:24,uuid:"C39A140A-1B96-4777-9DFB-308F280756AF"}
 */
function utils_enable(_form, _elements, _enabled) {
	for (var i = 0; i < _elements.length; i++) {
		forms[_form].elements[_elements[i]].enabled = _enabled;
	}
}

/**
 * @properties={typeid:24,uuid:"59FAD226-391E-447C-ADFC-40A9683A6C65"}
 */
function vc2_fullReport(form, fields, title) {
	var buffer = "";
	var fs = forms[form].foundset.duplicateFoundSet();
	var max = databaseManager.getFoundSetCount(fs);

	//costruisco l'intestazione
	for (var t = 0; t < fields.length; t++) {
		buffer += fields[t].title;
		buffer += (t == fields.length - 1) ? "\n" : "\t";
	}
	//prendo i valori relativi
	globals.nfx_progressBarShow(max);
	for (var i = 1; i <= max; i++) {
		var rec = fs.getRecord(i);
		for (var j = 0; j < fields.length; j++) {
			var value = (typeof fields[j].field == "function") ? rec[fields[j].field(rec)] : rec[fields[j].field];
			if (typeof value == "string") {
				value = value.replace(/\n/g, " ");
				value = value.replace(/\t/g, " ");
			}
			if (typeof value == "object") {

				var date = new Date(value);

				value = utils.dateFormat(date, "dd/MM/yyyy");

			}
			buffer += value;
			buffer += (j == fields.length - 1) ? "\n" : "\t";

		}
		globals.nfx_progressBarStep();
	}
	globals.utils_saveAsTXT(buffer, title + "-" + utils.dateFormat(new Date(), "yyyyMMdd") + ".xls");
}

/**
 * JStaffa aggiunto parametro in entrata di tipo Boolean
 * @param {Boolean} onlyValid
 * @properties={typeid:24,uuid:"44AB1CE4-FFCC-4FC8-B8FA-8C4BD48B87C2"}
 */
function vc2_setRightDepartments(onlyValid) {
	if (onlyValid) {
		databaseManager.addTableFilterParam("ferrari", "k8_enti", "movimento", "!=", "D", "valid_departments");
	} else {
		databaseManager.removeTableFilterParam("ferrari", "valid_departments");
	}
}

/**
 * JStaffa aggiunto il tipo di parametro in entrata e un valore di ritorno
 * @param {String} userName
 * @return {Boolean}
 * @properties={typeid:24,uuid:"298793D3-5D7E-4DA4-9D6F-544EC4087180"}
 */
function isDeveloper(userName) {
	var dev = globals["nfx_developers"];
	if (userName != null) {
		return (dev.indexOf(userName.toLocaleLowerCase()) != -1) ? true : false;
	} else {
		return false;
	}
}

/**
 * @properties={typeid:24,uuid:"81F1DCE4-1E6B-44EE-AA23-E387ABF9231D"}
 */
function isPowerUser(userName) {
	var pwr = globals["nfx_developers"].concat(globals["vc2_powerUsers"]);
	return (pwr.indexOf(userName.toLocaleLowerCase()) != -1) ? true : false;
}

/**
 * @properties={typeid:24,uuid:"644D1C1F-0996-4E70-BAD2-D5E1DB2D6318"}
 * @AllowToRunInFind
 */
function vc2_isSottogruppoFunzionaleRequired(group) {
	var sf_rec = forms.liste_valori.foundset.duplicateFoundSet();
	if (sf_rec.find()) {
		sf_rec.valuelist_name = "Gruppi";
		sf_rec.valore_1 = group;
		sf_rec.search();
	}
	return sf_rec.valore_3;
}

/**
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"8F00E763-E05E-4EBF-848C-7438D456303F"}
 */
function vc2_loadChartConfig(event) {
	forms.report_anomalie_container.loadConfig(null, event);
}

/**
 * @param {String} _formName
 * @param {Array<Object>} _fieldsChoices
 * @param {Array<Object>} _fieldsList
 *
 * @properties={typeid:24,uuid:"20B98CC8-B488-4B31-B896-6199A0BEDB54"}
 */
function vc2_drawFieldsInForm(_formName, _fieldsChoices, _fieldsList) {
	forms[_formName].controller.setDesignMode(true);
	var _formSM = solutionModel.getForm(_formName);

	//Cleanup the form
	while (_formSM.getFields() && _formSM.getFields().length) {
		var _n = _formSM.getFields()[0].name;
		_formSM.removeField(_n);
		_formSM.removeLabel(_n + "_label");
		_formSM.removeLabel(_n + "_btn");
		_formSM.removeLabel(_n + "_btn_lbl");
	}

	//Create new fields and labels
	var _next = 0;
	for (var i = 0; i < _fieldsList.length; i++) {
		var _ff = _fieldsChoices[_fieldsList[i]];
		/** @type {JSField} */
		var _fieldSM = _formSM.newField(_ff.dataprovider, _ff.type, _next, 30, _ff.size, 20);
		if (_ff.type == JSField.IMAGE_MEDIA) {
			_fieldSM.scrollbars = SM_SCROLLBAR.HORIZONTAL_SCROLLBAR_NEVER | SM_SCROLLBAR.VERTICAL_SCROLLBAR_NEVER;
		}
		_fieldSM.name = _ff.name;
		_fieldSM.editable = _ff.editable;
		_fieldSM.format = _ff.format;
		_fieldSM.horizontalAlignment = _ff.align;
		_fieldSM.valuelist = solutionModel.getValueList(_ff.valuelist);
		//FS
		//dep		_fieldSM.onDataChange = _formSM.getFormMethod(_ff.method);
		_fieldSM.onDataChange = _formSM.getMethod(_ff.method);
		_fieldSM.borderType = "Etched";
		_fieldSM.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH | SM_ANCHOR.EAST;
		//FS
		/** @type {String} */
		var textLabel = _fieldsList[i];
		//dep		var _labelSM = _formSM.newLabel(_fieldsList[i], _next, 0, _ff.size, 20);
		var _labelSM = _formSM.newLabel(textLabel, _next, 0, _ff.size, 20);

		_labelSM.toolTipText = _fieldsList[i];
		_labelSM.name = _ff.name + "_label";
		_labelSM.labelFor = _ff.name;
		_labelSM.horizontalAlignment = SM_ALIGNMENT.CENTER;
		_labelSM.anchors = SM_ANCHOR.WEST | SM_ANCHOR.NORTH | SM_ANCHOR.EAST;
		_labelSM.transparent = false;
		_labelSM.styleClass = "table";

		_next += _ff.size + 1;

		//If present create a buttn for the specified field
		if (_ff.button) {
			//FS
			//dep		var _buttonSM = _formSM.newButton("", _next, 30, 20, 20, _formSM.getFormMethod(_ff.button.action));
			var _buttonSM = _formSM.newButton("", _next, 30, 20, 20, _formSM.getMethod(_ff.button.action));

			_buttonSM.name = _ff.name + "_btn";
			_buttonSM.showClick = false;
			_buttonSM.showFocus = false;
			_buttonSM.imageMedia = solutionModel.getMedia(_ff.button.icon);
			_buttonSM.mediaOptions = SM_MEDIAOPTION.REDUCE | SM_MEDIAOPTION.KEEPASPECT;
			_buttonSM.borderType = "Etched";

			var _lbuttonSM = _formSM.newLabel("", _next, 0, 20, 20);
			_lbuttonSM.toolTipText = _ff.button.tooltip;
			_lbuttonSM.name = _ff.name + "_btn_lbl";
			_lbuttonSM.labelFor = _ff.name + "_btn";
			_lbuttonSM.horizontalAlignment = SM_ALIGNMENT.CENTER;
			_lbuttonSM.transparent = false;
			_lbuttonSM.styleClass = "table";

			_next += 20 + 1;
		}
	}

	forms[_formName].controller.setDesignMode(false);
}

/**
 * Callback method when form is (re)loaded.
 *
 * @param {JSEvent} event the event that triggered the action
 *
 * @properties={typeid:24,uuid:"C8F47AF6-5C3C-4A10-B8F6-4D68B1AA590E"}
 */
function vc2_loadCostomizedColumns(event) {
	var _l = forms.nfx_interfaccia_gestione_salvataggi.load(event.getFormName() + "_customColumns" + "_default");
	if (_l) forms[event.getFormName()]._fields = _l;
	globals.vc2_drawFieldsInForm(event.getFormName(), forms[event.getFormName()]._choices, forms[event.getFormName()]._fields);
}

/**
 * @properties={typeid:24,uuid:"E7B1A780-3641-484B-83C0-2471D1934CF8"}
 * @AllowToRunInFind
 */
function vc2_showOnLoad(_a) {
	var _address = _a.split("|");
	var _fs = forms[_address[0]].foundset.duplicateFoundSet();
	_fs.loadAllRecords();
	if (_fs.find()) {
		_fs[_address[1]] = _address[2];
		if (_fs.search()) {
			forms[_address[0]].foundset.loadRecords(_fs);
			forms.nfx_interfaccia_pannello_tree.goTo(_address[0]);

		} else {
			plugins.dialogs.showErrorDialog("Errore", "Il record cercato non esiste, se hai seguito un link contatta un amministratore.", "Ok");
		}
	}
}
